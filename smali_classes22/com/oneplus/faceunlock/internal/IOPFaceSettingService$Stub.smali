.class public abstract Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub;
.super Landroid/os/Binder;
.source "IOPFaceSettingService.java"

# interfaces
.implements Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.oneplus.faceunlock.internal.IOPFaceSettingService"

.field static final TRANSACTION_checkState:I = 0x1

.field static final TRANSACTION_removeFace:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 29
    const-string v0, "com.oneplus.faceunlock.internal.IOPFaceSettingService"

    invoke-virtual {p0, p0, v0}, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 37
    if-nez p0, :cond_0

    .line 38
    const/4 v0, 0x0

    return-object v0

    .line 40
    :cond_0
    const-string v0, "com.oneplus.faceunlock.internal.IOPFaceSettingService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 41
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    if-eqz v1, :cond_1

    .line 42
    move-object v1, v0

    check-cast v1, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    return-object v1

    .line 44
    :cond_1
    new-instance v1, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method

.method public static getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;
    .locals 1

    .line 152
    sget-object v0, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub$Proxy;->sDefaultImpl:Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    return-object v0
.end method

.method public static setDefaultImpl(Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;)Z
    .locals 1
    .param p0, "impl"    # Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    .line 145
    sget-object v0, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub$Proxy;->sDefaultImpl:Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 146
    sput-object p0, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub$Proxy;->sDefaultImpl:Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    .line 147
    const/4 v0, 0x1

    return v0

    .line 149
    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 48
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 52
    const-string v0, "com.oneplus.faceunlock.internal.IOPFaceSettingService"

    .line 53
    .local v0, "descriptor":Ljava/lang/String;
    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v2, 0x2

    if-eq p1, v2, :cond_1

    const v2, 0x5f4e5446

    if-eq p1, v2, :cond_0

    .line 81
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    return v1

    .line 57
    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    return v1

    .line 72
    :cond_1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 75
    .local v2, "_arg0":I
    invoke-virtual {p0, v2}, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub;->removeFace(I)V

    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 77
    return v1

    .line 62
    .end local v2    # "_arg0":I
    :cond_2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 65
    .restart local v2    # "_arg0":I
    invoke-virtual {p0, v2}, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService$Stub;->checkState(I)I

    move-result v3

    .line 66
    .local v3, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 67
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 68
    return v1
.end method
