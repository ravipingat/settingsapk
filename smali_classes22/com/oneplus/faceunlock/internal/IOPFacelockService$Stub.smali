.class public abstract Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;
.super Landroid/os/Binder;
.source "IOPFacelockService.java"

# interfaces
.implements Lcom/oneplus/faceunlock/internal/IOPFacelockService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oneplus/faceunlock/internal/IOPFacelockService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.oneplus.faceunlock.internal.IOPFacelockService"

.field static final TRANSACTION_prepare:I = 0x1

.field static final TRANSACTION_registerCallback:I = 0x5

.field static final TRANSACTION_release:I = 0x4

.field static final TRANSACTION_startFaceUnlock:I = 0x2

.field static final TRANSACTION_stopFaceUnlock:I = 0x3

.field static final TRANSACTION_unregisterCallback:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 40
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 41
    const-string v0, "com.oneplus.faceunlock.internal.IOPFacelockService"

    invoke-virtual {p0, p0, v0}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/oneplus/faceunlock/internal/IOPFacelockService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 49
    if-nez p0, :cond_0

    .line 50
    const/4 v0, 0x0

    return-object v0

    .line 52
    :cond_0
    const-string v0, "com.oneplus.faceunlock.internal.IOPFacelockService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 53
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/oneplus/faceunlock/internal/IOPFacelockService;

    if-eqz v1, :cond_1

    .line 54
    move-object v1, v0

    check-cast v1, Lcom/oneplus/faceunlock/internal/IOPFacelockService;

    return-object v1

    .line 56
    :cond_1
    new-instance v1, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method

.method public static getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFacelockService;
    .locals 1

    .line 271
    sget-object v0, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub$Proxy;->sDefaultImpl:Lcom/oneplus/faceunlock/internal/IOPFacelockService;

    return-object v0
.end method

.method public static setDefaultImpl(Lcom/oneplus/faceunlock/internal/IOPFacelockService;)Z
    .locals 1
    .param p0, "impl"    # Lcom/oneplus/faceunlock/internal/IOPFacelockService;

    .line 264
    sget-object v0, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub$Proxy;->sDefaultImpl:Lcom/oneplus/faceunlock/internal/IOPFacelockService;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 265
    sput-object p0, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub$Proxy;->sDefaultImpl:Lcom/oneplus/faceunlock/internal/IOPFacelockService;

    .line 266
    const/4 v0, 0x1

    return v0

    .line 268
    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 60
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 64
    const-string v0, "com.oneplus.faceunlock.internal.IOPFacelockService"

    .line 65
    .local v0, "descriptor":Ljava/lang/String;
    const v1, 0x5f4e5446

    const/4 v2, 0x1

    if-eq p1, v1, :cond_0

    packed-switch p1, :pswitch_data_0

    .line 124
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    return v1

    .line 115
    :pswitch_0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v1

    .line 118
    .local v1, "_arg0":Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;
    invoke-virtual {p0, v1}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;->unregisterCallback(Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;)V

    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    return v2

    .line 106
    .end local v1    # "_arg0":Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;
    :pswitch_1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v1

    .line 109
    .restart local v1    # "_arg0":Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;
    invoke-virtual {p0, v1}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;->registerCallback(Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;)V

    .line 110
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 111
    return v2

    .line 99
    .end local v1    # "_arg0":Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;
    :pswitch_2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;->release()V

    .line 101
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 102
    return v2

    .line 90
    :pswitch_3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 93
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;->stopFaceUnlock(I)V

    .line 94
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 95
    return v2

    .line 81
    .end local v1    # "_arg0":I
    :pswitch_4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 84
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;->startFaceUnlock(I)V

    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    return v2

    .line 74
    .end local v1    # "_arg0":I
    :pswitch_5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lcom/oneplus/faceunlock/internal/IOPFacelockService$Stub;->prepare()V

    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 77
    return v2

    .line 69
    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
