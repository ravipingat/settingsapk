.class Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub$Proxy;
.super Ljava/lang/Object;
.source "IOPFacelockCallback.java"

# interfaces
.implements Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# static fields
.field public static sDefaultImpl:Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 111
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .line 118
    const-string v0, "com.oneplus.faceunlock.internal.IOPFacelockCallback"

    return-object v0
.end method

.method public onBeginRecognize(I)V
    .locals 4
    .param p1, "faceId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 122
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 124
    .local v0, "_data":Landroid/os/Parcel;
    :try_start_0
    const-string v1, "com.oneplus.faceunlock.internal.IOPFacelockCallback"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 126
    iget-object v1, p0, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v1, v3, v0, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    .line 127
    .local v1, "_status":Z
    if-nez v1, :cond_0

    invoke-static {}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 128
    invoke-static {}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;->onBeginRecognize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 129
    return-void

    .line 133
    .end local v1    # "_status":Z
    :cond_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 134
    nop

    .line 135
    return-void

    .line 133
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v1
.end method

.method public onCompared(IIIII)V
    .locals 9
    .param p1, "faceId"    # I
    .param p2, "userId"    # I
    .param p3, "result"    # I
    .param p4, "compareTimeMillis"    # I
    .param p5, "score"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 138
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 140
    .local v0, "_data":Landroid/os/Parcel;
    :try_start_0
    const-string v1, "com.oneplus.faceunlock.internal.IOPFacelockCallback"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 142
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 145
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 146
    iget-object v1, p0, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    .line 147
    .local v1, "_status":Z
    if-nez v1, :cond_0

    invoke-static {}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 148
    invoke-static {}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v3

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move v8, p5

    invoke-interface/range {v3 .. v8}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;->onCompared(IIIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 149
    return-void

    .line 153
    .end local v1    # "_status":Z
    :cond_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 154
    nop

    .line 155
    return-void

    .line 153
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v1
.end method

.method public onEndRecognize(III)V
    .locals 5
    .param p1, "faceId"    # I
    .param p2, "userId"    # I
    .param p3, "result"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 158
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 160
    .local v0, "_data":Landroid/os/Parcel;
    :try_start_0
    const-string v1, "com.oneplus.faceunlock.internal.IOPFacelockCallback"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 162
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 164
    iget-object v1, p0, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    .line 165
    .local v1, "_status":Z
    if-nez v1, :cond_0

    invoke-static {}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 166
    invoke-static {}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback$Stub;->getDefaultImpl()Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lcom/oneplus/faceunlock/internal/IOPFacelockCallback;->onEndRecognize(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 167
    return-void

    .line 171
    .end local v1    # "_status":Z
    :cond_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 172
    nop

    .line 173
    return-void

    .line 171
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v1
.end method
