.class public Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;
.super Landroid/widget/FrameLayout;
.source "OPFingerPrintFodBgAnimView.java"


# static fields
.field public static final ANIM_DELAY_TIME:I = 0x20

.field public static final PATH_1:I = 0x0

.field public static final PATH_2:I = 0x1

.field public static final PATH_3:I = 0x2

.field public static final PATH_4:I = 0x3

.field public static final PATH_5:I = 0x4

.field public static final PATH_6:I = 0x5

.field public static final PATH_7:I = 0x6


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFodBgView:Landroid/widget/FrameLayout;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mPath_1:Landroid/widget/ImageView;

.field private mPath_10:Landroid/widget/ImageView;

.field private mPath_11:Landroid/widget/ImageView;

.field private mPath_2:Landroid/widget/ImageView;

.field private mPath_3:Landroid/widget/ImageView;

.field private mPath_4:Landroid/widget/ImageView;

.field private mPath_5:Landroid/widget/ImageView;

.field private mPath_6:Landroid/widget/ImageView;

.field private mPath_7:Landroid/widget/ImageView;

.field private mPath_8:Landroid/widget/ImageView;

.field private mPath_9:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 63
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 65
    invoke-virtual {p0, p1}, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->initView(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->initView(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    invoke-virtual {p0, p1}, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->initView(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method public static autoGenericCode(II)Ljava/lang/String;
    .locals 4
    .param p0, "code"    # I
    .param p1, "num"    # I

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method public hide(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .line 127
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 128
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 130
    :cond_0
    return-void
.end method

.method public initBgAnimView(Landroid/content/Context;Landroid/widget/FrameLayout;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "container"    # Landroid/widget/FrameLayout;

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->setCenterVisible(Z)V

    .line 89
    return-void
.end method

.method public initView(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 69
    iput-object p1, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mContext:Landroid/content/Context;

    .line 70
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 71
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0d01a4

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    .line 73
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04d9

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_1:Landroid/widget/ImageView;

    .line 74
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04da

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_2:Landroid/widget/ImageView;

    .line 75
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04db

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_3:Landroid/widget/ImageView;

    .line 76
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04dc

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_4:Landroid/widget/ImageView;

    .line 77
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04dd

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_5:Landroid/widget/ImageView;

    .line 78
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04de

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_6:Landroid/widget/ImageView;

    .line 79
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04df

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_7:Landroid/widget/ImageView;

    .line 80
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04e0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_8:Landroid/widget/ImageView;

    .line 81
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04e1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_9:Landroid/widget/ImageView;

    .line 82
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04e2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_10:Landroid/widget/ImageView;

    .line 83
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a04e3

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_11:Landroid/widget/ImageView;

    .line 84
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mFodBgView:Landroid/widget/FrameLayout;

    invoke-virtual {p0, p1, v0}, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->initBgAnimView(Landroid/content/Context;Landroid/widget/FrameLayout;)V

    .line 85
    return-void
.end method

.method public setCenterVisible(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .line 98
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_1:Landroid/widget/ImageView;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_2:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    move v3, v1

    goto :goto_1

    :cond_1
    move v3, v2

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_3:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    move v3, v1

    goto :goto_2

    :cond_2
    move v3, v2

    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_4:Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move v1, v2

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_5:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_6:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_7:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_8:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_9:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_10:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_11:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    return-void
.end method

.method public setEdgeVisible(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .line 112
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_1:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_3:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_4:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_5:Landroid/widget/ImageView;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_6:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    move v3, v2

    goto :goto_1

    :cond_1
    move v3, v1

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_7:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    move v3, v2

    goto :goto_2

    :cond_2
    move v3, v1

    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_8:Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    move v3, v2

    goto :goto_3

    :cond_3
    move v3, v1

    :goto_3
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_9:Landroid/widget/ImageView;

    if-eqz p1, :cond_4

    move v3, v2

    goto :goto_4

    :cond_4
    move v3, v1

    :goto_4
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_10:Landroid/widget/ImageView;

    if-eqz p1, :cond_5

    move v3, v2

    goto :goto_5

    :cond_5
    move v3, v1

    :goto_5
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_11:Landroid/widget/ImageView;

    if-eqz p1, :cond_6

    move v1, v2

    :cond_6
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 124
    return-void
.end method

.method public startTouchDownAnim()V
    .locals 21

    .line 134
    move-object/from16 v0, p0

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 135
    .local v1, "animSet":Landroid/view/animation/AnimationSet;
    new-instance v12, Landroid/view/animation/ScaleAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3f70a3d7    # 0.94f

    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3f70a3d7    # 0.94f

    const/4 v8, 0x2

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v10, 0x2

    const/high16 v11, 0x3f000000    # 0.5f

    move-object v3, v12

    invoke-direct/range {v3 .. v11}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 136
    .local v3, "scalSmallAnim":Landroid/view/animation/ScaleAnimation;
    invoke-virtual {v3, v2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 137
    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 138
    const-wide/16 v6, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 139
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 141
    new-instance v6, Landroid/view/animation/ScaleAnimation;

    const/high16 v9, 0x3f800000    # 1.0f

    const v10, 0x3f882b93

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f882b93

    const/4 v13, 0x2

    const/high16 v14, 0x3f000000    # 0.5f

    const/4 v15, 0x2

    const/high16 v16, 0x3f000000    # 0.5f

    move-object v8, v6

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 142
    .local v6, "scalToNormalAnim":Landroid/view/animation/ScaleAnimation;
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 143
    const-wide/16 v8, 0x96

    invoke-virtual {v6, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 144
    const-wide/16 v10, 0x21e

    invoke-virtual {v6, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 145
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 146
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_1:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 147
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_5:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 150
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v1, v10

    .line 151
    new-instance v19, Landroid/view/animation/ScaleAnimation;

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f70a3d7    # 0.94f

    const/high16 v13, 0x3f800000    # 1.0f

    const v14, 0x3f70a3d7    # 0.94f

    const/16 v17, 0x2

    const/high16 v18, 0x3f000000    # 0.5f

    move-object/from16 v10, v19

    invoke-direct/range {v10 .. v18}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v3, v19

    .line 152
    invoke-virtual {v3, v2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 153
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 154
    const-wide/16 v10, 0x20

    invoke-virtual {v3, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 155
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 157
    new-instance v10, Landroid/view/animation/ScaleAnimation;

    const v14, 0x3f882b93

    const/high16 v15, 0x3f800000    # 1.0f

    const v16, 0x3f882b93

    const/16 v19, 0x2

    const/high16 v20, 0x3f000000    # 0.5f

    move-object v12, v10

    invoke-direct/range {v12 .. v20}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v6, v10

    .line 158
    invoke-virtual {v6, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 159
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 160
    const-wide/16 v10, 0x1fe

    invoke-virtual {v6, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 161
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 162
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_2:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 163
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_6:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 166
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v1, v10

    .line 167
    new-instance v19, Landroid/view/animation/ScaleAnimation;

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f70a3d7    # 0.94f

    const v14, 0x3f70a3d7    # 0.94f

    const/4 v15, 0x2

    const/high16 v16, 0x3f000000    # 0.5f

    move-object/from16 v10, v19

    invoke-direct/range {v10 .. v18}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v3, v19

    .line 168
    invoke-virtual {v3, v2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 169
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 170
    const-wide/16 v10, 0x40

    invoke-virtual {v3, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 171
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 173
    new-instance v10, Landroid/view/animation/ScaleAnimation;

    const v14, 0x3f882b93

    const/high16 v15, 0x3f800000    # 1.0f

    const v16, 0x3f882b93

    const/16 v19, 0x2

    move-object v12, v10

    invoke-direct/range {v12 .. v20}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v6, v10

    .line 174
    invoke-virtual {v6, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 175
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 176
    const-wide/16 v10, 0x1de

    invoke-virtual {v6, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 177
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 178
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_3:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 179
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_7:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 182
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v1, v10

    .line 183
    new-instance v19, Landroid/view/animation/ScaleAnimation;

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f70a3d7    # 0.94f

    const v14, 0x3f70a3d7    # 0.94f

    const/4 v15, 0x2

    const/high16 v16, 0x3f000000    # 0.5f

    move-object/from16 v10, v19

    invoke-direct/range {v10 .. v18}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v3, v19

    .line 184
    invoke-virtual {v3, v2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 185
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 186
    const-wide/16 v10, 0x60

    invoke-virtual {v3, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 187
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 189
    new-instance v10, Landroid/view/animation/ScaleAnimation;

    const v14, 0x3f882b93

    const/high16 v15, 0x3f800000    # 1.0f

    const v16, 0x3f882b93

    const/16 v19, 0x2

    move-object v12, v10

    invoke-direct/range {v12 .. v20}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v6, v10

    .line 190
    invoke-virtual {v6, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 191
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 192
    const-wide/16 v10, 0x1be

    invoke-virtual {v6, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 193
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 194
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_4:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 195
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_8:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 198
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v1, v10

    .line 199
    new-instance v19, Landroid/view/animation/ScaleAnimation;

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f70a3d7    # 0.94f

    const v14, 0x3f70a3d7    # 0.94f

    const/4 v15, 0x2

    const/high16 v16, 0x3f000000    # 0.5f

    move-object/from16 v10, v19

    invoke-direct/range {v10 .. v18}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v3, v19

    .line 200
    invoke-virtual {v3, v2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 201
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 202
    const-wide/16 v10, 0x80

    invoke-virtual {v3, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 203
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 205
    new-instance v10, Landroid/view/animation/ScaleAnimation;

    const v14, 0x3f882b93

    const/high16 v15, 0x3f800000    # 1.0f

    const v16, 0x3f882b93

    const/16 v19, 0x2

    move-object v12, v10

    invoke-direct/range {v12 .. v20}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v6, v10

    .line 206
    invoke-virtual {v6, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 207
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 208
    const-wide/16 v10, 0x19e

    invoke-virtual {v6, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 209
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 210
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_9:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 213
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v1, v10

    .line 214
    new-instance v19, Landroid/view/animation/ScaleAnimation;

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f70a3d7    # 0.94f

    const v14, 0x3f70a3d7    # 0.94f

    const/4 v15, 0x2

    const/high16 v16, 0x3f000000    # 0.5f

    move-object/from16 v10, v19

    invoke-direct/range {v10 .. v18}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v3, v19

    .line 215
    invoke-virtual {v3, v2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 216
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 217
    const-wide/16 v10, 0xa0

    invoke-virtual {v3, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 218
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 220
    new-instance v10, Landroid/view/animation/ScaleAnimation;

    const v14, 0x3f882b93

    const/high16 v15, 0x3f800000    # 1.0f

    const v16, 0x3f882b93

    const/16 v19, 0x2

    move-object v12, v10

    invoke-direct/range {v12 .. v20}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v6, v10

    .line 221
    invoke-virtual {v6, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 222
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 223
    const-wide/16 v10, 0x17e

    invoke-virtual {v6, v10, v11}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 224
    invoke-virtual {v1, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 225
    iget-object v10, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_10:Landroid/widget/ImageView;

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 228
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v1, v10

    .line 229
    new-instance v19, Landroid/view/animation/ScaleAnimation;

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f70a3d7    # 0.94f

    const v14, 0x3f70a3d7    # 0.94f

    const/4 v15, 0x2

    const/high16 v16, 0x3f000000    # 0.5f

    move-object/from16 v10, v19

    invoke-direct/range {v10 .. v18}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v3, v19

    .line 230
    invoke-virtual {v3, v2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 231
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 232
    const-wide/16 v4, 0xc0

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 233
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 235
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    const v12, 0x3f882b93

    const v14, 0x3f882b93

    move-object v10, v2

    invoke-direct/range {v10 .. v18}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 236
    .end local v6    # "scalToNormalAnim":Landroid/view/animation/ScaleAnimation;
    .local v2, "scalToNormalAnim":Landroid/view/animation/ScaleAnimation;
    invoke-virtual {v2, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 237
    invoke-virtual {v2, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 238
    const-wide/16 v4, 0x15e

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 239
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 240
    iget-object v4, v0, Lcom/oneplus/settings/opfinger/OPFingerPrintFodBgAnimView;->mPath_11:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 241
    return-void
.end method
