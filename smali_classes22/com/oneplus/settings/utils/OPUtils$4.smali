.class Lcom/oneplus/settings/utils/OPUtils$4;
.super Ljava/lang/Object;
.source "OPUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/oneplus/settings/utils/OPUtils;->disableCardPackageEntranceInLauncher(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1121
    iput-object p1, p0, Lcom/oneplus/settings/utils/OPUtils$4;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1124
    iget-object v0, p0, Lcom/oneplus/settings/utils/OPUtils$4;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1125
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "com.oneplus.card"

    .line 1126
    .local v1, "pkgName":Ljava/lang/String;
    const-string v2, "com.oneplus.card.entity.activity.CardlistActivity"

    .line 1127
    .local v2, "mainActivityName":Ljava/lang/String;
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isO2()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/oneplus/settings/utils/OPUtils$4;->val$context:Landroid/content/Context;

    .line 1128
    invoke-static {v3}, Lcom/oneplus/settings/utils/OPUtils;->access$000(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1130
    :try_start_0
    const-string v3, "OPUtils"

    const-string v4, "disableCardPackageEntranceInLauncher"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1131
    iget-object v3, p0, Lcom/oneplus/settings/utils/OPUtils$4;->val$context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1132
    .local v3, "pm":Landroid/content/pm/PackageManager;
    new-instance v4, Landroid/content/ComponentName;

    invoke-direct {v4, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1139
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    goto :goto_0

    .line 1137
    :catch_0
    move-exception v3

    .line 1138
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 1141
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method
