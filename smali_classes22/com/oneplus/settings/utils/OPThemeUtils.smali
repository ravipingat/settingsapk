.class public final Lcom/oneplus/settings/utils/OPThemeUtils;
.super Ljava/lang/Object;
.source "OPThemeUtils.java"


# static fields
.field private static CLOCK_TYPE_ANALOG:I = 0x0

.field private static CLOCK_TYPE_DEFAULT:I = 0x0

.field private static CLOCK_TYPE_MINIMALISM:I = 0x0

.field private static CLOCK_TYPE_NONE:I = 0x0

.field public static KEY_AOD_CLOCK_STYLE:Ljava/lang/String; = null

.field public static final KEY_DARK_MODE_ACTION:Ljava/lang/String; = "oem_black_mode"

.field public static final KEY_ORIGIN_DARK_MODE_ACTION:Ljava/lang/String; = "origin_oem_black_mode"

.field public static final OEM_BLACK_MODE_ACCENT_COLOR:Ljava/lang/String; = "oem_black_mode_accent_color"

.field private static final OEM_BLACK_MODE_ACCENT_COLOR_INDEX:Ljava/lang/String; = "oem_black_mode_accent_color_index"

.field public static final OEM_WHITE_MODE_ACCENT_COLOR:Ljava/lang/String; = "oem_white_mode_accent_color"

.field private static final OEM_WHITE_MODE_ACCENT_COLOR_INDEX:Ljava/lang/String; = "oem_white_mode_accent_color_index"

.field public static final OP_COLORFUL_WALLPAPER_DEFAULTE_NAME:Ljava/lang/String; = "wallpaper_g_live_gold"

.field public static final OP_CUSTOMIZATION_THEME_ANDROID_MODE:I = 0x2

.field public static final OP_CUSTOMIZATION_THEME_DARK_MODE:I = 0x1

.field public static final OP_CUSTOMIZATION_THEME_LIGHT_MODE:I = 0x0

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION:Ljava/lang/String; = "oneplus_aodnotification"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_BLUE:Ljava/lang/String; = "blue"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_BLUE_INDEX:I = 0x0

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_GOLD:Ljava/lang/String; = "gold"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_GOLD_INDEX:I = 0x2

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_MCL:Ljava/lang/String; = "mcl"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_MCL_INDEX:I = 0xa

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_PURPLE:Ljava/lang/String; = "purple"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_PURPLE_INDEX:I = 0x3

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_RED:Ljava/lang/String; = "red"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_AODNOTIFICATION_RED_INDEX:I = 0x1

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_BASICCOLOR:Ljava/lang/String; = "oneplus_basiccolor"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_BASICCOLOR_BLCAK:Ljava/lang/String; = "black"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_BASICCOLOR_WHITE:Ljava/lang/String; = "white"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_DYNAMICFONT:Ljava/lang/String; = "oneplus_dynamicfont"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_DYNAMICFONT_NOTOSANS:Ljava/lang/String; = "1"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_DYNAMICFONT_NOTOSANS_INDEX:I = 0x1

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_DYNAMICFONT_SLATE_OF_ONEPLUS_STYLE:Ljava/lang/String; = "2"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_DYNAMICFONT_SLATE_OF_ONEPLUS_STYLE_INDEX:I = 0x2

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE:Ljava/lang/String; = "oneplus_shape"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_CIRCLE:Ljava/lang/String; = "circle"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_CIRCLE_INDEX:I = 0x1

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_ROUNDEDRECT:Ljava/lang/String; = "roundedrect"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_ROUNDEDRECT_INDEX:I = 0x2

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_SQUIRCLE:Ljava/lang/String; = "squircle"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_SQUIRCLE_INDEX:I = 0x4

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_TEARDROP:Ljava/lang/String; = "teardrop"

.field public static final OP_CUSTOMIZATION_THEME_ONEPLUS_SHAPE_TEARDROP_INDEX:I = 0x3

.field public static final OP_CUSTOMIZATION_THEME_STYLE:Ljava/lang/String; = "op_customization_theme_style"

.field public static final OP_DARK_WALLPAPER_DEFAULT_NAME:Ljava/lang/String; = "wallpaper_g_live_gray"

.field public static final OP_LIGHT_WALLPAPER_DEFAULT_NAME:Ljava/lang/String; = "wallpaper_g_live_blue"

.field public static final OP_OEM_FONT_MODE:Ljava/lang/String; = "oem_font_mode"

.field public static final THEME_ANDROID_MODE:I = 0x2

.field public static final THEME_DARK_MODE:I = 0x1

.field public static final THEME_LIGHT_MODE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 103
    const-string v0, "aod_clock_style"

    sput-object v0, Lcom/oneplus/settings/utils/OPThemeUtils;->KEY_AOD_CLOCK_STYLE:Ljava/lang/String;

    .line 104
    const/4 v0, 0x0

    sput v0, Lcom/oneplus/settings/utils/OPThemeUtils;->CLOCK_TYPE_DEFAULT:I

    .line 105
    const/4 v0, 0x1

    sput v0, Lcom/oneplus/settings/utils/OPThemeUtils;->CLOCK_TYPE_ANALOG:I

    .line 106
    const/4 v0, 0x2

    sput v0, Lcom/oneplus/settings/utils/OPThemeUtils;->CLOCK_TYPE_MINIMALISM:I

    .line 107
    const/4 v0, 0x3

    sput v0, Lcom/oneplus/settings/utils/OPThemeUtils;->CLOCK_TYPE_NONE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static disableAllColorfulThemes(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 324
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 325
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v1, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 327
    .local v1, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    const-string v2, "oneplus_basiccolor"

    const-string v3, "white"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 329
    const-string v3, "black"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 331
    const/4 v2, 0x2

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 333
    const-string v3, "oneplus_dynamicfont"

    const-string v4, "2"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 335
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 337
    const-string v3, "oneplus_shape"

    const-string v4, "squircle"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 339
    const/4 v3, 0x4

    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 341
    const-string v3, "oneplus_aodnotification"

    const-string v4, "gold"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 343
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 344
    return-void
.end method

.method public static disableAllDarkThemes(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 303
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 304
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v1, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 306
    .local v1, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    const-string v2, "oneplus_basiccolor"

    const-string v3, "black"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 308
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 310
    const-string v3, "oneplus_dynamicfont"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 312
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 314
    const-string v2, "oneplus_shape"

    const-string v3, "roundedrect"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 316
    const/4 v2, 0x2

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 318
    const-string v2, "oneplus_aodnotification"

    const-string v3, "purple"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    const/4 v2, 0x3

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 320
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 321
    return-void
.end method

.method public static disableAllLightThemes(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .line 278
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 279
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v1, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 281
    .local v1, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    const-string v2, "oneplus_basiccolor"

    const-string v3, "white"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 283
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 285
    const-string v3, "oneplus_dynamicfont"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 287
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 289
    const-string v4, "oneplus_shape"

    const-string v5, "circle"

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 291
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 293
    const-string v3, "oneplus_aodnotification"

    const-string v4, "red"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 295
    const-string v4, "gold"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 297
    const-string v4, "purple"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 299
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 300
    return-void
.end method

.method public static disableAllThemes(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 347
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 348
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v1, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 350
    .local v1, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    const/4 v2, 0x2

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 352
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 353
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 354
    const-string v3, "oneplus_dynamicfont"

    const-string v4, "2"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 356
    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 359
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 360
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 361
    const-string v2, "oneplus_shape"

    const-string v3, "squircle"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 363
    const-string v3, "circle"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 365
    const-string v3, "teardrop"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 367
    const-string v3, "roundedrect"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 370
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 371
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 372
    const-string v2, "oneplus_aodnotification"

    const-string v3, "gold"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 374
    const-string v3, "red"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 376
    const-string v3, "purple"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 378
    return-void
.end method

.method public static disableCurrentTheme(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 261
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->getCurrentCustomizationTheme(Landroid/content/Context;)I

    move-result v0

    .line 262
    .local v0, "currentTheme":I
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 270
    :cond_0
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->disableAllColorfulThemes(Landroid/content/Context;)V

    .line 271
    goto :goto_0

    .line 267
    :cond_1
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->disableAllDarkThemes(Landroid/content/Context;)V

    .line 268
    goto :goto_0

    .line 264
    :cond_2
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->disableAllLightThemes(Landroid/content/Context;)V

    .line 265
    nop

    .line 275
    :goto_0
    return-void
.end method

.method public static disableTheme(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "secondCategory"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .line 254
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 255
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v1, p2}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 256
    .local v1, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    invoke-virtual {v1, v0}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 258
    return-void
.end method

.method private static enableAospDarkTheme(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enable"    # Z

    .line 569
    const-class v0, Landroid/app/UiModeManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 570
    if-eqz p1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    .line 571
    return-void
.end method

.method public static enableColorfulThemes(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .line 478
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "origin_oem_black_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 481
    .local v0, "themeMode":I
    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    .line 482
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 487
    :cond_0
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableAospDarkTheme(Landroid/content/Context;Z)V

    .line 488
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->disableAllThemes(Landroid/content/Context;)V

    .line 490
    const-string v1, "wallpaper_g_live_gold"

    invoke-static {p0, v1}, Lcom/oneplus/settings/utils/OPThemeUtils;->setDefaultWallpaper(Landroid/content/Context;Ljava/lang/String;)V

    .line 491
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 492
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v4, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 494
    .local v4, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 495
    .local v5, "disableMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "oneplus_basiccolor"

    const-string v7, "white"

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    invoke-virtual {v4, v5}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 497
    const-string v7, "black"

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    invoke-virtual {v4, v5}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 499
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 501
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 502
    .local v6, "mapEnable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v7, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v7, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 504
    .local v7, "opThemeEnable":Lcom/oneplus/sdk/utils/OpTheme;
    const-string v8, "oneplus_dynamicfont"

    const-string v9, "2"

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 507
    const-string v8, "oneplus_shape"

    const-string v9, "squircle"

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    const/4 v8, 0x4

    invoke-static {p0, v8}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 510
    const-string v8, "oneplus_aodnotification"

    const-string v9, "gold"

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 512
    invoke-virtual {v7, v6}, Lcom/oneplus/sdk/utils/OpTheme;->enableTheme(Ljava/util/HashMap;)I

    .line 513
    const v8, 0x7f0603c7

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 514
    .local v9, "accentColor":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v11

    const-string v12, "oneplus_accent_color"

    invoke-static {v10, v12, v9, v11}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 515
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 516
    const-string v10, "#"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 518
    :cond_1
    const-string v10, "persist.sys.theme.accentcolor"

    invoke-static {v10, v9}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v11, "oem_black_mode_accent_color"

    invoke-static {v10, v11, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 520
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0603c6

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "oem_white_mode_accent_color"

    invoke-static {v8, v11, v10}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 521
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v10, "oem_black_mode_accent_color_index"

    invoke-static {v8, v10, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 522
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v10, "oem_white_mode_accent_color_index"

    invoke-static {v8, v10, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 523
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v8, -0x2

    const-string v10, "op_custom_unlock_animation_style"

    invoke-static {v2, v10, v3, v8}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 525
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/oneplus/settings/utils/OPThemeUtils;->KEY_AOD_CLOCK_STYLE:Ljava/lang/String;

    sget v10, Lcom/oneplus/settings/utils/OPThemeUtils;->CLOCK_TYPE_MINIMALISM:I

    invoke-static {v2, v3, v10, v8}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 526
    return-void
.end method

.method public static enableDarkThemes(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .line 432
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "origin_oem_black_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 435
    .local v0, "themeMode":I
    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    .line 436
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 441
    :cond_0
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->disableAllThemes(Landroid/content/Context;)V

    .line 443
    const-string v1, "wallpaper_g_live_gray"

    invoke-static {p0, v1}, Lcom/oneplus/settings/utils/OPThemeUtils;->setDefaultWallpaper(Landroid/content/Context;Ljava/lang/String;)V

    .line 444
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 445
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v4, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 448
    .local v4, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableAospDarkTheme(Landroid/content/Context;Z)V

    .line 449
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 451
    const-string v5, "oneplus_dynamicfont"

    const-string v6, "1"

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 454
    const-string v5, "oneplus_shape"

    const-string v6, "roundedrect"

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    const/4 v5, 0x2

    invoke-static {p0, v5}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 457
    const-string v5, "oneplus_aodnotification"

    const-string v6, "purple"

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    const/4 v5, 0x3

    invoke-static {p0, v5}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 459
    invoke-virtual {v4, v1}, Lcom/oneplus/sdk/utils/OpTheme;->enableTheme(Ljava/util/HashMap;)I

    .line 460
    const v5, 0x7f0603c6

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 461
    .local v6, "accentColor":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v8

    const-string v9, "oneplus_accent_color"

    invoke-static {v7, v9, v6, v8}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 462
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 463
    const-string v7, "#"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 465
    :cond_1
    const-string v7, "persist.sys.theme.accentcolor"

    invoke-static {v7, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0603c7

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "oem_black_mode_accent_color"

    invoke-static {v7, v9, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 467
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v8, "oem_white_mode_accent_color"

    invoke-static {v7, v8, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 468
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v7, "oem_black_mode_accent_color_index"

    invoke-static {v5, v7, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 469
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v7, "oem_white_mode_accent_color_index"

    invoke-static {v5, v7, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 470
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, -0x2

    const-string v7, "op_custom_unlock_animation_style"

    invoke-static {v2, v7, v3, v5}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 472
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/oneplus/settings/utils/OPThemeUtils;->KEY_AOD_CLOCK_STYLE:Ljava/lang/String;

    sget v7, Lcom/oneplus/settings/utils/OPThemeUtils;->CLOCK_TYPE_ANALOG:I

    invoke-static {v2, v3, v7, v5}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 473
    return-void
.end method

.method public static enableLightThemes(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .line 383
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "origin_oem_black_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 386
    .local v0, "themeMode":I
    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 392
    :cond_0
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->disableAllThemes(Landroid/content/Context;)V

    .line 394
    const-string v1, "wallpaper_g_live_blue"

    invoke-static {p0, v1}, Lcom/oneplus/settings/utils/OPThemeUtils;->setDefaultWallpaper(Landroid/content/Context;Ljava/lang/String;)V

    .line 395
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 396
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v3, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 398
    .local v3, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    const-string v4, "oneplus_basiccolor"

    const-string v5, "black"

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    invoke-virtual {v3, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 400
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 401
    const-string v5, "white"

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    invoke-virtual {v3, v1}, Lcom/oneplus/sdk/utils/OpTheme;->enableTheme(Ljava/util/HashMap;)I

    .line 403
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableAospDarkTheme(Landroid/content/Context;Z)V

    .line 404
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 406
    const-string v4, "oneplus_dynamicfont"

    const-string v5, "1"

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    const/4 v4, 0x1

    invoke-static {p0, v4}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 409
    const-string v5, "oneplus_shape"

    const-string v6, "circle"

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    invoke-static {p0, v4}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 411
    invoke-virtual {v3, v1}, Lcom/oneplus/sdk/utils/OpTheme;->enableTheme(Ljava/util/HashMap;)I

    .line 413
    invoke-static {p0, v2}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 414
    const v4, 0x7f0603c7

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 415
    .local v5, "accentColor":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v7

    const-string v8, "oneplus_accent_color"

    invoke-static {v6, v8, v5, v7}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 416
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 417
    const-string v6, "#"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 419
    :cond_1
    const-string v6, "persist.sys.theme.accentcolor"

    invoke-static {v6, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "oem_black_mode_accent_color"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 421
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0603c6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "oem_white_mode_accent_color"

    invoke-static {v4, v7, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 422
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "oem_black_mode_accent_color_index"

    invoke-static {v4, v6, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 423
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "oem_white_mode_accent_color_index"

    invoke-static {v4, v6, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 424
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, -0x2

    const-string v7, "op_custom_unlock_animation_style"

    invoke-static {v4, v7, v2, v6}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 426
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/oneplus/settings/utils/OPThemeUtils;->KEY_AOD_CLOCK_STYLE:Ljava/lang/String;

    sget v7, Lcom/oneplus/settings/utils/OPThemeUtils;->CLOCK_TYPE_DEFAULT:I

    invoke-static {v2, v4, v7, v6}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 427
    return-void
.end method

.method public static enableMclThemes(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .line 529
    const-string v0, "OPCustomFingerAnimVideoPreference"

    const-string v1, "enableMclThemes 1 "

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPThemeUtils;->disableAllThemes(Landroid/content/Context;)V

    .line 532
    const-string v1, "wallpaper_g_live_blue"

    invoke-static {p0, v1}, Lcom/oneplus/settings/utils/OPThemeUtils;->setDefaultWallpaper(Landroid/content/Context;Ljava/lang/String;)V

    .line 533
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 534
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v2, p0}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 538
    .local v2, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentBasicColorMode(Landroid/content/Context;I)V

    .line 539
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableAospDarkTheme(Landroid/content/Context;Z)V

    .line 541
    const-string v4, "oneplus_dynamicfont"

    const-string v5, "1"

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentFont(Landroid/content/Context;I)V

    .line 545
    const-string v4, "oneplus_shape"

    const-string v5, "circle"

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 547
    const-string v3, "oneplus_aodnotification"

    const-string v4, "mcl"

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    const/16 v3, 0xa

    invoke-static {p0, v3}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentHorizonLight(Landroid/content/Context;I)V

    .line 549
    invoke-virtual {v2, v1}, Lcom/oneplus/sdk/utils/OpTheme;->enableTheme(Ljava/util/HashMap;)I

    .line 550
    const-string v4, "oneplus_accentcolor"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    invoke-virtual {v2, v1}, Lcom/oneplus/sdk/utils/OpTheme;->enableTheme(Ljava/util/HashMap;)I

    .line 552
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v5

    const-string v6, "oneplus_accent_color"

    const-string v7, "#FFF19116"

    invoke-static {v4, v6, v7, v5}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 553
    const-string v4, "persist.sys.theme.accentcolor"

    const-string v5, "FFF19116"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 555
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v5

    .line 554
    const-string v6, "aod_clock_style"

    invoke-static {v4, v6, v3, v5}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 557
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "op_custom_unlock_animation_style"

    const/4 v5, 0x3

    const/4 v6, -0x2

    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 559
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, -0x1

    const-string v5, "oem_black_mode_accent_color_index"

    invoke-static {v3, v5, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 560
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "oem_white_mode_accent_color_index"

    invoke-static {v3, v5, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 561
    const-string v3, "enableMclThemes 2 "

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    return-void
.end method

.method public static enableTheme(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "secondCategory"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .line 209
    new-instance v0, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v0, p2}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 210
    .local v0, "opTheme":Lcom/oneplus/sdk/utils/OpTheme;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 213
    .local v1, "mapDisable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "oneplus_basiccolor"

    invoke-static {p0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    const-string v3, "white"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 216
    const-string v3, "black"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    goto :goto_0

    .line 218
    :cond_0
    const-string v2, "oneplus_dynamicfont"

    invoke-static {p0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 220
    const-string v3, "2"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 222
    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    goto :goto_0

    .line 224
    :cond_1
    const-string v2, "oneplus_shape"

    invoke-static {p0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 226
    const-string v3, "squircle"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 228
    const-string v3, "circle"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 230
    const-string v3, "teardrop"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 232
    const-string v3, "roundedrect"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    goto :goto_0

    .line 234
    :cond_2
    const-string v2, "oneplus_aodnotification"

    invoke-static {p0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 236
    const-string v3, "gold"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 238
    const-string v3, "red"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 240
    const-string v3, "purple"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 243
    :cond_3
    :goto_0
    invoke-virtual {v0, v1}, Lcom/oneplus/sdk/utils/OpTheme;->disableTheme(Ljava/util/HashMap;)I

    .line 244
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "oneplus--enableTheme-category:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " secondCategory:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 246
    new-instance v2, Lcom/oneplus/sdk/utils/OpTheme;

    invoke-direct {v2, p2}, Lcom/oneplus/sdk/utils/OpTheme;-><init>(Landroid/content/Context;)V

    .line 247
    .local v2, "opThemeEnable":Lcom/oneplus/sdk/utils/OpTheme;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 248
    .local v3, "mapEnable":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v3, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    invoke-virtual {v2, v3}, Lcom/oneplus/sdk/utils/OpTheme;->enableTheme(Ljava/util/HashMap;)I

    .line 250
    return-void
.end method

.method public static getCurrentBasicColorMode(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 127
    nop

    .line 128
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oem_black_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 127
    return v0
.end method

.method public static getCurrentCustomizationTheme(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 115
    nop

    .line 116
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "op_customization_theme_style"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 115
    return v0
.end method

.method public static getCurrentFont(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 167
    nop

    .line 168
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oem_font_mode"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 167
    return v0
.end method

.method public static getCurrentHorizonLight(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 196
    nop

    .line 197
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "op_custom_horizon_light_animation_style"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 196
    return v0
.end method

.method public static getCurrentHorizonLightByIndex(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "index"    # I

    .line 179
    const-string v0, "blue"

    if-eqz p1, :cond_4

    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    const/16 v1, 0xa

    if-eq p1, v1, :cond_0

    .line 191
    return-object v0

    .line 189
    :cond_0
    const-string v0, "mcl"

    return-object v0

    .line 187
    :cond_1
    const-string v0, "purple"

    return-object v0

    .line 185
    :cond_2
    const-string v0, "gold"

    return-object v0

    .line 183
    :cond_3
    const-string v0, "red"

    return-object v0

    .line 181
    :cond_4
    return-object v0
.end method

.method public static getCurrentShape(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 155
    nop

    .line 156
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oneplus_shape"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 155
    return v0
.end method

.method public static getCurrentShapeByIndex(I)Ljava/lang/String;
    .locals 2
    .param p0, "index"    # I

    .line 140
    const/4 v0, 0x1

    const-string v1, "circle"

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    .line 150
    return-object v1

    .line 148
    :cond_0
    const-string v0, "squircle"

    return-object v0

    .line 146
    :cond_1
    const-string v0, "teardrop"

    return-object v0

    .line 144
    :cond_2
    const-string v0, "roundedrect"

    return-object v0

    .line 142
    :cond_3
    return-object v1
.end method

.method public static isSupportMclTheme()Z
    .locals 2

    .line 565
    sget-object v0, Lcom/oneplus/custom/utils/OpCustomizeSettings$CUSTOM_TYPE;->MCL:Lcom/oneplus/custom/utils/OpCustomizeSettings$CUSTOM_TYPE;

    invoke-static {}, Lcom/oneplus/custom/utils/OpCustomizeSettings;->getCustomType()Lcom/oneplus/custom/utils/OpCustomizeSettings$CUSTOM_TYPE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/oneplus/custom/utils/OpCustomizeSettings$CUSTOM_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static setCurrentBasicColorMode(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "colorMode"    # I

    .line 134
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oem_black_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 136
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "persist.sys.theme.status"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method public static setCurrentCustomizationTheme(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "themeMode"    # I

    .line 122
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "op_customization_theme_style"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 124
    return-void
.end method

.method public static setCurrentFont(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "font"    # I

    .line 174
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oem_font_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 176
    return-void
.end method

.method public static setCurrentHorizonLight(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "horizoLight"    # I

    .line 203
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "op_custom_horizon_light_animation_style"

    const/4 v2, -0x2

    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 205
    return-void
.end method

.method public static setCurrentShape(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shapeMode"    # I

    .line 162
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oneplus_shape"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 164
    return-void
.end method

.method public static setDefaultWallpaper(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "wallpaperName"    # Ljava/lang/String;

    .line 616
    return-void
.end method
