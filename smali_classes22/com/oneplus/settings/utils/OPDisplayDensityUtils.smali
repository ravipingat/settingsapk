.class public Lcom/oneplus/settings/utils/OPDisplayDensityUtils;
.super Ljava/lang/Object;
.source "OPDisplayDensityUtils.java"


# static fields
.field public static final DEFAULT_DENSITY_INDEX:I = 0x1

.field public static final DEFAULT_LARGE_DPI:Ljava/lang/String; = "560"


# instance fields
.field private mCurrentIndex:I

.field private mDefaultDensity:I

.field private mEntries:[Ljava/lang/String;

.field private mValues:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x5

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mValues:[I

    .line 24
    const-string v1, "ro.sf.lcd_density"

    const-string v2, "480"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "defaultDpi":Ljava/lang/String;
    new-instance v2, Lcom/android/settingslib/display/DisplayDensityUtils;

    invoke-direct {v2, p1}, Lcom/android/settingslib/display/DisplayDensityUtils;-><init>(Landroid/content/Context;)V

    .line 26
    .local v2, "density":Lcom/android/settingslib/display/DisplayDensityUtils;
    invoke-virtual {v2}, Lcom/android/settingslib/display/DisplayDensityUtils;->getDefaultDensity()I

    move-result v3

    iput v3, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mDefaultDensity:I

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x2

    const-string v5, "oneplus_screen_resolution_adjust"

    invoke-static {v3, v5, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 28
    .local v3, "value":I
    const-string v5, "560"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v3, :cond_0

    if-ne v3, v4, :cond_1

    .line 30
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0300c9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    iput-object v5, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mValues:[I

    .line 32
    :cond_1
    new-array v0, v0, [Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f121290

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v0, v6

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f12128d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    aput-object v5, v0, v7

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f12128f

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    const/4 v4, 0x3

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f121291

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    const/4 v4, 0x4

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f12128e

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    iput-object v0, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mEntries:[Ljava/lang/String;

    .line 43
    nop

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, -0x2

    const-string v5, "display_density_forced"

    invoke-static {v0, v5, v4}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "currentDpi":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 48
    iput v7, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mCurrentIndex:I

    goto :goto_1

    .line 50
    :cond_2
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v5, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mValues:[I

    array-length v8, v5

    if-ge v4, v8, :cond_4

    .line 51
    aget v5, v5, v4

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 52
    iput v4, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mCurrentIndex:I

    .line 50
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 56
    .end local v4    # "i":I
    :cond_4
    :goto_1
    iget v4, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mCurrentIndex:I

    iget-object v5, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mValues:[I

    array-length v8, v5

    sub-int/2addr v8, v7

    if-lt v4, v8, :cond_5

    .line 57
    array-length v4, v5

    sub-int/2addr v4, v7

    iput v4, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mCurrentIndex:I

    .line 59
    :cond_5
    iget v4, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mCurrentIndex:I

    if-gtz v4, :cond_6

    .line 60
    iput v6, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mCurrentIndex:I

    .line 72
    :cond_6
    return-void

    :array_0
    .array-data 4
        0x17c
        0x1a4
        0x1e0
        0x1f4
        0x21c
    .end array-data
.end method


# virtual methods
.method public getCurrentIndex()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mCurrentIndex:I

    return v0
.end method

.method public getDefaultDensity()I
    .locals 1

    .line 87
    iget v0, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mDefaultDensity:I

    return v0
.end method

.method public getEntries()[Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mEntries:[Ljava/lang/String;

    return-object v0
.end method

.method public getValues()[I
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/oneplus/settings/utils/OPDisplayDensityUtils;->mValues:[I

    return-object v0
.end method
