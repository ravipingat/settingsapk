.class public Lcom/oneplus/settings/widget/OPFooterPreference;
.super Lcom/android/settingslib/widget/FooterPreference;
.source "OPFooterPreference.java"


# static fields
.field public static final KEY_FOOTER:Ljava/lang/String; = "footer_preference"

.field static final ORDER_FOOTER:I = 0x7ffffffe


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/oneplus/settings/widget/OPFooterPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/widget/FooterPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-direct {p0}, Lcom/oneplus/settings/widget/OPFooterPreference;->init()V

    .line 41
    return-void
.end method

.method private init()V
    .locals 1

    .line 49
    const v0, 0x7f080225

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/widget/OPFooterPreference;->setIcon(I)V

    .line 50
    return-void
.end method
