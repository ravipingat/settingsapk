.class public Lcom/oneplus/settings/widget/OPVideoPreference;
.super Landroidx/preference/Preference;
.source "OPVideoPreference.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "OPVideoPreference"


# instance fields
.field private imageView:Landroid/widget/ImageView;

.field private layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

.field mAnimationAvailable:Z
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private mAspectRadio:F

.field private final mContext:Landroid/content/Context;

.field mMediaPlayer:Landroid/media/MediaPlayer;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private mPreviewResource:I

.field private mVideoPath:Landroid/net/Uri;

.field private mVideoPaused:Z

.field private mVideoReady:Z

.field private pos:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 61
    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mAspectRadio:F

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->pos:I

    .line 62
    iput-object p1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mContext:Landroid/content/Context;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/android/settings/R$styleable;->VideoPreference:[I

    invoke-virtual {v1, p2, v2, v0, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 68
    .local v1, "attributes":Landroid/content/res/TypedArray;
    :try_start_0
    invoke-virtual {v1, v0, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 69
    .local v2, "animation":I
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "android.resource"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 71
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 72
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoPath:Landroid/net/Uri;

    .line 73
    iget-object v3, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoPath:Landroid/net/Uri;

    invoke-static {v3, v4}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v3

    iput-object v3, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 74
    iget-object v3, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v3

    if-lez v3, :cond_0

    .line 75
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/oneplus/settings/widget/OPVideoPreference;->setVisible(Z)V

    .line 76
    const v4, 0x7f0d023a

    invoke-virtual {p0, v4}, Lcom/oneplus/settings/widget/OPVideoPreference;->setLayoutResource(I)V

    .line 78
    invoke-virtual {v1, v3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mPreviewResource:I

    .line 81
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v4, Lcom/oneplus/settings/widget/-$$Lambda$OPVideoPreference$ynCil1Vg3ClpXktrurvZlqx29d4;

    invoke-direct {v4, p0}, Lcom/oneplus/settings/widget/-$$Lambda$OPVideoPreference$ynCil1Vg3ClpXktrurvZlqx29d4;-><init>(Lcom/oneplus/settings/widget/OPVideoPreference;)V

    invoke-virtual {v0, v4}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 83
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    sget-object v4, Lcom/oneplus/settings/widget/-$$Lambda$OPVideoPreference$uc3qMTOa2JodIJYV5EhB8pjdWdg;->INSTANCE:Lcom/oneplus/settings/widget/-$$Lambda$OPVideoPreference$uc3qMTOa2JodIJYV5EhB8pjdWdg;

    invoke-virtual {v0, v4}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 84
    iput-boolean v3, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mAnimationAvailable:Z

    .line 85
    invoke-virtual {p0}, Lcom/oneplus/settings/widget/OPVideoPreference;->updateAspectRatio()V

    goto :goto_0

    .line 87
    :cond_0
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/widget/OPVideoPreference;->setVisible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    .end local v2    # "animation":I
    :goto_0
    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "OPVideoPreference"

    const-string v3, "Animation resource not found. Will not show animation."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 93
    nop

    .line 94
    return-void

    .line 92
    :goto_2
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method static synthetic access$000(Lcom/oneplus/settings/widget/OPVideoPreference;)Z
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/widget/OPVideoPreference;

    .line 42
    iget-boolean v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoReady:Z

    return v0
.end method

.method static synthetic access$002(Lcom/oneplus/settings/widget/OPVideoPreference;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/widget/OPVideoPreference;
    .param p1, "x1"    # Z

    .line 42
    iput-boolean p1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoReady:Z

    return p1
.end method

.method static synthetic access$100(Lcom/oneplus/settings/widget/OPVideoPreference;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/widget/OPVideoPreference;

    .line 42
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->imageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oneplus/settings/widget/OPVideoPreference;)Z
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/widget/OPVideoPreference;

    .line 42
    iget-boolean v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoPaused:Z

    return v0
.end method

.method static synthetic lambda$new$1(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p0, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V

    return-void
.end method


# virtual methods
.method public isVideoPaused()Z
    .locals 1

    .line 164
    iget-boolean v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoPaused:Z

    return v0
.end method

.method public synthetic lambda$new$0$OPVideoPreference(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoReady:Z

    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroidx/preference/PreferenceViewHolder;

    .line 98
    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 100
    iget-boolean v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mAnimationAvailable:Z

    if-nez v0, :cond_0

    .line 101
    return-void

    .line 104
    :cond_0
    const v0, 0x7f0a0757

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    .line 105
    .local v0, "video":Landroid/view/TextureView;
    const v1, 0x7f0a0754

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->imageView:Landroid/widget/ImageView;

    .line 106
    const v1, 0x7f0a0750

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/settings/widget/AspectRatioFrameLayout;

    iput-object v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

    .line 109
    iget-object v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->imageView:Landroid/widget/ImageView;

    iget v2, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mPreviewResource:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 110
    iget-object v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

    iget v2, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mAspectRadio:F

    invoke-virtual {v1, v2}, Lcom/android/settings/widget/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 111
    iget-object v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 113
    new-instance v1, Lcom/oneplus/settings/widget/OPVideoPreference$1;

    invoke-direct {v1, p0}, Lcom/oneplus/settings/widget/OPVideoPreference$1;-><init>(Lcom/oneplus/settings/widget/OPVideoPreference;)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 148
    return-void
.end method

.method public onViewInvisible()V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 161
    :cond_0
    return-void
.end method

.method public onViewVisible(Z)V
    .locals 2
    .param p1, "videoPaused"    # Z

    .line 151
    iput-boolean p1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoPaused:Z

    .line 152
    iget-boolean v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoReady:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 155
    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 181
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 182
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 185
    :cond_0
    return-void
.end method

.method public setVideoPaused()V
    .locals 2

    .line 188
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 190
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->imageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

    if-eqz v1, :cond_0

    .line 191
    iget v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mPreviewResource:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 192
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

    iget v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mAspectRadio:F

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 196
    :cond_0
    return-void
.end method

.method public setVideoResume()V
    .locals 1

    .line 173
    iget-boolean v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mVideoPaused:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 176
    :cond_0
    return-void
.end method

.method updateAspectRatio()V
    .locals 2
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/oneplus/settings/widget/OPVideoPreference;->mAspectRadio:F

    .line 170
    return-void
.end method
