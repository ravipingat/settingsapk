.class public Lcom/oneplus/settings/OPThemeSettings;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "OPThemeSettings.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/oneplus/settings/widget/OPThemePresetDialogPreference$OnOPThemePresetDialogClickListener;


# static fields
.field public static final ACTION_THEME_CHANGE:Ljava/lang/String; = "com.oneplus.skin.theme_change"

.field private static final KEY_ONEPLUS_THEME:Ljava/lang/String; = "oneplus_theme_system_key"

.field private static final KEY_ONEPLUS_THEME_CUSTOM:Ljava/lang/String; = "oneplus_theme_custom_key"

.field private static final KEY_PRESET:Ljava/lang/String; = "oneplus_theme_preset"

.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;

.field private static final TAG:Ljava/lang/String; = "OPThemeSettings"

.field private static final THEME_ANDROID_MODE:I = 0x2

.field private static final THEME_DARK_MODE:I = 0x1

.field private static final THEME_LIGHT_MODE:I = 0x0

.field private static final THEME_MCL_MODE:I = 0x3


# instance fields
.field private mAm:Landroid/app/ActivityManager;

.field private mContext:Landroid/content/Context;

.field private mDefaultThemeMode:I

.field private mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

.field private mHandler:Landroid/os/Handler;

.field private mLoadingDialog:Landroidx/appcompat/app/AlertDialog;

.field private mOPThemeIconPreference:Lcom/oneplus/settings/ui/OPThemeIconPreference;

.field private mOPThemePreference:Lcom/oneplus/settings/ui/OPThemePreference;

.field private mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

.field private mWarnDialog:Lcom/oneplus/lib/app/OPAlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 197
    new-instance v0, Lcom/oneplus/settings/OPThemeSettings$1;

    invoke-direct {v0}, Lcom/oneplus/settings/OPThemeSettings$1;-><init>()V

    sput-object v0, Lcom/oneplus/settings/OPThemeSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mDefaultThemeMode:I

    return-void
.end method

.method private applyTheme(I)V
    .locals 5
    .param p1, "mode"    # I

    .line 99
    const-string v0, "oem_special_theme"

    :try_start_0
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mAm:Landroid/app/ActivityManager;

    invoke-static {v1}, Lcom/oneplus/settings/utils/OPApplicationUtils;->killProcess(Landroid/app/ActivityManager;)V

    .line 101
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentCustomizationTheme(Landroid/content/Context;I)V

    .line 102
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    const/4 v1, 0x1

    const-string v2, "status"

    const-string v3, "pre_theme"

    if-ne p1, v1, :cond_0

    .line 105
    :try_start_1
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableDarkThemes(Landroid/content/Context;)V

    .line 106
    const-string v0, "dark"

    invoke-static {v3, v2, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_0
    if-nez p1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableLightThemes(Landroid/content/Context;)V

    .line 109
    const-string v0, "simple"

    invoke-static {v3, v2, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_1
    const/4 v4, 0x3

    if-ne p1, v4, :cond_2

    .line 111
    iget-object v4, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableMclThemes(Landroid/content/Context;)V

    .line 112
    iget-object v4, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 113
    const-string v0, "mcl"

    invoke-static {v3, v2, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableColorfulThemes(Landroid/content/Context;)V

    .line 116
    const-string v0, "color"

    invoke-static {v3, v2, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :goto_0
    const-string v0, "oneplus_theme_preset"

    invoke-static {v0, p1}, Lcom/oneplus/settings/utils/OPUtils;->sendAppTracker(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 128
    goto :goto_1

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v1, "OPThemeSettings"

    const-string v2, "could not persist screen timeout setting"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 129
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :goto_1
    return-void
.end method

.method static synthetic lambda$showWarningDialog$2(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .line 91
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    .line 92
    return-void
.end method

.method private updateThemeSummary(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 133
    const v0, 0x7f120f7c

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    invoke-virtual {v1, v0}, Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;->setSummary(I)V

    goto :goto_0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    const v1, 0x7f120f7d

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;->setSummary(I)V

    .line 145
    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    const v1, 0x7f120f7a

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;->setSummary(I)V

    .line 142
    goto :goto_0

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    const v1, 0x7f120f7b

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;->setSummary(I)V

    .line 139
    goto :goto_0

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    invoke-virtual {v1, v0}, Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;->setSummary(I)V

    .line 136
    nop

    .line 149
    :goto_0
    return-void
.end method


# virtual methods
.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 192
    const-string v0, "OPThemeSettings"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 186
    const/16 v0, 0x270f

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 177
    invoke-static {}, Lcom/oneplus/settings/utils/OPThemeUtils;->isSupportMclTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    const v0, 0x7f1600c1

    return v0

    .line 180
    :cond_0
    const v0, 0x7f1600c0

    return v0
.end method

.method public synthetic lambda$showWarningDialog$0$OPThemeSettings(I)V
    .locals 0
    .param p1, "mode"    # I

    .line 85
    invoke-direct {p0, p1}, Lcom/oneplus/settings/OPThemeSettings;->applyTheme(I)V

    .line 86
    invoke-direct {p0, p1}, Lcom/oneplus/settings/OPThemeSettings;->updateThemeSummary(I)V

    .line 87
    return-void
.end method

.method public synthetic lambda$showWarningDialog$1$OPThemeSettings(ILandroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "mode"    # I
    .param p2, "dialog"    # Landroid/content/DialogInterface;
    .param p3, "which"    # I

    .line 79
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    .line 80
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 81
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    const-class v2, Lcom/oneplus/settings/OPThemeService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 82
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 83
    iget-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/oneplus/settings/-$$Lambda$OPThemeSettings$C6kQtaLLIAgYempaPUo6K7qFVBw;

    invoke-direct {v2, p0, p1}, Lcom/oneplus/settings/-$$Lambda$OPThemeSettings$C6kQtaLLIAgYempaPUo6K7qFVBw;-><init>(Lcom/oneplus/settings/OPThemeSettings;I)V

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 88
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 58
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/oneplus/settings/OPThemeSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    .line 61
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPThemeSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mAm:Landroid/app/ActivityManager;

    .line 62
    const-string v0, "oneplus_theme_preset"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPThemeSettings;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    iput-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mThemeModePreference:Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;

    invoke-virtual {v0, p0}, Lcom/oneplus/settings/widget/OPThemePresetDialogPreference;->setOnOPThemePresetDialogClickListener(Lcom/oneplus/settings/widget/OPThemePresetDialogPreference$OnOPThemePresetDialogClickListener;)V

    .line 64
    const-string v0, "oneplus_theme_custom_key"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPThemeSettings;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPThemeIconPreference;

    iput-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mOPThemeIconPreference:Lcom/oneplus/settings/ui/OPThemeIconPreference;

    .line 65
    invoke-virtual {p0}, Lcom/oneplus/settings/OPThemeSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/Utils;->getFingerprintManagerOrNull(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

    .line 66
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPThemeUtils;->getCurrentCustomizationTheme(Landroid/content/Context;)I

    move-result v0

    .line 67
    .local v0, "themeMode":I
    invoke-direct {p0, v0}, Lcom/oneplus/settings/OPThemeSettings;->updateThemeSummary(I)V

    .line 68
    const-string v1, "oneplus_theme_system_key"

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/OPThemeSettings;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/oneplus/settings/ui/OPThemePreference;

    iput-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mOPThemePreference:Lcom/oneplus/settings/ui/OPThemePreference;

    .line 69
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/oneplus/settings/OPThemeSettings;->mHandler:Landroid/os/Handler;

    .line 70
    return-void
.end method

.method public onDialogClickListener(I)V
    .locals 0
    .param p1, "theme"    # I

    .line 220
    invoke-virtual {p0, p1}, Lcom/oneplus/settings/OPThemeSettings;->showWarningDialog(I)V

    .line 221
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .line 167
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "key":Ljava/lang/String;
    const-string v1, "oneplus_theme_preset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 170
    .local v1, "mode":I
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/OPThemeSettings;->showWarningDialog(I)V

    .line 172
    .end local v1    # "mode":I
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public onPreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 2
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 161
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "key":Ljava/lang/String;
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onPreferenceTreeClick(Landroidx/preference/Preference;)Z

    move-result v1

    return v1
.end method

.method public onResume()V
    .locals 1

    .line 154
    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    .line 155
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mOPThemeIconPreference:Lcom/oneplus/settings/ui/OPThemeIconPreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPThemeIconPreference;->refreshUI()V

    .line 156
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mOPThemePreference:Lcom/oneplus/settings/ui/OPThemePreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPThemePreference;->refreshUI()V

    .line 157
    return-void
.end method

.method public showWarningDialog(I)V
    .locals 3
    .param p1, "mode"    # I

    .line 73
    new-instance v0, Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    .line 74
    invoke-virtual {p0}, Lcom/oneplus/settings/OPThemeSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    const v1, 0x7f120f68

    invoke-virtual {v0, v1}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setTitle(I)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v0

    .line 76
    const v1, 0x7f120f67

    invoke-virtual {v0, v1}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setMessage(I)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/oneplus/settings/-$$Lambda$OPThemeSettings$AsO1ozfnDsTk_lbezIGWSfxxNGo;

    invoke-direct {v1, p0, p1}, Lcom/oneplus/settings/-$$Lambda$OPThemeSettings$AsO1ozfnDsTk_lbezIGWSfxxNGo;-><init>(Lcom/oneplus/settings/OPThemeSettings;I)V

    .line 77
    const v2, 0x7f1201ab

    invoke-virtual {v0, v2, v1}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/oneplus/settings/-$$Lambda$OPThemeSettings$QEX9kLB0ud0tfjfKljyzvaEhAzU;->INSTANCE:Lcom/oneplus/settings/-$$Lambda$OPThemeSettings$QEX9kLB0ud0tfjfKljyzvaEhAzU;

    .line 89
    const v2, 0x7f1203d8

    invoke-virtual {v0, v2, v1}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->create()Lcom/oneplus/lib/app/OPAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mWarnDialog:Lcom/oneplus/lib/app/OPAlertDialog;

    .line 93
    iget-object v0, p0, Lcom/oneplus/settings/OPThemeSettings;->mWarnDialog:Lcom/oneplus/lib/app/OPAlertDialog;

    invoke-virtual {v0}, Lcom/oneplus/lib/app/OPAlertDialog;->show()V

    .line 94
    return-void
.end method
