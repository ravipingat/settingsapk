.class public Lcom/oneplus/settings/laboratory/OPLabPluginModel;
.super Ljava/lang/Object;
.source "OPLabPluginModel.java"


# instance fields
.field private featureIconId:I

.field private featureKey:Ljava/lang/String;

.field private featureSummary:Ljava/lang/String;

.field private featureTitle:Ljava/lang/String;

.field private multiToggleName:[Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private toggleCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public geFeatureIconId()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureIconId:I

    return v0
.end method

.method public getFeatureKey()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureKey:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureSummary()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureSummary:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureTitle()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getMultiToggleName()[Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->multiToggleName:[Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getToggleCount()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->toggleCount:I

    return v0
.end method

.method public setFeatureIconId(I)V
    .locals 0
    .param p1, "featureIconId"    # I

    .line 22
    iput p1, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureIconId:I

    .line 23
    return-void
.end method

.method public setFeatureKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "featureKey"    # Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureKey:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setFeatureSummary(Ljava/lang/String;)V
    .locals 0
    .param p1, "featureSummary"    # Ljava/lang/String;

    .line 58
    iput-object p1, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureSummary:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setFeatureTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "featureTitle"    # Ljava/lang/String;

    .line 52
    iput-object p1, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->featureTitle:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setMultiToggleName([Ljava/lang/String;)V
    .locals 0
    .param p1, "multiToggleName"    # [Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->multiToggleName:[Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->packageName:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setToggleCount(I)V
    .locals 0
    .param p1, "toggleCount"    # I

    .line 40
    iput p1, p0, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->toggleCount:I

    .line 41
    return-void
.end method
