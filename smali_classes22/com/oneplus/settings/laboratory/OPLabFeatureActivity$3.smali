.class Lcom/oneplus/settings/laboratory/OPLabFeatureActivity$3;
.super Lcom/android/settings/search/BaseSearchIndexProvider;
.source "OPLabFeatureActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 242
    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchIndexProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public getRawDataToIndex(Landroid/content/Context;Z)Ljava/util/List;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "enabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchIndexableRaw;",
            ">;"
        }
    .end annotation

    .line 245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/search/SearchIndexableRaw;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 247
    .local v1, "res":Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->access$300(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 248
    .local v2, "pluginData":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;

    invoke-direct {v3, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 249
    .local v3, "component":Landroid/content/ComponentName;
    const-string v4, "oneplus.intent.action.ONEPLUS_LAB_FEATURE"

    .line 250
    .local v4, "intentAction":Ljava/lang/String;
    const v5, 0x7f120d6a

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 251
    .local v5, "screenTitle":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/oneplus/settings/laboratory/OPLabPluginModel;

    .line 252
    .local v7, "m":Lcom/oneplus/settings/laboratory/OPLabPluginModel;
    new-instance v8, Lcom/android/settings/search/SearchIndexableRaw;

    invoke-direct {v8, p1}, Lcom/android/settings/search/SearchIndexableRaw;-><init>(Landroid/content/Context;)V

    .line 253
    .local v8, "data":Lcom/android/settings/search/SearchIndexableRaw;
    invoke-virtual {v7}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->getFeatureKey()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/settings/search/SearchIndexableRaw;->key:Ljava/lang/String;

    .line 254
    invoke-virtual {v7}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->getFeatureTitle()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/settings/search/SearchIndexableRaw;->title:Ljava/lang/String;

    .line 255
    invoke-virtual {v7}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->geFeatureIconId()I

    move-result v9

    iput v9, v8, Lcom/android/settings/search/SearchIndexableRaw;->iconResId:I

    .line 256
    iput-object v5, v8, Lcom/android/settings/search/SearchIndexableRaw;->screenTitle:Ljava/lang/String;

    .line 257
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/settings/search/SearchIndexableRaw;->intentTargetPackage:Ljava/lang/String;

    .line 258
    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/android/settings/search/SearchIndexableRaw;->intentTargetClass:Ljava/lang/String;

    .line 259
    iput-object v4, v8, Lcom/android/settings/search/SearchIndexableRaw;->intentAction:Ljava/lang/String;

    .line 260
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    .end local v7    # "m":Lcom/oneplus/settings/laboratory/OPLabPluginModel;
    .end local v8    # "data":Lcom/android/settings/search/SearchIndexableRaw;
    goto :goto_0

    .line 262
    :cond_0
    return-object v0
.end method
