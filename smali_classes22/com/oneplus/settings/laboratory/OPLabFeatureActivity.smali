.class public Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;
.super Lcom/oneplus/settings/BaseActivity;
.source "OPLabFeatureActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final DATA_LOAD_COMPLETED:I = 0x0

.field private static final ONEPLUS_LAB_FEATURE_ICON_ID:Ljava/lang/String; = "oneplus_lab_feature_icon_id"

.field private static final ONEPLUS_LAB_FEATURE_KEY:Ljava/lang/String; = "oneplus_lab_feature_key"

.field private static final ONEPLUS_LAB_FEATURE_SUMMARY:Ljava/lang/String; = "oneplus_lab_feature_Summary"

.field private static final ONEPLUS_LAB_FEATURE_TITLE:Ljava/lang/String; = "oneplus_lab_feature_title"

.field private static final ONEPLUS_LAB_FEATURE_TOGGLE_COUNT:Ljava/lang/String; = "oneplus_lab_feature_toggle_count"

.field private static final ONEPLUS_LAB_FEATURE_TOGGLE_NAMES:Ljava/lang/String; = "oneplus_lab_feature_toggle_names"

.field private static final ONEPLUS_NFC_SECURITY_MODULE_KEY:Ljava/lang/String; = "oneplus_nfc_security_module_key"

.field private static final PLUGIN_ACTION:Ljava/lang/String; = "com.android.ONEPLUS_LAB_PLUGIN"

.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mPluginData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/oneplus/settings/laboratory/OPLabPluginModel;",
            ">;"
        }
    .end annotation
.end field

.field private mPluginHeadImageView:Landroid/widget/ImageView;

.field private mPluginList:Landroid/widget/ListView;

.field private mPluginListAdapter:Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;

.field private mThreadPool:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 242
    new-instance v0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity$3;

    invoke-direct {v0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity$3;-><init>()V

    sput-object v0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 39
    invoke-direct {p0}, Lcom/oneplus/settings/BaseActivity;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginData:Ljava/util/List;

    .line 54
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 55
    new-instance v0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity$1;-><init>(Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;)Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;

    .line 39
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginListAdapter:Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;

    .line 39
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102(Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;
    .param p1, "x1"    # Ljava/util/List;

    .line 39
    iput-object p1, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginData:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$200(Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;

    .line 39
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .line 39
    invoke-static {p0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->fetchLockedAppListByPackageInfo(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static fetchLockedAppListByPackageInfo(Landroid/content/Context;)Ljava/util/List;
    .locals 32
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/oneplus/settings/laboratory/OPLabPluginModel;",
            ">;"
        }
    .end annotation

    .line 135
    const-string v0, "oneplus_lab_feature"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 136
    .local v2, "curtime":J
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v4

    .line 137
    .local v4, "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 138
    return-object v1

    .line 140
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .local v5, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_11

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageInfo;

    .line 143
    .local v7, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 144
    .local v8, "metaData":Landroid/os/Bundle;
    if-eqz v8, :cond_10

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 145
    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/4 v10, 0x0

    move-object/from16 v11, p0

    invoke-virtual {v11, v9, v10}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v9

    .line 146
    .local v9, "context":Landroid/content/Context;
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 147
    .local v12, "oneplusLabFeature":Ljava/lang/String;
    const-string v13, ";"

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 148
    .local v13, "featureColume":[Ljava/lang/String;
    move v14, v10

    .local v14, "i":I
    :goto_1
    array-length v15, v13

    if-ge v14, v15, :cond_f

    .line 149
    new-instance v15, Lcom/oneplus/settings/laboratory/OPLabPluginModel;

    invoke-direct {v15}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;-><init>()V

    .line 150
    .local v15, "pluginModel":Lcom/oneplus/settings/laboratory/OPLabPluginModel;
    aget-object v1, v13, v14

    const-string v10, ","

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 151
    .local v1, "columeKey":[Ljava/lang/String;
    array-length v10, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .local v10, "columrKeyLength":I
    const/16 v17, 0x2

    .line 154
    .local v17, "featureToggleCount":I
    move-object/from16 v18, v0

    const-string v0, "drawable"

    move-wide/from16 v19, v2

    .end local v2    # "curtime":J
    .local v19, "curtime":J
    const/4 v2, 0x4

    const/16 v21, 0x3

    const-string v3, "string"

    if-le v10, v2, :cond_7

    .line 155
    const/16 v16, 0x0

    :try_start_1
    aget-object v24, v1, v16

    move-object/from16 v25, v24

    .line 156
    .local v25, "featureTitle":Ljava/lang/String;
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v26, v4

    .end local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .local v26, "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    iget-object v4, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v27, v6

    move-object/from16 v6, v25

    .end local v25    # "featureTitle":Ljava/lang/String;
    .local v6, "featureTitle":Ljava/lang/String;
    invoke-virtual {v2, v6, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 157
    .local v2, "featureTitleId":I
    const/4 v4, 0x1

    aget-object v4, v1, v4

    .line 158
    .local v4, "featureSummary":Ljava/lang/String;
    move-object/from16 v23, v6

    .end local v6    # "featureTitle":Ljava/lang/String;
    .local v23, "featureTitle":Ljava/lang/String;
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-object/from16 v25, v8

    .end local v8    # "metaData":Landroid/os/Bundle;
    .local v25, "metaData":Landroid/os/Bundle;
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v4, v3, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 159
    .local v6, "featureSummaryId":I
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    move-object/from16 v28, v4

    const/16 v22, 0x2

    .end local v4    # "featureSummary":Ljava/lang/String;
    .local v28, "featureSummary":Ljava/lang/String;
    aget-object v4, v1, v22

    iget-object v11, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v4, v0, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 160
    .local v0, "featureIcon":I
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    aget-object v8, v1, v21

    iget-object v11, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v8, v3, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 161
    .local v4, "featureKeyId":I
    if-eqz v4, :cond_1

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_1
    aget-object v8, v1, v21

    .line 162
    .local v8, "featureKey":Ljava/lang/String;
    :goto_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 163
    move-object v2, v5

    move-object/from16 v24, v12

    move-object/from16 v29, v13

    move/from16 v30, v14

    const/4 v4, 0x0

    goto/16 :goto_c

    .line 165
    :cond_2
    const/4 v11, 0x4

    aget-object v11, v1, v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    move/from16 v17, v11

    .line 166
    const/4 v11, 0x5

    invoke-static {v1, v11, v10}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    .line 167
    .local v11, "featureToggleNameIds":[Ljava/lang/String;
    move/from16 v22, v4

    .end local v4    # "featureKeyId":I
    .local v22, "featureKeyId":I
    array-length v4, v11

    new-array v4, v4, [Ljava/lang/String;

    .line 168
    .local v4, "featureToggleNames":[Ljava/lang/String;
    const/16 v16, 0x0

    move/from16 v21, v16

    move-object/from16 v24, v12

    move/from16 v12, v21

    .local v12, "j":I
    .local v24, "oneplusLabFeature":Ljava/lang/String;
    :goto_3
    move-object/from16 v29, v13

    .end local v13    # "featureColume":[Ljava/lang/String;
    .local v29, "featureColume":[Ljava/lang/String;
    array-length v13, v11

    if-ge v12, v13, :cond_4

    .line 169
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    move/from16 v30, v14

    .end local v14    # "i":I
    .local v30, "i":I
    aget-object v14, v11, v12

    move-object/from16 v31, v5

    .end local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .local v31, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    iget-object v5, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v14, v3, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 170
    .local v5, "featureToggleNameId":I
    if-eqz v5, :cond_3

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto :goto_4

    :cond_3
    aget-object v13, v11, v12

    :goto_4
    aput-object v13, v4, v12

    .line 168
    .end local v5    # "featureToggleNameId":I
    add-int/lit8 v12, v12, 0x1

    move-object/from16 v13, v29

    move/from16 v14, v30

    move-object/from16 v5, v31

    goto :goto_3

    .end local v30    # "i":I
    .end local v31    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .local v5, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .restart local v14    # "i":I
    :cond_4
    move-object/from16 v31, v5

    move/from16 v30, v14

    .line 172
    .end local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .end local v12    # "j":I
    .end local v14    # "i":I
    .restart local v30    # "i":I
    .restart local v31    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    invoke-virtual {v15, v0}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureIconId(I)V

    .line 173
    if-eqz v2, :cond_5

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    :cond_5
    move-object/from16 v3, v23

    :goto_5
    invoke-virtual {v15, v3}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureTitle(Ljava/lang/String;)V

    .line 174
    if-eqz v6, :cond_6

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    :cond_6
    move-object/from16 v3, v28

    :goto_6
    invoke-virtual {v15, v3}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureSummary(Ljava/lang/String;)V

    .line 175
    invoke-virtual {v15, v4}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setMultiToggleName([Ljava/lang/String;)V

    .line 176
    invoke-virtual {v15, v8}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureKey(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 177
    .end local v0    # "featureIcon":I
    .end local v2    # "featureTitleId":I
    .end local v4    # "featureToggleNames":[Ljava/lang/String;
    .end local v6    # "featureSummaryId":I
    .end local v11    # "featureToggleNameIds":[Ljava/lang/String;
    .end local v22    # "featureKeyId":I
    .end local v23    # "featureTitle":Ljava/lang/String;
    .end local v28    # "featureSummary":Ljava/lang/String;
    move/from16 v0, v17

    const/4 v4, 0x0

    goto/16 :goto_b

    .line 178
    .end local v24    # "oneplusLabFeature":Ljava/lang/String;
    .end local v25    # "metaData":Landroid/os/Bundle;
    .end local v26    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v29    # "featureColume":[Ljava/lang/String;
    .end local v30    # "i":I
    .end local v31    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .local v4, "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .local v8, "metaData":Landroid/os/Bundle;
    .local v12, "oneplusLabFeature":Ljava/lang/String;
    .restart local v13    # "featureColume":[Ljava/lang/String;
    .restart local v14    # "i":I
    :cond_7
    move-object/from16 v26, v4

    move-object/from16 v31, v5

    move-object/from16 v27, v6

    move-object/from16 v25, v8

    move-object/from16 v24, v12

    move-object/from16 v29, v13

    move/from16 v30, v14

    .end local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .end local v8    # "metaData":Landroid/os/Bundle;
    .end local v12    # "oneplusLabFeature":Ljava/lang/String;
    .end local v13    # "featureColume":[Ljava/lang/String;
    .end local v14    # "i":I
    .restart local v24    # "oneplusLabFeature":Ljava/lang/String;
    .restart local v25    # "metaData":Landroid/os/Bundle;
    .restart local v26    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v29    # "featureColume":[Ljava/lang/String;
    .restart local v30    # "i":I
    .restart local v31    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    const-string v2, ""

    const/4 v4, 0x1

    if-le v10, v4, :cond_8

    const/4 v4, 0x0

    :try_start_2
    aget-object v5, v1, v4

    goto :goto_7

    :cond_8
    const/4 v4, 0x0

    move-object v5, v2

    .line 179
    .local v5, "featureTitle":Ljava/lang/String;
    :goto_7
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v5, v3, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 180
    .local v6, "featureTitleId":I
    const/4 v8, 0x2

    if-le v10, v8, :cond_9

    const/4 v2, 0x1

    aget-object v2, v1, v2

    .line 181
    .local v2, "featureSummary":Ljava/lang/String;
    :cond_9
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget-object v11, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v2, v3, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 182
    .local v8, "featureSummaryId":I
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const/4 v12, 0x2

    aget-object v12, v1, v12

    iget-object v13, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v12, v0, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 184
    .restart local v0    # "featureIcon":I
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    aget-object v12, v1, v21

    iget-object v13, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v12, v3, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 185
    .local v3, "featureKeyId":I
    if-eqz v3, :cond_a

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_8

    :cond_a
    aget-object v11, v1, v21

    .line 186
    .local v11, "featureKey":Ljava/lang/String;
    :goto_8
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 187
    move-object/from16 v2, v31

    goto :goto_c

    .line 189
    :cond_b
    invoke-virtual {v15, v0}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureIconId(I)V

    .line 190
    if-eqz v6, :cond_c

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_9

    :cond_c
    move-object v12, v5

    :goto_9
    invoke-virtual {v15, v12}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureTitle(Ljava/lang/String;)V

    .line 191
    if-eqz v8, :cond_d

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_a

    :cond_d
    move-object v12, v2

    :goto_a
    invoke-virtual {v15, v12}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureSummary(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v15, v11}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureKey(Ljava/lang/String;)V

    move-object v8, v11

    move/from16 v0, v17

    .line 194
    .end local v2    # "featureSummary":Ljava/lang/String;
    .end local v3    # "featureKeyId":I
    .end local v5    # "featureTitle":Ljava/lang/String;
    .end local v6    # "featureTitleId":I
    .end local v11    # "featureKey":Ljava/lang/String;
    .end local v17    # "featureToggleCount":I
    .local v0, "featureToggleCount":I
    .local v8, "featureKey":Ljava/lang/String;
    :goto_b
    invoke-static {v9}, Lcom/oneplus/settings/utils/OPUtils;->isSurportSimNfc(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_e

    const-string v2, "oneplus_nfc_security_module_key"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 195
    move-object/from16 v2, v31

    goto :goto_c

    .line 197
    :cond_e
    invoke-virtual {v15, v0}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setToggleCount(I)V

    .line 198
    move-object/from16 v2, v31

    .end local v31    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .local v2, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 148
    .end local v0    # "featureToggleCount":I
    .end local v1    # "columeKey":[Ljava/lang/String;
    .end local v8    # "featureKey":Ljava/lang/String;
    .end local v10    # "columrKeyLength":I
    .end local v15    # "pluginModel":Lcom/oneplus/settings/laboratory/OPLabPluginModel;
    :goto_c
    add-int/lit8 v14, v30, 0x1

    move-object/from16 v11, p0

    move-object v5, v2

    move v10, v4

    move-object/from16 v0, v18

    move-wide/from16 v2, v19

    move-object/from16 v12, v24

    move-object/from16 v8, v25

    move-object/from16 v4, v26

    move-object/from16 v6, v27

    move-object/from16 v13, v29

    const/4 v1, 0x0

    .end local v30    # "i":I
    .restart local v14    # "i":I
    goto/16 :goto_1

    .end local v19    # "curtime":J
    .end local v24    # "oneplusLabFeature":Ljava/lang/String;
    .end local v25    # "metaData":Landroid/os/Bundle;
    .end local v26    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v29    # "featureColume":[Ljava/lang/String;
    .local v2, "curtime":J
    .restart local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .local v5, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .local v8, "metaData":Landroid/os/Bundle;
    .restart local v12    # "oneplusLabFeature":Ljava/lang/String;
    .restart local v13    # "featureColume":[Ljava/lang/String;
    :cond_f
    move-object/from16 v18, v0

    move-wide/from16 v19, v2

    move-object/from16 v26, v4

    move-object v2, v5

    move-object/from16 v27, v6

    move-object/from16 v25, v8

    move-object/from16 v24, v12

    move-object/from16 v29, v13

    move/from16 v30, v14

    .end local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .end local v8    # "metaData":Landroid/os/Bundle;
    .end local v12    # "oneplusLabFeature":Ljava/lang/String;
    .end local v13    # "featureColume":[Ljava/lang/String;
    .end local v14    # "i":I
    .local v2, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .restart local v19    # "curtime":J
    .restart local v24    # "oneplusLabFeature":Ljava/lang/String;
    .restart local v25    # "metaData":Landroid/os/Bundle;
    .restart local v26    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v29    # "featureColume":[Ljava/lang/String;
    .restart local v30    # "i":I
    goto :goto_d

    .line 144
    .end local v9    # "context":Landroid/content/Context;
    .end local v19    # "curtime":J
    .end local v24    # "oneplusLabFeature":Ljava/lang/String;
    .end local v25    # "metaData":Landroid/os/Bundle;
    .end local v26    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v29    # "featureColume":[Ljava/lang/String;
    .end local v30    # "i":I
    .local v2, "curtime":J
    .restart local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .restart local v8    # "metaData":Landroid/os/Bundle;
    :cond_10
    move-object/from16 v18, v0

    move-wide/from16 v19, v2

    move-object/from16 v26, v4

    move-object v2, v5

    move-object/from16 v27, v6

    move-object/from16 v25, v8

    .line 201
    .end local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .end local v7    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v8    # "metaData":Landroid/os/Bundle;
    .local v2, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .restart local v19    # "curtime":J
    .restart local v26    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :goto_d
    move-object v5, v2

    move-object/from16 v0, v18

    move-wide/from16 v2, v19

    move-object/from16 v4, v26

    move-object/from16 v6, v27

    const/4 v1, 0x0

    goto/16 :goto_0

    .line 202
    .end local v19    # "curtime":J
    .end local v26    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .local v2, "curtime":J
    .restart local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    :cond_11
    move-wide/from16 v19, v2

    move-object v2, v5

    .end local v5    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .local v2, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .restart local v19    # "curtime":J
    return-object v2

    .line 204
    .end local v2    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/laboratory/OPLabPluginModel;>;"
    .end local v4    # "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v19    # "curtime":J
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PluginDemo"

    const-string v2, "some unknown error happened."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 208
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    return-object v1
.end method

.method private gotoDetailPage(Lcom/oneplus/settings/laboratory/OPLabPluginModel;)V
    .locals 3
    .param p1, "labPluginModel"    # Lcom/oneplus/settings/laboratory/OPLabPluginModel;

    .line 123
    new-instance v0, Landroid/content/Intent;

    const-string v1, "oneplus.intent.action.ONEPLUS_LAB_FEATURE_DETAILS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 124
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->getToggleCount()I

    move-result v1

    const-string v2, "oneplus_lab_feature_toggle_count"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 125
    invoke-virtual {p1}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->getMultiToggleName()[Ljava/lang/String;

    move-result-object v1

    const-string v2, "oneplus_lab_feature_toggle_names"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    invoke-virtual {p1}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->getFeatureTitle()Ljava/lang/String;

    move-result-object v1

    const-string v2, "oneplus_lab_feature_title"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {p1}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->getFeatureSummary()Ljava/lang/String;

    move-result-object v1

    const-string v2, "oneplus_lab_feature_Summary"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    invoke-virtual {p1}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->getFeatureKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "oneplus_lab_feature_key"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    invoke-virtual {p1}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->geFeatureIconId()I

    move-result v1

    const-string v2, "oneplus_lab_feature_icon_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 130
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->startActivity(Landroid/content/Intent;)V

    .line 131
    return-void
.end method

.method private initData(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .line 106
    new-instance v0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity$2;-><init>(Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;Landroid/os/Handler;)V

    .line 113
    .local v0, "initRunnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 114
    return-void
.end method


# virtual methods
.method public fetchLockedAppListByActivityInfo()V
    .locals 11

    .line 213
    :try_start_0
    invoke-virtual {p0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.ONEPLUS_LAB_PLUGIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 214
    .local v0, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    return-void

    .line 217
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 218
    .local v2, "reInfo":Landroid/content/pm/ResolveInfo;
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "oneplus_lab_package_name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 219
    .local v3, "oneplusLabPackageName":Ljava/lang/String;
    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v5, "oneplus_lab_feature_title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 220
    .local v4, "oneplusLabFeatureTitle":Ljava/lang/String;
    iget-object v5, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v6, "oneplus_lab_feature_summary"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 221
    .local v5, "oneplusLabFeatureSummary":Ljava/lang/String;
    iget-object v6, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v7, "oneplus_lab_feature_toggle_key"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 222
    .local v6, "oneplusLabFeatureToggleKey":Ljava/lang/String;
    iget-object v7, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v8, "oneplus_lab_feature_icon_id"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 225
    .local v7, "oneplusLabFeatureIconId":I
    iget-object v8, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v8, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 226
    .local v8, "uid":I
    new-instance v9, Lcom/oneplus/settings/laboratory/OPLabPluginModel;

    invoke-direct {v9}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;-><init>()V

    .line 227
    .local v9, "pluginModel":Lcom/oneplus/settings/laboratory/OPLabPluginModel;
    invoke-virtual {v9, v3}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setPackageName(Ljava/lang/String;)V

    .line 228
    invoke-virtual {v9, v4}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureTitle(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v9, v5}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureSummary(Ljava/lang/String;)V

    .line 230
    invoke-virtual {v9, v6}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureKey(Ljava/lang/String;)V

    .line 231
    invoke-virtual {v9, v7}, Lcom/oneplus/settings/laboratory/OPLabPluginModel;->setFeatureIconId(I)V

    .line 232
    iget-object v10, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginData:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    nop

    .end local v2    # "reInfo":Landroid/content/pm/ResolveInfo;
    .end local v3    # "oneplusLabPackageName":Ljava/lang/String;
    .end local v4    # "oneplusLabFeatureTitle":Ljava/lang/String;
    .end local v5    # "oneplusLabFeatureSummary":Ljava/lang/String;
    .end local v6    # "oneplusLabFeatureToggleKey":Ljava/lang/String;
    .end local v7    # "oneplusLabFeatureIconId":I
    .end local v8    # "uid":I
    .end local v9    # "pluginModel":Lcom/oneplus/settings/laboratory/OPLabPluginModel;
    goto :goto_0

    .line 238
    .end local v0    # "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_1
    goto :goto_1

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PluginDemo"

    const-string v2, "some unknown error happened."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 239
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 76
    invoke-super {p0, p1}, Lcom/oneplus/settings/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    iput-object p0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mContext:Landroid/content/Context;

    .line 78
    const v0, 0x7f0d01c6

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->setContentView(I)V

    .line 79
    const v0, 0x7f0a04b2

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginHeadImageView:Landroid/widget/ImageView;

    .line 80
    const v0, 0x7f0a04b3

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginList:Landroid/widget/ListView;

    .line 81
    invoke-virtual {p0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPUtils;->isBlackModeOn(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginHeadImageView:Landroid/widget/ImageView;

    const v1, 0x7f080428

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginHeadImageView:Landroid/widget/ImageView;

    const v1, 0x7f080429

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    :goto_0
    new-instance v0, Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;

    iget-object v1, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginData:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginListAdapter:Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;

    .line 87
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginListAdapter:Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 88
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {p0, v0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->initData(Landroid/os/Handler;)V

    .line 90
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 119
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->mPluginListAdapter:Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;

    invoke-virtual {v0, p3}, Lcom/oneplus/settings/laboratory/OPLabPluginListAdapter;->getItem(I)Lcom/oneplus/settings/laboratory/OPLabPluginModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->gotoDetailPage(Lcom/oneplus/settings/laboratory/OPLabPluginModel;)V

    .line 120
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 94
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-eq v0, v1, :cond_0

    .line 102
    invoke-super {p0, p1}, Lcom/oneplus/settings/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/oneplus/settings/laboratory/OPLabFeatureActivity;->finish()V

    .line 97
    const/4 v0, 0x1

    return v0
.end method
