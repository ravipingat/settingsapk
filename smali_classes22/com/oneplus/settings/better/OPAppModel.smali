.class public Lcom/oneplus/settings/better/OPAppModel;
.super Ljava/lang/Object;
.source "OPAppModel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/oneplus/settings/better/OPAppModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private appIcon:Landroid/graphics/drawable/Drawable;

.field private appLabel:Ljava/lang/String;

.field private editMode:Z

.field private isGameAPP:Z

.field private isSelected:Z

.field private label:Ljava/lang/String;

.field private lockMode:I

.field private pkgName:Ljava/lang/String;

.field private shortCutIcon:Landroid/graphics/drawable/Drawable;

.field private shortCutId:Ljava/lang/String;

.field private type:I

.field private uid:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 160
    new-instance v0, Lcom/oneplus/settings/better/OPAppModel$1;

    invoke-direct {v0}, Lcom/oneplus/settings/better/OPAppModel$1;-><init>()V

    sput-object v0, Lcom/oneplus/settings/better/OPAppModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    invoke-virtual {p0, p1}, Lcom/oneplus/settings/better/OPAppModel;->readFromParcel(Landroid/os/Parcel;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "shortCutId"    # Ljava/lang/String;
    .param p4, "uid"    # I
    .param p5, "isSelected"    # Z

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/oneplus/settings/better/OPAppModel;->pkgName:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/oneplus/settings/better/OPAppModel;->label:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/oneplus/settings/better/OPAppModel;->shortCutId:Ljava/lang/String;

    .line 26
    iput p4, p0, Lcom/oneplus/settings/better/OPAppModel;->uid:I

    .line 27
    iput-boolean p5, p0, Lcom/oneplus/settings/better/OPAppModel;->isSelected:Z

    .line 28
    return-void
.end method

.method public static getCreator()Landroid/os/Parcelable$Creator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/Parcelable$Creator<",
            "Lcom/oneplus/settings/better/OPAppModel;",
            ">;"
        }
    .end annotation

    .line 127
    sget-object v0, Lcom/oneplus/settings/better/OPAppModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 137
    const/4 v0, 0x0

    return v0
.end method

.method public getAppIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->appIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppLabel()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->appLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getLockMode()I
    .locals 1

    .line 119
    iget v0, p0, Lcom/oneplus/settings/better/OPAppModel;->lockMode:I

    return v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getShortCutIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->shortCutIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getShortCutId()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->shortCutId:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/oneplus/settings/better/OPAppModel;->type:I

    return v0
.end method

.method public getUid()I
    .locals 1

    .line 111
    iget v0, p0, Lcom/oneplus/settings/better/OPAppModel;->uid:I

    return v0
.end method

.method public isEditMode()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/oneplus/settings/better/OPAppModel;->editMode:Z

    return v0
.end method

.method public isGameAPP()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/oneplus/settings/better/OPAppModel;->isGameAPP:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .line 47
    iget-boolean v0, p0, Lcom/oneplus/settings/better/OPAppModel;->isSelected:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->pkgName:Ljava/lang/String;

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->pkgName:Ljava/lang/String;

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/oneplus/settings/better/OPAppModel;->uid:I

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/oneplus/settings/better/OPAppModel;->lockMode:I

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/oneplus/settings/better/OPAppModel;->isSelected:Z

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_1

    move v1, v2

    :cond_1
    iput-boolean v1, p0, Lcom/oneplus/settings/better/OPAppModel;->isGameAPP:Z

    .line 158
    return-void
.end method

.method public setAppIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "appIcon"    # Landroid/graphics/drawable/Drawable;

    .line 99
    iput-object p1, p0, Lcom/oneplus/settings/better/OPAppModel;->appIcon:Landroid/graphics/drawable/Drawable;

    .line 100
    return-void
.end method

.method public setAppLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "appLabel"    # Ljava/lang/String;

    .line 91
    iput-object p1, p0, Lcom/oneplus/settings/better/OPAppModel;->appLabel:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setEditMode(Z)V
    .locals 0
    .param p1, "editMode"    # Z

    .line 35
    iput-boolean p1, p0, Lcom/oneplus/settings/better/OPAppModel;->editMode:Z

    .line 36
    return-void
.end method

.method public setGameAPP(Z)V
    .locals 0
    .param p1, "isGameAPP"    # Z

    .line 43
    iput-boolean p1, p0, Lcom/oneplus/settings/better/OPAppModel;->isGameAPP:Z

    .line 44
    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .line 83
    iput-object p1, p0, Lcom/oneplus/settings/better/OPAppModel;->label:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setLockMode(I)V
    .locals 0
    .param p1, "lockMode"    # I

    .line 123
    iput p1, p0, Lcom/oneplus/settings/better/OPAppModel;->lockMode:I

    .line 124
    return-void
.end method

.method public setPkgName(Ljava/lang/String;)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 75
    iput-object p1, p0, Lcom/oneplus/settings/better/OPAppModel;->pkgName:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .line 51
    iput-boolean p1, p0, Lcom/oneplus/settings/better/OPAppModel;->isSelected:Z

    .line 52
    return-void
.end method

.method public setShortCutIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "shortCutIcon"    # Landroid/graphics/drawable/Drawable;

    .line 107
    iput-object p1, p0, Lcom/oneplus/settings/better/OPAppModel;->shortCutIcon:Landroid/graphics/drawable/Drawable;

    .line 108
    return-void
.end method

.method public setShortCutId(Ljava/lang/String;)V
    .locals 0
    .param p1, "shortCutId"    # Ljava/lang/String;

    .line 59
    iput-object p1, p0, Lcom/oneplus/settings/better/OPAppModel;->shortCutId:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .line 67
    iput p1, p0, Lcom/oneplus/settings/better/OPAppModel;->type:I

    .line 68
    return-void
.end method

.method public setUid(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 115
    iput p1, p0, Lcom/oneplus/settings/better/OPAppModel;->uid:I

    .line 116
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 143
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->pkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/oneplus/settings/better/OPAppModel;->label:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget v0, p0, Lcom/oneplus/settings/better/OPAppModel;->uid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 146
    iget v0, p0, Lcom/oneplus/settings/better/OPAppModel;->lockMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 147
    iget-boolean v0, p0, Lcom/oneplus/settings/better/OPAppModel;->isSelected:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 148
    iget-boolean v0, p0, Lcom/oneplus/settings/better/OPAppModel;->isGameAPP:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    return-void
.end method
