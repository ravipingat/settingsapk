.class public Lcom/oneplus/settings/better/OPSuwScreenColorMode;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "OPSuwScreenColorMode.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;
.implements Lcom/oneplus/settings/ui/OPSuwSeekBarPreference$OPColorModeSeekBarChangeListener;
.implements Lcom/android/settings/search/Indexable;


# static fields
.field private static final DCI_P3_PATH:Ljava/lang/String; = "/sys/devices/virtual/graphics/fb0/DCI_P3"

.field public static final DEFAULT_COLOR_PROGRESS:I

.field private static final KEY_SCREEN_COLOR_MODE_ADAPTIVE_MODEL_SETTINGS:Ljava/lang/String; = "screen_color_mode_adaptive_model_settings"

.field private static final KEY_SCREEN_COLOR_MODE_ADAPTIVE_MODEL_SETTINGS_DIVIDER:Ljava/lang/String; = "screen_color_mode_adaptive_model_settings_divider"

.field private static final KEY_SCREEN_COLOR_MODE_ADVANCED_SETTINGS:Ljava/lang/String; = "screen_color_mode_advanced_settings"

.field private static final KEY_SCREEN_COLOR_MODE_ADVANCED_SETTINGS_DISPLAY_P3:Ljava/lang/String; = "screen_color_mode_advanced_settings_display_p3"

.field private static final KEY_SCREEN_COLOR_MODE_ADVANCED_SETTINGS_NTSC:Ljava/lang/String; = "screen_color_mode_advanced_settings_ntsc"

.field private static final KEY_SCREEN_COLOR_MODE_ADVANCED_SETTINGS_SRGB:Ljava/lang/String; = "screen_color_mode_advanced_settings_srgb"

.field private static final KEY_SCREEN_COLOR_MODE_AUTO_SETTINGS:Ljava/lang/String; = "screen_color_mode_auto_settings"

.field private static final KEY_SCREEN_COLOR_MODE_AUTO_SETTINGS_DIVIDER:Ljava/lang/String; = "screen_color_mode_auto_settings_divider"

.field private static final KEY_SCREEN_COLOR_MODE_BASIC_SETTINGS:Ljava/lang/String; = "screen_color_mode_basic_settings"

.field private static final KEY_SCREEN_COLOR_MODE_DCI_P3_SETTINGS:Ljava/lang/String; = "screen_color_mode_dci_p3_settings"

.field private static final KEY_SCREEN_COLOR_MODE_DEFAULT_SETTINGS:Ljava/lang/String; = "screen_color_mode_default_settings"

.field private static final KEY_SCREEN_COLOR_MODE_DEFINED_SETTINGS:Ljava/lang/String; = "screen_color_mode_defined_settings"

.field private static final KEY_SCREEN_COLOR_MODE_SEEKBAR:Ljava/lang/String; = "screen_color_mode_seekbar"

.field private static final KEY_SCREEN_COLOR_MODE_SOFT_SETTINGS:Ljava/lang/String; = "screen_color_mode_soft_settings"

.field private static final KEY_SCREEN_COLOR_MODE_TITLE_SUMMARY:Ljava/lang/String; = "oneplus_screen_color_mode_title_summary"

.field public static final NIGHT_MODE_ENABLED:Ljava/lang/String; = "night_mode_enabled"

.field private static final OPEN_VALUE:Ljava/lang/String; = "mode = 1"

.field private static final RGB_PATH:Ljava/lang/String; = "/sys/devices/virtual/graphics/fb0/SRGB"

.field private static final SCREEN_COLOR_MODE_ADAPTIVE_MODEL_SETTINGS_VALUE:I = 0x5

.field private static final SCREEN_COLOR_MODE_AUTO_SETTINGS_VALUE:I = 0xa

.field private static final SCREEN_COLOR_MODE_BASIC_SETTINGS_VALUE:I = 0x2

.field private static final SCREEN_COLOR_MODE_DCI_P3_SETTINGS_VALUE:I = 0x4

.field private static final SCREEN_COLOR_MODE_DEFAULT_SETTINGS_VALUE:I = 0x1

.field private static final SCREEN_COLOR_MODE_DEFINED_SETTINGS_VALUE:I = 0x3

.field private static final SCREEN_COLOR_MODE_SOFT_SETTINGS_VALUE:I = 0x6

.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field private static final TAG:Ljava/lang/String; = "OPSuwScreenColorMode"

.field private static isSupportAdaptive:Z

.field private static isSupportDcip3:Z

.field private static isSupportMMDisplayColor:Z

.field private static isSupportSoft:Z


# instance fields
.field private isSupportReadingMode:Z

.field private mContext:Landroid/content/Context;

.field private mDeviceProvision:Z

.field private mHandler:Landroid/os/Handler;

.field private mOPScreenColorModeSummary:Lcom/oneplus/settings/ui/OPScreenColorModeSummary;

.field private mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeContentObserver:Landroid/database/ContentObserver;

.field private mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

.field private mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

.field private mScreenColorModeValue:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekBarpreference:Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 85
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportMMDisplayColorScreenMode()Z

    move-result v0

    sput-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportMMDisplayColor:Z

    .line 86
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportMMDisplayColorScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    goto :goto_0

    :cond_0
    const/16 v0, 0x2b

    :goto_0
    sput v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->DEFAULT_COLOR_PROGRESS:I

    .line 543
    new-instance v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode$3;

    invoke-direct {v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode$3;-><init>()V

    sput-object v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 35
    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mDeviceProvision:Z

    .line 87
    new-instance v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode$1;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode$1;-><init>(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)V

    iput-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mHandler:Landroid/os/Handler;

    .line 175
    new-instance v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode$2;-><init>(Lcom/oneplus/settings/better/OPSuwScreenColorMode;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeContentObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mSeekBarpreference:Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/oneplus/settings/ui/OPScreenColorModeSummary;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mOPScreenColorModeSummary:Lcom/oneplus/settings/ui/OPScreenColorModeSummary;

    return-object v0
.end method

.method static synthetic access$1200()Z
    .locals 1

    .line 35
    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportDcip3:Z

    return v0
.end method

.method static synthetic access$1300()Z
    .locals 1

    .line 35
    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportAdaptive:Z

    return v0
.end method

.method static synthetic access$1400()Z
    .locals 1

    .line 35
    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportSoft:Z

    return v0
.end method

.method static synthetic access$1500()Z
    .locals 1

    .line 35
    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportMMDisplayColor:Z

    return v0
.end method

.method static synthetic access$200(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/RadioButtonPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/RadioButtonPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/RadioButtonPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    return-object v0
.end method

.method static synthetic access$500(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/RadioButtonPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    return-object v0
.end method

.method static synthetic access$600(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/RadioButtonPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    return-object v0
.end method

.method static synthetic access$700(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/RadioButtonPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    return-object v0
.end method

.method static synthetic access$800(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/RadioButtonPreference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    return-object v0
.end method

.method static synthetic access$900(Lcom/oneplus/settings/better/OPSuwScreenColorMode;)Lcom/android/settings/ui/OPSuwPreferenceCategory;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPSuwScreenColorMode;

    .line 35
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    return-object v0
.end method

.method private updateAdvancedSettingsRadioButtons()V
    .locals 5

    .line 270
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "screen_color_mode_advanced_settings_value"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 271
    .local v0, "value":I
    iget-object v2, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

    const/4 v3, 0x1

    if-nez v0, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    move v4, v1

    :goto_0
    invoke-virtual {v2, v4}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 272
    iget-object v2, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne v0, v3, :cond_1

    move v4, v3

    goto :goto_1

    :cond_1
    move v4, v1

    :goto_1
    invoke-virtual {v2, v4}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 273
    iget-object v2, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    move v1, v3

    :cond_2
    invoke-virtual {v2, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 274
    return-void
.end method

.method private updateRadioButtons(I)V
    .locals 4
    .param p1, "value"    # I

    .line 284
    const-string v0, "screen_color_mode_seekbar"

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v1, p1, :cond_0

    .line 285
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 286
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 287
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 288
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 289
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 290
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 291
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 292
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 293
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_6

    .line 294
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 296
    :cond_0
    const/4 v3, 0x2

    if-ne v3, p1, :cond_1

    .line 297
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 298
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 299
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 300
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 301
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 302
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 303
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 304
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 305
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_6

    .line 306
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 308
    :cond_1
    const/4 v3, 0x3

    if-ne v3, p1, :cond_2

    .line 309
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 310
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 311
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 312
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 313
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 314
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 315
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mSeekBarpreference:Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->addPreference(Landroidx/preference/Preference;)Z

    .line 316
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_6

    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportMMDisplayColor:Z

    if-eqz v0, :cond_6

    .line 317
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->addPreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 319
    :cond_2
    const/4 v3, 0x4

    if-ne v3, p1, :cond_3

    .line 320
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 321
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 322
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 323
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 324
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 325
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 326
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 327
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 328
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_6

    .line 329
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 331
    :cond_3
    const/4 v3, 0x5

    if-ne v3, p1, :cond_4

    .line 332
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 333
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 334
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 335
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 336
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 337
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 338
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 339
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 340
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_6

    .line 341
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    .line 343
    :cond_4
    const/4 v3, 0x6

    if-ne v3, p1, :cond_5

    .line 344
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 345
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 346
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 347
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 348
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 349
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 350
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 351
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 352
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_6

    .line 353
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    .line 355
    :cond_5
    const/16 v3, 0xa

    if-ne v3, p1, :cond_6

    .line 356
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 357
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 358
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 359
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 360
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 361
    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 362
    iget-object v2, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v2, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 363
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 364
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_6

    .line 365
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    .line 368
    :cond_6
    :goto_0
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 508
    const/16 v0, 0x270f

    return v0
.end method

.method public getScreenColorModeSettingsValue()I
    .locals 4

    .line 528
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mContext:Landroid/content/Context;

    .line 529
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 528
    const-string v1, "screen_color_mode_settings_value"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 530
    .local v0, "value":I
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "icicle"    # Landroid/os/Bundle;

    .line 105
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const v0, 0x7f1600bd

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->addPreferencesFromResource(I)V

    .line 107
    sget-object v0, Lcom/oneplus/settings/SettingsBaseApplication;->mApplication:Landroid/app/Application;

    iput-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mContext:Landroid/content/Context;

    .line 108
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "oem.read_mode.support"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportReadingMode:Z

    .line 109
    const-string v0, "screen_color_mode_default_settings"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    .line 110
    const-string v0, "screen_color_mode_basic_settings"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    .line 111
    const-string v1, "screen_color_mode_defined_settings"

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    .line 112
    const-string v1, "screen_color_mode_dci_p3_settings"

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v2, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    .line 113
    const-string v2, "screen_color_mode_adaptive_model_settings"

    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    .line 114
    const-string v3, "screen_color_mode_soft_settings"

    invoke-virtual {p0, v3}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    .line 115
    const-string v4, "screen_color_mode_auto_settings"

    invoke-virtual {p0, v4}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    .line 116
    const-string v5, "screen_color_mode_advanced_settings_ntsc"

    invoke-virtual {p0, v5}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

    .line 117
    const-string v5, "screen_color_mode_advanced_settings_srgb"

    invoke-virtual {p0, v5}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

    .line 118
    const-string v5, "screen_color_mode_advanced_settings_display_p3"

    invoke-virtual {p0, v5}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

    .line 119
    const-string v5, "screen_color_mode_advanced_settings"

    invoke-virtual {p0, v5}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/android/settings/ui/OPSuwPreferenceCategory;

    iput-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    .line 120
    const-string v5, "oneplus_screen_color_mode_title_summary"

    invoke-virtual {p0, v5}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/oneplus/settings/ui/OPScreenColorModeSummary;

    iput-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mOPScreenColorModeSummary:Lcom/oneplus/settings/ui/OPScreenColorModeSummary;

    .line 121
    const-string v5, "screen_color_mode_seekbar"

    invoke-virtual {p0, v5}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;

    iput-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mSeekBarpreference:Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;

    .line 122
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mSeekBarpreference:Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;

    invoke-virtual {v5, p0}, Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;->setOPColorModeSeekBarChangeListener(Lcom/oneplus/settings/ui/OPSuwSeekBarPreference$OPColorModeSeekBarChangeListener;)V

    .line 123
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 124
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 125
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 126
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 127
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 128
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 129
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 130
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 131
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 132
    iget-object v5, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v5, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 133
    sget-boolean v5, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportMMDisplayColor:Z

    const-string v6, "oneplus_screen_color_mode_basic_divider"

    const-string v7, "screen_color_mode_soft_settings_divider"

    const-string v8, "screen_color_mode_auto_settings_divider"

    if-eqz v5, :cond_0

    .line 134
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    const v5, 0x7f12126d

    invoke-virtual {v4, v5}, Lcom/android/settings/ui/RadioButtonPreference;->setTitle(I)V

    .line 135
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    const v5, 0x7f12126b

    invoke-virtual {v4, v5}, Lcom/android/settings/ui/RadioButtonPreference;->setTitle(I)V

    .line 136
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 137
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 138
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 139
    const-string v0, "screen_color_mode_adaptive_model_settings_divider"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 140
    invoke-virtual {p0, v7}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 141
    invoke-virtual {p0, v6}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 142
    const-string v0, "screen_color_mode_advanced_settings_divider"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 143
    invoke-virtual {p0, v8}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    goto :goto_0

    .line 145
    :cond_0
    invoke-virtual {p0, v4}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 146
    invoke-virtual {p0, v8}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 147
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v4}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    .line 149
    :goto_0
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mOPScreenColorModeSummary:Lcom/oneplus/settings/ui/OPScreenColorModeSummary;

    invoke-virtual {v0, v4}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    .line 150
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v4, "oem.dcip3.support"

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportDcip3:Z

    .line 151
    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportDcip3:Z

    if-nez v0, :cond_1

    .line 152
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 153
    invoke-virtual {p0, v6}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 154
    const-string v0, "screen_color_mode_defined_settings_divider"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "oem.display.adaptive.mode.support"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportAdaptive:Z

    .line 157
    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportAdaptive:Z

    if-nez v0, :cond_2

    .line 158
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "oem.display.soft.support"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportSoft:Z

    .line 161
    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportSoft:Z

    if-nez v0, :cond_3

    .line 162
    invoke-virtual {p0, v3}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 163
    invoke-virtual {p0, v7}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 165
    :cond_3
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "device_provisioned"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    move v1, v2

    :cond_4
    iput-boolean v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mDeviceProvision:Z

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mDeviceProvision = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mDeviceProvision:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OPScreenColorMode"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return-void
.end method

.method public onPause()V
    .locals 2

    .line 265
    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    .line 266
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 267
    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 280
    const/4 v0, 0x1

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .line 513
    iput p2, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeValue:I

    .line 514
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oem_screen_better_value"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 515
    return-void
.end method

.method public onRadioButtonClicked(Lcom/android/settings/ui/RadioButtonPreference;)V
    .locals 6
    .param p1, "emiter"    # Lcom/android/settings/ui/RadioButtonPreference;

    .line 372
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 374
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 375
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 376
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 377
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 378
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 379
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    goto/16 :goto_0

    .line 380
    :cond_0
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefaultSettings:Lcom/android/settings/ui/RadioButtonPreference;

    const-string v2, "screen_color_mode_seekbar"

    const/4 v3, 0x1

    if-ne p1, v1, :cond_2

    .line 381
    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 382
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 383
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 384
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 385
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 386
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 387
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 388
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 389
    invoke-virtual {p0, v3}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeSettingsValue(I)V

    .line 391
    :cond_1
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 392
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_11

    .line 393
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 395
    :cond_2
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    const/4 v5, 0x2

    if-ne p1, v4, :cond_4

    .line 396
    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 397
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 398
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 399
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 400
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 401
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 402
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 403
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    if-eq v0, v5, :cond_3

    .line 404
    invoke-virtual {p0, v5}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeSettingsValue(I)V

    .line 406
    :cond_3
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 407
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_11

    .line 408
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 410
    :cond_4
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v4, :cond_6

    .line 411
    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 412
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 413
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 414
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 415
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 416
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 417
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 418
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    .line 419
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeSettingsValue(I)V

    .line 420
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 422
    :cond_5
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mSeekBarpreference:Lcom/oneplus/settings/ui/OPSuwSeekBarPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->addPreference(Landroidx/preference/Preference;)Z

    .line 423
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_11

    sget-boolean v0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->isSupportMMDisplayColor:Z

    if-eqz v0, :cond_11

    .line 424
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->addPreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 426
    :cond_6
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v4, :cond_8

    .line 427
    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 428
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 429
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 430
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 431
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 432
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 433
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 434
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    .line 435
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeSettingsValue(I)V

    .line 437
    :cond_7
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 438
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_11

    .line 439
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 441
    :cond_8
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v4, :cond_a

    .line 442
    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 443
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 444
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 445
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 446
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 447
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 448
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 449
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_9

    .line 450
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeSettingsValue(I)V

    .line 452
    :cond_9
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 453
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_11

    .line 454
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 456
    :cond_a
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v4, :cond_c

    .line 457
    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 458
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 459
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 460
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 461
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 462
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 463
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 464
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_b

    .line 465
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeSettingsValue(I)V

    .line 467
    :cond_b
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 468
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_11

    .line 469
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto/16 :goto_0

    .line 471
    :cond_c
    iget-object v4, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v4, :cond_e

    .line 472
    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 473
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeBasicSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 474
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDefinedSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 475
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeDciP3Settings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 476
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdaptiveModelSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 477
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeSoftSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 478
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAutoSettings:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 479
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_d

    .line 480
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeSettingsValue(I)V

    .line 482
    :cond_d
    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->removePreference(Ljava/lang/String;)Z

    .line 483
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    if-eqz v0, :cond_11

    .line 484
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeCustomPreferenceCategory:Lcom/android/settings/ui/OPSuwPreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    .line 486
    :cond_e
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

    const/4 v2, -0x2

    const-string v4, "screen_color_mode_advanced_settings_value"

    if-ne p1, v1, :cond_f

    .line 487
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4, v0, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 488
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 489
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 490
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    .line 491
    :cond_f
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v1, :cond_10

    .line 492
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4, v3, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 493
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 494
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 495
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    .line 496
    :cond_10
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v1, :cond_11

    .line 497
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4, v5, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 498
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsNTSC:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 499
    iget-object v1, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsSRGB:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 500
    iget-object v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeAdvancedSettingsDisplayP3:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 502
    :cond_11
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "the screen color mode settings value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OPSuwScreenColorMode"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    return-void
.end method

.method public onResume()V
    .locals 6

    .line 213
    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    .line 229
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getScreenColorModeSettingsValue()I

    move-result v0

    .line 230
    .local v0, "value":I
    invoke-direct {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->updateRadioButtons(I)V

    .line 235
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 236
    const-string v2, "night_display_activated"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeContentObserver:Landroid/database/ContentObserver;

    .line 235
    const/4 v4, -0x1

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 238
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 239
    const-string v2, "reading_mode_status_manual"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeContentObserver:Landroid/database/ContentObserver;

    .line 238
    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 241
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->updateAdvancedSettingsRadioButtons()V

    .line 242
    return-void
.end method

.method public onSaveScreenColorModeSettingsValue(I)V
    .locals 3
    .param p1, "value"    # I

    .line 534
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_color_mode_settings_value"

    const/4 v2, -0x2

    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 536
    return-void
.end method

.method public onSaveScreenColorModeValue(I)V
    .locals 2
    .param p1, "value"    # I

    .line 539
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oem_screen_better_value"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 541
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .line 520
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .line 524
    iget v0, p0, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->mScreenColorModeValue:I

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->onSaveScreenColorModeValue(I)V

    .line 525
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 171
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 172
    const v0, 0x7f0d0228

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPSuwScreenColorMode;->setPinnedHeaderView(I)Landroid/view/View;

    .line 173
    return-void
.end method

.method public readFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .line 245
    const-string v0, "0"

    .line 246
    .local v0, "value":Ljava/lang/String;
    const/4 v1, 0x0

    .line 248
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v2

    .line 249
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v2

    .line 253
    nop

    .line 255
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 258
    :goto_0
    goto :goto_1

    .line 256
    :catch_0
    move-exception v2

    .line 257
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 253
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 250
    :catch_1
    move-exception v2

    .line 251
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 253
    .end local v2    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_0

    .line 255
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 261
    :cond_0
    :goto_1
    return-object v0

    .line 253
    :goto_2
    if-eqz v1, :cond_1

    .line 255
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 258
    goto :goto_3

    .line 256
    :catch_2
    move-exception v3

    .line 257
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 258
    .end local v3    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    throw v2
.end method
