.class public Lcom/oneplus/settings/better/OPESportsModeIntroduction;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "OPESportsModeIntroduction.java"


# static fields
.field private static final KEY_ONEPLUS_E_SPORTS_MODE_NETWORK_INTRODUCTION:Ljava/lang/String; = "oneplus_e_sports_mode_network_introduction"


# instance fields
.field private mNetworkPre:Landroidx/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private initNetworkPreference()V
    .locals 3

    .line 52
    const-string v0, "oneplus_e_sports_mode_network_introduction"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->mNetworkPre:Landroidx/preference/Preference;

    .line 54
    iget-object v0, p0, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->mNetworkPre:Landroidx/preference/Preference;

    if-nez v0, :cond_0

    .line 55
    return-void

    .line 58
    :cond_0
    invoke-static {}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->isSupportDualLTEProject()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->isDualSimCard()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->mNetworkPre:Landroidx/preference/Preference;

    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f120c93

    invoke-virtual {v1, v2}, Landroidx/fragment/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->mNetworkPre:Landroidx/preference/Preference;

    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f120c94

    invoke-virtual {v1, v2}, Landroidx/fragment/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 63
    :goto_0
    return-void
.end method

.method private isDualSimCard()Z
    .locals 3

    .line 66
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 67
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v1

    .line 68
    .local v1, "phoneCount":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 69
    const/4 v2, 0x1

    return v2

    .line 71
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public static isSupportDualLTEProject()Z
    .locals 2

    .line 75
    sget-object v0, Lcom/oneplus/settings/SettingsBaseApplication;->mApplication:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1110063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 81
    const/16 v0, 0x270f

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "icicle"    # Landroid/os/Bundle;

    .line 46
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f160096

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->addPreferencesFromResource(I)V

    .line 48
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPESportsModeIntroduction;->initNetworkPreference()V

    .line 49
    return-void
.end method
