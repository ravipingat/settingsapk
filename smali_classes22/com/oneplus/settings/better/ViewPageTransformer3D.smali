.class public Lcom/oneplus/settings/better/ViewPageTransformer3D;
.super Ljava/lang/Object;
.source "ViewPageTransformer3D.java"

# interfaces
.implements Landroidx/viewpager/widget/ViewPager$PageTransformer;


# static fields
.field private static final MAX_ROTATION:F = 20.0f

.field private static final MAX_TRANSLATE:F = 20.0f

.field private static final MIN_SCALE:F = 0.75f


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transformPage(Landroid/view/View;F)V
    .locals 6
    .param p1, "page"    # Landroid/view/View;
    .param p2, "position"    # F

    .line 13
    const/high16 v0, -0x40800000    # -1.0f

    cmpg-float v0, p2, v0

    const/high16 v1, -0x3e600000    # -20.0f

    const/high16 v2, 0x41a00000    # 20.0f

    const/high16 v3, 0x3f400000    # 0.75f

    if-gez v0, :cond_0

    .line 14
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 15
    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleX(F)V

    .line 16
    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleY(F)V

    .line 17
    invoke-virtual {p1, v1}, Landroid/view/View;->setRotationY(F)V

    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    const/high16 v4, 0x3e800000    # 0.25f

    const/high16 v5, 0x3f800000    # 1.0f

    if-gtz v0, :cond_1

    .line 20
    mul-float/2addr v1, p2

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 21
    add-float/2addr v5, p2

    mul-float/2addr v5, v4

    add-float/2addr v5, v3

    .line 22
    .local v5, "scale":F
    invoke-virtual {p1, v5}, Landroid/view/View;->setScaleX(F)V

    .line 23
    invoke-virtual {p1, v5}, Landroid/view/View;->setScaleY(F)V

    .line 24
    mul-float/2addr v2, p2

    invoke-virtual {p1, v2}, Landroid/view/View;->setRotationY(F)V

    .line 25
    .end local v5    # "scale":F
    goto :goto_0

    .line 26
    :cond_1
    cmpg-float v0, p2, v5

    if-gtz v0, :cond_2

    .line 27
    mul-float/2addr v1, p2

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 28
    sub-float/2addr v5, p2

    mul-float/2addr v5, v4

    add-float/2addr v5, v3

    .line 29
    .restart local v5    # "scale":F
    invoke-virtual {p1, v5}, Landroid/view/View;->setScaleX(F)V

    .line 30
    invoke-virtual {p1, v5}, Landroid/view/View;->setScaleY(F)V

    .line 31
    mul-float/2addr v2, p2

    invoke-virtual {p1, v2}, Landroid/view/View;->setRotationY(F)V

    .line 32
    .end local v5    # "scale":F
    goto :goto_0

    .line 34
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 35
    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleX(F)V

    .line 36
    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleY(F)V

    .line 37
    invoke-virtual {p1, v2}, Landroid/view/View;->setRotationY(F)V

    .line 39
    :goto_0
    return-void
.end method
