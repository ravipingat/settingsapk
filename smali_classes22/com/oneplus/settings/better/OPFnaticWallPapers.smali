.class public Lcom/oneplus/settings/better/OPFnaticWallPapers;
.super Lcom/oneplus/settings/BaseActivity;
.source "OPFnaticWallPapers.java"


# static fields
.field public static final ACTIVITED_MODE:I = 0x1

.field public static final DEFAULT_MODE:I = 0x0

.field public static final FNATIC_MODE_EGG_ACTIVATED:Ljava/lang/String; = "fnatic_mode_egg_activated"

.field public static final TAG:Ljava/lang/String; = "OPFnaticWallPapers"

.field public static final TAPS_TO_ACTIVE_HIDDEN_WALLPAPERS:I = 0x5


# instance fields
.field private endColor:I

.field private imageView:Landroid/widget/ImageView;

.field private layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

.field mAnimationAvailable:Z

.field private mAspectRadio:F

.field private mHeadView:Landroid/widget/TextView;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNextButton:Landroid/widget/Button;

.field mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

.field private mVideoLayout:Landroid/view/View;

.field private mVideoPath:Landroid/net/Uri;

.field private mVideoPaused:Z

.field private mVideoReady:Z

.field private mViewPager:Landroidx/viewpager/widget/ViewPager;

.field private mWallPaperLayout:Landroid/view/View;

.field private mWallPaperViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mWallPapers:[I

.field private startColor:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 63
    invoke-direct {p0}, Lcom/oneplus/settings/BaseActivity;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->startColor:I

    .line 68
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->endColor:I

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPapers:[I

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperViews:Ljava/util/ArrayList;

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mAspectRadio:F

    .line 202
    new-instance v0, Lcom/oneplus/settings/better/OPFnaticWallPapers$2;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers$2;-><init>(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f080176
        0x7f080177
        0x7f080178
    .end array-data
.end method

.method static synthetic access$000(Lcom/oneplus/settings/better/OPFnaticWallPapers;)I
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getCurrentState()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->activeFnaticWallPapers()V

    return-void
.end method

.method static synthetic access$1000(Lcom/oneplus/settings/better/OPFnaticWallPapers;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->imageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oneplus/settings/better/OPFnaticWallPapers;)[I
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPapers:[I

    return-object v0
.end method

.method static synthetic access$300(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->updateNextButtonState()V

    return-void
.end method

.method static synthetic access$400(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->gotoWallPaperPickerActivity()V

    return-void
.end method

.method static synthetic access$500(Lcom/oneplus/settings/better/OPFnaticWallPapers;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperViews:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/oneplus/settings/better/OPFnaticWallPapers;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/oneplus/settings/better/OPFnaticWallPapers;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/oneplus/settings/better/OPFnaticWallPapers;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$900(Lcom/oneplus/settings/better/OPFnaticWallPapers;)Z
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;

    .line 63
    iget-boolean v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoReady:Z

    return v0
.end method

.method static synthetic access$902(Lcom/oneplus/settings/better/OPFnaticWallPapers;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/better/OPFnaticWallPapers;
    .param p1, "x1"    # Z

    .line 63
    iput-boolean p1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoReady:Z

    return p1
.end method

.method private activeFnaticWallPapers()V
    .locals 4

    .line 161
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "fnatic_mode_egg_activated"

    const-string v2, "1"

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 163
    return-void
.end method

.method private getCurrentState()I
    .locals 4

    .line 166
    nop

    .line 167
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 166
    const-string v1, "fnatic_mode_egg_activated"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method private gotoWallPaperPickerActivity()V
    .locals 3

    .line 150
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SET_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 151
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "net.oneplus.launcher"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v1, "from_easter_egg_page"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 154
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    goto :goto_0

    .line 155
    :catch_0
    move-exception v1

    .line 156
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 158
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private initMediaPlayer()V
    .locals 4

    .line 240
    const v0, 0x7f0a027c

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoLayout:Landroid/view/View;

    .line 241
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 242
    .local v0, "anima":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 243
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 245
    :try_start_0
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "android.resource"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 246
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const v2, 0x7f11000a

    .line 247
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 248
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoPath:Landroid/net/Uri;

    .line 249
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoPath:Landroid/net/Uri;

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 250
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    if-lez v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/oneplus/settings/better/-$$Lambda$OPFnaticWallPapers$addNcANW3El0e9XKqAj5iPLWAoU;

    invoke-direct {v2, p0}, Lcom/oneplus/settings/better/-$$Lambda$OPFnaticWallPapers$addNcANW3El0e9XKqAj5iPLWAoU;-><init>(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 252
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    sget-object v2, Lcom/oneplus/settings/better/-$$Lambda$OPFnaticWallPapers$ceFLfLCph-KLYewvMbZCy7tA5p0;->INSTANCE:Lcom/oneplus/settings/better/-$$Lambda$OPFnaticWallPapers$ceFLfLCph-KLYewvMbZCy7tA5p0;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 253
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/oneplus/settings/better/OPFnaticWallPapers$3;

    invoke-direct {v2, p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers$3;-><init>(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 265
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mAnimationAvailable:Z

    .line 266
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->updateAspectRatio()V

    goto :goto_0

    .line 268
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->setVisible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    goto :goto_1

    .line 270
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "OPFnaticWallPapers"

    const-string v3, "Animation resource not found. Will not show animation."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    const v1, 0x7f0a0757

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/TextureView;

    .line 274
    .local v1, "video":Landroid/view/TextureView;
    const v2, 0x7f0a0754

    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->imageView:Landroid/widget/ImageView;

    .line 275
    const v2, 0x7f0a0750

    invoke-virtual {p0, v2}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/settings/widget/AspectRatioFrameLayout;

    iput-object v2, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

    .line 276
    iget-object v2, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

    iget v3, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mAspectRadio:F

    invoke-virtual {v2, v3}, Lcom/android/settings/widget/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 283
    new-instance v2, Lcom/oneplus/settings/better/OPFnaticWallPapers$4;

    invoke-direct {v2, p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers$4;-><init>(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 316
    return-void
.end method

.method private initViews()V
    .locals 4

    .line 100
    const v0, 0x7f0a027e

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperLayout:Landroid/view/View;

    .line 102
    const v0, 0x7f0a02cd

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mHeadView:Landroid/widget/TextView;

    .line 103
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mHeadView:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPapers:[I

    array-length v2, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f120ceb

    invoke-virtual {p0, v2, v1}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    const v0, 0x7f0a042e

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mNextButton:Landroid/widget/Button;

    .line 105
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mNextButton:Landroid/widget/Button;

    new-instance v1, Lcom/oneplus/settings/better/OPFnaticWallPapers$1;

    invoke-direct {v1, p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers$1;-><init>(Lcom/oneplus/settings/better/OPFnaticWallPapers;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    const v0, 0x7f0a0280

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    .line 128
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->initWallPapers()V

    .line 129
    return-void
.end method

.method private initWallPapers()V
    .locals 5

    .line 225
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 226
    .local v0, "mLi":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 227
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPapers:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 228
    const v2, 0x7f0d01b0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 229
    .local v2, "itemView":Landroid/view/View;
    const v3, 0x7f0a027d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 232
    .local v3, "wallPaperView":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPapers:[I

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 233
    iget-object v4, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    .end local v2    # "itemView":Landroid/view/View;
    .end local v3    # "wallPaperView":Landroid/widget/ImageView;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 237
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method static synthetic lambda$initMediaPlayer$1(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p0, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .line 252
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V

    return-void
.end method

.method private runAlphaAnim()V
    .locals 4

    .line 196
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperLayout:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [I

    iget v2, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->startColor:I

    const/4 v3, 0x0

    aput v2, v1, v3

    iget v2, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->endColor:I

    const/4 v3, 0x1

    aput v2, v1, v3

    const-string v2, "backgroundColor"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 197
    .local v0, "alpahAnimator":Landroid/animation/ObjectAnimator;
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 198
    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 199
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 200
    return-void
.end method

.method private updateNextButtonState()V
    .locals 6

    .line 172
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getCurrentState()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 173
    .local v0, "unlocked":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 174
    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mNextButton:Landroid/widget/Button;

    const v2, 0x7f120cec

    .line 175
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    .line 177
    :cond_1
    iget-object v3, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mNextButton:Landroid/widget/Button;

    const v4, 0x7f120cdf

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPapers:[I

    array-length v5, v5

    .line 178
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 177
    invoke-virtual {p0, v4, v2}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 180
    :goto_1
    return-void
.end method


# virtual methods
.method public synthetic lambda$initMediaPlayer$0$OPFnaticWallPapers(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoReady:Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 89
    invoke-super {p0, p1}, Lcom/oneplus/settings/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    const v0, 0x7f0d01b1

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->setContentView(I)V

    .line 91
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 93
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 94
    .local v0, "actionBar":Landroid/app/ActionBar;
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 95
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->initViews()V

    .line 96
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->initMediaPlayer()V

    .line 97
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 190
    invoke-super {p0}, Lcom/oneplus/settings/BaseActivity;->onDestroy()V

    .line 191
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->releaseMediaPlayer()V

    .line 192
    return-void
.end method

.method public onPause()V
    .locals 0

    .line 184
    invoke-super {p0}, Lcom/oneplus/settings/BaseActivity;->onPause()V

    .line 185
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->setVideoPaused()V

    .line 186
    return-void
.end method

.method public onResume()V
    .locals 6

    .line 134
    const-string v0, "com.android.systemui"

    invoke-super {p0}, Lcom/oneplus/settings/BaseActivity;->onResume()V

    .line 135
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->isInMultiWindowMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 138
    .local v2, "systemUIContext":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "dock_forced_resizable"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 139
    .local v0, "toastStringId":I
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 140
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v0    # "toastStringId":I
    .end local v2    # "systemUIContext":Landroid/content/Context;
    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 145
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->setVideoResume()V

    .line 146
    invoke-direct {p0}, Lcom/oneplus/settings/better/OPFnaticWallPapers;->updateNextButtonState()V

    .line 147
    return-void
.end method

.method public releaseMediaPlayer()V
    .locals 1

    .line 332
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 333
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 334
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 335
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 338
    :cond_0
    return-void
.end method

.method public setVideoPaused()V
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->layout:Lcom/android/settings/widget/AspectRatioFrameLayout;

    if-eqz v0, :cond_1

    .line 345
    iget v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mAspectRadio:F

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 347
    :cond_1
    return-void
.end method

.method public setVideoResume()V
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mWallPaperLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 320
    return-void

    .line 322
    :cond_0
    iget-boolean v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mVideoPaused:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 325
    :cond_1
    return-void
.end method

.method updateAspectRatio()V
    .locals 2

    .line 328
    iget-object v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/oneplus/settings/better/OPFnaticWallPapers;->mAspectRadio:F

    .line 329
    return-void
.end method
