.class public Lcom/oneplus/settings/ui/OPWallPaperUtils;
.super Ljava/lang/Object;
.source "OPWallPaperUtils.java"


# static fields
.field public static final ATLEAST_OREO_MR1:Z

.field public static final CROP_FILE_PREFIX:Ljava/lang/String; = "crop_"

.field public static final DEFAULT_COMPRESS_FORMAT:Landroid/graphics/Bitmap$CompressFormat;

.field public static final DEFAULT_COMPRESS_QUALITY:I = 0x64

.field private static final DUMMY_WALLPAPER:Landroid/content/ComponentName;

.field public static final LEGACY_ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

.field private static final MAX_WALLPAPER_BITMAP_SIZE:I = 0x1800

.field public static final ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

.field public static final ONEPLUS_H2_BLUR_WALLPAPER:Landroid/content/ComponentName;

.field private static final ONEPLUS_LIVE_WALLPAPER:Landroid/content/ComponentName;

.field private static final ONEPLUS_LIVE_WALLPAPER_URI:Landroid/net/Uri;

.field public static final TAG:Ljava/lang/String; = "OPWallpaperUtils"

.field public static final WALLPAPER_NAME:Ljava/lang/String; = "wallpaper"

.field private static sMinValidLiveWallpaperPreviewHeightPx:I

.field private static sMinValidLiveWallpaperPreviewWidthPx:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    const-string v0, "net.oneplus.launcher/.wallpaper.BlurWallpaper"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    sput-object v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

    .line 30
    const-string v0, "com.oneplus.wallpaper/.BlurWallpaper"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    sput-object v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->LEGACY_ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

    .line 31
    const-string v0, "net.oneplus.h2launcher/.wallpaper.BlurWallpaper"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    sput-object v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_H2_BLUR_WALLPAPER:Landroid/content/ComponentName;

    .line 32
    nop

    .line 33
    const-string v0, "com.oneplus.wallpaper/.LiveWallpaper"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    sput-object v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_LIVE_WALLPAPER:Landroid/content/ComponentName;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_LIVE_WALLPAPER:Landroid/content/ComponentName;

    .line 35
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/image/wallpaper"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_LIVE_WALLPAPER_URI:Landroid/net/Uri;

    .line 37
    const-string v0, "net.oneplus.launcher/.wallpaper.DummyWallpaper"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    sput-object v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->DUMMY_WALLPAPER:Landroid/content/ComponentName;

    .line 41
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    sput-object v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->DEFAULT_COMPRESS_FORMAT:Landroid/graphics/Bitmap$CompressFormat;

    .line 43
    const/4 v0, -0x1

    sput v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->sMinValidLiveWallpaperPreviewWidthPx:I

    .line 44
    sput v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->sMinValidLiveWallpaperPreviewHeightPx:I

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1b

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ATLEAST_OREO_MR1:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 7
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "requiredWidth"    # I
    .param p2, "requiredHeight"    # I

    .line 178
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 179
    .local v0, "height":I
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 180
    .local v1, "width":I
    const/4 v2, 0x1

    .line 181
    .local v2, "inSampleSize":I
    if-gt v0, p2, :cond_0

    if-le v1, p1, :cond_2

    .line 182
    :cond_0
    div-int/lit8 v3, v0, 0x2

    .line 183
    .local v3, "halfHeight":I
    div-int/lit8 v4, v1, 0x2

    .line 184
    .local v4, "halfWidth":I
    :goto_0
    div-int v5, v3, v2

    if-le v5, p2, :cond_1

    div-int v5, v4, v2

    if-gt v5, p1, :cond_3

    :cond_1
    div-int v5, v3, v2

    const/16 v6, 0x1800

    if-gt v5, v6, :cond_3

    div-int v5, v4, v2

    if-le v5, v6, :cond_2

    goto :goto_1

    .line 191
    .end local v3    # "halfHeight":I
    .end local v4    # "halfWidth":I
    :cond_2
    return v2

    .line 188
    .restart local v3    # "halfHeight":I
    .restart local v4    # "halfWidth":I
    :cond_3
    :goto_1
    mul-int/lit8 v2, v2, 0x2

    goto :goto_0
.end method

.method public static checkPeekWallpaperPermission(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 210
    sget-boolean v0, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ATLEAST_OREO_MR1:Z

    if-eqz v0, :cond_0

    .line 211
    const-string v0, "android.permission.READ_EXTERNAL_STORAGE"

    .line 212
    .local v0, "permission":Ljava/lang/String;
    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {p0, v1}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->hasGrantedPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 214
    .end local v0    # "permission":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public static getBitmapResizeWidthHeight(Landroid/content/Context;Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "is"    # Ljava/io/InputStream;

    .line 304
    const-string v0, "OPWallpaperUtils"

    const/4 v1, 0x0

    .line 306
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 307
    .local v2, "r":Landroid/content/res/Resources;
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 308
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v4, 0x8

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 309
    const/4 v4, 0x0

    invoke-static {p1, v4, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 314
    .end local v2    # "r":Landroid/content/res/Resources;
    .end local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    goto :goto_1

    .line 312
    :catch_0
    move-exception v2

    .line 313
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBitmapResizeWidthHeight e = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 310
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 311
    .local v2, "error":Ljava/lang/OutOfMemoryError;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBitmapResizeWidthHeight error = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "error":Ljava/lang/OutOfMemoryError;
    goto :goto_0

    .line 315
    :goto_1
    return-object v1
.end method

.method public static getOnePlusBlurWallpaperFile(Landroid/content/Context;)Ljava/io/File;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 173
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "wallpaper"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getOnePlusLiveWallpaperBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .line 234
    const-string v0, "OPWallpaperUtils"

    const/4 v1, 0x0

    .line 235
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_LIVE_WALLPAPER_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    .local v2, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-static {p0, v2}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->getBitmapResizeWidthHeight(Landroid/content/Context;Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v1, v3

    .line 238
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOnePlusLiveWallpaperBitmap bitmap = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, " is = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 241
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 245
    .end local v2    # "is":Ljava/io/InputStream;
    goto :goto_1

    .line 235
    .restart local v2    # "is":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "is":Ljava/io/InputStream;
    .end local p0    # "context":Landroid/content/Context;
    :try_start_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 241
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local p0    # "context":Landroid/content/Context;
    :catchall_1
    move-exception v4

    if-eqz v2, :cond_0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v5

    :try_start_5
    invoke-virtual {v3, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local p0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    throw v4
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p0    # "context":Landroid/content/Context;
    :catch_0
    move-exception v2

    .line 242
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOnePlusLiveWallpaperBitmap# error loading wallpaper image, error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    return-object v1
.end method

.method public static hasGrantedPermission(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permission"    # Ljava/lang/String;

    .line 196
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->hasGrantedPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static varargs hasGrantedPermissions(Landroid/content/Context;[Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permissions"    # [Ljava/lang/String;

    .line 201
    const/4 v0, 0x1

    .line 202
    .local v0, "hasPermission":Z
    array-length v1, p1

    const/4 v2, 0x0

    move v3, v0

    move v0, v2

    .end local v0    # "hasPermission":Z
    .local v3, "hasPermission":Z
    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v4, p1, v0

    .line 203
    .local v4, "p":Ljava/lang/String;
    invoke-virtual {p0, v4}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    move v5, v2

    :goto_1
    and-int/2addr v3, v5

    .line 202
    .end local v4    # "p":Ljava/lang/String;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_1
    return v3
.end method

.method public static isDefaultImageWallpaper(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 79
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->checkPeekWallpaperPermission(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 80
    const-string v0, "OPWallpaperUtils"

    const-string v2, "[isImageWallpaper] permission is not granted, return false"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    return v1

    .line 85
    :cond_0
    nop

    .line 86
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 87
    .local v0, "wallpaperManager":Landroid/app/WallpaperManager;
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->peekFastDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_1

    .line 88
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 87
    :goto_0
    return v1
.end method

.method public static isImageWallpaper(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 92
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->checkPeekWallpaperPermission(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 93
    const-string v0, "OPWallpaperUtils"

    const-string v2, "[isImageWallpaper] permission is not granted, return false"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return v1

    .line 98
    :cond_0
    nop

    .line 99
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 100
    .local v0, "wallpaperManager":Landroid/app/WallpaperManager;
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->peekFastDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 101
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 100
    :goto_0
    return v1
.end method

.method public static isLegacyOnePlusBlurWallpaper(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 72
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    .line 74
    .local v0, "info":Landroid/app/WallpaperInfo;
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    sget-object v2, Lcom/oneplus/settings/ui/OPWallPaperUtils;->LEGACY_ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 74
    :goto_0
    return v1
.end method

.method public static isNormalLiveWallpaper(Landroid/app/WallpaperInfo;)Z
    .locals 2
    .param p0, "wallpaperInfo"    # Landroid/app/WallpaperInfo;

    .line 224
    if-eqz p0, :cond_0

    .line 225
    invoke-virtual {p0}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    sget-object v1, Lcom/oneplus/settings/ui/OPWallPaperUtils;->LEGACY_ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    invoke-virtual {p0}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    sget-object v1, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    sget-object v1, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_H2_BLUR_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    invoke-virtual {p0}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    sget-object v1, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_LIVE_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 224
    :goto_0
    return v0
.end method

.method public static isNormalLiveWallpaper(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 218
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    .line 220
    .local v0, "info":Landroid/app/WallpaperInfo;
    invoke-static {v0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->isNormalLiveWallpaper(Landroid/app/WallpaperInfo;)Z

    move-result v1

    return v1
.end method

.method public static isOnePlusBlurWallpaper(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 55
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 56
    .local v0, "user":Landroid/os/UserHandle;
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v1

    .line 58
    .local v1, "info":Landroid/app/WallpaperInfo;
    if-eqz v1, :cond_1

    .line 59
    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    sget-object v3, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 60
    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    sget-object v3, Lcom/oneplus/settings/ui/OPWallPaperUtils;->LEGACY_ONEPLUS_BLUR_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 58
    :goto_0
    return v2
.end method

.method public static isOnePlusH2BlurWallpaper(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 64
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 65
    .local v0, "user":Landroid/os/UserHandle;
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v1

    .line 67
    .local v1, "info":Landroid/app/WallpaperInfo;
    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    sget-object v3, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_H2_BLUR_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 67
    :goto_0
    return v2
.end method

.method public static isOnePlusLiveWallpaper(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 48
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    .line 50
    .local v0, "info":Landroid/app/WallpaperInfo;
    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v0}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    sget-object v2, Lcom/oneplus/settings/ui/OPWallPaperUtils;->ONEPLUS_LIVE_WALLPAPER:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 50
    :goto_0
    return v1
.end method

.method public static loadHomeWallpaper(Landroid/content/Context;Landroid/app/WallpaperInfo;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "wallpaperInfo"    # Landroid/app/WallpaperInfo;

    .line 251
    nop

    .line 252
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 253
    .local v0, "wallpaperManager":Landroid/app/WallpaperManager;
    const/4 v1, 0x0

    .line 255
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->isOnePlusLiveWallpaper(Landroid/content/Context;)Z

    move-result v2

    const-string v3, "OPWallpaperUtils"

    if-eqz v2, :cond_0

    .line 256
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->getOnePlusLiveWallpaperBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_2

    .line 257
    :cond_0
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->isImageWallpaper(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 258
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 259
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v4, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_5

    .line 260
    move-object v4, v2

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_2

    .line 262
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->isNormalLiveWallpaper(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p1, :cond_2

    .line 264
    if-nez v1, :cond_5

    .line 265
    nop

    .line 266
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 265
    invoke-virtual {p1, v2}, Landroid/app/WallpaperInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 267
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    invoke-static {p0, v2}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->loadThumbnailFromWallpaperIcon(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 268
    .end local v2    # "icon":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .line 269
    :cond_2
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->isOnePlusBlurWallpaper(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 270
    invoke-static {p0}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->getOnePlusBlurWallpaperFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 271
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 273
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 274
    .local v4, "r":Landroid/content/res/Resources;
    const v5, 0x7f0704d6

    .line 275
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 276
    .local v5, "reqW":I
    const v6, 0x7f0704d5

    .line 277
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 278
    .local v6, "reqH":I
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 279
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v8, 0x1

    iput-boolean v8, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 280
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 281
    invoke-static {v7, v5, v6}, Lcom/oneplus/settings/ui/OPWallPaperUtils;->calculateSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v8

    iput v8, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 283
    const/4 v8, 0x0

    iput-boolean v8, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 284
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v8

    .line 287
    .end local v4    # "r":Landroid/content/res/Resources;
    .end local v5    # "reqW":I
    .end local v6    # "reqH":I
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    goto :goto_1

    .line 285
    :catch_0
    move-exception v4

    .line 286
    .local v4, "error":Ljava/lang/OutOfMemoryError;
    const-string v5, "failed loading oneplus blur wallpaper file"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "error":Ljava/lang/OutOfMemoryError;
    goto :goto_0

    .line 289
    :cond_3
    const-string v4, "wallpaper for latest blur wallpaper is empty"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    .end local v2    # "f":Ljava/io/File;
    :goto_1
    goto :goto_2

    .line 292
    :cond_4
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 293
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v4, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_5

    .line 294
    move-object v4, v2

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 297
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    :goto_2
    if-eqz v1, :cond_6

    .line 298
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OPSettings-loadHomeWallpaper-widht:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " height:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_6
    return-object v1
.end method

.method public static loadThumbnailFromWallpaperIcon(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .line 106
    const/4 v0, 0x0

    const-string v1, "OPWallpaperUtils"

    if-nez p0, :cond_0

    .line 107
    const-string v2, "[loadThumbnailFromWallpaperIcon] invalid context"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    return-object v0

    .line 111
    :cond_0
    if-nez p1, :cond_1

    .line 112
    const-string v2, "[loadThumbnailFromWallpaperIcon] invalid icon drawable"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return-object v0

    .line 116
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 117
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0704d6

    .line 118
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 119
    .local v1, "w":I
    const v2, 0x7f0704d5

    .line 120
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 121
    .local v2, "h":I
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 122
    .local v3, "bitmap":Landroid/graphics/Bitmap;
    const/high16 v4, -0x1000000

    invoke-static {p1, v4}, Lcom/oneplus/settings/ui/OPColorUtils;->getMainColor(Landroid/graphics/drawable/Drawable;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 123
    return-object v3
.end method
