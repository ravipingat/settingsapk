.class Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;
.super Lcom/oneplus/lib/widget/recyclerview/OPItemDecoration;
.source "OPCustomShapePreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oneplus/settings/ui/OPCustomShapePreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SpaceItemDecoration"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/oneplus/settings/ui/OPCustomShapePreference;


# direct methods
.method public constructor <init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/oneplus/settings/ui/OPCustomShapePreference;
    .param p2, "space"    # I

    .line 361
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomShapePreference;

    .line 362
    invoke-direct {p0, p2}, Lcom/oneplus/lib/widget/recyclerview/OPItemDecoration;-><init>(I)V

    .line 363
    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Lcom/oneplus/lib/widget/recyclerview/RecyclerView;Lcom/oneplus/lib/widget/recyclerview/RecyclerView$State;)V
    .locals 3
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Lcom/oneplus/lib/widget/recyclerview/RecyclerView;
    .param p4, "state"    # Lcom/oneplus/lib/widget/recyclerview/RecyclerView$State;

    .line 368
    invoke-super {p0, p1, p2, p3, p4}, Lcom/oneplus/lib/widget/recyclerview/OPItemDecoration;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Lcom/oneplus/lib/widget/recyclerview/RecyclerView;Lcom/oneplus/lib/widget/recyclerview/RecyclerView$State;)V

    .line 369
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomShapePreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070460

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 371
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/oneplus/lib/widget/recyclerview/RecyclerView$LayoutParams;

    invoke-virtual {v0}, Lcom/oneplus/lib/widget/recyclerview/RecyclerView$LayoutParams;->getViewAdapterPosition()I

    move-result v0

    .line 372
    .local v0, "position":I
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomShapePreference;

    invoke-static {v2}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->access$200(Lcom/oneplus/settings/ui/OPCustomShapePreference;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_0

    .line 373
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomShapePreference;

    invoke-virtual {v2}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 376
    :cond_0
    return-void
.end method
