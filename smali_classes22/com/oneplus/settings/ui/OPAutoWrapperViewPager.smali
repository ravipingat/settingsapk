.class public Lcom/oneplus/settings/ui/OPAutoWrapperViewPager;
.super Landroidx/viewpager/widget/ViewPager;
.source "OPAutoWrapperViewPager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 11
    invoke-direct {p0, p1}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 14
    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 19
    const/4 v0, 0x0

    .line 20
    .local v0, "height":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPAutoWrapperViewPager;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 21
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/ui/OPAutoWrapperViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 22
    .local v2, "child":Landroid/view/View;
    nop

    .line 23
    const/4 v3, 0x0

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 22
    invoke-virtual {v2, p1, v3}, Landroid/view/View;->measure(II)V

    .line 24
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 25
    .local v3, "h":I
    if-le v3, v0, :cond_0

    move v0, v3

    .line 20
    .end local v2    # "child":Landroid/view/View;
    .end local v3    # "h":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 28
    .end local v1    # "i":I
    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 31
    invoke-super {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;->onMeasure(II)V

    .line 32
    return-void
.end method
