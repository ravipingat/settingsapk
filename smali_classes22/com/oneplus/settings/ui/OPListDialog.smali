.class public Lcom/oneplus/settings/ui/OPListDialog;
.super Ljava/lang/Object;
.source "OPListDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/settings/ui/OPListDialog$OnDialogListItemClickListener;,
        Lcom/oneplus/settings/ui/OPListDialog$DialogListAdapter;
    }
.end annotation


# static fields
.field private static final TYPE_NEGATIVE:I = 0x2

.field private static final TYPE_POSITIVE:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentIndex:I

.field private mDialog:Landroid/app/AlertDialog;

.field private mDialogListAdapter:Lcom/oneplus/settings/ui/OPListDialog$DialogListAdapter;

.field private mListEntries:[Ljava/lang/String;

.field private mListEntriesValue:[Ljava/lang/String;

.field private mListView:Landroid/widget/ListView;

.field private mNegativeCount:I

.field private mOnDialogListItemClickListener:Lcom/oneplus/settings/ui/OPListDialog$OnDialogListItemClickListener;

.field private mPositiveCount:I

.field private mRootContainer:Landroid/widget/RadioGroup;

.field private mVibrateKey:Ljava/lang/String;

.field private n:Landroid/widget/Button;

.field private p:Landroid/widget/Button;

.field private status:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->p:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->n:Landroid/widget/Button;

    .line 36
    const/4 v1, 0x0

    iput v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mPositiveCount:I

    .line 37
    iput v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mNegativeCount:I

    .line 38
    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    .line 42
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/oneplus/settings/ui/OPListDialog;->status:Z

    .line 45
    iput v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    .line 80
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mContext:Landroid/content/Context;

    .line 81
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d01cd

    invoke-virtual {v1, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mRootContainer:Landroid/widget/RadioGroup;

    .line 85
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mRootContainer:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    .line 87
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "listEntriesValue"    # [Ljava/lang/String;
    .param p4, "listEntries"    # [Ljava/lang/String;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->p:Landroid/widget/Button;

    .line 35
    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->n:Landroid/widget/Button;

    .line 36
    const/4 v1, 0x0

    iput v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mPositiveCount:I

    .line 37
    iput v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mNegativeCount:I

    .line 38
    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    .line 42
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/oneplus/settings/ui/OPListDialog;->status:Z

    .line 45
    iput v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    .line 91
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mContext:Landroid/content/Context;

    .line 92
    iput-object p3, p0, Lcom/oneplus/settings/ui/OPListDialog;->mListEntriesValue:[Ljava/lang/String;

    .line 93
    iput-object p4, p0, Lcom/oneplus/settings/ui/OPListDialog;->mListEntries:[Ljava/lang/String;

    .line 94
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d01cd

    invoke-virtual {v1, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 95
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0a0582

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mRootContainer:Landroid/widget/RadioGroup;

    .line 97
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/oneplus/settings/ui/OPListDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/oneplus/settings/ui/OPListDialog;->mRootContainer:Landroid/widget/RadioGroup;

    .line 98
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 99
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 100
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v3, Lcom/oneplus/settings/ui/OPListDialog$2;

    invoke-direct {v3, p0}, Lcom/oneplus/settings/ui/OPListDialog$2;-><init>(Lcom/oneplus/settings/ui/OPListDialog;)V

    .line 101
    const v4, 0x7f120b98

    invoke-virtual {v1, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v3, Lcom/oneplus/settings/ui/OPListDialog$1;

    invoke-direct {v3, p0}, Lcom/oneplus/settings/ui/OPListDialog$1;-><init>(Lcom/oneplus/settings/ui/OPListDialog;)V

    .line 112
    const v4, 0x7f1203d8

    invoke-virtual {v1, v4, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 122
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    .line 123
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 124
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/oneplus/settings/ui/OPListDialog$3;

    invoke-direct {v2, p0}, Lcom/oneplus/settings/ui/OPListDialog$3;-><init>(Lcom/oneplus/settings/ui/OPListDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 132
    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/ui/OPListDialog;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPListDialog;

    .line 28
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mListEntriesValue:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/oneplus/settings/ui/OPListDialog;)Lcom/oneplus/settings/ui/OPListDialog$OnDialogListItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPListDialog;

    .line 28
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mOnDialogListItemClickListener:Lcom/oneplus/settings/ui/OPListDialog$OnDialogListItemClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oneplus/settings/ui/OPListDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPListDialog;

    .line 28
    iget v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$202(Lcom/oneplus/settings/ui/OPListDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPListDialog;
    .param p1, "x1"    # I

    .line 28
    iput p1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    return p1
.end method

.method static synthetic access$300(Lcom/oneplus/settings/ui/OPListDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPListDialog;

    .line 28
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .line 277
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/ui/OPListDialog;->setStatus(Z)V

    .line 279
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 281
    :try_start_0
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    goto :goto_0

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 287
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method public getNButton()Landroid/widget/Button;
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->n:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 332
    return-object v0

    .line 334
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPButton()Landroid/widget/Button;
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->p:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 325
    return-object v0

    .line 327
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRootContainer()Landroid/view/View;
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mRootContainer:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method public setButtonType(IIZ)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "count"    # I
    .param p3, "isDisable"    # Z

    .line 299
    if-gtz p2, :cond_0

    .line 300
    return-void

    .line 303
    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 304
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->p:Landroid/widget/Button;

    .line 305
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->p:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 306
    iput p2, p0, Lcom/oneplus/settings/ui/OPListDialog;->mPositiveCount:I

    goto :goto_0

    .line 309
    :cond_1
    const/4 v0, -0x2

    if-ne p1, v0, :cond_2

    .line 310
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->n:Landroid/widget/Button;

    .line 311
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->n:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 312
    iput p2, p0, Lcom/oneplus/settings/ui/OPListDialog;->mNegativeCount:I

    .line 316
    :cond_2
    :goto_0
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .line 231
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 232
    return-void
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "listener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p3, "count"    # I

    .line 264
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 265
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/oneplus/settings/ui/OPListDialog$6;

    invoke-direct {v1, p0, p2}, Lcom/oneplus/settings/ui/OPListDialog$6;-><init>(Lcom/oneplus/settings/ui/OPListDialog;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 273
    return-void
.end method

.method public setOnDialogListItemClickListener(Lcom/oneplus/settings/ui/OPListDialog$OnDialogListItemClickListener;)V
    .locals 0
    .param p1, "onDialogListItemClickListener"    # Lcom/oneplus/settings/ui/OPListDialog$OnDialogListItemClickListener;

    .line 143
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPListDialog;->mOnDialogListItemClickListener:Lcom/oneplus/settings/ui/OPListDialog$OnDialogListItemClickListener;

    .line 144
    return-void
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "listener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p3, "count"    # I

    .line 257
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 259
    return-void
.end method

.method public setStatus(Z)V
    .locals 0
    .param p1, "status"    # Z

    .line 250
    iput-boolean p1, p0, Lcom/oneplus/settings/ui/OPListDialog;->status:Z

    .line 252
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "resId"    # I

    .line 235
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 236
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 240
    return-void
.end method

.method public setVibrateKey(Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .line 147
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportXVibrate()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPVibrateUtils;->getRealXVibrateValueToIndex(I)I

    move-result v0

    iput v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    goto :goto_0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mListEntriesValue:[Ljava/lang/String;

    array-length v0, v0

    .line 153
    .local v0, "groupSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_2

    .line 154
    iget-object v3, p0, Lcom/oneplus/settings/ui/OPListDialog;->mRootContainer:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v2}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 155
    .local v3, "radioButton":Landroid/widget/RadioButton;
    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 156
    iget-object v4, p0, Lcom/oneplus/settings/ui/OPListDialog;->mListEntries:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget v4, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    if-ne v4, v2, :cond_1

    .line 158
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2

    .line 160
    :cond_1
    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 162
    :goto_2
    new-instance v4, Lcom/oneplus/settings/ui/OPListDialog$4;

    invoke-direct {v4, p0}, Lcom/oneplus/settings/ui/OPListDialog$4;-><init>(Lcom/oneplus/settings/ui/OPListDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    .end local v3    # "radioButton":Landroid/widget/RadioButton;
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 188
    .end local v2    # "i":I
    :cond_2
    return-void
.end method

.method public setVibrateLevelKey(Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .line 191
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    .line 192
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mListEntriesValue:[Ljava/lang/String;

    array-length v0, v0

    .line 193
    .local v0, "groupSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 194
    iget-object v3, p0, Lcom/oneplus/settings/ui/OPListDialog;->mRootContainer:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v2}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 195
    .local v3, "radioButton":Landroid/widget/RadioButton;
    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 196
    iget-object v4, p0, Lcom/oneplus/settings/ui/OPListDialog;->mListEntries:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget v4, p0, Lcom/oneplus/settings/ui/OPListDialog;->mCurrentIndex:I

    if-ne v4, v2, :cond_0

    .line 198
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1

    .line 200
    :cond_0
    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 202
    :goto_1
    new-instance v4, Lcom/oneplus/settings/ui/OPListDialog$5;

    invoke-direct {v4, p0}, Lcom/oneplus/settings/ui/OPListDialog$5;-><init>(Lcom/oneplus/settings/ui/OPListDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    .end local v3    # "radioButton":Landroid/widget/RadioButton;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 228
    .end local v2    # "i":I
    :cond_1
    return-void
.end method

.method public show()V
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 246
    return-void
.end method
