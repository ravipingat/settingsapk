.class public Lcom/oneplus/settings/ui/OPCustomShapePreference;
.super Landroidx/preference/Preference;
.source "OPCustomShapePreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;,
        Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;,
        Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;,
        Lcom/oneplus/settings/ui/OPCustomShapePreference$ChooseStyleAdapter;
    }
.end annotation


# instance fields
.field private mAdaptiveAirplane:Landroid/view/View;

.field private mAdaptiveFlashlight:Landroid/view/View;

.field private mAdaptiveNfc:Landroid/view/View;

.field private mAdaptiveWifi:Landroid/view/View;

.field private mAm:Landroid/app/ActivityManager;

.field private mChooseStyleAdapter:Lcom/oneplus/settings/ui/OPCustomShapePreference$ChooseStyleAdapter;

.field private mCurrentVH:Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;

.field private mIndicator1:Landroid/view/View;

.field private mIndicator2:Landroid/view/View;

.field private final mItemEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;",
            ">;"
        }
    .end annotation
.end field

.field protected mLastIndex:I

.field private mNotificationGroup:Landroid/view/ViewGroup;

.field private mOPRecyclerView:Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;

.field private mPowerPanelGroup:Landroid/view/ViewGroup;

.field private final mPreviewList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviewViewPager:Lcom/android/internal/widget/ViewPager;

.field private mQuicklySettingsGroup:Landroid/view/ViewGroup;

.field protected mSelectedIndex:I

.field private mSoundPanelGroup:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPreviewList:Ljava/util/List;

    .line 64
    const v0, 0x7f0d018a

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->setLayoutResource(I)V

    .line 65
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAm:Landroid/app/ActivityManager;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/ui/OPCustomShapePreference;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPCustomShapePreference;

    .line 45
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPreviewList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/oneplus/settings/ui/OPCustomShapePreference;I)V
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPCustomShapePreference;
    .param p1, "x1"    # I

    .line 45
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->setIndicator(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/oneplus/settings/ui/OPCustomShapePreference;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPCustomShapePreference;

    .line 45
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/oneplus/settings/ui/OPCustomShapePreference;)Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPCustomShapePreference;

    .line 45
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mCurrentVH:Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;

    return-object v0
.end method

.method static synthetic access$302(Lcom/oneplus/settings/ui/OPCustomShapePreference;Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;)Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPCustomShapePreference;
    .param p1, "x1"    # Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;

    .line 45
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mCurrentVH:Lcom/oneplus/settings/ui/OPCustomShapePreference$ShapeViewHolder;

    return-object p1
.end method

.method static synthetic access$400(Lcom/oneplus/settings/ui/OPCustomShapePreference;I)V
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPCustomShapePreference;
    .param p1, "x1"    # I

    .line 45
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->changeAdaptive(I)V

    return-void
.end method

.method private changeAdaptive(I)V
    .locals 3
    .param p1, "currentShape"    # I

    .line 249
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mQuicklySettingsGroup:Landroid/view/ViewGroup;

    const v1, 0x7f0805f2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 252
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mNotificationGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 253
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSoundPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 254
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPowerPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 256
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveWifi:Landroid/view/View;

    const v1, 0x7f0802ec

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 257
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveFlashlight:Landroid/view/View;

    const v2, 0x7f0802eb

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 258
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveNfc:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 259
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveAirplane:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 261
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mQuicklySettingsGroup:Landroid/view/ViewGroup;

    const v1, 0x7f0805f3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 263
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mNotificationGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 264
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSoundPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 265
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPowerPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 267
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveWifi:Landroid/view/View;

    const v1, 0x7f0802f0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 268
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveFlashlight:Landroid/view/View;

    const v2, 0x7f0802ef

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 269
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveNfc:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 270
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveAirplane:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 271
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 273
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mQuicklySettingsGroup:Landroid/view/ViewGroup;

    const v1, 0x7f0805f4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 274
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mNotificationGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 275
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSoundPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 276
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPowerPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 278
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveWifi:Landroid/view/View;

    const v1, 0x7f0802f9

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 279
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveFlashlight:Landroid/view/View;

    const v2, 0x7f0802f8

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 280
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveNfc:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 281
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveAirplane:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 282
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 284
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mQuicklySettingsGroup:Landroid/view/ViewGroup;

    const v1, 0x7f0805f1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 285
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mNotificationGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 286
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSoundPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 287
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPowerPanelGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 289
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveWifi:Landroid/view/View;

    const v1, 0x7f0802f3

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 290
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveFlashlight:Landroid/view/View;

    const v2, 0x7f0802f2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 291
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveNfc:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 292
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveAirplane:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 294
    :cond_3
    :goto_0
    return-void
.end method

.method private setIndicator(I)V
    .locals 3
    .param p1, "index"    # I

    .line 229
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPUtils;->isBlackModeOn(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    const v0, 0x7f080661

    const v1, 0x7f08065f

    if-nez p1, :cond_0

    .line 231
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator1:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 232
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator2:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 234
    :cond_0
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator2:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 235
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator1:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 238
    :cond_1
    const v0, 0x7f080662

    const v1, 0x7f080660

    if-nez p1, :cond_2

    .line 239
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator1:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 240
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator2:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 242
    :cond_2
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator2:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 243
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator1:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 246
    :goto_0
    return-void
.end method


# virtual methods
.method public needShowWarningDialog()Z
    .locals 2

    .line 104
    iget v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mLastIndex:I

    iget v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onAttached()V
    .locals 0

    .line 100
    invoke-super {p0}, Landroidx/preference/Preference;->onAttached()V

    .line 101
    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 16
    .param p1, "holder"    # Landroidx/preference/PreferenceViewHolder;

    .line 109
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super/range {p0 .. p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 110
    const v2, 0x7f0a01b4

    invoke-virtual {v1, v2}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;

    iput-object v2, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mOPRecyclerView:Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;

    .line 111
    new-instance v2, Lcom/oneplus/lib/widget/recyclerview/LinearLayoutManager;

    invoke-virtual/range {p0 .. p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/oneplus/lib/widget/recyclerview/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 112
    .local v2, "manager":Lcom/oneplus/lib/widget/recyclerview/LinearLayoutManager;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/oneplus/lib/widget/recyclerview/LinearLayoutManager;->setOrientation(I)V

    .line 113
    iget-object v4, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mOPRecyclerView:Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;

    invoke-virtual {v4, v2}, Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;->setLayoutManager(Lcom/oneplus/lib/widget/recyclerview/RecyclerView$LayoutManager;)V

    .line 114
    iget-object v4, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mOPRecyclerView:Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;

    new-instance v5, Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;

    invoke-direct {v5, v0, v3}, Lcom/oneplus/settings/ui/OPCustomShapePreference$SpaceItemDecoration;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;I)V

    invoke-virtual {v4, v5}, Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;->addOPItemDecoration(Lcom/oneplus/lib/widget/recyclerview/OPItemDecoration;)V

    .line 116
    const v4, 0x7f0a04f9

    invoke-virtual {v1, v4}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator1:Landroid/view/View;

    .line 117
    const v4, 0x7f0a04fa

    invoke-virtual {v1, v4}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mIndicator2:Landroid/view/View;

    .line 118
    invoke-direct {v0, v3}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->setIndicator(I)V

    .line 119
    invoke-virtual/range {p0 .. p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 120
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const/4 v5, 0x0

    const v6, 0x7f0d018c

    invoke-virtual {v4, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 121
    .local v6, "viewNotification":Landroid/view/View;
    const v7, 0x7f0d018b

    invoke-virtual {v4, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 123
    .local v5, "viewPanel":Landroid/view/View;
    iget-object v7, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPreviewList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v7, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPreviewList:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const v7, 0x7f0a0552

    invoke-virtual {v1, v7}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/ViewPager;

    iput-object v7, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPreviewViewPager:Lcom/android/internal/widget/ViewPager;

    .line 130
    new-instance v7, Lcom/oneplus/settings/ui/OPCustomShapePreference$1;

    invoke-direct {v7, v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference$1;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;)V

    .line 157
    .local v7, "adapter":Lcom/android/internal/widget/PagerAdapter;
    iget-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPreviewViewPager:Lcom/android/internal/widget/ViewPager;

    invoke-virtual {v8, v7}, Lcom/android/internal/widget/ViewPager;->setAdapter(Lcom/android/internal/widget/PagerAdapter;)V

    .line 159
    iget-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPreviewViewPager:Lcom/android/internal/widget/ViewPager;

    new-instance v9, Lcom/oneplus/settings/ui/OPCustomShapePreference$2;

    invoke-direct {v9, v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference$2;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;)V

    invoke-virtual {v8, v9}, Lcom/android/internal/widget/ViewPager;->setOnPageChangeListener(Lcom/android/internal/widget/ViewPager$OnPageChangeListener;)V

    .line 176
    const v8, 0x7f0a005f

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveWifi:Landroid/view/View;

    .line 177
    const v8, 0x7f0a005d

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveFlashlight:Landroid/view/View;

    .line 178
    const v8, 0x7f0a005e

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveNfc:Landroid/view/View;

    .line 179
    const v8, 0x7f0a005c

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAdaptiveAirplane:Landroid/view/View;

    .line 181
    const v8, 0x7f0a057f

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mQuicklySettingsGroup:Landroid/view/ViewGroup;

    .line 182
    const v8, 0x7f0a0444

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mNotificationGroup:Landroid/view/ViewGroup;

    .line 183
    const v8, 0x7f0a0643

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSoundPanelGroup:Landroid/view/ViewGroup;

    .line 184
    const v8, 0x7f0a0542

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    iput-object v8, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mPowerPanelGroup:Landroid/view/ViewGroup;

    .line 187
    new-instance v8, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    invoke-virtual/range {p0 .. p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f120f81

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const v11, 0x7f0802ed

    invoke-direct {v8, v0, v9, v11, v10}, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;Ljava/lang/String;II)V

    .line 188
    .local v8, "item1":Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;
    new-instance v9, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    invoke-virtual/range {p0 .. p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f120f83

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    const v13, 0x7f0802f1

    invoke-direct {v9, v0, v11, v13, v12}, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;Ljava/lang/String;II)V

    .line 189
    .local v9, "item2":Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;
    new-instance v11, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    invoke-virtual/range {p0 .. p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f120f84

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x3

    const v15, 0x7f0802fa

    invoke-direct {v11, v0, v13, v15, v14}, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;Ljava/lang/String;II)V

    .line 190
    .local v11, "item3":Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;
    new-instance v13, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    invoke-virtual/range {p0 .. p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v15

    const v14, 0x7f120f82

    invoke-virtual {v15, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x4

    const v12, 0x7f0802f4

    invoke-direct {v13, v0, v14, v12, v15}, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;Ljava/lang/String;II)V

    move-object v12, v13

    .line 191
    .local v12, "item4":Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;
    iget-object v13, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    invoke-interface {v13, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v13, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    invoke-interface {v13, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v13, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    invoke-interface {v13, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v13, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    invoke-interface {v13, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/oneplus/settings/utils/OPThemeUtils;->getCurrentShape(Landroid/content/Context;)I

    move-result v13

    .line 199
    .local v13, "currentShape":I
    if-ne v13, v10, :cond_0

    .line 200
    iput-boolean v10, v8, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->selected:Z

    .line 201
    iput v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mLastIndex:I

    .line 202
    iput v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    goto :goto_0

    .line 203
    :cond_0
    const/4 v3, 0x2

    if-ne v13, v3, :cond_1

    .line 204
    iput-boolean v10, v9, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->selected:Z

    .line 205
    iput v10, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mLastIndex:I

    .line 206
    iput v10, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    goto :goto_0

    .line 207
    :cond_1
    const/4 v3, 0x3

    if-ne v13, v3, :cond_2

    .line 208
    iput-boolean v10, v11, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->selected:Z

    .line 209
    const/4 v3, 0x2

    iput v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mLastIndex:I

    .line 210
    iput v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    goto :goto_0

    .line 211
    :cond_2
    if-ne v13, v15, :cond_3

    .line 212
    iput-boolean v10, v12, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->selected:Z

    .line 213
    const/4 v3, 0x3

    iput v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mLastIndex:I

    .line 214
    iput v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    .line 217
    :cond_3
    :goto_0
    invoke-direct {v0, v13}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->changeAdaptive(I)V

    .line 219
    new-instance v3, Lcom/oneplus/settings/ui/OPCustomShapePreference$ChooseStyleAdapter;

    invoke-direct {v3, v0}, Lcom/oneplus/settings/ui/OPCustomShapePreference$ChooseStyleAdapter;-><init>(Lcom/oneplus/settings/ui/OPCustomShapePreference;)V

    iput-object v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mChooseStyleAdapter:Lcom/oneplus/settings/ui/OPCustomShapePreference$ChooseStyleAdapter;

    .line 220
    iget-object v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mOPRecyclerView:Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;

    iget-object v10, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mChooseStyleAdapter:Lcom/oneplus/settings/ui/OPCustomShapePreference$ChooseStyleAdapter;

    invoke-virtual {v3, v10}, Lcom/oneplus/lib/widget/recyclerview/OPRecyclerView;->setAdapter(Lcom/oneplus/lib/widget/recyclerview/RecyclerView$Adapter;)V

    .line 222
    iget v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    if-ltz v3, :cond_4

    iget-object v10, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v3, v10, :cond_4

    .line 223
    iget v3, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    invoke-virtual {v2, v3}, Lcom/oneplus/lib/widget/recyclerview/LinearLayoutManager;->scrollToPosition(I)V

    .line 225
    :cond_4
    return-void
.end method

.method public saveSelectedShape()V
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mAm:Landroid/app/ActivityManager;

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPApplicationUtils;->killProcess(Landroid/app/ActivityManager;)V

    .line 84
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    iget v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    iget v0, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->index:I

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPThemeUtils;->getCurrentShapeByIndex(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "oneplus_shape"

    invoke-static {v2, v0, v1}, Lcom/oneplus/settings/utils/OPThemeUtils;->enableTheme(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 85
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCustomShapePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    iget v2, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    iget v1, v1, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->index:I

    invoke-static {v0, v1}, Lcom/oneplus/settings/utils/OPThemeUtils;->setCurrentShape(Landroid/content/Context;I)V

    .line 86
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    iget v1, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    iget v0, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->index:I

    const-string v1, "status"

    const-string v2, "shape"

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 87
    const-string v0, "round"

    invoke-static {v2, v1, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    iget v3, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    iget v0, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->index:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 89
    const-string v0, "square"

    invoke-static {v2, v1, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    iget v3, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    iget v0, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->index:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 91
    const-string v0, "teardrop"

    invoke-static {v2, v1, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mItemEntities:Ljava/util/List;

    iget v3, p0, Lcom/oneplus/settings/ui/OPCustomShapePreference;->mSelectedIndex:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;

    iget v0, v0, Lcom/oneplus/settings/ui/OPCustomShapePreference$ItemEntity;->index:I

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    .line 93
    const-string v0, "rectangle"

    invoke-static {v2, v1, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_3
    :goto_0
    return-void
.end method
