.class Lcom/oneplus/settings/ui/OPCustomTonePreference$SpaceItemDecoration;
.super Lcom/oneplus/lib/widget/recyclerview/OPItemDecoration;
.source "OPCustomTonePreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oneplus/settings/ui/OPCustomTonePreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SpaceItemDecoration"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/oneplus/settings/ui/OPCustomTonePreference;


# direct methods
.method constructor <init>(Lcom/oneplus/settings/ui/OPCustomTonePreference;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/oneplus/settings/ui/OPCustomTonePreference;
    .param p2, "space"    # I

    .line 302
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCustomTonePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomTonePreference;

    .line 303
    invoke-direct {p0, p2}, Lcom/oneplus/lib/widget/recyclerview/OPItemDecoration;-><init>(I)V

    .line 304
    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Lcom/oneplus/lib/widget/recyclerview/RecyclerView;Lcom/oneplus/lib/widget/recyclerview/RecyclerView$State;)V
    .locals 4
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Lcom/oneplus/lib/widget/recyclerview/RecyclerView;
    .param p4, "state"    # Lcom/oneplus/lib/widget/recyclerview/RecyclerView$State;

    .line 309
    invoke-super {p0, p1, p2, p3, p4}, Lcom/oneplus/lib/widget/recyclerview/OPItemDecoration;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Lcom/oneplus/lib/widget/recyclerview/RecyclerView;Lcom/oneplus/lib/widget/recyclerview/RecyclerView$State;)V

    .line 310
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCustomTonePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomTonePreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomTonePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070460

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 312
    .local v0, "margin":I
    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 313
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/oneplus/lib/widget/recyclerview/RecyclerView$LayoutParams;

    invoke-virtual {v2}, Lcom/oneplus/lib/widget/recyclerview/RecyclerView$LayoutParams;->getViewAdapterPosition()I

    move-result v2

    .line 314
    .local v2, "position":I
    iget-object v3, p0, Lcom/oneplus/settings/ui/OPCustomTonePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomTonePreference;

    invoke-static {v3}, Lcom/oneplus/settings/ui/OPCustomTonePreference;->access$100(Lcom/oneplus/settings/ui/OPCustomTonePreference;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_0

    .line 315
    iget-object v3, p0, Lcom/oneplus/settings/ui/OPCustomTonePreference$SpaceItemDecoration;->this$0:Lcom/oneplus/settings/ui/OPCustomTonePreference;

    invoke-virtual {v3}, Lcom/oneplus/settings/ui/OPCustomTonePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 318
    :cond_0
    return-void
.end method
