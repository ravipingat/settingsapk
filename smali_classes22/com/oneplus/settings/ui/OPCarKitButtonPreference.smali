.class public Lcom/oneplus/settings/ui/OPCarKitButtonPreference;
.super Lcom/android/settingslib/RestrictedPreference;
.source "OPCarKitButtonPreference.java"


# instance fields
.field protected mButtonEnable:Z

.field protected mButtonString:Ljava/lang/String;

.field private mButtonVisible:Z

.field private mContext:Landroid/content/Context;

.field protected mIcon:Landroid/graphics/drawable/Drawable;

.field private mLeftIcon:Landroid/widget/ImageView;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mRightButton:Landroid/widget/Button;

.field private mTextButtonColor:Landroid/content/res/ColorStateList;

.field private mTextSummaryString:Ljava/lang/String;

.field private mTextSummaryVisible:Z

.field private mTextTitle:Landroid/widget/TextView;

.field private mTextTitleString:Ljava/lang/String;

.field private resid:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 63
    invoke-direct {p0, p1}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;)V

    .line 36
    const v0, 0x7f0d0178

    iput v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->resid:I

    .line 64
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->initViews(Landroid/content/Context;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const v0, 0x7f0d0178

    iput v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->resid:I

    .line 59
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->initViews(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    const v0, 0x7f0d0178

    iput v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->resid:I

    .line 54
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->initViews(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method private initViews(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 68
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mContext:Landroid/content/Context;

    .line 69
    iget v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->resid:I

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->setLayoutResource(I)V

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextTitleString:Ljava/lang/String;

    .line 71
    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryString:Ljava/lang/String;

    .line 72
    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonString:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonEnable:Z

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonVisible:Z

    .line 80
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPUtils;->creatOneplusPrimaryColorStateList(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextButtonColor:Landroid/content/res/ColorStateList;

    .line 81
    return-void
.end method


# virtual methods
.method public getButtonString()Ljava/lang/String;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonString:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getLeftButton()Landroid/widget/Button;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mRightButton:Landroid/widget/Button;

    return-object v0
.end method

.method public getLeftTextSummary()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryString:Ljava/lang/String;

    return-object v0
.end method

.method public getLeftTextTitle()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextTitleString:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryString:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextTitleString:Ljava/lang/String;

    return-object v0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3
    .param p1, "view"    # Landroidx/preference/PreferenceViewHolder;

    .line 85
    invoke-super {p0, p1}, Lcom/android/settingslib/RestrictedPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 86
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->setDividerAllowedBelow(Z)V

    .line 87
    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->setDividerAllowedAbove(Z)V

    .line 88
    const v1, 0x7f0a0391

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mLeftIcon:Landroid/widget/ImageView;

    .line 89
    const v1, 0x7f0a05ac

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mRightButton:Landroid/widget/Button;

    .line 90
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mRightButton:Landroid/widget/Button;

    iget-boolean v2, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonVisible:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mRightButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextButtonColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 92
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mRightButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mRightButton:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 94
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mRightButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 95
    const v0, 0x7f0a0396

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextTitle:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextTitleString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 99
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mLeftIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    :cond_1
    return-void
.end method

.method public setButtonEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 116
    iput-boolean p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonEnable:Z

    .line 117
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->notifyChanged()V

    .line 118
    return-void
.end method

.method public setButtonString(Ljava/lang/String;)V
    .locals 0
    .param p1, "buttonString"    # Ljava/lang/String;

    .line 149
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonString:Ljava/lang/String;

    .line 150
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->notifyChanged()V

    .line 151
    return-void
.end method

.method public setButtonVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .line 121
    iput-boolean p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mButtonVisible:Z

    .line 122
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->notifyChanged()V

    .line 123
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 159
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 160
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->notifyChanged()V

    .line 161
    return-void
.end method

.method public setLeftTextSummary(Ljava/lang/String;)V
    .locals 1
    .param p1, "summary"    # Ljava/lang/String;

    .line 135
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryString:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryString:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryVisible:Z

    goto :goto_0

    .line 139
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryVisible:Z

    .line 141
    :goto_0
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->notifyChanged()V

    .line 142
    return-void
.end method

.method public setLeftTextTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .line 126
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextTitleString:Ljava/lang/String;

    .line 127
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->notifyChanged()V

    .line 128
    return-void
.end method

.method public setOnButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "onClickListener"    # Landroid/view/View$OnClickListener;

    .line 104
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 105
    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "summary"    # Ljava/lang/CharSequence;

    .line 175
    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->setLeftTextSummary(Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method public setSummaryVisibility(Z)V
    .locals 0
    .param p1, "visiable"    # Z

    .line 108
    iput-boolean p1, p0, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->mTextSummaryVisible:Z

    .line 109
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->notifyChanged()V

    .line 110
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .line 170
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->setLeftTextTitle(Ljava/lang/String;)V

    .line 171
    return-void
.end method
