.class public Lcom/oneplus/settings/ui/ColorPickerView;
.super Landroid/view/View;
.source "ColorPickerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final RESTRICT_MODE:Z = false

.field private static final TAG:Ljava/lang/String; = "ColorPickerView"


# instance fields
.field private mDrawingRect:Landroid/graphics/RectF;

.field private mHue:F

.field private mHueIndicatorPaint:Landroid/graphics/Paint;

.field private mHueIndicatorWidth:F

.field private mHueInidcatorOffset:F

.field private mHuePaint:Landroid/graphics/Paint;

.field private mHuePanelHeight:F

.field private mHueRect:Landroid/graphics/RectF;

.field private mHueShader:Landroid/graphics/Shader;

.field private mHueTouchRect:Landroid/graphics/RectF;

.field private mIndicatorBlurRadius:F

.field private mIndicatorBorderWidth:F

.field private mIndicatorColor:I

.field private mIndicatorCornerRadius:F

.field private mListener:Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;

.field private mMarginLeft:F

.field private mMarginTop:F

.field private mMinHeight:F

.field private mMinWidth:F

.field private mPadding:F

.field private mPanelSpacing:F

.field private mPanelWidth:F

.field private mSVIndicatorPaint:Landroid/graphics/Paint;

.field private mSVIndicatorWidth:F

.field private mSVPanelHeight:F

.field private mSat:F

.field private mSatPaint:Landroid/graphics/Paint;

.field private mSatRect:Landroid/graphics/RectF;

.field private mSatShader:Landroid/graphics/Shader;

.field private mSatTouchRect:Landroid/graphics/RectF;

.field private mSatValPaint:Landroid/graphics/Paint;

.field private mSatValRect:Landroid/graphics/RectF;

.field private mSatValTouchRect:Landroid/graphics/RectF;

.field private mTouchPoint:Landroid/graphics/Point;

.field private mVal:F

.field private mValPaint:Landroid/graphics/Paint;

.field private mValRect:Landroid/graphics/RectF;

.field private mValShader:Landroid/graphics/Shader;

.field private mValTouchRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/oneplus/settings/ui/ColorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/oneplus/settings/ui/ColorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 100
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mTouchPoint:Landroid/graphics/Point;

    .line 79
    const/high16 v0, 0x43b40000    # 360.0f

    iput v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    .line 81
    iput v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    .line 101
    invoke-direct {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->init()V

    .line 105
    return-void
.end method

.method private drawHuePanel(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 298
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    .line 300
    .local v0, "rect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueShader:Landroid/graphics/Shader;

    if-nez v1, :cond_1

    .line 301
    const/16 v1, 0x169

    new-array v1, v1, [I

    .line 302
    .local v1, "hue":[I
    const/4 v2, 0x0

    .line 303
    .local v2, "count":I
    const/4 v3, 0x0

    move v10, v2

    .end local v2    # "count":I
    .local v3, "i":I
    .local v10, "count":I
    :goto_0
    array-length v2, v1

    const/4 v4, 0x1

    sub-int/2addr v2, v4

    if-gt v3, v2, :cond_0

    .line 304
    const/4 v2, 0x3

    new-array v2, v2, [F

    const/4 v5, 0x0

    int-to-float v6, v3

    aput v6, v2, v5

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v2, v4

    const/4 v4, 0x2

    aput v5, v2, v4

    invoke-static {v2}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v2

    aput v2, v1, v10

    .line 303
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 306
    .end local v3    # "i":I
    :cond_0
    new-instance v11, Landroid/graphics/LinearGradient;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    iget v5, v0, Landroid/graphics/RectF;->right:F

    iget v6, v0, Landroid/graphics/RectF;->top:F

    const/4 v8, 0x0

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v2, v11

    move-object v7, v1

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v11, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueShader:Landroid/graphics/Shader;

    .line 314
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueShader:Landroid/graphics/Shader;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 317
    .end local v1    # "hue":[I
    .end local v10    # "count":I
    :cond_1
    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    const/high16 v2, 0x40400000    # 3.0f

    mul-float v3, v1, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 319
    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    .line 320
    .local v1, "halfHueIndicatorWidth":F
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    invoke-direct {p0, v2}, Lcom/oneplus/settings/ui/ColorPickerView;->hueToPoint(F)Landroid/graphics/Point;

    move-result-object v2

    .line 321
    .local v2, "p":Landroid/graphics/Point;
    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    iget-object v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    iget v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    const/high16 v4, 0x43b40000    # 360.0f

    div-float/2addr v3, v4

    iget-object v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 323
    .local v3, "left":F
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 324
    .local v4, "r":Landroid/graphics/RectF;
    iput v3, v4, Landroid/graphics/RectF;->left:F

    .line 325
    iget v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    add-float/2addr v5, v3

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 326
    iget v5, v0, Landroid/graphics/RectF;->top:F

    iget v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    sub-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 327
    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    add-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 329
    iget v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 331
    return-void
.end method

.method private drawSatPanel(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 226
    const/4 v0, 0x3

    new-array v0, v0, [F

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    const/4 v2, 0x0

    aput v1, v0, v2

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    aput v1, v0, v2

    const/4 v2, 0x2

    aput v1, v0, v2

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    .line 227
    .local v0, "rgb":I
    new-instance v1, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v5, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v6, v2, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v7, v2, Landroid/graphics/RectF;->top:F

    sget-object v10, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const/4 v8, -0x1

    move-object v3, v1

    move v9, v0

    invoke-direct/range {v3 .. v10}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatShader:Landroid/graphics/Shader;

    .line 230
    .end local v0    # "rgb":I
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatShader:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 231
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    const/high16 v2, 0x40400000    # 3.0f

    mul-float v3, v1, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 233
    iget v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 236
    .local v0, "left":F
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 237
    .local v1, "rect":Landroid/graphics/RectF;
    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 238
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    add-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 239
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 240
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 242
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 244
    return-void
.end method

.method private drawSatValPanel(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 199
    invoke-direct {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->generateSVShader()Landroid/graphics/ComposeShader;

    move-result-object v0

    .line 201
    .local v0, "mShader":Landroid/graphics/ComposeShader;
    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 202
    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 204
    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    invoke-direct {p0, v1, v2}, Lcom/oneplus/settings/ui/ColorPickerView;->satValToPoint(FF)Landroid/graphics/Point;

    move-result-object v1

    .line 207
    .local v1, "p":Landroid/graphics/Point;
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 208
    .local v2, "rect":Landroid/graphics/RectF;
    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorWidth:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 209
    .local v3, "halfWidth":F
    iget v4, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    iput v4, v2, Landroid/graphics/RectF;->left:F

    .line 210
    iget v4, v1, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    iput v4, v2, Landroid/graphics/RectF;->right:F

    .line 211
    iget v4, v1, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    iput v4, v2, Landroid/graphics/RectF;->top:F

    .line 212
    iget v4, v1, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    iput v4, v2, Landroid/graphics/RectF;->bottom:F

    .line 214
    iget v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 217
    return-void
.end method

.method private drawValPanel(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 252
    new-instance v8, Landroid/graphics/LinearGradient;

    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->top:F

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const/4 v5, -0x1

    const/high16 v6, -0x1000000

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v8, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    .line 255
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 256
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    const/high16 v2, 0x40400000    # 3.0f

    mul-float v3, v1, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 258
    iget v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    sub-float/2addr v0, v2

    mul-float/2addr v1, v0

    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v0

    iget v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    add-float/2addr v1, v0

    .line 261
    .local v1, "left":F
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 262
    .local v0, "rect":Landroid/graphics/RectF;
    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 263
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 264
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    sub-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 265
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 267
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 269
    return-void
.end method

.method private generateRestrictedSVShader()Landroid/graphics/ComposeShader;
    .locals 11

    .line 272
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    if-nez v0, :cond_0

    .line 273
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    const/4 v6, -0x1

    const v7, -0xaaaaab

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    .line 277
    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    aput v2, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    .line 278
    .local v0, "rgb":I
    new-instance v1, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v5, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v6, v2, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v7, v2, Landroid/graphics/RectF;->top:F

    const v8, -0x555556

    sget-object v10, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v3, v1

    move v9, v0

    invoke-direct/range {v3 .. v10}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 281
    .local v1, "satShader":Landroid/graphics/Shader;
    new-instance v2, Landroid/graphics/ComposeShader;

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3, v1, v4}, Landroid/graphics/ComposeShader;-><init>(Landroid/graphics/Shader;Landroid/graphics/Shader;Landroid/graphics/PorterDuff$Mode;)V

    return-object v2
.end method

.method private generateSVShader()Landroid/graphics/ComposeShader;
    .locals 11

    .line 285
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    const/4 v6, -0x1

    const/high16 v7, -0x1000000

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    .line 290
    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    aput v2, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    .line 291
    .local v0, "rgb":I
    new-instance v1, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v5, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v6, v2, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v7, v2, Landroid/graphics/RectF;->top:F

    const/4 v8, -0x1

    sget-object v10, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v3, v1

    move v9, v0

    invoke-direct/range {v3 .. v10}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 294
    .local v1, "satShader":Landroid/graphics/Shader;
    new-instance v2, Landroid/graphics/ComposeShader;

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValShader:Landroid/graphics/Shader;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3, v1, v4}, Landroid/graphics/ComposeShader;-><init>(Landroid/graphics/Shader;Landroid/graphics/Shader;Landroid/graphics/PorterDuff$Mode;)V

    return-object v2
.end method

.method private hueToPoint(F)Landroid/graphics/Point;
    .locals 6
    .param p1, "hue"    # F

    .line 334
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    .line 335
    .local v0, "rect":Landroid/graphics/RectF;
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 337
    .local v1, "width":F
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 338
    .local v2, "p":Landroid/graphics/Point;
    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Point;->y:I

    .line 339
    iget v3, v0, Landroid/graphics/RectF;->left:F

    mul-float v4, p1, v1

    const/high16 v5, 0x43b40000    # 360.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 340
    return-object v2
.end method

.method private init()V
    .locals 4

    .line 108
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 111
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0704ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelWidth:F

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "panel width 375dp:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelWidth:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ColorPickerView"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device width pixels:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v1, 0x0

    iput v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelSpacing:F

    .line 119
    const v2, 0x7f0702ab

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMarginTop:F

    .line 120
    const v2, 0x7f0702a9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMarginLeft:F

    .line 122
    const v2, 0x7f0701a6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePanelHeight:F

    .line 123
    const v2, 0x7f070507

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVPanelHeight:F

    .line 125
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelWidth:F

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMinWidth:F

    .line 126
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePanelHeight:F

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVPanelHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelSpacing:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMinHeight:F

    .line 128
    const v2, 0x7f0701a8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorCornerRadius:F

    .line 129
    const v2, 0x7f0701a9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBlurRadius:F

    .line 130
    const v2, 0x7f0701a7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    .line 131
    const v2, 0x7f060200

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorColor:I

    .line 132
    const v2, 0x7f0701a5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorWidth:F

    .line 133
    const v2, 0x7f0701a4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    .line 134
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    iput v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorWidth:F

    .line 135
    iput v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPadding:F

    .line 136
    invoke-direct {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->initPaintTools()V

    .line 137
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/ui/ColorPickerView;->setFocusable(Z)V

    .line 138
    invoke-virtual {p0, v1}, Lcom/oneplus/settings/ui/ColorPickerView;->setFocusableInTouchMode(Z)V

    .line 139
    return-void
.end method

.method private initPaintTools()V
    .locals 5

    .line 141
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValPaint:Landroid/graphics/Paint;

    .line 142
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePaint:Landroid/graphics/Paint;

    .line 143
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatPaint:Landroid/graphics/Paint;

    .line 144
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValPaint:Landroid/graphics/Paint;

    .line 146
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorPaint:Landroid/graphics/Paint;

    .line 147
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 149
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 150
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 151
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueIndicatorPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBlurRadius:F

    const v3, -0x777778

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v4, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 153
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    .line 154
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 155
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 156
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBorderWidth:F

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 157
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 158
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mIndicatorBlurRadius:F

    invoke-virtual {v0, v1, v4, v4, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 159
    return-void
.end method

.method private static isUnspecified(I)Z
    .locals 1
    .param p0, "mode"    # I

    .line 180
    const/high16 v0, 0x40000000    # 2.0f

    if-eq p0, v0, :cond_0

    const/high16 v0, -0x80000000

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private pointToHue(F)F
    .locals 3
    .param p1, "x"    # F

    .line 477
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    .line 478
    .local v0, "rect":Landroid/graphics/RectF;
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 480
    .local v1, "width":F
    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v2, p1, v2

    if-gez v2, :cond_0

    .line 481
    const/4 p1, 0x0

    goto :goto_0

    .line 482
    :cond_0
    iget v2, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, p1, v2

    if-lez v2, :cond_1

    .line 483
    move p1, v1

    goto :goto_0

    .line 485
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v2

    .line 487
    :goto_0
    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v2, p1

    div-float/2addr v2, v1

    return v2
.end method

.method private pointToSat(FF)F
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 425
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    .line 426
    .local v0, "rect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 428
    .local v1, "result":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 430
    .local v2, "width":F
    iget v3, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v3, p1, v3

    if-gez v3, :cond_0

    .line 431
    const/4 p1, 0x0

    goto :goto_0

    .line 432
    :cond_0
    iget v3, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v3, p1, v3

    if-lez v3, :cond_1

    .line 433
    move p1, v2

    goto :goto_0

    .line 435
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v3

    .line 437
    :goto_0
    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v3, v2

    mul-float/2addr v3, p1

    .line 438
    .end local v1    # "result":F
    .local v3, "result":F
    return v3
.end method

.method private pointToSatRestrictedMode(FF)F
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 408
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    .line 409
    .local v0, "rect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 411
    .local v1, "result":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 413
    .local v2, "width":F
    iget v3, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v3, p1, v3

    if-gez v3, :cond_0

    .line 414
    const/4 p1, 0x0

    goto :goto_0

    .line 415
    :cond_0
    iget v3, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v3, p1, v3

    if-lez v3, :cond_1

    .line 416
    move p1, v2

    goto :goto_0

    .line 418
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v3

    .line 420
    :goto_0
    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v3, v2

    mul-float/2addr v3, p1

    .line 421
    .end local v1    # "result":F
    .local v3, "result":F
    return v3
.end method

.method private pointToSatVal(FF)[F
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 382
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    .line 383
    .local v0, "rect":Landroid/graphics/RectF;
    const/4 v1, 0x2

    new-array v1, v1, [F

    .line 385
    .local v1, "result":[F
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 386
    .local v2, "width":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 388
    .local v3, "height":F
    iget v4, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v4, p1, v4

    if-gez v4, :cond_0

    .line 389
    const/4 p1, 0x0

    goto :goto_0

    .line 390
    :cond_0
    iget v4, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v4, p1, v4

    if-lez v4, :cond_1

    .line 391
    move p1, v2

    goto :goto_0

    .line 393
    :cond_1
    iget v4, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v4

    .line 396
    :goto_0
    iget v4, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v4, p2, v4

    if-gez v4, :cond_2

    .line 397
    const/4 p2, 0x0

    goto :goto_1

    .line 398
    :cond_2
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v4, p2, v4

    if-lez v4, :cond_3

    .line 399
    move p2, v3

    goto :goto_1

    .line 401
    :cond_3
    iget v4, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr p2, v4

    .line 403
    :goto_1
    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    div-float v6, v5, v2

    mul-float/2addr v6, p1

    aput v6, v1, v4

    .line 404
    const/4 v4, 0x1

    div-float v6, v5, v3

    mul-float/2addr v6, p2

    sub-float/2addr v5, v6

    aput v5, v1, v4

    .line 405
    return-object v1
.end method

.method private pointToSatValRestrictedMode(FF)[F
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 355
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    .line 356
    .local v0, "rect":Landroid/graphics/RectF;
    const/4 v1, 0x2

    new-array v1, v1, [F

    .line 358
    .local v1, "result":[F
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 359
    .local v2, "width":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 361
    .local v3, "height":F
    iget v4, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v4, p1, v4

    if-gez v4, :cond_0

    .line 362
    const/4 p1, 0x0

    goto :goto_0

    .line 363
    :cond_0
    iget v4, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v4, p1, v4

    if-lez v4, :cond_1

    .line 364
    move p1, v2

    goto :goto_0

    .line 366
    :cond_1
    iget v4, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v4

    .line 369
    :goto_0
    iget v4, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v4, p2, v4

    if-gez v4, :cond_2

    .line 370
    const/4 p2, 0x0

    goto :goto_1

    .line 371
    :cond_2
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v4, p2, v4

    if-lez v4, :cond_3

    .line 372
    move p2, v3

    goto :goto_1

    .line 374
    :cond_3
    iget v4, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr p2, v4

    .line 376
    :goto_1
    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    div-float v6, v5, v2

    mul-float/2addr v6, p1

    aput v6, v1, v4

    .line 377
    const/4 v4, 0x1

    div-float v6, v5, v3

    mul-float/2addr v6, p2

    sub-float/2addr v5, v6

    aput v5, v1, v4

    .line 378
    return-object v1
.end method

.method private pointToVal(FF)F
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 459
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    .line 460
    .local v0, "rect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 462
    .local v1, "result":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 464
    .local v2, "width":F
    iget v3, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v3, p1, v3

    if-gez v3, :cond_0

    .line 465
    const/4 p1, 0x0

    goto :goto_0

    .line 466
    :cond_0
    iget v3, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v3, p1, v3

    if-lez v3, :cond_1

    .line 467
    move p1, v2

    goto :goto_0

    .line 469
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v3

    .line 472
    :goto_0
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v4, v3, v2

    mul-float/2addr v4, p1

    sub-float/2addr v3, v4

    .line 473
    .end local v1    # "result":F
    .local v3, "result":F
    return v3
.end method

.method private pointToValRestrictedMode(FF)F
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 442
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    .line 443
    .local v0, "rect":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 445
    .local v1, "result":F
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 447
    .local v2, "width":F
    iget v3, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v3, p1, v3

    if-gez v3, :cond_0

    .line 448
    const/4 p1, 0x0

    goto :goto_0

    .line 449
    :cond_0
    iget v3, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v3, p1, v3

    if-lez v3, :cond_1

    .line 450
    move p1, v2

    goto :goto_0

    .line 452
    :cond_1
    iget v3, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr p1, v3

    .line 454
    :goto_0
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v4, v3, v2

    mul-float/2addr v4, p1

    sub-float/2addr v3, v4

    .line 455
    .end local v1    # "result":F
    .local v3, "result":F
    return v3
.end method

.method private satValToPoint(FF)Landroid/graphics/Point;
    .locals 5
    .param p1, "sat"    # F
    .param p2, "val"    # F

    .line 345
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 346
    .local v0, "height":F
    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 348
    .local v1, "width":F
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 349
    .local v2, "p":Landroid/graphics/Point;
    mul-float v3, p1, v1

    iget-object v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 350
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, p2

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Point;->y:I

    .line 351
    return-object v2
.end method

.method private setupHueRect()V
    .locals 8

    .line 602
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    .line 603
    .local v0, "dRect":Landroid/graphics/RectF;
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 604
    .local v1, "left":F
    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 605
    .local v2, "top":F
    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePanelHeight:F

    add-float/2addr v3, v2

    .line 606
    .local v3, "bottom":F
    iget v4, v0, Landroid/graphics/RectF;->right:F

    .line 607
    .local v4, "right":F
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v4, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    .line 608
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    iput-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueTouchRect:Landroid/graphics/RectF;

    .line 609
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 610
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 611
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 612
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 613
    return-void
.end method

.method private setupSatValRect()V
    .locals 9

    .line 569
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    .line 570
    .local v0, "dRect":Landroid/graphics/RectF;
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 571
    .local v1, "left":F
    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePanelHeight:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMarginTop:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelSpacing:F

    add-float/2addr v2, v3

    .line 572
    .local v2, "top":F
    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMarginTop:F

    sub-float/2addr v3, v4

    .line 573
    .local v3, "bottom":F
    iget v4, v0, Landroid/graphics/RectF;->right:F

    .line 574
    .local v4, "right":F
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v4, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    .line 575
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    iput-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValTouchRect:Landroid/graphics/RectF;

    .line 576
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget v7, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorWidth:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 577
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget v7, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSVIndicatorWidth:F

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 578
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 579
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 581
    iget v5, v0, Landroid/graphics/RectF;->top:F

    iget v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePanelHeight:F

    add-float/2addr v5, v6

    iget v7, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMarginTop:F

    add-float/2addr v5, v7

    iget v7, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelSpacing:F

    add-float/2addr v5, v7

    .line 582
    .end local v2    # "top":F
    .local v5, "top":F
    add-float/2addr v6, v5

    .line 583
    .end local v3    # "bottom":F
    .local v6, "bottom":F
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v1, v5, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    .line 584
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatTouchRect:Landroid/graphics/RectF;

    .line 585
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatTouchRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 586
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatTouchRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 587
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatTouchRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 588
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatTouchRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 590
    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMarginTop:F

    add-float/2addr v2, v6

    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelSpacing:F

    add-float/2addr v2, v3

    .line 591
    .end local v5    # "top":F
    .restart local v2    # "top":F
    iget v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePanelHeight:F

    add-float/2addr v3, v2

    .line 592
    .end local v6    # "bottom":F
    .restart local v3    # "bottom":F
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v4, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    .line 593
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    iput-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValTouchRect:Landroid/graphics/RectF;

    .line 594
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 595
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 596
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 597
    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValTouchRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 599
    return-void
.end method

.method private updateIndicatorIfNeeded(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 517
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mTouchPoint:Landroid/graphics/Point;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 518
    :cond_0
    const/4 v1, 0x0

    .line 519
    .local v1, "update":Z
    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 520
    .local v0, "startX":I
    iget-object v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mTouchPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    .line 521
    .local v2, "startY":I
    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueTouchRect:Landroid/graphics/RectF;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 523
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-direct {p0, v3}, Lcom/oneplus/settings/ui/ColorPickerView;->pointToHue(F)F

    move-result v3

    iput v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    .line 524
    const/4 v1, 0x1

    goto :goto_1

    .line 525
    :cond_1
    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSatTouchRect:Landroid/graphics/RectF;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 531
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/oneplus/settings/ui/ColorPickerView;->pointToSat(FF)F

    move-result v3

    .line 533
    .local v3, "result":F
    iput v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    .line 534
    const/4 v1, 0x1

    .end local v3    # "result":F
    goto :goto_0

    .line 535
    :cond_2
    iget-object v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mValTouchRect:Landroid/graphics/RectF;

    int-to-float v4, v0

    int-to-float v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 541
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/oneplus/settings/ui/ColorPickerView;->pointToVal(FF)F

    move-result v3

    .line 543
    .restart local v3    # "result":F
    iput v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    .line 544
    const/4 v1, 0x1

    goto :goto_1

    .line 535
    .end local v3    # "result":F
    :cond_3
    :goto_0
    nop

    .line 546
    :goto_1
    return v1
.end method


# virtual methods
.method public getColor()I
    .locals 3

    .line 620
    const/4 v0, 0x3

    new-array v0, v0, [F

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    const/4 v2, 0x1

    aput v1, v0, v2

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    const/4 v2, 0x2

    aput v1, v0, v2

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 185
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    goto :goto_0

    .line 187
    :cond_0
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/ColorPickerView;->drawHuePanel(Landroid/graphics/Canvas;)V

    .line 188
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/ColorPickerView;->drawSatPanel(Landroid/graphics/Canvas;)V

    .line 189
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/ColorPickerView;->drawValPanel(Landroid/graphics/Canvas;)V

    .line 190
    return-void

    .line 185
    :cond_1
    :goto_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 163
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 164
    .local v0, "widthMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 165
    .local v1, "heightMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 166
    .local v2, "widthSpecified":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 168
    .local v3, "heightSpecified":I
    invoke-static {v0}, Lcom/oneplus/settings/ui/ColorPickerView;->isUnspecified(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMinWidth:F

    float-to-int v4, v4

    goto :goto_0

    :cond_0
    move v4, v2

    :goto_0
    move v2, v4

    .line 169
    invoke-static {v0}, Lcom/oneplus/settings/ui/ColorPickerView;->isUnspecified(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMinHeight:F

    float-to-int v4, v4

    goto :goto_1

    :cond_1
    move v4, v3

    :goto_1
    move v3, v4

    .line 176
    invoke-virtual {p0, v2, v3}, Lcom/oneplus/settings/ui/ColorPickerView;->setMeasuredDimension(II)V

    .line 177
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .line 552
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 558
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    .line 559
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPadding:F

    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 560
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    int-to-float v1, p1

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPadding:F

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 561
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPadding:F

    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 562
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    int-to-float v1, p2

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPadding:F

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->getPaddingBottom()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 563
    iget-object v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mDrawingRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mMarginTop:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mPanelSpacing:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHueInidcatorOffset:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHuePanelHeight:F

    .line 564
    invoke-direct {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->setupSatValRect()V

    .line 565
    invoke-direct {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->setupHueRect()V

    .line 566
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 492
    const/4 v0, 0x0

    .line 493
    .local v0, "isUpdated":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v3, :cond_1

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 499
    :cond_0
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/ColorPickerView;->updateIndicatorIfNeeded(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 500
    goto :goto_0

    .line 502
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mTouchPoint:Landroid/graphics/Point;

    .line 503
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/ColorPickerView;->updateIndicatorIfNeeded(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 495
    :cond_2
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v1, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mTouchPoint:Landroid/graphics/Point;

    .line 496
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/ColorPickerView;->updateIndicatorIfNeeded(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 497
    nop

    .line 506
    :goto_0
    if-eqz v0, :cond_4

    .line 507
    iget-object v1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mListener:Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;

    if-eqz v1, :cond_3

    .line 508
    const/4 v4, 0x3

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    aput v6, v4, v5

    iget v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    aput v5, v4, v3

    iget v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    aput v5, v4, v2

    invoke-static {v4}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;->onColorChanged(I)V

    .line 510
    :cond_3
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->invalidate()V

    .line 511
    return v3

    .line 513
    :cond_4
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .line 624
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/oneplus/settings/ui/ColorPickerView;->setColor(IZ)V

    .line 625
    return-void
.end method

.method public setColor(IZ)V
    .locals 7
    .param p1, "color"    # I
    .param p2, "callback"    # Z

    .line 628
    const/4 v0, 0x3

    new-array v1, v0, [F

    .line 629
    .local v1, "hsv":[F
    invoke-static {p1, v1}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 630
    const/4 v2, 0x0

    aget v3, v1, v2

    iput v3, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    .line 631
    const/4 v3, 0x1

    aget v4, v1, v3

    iput v4, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    .line 632
    const/4 v4, 0x2

    aget v5, v1, v4

    iput v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    .line 633
    if-eqz p2, :cond_0

    iget-object v5, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mListener:Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;

    if-eqz v5, :cond_0

    .line 634
    new-array v0, v0, [F

    iget v6, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mHue:F

    aput v6, v0, v2

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mSat:F

    aput v2, v0, v3

    iget v2, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mVal:F

    aput v2, v0, v4

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    invoke-interface {v5, v0}, Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;->onColorChanged(I)V

    .line 636
    :cond_0
    invoke-virtual {p0}, Lcom/oneplus/settings/ui/ColorPickerView;->invalidate()V

    .line 637
    return-void
.end method

.method public setOnColorChangedListener(Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;

    .line 616
    iput-object p1, p0, Lcom/oneplus/settings/ui/ColorPickerView;->mListener:Lcom/oneplus/settings/ui/ColorPickerView$OnColorChangedListener;

    .line 617
    return-void
.end method
