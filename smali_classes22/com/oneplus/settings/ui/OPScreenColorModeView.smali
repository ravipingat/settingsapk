.class public Lcom/oneplus/settings/ui/OPScreenColorModeView;
.super Landroid/widget/FrameLayout;
.source "OPScreenColorModeView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/settings/ui/OPScreenColorModeView$MyOnPageChangeListener;
    }
.end annotation


# static fields
.field private static final LAYOUT_RES_ID:I = 0x7f0d0205

.field private static final LAYOUT_RES_ID_2K:I = 0x7f0d0206


# instance fields
.field private currIndex:I

.field private mContext:Landroid/content/Context;

.field private mPage0:Landroid/widget/ImageView;

.field private mPage1:Landroid/widget/ImageView;

.field private mPage2:Landroid/widget/ImageView;

.field private mViewPager:Landroidx/viewpager/widget/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 44
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->currIndex:I

    .line 46
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPScreenColorModeView;->initView(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->currIndex:I

    .line 40
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPScreenColorModeView;->initView(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->currIndex:I

    .line 34
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPScreenColorModeView;->initView(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/ui/OPScreenColorModeView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPScreenColorModeView;

    .line 16
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/oneplus/settings/ui/OPScreenColorModeView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPScreenColorModeView;

    .line 16
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mPage0:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oneplus/settings/ui/OPScreenColorModeView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPScreenColorModeView;

    .line 16
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mPage1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/oneplus/settings/ui/OPScreenColorModeView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPScreenColorModeView;

    .line 16
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mPage2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/oneplus/settings/ui/OPScreenColorModeView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/ui/OPScreenColorModeView;
    .param p1, "x1"    # I

    .line 16
    iput p1, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->currIndex:I

    return p1
.end method

.method private initView(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 50
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mContext:Landroid/content/Context;

    .line 51
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 52
    .local v0, "wm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 53
    .local v1, "width":I
    const/16 v2, 0x5a0

    if-ne v1, v2, :cond_0

    .line 54
    const v2, 0x7f0d0206

    invoke-direct {p0, v2}, Lcom/oneplus/settings/ui/OPScreenColorModeView;->setLayoutResource(I)V

    goto :goto_0

    .line 56
    :cond_0
    const v2, 0x7f0d0205

    invoke-direct {p0, v2}, Lcom/oneplus/settings/ui/OPScreenColorModeView;->setLayoutResource(I)V

    .line 58
    :goto_0
    invoke-virtual {p0, p0}, Lcom/oneplus/settings/ui/OPScreenColorModeView;->onBindViewHolder(Landroid/view/View;)V

    .line 59
    return-void
.end method

.method private setLayoutResource(I)V
    .locals 2
    .param p1, "layoutResId"    # I

    .line 62
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 63
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 64
    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/view/View;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;

    .line 68
    const v0, 0x7f0a0772

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    .line 69
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    new-instance v1, Lcom/oneplus/settings/ui/OPScreenColorModeView$MyOnPageChangeListener;

    invoke-direct {v1, p0}, Lcom/oneplus/settings/ui/OPScreenColorModeView$MyOnPageChangeListener;-><init>(Lcom/oneplus/settings/ui/OPScreenColorModeView;)V

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 70
    const v0, 0x7f0a04f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mPage0:Landroid/widget/ImageView;

    .line 71
    const v0, 0x7f0a04fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mPage1:Landroid/widget/ImageView;

    .line 72
    const v0, 0x7f0a04fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mPage2:Landroid/widget/ImageView;

    .line 74
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 75
    .local v0, "mLi":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    const v2, 0x7f0d0208

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 76
    .local v3, "view1":Landroid/view/View;
    const v4, 0x7f0a0302

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 77
    .local v5, "mImageView1":Landroid/widget/ImageView;
    const v6, 0x7f08059a

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 79
    .local v6, "view2":Landroid/view/View;
    invoke-virtual {v6, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 80
    .local v7, "mImageView2":Landroid/widget/ImageView;
    const v8, 0x7f08059b

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 82
    .local v1, "view3":Landroid/view/View;
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 83
    .local v2, "mImageView3":Landroid/widget/ImageView;
    const v4, 0x7f08059c

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 85
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v4, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v8, Lcom/oneplus/settings/ui/OPScreenColorModeView$1;

    invoke-direct {v8, p0, v4}, Lcom/oneplus/settings/ui/OPScreenColorModeView$1;-><init>(Lcom/oneplus/settings/ui/OPScreenColorModeView;Ljava/util/ArrayList;)V

    .line 114
    .local v8, "mPagerAdapter":Landroidx/viewpager/widget/PagerAdapter;
    iget-object v9, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v9, v8}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 116
    iget-object v9, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    iget v10, p0, Lcom/oneplus/settings/ui/OPScreenColorModeView;->currIndex:I

    invoke-virtual {v9, v10}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 118
    return-void
.end method
