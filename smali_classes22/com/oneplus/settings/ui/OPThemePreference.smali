.class public Lcom/oneplus/settings/ui/OPThemePreference;
.super Landroidx/preference/Preference;
.source "OPThemePreference.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mThemeIcon1:Landroid/widget/ImageView;

.field private mThemeIcon2:Landroid/widget/ImageView;

.field private mThemeIcon3:Landroid/widget/ImageView;

.field private mThemeIcon4:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 30
    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const v0, 0x7f0d022c

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/ui/OPThemePreference;->setLayoutResource(I)V

    .line 32
    iput-object p1, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "packagename"    # Ljava/lang/String;

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 150
    .local v0, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 151
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAppIcon e = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " packagename = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OPThemePreference"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method private getColorThmeImage()I
    .locals 3

    .line 72
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "oem_black_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 74
    .local v0, "style":I
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 85
    const v1, 0x7f08053c

    .local v1, "imageId":I
    goto :goto_0

    .line 82
    .end local v1    # "imageId":I
    :cond_0
    const v1, 0x7f080536

    .line 83
    .restart local v1    # "imageId":I
    goto :goto_0

    .line 79
    .end local v1    # "imageId":I
    :cond_1
    const v1, 0x7f080539

    .line 80
    .restart local v1    # "imageId":I
    goto :goto_0

    .line 76
    .end local v1    # "imageId":I
    :cond_2
    const v1, 0x7f08053c

    .line 77
    .restart local v1    # "imageId":I
    nop

    .line 88
    :goto_0
    return v1
.end method

.method private initAppIconView(Landroidx/preference/PreferenceViewHolder;)V
    .locals 6
    .param p1, "view"    # Landroidx/preference/PreferenceViewHolder;

    .line 158
    const v0, 0x7f0a06de

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 159
    .local v0, "iconpack":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    const v1, 0x7f0a06da

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon1:Landroid/widget/ImageView;

    .line 161
    const v1, 0x7f0a06db

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon2:Landroid/widget/ImageView;

    .line 162
    const v1, 0x7f0a06dc

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon3:Landroid/widget/ImageView;

    .line 163
    const v1, 0x7f0a06dd

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon4:Landroid/widget/ImageView;

    .line 164
    const-string v1, "com.android.dialer"

    invoke-direct {p0, v1}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 165
    .local v1, "drawable1":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 166
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon1:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 168
    :cond_0
    const-string v2, "com.oneplus.mms"

    invoke-direct {p0, v2}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 169
    .local v2, "drawable2":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_1

    .line 170
    iget-object v3, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon2:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 172
    :cond_1
    const-string v3, "com.oneplus.deskclock"

    invoke-direct {p0, v3}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 173
    .local v3, "drawable3":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_2

    .line 174
    iget-object v4, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon3:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 176
    :cond_2
    const-string v4, "com.oneplus.camera"

    invoke-direct {p0, v4}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 177
    .local v4, "drawable4":Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_3

    .line 178
    iget-object v5, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon4:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 180
    :cond_3
    return-void
.end method


# virtual methods
.method public initViewStatus(Landroidx/preference/PreferenceViewHolder;)V
    .locals 6
    .param p1, "v"    # Landroidx/preference/PreferenceViewHolder;

    .line 92
    const v0, 0x7f0a06ce

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "checkbox":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 94
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 95
    const v2, 0x7f0a06cf

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 96
    .local v2, "radiobutton":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 97
    invoke-virtual {v2, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 98
    const v3, 0x7f0a06d1

    invoke-virtual {p1, v3}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 99
    .local v3, "opswitch":Landroid/view/View;
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 100
    invoke-virtual {v3, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 101
    const v4, 0x7f0a06d0

    invoke-virtual {p1, v4}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 102
    .local v4, "seekbar":Landroid/view/View;
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 103
    invoke-virtual {v4, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 104
    const v5, 0x7f0a06cd

    invoke-virtual {p1, v5}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 105
    .local v5, "button":Landroid/view/View;
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 106
    invoke-virtual {v5, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 107
    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 7
    .param p1, "view"    # Landroidx/preference/PreferenceViewHolder;

    .line 36
    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 37
    nop

    .line 38
    const v0, 0x7f0a06cc

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 39
    .local v0, "accentColor":Landroid/view/ViewGroup;
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 52
    const v1, 0x7f0a06e5

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/oneplus/settings/ui/RadiusImageView;

    .line 53
    .local v1, "themeTone":Lcom/oneplus/settings/ui/RadiusImageView;
    invoke-direct {p0}, Lcom/oneplus/settings/ui/OPThemePreference;->getColorThmeImage()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/oneplus/settings/ui/RadiusImageView;->setImageResource(I)V

    .line 54
    invoke-virtual {v1, p0}, Lcom/oneplus/settings/ui/RadiusImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v2, 0x7f0a06e0

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 56
    .local v2, "themeShape":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    const v3, 0x7f0a06d8

    invoke-virtual {p1, v3}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 58
    .local v3, "fontstyle":Landroid/view/View;
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportFontStyleSetting()Z

    move-result v4

    if-nez v4, :cond_0

    .line 60
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 61
    const v5, 0x7f0a0281

    invoke-virtual {p1, v5}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 62
    const v5, 0x7f0a06d7

    invoke-virtual {p1, v5}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 63
    .local v5, "container1":Landroid/view/View;
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 64
    const v6, 0x7f0a06d9

    invoke-virtual {p1, v6}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 65
    .local v6, "container2":Landroid/view/View;
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    .line 67
    .end local v5    # "container1":Landroid/view/View;
    .end local v6    # "container2":Landroid/view/View;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/oneplus/settings/ui/OPThemePreference;->initViewStatus(Landroidx/preference/PreferenceViewHolder;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/oneplus/settings/ui/OPThemePreference;->initAppIconView(Landroidx/preference/PreferenceViewHolder;)V

    .line 69
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .line 118
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 119
    .local v0, "id":I
    const v1, 0x7f0a06cc

    if-ne v0, v1, :cond_0

    .line 120
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 121
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    const-class v3, Lcom/oneplus/settings/OPColorPickerActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 122
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 123
    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_1

    :cond_0
    const v1, 0x7f0a06de

    if-ne v0, v1, :cond_1

    .line 124
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 125
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "net.oneplus.launcher"

    const-string v3, "net.oneplus.launcher.IconPackSelectorActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 128
    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_1

    :cond_1
    const v1, 0x7f0a06e5

    const/high16 v2, 0x10000000

    const-string v3, "com.android.settings"

    if-ne v0, v1, :cond_2

    .line 129
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 130
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v4, "com.android.settings.Settings$OPCustomToneSettingsActivity"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 133
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 134
    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_1

    :cond_2
    const v1, 0x7f0a06e0

    if-ne v0, v1, :cond_3

    .line 135
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 136
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v4, "com.android.settings.Settings$OPCustomShapeSettingsActivity"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 139
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 140
    :cond_3
    const v1, 0x7f0a06d8

    if-ne v0, v1, :cond_4

    .line 141
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 142
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    const-class v3, Lcom/oneplus/settings/OPFontStyleActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 143
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 140
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_4
    :goto_0
    nop

    .line 145
    :goto_1
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public refreshUI()V
    .locals 5

    .line 183
    iget-object v0, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon1:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon2:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon3:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon4:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    goto :goto_0

    .line 186
    :cond_0
    const-string v0, "com.android.dialer"

    invoke-direct {p0, v0}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 187
    .local v0, "drawable1":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1

    .line 188
    iget-object v1, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon1:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    :cond_1
    const-string v1, "com.oneplus.mms"

    invoke-direct {p0, v1}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 191
    .local v1, "drawable2":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_2

    .line 192
    iget-object v2, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon2:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    :cond_2
    const-string v2, "com.oneplus.deskclock"

    invoke-direct {p0, v2}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 195
    .local v2, "drawable3":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_3

    .line 196
    iget-object v3, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon3:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 198
    :cond_3
    const-string v3, "com.oneplus.camera"

    invoke-direct {p0, v3}, Lcom/oneplus/settings/ui/OPThemePreference;->getAppIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 199
    .local v3, "drawable4":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_4

    .line 200
    iget-object v4, p0, Lcom/oneplus/settings/ui/OPThemePreference;->mThemeIcon4:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 202
    :cond_4
    return-void

    .line 184
    .end local v0    # "drawable1":Landroid/graphics/drawable/Drawable;
    .end local v1    # "drawable2":Landroid/graphics/drawable/Drawable;
    .end local v2    # "drawable3":Landroid/graphics/drawable/Drawable;
    .end local v3    # "drawable4":Landroid/graphics/drawable/Drawable;
    :cond_5
    :goto_0
    return-void
.end method
