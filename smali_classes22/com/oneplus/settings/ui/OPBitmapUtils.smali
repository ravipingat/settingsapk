.class public Lcom/oneplus/settings/ui/OPBitmapUtils;
.super Ljava/lang/Object;
.source "OPBitmapUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "OPBitmapUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBitmapFromDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 40
    const/4 v0, 0x0

    .line 41
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    instance-of v1, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 42
    move-object v1, p0

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 43
    :cond_0
    if-eqz p0, :cond_1

    .line 44
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 45
    .local v1, "width":I
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 47
    .local v2, "height":I
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v0, v3

    .line 48
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 49
    .local v3, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p0, v6, v6, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 50
    invoke-virtual {p0, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    goto :goto_0

    .line 51
    :catch_0
    move-exception v3

    .line 52
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    const-string v4, "OPBitmapUtils"

    const-string v5, "the set icon is too large"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    .end local v1    # "width":I
    .end local v2    # "height":I
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    :cond_1
    :goto_0
    return-object v0
.end method

.method public static getRotationFromExif(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .line 59
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1, p0, p1}, Lcom/oneplus/settings/ui/OPBitmapUtils;->getRotationFromExifHelper(Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method private static getRotationFromExifHelper(Landroid/content/res/Resources;ILandroid/content/Context;Landroid/net/Uri;)I
    .locals 8
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "resId"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "uri"    # Landroid/net/Uri;

    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "is":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 65
    .local v1, "bis":Ljava/io/BufferedInputStream;
    const/4 v2, 0x0

    .line 67
    .local v2, "degree":I
    const-string v3, "Error closing input stream(s)"

    const-string v4, "OPBitmapUtils"

    if-eqz p3, :cond_0

    .line 68
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    move-object v0, v5

    .line 69
    if-eqz v0, :cond_1

    .line 70
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v1, v5

    goto :goto_0

    .line 73
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v5

    move-object v0, v5

    .line 74
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v1, v5

    .line 77
    :cond_1
    :goto_0
    new-instance v5, Landroid/media/ExifInterface;

    invoke-direct {v5, v1}, Landroid/media/ExifInterface;-><init>(Ljava/io/InputStream;)V

    .line 78
    .local v5, "ei":Landroid/media/ExifInterface;
    const-string v6, "Orientation"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    .local v6, "orientation":I
    const/4 v7, 0x3

    if-eq v6, v7, :cond_4

    const/4 v7, 0x6

    if-eq v6, v7, :cond_3

    const/16 v7, 0x8

    if-eq v6, v7, :cond_2

    .line 91
    const/4 v2, 0x0

    goto :goto_1

    .line 88
    :cond_2
    const/16 v2, 0x10e

    .line 89
    goto :goto_1

    .line 82
    :cond_3
    const/16 v2, 0x5a

    .line 83
    goto :goto_1

    .line 85
    :cond_4
    const/16 v2, 0xb4

    .line 86
    nop

    .line 97
    .end local v5    # "ei":Landroid/media/ExifInterface;
    .end local v6    # "orientation":I
    :goto_1
    if-eqz v1, :cond_5

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_2

    .line 99
    :catch_0
    move-exception v5

    goto :goto_3

    .line 98
    :cond_5
    :goto_2
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 100
    .local v5, "e":Ljava/io/IOException;
    :goto_3
    invoke-static {v4, v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 102
    .end local v5    # "e":Ljava/io/IOException;
    goto :goto_5

    .line 101
    :cond_6
    :goto_4
    goto :goto_5

    .line 96
    :catchall_0
    move-exception v5

    goto :goto_6

    .line 93
    :catch_1
    move-exception v5

    .line 94
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Getting exif data failed, resId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ", uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97
    .end local v5    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_7

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 98
    :cond_7
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    .line 103
    :goto_5
    return v2

    .line 97
    :goto_6
    if-eqz v1, :cond_8

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_7

    .line 99
    :catch_2
    move-exception v6

    goto :goto_8

    .line 98
    :cond_8
    :goto_7
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_9

    .line 100
    .local v6, "e":Ljava/io/IOException;
    :goto_8
    invoke-static {v4, v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    .line 101
    .end local v6    # "e":Ljava/io/IOException;
    :cond_9
    :goto_9
    nop

    :goto_a
    throw v5
.end method

.method public static rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "source"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I
    .param p2, "isFlip"    # Z

    .line 107
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 108
    return-object p0

    .line 111
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 112
    .local v7, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 113
    .local v8, "height":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rotate: degrees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isFlip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OPBitmapUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    move-object v9, v0

    .line 116
    .local v9, "matrix":Landroid/graphics/Matrix;
    if-eqz p2, :cond_1

    .line 117
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 119
    :cond_1
    int-to-float v0, p1

    div-int/lit8 v1, v7, 0x2

    int-to-float v1, v1

    div-int/lit8 v2, v8, 0x2

    int-to-float v2, v2

    invoke-virtual {v9, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 121
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move v3, v7

    move v4, v8

    move-object v5, v9

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static toBytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B
    .locals 5
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p2, "quality"    # I

    .line 125
    const/4 v0, 0x0

    .line 126
    .local v0, "bytes":[B
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    invoke-virtual {p0, p1, p2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 128
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 129
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 132
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    nop

    .line 133
    return-object v0

    .line 126
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    :catchall_0
    move-exception v2

    .end local v0    # "bytes":[B
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    .end local p1    # "format":Landroid/graphics/Bitmap$CompressFormat;
    .end local p2    # "quality":I
    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 129
    .restart local v0    # "bytes":[B
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p1    # "format":Landroid/graphics/Bitmap$CompressFormat;
    .restart local p2    # "quality":I
    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v4

    :try_start_5
    invoke-virtual {v2, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "bytes":[B
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    .end local p1    # "format":Landroid/graphics/Bitmap$CompressFormat;
    .end local p2    # "quality":I
    :goto_0
    throw v3
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "bytes":[B
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p1    # "format":Landroid/graphics/Bitmap$CompressFormat;
    .restart local p2    # "quality":I
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "OPBitmapUtils"

    const-string v3, "failed to create wallpaper bytes"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    return-object v0
.end method
