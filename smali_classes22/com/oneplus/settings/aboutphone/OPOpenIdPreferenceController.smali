.class public Lcom/oneplus/settings/aboutphone/OPOpenIdPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPOpenIdPreferenceController.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/oneplus/settings/aboutphone/OPOpenIdPreferenceController;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 0
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 29
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 30
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPOpenIdPreferenceController;->mContext:Landroid/content/Context;

    const-string v1, "com.heytap.openid"

    invoke-static {v0, v1}, Lcom/oneplus/settings/utils/OPUtils;->isAppExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 4

    .line 35
    const-string v0, ""

    .line 37
    .local v0, "id":Ljava/lang/String;
    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    const-string v3, "OUID"

    invoke-static {v1, v2, v3}, Lcom/oneplus/android/openid/OpOpenIdManagerInjector;->getOpenId(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 40
    goto :goto_0

    .line 38
    :catch_0
    move-exception v1

    .line 39
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 41
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-object v0
.end method
