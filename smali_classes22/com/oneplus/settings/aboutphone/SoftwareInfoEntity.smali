.class public Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;
.super Ljava/lang/Object;
.source "SoftwareInfoEntity.java"


# instance fields
.field private intent:Ljava/lang/String;

.field private resIcon:I

.field private summary:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIntent()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->intent:Ljava/lang/String;

    return-object v0
.end method

.method public getResIcon()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->resIcon:I

    return v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setIntent(Ljava/lang/String;)V
    .locals 0
    .param p1, "intent"    # Ljava/lang/String;

    .line 44
    iput-object p1, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->intent:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setResIcon(I)V
    .locals 0
    .param p1, "resIcon"    # I

    .line 36
    iput p1, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->resIcon:I

    .line 37
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 0
    .param p1, "summary"    # Ljava/lang/String;

    .line 28
    iput-object p1, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->summary:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;->title:Ljava/lang/String;

    .line 21
    return-void
.end method
