.class public Lcom/oneplus/settings/aboutphone/OPAboutPhone;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "OPAboutPhone.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/oneplus/settings/aboutphone/Contract$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/settings/aboutphone/OPAboutPhone$SummaryProvider;
    }
.end annotation


# static fields
.field private static final KEY_HARDWARE_VIEW:Ljava/lang/String; = "hardware_view"

.field private static final KEY_SOFT_VIEW_1:Ljava/lang/String; = "soft_view"

.field static final REQUEST_CONFIRM_PASSWORD_FOR_DEV_PREF:I = 0x64

.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/SummaryLoader$SummaryProviderFactory;

.field private static final TAG:Ljava/lang/String; = "OPAboutPhone"


# instance fields
.field private mAdapter:Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;

.field private mContext:Landroid/content/Context;

.field private mCurrentClickView:Landroid/view/View;

.field private mDevHitToast:Landroid/widget/Toast;

.field private mPresenter:Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;

.field private mRecyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 181
    sget-object v0, Lcom/oneplus/settings/aboutphone/-$$Lambda$OPAboutPhone$RKK-fwpKlUC1qzXgjTLs813iYKo;->INSTANCE:Lcom/oneplus/settings/aboutphone/-$$Lambda$OPAboutPhone$RKK-fwpKlUC1qzXgjTLs813iYKo;

    sput-object v0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/SummaryLoader$SummaryProviderFactory;

    .line 185
    new-instance v0, Lcom/oneplus/settings/aboutphone/OPAboutPhone$3;

    invoke-direct {v0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone$3;-><init>()V

    sput-object v0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/oneplus/settings/aboutphone/OPAboutPhone;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/aboutphone/OPAboutPhone;
    .param p1, "x1"    # Landroid/view/View;

    .line 29
    iput-object p1, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mCurrentClickView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100(Lcom/oneplus/settings/aboutphone/OPAboutPhone;)Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/aboutphone/OPAboutPhone;

    .line 29
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mPresenter:Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oneplus/settings/aboutphone/OPAboutPhone;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/aboutphone/OPAboutPhone;

    .line 29
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private initView()V
    .locals 5

    .line 90
    nop

    .line 91
    invoke-virtual {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "soft_view"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPLayoutPreference;

    .line 92
    .local v0, "SoftWare1Preference":Lcom/oneplus/settings/ui/OPLayoutPreference;
    const v1, 0x7f0a052a

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/ui/OPLayoutPreference;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 93
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0a058d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v2, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 94
    new-instance v2, Landroidx/recyclerview/widget/GridLayoutManager;

    iget-object v3, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mContext:Landroid/content/Context;

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 95
    .local v2, "manager":Landroidx/recyclerview/widget/GridLayoutManager;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroidx/recyclerview/widget/GridLayoutManager;->setOrientation(I)V

    .line 96
    iget-object v3, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 98
    iget-object v3, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v4, Lcom/oneplus/settings/aboutphone/OPAboutPhone$2;

    invoke-direct {v4, p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone$2;-><init>(Lcom/oneplus/settings/aboutphone/OPAboutPhone;)V

    invoke-virtual {v3, v4}, Landroidx/recyclerview/widget/RecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 105
    return-void
.end method

.method static synthetic lambda$static$0(Landroid/app/Activity;Lcom/android/settings/dashboard/SummaryLoader;)Lcom/android/settings/dashboard/SummaryLoader$SummaryProvider;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "summaryLoader"    # Lcom/android/settings/dashboard/SummaryLoader;

    .line 182
    new-instance v0, Lcom/oneplus/settings/aboutphone/OPAboutPhone$SummaryProvider;

    invoke-direct {v0, p1}, Lcom/oneplus/settings/aboutphone/OPAboutPhone$SummaryProvider;-><init>(Lcom/android/settings/dashboard/SummaryLoader;)V

    return-object v0
.end method


# virtual methods
.method public cancelToast()V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mDevHitToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 130
    :cond_0
    return-void
.end method

.method public displayHardWarePreference(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "phoneImageResId"    # I
    .param p2, "cameraInfo"    # Ljava/lang/String;
    .param p3, "cpuName"    # Ljava/lang/String;
    .param p4, "screenInfo"    # Ljava/lang/String;
    .param p5, "totalMemory"    # Ljava/lang/String;

    .line 60
    nop

    .line 61
    invoke-virtual {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "hardware_view"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPLayoutPreference;

    .line 62
    .local v0, "HardWarePreference":Lcom/oneplus/settings/ui/OPLayoutPreference;
    const v1, 0x7f0a0528

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/ui/OPLayoutPreference;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 63
    .local v1, "hardwareinfoView":Landroid/view/View;
    nop

    .line 64
    invoke-virtual {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, p0, v1}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->newInstance(Landroid/app/Activity;Landroidx/fragment/app/Fragment;Landroid/view/View;)Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;

    move-result-object v2

    .line 65
    .local v2, "mOPAboutPhoneHardWareController":Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;
    iget-object v3, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->setPhoneImage(Landroid/graphics/drawable/Drawable;)Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->done()Landroid/view/View;

    .line 66
    invoke-virtual {v2, p2}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->setCameraMessage(Ljava/lang/String;)Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;

    .line 67
    invoke-virtual {v2, p3}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->setCpuMessage(Ljava/lang/String;)Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;

    .line 68
    invoke-virtual {v2, p4}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->setScreenMessage(Ljava/lang/String;)Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;

    .line 69
    invoke-static {p5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    .line 70
    .local v3, "ramsize":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "GB RAM + "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/oneplus/settings/utils/OPUtils;->showROMStorage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " ROM"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->setStorageMessage(Ljava/lang/String;)Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;

    .line 71
    invoke-virtual {v2}, Lcom/oneplus/settings/aboutphone/OPAboutPhoneHardWareController;->done()Landroid/view/View;

    .line 72
    return-void
.end method

.method public displaySoftWarePreference(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;",
            ">;)V"
        }
    .end annotation

    .line 76
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/oneplus/settings/aboutphone/SoftwareInfoEntity;>;"
    new-instance v0, Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;

    iget-object v1, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mAdapter:Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;

    .line 77
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mAdapter:Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 78
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mAdapter:Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;

    invoke-virtual {v0}, Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;->notifyDataSetChanged()V

    .line 80
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mAdapter:Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;

    new-instance v1, Lcom/oneplus/settings/aboutphone/OPAboutPhone$1;

    invoke-direct {v1, p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone$1;-><init>(Lcom/oneplus/settings/aboutphone/OPAboutPhone;)V

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter;->setOnItemClickListener(Lcom/oneplus/settings/aboutphone/SoftwareInfoAdapter$OnItemClickListener;)V

    .line 87
    return-void
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 157
    const-string v0, "OPAboutPhone"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 162
    const/16 v0, 0x270f

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 45
    const v0, 0x7f160081

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 139
    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    .line 140
    return-void

    .line 142
    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mPresenter:Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;

    invoke-virtual {v0}, Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;->enableDevelopmentSettings()V

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mPresenter:Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;->mProcessingLastDevHit:Z

    .line 146
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .line 50
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mContext:Landroid/content/Context;

    .line 53
    invoke-direct {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->initView()V

    .line 54
    new-instance v0, Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;

    invoke-virtual {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;-><init>(Landroid/app/Activity;Landroidx/fragment/app/Fragment;Lcom/oneplus/settings/aboutphone/Contract$View;)V

    iput-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mPresenter:Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;

    .line 55
    return-void
.end method

.method public onResume()V
    .locals 1

    .line 151
    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    .line 152
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mPresenter:Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;

    invoke-virtual {v0}, Lcom/oneplus/settings/aboutphone/AboutPhonePresenter;->onResume()V

    .line 153
    return-void
.end method

.method public performHapticFeedback()V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mCurrentClickView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 110
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 112
    :cond_0
    return-void
.end method

.method public showLongToast(I)V
    .locals 1
    .param p1, "resId"    # I

    .line 116
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->showLongToast(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public showLongToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .line 121
    invoke-virtual {p0}, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mDevHitToast:Landroid/widget/Toast;

    .line 122
    iget-object v0, p0, Lcom/oneplus/settings/aboutphone/OPAboutPhone;->mDevHitToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 123
    return-void
.end method
