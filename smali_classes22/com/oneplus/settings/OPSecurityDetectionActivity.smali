.class public Lcom/oneplus/settings/OPSecurityDetectionActivity;
.super Lcom/oneplus/settings/BaseActivity;
.source "OPSecurityDetectionActivity.java"


# static fields
.field private static final KEY_SECURITY_DETECTION_TYPE:I = 0xb

.field static final LEGAL_OPPO_URL:Ljava/lang/String; = "https://www.oppo.com/cn/service/help/640?id=640&name=%E6%9C%8D%E5%8A%A1%E6%94%BF%E7%AD%96&hdscreen=1"

.field static final LEGAL_TENCENT_URL:Ljava/lang/String; = "http://www.qq.com/privacy.htm"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/oneplus/settings/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/OPSecurityDetectionActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/OPSecurityDetectionActivity;

    .line 17
    iget-object v0, p0, Lcom/oneplus/settings/OPSecurityDetectionActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 26
    invoke-super {p0, p1}, Lcom/oneplus/settings/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isO2()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->finish()V

    .line 33
    :cond_0
    iput-object p0, p0, Lcom/oneplus/settings/OPSecurityDetectionActivity;->mContext:Landroid/content/Context;

    .line 34
    const v0, 0x7f0d0163

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->setContentView(I)V

    .line 35
    const v0, 0x7f0a06ba

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 36
    .local v0, "tencentLegal":Landroid/widget/TextView;
    const v1, 0x7f0a04ee

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 37
    .local v1, "oppoLegal":Landroid/widget/TextView;
    const v2, 0x7f120fff

    invoke-virtual {p0, v2}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "linkFrontContent":Ljava/lang/String;
    const-string v3, "http://www.qq.com/privacy.htm"

    .line 39
    .local v3, "linkUrl":Ljava/lang/String;
    const v4, 0x7f121000

    invoke-virtual {p0, v4}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "linkTitle":Ljava/lang/String;
    const-string v5, ""

    invoke-static {v2, v3, v4, v5}, Lcom/oneplus/settings/utils/OPUtils;->parseLink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 43
    const v6, 0x7f120ffd

    invoke-virtual {p0, v6}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 44
    .local v6, "linkFrontContent2":Ljava/lang/String;
    const-string v7, "https://www.oppo.com/cn/service/help/640?id=640&name=%E6%9C%8D%E5%8A%A1%E6%94%BF%E7%AD%96&hdscreen=1"

    .line 45
    .local v7, "linkUrl2":Ljava/lang/String;
    const v8, 0x7f120ffe

    invoke-virtual {p0, v8}, Lcom/oneplus/settings/OPSecurityDetectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 46
    .local v8, "linkTitle2":Ljava/lang/String;
    new-instance v9, Lcom/oneplus/settings/OPSecurityDetectionActivity$1;

    invoke-direct {v9, p0}, Lcom/oneplus/settings/OPSecurityDetectionActivity$1;-><init>(Lcom/oneplus/settings/OPSecurityDetectionActivity;)V

    invoke-static {v6, v8, v5, v9}, Lcom/oneplus/settings/utils/OPUtils;->parseLinkLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/text/style/ClickableSpan;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 53
    return-void
.end method
