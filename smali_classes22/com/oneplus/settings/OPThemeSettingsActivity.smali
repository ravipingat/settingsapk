.class public Lcom/oneplus/settings/OPThemeSettingsActivity;
.super Landroid/app/Activity;
.source "OPThemeSettingsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private setupPreferencesScreen()V
    .locals 0

    .line 26
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 12
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 13
    const v0, 0x7f0d022e

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPThemeSettingsActivity;->setContentView(I)V

    .line 14
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 18
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-direct {p0}, Lcom/oneplus/settings/OPThemeSettingsActivity;->setupPreferencesScreen()V

    .line 20
    return-void
.end method
