.class public Lcom/oneplus/settings/OneplusColorManager;
.super Ljava/lang/Object;
.source "OneplusColorManager.java"


# static fields
.field private static final OP_DCIP3_MODE_LEVEL:I = 0x8

.field private static final OP_DEFAULT_MODE_LEVEL:I = 0x0

.field private static final OP_REVERT_STATUS:I = 0xc

.field private static final OP_SRGB_MODE_LEVEL:I = 0x7

.field private static final OP_SYS_DCIP3_PROPERTY:Ljava/lang/String; = "sys.dci3p"

.field private static final OP_SYS_SRGB_PROPERTY:Ljava/lang/String; = "sys.srgb"

.field private static final TAG:Ljava/lang/String; = "OneplusColorManager"

.field private static mOneplusColorManager:Lcom/oneplus/settings/OneplusColorManager;


# instance fields
.field private isSupportReadingMode:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/oneplus/settings/OneplusColorManager;->mContext:Landroid/content/Context;

    .line 40
    iget-object v0, p0, Lcom/oneplus/settings/OneplusColorManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "oem.read_mode.support"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/oneplus/settings/OneplusColorManager;->isSupportReadingMode:Z

    .line 44
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/oneplus/settings/OneplusColorManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 32
    sget-object v0, Lcom/oneplus/settings/OneplusColorManager;->mOneplusColorManager:Lcom/oneplus/settings/OneplusColorManager;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/oneplus/settings/OneplusColorManager;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/OneplusColorManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/oneplus/settings/OneplusColorManager;->mOneplusColorManager:Lcom/oneplus/settings/OneplusColorManager;

    .line 35
    :cond_0
    sget-object v0, Lcom/oneplus/settings/OneplusColorManager;->mOneplusColorManager:Lcom/oneplus/settings/OneplusColorManager;

    return-object v0
.end method

.method private saveColorManagerMode()V
    .locals 0

    .line 74
    return-void
.end method


# virtual methods
.method public closeDciP3()V
    .locals 2

    .line 118
    const-string v0, "sys.dci3p"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public closesRGB()V
    .locals 2

    .line 101
    const-string v0, "sys.srgb"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public releaseColorManager()V
    .locals 0

    .line 160
    return-void
.end method

.method public resetScreenBetterDisplay()V
    .locals 0

    .line 143
    return-void
.end method

.method public restoreScreenBetterDisplay()V
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/oneplus/settings/OneplusColorManager;->saveColorManagerMode()V

    .line 89
    return-void
.end method

.method public revertStatus()V
    .locals 0

    .line 157
    return-void
.end method

.method public saveScreenBetter()V
    .locals 0

    .line 76
    invoke-direct {p0}, Lcom/oneplus/settings/OneplusColorManager;->saveColorManagerMode()V

    .line 77
    return-void
.end method

.method public setActiveMode(I)V
    .locals 0
    .param p1, "value"    # I

    .line 150
    return-void
.end method

.method public setActivetNightMode()V
    .locals 2

    .line 56
    const-string v0, "sys.srgb"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public setColorBalance(I)V
    .locals 0
    .param p1, "value"    # I

    .line 68
    return-void
.end method

.method public setDciP3()V
    .locals 2

    .line 114
    const-string v0, "sys.dci3p"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method public setNightModeLevel(I)V
    .locals 0
    .param p1, "value"    # I

    .line 130
    return-void
.end method

.method public setNotActivetNightMode()V
    .locals 2

    .line 47
    const-string v0, "sys.srgb"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public setsRGB()V
    .locals 2

    .line 97
    const-string v0, "sys.srgb"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    return-void
.end method
