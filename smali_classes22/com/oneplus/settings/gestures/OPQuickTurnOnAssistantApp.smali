.class public Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "OPQuickTurnOnAssistantApp.java"

# interfaces
.implements Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;


# static fields
.field private static final CAMERA_VALUE:I = 0x0

.field private static final KEY_CAMERA:Ljava/lang/String; = "camera"

.field private static final KEY_WALLET:Ljava/lang/String; = "wallet"

.field private static final OPEN_WALLET_CP_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "OPQuickTurnOnAssistantApp"

.field private static final WALLET_VALUE:I = 0x1


# instance fields
.field private mCamera:Lcom/android/settings/ui/RadioButtonPreference;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mOPQuickTurnOnAssistantAppSwitchPreferenceController:Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;

.field private mSwitchBar:Lcom/android/settings/widget/SwitchBar;

.field private mWallet:Lcom/android/settings/ui/RadioButtonPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    const-string v0, "content://finshell.wallet.quickstart.flag.provider.open/CARD_BAG_FLAG"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->OPEN_WALLET_CP_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 21
    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    .line 84
    new-instance v0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp$1;-><init>(Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContentObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;)V
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;

    .line 21
    invoke-direct {p0}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->updateQuickTurnOnAssistantAppEnabledStatus()V

    return-void
.end method

.method public static checkSupportWalletFlag(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .line 138
    const-string v0, "HAS_ACTIVE_CARD"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 141
    .local v7, "c":Landroid/database/Cursor;
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->OPEN_WALLET_CP_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v3, v0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v7, v1

    .line 142
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 143
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 144
    aget-object v1, v0, v8

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "support":Ljava/lang/String;
    const-string v2, "true"

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    nop

    .line 151
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 152
    const/4 v3, 0x0

    .line 145
    .end local v7    # "c":Landroid/database/Cursor;
    .local v3, "c":Landroid/database/Cursor;
    return v2

    .line 150
    .end local v1    # "support":Ljava/lang/String;
    .end local v3    # "c":Landroid/database/Cursor;
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_0
    if-eqz v7, :cond_1

    .line 151
    goto :goto_0

    .line 150
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 147
    :catch_0
    move-exception v1

    .line 148
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v7, :cond_1

    .line 151
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 152
    const/4 v7, 0x0

    .line 155
    :cond_1
    return v8

    .line 150
    :goto_1
    if-eqz v7, :cond_2

    .line 151
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 152
    const/4 v7, 0x0

    :cond_2
    throw v1
.end method

.method private updateQuickTurnOnAssistantAppEnabledStatus()V
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "camera_double_tap_power_gesture_disabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 101
    .local v0, "isChecked":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 102
    iget-object v3, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mCamera:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    .line 103
    iget-object v3, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->checkSupportWalletFlag(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 104
    iget-object v1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mWallet:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    goto :goto_1

    .line 106
    :cond_1
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mWallet:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v2, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    goto :goto_1

    .line 109
    :cond_2
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mCamera:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v2, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    .line 110
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mWallet:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v2, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    .line 112
    :goto_1
    return-void
.end method

.method private updateQuickTurnOnAssistantAppSelectStatus(I)V
    .locals 4
    .param p1, "value"    # I

    .line 95
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mCamera:Lcom/android/settings/ui/RadioButtonPreference;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p1, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    invoke-virtual {v0, v3}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 96
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mWallet:Lcom/android/settings/ui/RadioButtonPreference;

    if-ne p1, v2, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setChecked(Z)V

    .line 97
    return-void
.end method


# virtual methods
.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 129
    const-string v0, "OPQuickTurnOnAssistantApp"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 134
    const/16 v0, 0x270f

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 56
    const v0, 0x7f1600aa

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 37
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SettingsActivity;

    .line 39
    .local v0, "activity":Lcom/android/settings/SettingsActivity;
    invoke-virtual {v0}, Lcom/android/settings/SettingsActivity;->getSwitchBar()Lcom/android/settings/widget/SwitchBar;

    move-result-object v1

    iput-object v1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mSwitchBar:Lcom/android/settings/widget/SwitchBar;

    .line 40
    new-instance v1, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;

    new-instance v2, Lcom/android/settings/widget/SwitchBarController;

    iget-object v3, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mSwitchBar:Lcom/android/settings/widget/SwitchBar;

    invoke-direct {v2, v3}, Lcom/android/settings/widget/SwitchBarController;-><init>(Lcom/android/settings/widget/SwitchBar;)V

    invoke-direct {v1, v0, v2}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/widget/SwitchWidgetController;)V

    iput-object v1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mOPQuickTurnOnAssistantAppSwitchPreferenceController:Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;

    .line 42
    invoke-virtual {p0}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->getSettingsLifecycle()Lcom/android/settingslib/core/lifecycle/Lifecycle;

    move-result-object v1

    .line 43
    .local v1, "lifecycle":Lcom/android/settingslib/core/lifecycle/Lifecycle;
    if-eqz v1, :cond_0

    .line 44
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mOPQuickTurnOnAssistantAppSwitchPreferenceController:Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    iput-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    .line 47
    const-string v2, "camera"

    invoke-virtual {p0, v2}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mCamera:Lcom/android/settings/ui/RadioButtonPreference;

    .line 48
    const-string v2, "wallet"

    invoke-virtual {p0, v2}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/android/settings/ui/RadioButtonPreference;

    iput-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mWallet:Lcom/android/settings/ui/RadioButtonPreference;

    .line 49
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mCamera:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v2, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 50
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mWallet:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v2, p0}, Lcom/android/settings/ui/RadioButtonPreference;->setOnClickListener(Lcom/android/settings/ui/RadioButtonPreference$OnClickListener;)V

    .line 52
    return-void
.end method

.method public onRadioButtonClicked(Lcom/android/settings/ui/RadioButtonPreference;)V
    .locals 4
    .param p1, "preference"    # Lcom/android/settings/ui/RadioButtonPreference;

    .line 115
    invoke-virtual {p1}, Lcom/android/settings/ui/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "key":Ljava/lang/String;
    const/4 v1, 0x0

    .line 117
    .local v1, "value":I
    const-string v2, "camera"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    const/4 v1, 0x0

    goto :goto_0

    .line 119
    :cond_0
    const-string v2, "wallet"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    const/4 v1, 0x1

    .line 122
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "op_app_double_tap_power_gesture"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 123
    invoke-direct {p0, v1}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->updateQuickTurnOnAssistantAppSelectStatus(I)V

    .line 124
    return-void
.end method

.method public onResume()V
    .locals 7

    .line 62
    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    .line 63
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "op_app_double_tap_power_gesture"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 64
    .local v0, "value":I
    invoke-direct {p0, v0}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->updateQuickTurnOnAssistantAppSelectStatus(I)V

    .line 65
    invoke-direct {p0}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->updateQuickTurnOnAssistantAppEnabledStatus()V

    .line 66
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "camera_double_tap_power_gesture_disabled"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v6, 0x1

    invoke-virtual {v2, v4, v6, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 67
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->checkSupportWalletFlag(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mWallet:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v2, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    .line 69
    iget-object v2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v6, :cond_0

    move v2, v6

    goto :goto_0

    :cond_0
    move v2, v1

    .line 70
    .local v2, "isChecked":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 71
    iget-object v1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mCamera:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v1, v6}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    goto :goto_1

    .line 73
    :cond_1
    iget-object v3, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mCamera:Lcom/android/settings/ui/RadioButtonPreference;

    invoke-virtual {v3, v1}, Lcom/android/settings/ui/RadioButtonPreference;->setEnabled(Z)V

    .line 76
    .end local v2    # "isChecked":Z
    :cond_2
    :goto_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .line 80
    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onStop()V

    .line 81
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantApp;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 82
    return-void
.end method
