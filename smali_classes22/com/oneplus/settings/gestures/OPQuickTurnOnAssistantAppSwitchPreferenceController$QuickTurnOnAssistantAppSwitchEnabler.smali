.class Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;
.super Ljava/lang/Object;
.source "OPQuickTurnOnAssistantAppSwitchPreferenceController.java"

# interfaces
.implements Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "QuickTurnOnAssistantAppSwitchEnabler"
.end annotation


# instance fields
.field private mCallback:Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;

.field private mContext:Landroid/content/Context;

.field private final mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

.field private mValidListener:Z

.field final synthetic this$0:Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;


# direct methods
.method constructor <init>(Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;Landroid/content/Context;Lcom/android/settings/widget/SwitchWidgetController;)V
    .locals 1
    .param p1, "this$0"    # Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "switchController"    # Lcom/android/settings/widget/SwitchWidgetController;

    .line 45
    iput-object p1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->this$0:Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p2, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mContext:Landroid/content/Context;

    .line 47
    iput-object p3, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mValidListener:Z

    .line 49
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SwitchWidgetController;->setListener(Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;)V

    .line 50
    return-void
.end method

.method private setChecked(Z)V
    .locals 1
    .param p1, "isChecked"    # Z

    .line 78
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchWidgetController;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_1

    .line 81
    iget-boolean v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mValidListener:Z

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchWidgetController;->stopListening()V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/SwitchWidgetController;->setChecked(Z)V

    .line 85
    iget-boolean v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mValidListener:Z

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchWidgetController;->startListening()V

    .line 89
    :cond_1
    return-void
.end method

.method private triggerParentPreferenceCallback(Z)V
    .locals 1
    .param p1, "isChecked"    # Z

    .line 103
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mCallback:Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;

    if-eqz v0, :cond_0

    .line 104
    invoke-interface {v0, p1}, Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;->onSwitchToggled(Z)Z

    .line 106
    :cond_0
    return-void
.end method


# virtual methods
.method public onSwitchToggled(Z)Z
    .locals 3
    .param p1, "isChecked"    # Z

    .line 93
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    xor-int/lit8 v1, p1, 0x1

    const-string v2, "camera_double_tap_power_gesture_disabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 94
    invoke-direct {p0, p1}, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->triggerParentPreferenceCallback(Z)V

    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public pause()V
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mValidListener:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchWidgetController;->stopListening()V

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mValidListener:Z

    .line 75
    :cond_0
    return-void
.end method

.method public resume(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 61
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mContext:Landroid/content/Context;

    if-eq v0, p1, :cond_0

    .line 62
    iput-object p1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mContext:Landroid/content/Context;

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchWidgetController;->startListening()V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mValidListener:Z

    .line 66
    iget-object v1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "camera_double_tap_power_gesture_disabled"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v0, :cond_1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 67
    .local v0, "isChecked":Z
    :goto_0
    iget-object v1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/SwitchWidgetController;->setChecked(Z)V

    .line 68
    return-void
.end method

.method public setToggleCallback(Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;

    .line 99
    iput-object p1, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mCallback:Lcom/android/settings/widget/SwitchWidgetController$OnSwitchChangeListener;

    .line 100
    return-void
.end method

.method public setupSwitchController()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchWidgetController;->setupView()V

    .line 54
    return-void
.end method

.method public teardownSwitchController()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/oneplus/settings/gestures/OPQuickTurnOnAssistantAppSwitchPreferenceController$QuickTurnOnAssistantAppSwitchEnabler;->mSwitchController:Lcom/android/settings/widget/SwitchWidgetController;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchWidgetController;->teardownView()V

    .line 58
    return-void
.end method
