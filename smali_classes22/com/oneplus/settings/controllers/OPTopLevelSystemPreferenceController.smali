.class public Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPTopLevelSystemPreferenceController.java"

# interfaces
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnCreate;
.implements Lcom/android/settingslib/core/lifecycle/events/OnDestroy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;
    }
.end annotation


# static fields
.field private static final KEY_TOP_LEVEL_SYSTEM:Ljava/lang/String; = "top_level_system"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSystemUpdateObserver:Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;

.field private final mUm:Landroid/os/UserManager;

.field mUpdatePreference:Lcom/oneplus/settings/system/OPSystemUpdatePreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 53
    const-string v0, "top_level_system"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 54
    invoke-static {p1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mUm:Landroid/os/UserManager;

    .line 55
    iput-object p1, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mContext:Landroid/content/Context;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;

    .line 45
    iget-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 76
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 77
    invoke-virtual {p0}, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/system/OPSystemUpdatePreference;

    iput-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mUpdatePreference:Lcom/oneplus/settings/system/OPSystemUpdatePreference;

    .line 80
    :cond_0
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 71
    const-string v0, "top_level_system"

    return-object v0
.end method

.method public isNeedAvailable()Z
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportUss()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mUm:Landroid/os/UserManager;

    .line 66
    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 92
    invoke-virtual {p0}, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;-><init>(Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mSystemUpdateObserver:Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;

    .line 94
    iget-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mSystemUpdateObserver:Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;

    invoke-virtual {v0}, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;->startObserving()V

    .line 96
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mSystemUpdateObserver:Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0}, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;->stopObserving()V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->mSystemUpdateObserver:Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController$SystemUpdateObserver;

    .line 104
    :cond_0
    return-void
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 84
    invoke-virtual {p0}, Lcom/oneplus/settings/controllers/OPTopLevelSystemPreferenceController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    move-object v0, p1

    check-cast v0, Lcom/oneplus/settings/system/OPSystemUpdatePreference;

    .line 86
    .local v0, "updatePreference":Lcom/oneplus/settings/system/OPSystemUpdatePreference;
    invoke-virtual {v0}, Lcom/oneplus/settings/system/OPSystemUpdatePreference;->updateView()V

    .line 88
    .end local v0    # "updatePreference":Lcom/oneplus/settings/system/OPSystemUpdatePreference;
    :cond_0
    return-void
.end method
