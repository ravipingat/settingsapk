.class public Lcom/oneplus/settings/controllers/UstWfcStatusTracker;
.super Ljava/lang/Object;
.source "UstWfcStatusTracker.java"


# static fields
.field private static final SERVICE_INTENT_NAME:Ljava/lang/String; = "oneplus.intent.action.TMO_WFC_TRACKER_START"

.field private static final SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.oneplus.operator.tmo.wfctracker"

.field private static final START_OBSERVE:I = 0x0

.field private static final STOP_OBSERVE:I = 0x1

.field private static final TAG1:Ljava/lang/String; = "UstWfcStatusTracker"

.field private static final TAG2:Ljava/lang/String; = "UstWfcStatusTracker"


# instance fields
.field private mBound:Z

.field private mClientMessenger:Landroid/os/Messenger;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mPreference:Landroidx/preference/Preference;

.field private mServiceMessenger:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/preference/Preference;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preference"    # Landroidx/preference/Preference;

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mBound:Z

    .line 37
    new-instance v0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker$1;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/controllers/UstWfcStatusTracker$1;-><init>(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;)V

    iput-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mHandler:Landroid/os/Handler;

    .line 56
    new-instance v0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker$2;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/controllers/UstWfcStatusTracker$2;-><init>(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;)V

    iput-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mConnection:Landroid/content/ServiceConnection;

    .line 78
    const-string v0, "Init..."

    invoke-static {v0}, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->logd(Ljava/lang/String;)V

    .line 79
    iput-object p1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mContext:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mPreference:Landroidx/preference/Preference;

    .line 81
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mClientMessenger:Landroid/os/Messenger;

    .line 82
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .line 22
    invoke-static {p0}, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;)Landroidx/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/controllers/UstWfcStatusTracker;

    .line 22
    iget-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mPreference:Landroidx/preference/Preference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/controllers/UstWfcStatusTracker;

    .line 22
    iget-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;)Landroid/os/Messenger;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/controllers/UstWfcStatusTracker;

    .line 22
    iget-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mServiceMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic access$302(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/controllers/UstWfcStatusTracker;
    .param p1, "x1"    # Landroid/os/Messenger;

    .line 22
    iput-object p1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$402(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/controllers/UstWfcStatusTracker;
    .param p1, "x1"    # Z

    .line 22
    iput-boolean p1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mBound:Z

    return p1
.end method

.method static synthetic access$500(Lcom/oneplus/settings/controllers/UstWfcStatusTracker;)Landroid/os/Messenger;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/controllers/UstWfcStatusTracker;

    .line 22
    iget-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mClientMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method private static logd(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[UstWfcStatusTracker]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UstWfcStatusTracker"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-void
.end method


# virtual methods
.method public startObserve()V
    .locals 4

    .line 85
    const-string v0, "Start observe"

    invoke-static {v0}, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->logd(Ljava/lang/String;)V

    .line 86
    new-instance v0, Landroid/content/Intent;

    const-string v1, "oneplus.intent.action.TMO_WFC_TRACKER_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 87
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.oneplus.operator.tmo.wfctracker"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    iget-object v1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 89
    return-void
.end method

.method public stopObserve()V
    .locals 3

    .line 92
    const-string v0, "Stop observe"

    invoke-static {v0}, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->logd(Ljava/lang/String;)V

    .line 93
    iget-boolean v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mBound:Z

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 95
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 98
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mClientMessenger:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 101
    iget-object v1, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/oneplus/settings/controllers/UstWfcStatusTracker;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    goto :goto_0

    .line 102
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 105
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
