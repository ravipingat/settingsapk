.class public Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;
.super Landroid/content/BroadcastReceiver;
.source "OPTimerReceiverPowerOff.java"


# static fields
.field private static final ACTION_CANCEL_POWEROFF_ALARM:Ljava/lang/String; = "org.codeaurora.poweroffalarm.action.CANCEL_ALARM"

.field private static final ACTION_SET_POWEROFF_ALARM:Ljava/lang/String; = "org.codeaurora.poweroffalarm.action.SET_ALARM"

.field private static final POWER_OFF_ALARM_PACKAGE:Ljava/lang/String; = "com.qualcomm.qti.poweroffalarm"

.field private static final TIME:Ljava/lang/String; = "time"


# instance fields
.field private mLock:Landroid/os/PowerManager$WakeLock;

.field private pm:Landroid/os/PowerManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->pm:Landroid/os/PowerManager;

    .line 24
    iput-object v0, p0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->mLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method private cancleNewPlanLastPowerOn(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 232
    const/4 v0, 0x2

    new-array v0, v0, [J

    .line 233
    .local v0, "nearestTime":[J
    nop

    .line 234
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 233
    const-string v2, "def_timepower_config"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "timeConfig":Ljava/lang/String;
    invoke-static {v1}, Lcom/oneplus/settings/timer/timepower/SettingsUtil;->getNearestTime(Ljava/lang/String;)[J

    move-result-object v0

    .line 236
    new-instance v2, Landroid/content/Intent;

    const-string v3, "org.codeaurora.poweroffalarm.action.CANCEL_ALARM"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 238
    .local v2, "powerOffIntent":Landroid/content/Intent;
    const/high16 v3, 0x11000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 240
    const/4 v3, 0x0

    aget-wide v3, v0, v3

    const-string v5, "time"

    invoke-virtual {v2, v5, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 241
    const-string v3, "com.qualcomm.qti.poweroffalarm"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 243
    return-void
.end method

.method public static checkSwitch(Landroid/content/Context;Z)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "powerOnOrPowerOff"    # Z

    .line 256
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "def_timepower_config"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "config":Ljava/lang/String;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 260
    return v1

    .line 263
    :cond_0
    const/4 v2, 0x2

    filled-new-array {v2, v2}, [I

    move-result-object v3

    const-class v4, I

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[I

    .line 264
    .local v3, "mTimeArray":[[I
    filled-new-array {v2, v2}, [I

    move-result-object v4

    const-class v5, Z

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[Z

    .line 266
    .local v4, "mStateArray":[[Z
    const/4 v5, 0x0

    .local v5, "i":I
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_0
    const/4 v7, 0x6

    const/4 v8, 0x1

    if-gt v5, v7, :cond_1

    .line 267
    add-int/lit8 v9, v5, 0x6

    invoke-virtual {v0, v5, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 268
    .local v9, "tmp":Ljava/lang/String;
    aget-object v10, v3, v6

    invoke-virtual {v9, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    aput v11, v10, v1

    .line 269
    aget-object v10, v3, v6

    const/4 v11, 0x4

    invoke-virtual {v9, v2, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    aput v12, v10, v8

    .line 270
    aget-object v10, v4, v6

    const/4 v12, 0x5

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->intToBool(I)Z

    move-result v11

    aput-boolean v11, v10, v1

    .line 271
    aget-object v10, v4, v6

    invoke-virtual {v9, v12, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->intToBool(I)Z

    move-result v7

    aput-boolean v7, v10, v8

    .line 266
    .end local v9    # "tmp":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 274
    .end local v5    # "i":I
    .end local v6    # "j":I
    :cond_1
    if-eqz p1, :cond_2

    .line 275
    aget-object v2, v4, v1

    aget-boolean v2, v2, v8

    if-eqz v2, :cond_3

    .line 276
    return v8

    .line 279
    :cond_2
    aget-object v2, v4, v8

    aget-boolean v2, v2, v8

    if-eqz v2, :cond_3

    .line 280
    return v8

    .line 284
    :cond_3
    return v1
.end method

.method static intToBool(I)Z
    .locals 1
    .param p0, "i"    # I

    .line 31
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isPowerOffEnable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 251
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->checkSwitch(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public static isPowerOnEnable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 247
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->checkSwitch(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 37
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "action":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v3, v3, [J

    .line 41
    .local v3, "nearestTime":[J
    nop

    .line 42
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 41
    const-string v5, "def_timepower_config"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 43
    .local v4, "timeConfig":Ljava/lang/String;
    invoke-static {v4}, Lcom/oneplus/settings/timer/timepower/SettingsUtil;->getNearestTime(Ljava/lang/String;)[J

    move-result-object v3

    .line 47
    const-string v5, "power"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PowerManager;

    iput-object v5, v0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->pm:Landroid/os/PowerManager;

    .line 57
    const-string v5, "com.android.settings.action.REQUEST_POWER_OFF"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const-string v6, "android.intent.action.BOOT_COMPLETED"

    const-string v7, "android.intent.action.TIMEZONE_CHANGED"

    const-string v8, "android.intent.action.TIME_SET"

    const-wide/16 v10, 0x0

    const-string v12, "com.android.settings.POWER_OP_OFF"

    const-string v13, "alarm"

    const/4 v14, 0x1

    const/4 v9, 0x0

    if-nez v5, :cond_8

    .line 58
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 59
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 60
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto/16 :goto_1

    .line 88
    :cond_0
    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/high16 v15, 0x10000000

    if-eqz v5, :cond_4

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 93
    .local v16, "currentTime":J
    aget-wide v18, v3, v14

    sub-long v18, v16, v18

    const-wide/32 v20, 0x5265c00

    sub-long v18, v18, v20

    rem-long v18, v18, v20

    .line 94
    .local v18, "wrongTime":J
    cmp-long v5, v18, v10

    if-ltz v5, :cond_1

    const-wide/32 v20, 0xea60

    cmp-long v5, v18, v20

    if-gtz v5, :cond_2

    :cond_1
    cmp-long v5, v18, v10

    if-gez v5, :cond_3

    const-wide/32 v10, -0x52571a0

    cmp-long v5, v18, v10

    if-lez v5, :cond_3

    .line 95
    :cond_2
    return-void

    .line 102
    :cond_3
    new-instance v5, Landroid/content/Intent;

    const-class v10, Lcom/oneplus/settings/timer/timepower/OPPowerOffPromptActivity;

    invoke-direct {v5, v1, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v5, "it":Landroid/content/Intent;
    invoke-virtual {v5, v15}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 108
    invoke-virtual {v1, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 123
    .end local v5    # "it":Landroid/content/Intent;
    .end local v16    # "currentTime":J
    .end local v18    # "wrongTime":J
    move-object v11, v4

    goto/16 :goto_3

    :cond_4
    const-string v5, "com.android.settings.POWER_CONFIRM_OP_OFF"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 130
    iget-object v5, v0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->mLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v5, :cond_5

    .line 131
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 132
    const/4 v5, 0x0

    iput-object v5, v0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->mLock:Landroid/os/PowerManager$WakeLock;

    .line 135
    :cond_5
    iget-object v5, v0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->pm:Landroid/os/PowerManager;

    const v10, 0x1000000a

    .line 136
    const-string v11, "TimepowerWakeLock"

    invoke-virtual {v5, v10, v11}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    iput-object v5, v0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->mLock:Landroid/os/PowerManager$WakeLock;

    .line 139
    iget-object v5, v0, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->mLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 141
    new-instance v5, Landroid/content/Intent;

    const-string v10, "com.android.internal.intent.action.REQUEST_SHUTDOWN"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 142
    .local v5, "intet":Landroid/content/Intent;
    const-string v10, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v5, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 143
    invoke-virtual {v5, v15}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 144
    invoke-virtual {v1, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .end local v5    # "intet":Landroid/content/Intent;
    goto :goto_0

    .line 146
    :cond_6
    const-string v5, "com.android.settings.POWER_CANCEL_OP_OFF"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 160
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 162
    .local v5, "powerOffIntent":Landroid/content/Intent;
    const/high16 v10, 0x11000000

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 164
    nop

    .line 165
    invoke-virtual {v1, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/AlarmManager;

    .line 166
    .local v10, "am":Landroid/app/AlarmManager;
    const/high16 v11, 0x8000000

    invoke-static {v1, v9, v5, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    .line 172
    .local v12, "sender":Landroid/app/PendingIntent;
    invoke-virtual {v10, v12}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    move-object v11, v4

    goto :goto_3

    .line 146
    .end local v5    # "powerOffIntent":Landroid/content/Intent;
    .end local v10    # "am":Landroid/app/AlarmManager;
    .end local v12    # "sender":Landroid/app/PendingIntent;
    :cond_7
    :goto_0
    move-object v11, v4

    goto :goto_3

    .line 63
    :cond_8
    :goto_1
    invoke-static/range {p1 .. p1}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->isPowerOffEnable(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 64
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 67
    .local v5, "powerOFFIntent":Landroid/content/Intent;
    const/high16 v12, 0x11000000

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 69
    aget-wide v16, v3, v14

    cmp-long v10, v16, v10

    if-eqz v10, :cond_9

    .line 72
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 73
    .local v10, "c":Ljava/util/Calendar;
    aget-wide v11, v3, v9

    invoke-virtual {v10, v11, v12}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 74
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Power on alarm with flag set:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v10}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 74
    const-string v12, "boot"

    invoke-static {v12, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    .end local v10    # "c":Ljava/util/Calendar;
    nop

    .line 79
    invoke-virtual {v1, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/AlarmManager;

    .line 80
    .local v10, "am":Landroid/app/AlarmManager;
    const/high16 v11, 0x8000000

    invoke-static {v1, v9, v5, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    .line 84
    .restart local v12    # "sender":Landroid/app/PendingIntent;
    move-object v11, v4

    move-object v15, v5

    .end local v4    # "timeConfig":Ljava/lang/String;
    .end local v5    # "powerOFFIntent":Landroid/content/Intent;
    .local v11, "timeConfig":Ljava/lang/String;
    .local v15, "powerOFFIntent":Landroid/content/Intent;
    aget-wide v4, v3, v14

    invoke-virtual {v10, v9, v4, v5, v12}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto :goto_2

    .line 69
    .end local v10    # "am":Landroid/app/AlarmManager;
    .end local v11    # "timeConfig":Ljava/lang/String;
    .end local v12    # "sender":Landroid/app/PendingIntent;
    .end local v15    # "powerOFFIntent":Landroid/content/Intent;
    .restart local v4    # "timeConfig":Ljava/lang/String;
    .restart local v5    # "powerOFFIntent":Landroid/content/Intent;
    :cond_9
    move-object v11, v4

    move-object v15, v5

    .line 86
    .end local v4    # "timeConfig":Ljava/lang/String;
    .end local v5    # "powerOFFIntent":Landroid/content/Intent;
    .restart local v11    # "timeConfig":Ljava/lang/String;
    :goto_2
    goto :goto_3

    .line 63
    .end local v11    # "timeConfig":Ljava/lang/String;
    .restart local v4    # "timeConfig":Ljava/lang/String;
    :cond_a
    move-object v11, v4

    .line 181
    .end local v4    # "timeConfig":Ljava/lang/String;
    .restart local v11    # "timeConfig":Ljava/lang/String;
    :goto_3
    const-string v4, "com.android.settings.POWER_OP_ON"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 182
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 183
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 184
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 187
    :cond_b
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportNewPlanPowerOffAlarm()Z

    move-result v5

    if-nez v5, :cond_d

    .line 188
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v4, v5

    .line 190
    .local v4, "powerONIntent":Landroid/content/Intent;
    const/high16 v5, 0x11000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 192
    invoke-virtual {v1, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/AlarmManager;

    .line 193
    .local v5, "am":Landroid/app/AlarmManager;
    invoke-static {v1, v14, v4, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 194
    .local v6, "sender":Landroid/app/PendingIntent;
    invoke-static/range {p1 .. p1}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->isPowerOnEnable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 196
    aget-wide v7, v3, v9

    invoke-virtual {v5, v9, v7, v8, v6}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    goto :goto_4

    .line 199
    :cond_c
    invoke-virtual {v5, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 201
    .end local v4    # "powerONIntent":Landroid/content/Intent;
    .end local v5    # "am":Landroid/app/AlarmManager;
    .end local v6    # "sender":Landroid/app/PendingIntent;
    :goto_4
    goto :goto_5

    .line 203
    :cond_d
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v4, v5

    .line 205
    .local v4, "poweron":Landroid/content/Intent;
    const/high16 v5, 0x11000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 207
    invoke-virtual {v1, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/AlarmManager;

    .line 209
    .restart local v5    # "am":Landroid/app/AlarmManager;
    const/high16 v6, 0x8000000

    invoke-static {v1, v9, v4, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 211
    .restart local v6    # "sender":Landroid/app/PendingIntent;
    invoke-static/range {p1 .. p1}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->isPowerOnEnable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 212
    invoke-direct/range {p0 .. p1}, Lcom/oneplus/settings/timer/OPTimerReceiverPowerOff;->cancleNewPlanLastPowerOn(Landroid/content/Context;)V

    .line 213
    aget-wide v7, v3, v9

    invoke-virtual {v5, v9, v7, v8, v6}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 214
    new-instance v7, Landroid/content/Intent;

    const-string v8, "org.codeaurora.poweroffalarm.action.SET_ALARM"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 216
    .local v7, "powerOffIntent":Landroid/content/Intent;
    const/high16 v8, 0x11000000

    invoke-virtual {v7, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 218
    const-string v8, "com.qualcomm.qti.poweroffalarm"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    aget-wide v8, v3, v9

    const-string v10, "time"

    invoke-virtual {v7, v10, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 220
    invoke-virtual {v1, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 221
    .end local v7    # "powerOffIntent":Landroid/content/Intent;
    goto :goto_5

    .line 222
    :cond_e
    invoke-virtual {v5, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 228
    .end local v4    # "poweron":Landroid/content/Intent;
    .end local v5    # "am":Landroid/app/AlarmManager;
    .end local v6    # "sender":Landroid/app/PendingIntent;
    :cond_f
    :goto_5
    return-void
.end method
