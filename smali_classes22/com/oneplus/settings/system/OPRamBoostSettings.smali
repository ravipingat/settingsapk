.class public Lcom/oneplus/settings/system/OPRamBoostSettings;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "OPRamBoostSettings.java"


# static fields
.field private static final KEY_RAMBOOST_INSTRUCTIONS:Ljava/lang/String; = "op_ramboost_instructions"

.field public static final KEY_SETTINGS_PROVIDER:Ljava/lang/String; = "op_smartboost_enable"


# instance fields
.field private final EVENT_RAMBOOST:Ljava/lang/String;

.field private final KEY_RAMBOOST_SWITCH:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mLottieAnimPreference:Lcom/oneplus/settings/system/RamBoostLottieAnimPreference;

.field private mSwitchPreference:Landroidx/preference/SwitchPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    .line 35
    const-string v0, "op_ramboost_switch"

    iput-object v0, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->KEY_RAMBOOST_SWITCH:Ljava/lang/String;

    .line 36
    const-string v0, "RAM_Boost"

    iput-object v0, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->EVENT_RAMBOOST:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/system/OPRamBoostSettings;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/oneplus/settings/system/OPRamBoostSettings;

    .line 28
    iget-object v0, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getRamBoostState(Landroid/content/Context;)Z
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;

    .line 82
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "op_smartboost_enable"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 84
    .local v0, "state":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method private refreshUI()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/oneplus/settings/system/OPRamBoostSettings;->getRamBoostState(Landroid/content/Context;)Z

    move-result v0

    .line 71
    .local v0, "state":Z
    iget-object v1, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mSwitchPreference:Landroidx/preference/SwitchPreference;

    invoke-virtual {v1, v0}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .line 72
    return-void
.end method

.method public static sendDefaultAppTracker()V
    .locals 2

    .line 92
    sget-object v0, Lcom/oneplus/settings/SettingsBaseApplication;->mApplication:Landroid/app/Application;

    invoke-static {v0}, Lcom/oneplus/settings/system/OPRamBoostSettings;->getRamBoostState(Landroid/content/Context;)Z

    move-result v0

    .line 93
    .local v0, "state":Z
    const-string v1, "op_ramboost_instructions"

    invoke-static {v1, v0}, Lcom/oneplus/settings/utils/OPUtils;->sendAppTracker(Ljava/lang/String;I)V

    .line 94
    return-void
.end method

.method public static setRamBoostState(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .line 88
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "op_smartboost_enable"

    invoke-static {v0, v2, p1, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 89
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 76
    const/16 v0, 0x270f

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "icicle"    # Landroid/os/Bundle;

    .line 40
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPRamBoostSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 46
    const v0, 0x7f1600ae

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/system/OPRamBoostSettings;->addPreferencesFromResource(I)V

    .line 47
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPRamBoostSettings;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    .line 48
    .local v0, "root":Landroidx/preference/PreferenceScreen;
    const-string v1, "op_ramboost_switch"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Landroidx/preference/SwitchPreference;

    iput-object v1, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mSwitchPreference:Landroidx/preference/SwitchPreference;

    .line 49
    const-string v1, "op_ramboost_instructions"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/oneplus/settings/system/RamBoostLottieAnimPreference;

    iput-object v1, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mLottieAnimPreference:Lcom/oneplus/settings/system/RamBoostLottieAnimPreference;

    .line 50
    const-string v1, "op_ramboost_anim_white.json"

    .line 51
    .local v1, "jsonFile":Ljava/lang/String;
    iget-object v2, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Lcom/oneplus/settings/utils/OPUtils;->isBlackModeOn(Landroid/content/ContentResolver;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    const-string v1, "op_ramboost_anim_dark.json"

    .line 54
    :cond_0
    iget-object v2, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mLottieAnimPreference:Lcom/oneplus/settings/system/RamBoostLottieAnimPreference;

    invoke-virtual {v2, v1}, Lcom/oneplus/settings/system/RamBoostLottieAnimPreference;->setAnimFile(Ljava/lang/String;)V

    .line 55
    iget-object v2, p0, Lcom/oneplus/settings/system/OPRamBoostSettings;->mSwitchPreference:Landroidx/preference/SwitchPreference;

    new-instance v3, Lcom/oneplus/settings/system/OPRamBoostSettings$1;

    invoke-direct {v3, p0}, Lcom/oneplus/settings/system/OPRamBoostSettings$1;-><init>(Lcom/oneplus/settings/system/OPRamBoostSettings;)V

    invoke-virtual {v2, v3}, Landroidx/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    .line 65
    invoke-direct {p0}, Lcom/oneplus/settings/system/OPRamBoostSettings;->refreshUI()V

    .line 66
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 67
    return-void
.end method
