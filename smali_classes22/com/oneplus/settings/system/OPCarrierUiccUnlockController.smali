.class public Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPCarrierUiccUnlockController.java"


# static fields
.field private static final KEY_UICC_UNLOCK:Ljava/lang/String; = "uicc_unlock"

.field private static final TAG:Ljava/lang/String; = "OPCarrierUiccUnlockCont"


# instance fields
.field private mContext:Landroid/content/Context;

.field private unlockPreference:Landroidx/preference/Preference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 27
    const-string v0, "uicc_unlock"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->mContext:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 52
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 53
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->unlockPreference:Landroidx/preference/Preference;

    .line 54
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    .line 33
    const/4 v0, 0x4

    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 38
    const-string v0, "uicc_unlock"

    return-object v0
.end method

.method public bridge synthetic getSummary()Ljava/lang/CharSequence;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->getSummary()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 3

    .line 57
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->getType()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "type":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 60
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1215b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .local v1, "summary":Ljava/lang/String;
    goto :goto_0

    .line 63
    .end local v1    # "summary":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1215b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    .restart local v1    # "summary":Ljava/lang/String;
    :goto_0
    return-object v1
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    .line 69
    const-string v0, ""

    .line 70
    .local v0, "type":Ljava/lang/String;
    const-string v1, "value"

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->queryParamstore(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    const-string v1, "defaultvalue"

    invoke-virtual {p0, v1}, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->queryParamstore(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :cond_0
    return-object v0
.end method

.method public queryParamstore(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "item"    # Ljava/lang/String;

    .line 78
    const-string v0, ""

    .line 79
    .local v0, "value":Ljava/lang/String;
    const-string v1, "content://com.redbend.app.provider"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 81
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v2, p0, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/String;

    const-string v3, "sim_rssb_indicator"

    const/4 v8, 0x0

    aput-object v3, v6, v8

    const/4 v9, 0x1

    aput-object p1, v6, v9

    const/4 v3, 0x2

    const-string v7, "0"

    aput-object v7, v6, v3

    const/4 v7, 0x0

    move-object v3, v1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 82
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ne v3, v9, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 83
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 84
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :cond_0
    return-object v0

    .line 87
    .end local v2    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v2

    .line 88
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 90
    .end local v2    # "e":Ljava/lang/Exception;
    return-object v0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 2
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 43
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportUss()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->unlockPreference:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPCarrierUiccUnlockController;->getSummary()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 46
    :cond_0
    const-string v0, "OPCarrierUiccUnlockCont"

    const-string v1, "updateState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_1
    return-void
.end method
