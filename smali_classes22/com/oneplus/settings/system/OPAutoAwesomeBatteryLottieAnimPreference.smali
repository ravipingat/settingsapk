.class public Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;
.super Landroidx/preference/Preference;
.source "OPAutoAwesomeBatteryLottieAnimPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$OnPreferenceViewClickListener;
    }
.end annotation


# static fields
.field private static final MSG_PLAY:I


# instance fields
.field private animFile:Ljava/lang/String;

.field private anim_res:Lcom/airbnb/lottie/LottieAnimationView;

.field private img_play:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mListener:Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$OnPreferenceViewClickListener;

.field private resid:I

.field private showPlayBtn:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 74
    invoke-direct {p0, p1}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 30
    const v0, 0x7f0d01dc

    iput v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->resid:I

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->animFile:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->showPlayBtn:Z

    .line 39
    new-instance v0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$1;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$1;-><init>(Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;)V

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mHandler:Landroid/os/Handler;

    .line 75
    invoke-direct {p0, p1}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->initViews(Landroid/content/Context;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 69
    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const v0, 0x7f0d01dc

    iput v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->resid:I

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->animFile:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->showPlayBtn:Z

    .line 39
    new-instance v0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$1;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$1;-><init>(Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;)V

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mHandler:Landroid/os/Handler;

    .line 70
    invoke-direct {p0, p1}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->initViews(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const v0, 0x7f0d01dc

    iput v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->resid:I

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->animFile:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->showPlayBtn:Z

    .line 39
    new-instance v0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$1;

    invoke-direct {v0, p0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$1;-><init>(Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;)V

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mHandler:Landroid/os/Handler;

    .line 65
    invoke-direct {p0, p1}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->initViews(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;)V
    .locals 0
    .param p0, "x0"    # Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;

    .line 28
    invoke-direct {p0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->startOrStopAnim()V

    return-void
.end method

.method private initViews(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 79
    iput-object p1, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mContext:Landroid/content/Context;

    .line 80
    iget v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->resid:I

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->setLayoutResource(I)V

    .line 81
    return-void
.end method

.method private startAnim()V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->img_play:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->anim_res:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->resumeAnimation()V

    .line 137
    return-void
.end method

.method private startOrStopAnim()V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->anim_res:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->stopAnim()V

    .line 128
    return-void

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->startAnim()V

    .line 132
    return-void
.end method


# virtual methods
.method public isShowPlayBtn()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->showPlayBtn:Z

    return v0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3
    .param p1, "view"    # Landroidx/preference/PreferenceViewHolder;

    .line 85
    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 86
    const v0, 0x7f0a0309

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->img_play:Landroid/widget/ImageView;

    .line 87
    const v0, 0x7f0a0091

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->anim_res:Lcom/airbnb/lottie/LottieAnimationView;

    .line 88
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/oneplus/settings/utils/OPUtils;->isBlackModeOn(Landroid/content/ContentResolver;)Z

    move-result v0

    .line 89
    .local v0, "blackModel":Z
    if-eqz v0, :cond_0

    .line 90
    iget-object v1, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->anim_res:Lcom/airbnb/lottie/LottieAnimationView;

    const-string v2, "auto_awesome_battery_dark.json"

    invoke-virtual {v1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->anim_res:Lcom/airbnb/lottie/LottieAnimationView;

    const-string v2, "auto_awesome_battery_light.json"

    invoke-virtual {v1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    .line 94
    :goto_0
    iget-object v1, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->anim_res:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->loop(Z)V

    .line 95
    invoke-direct {p0}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->startAnim()V

    .line 108
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .line 154
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mListener:Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$OnPreferenceViewClickListener;

    if-eqz v0, :cond_0

    .line 155
    invoke-interface {v0, p1}, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$OnPreferenceViewClickListener;->onPreferenceViewClick(Landroid/view/View;)V

    .line 157
    :cond_0
    return-void
.end method

.method public playOrStopAnim()V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 119
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 120
    return-void
.end method

.method public setAnimFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "animFile"    # Ljava/lang/String;

    .line 60
    iput-object p1, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->animFile:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setShowPlayBtn(Z)V
    .locals 0
    .param p1, "showPlayBtn"    # Z

    .line 56
    iput-boolean p1, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->showPlayBtn:Z

    .line 57
    return-void
.end method

.method public setViewOnClick(Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$OnPreferenceViewClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$OnPreferenceViewClickListener;

    .line 111
    iput-object p1, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->mListener:Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference$OnPreferenceViewClickListener;

    .line 112
    return-void
.end method

.method public stopAnim()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->anim_res:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->pauseAnimation()V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/oneplus/settings/system/OPAutoAwesomeBatteryLottieAnimPreference;->img_play:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 144
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    :cond_1
    return-void
.end method
