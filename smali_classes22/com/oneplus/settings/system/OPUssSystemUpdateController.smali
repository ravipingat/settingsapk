.class public Lcom/oneplus/settings/system/OPUssSystemUpdateController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPUssSystemUpdateController.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mUm:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/oneplus/settings/system/OPUssSystemUpdateController;->mContext:Landroid/content/Context;

    .line 24
    invoke-static {p1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    iput-object v0, p0, Lcom/oneplus/settings/system/OPUssSystemUpdateController;->mUm:Landroid/os/UserManager;

    .line 25
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/oneplus/settings/system/OPUssSystemUpdateController;->isNeedAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0
.end method

.method public isNeedAvailable()Z
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/oneplus/settings/system/OPUssSystemUpdateController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportUss()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/oneplus/settings/system/OPUssSystemUpdateController;->mUm:Landroid/os/UserManager;

    .line 35
    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 33
    :goto_0
    return v0
.end method
