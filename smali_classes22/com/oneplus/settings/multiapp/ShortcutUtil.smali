.class public Lcom/oneplus/settings/multiapp/ShortcutUtil;
.super Ljava/lang/Object;
.source "ShortcutUtil.java"


# static fields
.field public static final LAUNCHER_FAVORITES_AUTHORITY:Ljava/lang/String; = "net.oneplus.launcher.settings"

.field public static final LAUNCHER_FAVORITES_CONTENT_URI:Landroid/net/Uri;

.field public static final LAUNCHER_FAVORITES_TABLE_NAME:Ljava/lang/String; = "favorites"

.field private static final PREFIX_SHORT_CUT_ID:Ljava/lang/String; = "com.oneplus.gamespace.shortcut:"

.field private static final TAG:Ljava/lang/String; = "Settings_ShortcutUtil"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    const-string v0, "content://net.oneplus.launcher.settings/favorites"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/oneplus/settings/multiapp/ShortcutUtil;->LAUNCHER_FAVORITES_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getLauncherFavoritesContentUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "id"    # J

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://net.oneplus.launcher.settings/favorites/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getShortcutIdWithNoTime(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "pkgName"    # Ljava/lang/String;
    .param p1, "uid"    # I

    .line 127
    const-string v0, "com.oneplus.gamespace.shortcut:"

    if-lez p1, :cond_0

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 130
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static removeShortcut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shortcutName"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "removeAll"    # Z

    .line 45
    move-object/from16 v1, p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeShortcut shortcutName:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " pkgName:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v2, p2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " uid:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v3, p3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Settings_ShortcutUtil"

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    .line 69
    .local v11, "cr":Landroid/content/ContentResolver;
    sget-object v6, Lcom/oneplus/settings/multiapp/ShortcutUtil;->LAUNCHER_FAVORITES_CONTENT_URI:Landroid/net/Uri;

    const-string v0, "intent"

    const-string v12, "_id"

    filled-new-array {v12, v0}, [Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x1

    new-array v9, v5, [Ljava/lang/String;

    const/4 v13, 0x0

    aput-object v1, v9, v13

    const-string v8, "title=?"

    const/4 v10, 0x0

    move-object v5, v11

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 72
    .local v5, "c":Landroid/database/Cursor;
    if-nez v5, :cond_0

    .line 73
    const-string v0, "removeShortcut error, Cursor is null !!!!!"

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    return-void

    .line 77
    :cond_0
    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 78
    .local v6, "intentIndex":I
    invoke-interface {v5, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 80
    .local v7, "idIndex":I
    const/4 v0, 0x0

    .line 82
    .local v0, "changed":Z
    invoke-static/range {p2 .. p3}, Lcom/oneplus/settings/multiapp/ShortcutUtil;->getShortcutIdWithNoTime(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    move v9, v0

    .line 84
    .end local v0    # "changed":Z
    .local v8, "shortcutId":Ljava/lang/String;
    .local v9, "changed":Z
    :goto_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v10, 0x0

    if-eqz v0, :cond_4

    .line 87
    :try_start_1
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v13}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 88
    .local v0, "it":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 89
    goto :goto_1

    .line 92
    :cond_1
    const-string v12, "shortcut_id"

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 93
    .local v12, "shortcutIdInDb":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "removeShortcut shortcutIdInDb:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, " shortcutId:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v4, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v14, 0x0

    .line 97
    .local v14, "needDelete":Z
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 98
    invoke-virtual {v12, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 99
    const/4 v14, 0x1

    .line 103
    :cond_2
    if-eqz v14, :cond_3

    .line 104
    invoke-interface {v5, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 105
    .local v15, "id":J
    invoke-static/range {v15 .. v16}, Lcom/oneplus/settings/multiapp/ShortcutUtil;->getLauncherFavoritesContentUri(J)Landroid/net/Uri;

    move-result-object v17

    move-object/from16 v18, v17

    .line 106
    .local v18, "uri":Landroid/net/Uri;
    move-object/from16 v13, v18

    .end local v18    # "uri":Landroid/net/Uri;
    .local v13, "uri":Landroid/net/Uri;
    invoke-virtual {v11, v13, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    const/4 v9, 0x1

    .line 108
    if-nez p4, :cond_3

    .line 109
    goto :goto_2

    .line 114
    .end local v0    # "it":Landroid/content/Intent;
    .end local v12    # "shortcutIdInDb":Ljava/lang/String;
    .end local v13    # "uri":Landroid/net/Uri;
    .end local v14    # "needDelete":Z
    .end local v15    # "id":J
    :cond_3
    const/4 v13, 0x0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/net/URISyntaxException;
    :try_start_2
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 114
    .end local v0    # "e":Ljava/net/URISyntaxException;
    nop

    .line 84
    :goto_1
    const/4 v13, 0x0

    goto :goto_0

    .line 117
    :cond_4
    :goto_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 118
    nop

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "removeShortcut changed:"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v12, " shortcut uninstalled"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    if-eqz v9, :cond_5

    .line 122
    sget-object v0, Lcom/oneplus/settings/multiapp/ShortcutUtil;->LAUNCHER_FAVORITES_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v11, v0, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 124
    :cond_5
    return-void

    .line 117
    :catchall_0
    move-exception v0

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0
.end method
