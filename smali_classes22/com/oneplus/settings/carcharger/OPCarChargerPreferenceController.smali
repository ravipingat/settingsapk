.class public Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPCarChargerPreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final KEY_AUTO_TURN_ON_CAR_CHARGER:Ljava/lang/String; = "car_charger_auto_turn_on"

.field private static final KEY_AUTO_TURN_ON_DND:Ljava/lang/String; = "car_charger_auto_turn_on_dnd"


# instance fields
.field private KEY:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->KEY:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 3

    .line 50
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isO2()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->KEY:Ljava/lang/String;

    const-string v2, "car_charger_auto_turn_on_dnd"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    return v1

    .line 52
    :cond_0
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isO2()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->KEY:Ljava/lang/String;

    const-string v2, "car_charger_auto_turn_on"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    return v1

    .line 55
    :cond_1
    const/4 v0, 0x2

    return v0
.end method

.method public final onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 25
    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 26
    .local v0, "settingsOn":Z
    iget-object v1, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->KEY:Ljava/lang/String;

    const-string v2, "car_charger_auto_turn_on_dnd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v3, "on"

    const-string v4, "off"

    if-eqz v1, :cond_1

    .line 27
    iget-object v1, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 28
    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v3, v4

    :goto_0
    const-string v1, "charge_dnd"

    invoke-static {v1, v3}, Lcom/oneplus/settings/utils/OPUtils;->sendAppTracker(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 29
    :cond_1
    iget-object v1, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->KEY:Ljava/lang/String;

    const-string v2, "car_charger_auto_turn_on"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 30
    iget-object v1, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 31
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move-object v3, v4

    :goto_1
    const-string v1, "charge_carmode"

    invoke-static {v1, v3}, Lcom/oneplus/settings/utils/OPUtils;->sendAppTracker(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :cond_3
    :goto_2
    const/4 v1, 0x1

    return v1
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 4
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 38
    iget-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->KEY:Ljava/lang/String;

    const-string v1, "car_charger_auto_turn_on_dnd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 40
    .local v0, "value":I
    move-object v1, p1

    check-cast v1, Landroidx/preference/SwitchPreference;

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .end local v0    # "value":I
    goto :goto_2

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->KEY:Ljava/lang/String;

    const-string v1, "car_charger_auto_turn_on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    iget-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 43
    .restart local v0    # "value":I
    move-object v1, p1

    check-cast v1, Landroidx/preference/SwitchPreference;

    if-ne v0, v2, :cond_2

    goto :goto_1

    :cond_2
    move v2, v3

    :goto_1
    invoke-virtual {v1, v2}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_3

    .line 41
    .end local v0    # "value":I
    :cond_3
    :goto_2
    nop

    .line 45
    :goto_3
    return-void
.end method
