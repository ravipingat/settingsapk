.class public Lcom/oneplus/settings/carcharger/OPCarChargerSettings;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "OPCarChargerSettings.java"


# static fields
.field private static final KEY_CAR_CHARGER_AUTO_TURN_ON:Ljava/lang/String; = "car_charger_auto_turn_on"

.field private static final KEY_CAR_CHARGER_AUTO_TURN_ON_DND:Ljava/lang/String; = "car_charger_auto_turn_on_dnd"

.field static final KEY_FOOTER_PREF:Ljava/lang/String; = "footer_preference"
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "OPCarChargerSettings"


# instance fields
.field mFooterPreference:Lcom/oneplus/settings/widget/OPFooterPreference;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected createPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v0, "controllers":Ljava/util/List;, "Ljava/util/List<Lcom/android/settingslib/core/AbstractPreferenceController;>;"
    new-instance v1, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;

    const-string v2, "car_charger_auto_turn_on"

    invoke-direct {v1, p1, v2}, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v1, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;

    const-string v2, "car_charger_auto_turn_on_dnd"

    invoke-direct {v1, p1, v2}, Lcom/oneplus/settings/carcharger/OPCarChargerPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance v1, Lcom/oneplus/settings/carcharger/OPCarChargerAutoOpenSpecifiedAppPreferenceController;

    invoke-direct {v1, p1}, Lcom/oneplus/settings/carcharger/OPCarChargerAutoOpenSpecifiedAppPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-object v0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 61
    const-string v0, "OPCarChargerSettings"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 66
    const/16 v0, 0x270f

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 55
    const v0, 0x7f16008a

    return v0
.end method

.method initPreferencesFromPreferenceScreen()V
    .locals 1

    .line 30
    const-string v0, "footer_preference"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/carcharger/OPCarChargerSettings;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/widget/OPFooterPreference;

    iput-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerSettings;->mFooterPreference:Lcom/oneplus/settings/widget/OPFooterPreference;

    .line 31
    iget-object v0, p0, Lcom/oneplus/settings/carcharger/OPCarChargerSettings;->mFooterPreference:Lcom/oneplus/settings/widget/OPFooterPreference;

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/carcharger/OPCarChargerSettings;->updateFooterPreference(Landroidx/preference/Preference;)V

    .line 32
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 25
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/oneplus/settings/carcharger/OPCarChargerSettings;->initPreferencesFromPreferenceScreen()V

    .line 27
    return-void
.end method

.method updateFooterPreference(Landroidx/preference/Preference;)V
    .locals 1
    .param p1, "mPreference"    # Landroidx/preference/Preference;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 36
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isO2()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const v0, 0x7f120bf4

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/carcharger/OPCarChargerSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 39
    :cond_0
    const v0, 0x7f120bf3

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/carcharger/OPCarChargerSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 41
    :goto_0
    return-void
.end method
