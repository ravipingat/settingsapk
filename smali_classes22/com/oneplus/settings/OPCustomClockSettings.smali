.class public Lcom/oneplus/settings/OPCustomClockSettings;
.super Lcom/oneplus/settings/OPQuitConfirmFragment;
.source "OPCustomClockSettings.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Lcom/oneplus/settings/OnBackPressListener;


# static fields
.field private static final KEY_PREFERENCE:Ljava/lang/String; = "op_custom_clock_preference"


# instance fields
.field private mOPCustomClockPreference:Lcom/oneplus/settings/ui/OPCustomClockPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/oneplus/settings/OPQuitConfirmFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 70
    const-string v0, "OPCustomClockSettings"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 46
    const/16 v0, 0x270f

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 107
    const v0, 0x7f16008c

    return v0
.end method

.method public synthetic lambda$onCreateOptionsMenu$0$OPCustomClockSettings(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "target"    # Landroid/view/MenuItem;

    .line 91
    iget-object v0, p0, Lcom/oneplus/settings/OPCustomClockSettings;->mOPCustomClockPreference:Lcom/oneplus/settings/ui/OPCustomClockPreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomClockPreference;->saveSelectedClock()V

    .line 92
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomClockSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method protected needShowWarningDialog()Z
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/oneplus/settings/OPCustomClockSettings;->mOPCustomClockPreference:Lcom/oneplus/settings/ui/OPCustomClockPreference;

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomClockPreference;->needShowWarningDialog()Z

    move-result v0

    return v0

    .line 78
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    invoke-super {p0, p1}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 31
    invoke-super {p0, p1}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onAttach(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public onCancelPressed()V
    .locals 1

    .line 64
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomClockSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    .line 65
    return-void
.end method

.method public onConfirmPressed()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/oneplus/settings/OPCustomClockSettings;->mOPCustomClockPreference:Lcom/oneplus/settings/ui/OPCustomClockPreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomClockPreference;->saveSelectedClock()V

    .line 58
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomClockSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    .line 59
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 22
    invoke-super {p0, p1}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0, p0}, Lcom/oneplus/settings/OPCustomClockSettings;->setOnBackPressListener(Lcom/oneplus/settings/OnBackPressListener;)V

    .line 24
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPCustomClockSettings;->setHasOptionsMenu(Z)V

    .line 25
    const-string v0, "op_custom_clock_preference"

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPCustomClockSettings;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPCustomClockPreference;

    iput-object v0, p0, Lcom/oneplus/settings/OPCustomClockSettings;->mOPCustomClockPreference:Lcom/oneplus/settings/ui/OPCustomClockPreference;

    .line 26
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .line 84
    if-nez p1, :cond_0

    .line 85
    return-void

    .line 87
    :cond_0
    const v0, 0x7f120cbd

    const/4 v1, 0x0

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 88
    .local v0, "menuItem":Landroid/view/MenuItem;
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 89
    const v1, 0x7f0804ad

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 90
    new-instance v1, Lcom/oneplus/settings/-$$Lambda$OPCustomClockSettings$rwsIykFqx3DmOd1q5XLVBCvmW6E;

    invoke-direct {v1, p0}, Lcom/oneplus/settings/-$$Lambda$OPCustomClockSettings$rwsIykFqx3DmOd1q5XLVBCvmW6E;-><init>(Lcom/oneplus/settings/OPCustomClockSettings;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 95
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 99
    invoke-super {p0}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onDestroy()V

    .line 101
    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 0

    .line 36
    invoke-super {p0}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onResume()V

    .line 37
    return-void
.end method
