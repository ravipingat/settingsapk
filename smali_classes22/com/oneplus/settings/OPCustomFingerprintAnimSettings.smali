.class public Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;
.super Lcom/oneplus/settings/OPQuitConfirmFragment;
.source "OPCustomFingerprintAnimSettings.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Lcom/oneplus/settings/OnBackPressListener;


# static fields
.field private static final KEY_PREFERENCE:Ljava/lang/String; = "op_custom_fingerprint_anim"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFingeprintAnimPreference:Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/oneplus/settings/OPQuitConfirmFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 82
    const-string v0, "OPCustomFingerprintAnimSettings"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 50
    const/16 v0, 0x270f

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 127
    const v0, 0x7f16008d

    return v0
.end method

.method public synthetic lambda$onCreateOptionsMenu$0$OPCustomFingerprintAnimSettings(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "target"    # Landroid/view/MenuItem;

    .line 103
    iget-object v0, p0, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->mFingeprintAnimPreference:Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;->saveSelectedAnim()V

    .line 104
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method protected needShowWarningDialog()Z
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->mFingeprintAnimPreference:Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;->needShowWarningDialog()Z

    move-result v0

    return v0

    .line 90
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    invoke-super {p0, p1}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->setHasOptionsMenu(Z)V

    .line 57
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    .line 58
    .local v0, "screen":Landroidx/preference/PreferenceScreen;
    const-string v1, "op_custom_fingerprint_anim"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;

    iput-object v1, p0, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->mFingeprintAnimPreference:Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;

    .line 59
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .line 34
    invoke-super {p0, p1}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onAttach(Landroid/app/Activity;)V

    .line 35
    iput-object p1, p0, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public onCancelPressed()V
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    .line 76
    :cond_0
    return-void
.end method

.method public onConfirmPressed()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->mFingeprintAnimPreference:Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;

    invoke-virtual {v0}, Lcom/oneplus/settings/ui/OPCustomFingeprintAnimVideoPreference;->saveSelectedAnim()V

    .line 65
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->onBackPressed()V

    .line 68
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 27
    invoke-super {p0, p1}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onCreate(Landroid/os/Bundle;)V

    .line 28
    invoke-super {p0, p1}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0, p0}, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->setOnBackPressListener(Lcom/oneplus/settings/OnBackPressListener;)V

    .line 30
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .line 96
    if-nez p1, :cond_0

    .line 97
    return-void

    .line 99
    :cond_0
    const v0, 0x7f120cbd

    const/4 v1, 0x0

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 100
    .local v0, "menuItem":Landroid/view/MenuItem;
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 101
    const v1, 0x7f0804ad

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 102
    new-instance v1, Lcom/oneplus/settings/-$$Lambda$OPCustomFingerprintAnimSettings$sijG1CDCIS-z_FChWSJQB8d5ITY;

    invoke-direct {v1, p0}, Lcom/oneplus/settings/-$$Lambda$OPCustomFingerprintAnimSettings$sijG1CDCIS-z_FChWSJQB8d5ITY;-><init>(Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 107
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .line 111
    invoke-super {p0}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onDestroy()V

    .line 112
    iget-object v0, p0, Lcom/oneplus/settings/OPCustomFingerprintAnimSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "op_custom_unlock_animation_style"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 113
    .local v0, "style":I
    const-string v1, "status"

    const-string v2, "fod_effect"

    if-nez v0, :cond_0

    .line 114
    const-string v3, "1"

    invoke-static {v2, v1, v3}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_0
    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 116
    const-string v3, "2"

    invoke-static {v2, v1, v3}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_1
    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    .line 118
    const-string v3, "3"

    invoke-static {v2, v1, v3}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_2
    const/16 v3, 0x9

    if-ne v0, v3, :cond_3

    .line 120
    const-string v3, "4"

    invoke-static {v2, v1, v3}, Lcom/oneplus/settings/utils/OPUtils;->sendAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_3
    :goto_0
    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 0

    .line 40
    invoke-super {p0}, Lcom/oneplus/settings/OPQuitConfirmFragment;->onResume()V

    .line 41
    return-void
.end method
