.class public Lcom/oneplus/custom/utils/ParamReader;
.super Ljava/lang/Object;
.source "ParamReader.java"


# static fields
.field private static final CUSTOM_BACK_COVER_FN_PATH:Ljava/lang/String; = "/sys/module/param_read_write/parameters/backcover_color"

.field private static final CUSTOM_FN_PATH:Ljava/lang/String; = "/sys/module/param_read_write/parameters/cust_flag"

.field private static final PARAMSERVICE_STUB_PRIOR_TO_Q:Ljava/lang/String; = "com.oem.os.IParamService$Stub"

.field private static final PARAMSERVICE_STUB_Q:Ljava/lang/String; = "com.oneplus.os.IParamService$Stub"

.field private static final PARAM_BACKCOVER_COLOR:I = 0x2

.field private static final PARAM_CUST_FLAG:I = 0x4

.field private static final PARAM_GET_SECURE_WP_KEY:I = 0x1a

.field private static final PARAM_SW_TYPE:I = 0x18

.field private static final TAG:Ljava/lang/String; = "ParamReader"

.field private static mParamReadRet:Z

.field private static mParamReadbyte:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/oneplus/custom/utils/ParamReader;->mParamReadbyte:Ljava/util/ArrayList;

    .line 189
    const/4 v0, 0x0

    sput-boolean v0, Lcom/oneplus/custom/utils/ParamReader;->mParamReadRet:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .line 21
    sput-boolean p0, Lcom/oneplus/custom/utils/ParamReader;->mParamReadRet:Z

    return p0
.end method

.method static synthetic access$102(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .line 21
    sput-object p0, Lcom/oneplus/custom/utils/ParamReader;->mParamReadbyte:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static getBackCoverColorVal()Ljava/lang/String;
    .locals 14

    .line 131
    const-string v0, "getBackCoverColorVal ~P result = "

    const-string v1, "00000000"

    .line 134
    .local v1, "result":Ljava/lang/String;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v3, "ParamReader"

    const/16 v4, 0x1b

    if-gt v2, v4, :cond_4

    .line 135
    new-instance v2, Ljava/io/File;

    const-string v4, "/sys/module/param_read_write/parameters/backcover_color"

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    .local v2, "custFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 137
    const-string v0, "CUSTOM_BACK_COVER_FN_PATH not existed"

    invoke-static {v3, v0}, Lcom/oneplus/custom/utils/MyLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-object v1

    .line 141
    :cond_0
    const/4 v4, 0x0

    .line 143
    .local v4, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v4, v5

    .line 145
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .local v6, "line":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 146
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v5

    goto :goto_0

    .line 152
    .end local v6    # "line":Ljava/lang/String;
    :cond_1
    nop

    .line 153
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 157
    goto :goto_1

    .line 155
    :catch_0
    move-exception v5

    .line 156
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/oneplus/custom/utils/MyLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-object v1

    .line 151
    :catchall_0
    move-exception v5

    goto :goto_5

    .line 148
    :catch_1
    move-exception v5

    .line 149
    .restart local v5    # "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 152
    .end local v5    # "e":Ljava/lang/Exception;
    if-eqz v4, :cond_2

    .line 153
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    .line 155
    :catch_2
    move-exception v5

    .line 156
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 157
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_3
    nop

    .line 158
    :goto_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_2

    .line 152
    :goto_5
    if-eqz v4, :cond_3

    .line 153
    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_6

    .line 155
    :catch_3
    move-exception v5

    .line 156
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 157
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_6
    nop

    .line 158
    :goto_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_2

    .line 163
    .end local v2    # "custFile":Ljava/io/File;
    .end local v4    # "br":Ljava/io/BufferedReader;
    :cond_4
    :try_start_5
    const-string v0, "android.os.ServiceManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 164
    .local v0, "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "getService"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v0, v2, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 165
    .local v2, "getService":Ljava/lang/reflect/Method;
    new-array v5, v4, [Ljava/lang/Object;

    const-string v6, "ParamService"

    aput-object v6, v5, v7

    const/4 v6, 0x0

    invoke-virtual {v2, v6, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 167
    .local v5, "oRemoteService":Ljava/lang/Object;
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x1c

    if-gt v8, v9, :cond_5

    .line 168
    const-string v8, "com.oem.os.IParamService$Stub"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .local v8, "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_8

    .line 170
    .end local v8    # "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_5
    const-string v8, "com.oneplus.os.IParamService$Stub"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .line 172
    .restart local v8    # "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_8
    const-string v9, "asInterface"

    new-array v10, v4, [Ljava/lang/Class;

    const-class v11, Landroid/os/IBinder;

    aput-object v11, v10, v7

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 173
    .local v9, "asInterface":Ljava/lang/reflect/Method;
    new-array v10, v4, [Ljava/lang/Object;

    aput-object v5, v10, v7

    invoke-virtual {v9, v6, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 174
    .local v6, "oIParamService":Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    const-string v11, "getParamIntSYNC"

    new-array v12, v4, [Ljava/lang/Class;

    sget-object v13, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v13, v12, v7

    invoke-virtual {v10, v11, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 176
    .local v10, "getParamIntSYNC":Ljava/lang/reflect/Method;
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v4, v7

    invoke-virtual {v10, v6, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 177
    .local v4, "resultInt":I
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, v7

    .line 178
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getBackCoverColorVal P~ result = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/oneplus/custom/utils/MyLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 181
    .end local v0    # "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "getService":Ljava/lang/reflect/Method;
    .end local v4    # "resultInt":I
    .end local v5    # "oRemoteService":Ljava/lang/Object;
    .end local v6    # "oIParamService":Ljava/lang/Object;
    .end local v8    # "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v9    # "asInterface":Ljava/lang/reflect/Method;
    .end local v10    # "getParamIntSYNC":Ljava/lang/reflect/Method;
    goto :goto_9

    .line 179
    :catch_4
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBackCoverColorVal throws exception: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_9
    return-object v1
.end method

.method public static getCustFlagVal()I
    .locals 14

    .line 63
    const-string v0, "getCustFlagVal ~P result = "

    const/4 v1, 0x0

    .line 66
    .local v1, "result":I
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, "ParamReader"

    const/16 v6, 0x1b

    if-gt v2, v6, :cond_8

    .line 67
    const/4 v2, 0x0

    .line 68
    .local v2, "br":Ljava/io/BufferedReader;
    new-instance v6, Ljava/io/File;

    const-string v7, "/sys/module/param_read_write/parameters/cust_flag"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .local v6, "themeStateFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 70
    const-string v0, "CUSTOM_FN_PATH not existed"

    invoke-static {v5, v0}, Lcom/oneplus/custom/utils/MyLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return v1

    .line 74
    :cond_0
    :try_start_0
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    invoke-direct {v8, v6}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v7

    .line 76
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .local v8, "line":Ljava/lang/String;
    if-eqz v7, :cond_5

    .line 77
    const/4 v7, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    const/4 v10, 0x2

    packed-switch v9, :pswitch_data_0

    :cond_1
    goto :goto_1

    :pswitch_0
    const-string v9, "3"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    move v7, v10

    goto :goto_1

    :pswitch_1
    const-string v9, "2"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    move v7, v4

    goto :goto_1

    :pswitch_2
    const-string v9, "1"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v9, :cond_1

    move v7, v3

    :goto_1
    if-eqz v7, :cond_4

    if-eq v7, v4, :cond_3

    if-eq v7, v10, :cond_2

    goto :goto_2

    .line 85
    :cond_2
    const/4 v1, 0x3

    .line 86
    goto :goto_2

    .line 82
    :cond_3
    const/4 v1, 0x2

    .line 83
    goto :goto_2

    .line 79
    :cond_4
    const/4 v1, 0x1

    .line 80
    nop

    .line 88
    :goto_2
    goto :goto_0

    .line 95
    .end local v8    # "line":Ljava/lang/String;
    :cond_5
    nop

    .line 96
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 100
    goto :goto_3

    .line 98
    :catch_0
    move-exception v3

    .line 99
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    :goto_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/oneplus/custom/utils/MyLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return v1

    .line 94
    :catchall_0
    move-exception v3

    goto :goto_7

    .line 91
    :catch_1
    move-exception v3

    .line 92
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 95
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_6

    .line 96
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_5

    .line 98
    :catch_2
    move-exception v3

    .line 99
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 100
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_5
    nop

    .line 101
    :goto_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_4

    .line 95
    :goto_7
    if-eqz v2, :cond_7

    .line 96
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_8

    .line 98
    :catch_3
    move-exception v3

    .line 99
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 100
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_8
    nop

    .line 101
    :goto_9
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_4

    .line 106
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v6    # "themeStateFile":Ljava/io/File;
    :cond_8
    :try_start_5
    const-string v0, "android.os.ServiceManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 107
    .local v0, "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "getService"

    new-array v6, v4, [Ljava/lang/Class;

    const-class v7, Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-virtual {v0, v2, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 108
    .local v2, "getService":Ljava/lang/reflect/Method;
    new-array v6, v4, [Ljava/lang/Object;

    const-string v7, "ParamService"

    aput-object v7, v6, v3

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 110
    .local v6, "oRemoteService":Ljava/lang/Object;
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x1c

    if-gt v8, v9, :cond_9

    .line 111
    const-string v8, "com.oem.os.IParamService$Stub"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .local v8, "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_a

    .line 113
    .end local v8    # "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_9
    const-string v8, "com.oneplus.os.IParamService$Stub"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .line 115
    .restart local v8    # "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_a
    const-string v9, "asInterface"

    new-array v10, v4, [Ljava/lang/Class;

    const-class v11, Landroid/os/IBinder;

    aput-object v11, v10, v3

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 116
    .local v9, "asInterface":Ljava/lang/reflect/Method;
    new-array v10, v4, [Ljava/lang/Object;

    aput-object v6, v10, v3

    invoke-virtual {v9, v7, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 117
    .local v7, "oIParamService":Ljava/lang/Object;
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    const-string v11, "getParamIntSYNC"

    new-array v12, v4, [Ljava/lang/Class;

    sget-object v13, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v13, v12, v3

    invoke-virtual {v10, v11, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 119
    .local v10, "getParamIntSYNC":Ljava/lang/reflect/Method;
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v11, 0x4

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v4, v3

    invoke-virtual {v10, v7, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v1, v3

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCustFlagVal P~ result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/oneplus/custom/utils/MyLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 123
    .end local v0    # "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "getService":Ljava/lang/reflect/Method;
    .end local v6    # "oRemoteService":Ljava/lang/Object;
    .end local v7    # "oIParamService":Ljava/lang/Object;
    .end local v8    # "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v9    # "asInterface":Ljava/lang/reflect/Method;
    .end local v10    # "getParamIntSYNC":Ljava/lang/reflect/Method;
    goto :goto_b

    .line 121
    :catch_4
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCustFlagVal throws exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_b
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getOneplusParamService()Lvendor/oneplus/hardware/param/V1_0/IOneplusParam;
    .locals 3

    .line 35
    :try_start_0
    invoke-static {}, Lvendor/oneplus/hardware/param/V1_0/IOneplusParam;->getService()Lvendor/oneplus/hardware/param/V1_0/IOneplusParam;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception getting OnePlus param service: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ParamReader"

    invoke-static {v2, v1}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const/4 v1, 0x0

    return-object v1
.end method

.method public static getSecureWPKey()[B
    .locals 8

    .line 192
    const-string v0, "ParamReader"

    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 194
    .local v1, "cdl":Ljava/util/concurrent/CountDownLatch;
    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Lcom/oneplus/custom/utils/ParamReader$1;

    invoke-direct {v3, v1}, Lcom/oneplus/custom/utils/ParamReader$1;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    .line 202
    .local v3, "cbk":Lvendor/oneplus/hardware/param/V1_0/IOneplusParam$getParamBufCallback;
    invoke-static {}, Lcom/oneplus/custom/utils/ParamReader;->getOneplusParamService()Lvendor/oneplus/hardware/param/V1_0/IOneplusParam;

    move-result-object v4

    .line 203
    .local v4, "op":Lvendor/oneplus/hardware/param/V1_0/IOneplusParam;
    if-nez v4, :cond_0

    .line 204
    const-string v5, "Can\'t get OneplusParamService"

    invoke-static {v0, v5}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    return-object v2

    .line 207
    :cond_0
    const/16 v5, 0x1a

    invoke-interface {v4, v5, v3}, Lvendor/oneplus/hardware/param/V1_0/IOneplusParam;->getParamBuf(ILvendor/oneplus/hardware/param/V1_0/IOneplusParam$getParamBufCallback;)V

    .line 208
    const-wide/16 v5, 0x64

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v5, v6, v7}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 209
    sget-object v5, Lcom/oneplus/custom/utils/ParamReader;->mParamReadbyte:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [B

    .line 211
    .local v5, "myResult":[B
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    sget-object v7, Lcom/oneplus/custom/utils/ParamReader;->mParamReadbyte:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 212
    sget-object v7, Lcom/oneplus/custom/utils/ParamReader;->mParamReadbyte:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Byte;

    invoke-virtual {v7}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    if-nez v7, :cond_1

    .line 213
    goto :goto_1

    .line 215
    :cond_1
    sget-object v7, Lcom/oneplus/custom/utils/ParamReader;->mParamReadbyte:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Byte;

    invoke-virtual {v7}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    aput-byte v7, v5, v6

    .line 211
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 217
    .end local v6    # "i":I
    :cond_2
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get WP key result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/oneplus/custom/utils/MyLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    return-object v5

    .line 219
    .end local v3    # "cbk":Lvendor/oneplus/hardware/param/V1_0/IOneplusParam$getParamBufCallback;
    .end local v4    # "op":Lvendor/oneplus/hardware/param/V1_0/IOneplusParam;
    .end local v5    # "myResult":[B
    :catch_0
    move-exception v3

    .line 220
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getParamBuf throws exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .end local v3    # "e":Ljava/lang/Exception;
    return-object v2
.end method

.method public static getSwTypeVal()I
    .locals 14

    .line 43
    const-string v0, "ParamReader"

    const/4 v1, 0x0

    .line 45
    .local v1, "result":I
    :try_start_0
    const-string v2, "android.os.ServiceManager"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 46
    .local v2, "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "getService"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 47
    .local v3, "getService":Ljava/lang/reflect/Method;
    new-array v5, v4, [Ljava/lang/Object;

    const-string v6, "ParamService"

    aput-object v6, v5, v7

    const/4 v6, 0x0

    invoke-virtual {v3, v6, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 48
    .local v5, "oRemoteService":Ljava/lang/Object;
    const-string v8, "com.oneplus.os.IParamService$Stub"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .line 49
    .local v8, "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v9, "asInterface"

    new-array v10, v4, [Ljava/lang/Class;

    const-class v11, Landroid/os/IBinder;

    aput-object v11, v10, v7

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 50
    .local v9, "asInterface":Ljava/lang/reflect/Method;
    new-array v10, v4, [Ljava/lang/Object;

    aput-object v5, v10, v7

    invoke-virtual {v9, v6, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 51
    .local v6, "oIParamService":Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    const-string v11, "getParamIntSYNC"

    new-array v12, v4, [Ljava/lang/Class;

    sget-object v13, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v13, v12, v7

    invoke-virtual {v10, v11, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 53
    .local v10, "getParamIntSYNC":Ljava/lang/reflect/Method;
    new-array v4, v4, [Ljava/lang/Object;

    const/16 v11, 0x18

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v4, v7

    invoke-virtual {v10, v6, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move v1, v4

    .line 54
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSwTypeVal result = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/oneplus/custom/utils/MyLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .end local v2    # "serviceManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "getService":Ljava/lang/reflect/Method;
    .end local v5    # "oRemoteService":Ljava/lang/Object;
    .end local v6    # "oIParamService":Ljava/lang/Object;
    .end local v8    # "cStub":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v9    # "asInterface":Ljava/lang/reflect/Method;
    .end local v10    # "getParamIntSYNC":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 55
    :catch_0
    move-exception v2

    .line 56
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSwTypeVal throws exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/oneplus/custom/utils/MyLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return v1
.end method
