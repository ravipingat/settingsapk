.class public Lcom/qualcomm/qti/remoteSimlock/IUimRemoteSimlockServiceCallback$Default;
.super Ljava/lang/Object;
.source "IUimRemoteSimlockServiceCallback.java"

# interfaces
.implements Lcom/qualcomm/qti/remoteSimlock/IUimRemoteSimlockServiceCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/remoteSimlock/IUimRemoteSimlockServiceCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Default"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public uimRemoteSimlockGenerateHMACResponse(II[B)V
    .locals 0
    .param p1, "token"    # I
    .param p2, "responseCode"    # I
    .param p3, "hmacData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 60
    return-void
.end method

.method public uimRemoteSimlockGetSharedKeyResponse(II[B)V
    .locals 0
    .param p1, "token"    # I
    .param p2, "responseCode"    # I
    .param p3, "encryptedKey"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 43
    return-void
.end method

.method public uimRemoteSimlockGetSimlockStatusResponse(IIIJ)V
    .locals 0
    .param p1, "token"    # I
    .param p2, "responseCode"    # I
    .param p3, "unlockStatus"    # I
    .param p4, "unlockTime"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 105
    return-void
.end method

.method public uimRemoteSimlockGetVersionResponse(IIII)V
    .locals 0
    .param p1, "token"    # I
    .param p2, "responseCode"    # I
    .param p3, "majorVersion"    # I
    .param p4, "minorVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 80
    return-void
.end method

.method public uimRemoteSimlockProcessSimlockDataResponse(II[B)V
    .locals 0
    .param p1, "token"    # I
    .param p2, "responseCode"    # I
    .param p3, "simlockResponse"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 26
    return-void
.end method
