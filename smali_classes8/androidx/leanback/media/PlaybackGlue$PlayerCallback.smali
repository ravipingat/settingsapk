.class public abstract Landroidx/leanback/media/PlaybackGlue$PlayerCallback;
.super Ljava/lang/Object;
.source "PlaybackGlue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/leanback/media/PlaybackGlue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PlayerCallback"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlayCompleted(Landroidx/leanback/media/PlaybackGlue;)V
    .locals 0
    .param p1, "glue"    # Landroidx/leanback/media/PlaybackGlue;

    .line 71
    return-void
.end method

.method public onPlayStateChanged(Landroidx/leanback/media/PlaybackGlue;)V
    .locals 0
    .param p1, "glue"    # Landroidx/leanback/media/PlaybackGlue;

    .line 64
    return-void
.end method

.method public onPreparedStateChanged(Landroidx/leanback/media/PlaybackGlue;)V
    .locals 0
    .param p1, "glue"    # Landroidx/leanback/media/PlaybackGlue;

    .line 57
    return-void
.end method
