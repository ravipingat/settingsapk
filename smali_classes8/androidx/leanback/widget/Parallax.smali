.class public abstract Landroidx/leanback/widget/Parallax;
.super Ljava/lang/Object;
.source "Parallax.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/leanback/widget/Parallax$FloatPropertyMarkerValue;,
        Landroidx/leanback/widget/Parallax$FloatProperty;,
        Landroidx/leanback/widget/Parallax$IntPropertyMarkerValue;,
        Landroidx/leanback/widget/Parallax$IntProperty;,
        Landroidx/leanback/widget/Parallax$PropertyMarkerValue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropertyT:",
        "Landroid/util/Property;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mEffects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroidx/leanback/widget/ParallaxEffect;",
            ">;"
        }
    .end annotation
.end field

.field private mFloatValues:[F

.field final mProperties:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TPropertyT;>;"
        }
    .end annotation
.end field

.field final mPropertiesReadOnly:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TPropertyT;>;"
        }
    .end annotation
.end field

.field private mValues:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 78
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    .line 405
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroidx/leanback/widget/Parallax;->mPropertiesReadOnly:Ljava/util/List;

    .line 407
    const/4 v0, 0x4

    new-array v1, v0, [I

    iput-object v1, p0, Landroidx/leanback/widget/Parallax;->mValues:[I

    .line 408
    new-array v1, v0, [F

    iput-object v1, p0, Landroidx/leanback/widget/Parallax;->mFloatValues:[F

    .line 410
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Landroidx/leanback/widget/Parallax;->mEffects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public varargs addEffect([Landroidx/leanback/widget/Parallax$PropertyMarkerValue;)Landroidx/leanback/widget/ParallaxEffect;
    .locals 2
    .param p1, "ranges"    # [Landroidx/leanback/widget/Parallax$PropertyMarkerValue;

    .line 617
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Landroidx/leanback/widget/Parallax$PropertyMarkerValue;->getProperty()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroidx/leanback/widget/Parallax$IntProperty;

    if-eqz v0, :cond_0

    .line 618
    new-instance v0, Landroidx/leanback/widget/ParallaxEffect$IntEffect;

    invoke-direct {v0}, Landroidx/leanback/widget/ParallaxEffect$IntEffect;-><init>()V

    .local v0, "effect":Landroidx/leanback/widget/ParallaxEffect;
    goto :goto_0

    .line 620
    .end local v0    # "effect":Landroidx/leanback/widget/ParallaxEffect;
    :cond_0
    new-instance v0, Landroidx/leanback/widget/ParallaxEffect$FloatEffect;

    invoke-direct {v0}, Landroidx/leanback/widget/ParallaxEffect$FloatEffect;-><init>()V

    .line 622
    .restart local v0    # "effect":Landroidx/leanback/widget/ParallaxEffect;
    :goto_0
    invoke-virtual {v0, p1}, Landroidx/leanback/widget/ParallaxEffect;->setPropertyRanges([Landroidx/leanback/widget/Parallax$PropertyMarkerValue;)V

    .line 623
    iget-object v1, p0, Landroidx/leanback/widget/Parallax;->mEffects:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 624
    return-object v0
.end method

.method public final addProperty(Ljava/lang/String;)Landroid/util/Property;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TPropertyT;"
        }
    .end annotation

    .line 453
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 454
    .local v0, "newPropertyIndex":I
    invoke-virtual {p0, p1, v0}, Landroidx/leanback/widget/Parallax;->createProperty(Ljava/lang/String;I)Landroid/util/Property;

    move-result-object v1

    .line 455
    .local v1, "property":Landroid/util/Property;, "TPropertyT;"
    instance-of v2, v1, Landroidx/leanback/widget/Parallax$IntProperty;

    if-eqz v2, :cond_2

    .line 456
    iget-object v2, p0, Landroidx/leanback/widget/Parallax;->mValues:[I

    array-length v2, v2

    .line 457
    .local v2, "size":I
    if-ne v2, v0, :cond_1

    .line 458
    mul-int/lit8 v3, v2, 0x2

    new-array v3, v3, [I

    .line 459
    .local v3, "newValues":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 460
    iget-object v5, p0, Landroidx/leanback/widget/Parallax;->mValues:[I

    aget v5, v5, v4

    aput v5, v3, v4

    .line 459
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 462
    .end local v4    # "i":I
    :cond_0
    iput-object v3, p0, Landroidx/leanback/widget/Parallax;->mValues:[I

    .line 464
    .end local v3    # "newValues":[I
    :cond_1
    iget-object v3, p0, Landroidx/leanback/widget/Parallax;->mValues:[I

    const v4, 0x7fffffff

    aput v4, v3, v0

    .line 465
    .end local v2    # "size":I
    goto :goto_2

    :cond_2
    instance-of v2, v1, Landroidx/leanback/widget/Parallax$FloatProperty;

    if-eqz v2, :cond_5

    .line 466
    iget-object v2, p0, Landroidx/leanback/widget/Parallax;->mFloatValues:[F

    array-length v2, v2

    .line 467
    .restart local v2    # "size":I
    if-ne v2, v0, :cond_4

    .line 468
    mul-int/lit8 v3, v2, 0x2

    new-array v3, v3, [F

    .line 469
    .local v3, "newValues":[F
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    if-ge v4, v2, :cond_3

    .line 470
    iget-object v5, p0, Landroidx/leanback/widget/Parallax;->mFloatValues:[F

    aget v5, v5, v4

    aput v5, v3, v4

    .line 469
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 472
    .end local v4    # "i":I
    :cond_3
    iput-object v3, p0, Landroidx/leanback/widget/Parallax;->mFloatValues:[F

    .line 474
    .end local v3    # "newValues":[F
    :cond_4
    iget-object v3, p0, Landroidx/leanback/widget/Parallax;->mFloatValues:[F

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    aput v4, v3, v0

    .line 475
    .end local v2    # "size":I
    nop

    .line 478
    :goto_2
    iget-object v2, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    return-object v1

    .line 476
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid Property type"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public abstract createProperty(Ljava/lang/String;I)Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)TPropertyT;"
        }
    .end annotation
.end method

.method public getEffects()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroidx/leanback/widget/ParallaxEffect;",
            ">;"
        }
    .end annotation

    .line 589
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mEffects:Ljava/util/List;

    return-object v0
.end method

.method final getFloatPropertyValue(I)F
    .locals 1
    .param p1, "index"    # I

    .line 539
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mFloatValues:[F

    aget v0, v0, p1

    return v0
.end method

.method final getIntPropertyValue(I)I
    .locals 1
    .param p1, "index"    # I

    .line 428
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mValues:[I

    aget v0, v0, p1

    return v0
.end method

.method public abstract getMaxValue()F
.end method

.method public final getProperties()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TPropertyT;>;"
        }
    .end annotation

    .line 559
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mPropertiesReadOnly:Ljava/util/List;

    return-object v0
.end method

.method public removeAllEffects()V
    .locals 1

    .line 605
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mEffects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 606
    return-void
.end method

.method public removeEffect(Landroidx/leanback/widget/ParallaxEffect;)V
    .locals 1
    .param p1, "effect"    # Landroidx/leanback/widget/ParallaxEffect;

    .line 598
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mEffects:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 599
    return-void
.end method

.method final setFloatPropertyValue(IF)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # F

    .line 549
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 552
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mFloatValues:[F

    aput p2, v0, p1

    .line 553
    return-void

    .line 550
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method final setIntPropertyValue(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # I

    .line 438
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 441
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mValues:[I

    aput p2, v0, p1

    .line 442
    return-void

    .line 439
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public updateValues()V
    .locals 2
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    .line 577
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Landroidx/leanback/widget/Parallax;->mEffects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 578
    iget-object v1, p0, Landroidx/leanback/widget/Parallax;->mEffects:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/leanback/widget/ParallaxEffect;

    invoke-virtual {v1, p0}, Landroidx/leanback/widget/ParallaxEffect;->performMapping(Landroidx/leanback/widget/Parallax;)V

    .line 577
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 580
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method final verifyFloatProperties()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 510
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 511
    return-void

    .line 513
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/leanback/widget/Parallax;->getFloatPropertyValue(I)F

    move-result v2

    .line 514
    .local v2, "last":F
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 515
    invoke-virtual {p0, v3}, Landroidx/leanback/widget/Parallax;->getFloatPropertyValue(I)F

    move-result v4

    .line 516
    .local v4, "v":F
    cmpg-float v5, v4, v2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x1

    if-ltz v5, :cond_3

    .line 521
    const v5, -0x800001

    cmpl-float v5, v2, v5

    if-nez v5, :cond_2

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v5, v4, v5

    if-eqz v5, :cond_1

    goto :goto_1

    .line 523
    :cond_1
    new-instance v5, Ljava/lang/IllegalStateException;

    new-array v7, v7, [Ljava/lang/Object;

    add-int/lit8 v9, v3, -0x1

    .line 525
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v0

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    add-int/lit8 v9, v3, -0x1

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    .line 526
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v1

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    .line 523
    const-string v0, "Parallax Property[%d]\"%s\" is UNKNOWN_BEFORE and Property[%d]\"%s\" is UNKNOWN_AFTER"

    invoke-static {v0, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 528
    :cond_2
    :goto_1
    move v2, v4

    .line 514
    .end local v4    # "v":F
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 517
    .restart local v4    # "v":F
    :cond_3
    new-instance v5, Ljava/lang/IllegalStateException;

    new-array v7, v7, [Ljava/lang/Object;

    .line 519
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v0

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    add-int/lit8 v0, v3, -0x1

    .line 520
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v1

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    add-int/lit8 v1, v3, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    .line 517
    const-string v0, "Parallax Property[%d]\"%s\" is smaller than Property[%d]\"%s\""

    invoke-static {v0, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 530
    .end local v3    # "i":I
    .end local v4    # "v":F
    :cond_4
    return-void
.end method

.method verifyIntProperties()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 488
    .local p0, "this":Landroidx/leanback/widget/Parallax;, "Landroidx/leanback/widget/Parallax<TPropertyT;>;"
    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 489
    return-void

    .line 491
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/leanback/widget/Parallax;->getIntPropertyValue(I)I

    move-result v2

    .line 492
    .local v2, "last":I
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 493
    invoke-virtual {p0, v3}, Landroidx/leanback/widget/Parallax;->getIntPropertyValue(I)I

    move-result v4

    .line 494
    .local v4, "v":I
    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x1

    if-lt v4, v2, :cond_3

    .line 499
    const/high16 v8, -0x80000000

    if-ne v2, v8, :cond_2

    const v8, 0x7fffffff

    if-eq v4, v8, :cond_1

    goto :goto_1

    .line 500
    :cond_1
    new-instance v8, Ljava/lang/IllegalStateException;

    new-array v6, v6, [Ljava/lang/Object;

    add-int/lit8 v9, v3, -0x1

    .line 502
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v0

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    add-int/lit8 v9, v3, -0x1

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    .line 503
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    .line 500
    const-string v0, "Parallax Property[%d]\"%s\" is UNKNOWN_BEFORE and Property[%d]\"%s\" is UNKNOWN_AFTER"

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 505
    :cond_2
    :goto_1
    move v2, v4

    .line 492
    .end local v4    # "v":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 495
    .restart local v4    # "v":I
    :cond_3
    new-instance v8, Ljava/lang/IllegalStateException;

    new-array v6, v6, [Ljava/lang/Object;

    .line 497
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v0

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    add-int/lit8 v0, v3, -0x1

    .line 498
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    iget-object v0, p0, Landroidx/leanback/widget/Parallax;->mProperties:Ljava/util/List;

    add-int/lit8 v1, v3, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Property;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    .line 495
    const-string v0, "Parallax Property[%d]\"%s\" is smaller than Property[%d]\"%s\""

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 507
    .end local v3    # "i":I
    .end local v4    # "v":I
    :cond_4
    return-void
.end method
