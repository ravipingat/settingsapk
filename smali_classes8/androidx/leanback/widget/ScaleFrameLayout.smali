.class public Landroidx/leanback/widget/ScaleFrameLayout;
.super Landroid/widget/FrameLayout;
.source "ScaleFrameLayout.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY_GROUP_PREFIX:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# static fields
.field private static final DEFAULT_CHILD_GRAVITY:I = 0x800033


# instance fields
.field private mChildScale:F

.field private mLayoutScaleX:F

.field private mLayoutScaleY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroidx/leanback/widget/ScaleFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroidx/leanback/widget/ScaleFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    .line 38
    iput v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    .line 40
    iput v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mChildScale:F

    .line 53
    return-void
.end method

.method private static getScaledMeasureSpec(IF)I
    .locals 2
    .param p0, "measureSpec"    # I
    .param p1, "scale"    # F

    .line 182
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    move v0, p0

    goto :goto_0

    .line 183
    :cond_0
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 184
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 182
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_0
    return v0
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 81
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 82
    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mChildScale:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 83
    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mChildScale:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 84
    return-void
.end method

.method protected addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;
    .param p4, "preventRequestLayout"    # Z

    .line 89
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    .line 90
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 91
    iget v1, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mChildScale:F

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    .line 92
    iget v1, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mChildScale:F

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    .line 94
    :cond_0
    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 20
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 99
    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getChildCount()I

    move-result v1

    .line 102
    .local v1, "count":I
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getLayoutDirection()I

    move-result v2

    .line 103
    .local v2, "layoutDirection":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 104
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPivotX()F

    move-result v5

    sub-float/2addr v4, v5

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPivotX()F

    move-result v4

    :goto_0
    nop

    .line 106
    .local v4, "pivotX":F
    iget v5, v0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    const/high16 v7, 0x3f000000    # 0.5f

    if-eqz v5, :cond_1

    .line 107
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingLeft()I

    move-result v5

    iget v8, v0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    div-float v9, v4, v8

    sub-float v9, v4, v9

    add-float/2addr v9, v7

    float-to-int v9, v9

    add-int/2addr v5, v9

    .line 108
    .local v5, "parentLeft":I
    sub-int v9, p4, p2

    int-to-float v9, v9

    sub-float/2addr v9, v4

    div-float/2addr v9, v8

    add-float/2addr v9, v4

    add-float/2addr v9, v7

    float-to-int v8, v9

    .line 109
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingRight()I

    move-result v9

    sub-int/2addr v8, v9

    .local v8, "parentRight":I
    goto :goto_1

    .line 111
    .end local v5    # "parentLeft":I
    .end local v8    # "parentRight":I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingLeft()I

    move-result v5

    .line 112
    .restart local v5    # "parentLeft":I
    sub-int v8, p4, p2

    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingRight()I

    move-result v9

    sub-int/2addr v8, v9

    .line 116
    .restart local v8    # "parentRight":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPivotY()F

    move-result v9

    .line 117
    .local v9, "pivotY":F
    iget v10, v0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    cmpl-float v6, v10, v6

    if-eqz v6, :cond_2

    .line 118
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingTop()I

    move-result v6

    iget v10, v0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    div-float v11, v9, v10

    sub-float v11, v9, v11

    add-float/2addr v11, v7

    float-to-int v11, v11

    add-int/2addr v6, v11

    .line 119
    .local v6, "parentTop":I
    sub-int v11, p5, p3

    int-to-float v11, v11

    sub-float/2addr v11, v9

    div-float/2addr v11, v10

    add-float/2addr v11, v9

    add-float/2addr v11, v7

    float-to-int v7, v11

    .line 120
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingBottom()I

    move-result v10

    sub-int/2addr v7, v10

    .local v7, "parentBottom":I
    goto :goto_2

    .line 122
    .end local v6    # "parentTop":I
    .end local v7    # "parentBottom":I
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingTop()I

    move-result v6

    .line 123
    .restart local v6    # "parentTop":I
    sub-int v7, p5, p3

    invoke-virtual/range {p0 .. p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getPaddingBottom()I

    move-result v10

    sub-int/2addr v7, v10

    .line 126
    .restart local v7    # "parentBottom":I
    :goto_2
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    if-ge v10, v1, :cond_a

    .line 127
    invoke-virtual {v0, v10}, Landroidx/leanback/widget/ScaleFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 128
    .local v11, "child":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_9

    .line 129
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/FrameLayout$LayoutParams;

    .line 131
    .local v12, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    .line 132
    .local v13, "width":I
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    .line 137
    .local v14, "height":I
    iget v15, v12, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 138
    .local v15, "gravity":I
    const/4 v3, -0x1

    if-ne v15, v3, :cond_3

    .line 139
    const v15, 0x800033

    .line 142
    :cond_3
    invoke-static {v15, v2}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v3

    .line 143
    .local v3, "absoluteGravity":I
    and-int/lit8 v0, v15, 0x70

    .line 145
    .local v0, "verticalGravity":I
    move/from16 v16, v1

    .end local v1    # "count":I
    .local v16, "count":I
    and-int/lit8 v1, v3, 0x7

    move/from16 v17, v2

    const/4 v2, 0x1

    .end local v2    # "layoutDirection":I
    .local v17, "layoutDirection":I
    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    .line 155
    iget v1, v12, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v5

    .local v1, "childLeft":I
    goto :goto_4

    .line 151
    .end local v1    # "childLeft":I
    :cond_4
    sub-int v1, v8, v13

    iget v2, v12, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    .line 152
    .restart local v1    # "childLeft":I
    goto :goto_4

    .line 147
    .end local v1    # "childLeft":I
    :cond_5
    sub-int v1, v8, v5

    sub-int/2addr v1, v13

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v5

    iget v2, v12, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v2, v12, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    .line 149
    .restart local v1    # "childLeft":I
    nop

    .line 158
    :goto_4
    const/16 v2, 0x10

    if-eq v0, v2, :cond_8

    const/16 v2, 0x30

    if-eq v0, v2, :cond_7

    const/16 v2, 0x50

    if-eq v0, v2, :cond_6

    .line 170
    iget v2, v12, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v2, v6

    move/from16 v18, v0

    .local v2, "childTop":I
    goto :goto_5

    .line 167
    .end local v2    # "childTop":I
    :cond_6
    sub-int v2, v7, v14

    move/from16 v18, v0

    .end local v0    # "verticalGravity":I
    .local v18, "verticalGravity":I
    iget v0, v12, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int/2addr v2, v0

    .line 168
    .restart local v2    # "childTop":I
    goto :goto_5

    .line 160
    .end local v2    # "childTop":I
    .end local v18    # "verticalGravity":I
    .restart local v0    # "verticalGravity":I
    :cond_7
    move/from16 v18, v0

    .end local v0    # "verticalGravity":I
    .restart local v18    # "verticalGravity":I
    iget v0, v12, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int v2, v6, v0

    .line 161
    .restart local v2    # "childTop":I
    goto :goto_5

    .line 163
    .end local v2    # "childTop":I
    .end local v18    # "verticalGravity":I
    .restart local v0    # "verticalGravity":I
    :cond_8
    move/from16 v18, v0

    .end local v0    # "verticalGravity":I
    .restart local v18    # "verticalGravity":I
    sub-int v0, v7, v6

    sub-int/2addr v0, v14

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v6

    iget v2, v12, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v2

    iget v2, v12, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v2, v0, v2

    .line 165
    .restart local v2    # "childTop":I
    nop

    .line 173
    :goto_5
    add-int v0, v1, v13

    move/from16 v19, v3

    .end local v3    # "absoluteGravity":I
    .local v19, "absoluteGravity":I
    add-int v3, v2, v14

    invoke-virtual {v11, v1, v2, v0, v3}, Landroid/view/View;->layout(IIII)V

    .line 175
    int-to-float v0, v1

    sub-float v0, v4, v0

    invoke-virtual {v11, v0}, Landroid/view/View;->setPivotX(F)V

    .line 176
    int-to-float v0, v2

    sub-float v0, v9, v0

    invoke-virtual {v11, v0}, Landroid/view/View;->setPivotY(F)V

    goto :goto_6

    .line 128
    .end local v12    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v13    # "width":I
    .end local v14    # "height":I
    .end local v15    # "gravity":I
    .end local v16    # "count":I
    .end local v17    # "layoutDirection":I
    .end local v18    # "verticalGravity":I
    .end local v19    # "absoluteGravity":I
    .local v1, "count":I
    .local v2, "layoutDirection":I
    :cond_9
    move/from16 v16, v1

    move/from16 v17, v2

    .line 126
    .end local v1    # "count":I
    .end local v2    # "layoutDirection":I
    .end local v11    # "child":Landroid/view/View;
    .restart local v16    # "count":I
    .restart local v17    # "layoutDirection":I
    :goto_6
    add-int/lit8 v10, v10, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    goto/16 :goto_3

    .line 179
    .end local v10    # "i":I
    .end local v16    # "count":I
    .end local v17    # "layoutDirection":I
    .restart local v1    # "count":I
    .restart local v2    # "layoutDirection":I
    :cond_a
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 189
    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 198
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_1

    .line 190
    :cond_1
    :goto_0
    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    .line 191
    invoke-static {p1, v0}, Landroidx/leanback/widget/ScaleFrameLayout;->getScaledMeasureSpec(IF)I

    move-result v0

    .line 192
    .local v0, "scaledWidthMeasureSpec":I
    iget v1, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    .line 193
    invoke-static {p2, v1}, Landroidx/leanback/widget/ScaleFrameLayout;->getScaledMeasureSpec(IF)I

    move-result v1

    .line 194
    .local v1, "scaledHeightMeasureSpec":I
    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 195
    invoke-virtual {p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 196
    invoke-virtual {p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    mul-float/2addr v4, v5

    add-float/2addr v4, v3

    float-to-int v3, v4

    .line 195
    invoke-virtual {p0, v2, v3}, Landroidx/leanback/widget/ScaleFrameLayout;->setMeasuredDimension(II)V

    .line 197
    .end local v0    # "scaledWidthMeasureSpec":I
    .end local v1    # "scaledHeightMeasureSpec":I
    nop

    .line 200
    :goto_1
    return-void
.end method

.method public setChildScale(F)V
    .locals 2
    .param p1, "scale"    # F

    .line 70
    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mChildScale:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 71
    iput p1, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mChildScale:F

    .line 72
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Landroidx/leanback/widget/ScaleFrameLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 73
    invoke-virtual {p0, v0}, Landroidx/leanback/widget/ScaleFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setScaleX(F)V

    .line 74
    invoke-virtual {p0, v0}, Landroidx/leanback/widget/ScaleFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setScaleY(F)V

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .line 207
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLayoutScaleX(F)V
    .locals 1
    .param p1, "scaleX"    # F

    .line 56
    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 57
    iput p1, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleX:F

    .line 58
    invoke-virtual {p0}, Landroidx/leanback/widget/ScaleFrameLayout;->requestLayout()V

    .line 60
    :cond_0
    return-void
.end method

.method public setLayoutScaleY(F)V
    .locals 1
    .param p1, "scaleY"    # F

    .line 63
    iget v0, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 64
    iput p1, p0, Landroidx/leanback/widget/ScaleFrameLayout;->mLayoutScaleY:F

    .line 65
    invoke-virtual {p0}, Landroidx/leanback/widget/ScaleFrameLayout;->requestLayout()V

    .line 67
    :cond_0
    return-void
.end method
