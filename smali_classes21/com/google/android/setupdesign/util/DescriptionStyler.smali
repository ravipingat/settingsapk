.class public Lcom/google/android/setupdesign/util/DescriptionStyler;
.super Ljava/lang/Object;
.source "DescriptionStyler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static applyPartnerCustomizationStyle(Landroid/widget/TextView;)V
    .locals 7
    .param p0, "description"    # Landroid/widget/TextView;

    .line 16
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 18
    .local v0, "context":Landroid/content/Context;
    nop

    .line 19
    invoke-static {v0}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->get(Landroid/content/Context;)Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;

    move-result-object v1

    sget-object v2, Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;->CONFIG_DESCRIPTION_TEXT_COLOR:Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;

    .line 20
    invoke-virtual {v1, v0, v2}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->getColor(Landroid/content/Context;Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;)I

    move-result v1

    .line 21
    .local v1, "descriptionTextColor":I
    if-eqz v1, :cond_0

    .line 22
    invoke-static {p0, v1}, Lcom/google/android/setupdesign/util/DescriptionStyler;->setTextColor(Landroid/widget/TextView;I)V

    .line 25
    :cond_0
    nop

    .line 26
    invoke-static {v0}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->get(Landroid/content/Context;)Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;

    move-result-object v2

    sget-object v3, Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;->CONFIG_DESCRIPTION_LINK_TEXT_COLOR:Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;

    .line 27
    invoke-virtual {v2, v0, v3}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->getColor(Landroid/content/Context;Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;)I

    move-result v2

    .line 28
    .local v2, "descriptionLinkTextColor":I
    if-eqz v2, :cond_1

    .line 29
    invoke-static {p0, v2}, Lcom/google/android/setupdesign/util/DescriptionStyler;->setLinkTextColor(Landroid/widget/TextView;I)V

    .line 32
    :cond_1
    nop

    .line 33
    invoke-static {v0}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->get(Landroid/content/Context;)Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;

    move-result-object v3

    sget-object v4, Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;->CONFIG_DESCRIPTION_TEXT_SIZE:Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;

    .line 34
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->getDimension(Landroid/content/Context;Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;F)F

    move-result v3

    .line 35
    .local v3, "descriptionTextSize":F
    cmpl-float v4, v3, v5

    if-eqz v4, :cond_2

    .line 36
    invoke-static {p0, v3}, Lcom/google/android/setupdesign/util/DescriptionStyler;->setTextSize(Landroid/widget/TextView;F)V

    .line 39
    :cond_2
    nop

    .line 40
    invoke-static {v0}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->get(Landroid/content/Context;)Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;

    move-result-object v4

    sget-object v5, Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;->CONFIG_DESCRIPTION_FONT_FAMILY:Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;

    .line 41
    invoke-virtual {v4, v0, v5}, Lcom/google/android/setupcompat/partnerconfig/PartnerConfigHelper;->getString(Landroid/content/Context;Lcom/google/android/setupcompat/partnerconfig/PartnerConfig;)Ljava/lang/String;

    move-result-object v4

    .line 42
    .local v4, "fontFamilyName":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    .line 43
    .local v5, "font":Landroid/graphics/Typeface;
    if-eqz v5, :cond_3

    .line 44
    invoke-static {p0, v5}, Lcom/google/android/setupdesign/util/DescriptionStyler;->setFontFamily(Landroid/widget/TextView;Landroid/graphics/Typeface;)V

    .line 47
    :cond_3
    invoke-static {v0}, Lcom/google/android/setupdesign/util/PartnerStyleHelper;->getLayoutGravity(Landroid/content/Context;)I

    move-result v6

    invoke-static {p0, v6}, Lcom/google/android/setupdesign/util/DescriptionStyler;->setGravity(Landroid/widget/TextView;I)V

    .line 48
    return-void
.end method

.method static setFontFamily(Landroid/widget/TextView;Landroid/graphics/Typeface;)V
    .locals 0
    .param p0, "description"    # Landroid/widget/TextView;
    .param p1, "fontFamily"    # Landroid/graphics/Typeface;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 59
    if-eqz p0, :cond_0

    .line 60
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 62
    :cond_0
    return-void
.end method

.method static setGravity(Landroid/widget/TextView;I)V
    .locals 0
    .param p0, "description"    # Landroid/widget/TextView;
    .param p1, "gravity"    # I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 80
    if-eqz p0, :cond_0

    .line 81
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    .line 83
    :cond_0
    return-void
.end method

.method static setLinkTextColor(Landroid/widget/TextView;I)V
    .locals 0
    .param p0, "description"    # Landroid/widget/TextView;
    .param p1, "color"    # I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 73
    if-eqz p0, :cond_0

    .line 74
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 76
    :cond_0
    return-void
.end method

.method static setTextColor(Landroid/widget/TextView;I)V
    .locals 0
    .param p0, "description"    # Landroid/widget/TextView;
    .param p1, "color"    # I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 66
    if-eqz p0, :cond_0

    .line 67
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 69
    :cond_0
    return-void
.end method

.method static setTextSize(Landroid/widget/TextView;F)V
    .locals 1
    .param p0, "description"    # Landroid/widget/TextView;
    .param p1, "size"    # F
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 52
    if-eqz p0, :cond_0

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 55
    :cond_0
    return-void
.end method
