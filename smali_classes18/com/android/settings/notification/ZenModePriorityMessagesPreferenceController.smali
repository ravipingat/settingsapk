.class public Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;
.super Lcom/android/settings/notification/AbstractZenModePreferenceController;
.source "ZenModePriorityMessagesPreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field protected static final KEY:Ljava/lang/String; = "zen_mode_messages"


# instance fields
.field private final mBackend:Lcom/android/settings/notification/ZenModeBackend;

.field private final mListValues:[Ljava/lang/String;

.field private mPreference:Landroidx/preference/ListPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lifecycle"    # Lcom/android/settingslib/core/lifecycle/Lifecycle;

    .line 41
    const-string v0, "zen_mode_messages"

    invoke-direct {p0, p1, v0, p2}, Lcom/android/settings/notification/AbstractZenModePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    .line 42
    invoke-static {p1}, Lcom/android/settings/notification/ZenModeBackend;->getInstance(Landroid/content/Context;)Lcom/android/settings/notification/ZenModeBackend;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mBackend:Lcom/android/settings/notification/ZenModeBackend;

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030140

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mListValues:[Ljava/lang/String;

    .line 44
    return-void
.end method

.method private updateFromContactsValue(Landroidx/preference/Preference;)V
    .locals 4
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 77
    move-object v0, p1

    check-cast v0, Landroidx/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    .line 78
    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->getZenMode()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 87
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    .line 88
    iget-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mBackend:Lcom/android/settings/notification/ZenModeBackend;

    invoke-virtual {v0, v2}, Lcom/android/settings/notification/ZenModeBackend;->getContactsSummary(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setSummary(I)V

    .line 91
    iget-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mBackend:Lcom/android/settings/notification/ZenModeBackend;

    .line 92
    invoke-virtual {v0}, Lcom/android/settings/notification/ZenModeBackend;->getPriorityMessageSenders()I

    move-result v0

    .line 91
    invoke-static {v0}, Lcom/android/settings/notification/ZenModeBackend;->getKeyFromSetting(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "currentVal":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mListValues:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->getIndexOfSendersValue(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 81
    .end local v0    # "currentVal":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setEnabled(Z)V

    .line 82
    iget-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    const-string v1, "zen_mode_from_none"

    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mBackend:Lcom/android/settings/notification/ZenModeBackend;

    invoke-virtual {v1, v2}, Lcom/android/settings/notification/ZenModeBackend;->getAlarmsTotalSilenceCallsMessagesSummary(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setSummary(I)V

    .line 85
    nop

    .line 95
    :goto_0
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 58
    invoke-super {p0, p1}, Lcom/android/settings/notification/AbstractZenModePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 59
    const-string v0, "zen_mode_messages"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    .line 60
    return-void
.end method

.method protected getIndexOfSendersValue(Ljava/lang/String;)I
    .locals 4
    .param p1, "currentVal"    # Ljava/lang/String;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 99
    const/4 v0, 0x3

    .line 100
    .local v0, "index":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mListValues:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 101
    aget-object v2, v2, v1

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    return v1

    .line 100
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 106
    .end local v1    # "i":I
    :cond_1
    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 48
    const-string v0, "zen_mode_messages"

    return-object v0
.end method

.method public isAvailable()Z
    .locals 1

    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "selectedContactsFrom"    # Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->mBackend:Lcom/android/settings/notification/ZenModeBackend;

    .line 71
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/notification/ZenModeBackend;->getSettingFromPrefKey(Ljava/lang/String;)I

    move-result v1

    .line 70
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/notification/ZenModeBackend;->saveSenders(II)V

    .line 72
    invoke-direct {p0, p1}, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->updateFromContactsValue(Landroidx/preference/Preference;)V

    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 0
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 64
    invoke-super {p0, p1}, Lcom/android/settings/notification/AbstractZenModePreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 65
    invoke-direct {p0, p1}, Lcom/android/settings/notification/ZenModePriorityMessagesPreferenceController;->updateFromContactsValue(Landroidx/preference/Preference;)V

    .line 66
    return-void
.end method
