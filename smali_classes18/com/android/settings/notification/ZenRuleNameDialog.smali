.class public Lcom/android/settings/notification/ZenRuleNameDialog;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;
.source "ZenRuleNameDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/notification/ZenRuleNameDialog$PositiveClickListener;
    }
.end annotation


# static fields
.field private static final EXTRA_CONDITION_ID:Ljava/lang/String; = "extra_zen_condition_id"

.field private static final EXTRA_ZEN_RULE_NAME:Ljava/lang/String; = "zen_rule_name"

.field protected static final TAG:Ljava/lang/String; = "ZenRuleNameDialog"

.field protected static mPositiveClickListener:Lcom/android/settings/notification/ZenRuleNameDialog$PositiveClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/notification/ZenRuleNameDialog;Landroid/widget/EditText;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/notification/ZenRuleNameDialog;
    .param p1, "x1"    # Landroid/widget/EditText;

    .line 37
    invoke-direct {p0, p1}, Lcom/android/settings/notification/ZenRuleNameDialog;->trimmedText(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTitleResource(Landroid/net/Uri;Z)I
    .locals 3
    .param p1, "conditionId"    # Landroid/net/Uri;
    .param p2, "isNew"    # Z

    .line 114
    invoke-static {p1}, Landroid/service/notification/ZenModeConfig;->isValidEventConditionId(Landroid/net/Uri;)Z

    move-result v0

    .line 115
    .local v0, "isEvent":Z
    invoke-static {p1}, Landroid/service/notification/ZenModeConfig;->isValidScheduleConditionId(Landroid/net/Uri;)Z

    move-result v1

    .line 116
    .local v1, "isTime":Z
    const v2, 0x7f12199f

    .line 117
    .local v2, "titleResource":I
    if-eqz p2, :cond_1

    .line 118
    if-eqz v0, :cond_0

    .line 119
    const v2, 0x7f121928

    goto :goto_0

    .line 120
    :cond_0
    if-eqz v1, :cond_1

    .line 121
    const v2, 0x7f12192a

    .line 124
    :cond_1
    :goto_0
    return v2
.end method

.method public static show(Landroidx/fragment/app/Fragment;Ljava/lang/String;Landroid/net/Uri;Lcom/android/settings/notification/ZenRuleNameDialog$PositiveClickListener;)V
    .locals 4
    .param p0, "parent"    # Landroidx/fragment/app/Fragment;
    .param p1, "ruleName"    # Ljava/lang/String;
    .param p2, "conditionId"    # Landroid/net/Uri;
    .param p3, "listener"    # Lcom/android/settings/notification/ZenRuleNameDialog$PositiveClickListener;

    .line 57
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "zen_rule_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v1, "extra_zen_condition_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 60
    sput-object p3, Lcom/android/settings/notification/ZenRuleNameDialog;->mPositiveClickListener:Lcom/android/settings/notification/ZenRuleNameDialog$PositiveClickListener;

    .line 62
    new-instance v1, Lcom/android/settings/notification/ZenRuleNameDialog;

    invoke-direct {v1}, Lcom/android/settings/notification/ZenRuleNameDialog;-><init>()V

    .line 63
    .local v1, "dialog":Lcom/android/settings/notification/ZenRuleNameDialog;
    invoke-virtual {v1, v0}, Lcom/android/settings/notification/ZenRuleNameDialog;->setArguments(Landroid/os/Bundle;)V

    .line 64
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/android/settings/notification/ZenRuleNameDialog;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    .line 65
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "ZenRuleNameDialog"

    invoke-virtual {v1, v2, v3}, Lcom/android/settings/notification/ZenRuleNameDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method private trimmedText(Landroid/widget/EditText;)Ljava/lang/String;
    .locals 1
    .param p1, "editText"    # Landroid/widget/EditText;

    .line 110
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 45
    const/16 v0, 0x4f5

    return v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 70
    invoke-virtual {p0}, Lcom/android/settings/notification/ZenRuleNameDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 71
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v1, "extra_zen_condition_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 72
    .local v1, "conditionId":Landroid/net/Uri;
    const-string v2, "zen_rule_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 74
    .local v2, "ruleName":Ljava/lang/String;
    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    move v5, v3

    goto :goto_0

    :cond_0
    move v5, v4

    .line 75
    .local v5, "isNew":Z
    :goto_0
    move-object v6, v2

    .line 76
    .local v6, "originalRuleName":Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/android/settings/notification/ZenRuleNameDialog;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 77
    .local v7, "context":Landroid/content/Context;
    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f0d0361

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 79
    .local v4, "v":Landroid/view/View;
    const v8, 0x7f0a079a

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    .line 80
    .local v8, "editText":Landroid/widget/EditText;
    if-nez v5, :cond_1

    .line 82
    invoke-virtual {v8, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setSelection(I)V

    .line 86
    :cond_1
    invoke-virtual {v8, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 87
    new-instance v3, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v3, v7}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 88
    invoke-direct {p0, v1, v5}, Lcom/android/settings/notification/ZenRuleNameDialog;->getTitleResource(Landroid/net/Uri;Z)I

    move-result v9

    invoke-virtual {v3, v9}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v3

    .line 89
    invoke-virtual {v3, v4}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v3

    .line 90
    if-eqz v5, :cond_2

    const v9, 0x7f121927

    goto :goto_1

    :cond_2
    const v9, 0x7f120b98

    :goto_1
    new-instance v11, Lcom/android/settings/notification/ZenRuleNameDialog$1;

    invoke-direct {v11, p0, v8, v5, v6}, Lcom/android/settings/notification/ZenRuleNameDialog$1;-><init>(Lcom/android/settings/notification/ZenRuleNameDialog;Landroid/widget/EditText;ZLjava/lang/CharSequence;)V

    invoke-virtual {v3, v9, v11}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v3

    const v9, 0x7f1203d8

    .line 105
    invoke-virtual {v3, v9, v10}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v3

    .line 106
    invoke-virtual {v3}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v3

    .line 87
    return-object v3
.end method
