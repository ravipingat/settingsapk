.class public Lcom/android/settings/notification/BubblePreferenceController;
.super Lcom/android/settings/notification/NotificationPreferenceController;
.source "BubblePreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final KEY:Ljava/lang/String; = "bubble_pref"

.field static final SYSTEM_WIDE_OFF:I = 0x0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field static final SYSTEM_WIDE_ON:I = 0x1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "BubblePrefContr"


# instance fields
.field private mFragmentManager:Landroidx/fragment/app/FragmentManager;

.field private mIsAppPage:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lcom/android/settings/notification/NotificationBackend;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragmentManager"    # Landroidx/fragment/app/FragmentManager;
    .param p3, "backend"    # Lcom/android/settings/notification/NotificationBackend;
    .param p4, "isAppPage"    # Z

    .line 48
    invoke-direct {p0, p1, p3}, Lcom/android/settings/notification/NotificationPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/notification/NotificationBackend;)V

    .line 49
    iput-object p2, p0, Lcom/android/settings/notification/BubblePreferenceController;->mFragmentManager:Landroidx/fragment/app/FragmentManager;

    .line 50
    iput-boolean p4, p0, Lcom/android/settings/notification/BubblePreferenceController;->mIsAppPage:Z

    .line 51
    return-void
.end method

.method public static applyBubblesApproval(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 133
    new-instance v0, Lcom/android/settings/notification/NotificationBackend;

    invoke-direct {v0}, Lcom/android/settings/notification/NotificationBackend;-><init>()V

    .line 134
    .local v0, "backend":Lcom/android/settings/notification/NotificationBackend;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/settings/notification/NotificationBackend;->setAllowBubbles(Ljava/lang/String;IZ)Z

    .line 137
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "notification_bubbles"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 139
    return-void
.end method

.method private isGloballyEnabled()Z
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/android/settings/notification/BubblePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "notification_bubbles"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method public static revertBubblesApproval(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 123
    new-instance v0, Lcom/android/settings/notification/NotificationBackend;

    invoke-direct {v0}, Lcom/android/settings/notification/NotificationBackend;-><init>()V

    .line 124
    .local v0, "backend":Lcom/android/settings/notification/NotificationBackend;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/settings/notification/NotificationBackend;->setAllowBubbles(Ljava/lang/String;IZ)Z

    .line 127
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "notification_bubbles"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 129
    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 55
    const-string v0, "bubble_pref"

    return-object v0
.end method

.method public isAvailable()Z
    .locals 3

    .line 60
    invoke-super {p0}, Lcom/android/settings/notification/NotificationPreferenceController;->isAvailable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 61
    return v1

    .line 63
    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/notification/BubblePreferenceController;->mIsAppPage:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/notification/BubblePreferenceController;->isGloballyEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    return v1

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/BubblePreferenceController;->mChannel:Landroid/app/NotificationChannel;

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    .line 67
    invoke-virtual {p0}, Lcom/android/settings/notification/BubblePreferenceController;->isDefaultChannel()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    return v2

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->allowBubbles:Z

    if-eqz v0, :cond_3

    move v1, v2

    :cond_3
    return v1

    .line 73
    :cond_4
    return v2
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 93
    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/BubblePreferenceController;->isGloballyEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 94
    .local v0, "value":Z
    :goto_0
    iget-object v3, p0, Lcom/android/settings/notification/BubblePreferenceController;->mChannel:Landroid/app/NotificationChannel;

    if-eqz v3, :cond_1

    .line 95
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mChannel:Landroid/app/NotificationChannel;

    invoke-virtual {v1, v0}, Landroid/app/NotificationChannel;->setAllowBubbles(Z)V

    .line 96
    invoke-virtual {p0}, Lcom/android/settings/notification/BubblePreferenceController;->saveChannel()V

    .line 97
    return v2

    .line 98
    :cond_1
    iget-object v3, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/notification/BubblePreferenceController;->mFragmentManager:Landroidx/fragment/app/FragmentManager;

    if-eqz v3, :cond_3

    .line 99
    move-object v3, p1

    check-cast v3, Lcom/android/settingslib/RestrictedSwitchPreference;

    .line 102
    .local v3, "pref":Lcom/android/settingslib/RestrictedSwitchPreference;
    invoke-direct {p0}, Lcom/android/settings/notification/BubblePreferenceController;->isGloballyEnabled()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/android/settingslib/RestrictedSwitchPreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_2

    .line 103
    new-instance v2, Lcom/android/settings/notification/BubbleWarningDialogFragment;

    invoke-direct {v2}, Lcom/android/settings/notification/BubbleWarningDialogFragment;-><init>()V

    iget-object v4, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v4, v4, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget v5, v5, Lcom/android/settings/notification/NotificationBackend$AppRow;->uid:I

    .line 104
    invoke-virtual {v2, v4, v5}, Lcom/android/settings/notification/BubbleWarningDialogFragment;->setPkgInfo(Ljava/lang/String;I)Lcom/android/settings/notification/BubbleWarningDialogFragment;

    move-result-object v2

    iget-object v4, p0, Lcom/android/settings/notification/BubblePreferenceController;->mFragmentManager:Landroidx/fragment/app/FragmentManager;

    .line 105
    const-string v5, "dialog"

    invoke-virtual {v2, v4, v5}, Lcom/android/settings/notification/BubbleWarningDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 106
    return v1

    .line 108
    :cond_2
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iput-boolean v0, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->allowBubbles:Z

    .line 109
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mBackend:Lcom/android/settings/notification/NotificationBackend;

    iget-object v4, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v4, v4, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget v5, v5, Lcom/android/settings/notification/NotificationBackend$AppRow;->uid:I

    invoke-virtual {v1, v4, v5, v0}, Lcom/android/settings/notification/NotificationBackend;->setAllowBubbles(Ljava/lang/String;IZ)Z

    .line 112
    .end local v3    # "pref":Lcom/android/settingslib/RestrictedSwitchPreference;
    :cond_3
    return v2
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 6
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 77
    iget-object v0, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    if-eqz v0, :cond_3

    .line 78
    move-object v0, p1

    check-cast v0, Lcom/android/settingslib/RestrictedSwitchPreference;

    .line 79
    .local v0, "pref":Lcom/android/settingslib/RestrictedSwitchPreference;
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    .line 80
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mChannel:Landroid/app/NotificationChannel;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    .line 81
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mChannel:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->canBubble()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/BubblePreferenceController;->isGloballyEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {v0, v2}, Lcom/android/settingslib/RestrictedSwitchPreference;->setChecked(Z)V

    .line 82
    invoke-virtual {v0}, Lcom/android/settingslib/RestrictedSwitchPreference;->isDisabledByAdmin()Z

    move-result v1

    xor-int/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedSwitchPreference;->setEnabled(Z)V

    goto :goto_1

    .line 84
    :cond_1
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->allowBubbles:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/settings/notification/BubblePreferenceController;->isGloballyEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedSwitchPreference;->setChecked(Z)V

    .line 85
    iget-object v1, p0, Lcom/android/settings/notification/BubblePreferenceController;->mContext:Landroid/content/Context;

    const v4, 0x7f1203a9

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/notification/BubblePreferenceController;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v5, v5, Lcom/android/settings/notification/NotificationBackend$AppRow;->label:Ljava/lang/CharSequence;

    aput-object v5, v3, v2

    invoke-virtual {v1, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedSwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 89
    .end local v0    # "pref":Lcom/android/settingslib/RestrictedSwitchPreference;
    :cond_3
    :goto_1
    return-void
.end method
