.class public Lcom/android/settings/notification/ZenModeDurationPreferenceController;
.super Lcom/android/settings/notification/AbstractZenModePreferenceController;
.source "ZenModeDurationPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;


# static fields
.field protected static final KEY:Ljava/lang/String; = "zen_mode_duration_settings"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lifecycle"    # Lcom/android/settingslib/core/lifecycle/Lifecycle;

    .line 31
    const-string v0, "zen_mode_duration_settings"

    invoke-direct {p0, p1, v0, p2}, Lcom/android/settings/notification/AbstractZenModePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    .line 32
    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 41
    const-string v0, "zen_mode_duration_settings"

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 7

    .line 47
    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeDurationPreferenceController;->getZenDuration()I

    move-result v0

    .line 48
    .local v0, "zenDuration":I
    if-gez v0, :cond_0

    .line 51
    iget-object v1, p0, Lcom/android/settings/notification/ZenModeDurationPreferenceController;->mContext:Landroid/content/Context;

    const v2, 0x7f120fed

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .local v1, "summary":Ljava/lang/String;
    goto :goto_0

    .line 53
    .end local v1    # "summary":Ljava/lang/String;
    :cond_0
    if-nez v0, :cond_1

    .line 54
    iget-object v1, p0, Lcom/android/settings/notification/ZenModeDurationPreferenceController;->mContext:Landroid/content/Context;

    const v2, 0x7f121967

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "summary":Ljava/lang/String;
    goto :goto_0

    .line 56
    .end local v1    # "summary":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x3c

    if-lt v0, v3, :cond_2

    .line 57
    div-int/lit8 v3, v0, 0x3c

    .line 58
    .local v3, "hours":I
    iget-object v4, p0, Lcom/android/settings/notification/ZenModeDurationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100055

    new-array v2, v2, [Ljava/lang/Object;

    .line 59
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v1

    .line 58
    invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .end local v3    # "hours":I
    .restart local v1    # "summary":Ljava/lang/String;
    goto :goto_0

    .line 61
    .end local v1    # "summary":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/android/settings/notification/ZenModeDurationPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f121968

    new-array v2, v2, [Ljava/lang/Object;

    .line 62
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    .line 61
    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 66
    .restart local v1    # "summary":Ljava/lang/String;
    :goto_0
    return-object v1
.end method

.method public isAvailable()Z
    .locals 1

    .line 36
    const/4 v0, 0x1

    return v0
.end method
