.class public Lcom/android/settings/notification/ImportancePreference;
.super Landroidx/preference/Preference;
.source "ImportancePreference.java"


# static fields
.field private static final BUTTON_ANIM_TIME_MS:I = 0x64

.field private static final SHOW_BUTTON_SUMMARY:Z = false


# instance fields
.field private mAlertButton:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mDisplayInStatusBar:Z

.field private mDisplayOnLockscreen:Z

.field private mImportance:I

.field private mIsConfigurable:Z

.field private mSilenceButton:Landroid/view/View;

.field selectedBackground:Landroid/graphics/drawable/Drawable;

.field unselectedBackground:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    invoke-direct {p0, p1}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/notification/ImportancePreference;->mIsConfigurable:Z

    .line 77
    invoke-direct {p0, p1}, Lcom/android/settings/notification/ImportancePreference;->init(Landroid/content/Context;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 71
    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/notification/ImportancePreference;->mIsConfigurable:Z

    .line 72
    invoke-direct {p0, p1}, Lcom/android/settings/notification/ImportancePreference;->init(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/notification/ImportancePreference;->mIsConfigurable:Z

    .line 67
    invoke-direct {p0, p1}, Lcom/android/settings/notification/ImportancePreference;->init(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/notification/ImportancePreference;->mIsConfigurable:Z

    .line 62
    invoke-direct {p0, p1}, Lcom/android/settings/notification/ImportancePreference;->init(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method private getAccentTint()Landroid/content/res/ColorStateList;
    .locals 1

    .line 158
    invoke-virtual {p0}, Lcom/android/settings/notification/ImportancePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/Utils;->getColorAccent(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private getRegularTint()Landroid/content/res/ColorStateList;
    .locals 2

    .line 162
    invoke-virtual {p0}, Lcom/android/settings/notification/ImportancePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x1010036

    invoke-static {v0, v1}, Lcom/android/settings/Utils;->getColorAttr(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 81
    iput-object p1, p0, Lcom/android/settings/notification/ImportancePreference;->mContext:Landroid/content/Context;

    .line 82
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mContext:Landroid/content/Context;

    const v1, 0x7f08014e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->selectedBackground:Landroid/graphics/drawable/Drawable;

    .line 83
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mContext:Landroid/content/Context;

    const v1, 0x7f08014f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->unselectedBackground:Landroid/graphics/drawable/Drawable;

    .line 84
    const v0, 0x7f0d0131

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ImportancePreference;->setLayoutResource(I)V

    .line 85
    return-void
.end method


# virtual methods
.method public synthetic lambda$onBindViewHolder$0$ImportancePreference()V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 142
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 143
    return-void
.end method

.method public synthetic lambda$onBindViewHolder$1$ImportancePreference(Landroidx/preference/PreferenceViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "holder"    # Landroidx/preference/PreferenceViewHolder;
    .param p2, "v"    # Landroid/view/View;

    .line 133
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ImportancePreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 134
    iget-object v1, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->unselectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 135
    iget-object v1, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->selectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 136
    iget-object v1, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/android/settings/notification/ImportancePreference;->setImportanceSummary(Landroid/view/ViewGroup;IZ)V

    .line 140
    iget-object v0, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$tOS00Rm2-rgeNiYuwn9gyX0N6ow;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$tOS00Rm2-rgeNiYuwn9gyX0N6ow;-><init>(Lcom/android/settings/notification/ImportancePreference;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 144
    return-void
.end method

.method public synthetic lambda$onBindViewHolder$2$ImportancePreference()V
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 152
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 153
    return-void
.end method

.method public synthetic lambda$onBindViewHolder$3$ImportancePreference(Landroidx/preference/PreferenceViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "holder"    # Landroidx/preference/PreferenceViewHolder;
    .param p2, "v"    # Landroid/view/View;

    .line 146
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ImportancePreference;->callChangeListener(Ljava/lang/Object;)Z

    .line 147
    iget-object v1, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->unselectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 148
    iget-object v1, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->selectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 149
    iget-object v1, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/android/settings/notification/ImportancePreference;->setImportanceSummary(Landroid/view/ViewGroup;IZ)V

    .line 150
    iget-object v0, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$eik8Nolg6tRrn_QPd_sKGR2c5G4;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$eik8Nolg6tRrn_QPd_sKGR2c5G4;-><init>(Lcom/android/settings/notification/ImportancePreference;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 154
    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroidx/preference/PreferenceViewHolder;

    .line 105
    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 106
    iget-object v0, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 108
    const v0, 0x7f0a0621

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    .line 109
    const v0, 0x7f0a007a

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    .line 111
    iget-boolean v0, p0, Lcom/android/settings/notification/ImportancePreference;->mIsConfigurable:Z

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 113
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 116
    :cond_0
    iget-object v0, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget v2, p0, Lcom/android/settings/notification/ImportancePreference;->mImportance:I

    invoke-virtual {p0, v0, v2, v1}, Lcom/android/settings/notification/ImportancePreference;->setImportanceSummary(Landroid/view/ViewGroup;IZ)V

    .line 117
    iget v0, p0, Lcom/android/settings/notification/ImportancePreference;->mImportance:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 126
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->unselectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->selectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 128
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->unselectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 121
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/notification/ImportancePreference;->selectedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 123
    nop

    .line 132
    :goto_0
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mSilenceButton:Landroid/view/View;

    new-instance v1, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$0vzlC8JJzQy-Rg1hLzfwlza2t4M;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$0vzlC8JJzQy-Rg1hLzfwlza2t4M;-><init>(Lcom/android/settings/notification/ImportancePreference;Landroidx/preference/PreferenceViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lcom/android/settings/notification/ImportancePreference;->mAlertButton:Landroid/view/View;

    new-instance v1, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$GLzsWt7BiNJKqlCW3XDg8dS51DI;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/notification/-$$Lambda$ImportancePreference$GLzsWt7BiNJKqlCW3XDg8dS51DI;-><init>(Lcom/android/settings/notification/ImportancePreference;Landroidx/preference/PreferenceViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    return-void
.end method

.method public setConfigurable(Z)V
    .locals 0
    .param p1, "configurable"    # Z

    .line 92
    iput-boolean p1, p0, Lcom/android/settings/notification/ImportancePreference;->mIsConfigurable:Z

    .line 93
    return-void
.end method

.method public setDisplayInStatusBar(Z)V
    .locals 0
    .param p1, "display"    # Z

    .line 96
    iput-boolean p1, p0, Lcom/android/settings/notification/ImportancePreference;->mDisplayInStatusBar:Z

    .line 97
    return-void
.end method

.method public setDisplayOnLockscreen(Z)V
    .locals 0
    .param p1, "display"    # Z

    .line 100
    iput-boolean p1, p0, Lcom/android/settings/notification/ImportancePreference;->mDisplayOnLockscreen:Z

    .line 101
    return-void
.end method

.method public setImportance(I)V
    .locals 0
    .param p1, "importance"    # I

    .line 88
    iput p1, p0, Lcom/android/settings/notification/ImportancePreference;->mImportance:I

    .line 89
    return-void
.end method

.method setImportanceSummary(Landroid/view/ViewGroup;IZ)V
    .locals 11
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "importance"    # I
    .param p3, "fromUser"    # Z

    .line 166
    if-eqz p3, :cond_0

    .line 167
    new-instance v0, Landroid/transition/AutoTransition;

    invoke-direct {v0}, Landroid/transition/AutoTransition;-><init>()V

    .line 168
    .local v0, "transition":Landroid/transition/AutoTransition;
    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/transition/AutoTransition;->setDuration(J)Landroid/transition/TransitionSet;

    .line 169
    invoke-static {p1, v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 172
    .end local v0    # "transition":Landroid/transition/AutoTransition;
    :cond_0
    invoke-direct {p0}, Lcom/android/settings/notification/ImportancePreference;->getAccentTint()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 173
    .local v0, "colorAccent":Landroid/content/res/ColorStateList;
    invoke-direct {p0}, Lcom/android/settings/notification/ImportancePreference;->getRegularTint()Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 175
    .local v1, "colorNormal":Landroid/content/res/ColorStateList;
    const/4 v2, 0x3

    const/4 v3, 0x0

    const v4, 0x7f0a0624

    const v5, 0x7f0a0623

    const v6, 0x7f0a0622

    const v7, 0x7f0a007d

    const v8, 0x7f0a007c

    const/16 v9, 0x8

    const v10, 0x7f0a007e

    if-lt p2, v2, :cond_1

    .line 176
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 177
    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 178
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 180
    invoke-virtual {p1, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 181
    invoke-virtual {p1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 183
    invoke-virtual {p1, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 185
    :cond_1
    invoke-virtual {p1, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 186
    invoke-virtual {p1, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 187
    invoke-virtual {p1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 189
    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 190
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 191
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 193
    :goto_0
    return-void
.end method
