.class Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;
.super Landroid/database/ContentObserver;
.source "OPZenModeTurnOnSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/notification/OPZenModeTurnOnSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingObserver"
.end annotation


# instance fields
.field private final ZEN_MODE_CONFIG_ETAG_URI:Landroid/net/Uri;

.field private final ZEN_MODE_DURATION_URI:Landroid/net/Uri;

.field private final ZEN_MODE_URI:Landroid/net/Uri;

.field private final mPreference:Landroidx/preference/Preference;

.field final synthetic this$0:Lcom/android/settings/notification/OPZenModeTurnOnSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/notification/OPZenModeTurnOnSettings;Landroidx/preference/Preference;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/settings/notification/OPZenModeTurnOnSettings;
    .param p2, "preference"    # Landroidx/preference/Preference;

    .line 153
    iput-object p1, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->this$0:Lcom/android/settings/notification/OPZenModeTurnOnSettings;

    .line 154
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 145
    const-string v0, "zen_mode"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_URI:Landroid/net/Uri;

    .line 146
    const-string v0, "zen_mode_config_etag"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_CONFIG_ETAG_URI:Landroid/net/Uri;

    .line 148
    const-string v0, "zen_duration"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_DURATION_URI:Landroid/net/Uri;

    .line 155
    iput-object p2, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->mPreference:Landroidx/preference/Preference;

    .line 156
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 170
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 171
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_URI:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_CONFIG_ETAG_URI:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_DURATION_URI:Landroid/net/Uri;

    .line 172
    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->this$0:Lcom/android/settings/notification/OPZenModeTurnOnSettings;

    iget-object v0, v0, Lcom/android/settings/notification/OPZenModeTurnOnSettings;->mBackend:Lcom/android/settings/notification/ZenModeBackend;

    invoke-virtual {v0}, Lcom/android/settings/notification/ZenModeBackend;->updatePolicy()V

    .line 174
    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->this$0:Lcom/android/settings/notification/OPZenModeTurnOnSettings;

    iget-object v0, v0, Lcom/android/settings/notification/OPZenModeTurnOnSettings;->mBackend:Lcom/android/settings/notification/ZenModeBackend;

    invoke-virtual {v0}, Lcom/android/settings/notification/ZenModeBackend;->updateZenMode()V

    .line 178
    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->this$0:Lcom/android/settings/notification/OPZenModeTurnOnSettings;

    iget-object v1, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/OPZenModeTurnOnSettings;->updateState(Landroidx/preference/Preference;)V

    .line 180
    :cond_1
    return-void
.end method

.method public register(Landroid/content/ContentResolver;)V
    .locals 3
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .line 159
    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_URI:Landroid/net/Uri;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 160
    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_CONFIG_ETAG_URI:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 161
    iget-object v0, p0, Lcom/android/settings/notification/OPZenModeTurnOnSettings$SettingObserver;->ZEN_MODE_DURATION_URI:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 162
    return-void
.end method

.method public unregister(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .line 165
    invoke-virtual {p1, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 166
    return-void
.end method
