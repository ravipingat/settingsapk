.class public Lcom/android/settings/notification/ZenModeBypassingAppsSettings;
.super Lcom/android/settings/notification/ZenModeSettingsBase;
.source "ZenModeBypassingAppsSettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 78
    new-instance v0, Lcom/android/settings/notification/ZenModeBypassingAppsSettings$1;

    invoke-direct {v0}, Lcom/android/settings/notification/ZenModeBypassingAppsSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/notification/ZenModeBypassingAppsSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Lcom/android/settings/notification/ZenModeSettingsBase;-><init>()V

    .line 39
    const-string v0, "ZenBypassingApps"

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeBypassingAppsSettings;->TAG:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/app/Application;Landroidx/fragment/app/Fragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/app/Application;
    .param p2, "x2"    # Landroidx/fragment/app/Fragment;

    .line 37
    invoke-static {p0, p1, p2}, Lcom/android/settings/notification/ZenModeBypassingAppsSettings;->buildPreferenceControllers(Landroid/content/Context;Landroid/app/Application;Landroidx/fragment/app/Fragment;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static buildPreferenceControllers(Landroid/content/Context;Landroid/app/Application;Landroidx/fragment/app/Fragment;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "app"    # Landroid/app/Application;
    .param p2, "host"    # Landroidx/fragment/app/Fragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/Application;",
            "Landroidx/fragment/app/Fragment;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v0, "controllers":Ljava/util/List;, "Ljava/util/List<Lcom/android/settingslib/core/AbstractPreferenceController;>;"
    new-instance v1, Lcom/android/settings/notification/ZenModeAllBypassingAppsPreferenceController;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/notification/ZenModeAllBypassingAppsPreferenceController;-><init>(Landroid/content/Context;Landroid/app/Application;Landroidx/fragment/app/Fragment;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    return-object v0
.end method


# virtual methods
.method protected createPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeBypassingAppsSettings;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    .line 45
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    .local v1, "app":Landroid/app/Application;
    goto :goto_0

    .line 48
    .end local v1    # "app":Landroid/app/Application;
    :cond_0
    const/4 v1, 0x0

    .line 50
    .restart local v1    # "app":Landroid/app/Application;
    :goto_0
    invoke-static {p1, v1, p0}, Lcom/android/settings/notification/ZenModeBypassingAppsSettings;->buildPreferenceControllers(Landroid/content/Context;Landroid/app/Application;Landroidx/fragment/app/Fragment;)Ljava/util/List;

    move-result-object v2

    return-object v2
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 67
    const-string v0, "ZenBypassingApps"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 72
    const/16 v0, 0x634

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 62
    const v0, 0x7f16011d

    return v0
.end method
