.class public Lcom/android/settings/network/SubscriptionUtil;
.super Ljava/lang/Object;
.source "SubscriptionUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SubscriptionUtil"

.field private static sActiveResultsForTesting:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static sAvailableResultsForTesting:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActiveSubscriptions(Landroid/telephony/SubscriptionManager;)Ljava/util/List;
    .locals 2
    .param p0, "manager"    # Landroid/telephony/SubscriptionManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/telephony/SubscriptionManager;",
            ")",
            "Ljava/util/List<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation

    .line 50
    sget-object v0, Lcom/android/settings/network/SubscriptionUtil;->sActiveResultsForTesting:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 51
    return-object v0

    .line 53
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList(Z)Ljava/util/List;

    move-result-object v0

    .line 54
    .local v0, "subscriptions":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubscriptionInfo;>;"
    if-nez v0, :cond_1

    .line 55
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    return-object v1

    .line 57
    :cond_1
    return-object v0
.end method

.method public static getAvailableSubscriptions(Landroid/content/Context;)Ljava/util/List;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation

    .line 70
    sget-object v0, Lcom/android/settings/network/SubscriptionUtil;->sAvailableResultsForTesting:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 71
    return-object v0

    .line 73
    :cond_0
    const-class v0, Landroid/telephony/SubscriptionManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionManager;

    .line 74
    .local v0, "subMgr":Landroid/telephony/SubscriptionManager;
    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 76
    .local v1, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v2, Ljava/util/ArrayList;

    .line 77
    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getSelectableSubscriptionInfoList()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/util/CollectionUtils;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 80
    .local v2, "subscriptions":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubscriptionInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v3, "missing":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/UiccSlotInfo;>;"
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getUiccSlotsInfo()[Landroid/telephony/UiccSlotInfo;

    move-result-object v4

    .line 82
    .local v4, "slotsInfo":[Landroid/telephony/UiccSlotInfo;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-eqz v4, :cond_2

    array-length v6, v4

    if-ge v5, v6, :cond_2

    .line 83
    aget-object v6, v4, v5

    .line 84
    .local v6, "slotInfo":Landroid/telephony/UiccSlotInfo;
    invoke-static {v6}, Lcom/android/settings/network/SubscriptionUtil;->isInactiveInsertedPSim(Landroid/telephony/UiccSlotInfo;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 85
    invoke-virtual {v6}, Landroid/telephony/UiccSlotInfo;->getLogicalSlotIdx()I

    move-result v7

    .line 86
    .local v7, "index":I
    invoke-virtual {v6}, Landroid/telephony/UiccSlotInfo;->getCardId()Ljava/lang/String;

    move-result-object v8

    .line 88
    .local v8, "cardId":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v9

    new-instance v10, Lcom/android/settings/network/-$$Lambda$SubscriptionUtil$nnXoIOBnbfo0rTHIuOx4-dWYxE0;

    invoke-direct {v10, v7, v8}, Lcom/android/settings/network/-$$Lambda$SubscriptionUtil$nnXoIOBnbfo0rTHIuOx4-dWYxE0;-><init>(ILjava/lang/String;)V

    invoke-interface {v9, v10}, Ljava/util/stream/Stream;->anyMatch(Ljava/util/function/Predicate;)Z

    move-result v9

    .line 90
    .local v9, "found":Z
    if-nez v9, :cond_1

    .line 91
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    .end local v6    # "slotInfo":Landroid/telephony/UiccSlotInfo;
    .end local v7    # "index":I
    .end local v8    # "cardId":Ljava/lang/String;
    .end local v9    # "found":Z
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 95
    .end local v5    # "i":I
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    .line 96
    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getAllSubscriptionInfoList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/SubscriptionInfo;

    .line 97
    .local v6, "info":Landroid/telephony/SubscriptionInfo;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/UiccSlotInfo;

    .line 98
    .local v8, "slotInfo":Landroid/telephony/UiccSlotInfo;
    invoke-virtual {v6}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v9

    invoke-virtual {v8}, Landroid/telephony/UiccSlotInfo;->getLogicalSlotIdx()I

    move-result v10

    if-ne v9, v10, :cond_3

    .line 99
    invoke-virtual {v6}, Landroid/telephony/SubscriptionInfo;->getCardString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Landroid/telephony/UiccSlotInfo;->getCardId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 100
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    goto :goto_3

    .line 103
    .end local v8    # "slotInfo":Landroid/telephony/UiccSlotInfo;
    :cond_3
    goto :goto_2

    .line 104
    .end local v6    # "info":Landroid/telephony/SubscriptionInfo;
    :cond_4
    :goto_3
    goto :goto_1

    .line 106
    :cond_5
    return-object v2
.end method

.method public static getDisplayName(Landroid/telephony/SubscriptionInfo;)Ljava/lang/String;
    .locals 2
    .param p0, "info"    # Landroid/telephony/SubscriptionInfo;

    .line 110
    invoke-virtual {p0}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 111
    .local v0, "name":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    .line 112
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 114
    :cond_0
    const-string v1, ""

    return-object v1
.end method

.method static isInactiveInsertedPSim(Landroid/telephony/UiccSlotInfo;)Z
    .locals 3
    .param p0, "slotInfo"    # Landroid/telephony/UiccSlotInfo;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 62
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 63
    return v0

    .line 65
    :cond_0
    invoke-virtual {p0}, Landroid/telephony/UiccSlotInfo;->getIsEuicc()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/telephony/UiccSlotInfo;->getIsActive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 66
    invoke-virtual {p0}, Landroid/telephony/UiccSlotInfo;->getCardStateInfo()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 65
    :goto_0
    return v0
.end method

.method static synthetic lambda$getAvailableSubscriptions$0(ILjava/lang/String;Landroid/telephony/SubscriptionInfo;)Z
    .locals 1
    .param p0, "index"    # I
    .param p1, "cardId"    # Ljava/lang/String;
    .param p2, "info"    # Landroid/telephony/SubscriptionInfo;

    .line 89
    invoke-virtual {p2}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v0

    if-ne p0, v0, :cond_0

    invoke-virtual {p2}, Landroid/telephony/SubscriptionInfo;->getCardString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static setActiveSubscriptionsForTesting(Ljava/util/List;)V
    .locals 0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;)V"
        }
    .end annotation

    .line 46
    .local p0, "results":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubscriptionInfo;>;"
    sput-object p0, Lcom/android/settings/network/SubscriptionUtil;->sActiveResultsForTesting:Ljava/util/List;

    .line 47
    return-void
.end method

.method public static setAvailableSubscriptionsForTesting(Ljava/util/List;)V
    .locals 0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;)V"
        }
    .end annotation

    .line 41
    .local p0, "results":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubscriptionInfo;>;"
    sput-object p0, Lcom/android/settings/network/SubscriptionUtil;->sAvailableResultsForTesting:Ljava/util/List;

    .line 42
    return-void
.end method
