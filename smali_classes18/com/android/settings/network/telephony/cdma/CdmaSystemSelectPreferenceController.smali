.class public Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;
.super Lcom/android/settings/network/telephony/cdma/CdmaBasePreferenceController;
.source "CdmaSystemSelectPreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/android/settings/network/telephony/cdma/CdmaBasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method private resetCdmaRoamingModeToDefault()V
    .locals 4

    .line 74
    iget-object v0, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v0, Landroidx/preference/ListPreference;

    .line 76
    .local v0, "listPreference":Landroidx/preference/ListPreference;
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "roaming_settings"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 81
    iget-object v2, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, v1}, Landroid/telephony/TelephonyManager;->setCdmaRoamingMode(I)Z

    .line 82
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "object"    # Ljava/lang/Object;

    .line 62
    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 64
    .local v0, "newMode":I
    iget-object v1, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, v0}, Landroid/telephony/TelephonyManager;->setCdmaRoamingMode(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "roaming_settings"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 67
    const/4 v1, 0x1

    return v1

    .line 70
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 7
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 40
    invoke-super {p0, p1}, Lcom/android/settings/network/telephony/cdma/CdmaBasePreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 41
    move-object v0, p1

    check-cast v0, Landroidx/preference/ListPreference;

    .line 42
    .local v0, "listPreference":Landroidx/preference/ListPreference;
    invoke-virtual {p0}, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->getAvailabilityStatus()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setVisible(Z)V

    .line 43
    iget-object v1, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCdmaRoamingMode()I

    move-result v1

    .line 44
    .local v1, "mode":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_3

    .line 45
    if-eqz v1, :cond_2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    goto :goto_1

    .line 49
    :cond_1
    invoke-direct {p0}, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->resetCdmaRoamingModeToDefault()V

    goto :goto_2

    .line 47
    :cond_2
    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 52
    :cond_3
    :goto_2
    iget-object v4, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mContext:Landroid/content/Context;

    .line 53
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "preferred_network_mode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p0, Lcom/android/settings/network/telephony/cdma/CdmaSystemSelectPreferenceController;->mSubId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/android/internal/telephony/Phone;->PREFERRED_NT_MODE:I

    .line 52
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 56
    .local v4, "settingsNetworkMode":I
    const/16 v5, 0x9

    if-eq v4, v5, :cond_4

    goto :goto_3

    :cond_4
    move v2, v3

    :goto_3
    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setEnabled(Z)V

    .line 58
    return-void
.end method
