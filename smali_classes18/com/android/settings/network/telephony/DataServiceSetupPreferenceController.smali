.class public Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;
.super Lcom/android/settings/network/telephony/TelephonyBasePreferenceController;
.source "DataServiceSetupPreferenceController.java"


# instance fields
.field private mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

.field private mSetupUrl:Ljava/lang/String;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/android/settings/network/telephony/TelephonyBasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    const-class v0, Landroid/telephony/CarrierConfigManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    iput-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    .line 45
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 46
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "setup_prepaid_data_service_url"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mSetupUrl:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus(I)I
    .locals 4
    .param p1, "subId"    # I

    .line 52
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    move v0, v2

    .line 54
    .local v0, "isLteOnCdma":Z
    iget-object v2, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    invoke-virtual {v2, p1}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v2

    .line 59
    .local v2, "carrierConfig":Landroid/os/PersistableBundle;
    const/4 v3, -0x1

    if-eq p1, v3, :cond_1

    if-eqz v2, :cond_1

    .line 57
    const-string v3, "hide_carrier_network_settings_bool"

    invoke-virtual {v2, v3}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mSetupUrl:Ljava/lang/String;

    .line 59
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 60
    goto :goto_1

    .line 61
    :cond_1
    const/4 v1, 0x2

    .line 55
    :goto_1
    return v1
.end method

.method public handlePreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 6
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 71
    invoke-virtual {p0}, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mSetupUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "imsi":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 75
    const-string v0, ""

    .line 77
    :cond_0
    iget-object v3, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mSetupUrl:Ljava/lang/String;

    new-array v4, v2, [Ljava/lang/CharSequence;

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "url":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v3, v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 79
    .local v3, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 81
    .end local v0    # "imsi":Ljava/lang/String;
    .end local v1    # "url":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    return v2

    .line 84
    :cond_2
    return v1
.end method

.method public init(I)V
    .locals 2
    .param p1, "subId"    # I

    .line 65
    iput p1, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mSubId:I

    .line 66
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mSubId:I

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->createForSubscriptionId(I)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/DataServiceSetupPreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 67
    return-void
.end method
