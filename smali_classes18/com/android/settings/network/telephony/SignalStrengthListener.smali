.class public Lcom/android/settings/network/telephony/SignalStrengthListener;
.super Ljava/lang/Object;
.source "SignalStrengthListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/network/telephony/SignalStrengthListener$Callback;
    }
.end annotation


# instance fields
.field private mBaseTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mCallback:Lcom/android/settings/network/telephony/SignalStrengthListener$Callback;

.field private mListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Landroid/telephony/PhoneStateListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/network/telephony/SignalStrengthListener$Callback;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/android/settings/network/telephony/SignalStrengthListener$Callback;

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mBaseTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 45
    iput-object p2, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mCallback:Lcom/android/settings/network/telephony/SignalStrengthListener$Callback;

    .line 46
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/network/telephony/SignalStrengthListener;)Lcom/android/settings/network/telephony/SignalStrengthListener$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/network/telephony/SignalStrengthListener;

    .line 33
    iget-object v0, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mCallback:Lcom/android/settings/network/telephony/SignalStrengthListener$Callback;

    return-object v0
.end method

.method private startListening(I)V
    .locals 3
    .param p1, "subId"    # I

    .line 85
    iget-object v0, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mBaseTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForSubscriptionId(I)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 86
    .local v0, "mgr":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 87
    return-void
.end method

.method private stopListening(I)V
    .locals 3
    .param p1, "subId"    # I

    .line 90
    iget-object v0, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mBaseTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForSubscriptionId(I)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 91
    .local v0, "mgr":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 92
    return-void
.end method


# virtual methods
.method public pause()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 60
    .local v1, "subId":I
    invoke-direct {p0, v1}, Lcom/android/settings/network/telephony/SignalStrengthListener;->stopListening(I)V

    .line 61
    .end local v1    # "subId":I
    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 53
    .local v1, "subId":I
    invoke-direct {p0, v1}, Lcom/android/settings/network/telephony/SignalStrengthListener;->startListening(I)V

    .line 54
    .end local v1    # "subId":I
    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method

.method public updateSubscriptionIds(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 67
    .local p1, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/util/ArraySet;

    iget-object v1, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V

    .line 68
    .local v0, "currentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {v0, p1}, Lcom/google/common/collect/Sets;->difference(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/Sets$SetView;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 69
    .local v2, "idToRemove":I
    invoke-direct {p0, v2}, Lcom/android/settings/network/telephony/SignalStrengthListener;->stopListening(I)V

    .line 70
    iget-object v3, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .end local v2    # "idToRemove":I
    goto :goto_0

    .line 72
    :cond_0
    invoke-static {p1, v0}, Lcom/google/common/collect/Sets;->difference(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/Sets$SetView;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 73
    .local v2, "idToAdd":I
    new-instance v3, Lcom/android/settings/network/telephony/SignalStrengthListener$1;

    invoke-direct {v3, p0}, Lcom/android/settings/network/telephony/SignalStrengthListener$1;-><init>(Lcom/android/settings/network/telephony/SignalStrengthListener;)V

    .line 79
    .local v3, "listener":Landroid/telephony/PhoneStateListener;
    iget-object v4, p0, Lcom/android/settings/network/telephony/SignalStrengthListener;->mListeners:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-direct {p0, v2}, Lcom/android/settings/network/telephony/SignalStrengthListener;->startListening(I)V

    .line 81
    .end local v2    # "idToAdd":I
    .end local v3    # "listener":Landroid/telephony/PhoneStateListener;
    goto :goto_1

    .line 82
    :cond_1
    return-void
.end method
