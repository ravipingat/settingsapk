.class public Lcom/android/settings/network/telephony/DataConnectivityListener;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "DataConnectivityListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/network/telephony/DataConnectivityListener$Client;
    }
.end annotation


# instance fields
.field private mClient:Lcom/android/settings/network/telephony/DataConnectivityListener$Client;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private final mNetworkRequest:Landroid/net/NetworkRequest;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/network/telephony/DataConnectivityListener$Client;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "client"    # Lcom/android/settings/network/telephony/DataConnectivityListener$Client;

    .line 38
    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mContext:Landroid/content/Context;

    .line 40
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mContext:Landroid/content/Context;

    const-class v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 41
    iput-object p2, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mClient:Lcom/android/settings/network/telephony/DataConnectivityListener$Client;

    .line 42
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 43
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mNetworkRequest:Landroid/net/NetworkRequest;

    .line 45
    return-void
.end method


# virtual methods
.method public onCapabilitiesChanged(Landroid/net/Network;Landroid/net/NetworkCapabilities;)V
    .locals 2
    .param p1, "network"    # Landroid/net/Network;
    .param p2, "networkCapabilities"    # Landroid/net/NetworkCapabilities;

    .line 58
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetwork()Landroid/net/Network;

    move-result-object v0

    .line 59
    .local v0, "activeNetwork":Landroid/net/Network;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mClient:Lcom/android/settings/network/telephony/DataConnectivityListener$Client;

    invoke-interface {v1}, Lcom/android/settings/network/telephony/DataConnectivityListener$Client;->onDataConnectivityChange()V

    .line 62
    :cond_0
    return-void
.end method

.method public onLosing(Landroid/net/Network;I)V
    .locals 1
    .param p1, "network"    # Landroid/net/Network;
    .param p2, "maxMsToLive"    # I

    .line 66
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mClient:Lcom/android/settings/network/telephony/DataConnectivityListener$Client;

    invoke-interface {v0}, Lcom/android/settings/network/telephony/DataConnectivityListener$Client;->onDataConnectivityChange()V

    .line 67
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 1
    .param p1, "network"    # Landroid/net/Network;

    .line 71
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mClient:Lcom/android/settings/network/telephony/DataConnectivityListener$Client;

    invoke-interface {v0}, Lcom/android/settings/network/telephony/DataConnectivityListener$Client;->onDataConnectivityChange()V

    .line 72
    return-void
.end method

.method public start()V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mNetworkRequest:Landroid/net/NetworkRequest;

    iget-object v2, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mContext:Landroid/content/Context;

    .line 49
    invoke-virtual {v2}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v2

    .line 48
    invoke-virtual {v0, v1, p0, v2}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;Landroid/os/Handler;)V

    .line 50
    return-void
.end method

.method public stop()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/android/settings/network/telephony/DataConnectivityListener;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, p0}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 54
    return-void
.end method
