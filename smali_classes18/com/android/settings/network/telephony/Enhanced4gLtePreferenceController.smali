.class public Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;
.super Lcom/android/settings/network/telephony/TelephonyTogglePreferenceController;
.source "Enhanced4gLtePreferenceController.java"

# interfaces
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnStart;
.implements Lcom/android/settingslib/core/lifecycle/events/OnStop;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;,
        Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;
    }
.end annotation


# instance fields
.field private final VARIANT_TITLE_4G_CALLING:I

.field private final VARIANT_TITLE_ADVANCED_CALL:I

.field private final VARIANT_TITLE_VOLTE:I

.field private final m4gLteListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mCarrierConfig:Landroid/os/PersistableBundle;

.field private mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

.field mImsManager:Lcom/android/ims/ImsManager;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private mPhoneStateListener:Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;

.field private mPreference:Landroidx/preference/Preference;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private final mVariantSumaries:[Ljava/lang/CharSequence;

.field private final mVariantTitles:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/android/settings/network/telephony/TelephonyTogglePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->VARIANT_TITLE_VOLTE:I

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->VARIANT_TITLE_ADVANCED_CALL:I

    .line 60
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->VARIANT_TITLE_4G_CALLING:I

    .line 64
    const-class v0, Landroid/telephony/CarrierConfigManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->m4gLteListeners:Ljava/util/List;

    .line 66
    new-instance v0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;-><init>(Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mPhoneStateListener:Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const v1, 0x7f03007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mVariantTitles:[Ljava/lang/CharSequence;

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 70
    const v1, 0x7f030079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mVariantSumaries:[Ljava/lang/CharSequence;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;)Landroidx/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;

    .line 44
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mPreference:Landroidx/preference/Preference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;

    .line 44
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private is4gLtePrefEnabled()Z
    .locals 2

    .line 154
    iget v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v1, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    .line 155
    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getCallState(I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isNonTtyOrTtyOnVolteEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mCarrierConfig:Landroid/os/PersistableBundle;

    .line 158
    const-string v1, "editable_enhanced_4g_lte_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 154
    :goto_0
    return v0
.end method


# virtual methods
.method public addListener(Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;)Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;
    .locals 1
    .param p1, "lsn"    # Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;

    .line 149
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->m4gLteListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    return-object p0
.end method

.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 89
    invoke-super {p0, p1}, Lcom/android/settings/network/telephony/TelephonyTogglePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 90
    invoke-virtual {p0}, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mPreference:Landroidx/preference/Preference;

    .line 91
    return-void
.end method

.method public getAvailabilityStatus(I)I
    .locals 5
    .param p1, "subId"    # I

    .line 75
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    invoke-virtual {v0, p1}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 76
    .local v0, "carrierConfig":Landroid/os/PersistableBundle;
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eq p1, v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v3}, Lcom/android/ims/ImsManager;->isVolteEnabledByPlatform()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    .line 79
    invoke-virtual {v3}, Lcom/android/ims/ImsManager;->isVolteProvisionedOnDevice()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    .line 80
    invoke-static {v3}, Lcom/android/settings/network/telephony/MobileNetworkUtils;->isImsServiceStateReady(Lcom/android/ims/ImsManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    const-string v3, "hide_enhanced_4g_lte_bool"

    invoke-virtual {v0, v3}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 82
    .local v3, "isVisible":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 83
    invoke-direct {p0}, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->is4gLtePrefEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v2

    goto :goto_1

    .line 84
    :cond_1
    const/4 v1, 0x2

    .line 82
    :cond_2
    :goto_1
    return v1
.end method

.method public init(I)Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;
    .locals 2
    .param p1, "subId"    # I

    .line 138
    iput p1, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    .line 139
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->createForSubscriptionId(I)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 140
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    iget v1, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    invoke-virtual {v0, v1}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mCarrierConfig:Landroid/os/PersistableBundle;

    .line 141
    iget v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/ims/ImsManager;->getInstance(Landroid/content/Context;I)Lcom/android/ims/ImsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    .line 145
    :cond_0
    return-object p0
.end method

.method public isChecked()Z
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isEnhanced4gLteModeSettingEnabledByUser()Z

    move-result v0

    return v0
.end method

.method public onStart()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mPhoneStateListener:Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;

    iget v1, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mSubId:I

    invoke-virtual {v0, v1}, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;->register(I)V

    .line 96
    return-void
.end method

.method public onStop()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mPhoneStateListener:Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;

    invoke-virtual {v0}, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$PhoneCallStateListener;->unregister()V

    .line 101
    return-void
.end method

.method public setChecked(Z)Z
    .locals 2
    .param p1, "isChecked"    # Z

    .line 125
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    invoke-virtual {v0, p1}, Lcom/android/ims/ImsManager;->setEnhanced4gLteModeSetting(Z)V

    .line 126
    iget-object v0, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->m4gLteListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;

    .line 127
    .local v1, "lsn":Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;
    invoke-interface {v1}, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;->on4gLteUpdated()V

    .line 128
    .end local v1    # "lsn":Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController$On4gLteUpdateListener;
    goto :goto_0

    .line 129
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 6
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 105
    invoke-super {p0, p1}, Lcom/android/settings/network/telephony/TelephonyTogglePreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 106
    move-object v0, p1

    check-cast v0, Landroidx/preference/SwitchPreference;

    .line 107
    .local v0, "switchPreference":Landroidx/preference/SwitchPreference;
    iget-object v1, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mCarrierConfig:Landroid/os/PersistableBundle;

    const-string v2, "show_4g_for_lte_data_icon_bool"

    invoke-virtual {v1, v2}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 109
    .local v1, "show4GForLTE":Z
    iget-object v2, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mCarrierConfig:Landroid/os/PersistableBundle;

    const-string v3, "enhanced_4g_lte_title_variant_int"

    invoke-virtual {v2, v3}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 112
    .local v2, "variant4glteTitleIndex":I
    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v2, v4, :cond_1

    .line 113
    if-eqz v1, :cond_0

    const/4 v5, 0x2

    goto :goto_0

    :cond_0
    move v5, v3

    :goto_0
    move v2, v5

    .line 116
    :cond_1
    iget-object v5, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mVariantTitles:[Ljava/lang/CharSequence;

    aget-object v5, v5, v2

    invoke-virtual {v0, v5}, Landroidx/preference/SwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v5, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mVariantSumaries:[Ljava/lang/CharSequence;

    aget-object v5, v5, v2

    invoke-virtual {v0, v5}, Landroidx/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 118
    invoke-direct {p0}, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->is4gLtePrefEnabled()Z

    move-result v5

    invoke-virtual {v0, v5}, Landroidx/preference/SwitchPreference;->setEnabled(Z)V

    .line 119
    iget-object v5, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    invoke-virtual {v5}, Lcom/android/ims/ImsManager;->isEnhanced4gLteModeSettingEnabledByUser()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/settings/network/telephony/Enhanced4gLtePreferenceController;->mImsManager:Lcom/android/ims/ImsManager;

    .line 120
    invoke-virtual {v5}, Lcom/android/ims/ImsManager;->isNonTtyOrTtyOnVolteEnabled()Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    goto :goto_1

    :cond_2
    nop

    .line 119
    :goto_1
    invoke-virtual {v0, v3}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .line 121
    return-void
.end method
