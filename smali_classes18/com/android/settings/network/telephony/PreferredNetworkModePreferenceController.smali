.class public Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;
.super Lcom/android/settings/network/telephony/TelephonyBasePreferenceController;
.source "PreferredNetworkModePreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

.field private mIsGlobalCdma:Z

.field private mPersistableBundle:Landroid/os/PersistableBundle;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/android/settings/network/telephony/TelephonyBasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 47
    const-class v0, Landroid/telephony/CarrierConfigManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    iput-object v0, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    .line 48
    return-void
.end method

.method private getPreferredNetworkMode()I
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preferred_network_mode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mSubId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/android/internal/telephony/Phone;->PREFERRED_NT_MODE:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getPreferredNetworkModeSummaryResId(I)I
    .locals 4
    .param p1, "NetworkMode"    # I

    .line 119
    const v0, 0x7f121126

    const v1, 0x7f12111f

    packed-switch p1, :pswitch_data_0

    .line 178
    return v1

    .line 160
    :pswitch_0
    const v0, 0x7f121127

    return v0

    .line 170
    :pswitch_1
    const v0, 0x7f12112e

    return v0

    .line 152
    :pswitch_2
    const v0, 0x7f121129

    return v0

    .line 174
    :pswitch_3
    const v0, 0x7f12112b

    return v0

    .line 121
    :pswitch_4
    const v0, 0x7f121130

    return v0

    .line 150
    :pswitch_5
    const v0, 0x7f121128

    return v0

    .line 123
    :pswitch_6
    const v0, 0x7f12112f

    return v0

    .line 146
    :pswitch_7
    const v0, 0x7f12112a

    return v0

    .line 129
    :pswitch_8
    const v0, 0x7f121132

    return v0

    .line 158
    :pswitch_9
    const v0, 0x7f121131

    return v0

    .line 176
    :pswitch_a
    const v0, 0x7f12112c

    return v0

    .line 148
    :pswitch_b
    return v0

    .line 162
    :pswitch_c
    iget-object v2, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mIsGlobalCdma:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mSubId:I

    .line 164
    invoke-static {v2, v3}, Lcom/android/settings/network/telephony/MobileNetworkUtils;->isWorldMode(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 167
    :cond_0
    return v0

    .line 165
    :cond_1
    :goto_0
    return v1

    .line 154
    :pswitch_d
    const v0, 0x7f121125

    return v0

    .line 156
    :pswitch_e
    const v0, 0x7f121122

    return v0

    .line 172
    :pswitch_f
    const v0, 0x7f121119

    return v0

    .line 144
    :pswitch_10
    const v0, 0x7f12111e

    return v0

    .line 142
    :pswitch_11
    const v0, 0x7f12111b

    return v0

    .line 135
    :pswitch_12
    iget-object v0, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 139
    const v0, 0x7f12111a

    return v0

    .line 137
    :cond_2
    const v0, 0x7f12111c

    return v0

    .line 133
    :pswitch_13
    const v0, 0x7f121121

    return v0

    .line 131
    :pswitch_14
    const v0, 0x7f121134

    return v0

    .line 127
    :pswitch_15
    const v0, 0x7f121120

    return v0

    .line 125
    :pswitch_16
    const v0, 0x7f121135

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getAvailabilityStatus(I)I
    .locals 4
    .param p1, "subId"    # I

    .line 52
    iget-object v0, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    invoke-virtual {v0, p1}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 53
    .local v0, "carrierConfig":Landroid/os/PersistableBundle;
    iget-object v1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mContext:Landroid/content/Context;

    .line 54
    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/telephony/TelephonyManager;->createForSubscriptionId(I)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 56
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    .line 57
    const/4 v2, 0x0

    .local v2, "visible":Z
    goto :goto_0

    .line 58
    .end local v2    # "visible":Z
    :cond_0
    if-nez v0, :cond_1

    .line 59
    const/4 v2, 0x0

    .restart local v2    # "visible":Z
    goto :goto_0

    .line 60
    .end local v2    # "visible":Z
    :cond_1
    const-string v2, "hide_carrier_network_settings_bool"

    invoke-virtual {v0, v2}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    const/4 v2, 0x0

    .restart local v2    # "visible":Z
    goto :goto_0

    .line 63
    .end local v2    # "visible":Z
    :cond_2
    const-string v2, "hide_preferred_network_type_bool"

    invoke-virtual {v0, v2}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 65
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v2

    if-nez v2, :cond_3

    .line 66
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getDataRegState()I

    move-result v2

    if-nez v2, :cond_3

    .line 68
    const/4 v2, 0x0

    .restart local v2    # "visible":Z
    goto :goto_0

    .line 69
    .end local v2    # "visible":Z
    :cond_3
    const-string v2, "world_phone_bool"

    invoke-virtual {v0, v2}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 70
    const/4 v2, 0x1

    .restart local v2    # "visible":Z
    goto :goto_0

    .line 72
    .end local v2    # "visible":Z
    :cond_4
    const/4 v2, 0x0

    .line 75
    .restart local v2    # "visible":Z
    :goto_0
    if-eqz v2, :cond_5

    const/4 v3, 0x0

    goto :goto_1

    :cond_5
    const/4 v3, 0x2

    :goto_1
    return v3
.end method

.method public init(I)V
    .locals 5
    .param p1, "subId"    # I

    .line 102
    iput p1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mSubId:I

    .line 103
    iget-object v0, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mCarrierConfigManager:Landroid/telephony/CarrierConfigManager;

    iget v1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mSubId:I

    invoke-virtual {v0, v1}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 104
    .local v0, "carrierConfig":Landroid/os/PersistableBundle;
    iget-object v1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mSubId:I

    invoke-virtual {v1, v2}, Landroid/telephony/TelephonyManager;->createForSubscriptionId(I)Landroid/telephony/TelephonyManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 106
    iget-object v1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 107
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    .line 108
    .local v1, "isLteOnCdma":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 109
    const-string v4, "show_cdma_choices_bool"

    invoke-virtual {v0, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    goto :goto_1

    :cond_1
    nop

    :goto_1
    iput-boolean v2, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mIsGlobalCdma:Z

    .line 110
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "object"    # Ljava/lang/Object;

    .line 89
    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 91
    .local v0, "settingsMode":I
    iget-object v1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v2, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mSubId:I

    invoke-virtual {v1, v2, v0}, Landroid/telephony/TelephonyManager;->setPreferredNetworkType(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preferred_network_mode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->mSubId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 95
    const/4 v1, 0x1

    return v1

    .line 98
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 3
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 80
    invoke-super {p0, p1}, Lcom/android/settings/network/telephony/TelephonyBasePreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 81
    move-object v0, p1

    check-cast v0, Landroidx/preference/ListPreference;

    .line 82
    .local v0, "listPreference":Landroidx/preference/ListPreference;
    invoke-direct {p0}, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->getPreferredNetworkMode()I

    move-result v1

    .line 83
    .local v1, "networkMode":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 84
    invoke-direct {p0, v1}, Lcom/android/settings/network/telephony/PreferredNetworkModePreferenceController;->getPreferredNetworkModeSummaryResId(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setSummary(I)V

    .line 85
    return-void
.end method
