.class public Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPSimAndNetworkSettingsPreferenceController.java"

# interfaces
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;


# static fields
.field private static final KEY_OP_SIM_CORD:Ljava/lang/String; = "op_sim_cord"


# instance fields
.field private final mIsSecondaryUser:Z

.field private mPreference:Landroidx/preference/Preference;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    const-string v0, "op_sim_cord"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    const-string v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mUserManager:Landroid/os/UserManager;

    .line 40
    iget-object v0, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mIsSecondaryUser:Z

    .line 42
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 64
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 65
    invoke-virtual {p0}, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mPreference:Landroidx/preference/Preference;

    .line 66
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    .line 48
    invoke-virtual {p0}, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->isUserRestricted()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 70
    const-string v0, "op_sim_cord"

    return-object v0
.end method

.method public handlePreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 75
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "op_sim_cord"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 79
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Landroid/content/Intent;

    const-string v2, "oneplus.intent.action.SINGLE_SIM_AND_NETWORK_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 81
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 82
    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 84
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "oneplus.intent.action.SIM_AND_NETWORK_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    .end local v0    # "tm":Landroid/telephony/TelephonyManager;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    goto :goto_1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 90
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 92
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isUserRestricted()Z
    .locals 3

    .line 54
    iget-boolean v0, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mIsSecondaryUser:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/network/OPSimAndNetworkSettingsPreferenceController;->mContext:Landroid/content/Context;

    .line 58
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 55
    const-string v2, "no_config_mobile_networks"

    invoke-static {v0, v2, v1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 54
    :goto_1
    return v0
.end method
