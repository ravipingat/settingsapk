.class Lcom/android/settings/RadioInfo$11;
.super Ljava/lang/Object;
.source "RadioInfo.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/RadioInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/RadioInfo;


# direct methods
.method constructor <init>(Lcom/android/settings/RadioInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/settings/RadioInfo;

    .line 1302
    iput-object p1, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 1304
    iget-object v0, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    invoke-static {v0}, Lcom/android/settings/RadioInfo;->access$3400(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->isImsRegistered()Z

    move-result v0

    .line 1305
    .local v0, "isImsRegistered":Z
    iget-object v1, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    invoke-static {v1}, Lcom/android/settings/RadioInfo;->access$3400(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isVolteEnabled()Z

    move-result v1

    .line 1306
    .local v1, "availableVolte":Z
    iget-object v2, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    invoke-static {v2}, Lcom/android/settings/RadioInfo;->access$3400(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->isWifiCallingEnabled()Z

    move-result v2

    .line 1307
    .local v2, "availableWfc":Z
    iget-object v3, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    invoke-static {v3}, Lcom/android/settings/RadioInfo;->access$3400(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->isVideoEnabled()Z

    move-result v3

    .line 1308
    .local v3, "availableVt":Z
    iget-object v4, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    invoke-static {v4}, Lcom/android/settings/RadioInfo;->access$3400(Lcom/android/settings/RadioInfo;)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v4

    .line 1310
    .local v4, "availableUt":Z
    if-eqz v0, :cond_0

    .line 1311
    iget-object v5, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    const v6, 0x7f1211d3

    invoke-virtual {v5, v6}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 1312
    :cond_0
    iget-object v5, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    const v6, 0x7f1211d2

    invoke-virtual {v5, v6}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    nop

    .line 1314
    .local v5, "imsRegString":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    const v7, 0x7f1211cf

    invoke-virtual {v6, v7}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1315
    .local v6, "available":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    const v8, 0x7f1211d0

    invoke-virtual {v7, v8}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1318
    .local v7, "unavailable":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    const v9, 0x7f1211d1

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    .line 1320
    if-eqz v1, :cond_1

    move-object v11, v6

    goto :goto_1

    :cond_1
    move-object v11, v7

    :goto_1
    const/4 v12, 0x1

    aput-object v11, v10, v12

    const/4 v11, 0x2

    .line 1321
    if-eqz v2, :cond_2

    move-object v13, v6

    goto :goto_2

    :cond_2
    move-object v13, v7

    :goto_2
    aput-object v13, v10, v11

    const/4 v11, 0x3

    .line 1322
    if-eqz v3, :cond_3

    move-object v13, v6

    goto :goto_3

    :cond_3
    move-object v13, v7

    :goto_3
    aput-object v13, v10, v11

    const/4 v11, 0x4

    .line 1323
    if-eqz v4, :cond_4

    move-object v13, v6

    goto :goto_4

    :cond_4
    move-object v13, v7

    :goto_4
    aput-object v13, v10, v11

    .line 1318
    invoke-virtual {v8, v9, v10}, Lcom/android/settings/RadioInfo;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1325
    .local v8, "imsStatus":Ljava/lang/String;
    new-instance v9, Landroidx/appcompat/app/AlertDialog$Builder;

    iget-object v10, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    invoke-direct {v9, v10}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1326
    invoke-virtual {v9, v8}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/settings/RadioInfo$11;->this$0:Lcom/android/settings/RadioInfo;

    const v11, 0x7f1211d4

    .line 1327
    invoke-virtual {v10, v11}, Lcom/android/settings/RadioInfo;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v9

    .line 1328
    invoke-virtual {v9}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v9

    .line 1330
    .local v9, "imsDialog":Landroidx/appcompat/app/AlertDialog;
    invoke-virtual {v9}, Landroidx/appcompat/app/AlertDialog;->show()V

    .line 1332
    return v12
.end method
