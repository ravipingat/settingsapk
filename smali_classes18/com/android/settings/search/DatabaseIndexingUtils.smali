.class public Lcom/android/settings/search/DatabaseIndexingUtils;
.super Ljava/lang/Object;
.source "DatabaseIndexingUtils.java"


# static fields
.field public static final FIELD_NAME_SEARCH_INDEX_DATA_PROVIDER:Ljava/lang/String; = "SEARCH_INDEX_DATA_PROVIDER"

.field private static final TAG:Ljava/lang/String; = "IndexingUtil"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSearchIndexProvider(Ljava/lang/Class;)Lcom/android/settings/search/Indexable$SearchIndexProvider;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/android/settings/search/Indexable$SearchIndexProvider;"
        }
    .end annotation

    .line 37
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v0, "IndexingUtil"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "SEARCH_INDEX_DATA_PROVIDER"

    invoke-virtual {p0, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 38
    .local v2, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v2, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/search/Indexable$SearchIndexProvider;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 46
    .end local v2    # "f":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v2

    .line 47
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "Illegal argument when accessing field \'SEARCH_INDEX_DATA_PROVIDER\'"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 44
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 45
    .local v2, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "Illegal access to field \'SEARCH_INDEX_DATA_PROVIDER\'"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    goto :goto_0

    .line 41
    :catch_2
    move-exception v2

    .line 42
    .local v2, "se":Ljava/lang/SecurityException;
    const-string v3, "Security exception for field \'SEARCH_INDEX_DATA_PROVIDER\'"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v2    # "se":Ljava/lang/SecurityException;
    goto :goto_0

    .line 39
    :catch_3
    move-exception v2

    .line 40
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    const-string v3, "Cannot find field \'SEARCH_INDEX_DATA_PROVIDER\'"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    :goto_0
    nop

    .line 50
    :goto_1
    return-object v1
.end method
