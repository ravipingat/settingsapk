.class public Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;
.super Lcom/android/settings/sound/AudioSwitchPreferenceController;
.source "HandsFreeProfileOutputPreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final INVALID_INDEX:I = -0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/android/settings/sound/AudioSwitchPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method private getConnectedDeviceIndex(Ljava/lang/String;)I
    .locals 4
    .param p1, "hardwareAddress"    # Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .local v1, "size":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 77
    iget-object v2, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 78
    .local v2, "btDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    return v0

    .line 76
    .end local v2    # "btDevice":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_1
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public findActiveDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 2

    .line 183
    invoke-virtual {p0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->findActiveHearingAidDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 184
    .local v0, "activeDevice":Landroid/bluetooth/BluetoothDevice;
    iget-object v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getHeadsetProfile()Lcom/android/settingslib/bluetooth/HeadsetProfile;

    move-result-object v1

    .line 186
    .local v1, "headsetProfile":Lcom/android/settingslib/bluetooth/HeadsetProfile;
    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/HeadsetProfile;->getActiveDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 189
    :cond_0
    return-object v0
.end method

.method getDefaultDeviceIndex()I
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 48
    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    .line 49
    .local v0, "address":Ljava/lang/String;
    instance-of v1, p1, Landroidx/preference/ListPreference;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 50
    return v2

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mContext:Landroid/content/Context;

    const v3, 0x7f120a03

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 54
    .local v1, "defaultSummary":Ljava/lang/CharSequence;
    move-object v3, p1

    check-cast v3, Landroidx/preference/ListPreference;

    .line 55
    .local v3, "listPreference":Landroidx/preference/ListPreference;
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 57
    invoke-virtual {p0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->getDefaultDeviceIndex()I

    move-result v2

    iput v2, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    .line 58
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->setActiveBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)V

    .line 59
    invoke-virtual {v3, v1}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 62
    :cond_1
    invoke-direct {p0, v0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->getConnectedDeviceIndex(Ljava/lang/String;)I

    move-result v4

    .line 63
    .local v4, "connectedDeviceIndex":I
    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 64
    return v2

    .line 66
    :cond_2
    iget-object v2, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 67
    .local v2, "btDevice":Landroid/bluetooth/BluetoothDevice;
    iput v4, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    .line 68
    invoke-virtual {p0, v2}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->setActiveBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)V

    .line 69
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 71
    .end local v2    # "btDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v4    # "connectedDeviceIndex":I
    :goto_0
    const/4 v2, 0x1

    return v2
.end method

.method public setActiveBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)V
    .locals 6
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .line 166
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/Utils;->isAudioModeOngoingCall(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getHearingAidProfile()Lcom/android/settingslib/bluetooth/HearingAidProfile;

    move-result-object v0

    .line 170
    .local v0, "hapProfile":Lcom/android/settingslib/bluetooth/HearingAidProfile;
    iget-object v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getHeadsetProfile()Lcom/android/settingslib/bluetooth/HeadsetProfile;

    move-result-object v1

    .line 171
    .local v1, "hfpProfile":Lcom/android/settingslib/bluetooth/HeadsetProfile;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 172
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settingslib/bluetooth/HeadsetProfile;->setActiveDevice(Landroid/bluetooth/BluetoothDevice;)Z

    .line 173
    invoke-virtual {v0, v2}, Lcom/android/settingslib/bluetooth/HearingAidProfile;->setActiveDevice(Landroid/bluetooth/BluetoothDevice;)Z

    goto :goto_0

    .line 174
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/HearingAidProfile;->getHiSyncId(Landroid/bluetooth/BluetoothDevice;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 175
    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/HearingAidProfile;->setActiveDevice(Landroid/bluetooth/BluetoothDevice;)Z

    goto :goto_0

    .line 176
    :cond_2
    if-eqz v1, :cond_3

    .line 177
    invoke-virtual {v1, p1}, Lcom/android/settingslib/bluetooth/HeadsetProfile;->setActiveDevice(Landroid/bluetooth/BluetoothDevice;)Z

    .line 179
    :cond_3
    :goto_0
    return-void
.end method

.method setPreference([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;Landroidx/preference/Preference;)V
    .locals 2
    .param p1, "mediaOutputs"    # [Ljava/lang/CharSequence;
    .param p2, "mediaValues"    # [Ljava/lang/CharSequence;
    .param p3, "preference"    # Landroidx/preference/Preference;

    .line 157
    move-object v0, p3

    check-cast v0, Landroidx/preference/ListPreference;

    .line 158
    .local v0, "listPreference":Landroidx/preference/ListPreference;
    invoke-virtual {v0, p1}, Landroidx/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 159
    invoke-virtual {v0, p2}, Landroidx/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 160
    iget v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setValueIndex(I)V

    .line 161
    iget v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mAudioSwitchPreferenceCallback:Lcom/android/settings/sound/AudioSwitchPreferenceController$AudioSwitchCallback;

    invoke-interface {v1, v0}, Lcom/android/settings/sound/AudioSwitchPreferenceController$AudioSwitchCallback;->onPreferenceDataChanged(Landroidx/preference/ListPreference;)V

    .line 163
    return-void
.end method

.method setupPreferenceEntries([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "mediaOutputs"    # [Ljava/lang/CharSequence;
    .param p2, "mediaValues"    # [Ljava/lang/CharSequence;
    .param p3, "activeDevice"    # Landroid/bluetooth/BluetoothDevice;

    .line 138
    invoke-virtual {p0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->getDefaultDeviceIndex()I

    move-result v0

    iput v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    .line 140
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mContext:Landroid/content/Context;

    const v1, 0x7f120a03

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 141
    .local v0, "defaultSummary":Ljava/lang/CharSequence;
    iget v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    aput-object v0, p1, v1

    .line 143
    iget v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    aput-object v0, p2, v1

    .line 144
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v2, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 145
    iget-object v3, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 146
    .local v3, "btDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, p1, v1

    .line 147
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    aput-object v4, p2, v1

    .line 148
    invoke-virtual {v3, p3}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 150
    iput v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    .line 144
    .end local v3    # "btDevice":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 153
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_1
    return-void
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 5
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 88
    if-nez p1, :cond_0

    .line 90
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/Utils;->isAudioModeOngoingCall(Landroid/content/Context;)Z

    move-result v0

    const v1, 0x7f120a03

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    .line 96
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 97
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 104
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->getConnectedHfpDevices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 105
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->getConnectedHearingAidDevices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 107
    iget-object v0, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mConnectedDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 108
    .local v0, "numDevices":I
    const/4 v3, 0x1

    if-nez v0, :cond_2

    .line 110
    iget-object v4, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {v4, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    .line 111
    iget-object v4, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 112
    .local v1, "summary":Ljava/lang/CharSequence;
    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object v1, v3, v2

    move-object v2, v3

    .line 113
    .local v2, "defaultMediaOutput":[Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->getDefaultDeviceIndex()I

    move-result v3

    iput v3, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mSelectedIndex:I

    .line 114
    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {p0, v2, v2, p1}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->setPreference([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;Landroidx/preference/Preference;)V

    .line 116
    return-void

    .line 119
    .end local v1    # "summary":Ljava/lang/CharSequence;
    .end local v2    # "defaultMediaOutput":[Ljava/lang/CharSequence;
    :cond_2
    iget-object v1, p0, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {v1, v3}, Landroidx/preference/Preference;->setVisible(Z)V

    .line 120
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    .line 121
    .local v1, "mediaOutputs":[Ljava/lang/CharSequence;
    add-int/lit8 v2, v0, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    .line 124
    .local v2, "mediaValues":[Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->findActiveDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->setupPreferenceEntries([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;Landroid/bluetooth/BluetoothDevice;)V

    .line 127
    invoke-virtual {p0, v1, v2, p1}, Lcom/android/settings/sound/HandsFreeProfileOutputPreferenceController;->setPreference([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;Landroidx/preference/Preference;)V

    .line 128
    return-void
.end method
