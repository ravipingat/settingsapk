.class public Lcom/android/settings/SetupWizardUtils;
.super Ljava/lang/Object;
.source "SetupWizardUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyLifecycleExtra(Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3
    .param p0, "srcBundle"    # Landroid/os/Bundle;
    .param p1, "dstBundle"    # Landroid/os/Bundle;

    .line 114
    const-string v0, "firstRun"

    const-string v1, "isSetupFlow"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 117
    .local v1, "key":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 118
    .end local v1    # "key":Ljava/lang/String;
    goto :goto_0

    .line 119
    :cond_0
    return-object p1
.end method

.method public static copySetupExtras(Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 0
    .param p0, "fromIntent"    # Landroid/content/Intent;
    .param p1, "toIntent"    # Landroid/content/Intent;

    .line 109
    invoke-static {p0, p1}, Lcom/google/android/setupcompat/util/WizardManagerHelper;->copyWizardManagerExtras(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 110
    return-void
.end method

.method public static getTheme(Landroid/content/Intent;)I
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .line 81
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isO2()Z

    move-result v0

    const-string v1, "SetupWizardUtils"

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "Theme_Oneplus_SetupWizardTheme_Oxygen"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const v0, 0x7f13051a

    return v0

    .line 85
    :cond_0
    const-string v0, "Theme_Oneplus_SetupWizardTheme_Hydrogen"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const v0, 0x7f130517

    return v0
.end method

.method public static getThemeString(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .line 37
    const-string v0, "theme"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "theme":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 39
    invoke-static {}, Landroid/sysprop/SetupWizardProperties;->theme()Ljava/util/Optional;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/util/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 41
    :cond_0
    return-object v0
.end method

.method public static getTransparentTheme(Landroid/content/Intent;)I
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .line 92
    invoke-static {p0}, Lcom/android/settings/SetupWizardUtils;->getTheme(Landroid/content/Intent;)I

    move-result v0

    .line 93
    .local v0, "suwTheme":I
    const v1, 0x7f130121

    .line 94
    .local v1, "transparentTheme":I
    const v2, 0x7f130125

    if-ne v0, v2, :cond_0

    .line 95
    const v1, 0x7f130129

    goto :goto_0

    .line 96
    :cond_0
    const v2, 0x7f130127

    if-ne v0, v2, :cond_1

    .line 97
    const v1, 0x7f130128

    goto :goto_0

    .line 98
    :cond_1
    const v2, 0x7f13011f

    if-ne v0, v2, :cond_2

    .line 99
    const v1, 0x7f130122

    goto :goto_0

    .line 100
    :cond_2
    const v2, 0x7f13011e

    if-ne v0, v2, :cond_3

    .line 101
    const v1, 0x7f1303b1

    goto :goto_0

    .line 102
    :cond_3
    const v2, 0x7f13011d

    if-ne v0, v2, :cond_4

    .line 103
    const v1, 0x7f1303b2

    .line 105
    :cond_4
    :goto_0
    return v1
.end method
