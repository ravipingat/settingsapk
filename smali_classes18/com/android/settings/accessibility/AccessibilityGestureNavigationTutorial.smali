.class public Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;
.super Ljava/lang/Object;
.source "AccessibilityGestureNavigationTutorial.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial$DialogType;
    }
.end annotation


# static fields
.field private static final mOnClickListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 65
    sget-object v0, Lcom/android/settings/accessibility/-$$Lambda$AccessibilityGestureNavigationTutorial$8C7pOA2IdxSAhUypXc1abvjSbh8;->INSTANCE:Lcom/android/settings/accessibility/-$$Lambda$AccessibilityGestureNavigationTutorial$8C7pOA2IdxSAhUypXc1abvjSbh8;

    sput-object v0, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createDialog(Landroid/content/Context;I)Landroidx/appcompat/app/AlertDialog;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogType"    # I

    .line 152
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 153
    invoke-static {p0, p1}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->createTutorialDialogContentView(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 154
    const v2, 0x7f1200a0

    invoke-virtual {v0, v2, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    .line 157
    .local v0, "alertDialog":Landroidx/appcompat/app/AlertDialog;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog;->requestWindowFeature(I)Z

    .line 158
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 159
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog;->show()V

    .line 161
    return-object v0
.end method

.method private static createTutorialDialogContentView(Landroid/content/Context;I)Landroid/view/View;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogType"    # I

    .line 105
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 108
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .line 110
    .local v1, "content":Landroid/view/View;
    const/4 v2, 0x0

    if-eqz p1, :cond_6

    const/4 v3, 0x1

    const v4, 0x7f110016

    const v5, 0x7f110017

    const v6, 0x7f0a029f

    const v7, 0x7f0a02a1

    if-eq p1, v3, :cond_3

    const/4 v3, 0x2

    if-eq p1, v3, :cond_0

    goto :goto_4

    .line 131
    :cond_0
    const v3, 0x7f0d0325

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 133
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/TextureView;

    .line 135
    .local v2, "gestureSettingsTutorialVideo":Landroid/view/TextureView;
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 137
    .local v3, "gestureSettingsTutorialMessage":Landroid/widget/TextView;
    invoke-static {p0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->isTouchExploreOn(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 138
    goto :goto_0

    .line 139
    :cond_1
    move v4, v5

    .line 137
    :goto_0
    invoke-static {p0, v4, v2}, Lcom/android/settings/accessibility/VideoPlayer;->create(Landroid/content/Context;ILandroid/view/TextureView;)Lcom/android/settings/accessibility/VideoPlayer;

    .line 141
    invoke-static {p0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->isTouchExploreOn(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 143
    const v4, 0x7f1200a2

    goto :goto_1

    .line 144
    :cond_2
    const v4, 0x7f1200a3

    .line 141
    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 116
    .end local v2    # "gestureSettingsTutorialVideo":Landroid/view/TextureView;
    .end local v3    # "gestureSettingsTutorialMessage":Landroid/widget/TextView;
    :cond_3
    const v3, 0x7f0d0327

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 118
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/TextureView;

    .line 120
    .local v2, "gestureTutorialVideo":Landroid/view/TextureView;
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 122
    .local v3, "gestureTutorialMessage":Landroid/widget/TextView;
    invoke-static {p0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->isTouchExploreOn(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 123
    goto :goto_2

    .line 124
    :cond_4
    move v4, v5

    .line 122
    :goto_2
    invoke-static {p0, v4, v2}, Lcom/android/settings/accessibility/VideoPlayer;->create(Landroid/content/Context;ILandroid/view/TextureView;)Lcom/android/settings/accessibility/VideoPlayer;

    .line 126
    invoke-static {p0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->isTouchExploreOn(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 127
    const v4, 0x7f1200a4

    goto :goto_3

    .line 128
    :cond_5
    const v4, 0x7f1200a5

    .line 126
    :goto_3
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 129
    goto :goto_4

    .line 112
    .end local v2    # "gestureTutorialVideo":Landroid/view/TextureView;
    .end local v3    # "gestureTutorialMessage":Landroid/widget/TextView;
    :cond_6
    const v3, 0x7f0d0326

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 114
    nop

    .line 148
    :goto_4
    return-object v1
.end method

.method private static getAttrResourceId(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attributeColor"    # I

    .line 200
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 201
    .local v0, "attrs":[I
    invoke-virtual {p0, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 202
    .local v2, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v2, v1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 203
    .local v1, "colorResId":I
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 204
    return v1
.end method

.method private static getMessageStringWithIcon(Landroid/content/Context;I)Landroid/text/SpannableString;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "lineHeight"    # I

    .line 174
    nop

    .line 175
    const v0, 0x7f1200a1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "messageString":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    .line 179
    .local v1, "spannableMessage":Landroid/text/SpannableString;
    const-string v2, "%s"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 180
    .local v2, "indexIconStart":I
    add-int/lit8 v3, v2, 0x2

    .line 181
    .local v3, "indexIconEnd":I
    const v4, 0x7f080191

    invoke-virtual {p0, v4}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 182
    .local v4, "icon":Landroid/graphics/drawable/Drawable;
    const v5, 0x1010036

    invoke-static {p0, v5}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->getThemeAttrColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 183
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v5, p1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 184
    new-instance v5, Landroid/text/style/ImageSpan;

    invoke-direct {v5, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/16 v6, 0x21

    invoke-virtual {v1, v5, v2, v3, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 188
    return-object v1
.end method

.method private static getThemeAttrColor(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attributeColor"    # I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation

    .line 194
    invoke-static {p0, p1}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->getAttrResourceId(Landroid/content/Context;I)I

    move-result v0

    .line 195
    .local v0, "colorResId":I
    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    return v1
.end method

.method private static isGestureNavigateEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 208
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isTouchExploreOn(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 214
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 215
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    .line 214
    return v0
.end method

.method static synthetic lambda$static$0(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "which"    # I

    .line 66
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method static showAccessibilityButtonTutorialDialog(Landroid/content/Context;)Landroidx/appcompat/app/AlertDialog;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 83
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->createDialog(Landroid/content/Context;I)Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    .line 86
    .local v0, "alertDialog":Landroidx/appcompat/app/AlertDialog;
    invoke-static {p0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->isGestureNavigateEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    invoke-static {p0, v0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->updateMessageWithIcon(Landroid/content/Context;Landroidx/appcompat/app/AlertDialog;)V

    .line 90
    :cond_0
    return-object v0
.end method

.method public static showGestureNavigationSettingsTutorialDialog(Landroid/content/Context;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dismissListener"    # Landroid/content/DialogInterface$OnDismissListener;

    .line 70
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 71
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->createTutorialDialogContentView(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 73
    const v2, 0x7f1200a0

    invoke-virtual {v0, v2, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0, p1}, Landroidx/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    .line 77
    .local v0, "alertDialog":Landroidx/appcompat/app/AlertDialog;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog;->requestWindowFeature(I)Z

    .line 78
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 79
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog;->show()V

    .line 80
    return-void
.end method

.method static showGestureNavigationTutorialDialog(Landroid/content/Context;)Landroidx/appcompat/app/AlertDialog;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 94
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->createDialog(Landroid/content/Context;I)Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private static updateMessageWithIcon(Landroid/content/Context;Landroidx/appcompat/app/AlertDialog;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "alertDialog"    # Landroidx/appcompat/app/AlertDialog;

    .line 165
    const v0, 0x7f0a0112

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 169
    .local v0, "gestureTutorialMessage":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v1

    .line 170
    .local v1, "lineHeight":I
    invoke-static {p0, v1}, Lcom/android/settings/accessibility/AccessibilityGestureNavigationTutorial;->getMessageStringWithIcon(Landroid/content/Context;I)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    return-void
.end method
