.class Lcom/android/settings/RadioInfo$28;
.super Ljava/lang/Object;
.source "RadioInfo.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/RadioInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/RadioInfo;


# direct methods
.method constructor <init>(Lcom/android/settings/RadioInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/settings/RadioInfo;

    .line 1630
    iput-object p1, p0, Lcom/android/settings/RadioInfo$28;->this$0:Lcom/android/settings/RadioInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J

    .line 1633
    if-ltz p3, :cond_4

    invoke-static {}, Lcom/android/settings/RadioInfo;->access$4100()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-gt p3, v0, :cond_4

    .line 1635
    move v0, p3

    .line 1636
    .local v0, "phoneIndex":I
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 1637
    .local v2, "phones":[Lcom/android/internal/telephony/Phone;
    if-eqz v2, :cond_3

    array-length v3, v2

    if-gt v3, v0, :cond_0

    goto :goto_1

    .line 1641
    :cond_0
    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[I

    move-result-object v3

    .line 1642
    .local v3, "subIds":[I
    if-eqz v3, :cond_2

    array-length v4, v3

    if-ge v4, v1, :cond_1

    goto :goto_0

    .line 1645
    :cond_1
    iget-object v1, p0, Lcom/android/settings/RadioInfo$28;->this$0:Lcom/android/settings/RadioInfo;

    invoke-static {v1, v0}, Lcom/android/settings/RadioInfo;->access$4202(Lcom/android/settings/RadioInfo;I)I

    .line 1647
    iget-object v1, p0, Lcom/android/settings/RadioInfo$28;->this$0:Lcom/android/settings/RadioInfo;

    const/4 v4, 0x0

    aget v4, v3, v4

    invoke-static {v1, v0, v4}, Lcom/android/settings/RadioInfo;->access$4300(Lcom/android/settings/RadioInfo;II)V

    goto :goto_2

    .line 1643
    :cond_2
    :goto_0
    return-void

    .line 1638
    .end local v3    # "subIds":[I
    :cond_3
    :goto_1
    return-void

    .line 1649
    .end local v0    # "phoneIndex":I
    .end local v2    # "phones":[Lcom/android/internal/telephony/Phone;
    :cond_4
    :goto_2
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .param p1, "parent"    # Landroid/widget/AdapterView;

    .line 1652
    return-void
.end method
