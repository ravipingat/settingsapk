.class public Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;
.super Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;
.source "SetupFaceUnlockEnrollIntroduction.java"


# static fields
.field private static final FACE_RESULT_FAIL:I = 0x1

.field private static final FACE_RESULT_NOT_FOUND:I = 0x2

.field private static final FACE_RESULT_OK:I = 0x0

.field private static final KEY_LOCK_SCREEN_PRESENT:Ljava/lang/String; = "wasLockScreenPresent"

.field private static final TAG:Ljava/lang/String; = "SetupFaceUnlockEnrollIntroduction"


# instance fields
.field private mAlreadyHadLockScreenSetup:Z

.field private mFaceSettingService:Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

.field private mFaceUnlockConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 64
    invoke-direct {p0}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mAlreadyHadLockScreenSetup:Z

    .line 74
    new-instance v0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction$1;

    invoke-direct {v0, p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction$1;-><init>(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;)V

    iput-object v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mFaceUnlockConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$002(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;)Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;
    .locals 0
    .param p0, "x0"    # Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;
    .param p1, "x1"    # Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    .line 64
    iput-object p1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mFaceSettingService:Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;

    .line 64
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isFaceAdded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;)Lcom/oneplus/lib/widget/button/OPButton;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;

    .line 64
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getNextButton()Lcom/oneplus/lib/widget/button/OPButton;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;)Lcom/oneplus/lib/widget/button/OPButton;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;

    .line 64
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getNextButton()Lcom/oneplus/lib/widget/button/OPButton;

    move-result-object v0

    return-object v0
.end method

.method private adjustForGuideImage()V
    .locals 5

    .line 160
    const v0, 0x7f0a0687

    invoke-virtual {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 161
    .local v0, "imgHead":Landroid/widget/ImageView;
    const v1, 0x7f0a0308

    invoke-virtual {p0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 162
    .local v1, "imgGuide":Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 163
    .local v2, "paramsHead":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 165
    .local v3, "paramsGuide":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportXCamera()Z

    move-result v4

    if-nez v4, :cond_0

    .line 166
    iget v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    div-int/lit8 v4, v4, 0x2

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 167
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 169
    :cond_0
    invoke-static {p0}, Lcom/oneplus/settings/utils/OPUtils;->isLargerFontSize(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p0}, Lcom/oneplus/settings/utils/OPUtils;->isLargerScreenZoom(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 170
    iget v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    div-int/lit8 v4, v4, 0x2

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 171
    iget v4, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    div-int/lit8 v4, v4, 0x2

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 172
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    :cond_1
    :goto_0
    return-void
.end method

.method private bindFaceUnlockService()V
    .locals 4

    .line 95
    const-string v0, "SetupFaceUnlockEnrollIntroduction"

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 96
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.oneplus.faceunlock"

    const-string v3, "com.oneplus.faceunlock.FaceSettingService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    iget-object v2, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mFaceUnlockConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 99
    const-string v2, "Start bind oneplus face unlockservice"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    nop

    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 100
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "Bind oneplus face unlockservice exception"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private getMetricIntent(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .param p1, "data"    # Landroid/content/Intent;

    .line 357
    if-nez p1, :cond_0

    .line 358
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    move-object p1, v0

    .line 360
    :cond_0
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v0, p0}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    .line 361
    .local v0, "lockPatternUtils":Lcom/android/internal/widget/LockPatternUtils;
    nop

    .line 363
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v1

    .line 361
    const-string v2, ":settings:password_quality"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 364
    return-object p1
.end method

.method private goToNextPage()V
    .locals 3

    .line 345
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isKeyguardSecure()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mAlreadyHadLockScreenSetup:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getMetricIntent(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    :goto_0
    invoke-virtual {p0, v0, v2}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->setResult(ILandroid/content/Intent;)V

    .line 349
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->finish()V

    goto :goto_1

    .line 351
    :cond_1
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->setResult(I)V

    .line 352
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->finish()V

    .line 354
    :goto_1
    return-void
.end method

.method private isFaceAdded()Z
    .locals 6

    .line 111
    const-string v0, "SetupFaceUnlockEnrollIntroduction"

    iget-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mFaceSettingService:Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 112
    return v2

    .line 114
    :cond_0
    const/4 v3, 0x2

    .line 116
    .local v3, "addState":I
    :try_start_0
    invoke-interface {v1, v2}, Lcom/oneplus/faceunlock/internal/IOPFaceSettingService;->checkState(I)I

    move-result v1

    move v3, v1

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start check face state:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    goto :goto_0

    .line 118
    :catch_0
    move-exception v1

    .line 119
    .local v1, "re":Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Start check face State RemoteException:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    .end local v1    # "re":Landroid/os/RemoteException;
    :goto_0
    if-nez v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method private isKeyguardSecure()Z
    .locals 1

    .line 433
    const-class v0, Landroid/app/KeyguardManager;

    invoke-virtual {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    return v0
.end method

.method private launchFaceUnlock()V
    .locals 3

    .line 385
    :try_start_0
    invoke-static {p0}, Lcom/oneplus/settings/faceunlock/OPFaceUnlockSettings;->getSetupFaceUnlockIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 386
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startActivityForResult(Landroid/content/Intent;I)V

    .line 387
    const v1, 0x7f01005a

    const v2, 0x7f01005b

    invoke-virtual {p0, v1, v2}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->overridePendingTransition(II)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 388
    :catch_0
    move-exception v0

    .line 389
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 391
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :goto_0
    return-void
.end method

.method private startOPFaceUnlockModeSettingsActivity()V
    .locals 3

    .line 333
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 334
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "enter_faceunlock_mode_settings_from_suw"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 335
    iget-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mToken:[B

    const-string v2, "hw_auth_token"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 336
    iget v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mUserId:I

    const/16 v2, -0x2710

    if-eq v1, v2, :cond_0

    .line 337
    iget v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mUserId:I

    const-string v2, "android.intent.extra.USER_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 339
    :cond_0
    const-string v1, "com.android.settings"

    const-string v2, "com.oneplus.settings.faceunlock.OPFaceUnlockModeSettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startActivityForResult(Landroid/content/Intent;I)V

    .line 342
    return-void
.end method

.method private startSetupFingerprintEnrollIntroduction(Z)V
    .locals 3
    .param p1, "transportToken"    # Z

    .line 322
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/settings/biometrics/fingerprint/SetupFingerprintEnrollIntroduction;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 323
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 324
    iget-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mToken:[B

    const-string v2, "hw_auth_token"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 325
    iget v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mUserId:I

    const/16 v2, -0x2710

    if-eq v1, v2, :cond_0

    .line 326
    iget v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mUserId:I

    const-string v2, "android.intent.extra.USER_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 329
    :cond_0
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startActivityForResult(Landroid/content/Intent;I)V

    .line 330
    return-void
.end method

.method private unbindFaceUnlockService()V
    .locals 2

    .line 106
    const-string v0, "SetupFaceUnlockEnrollIntroduction"

    const-string v1, "Start unbind oneplus face unlockservice"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mFaceUnlockConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->unbindService(Landroid/content/ServiceConnection;)V

    .line 108
    return-void
.end method


# virtual methods
.method protected getChooseLockIntent()Landroid/content/Intent;
    .locals 3

    .line 140
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/settings/password/SetupChooseLockGeneric;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 142
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Landroid/os/storage/StorageManager;->isFileEncryptedNativeOrEmulated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    const/high16 v1, 0x20000

    const-string v2, "lockscreen.password_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 146
    const/4 v1, 0x1

    const-string v2, "show_options_button"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/SetupWizardUtils;->copySetupExtras(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 149
    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 438
    const/16 v0, 0xf9

    return v0
.end method

.method protected initViews()V
    .locals 5

    .line 213
    invoke-super {p0}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;->initViews()V

    .line 215
    const v0, 0x7f0a0685

    invoke-virtual {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 216
    .local v0, "description":Landroid/widget/TextView;
    const v1, 0x7f121309

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 219
    const v1, 0x7f0a0272

    invoke-virtual {p0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/oneplus/lib/widget/button/OPButton;

    iput-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mBtnNext:Lcom/oneplus/lib/widget/button/OPButton;

    .line 221
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportXCamera()Z

    move-result v1

    const v2, 0x7f0a05a9

    const v3, 0x7f120ca6

    if-eqz v1, :cond_0

    .line 222
    invoke-virtual {p0, v3}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->setHeaderText(I)V

    .line 223
    const v1, 0x7f120cb8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 225
    invoke-virtual {p0, v2}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 226
    :cond_0
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportCustomFingerprint()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 227
    const v1, 0x7f120fbb

    invoke-virtual {p0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->setHeaderText(I)V

    .line 228
    const v1, 0x7f120fba

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 231
    :cond_1
    const v1, 0x7f120ca5

    invoke-virtual {p0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->setHeaderText(I)V

    .line 232
    const v1, 0x7f120ca4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 237
    :goto_0
    iget-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mBtnNext:Lcom/oneplus/lib/widget/button/OPButton;

    invoke-virtual {v1, v3}, Lcom/oneplus/lib/widget/button/OPButton;->setText(I)V

    .line 238
    iget-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mBtnNext:Lcom/oneplus/lib/widget/button/OPButton;

    new-instance v3, Lcom/android/settings/biometrics/face/-$$Lambda$ieya5yOds0DIHWaiNHPzjd6_A6o;

    invoke-direct {v3, p0}, Lcom/android/settings/biometrics/face/-$$Lambda$ieya5yOds0DIHWaiNHPzjd6_A6o;-><init>(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;)V

    invoke-virtual {v1, v3}, Lcom/oneplus/lib/widget/button/OPButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    const v1, 0x7f0a026b

    invoke-virtual {p0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/oneplus/lib/widget/button/OPButton;

    iput-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mBtnCancel:Lcom/oneplus/lib/widget/button/OPButton;

    .line 242
    iget-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mBtnCancel:Lcom/oneplus/lib/widget/button/OPButton;

    new-instance v3, Lcom/android/settings/biometrics/face/-$$Lambda$lpHQg3KNzbCBGMh9I-Z1zygP-60;

    invoke-direct {v3, p0}, Lcom/android/settings/biometrics/face/-$$Lambda$lpHQg3KNzbCBGMh9I-Z1zygP-60;-><init>(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;)V

    invoke-virtual {v1, v3}, Lcom/oneplus/lib/widget/button/OPButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    iget-object v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mBtnCancel:Lcom/oneplus/lib/widget/button/OPButton;

    const v3, 0x7f121305

    invoke-virtual {v1, v3}, Lcom/oneplus/lib/widget/button/OPButton;->setText(I)V

    .line 245
    const v1, 0x7f0a029a

    invoke-virtual {p0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 246
    .local v1, "functionalTermsButton":Landroid/widget/TextView;
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isO2()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 247
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0603ee

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 249
    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0603ec

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 251
    :goto_1
    new-instance v3, Lcom/android/settings/biometrics/face/-$$Lambda$SetupFaceUnlockEnrollIntroduction$Du2jt-QVrWPSe4IPCZOEcqQ7vAQ;

    invoke-direct {v3, p0}, Lcom/android/settings/biometrics/face/-$$Lambda$SetupFaceUnlockEnrollIntroduction$Du2jt-QVrWPSe4IPCZOEcqQ7vAQ;-><init>(Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    const v3, 0x7f060332

    invoke-virtual {p0, v3}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getColor(I)I

    move-result v3

    .line 260
    .local v3, "textColor":I
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 261
    const v4, 0x7f0a0679

    invoke-virtual {p0, v4}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 262
    invoke-virtual {p0, v2}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v4, 0x7f060336

    .line 263
    invoke-virtual {p0, v4}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getColor(I)I

    move-result v4

    .line 262
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 264
    iget-object v2, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mBtnNext:Lcom/oneplus/lib/widget/button/OPButton;

    const v4, 0x7f06039e

    invoke-virtual {p0, v4}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/oneplus/lib/widget/button/OPButton;->setTextColor(I)V

    .line 266
    return-void
.end method

.method public synthetic lambda$initViews$0$SetupFaceUnlockEnrollIntroduction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .line 252
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.oem.intent.action.OP_LEGAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 253
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "op_legal_notices_type"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 254
    const-string v1, "key_from_settings"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 255
    invoke-virtual {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startActivity(Landroid/content/Intent;)V

    .line 256
    return-void
.end method

.method protected launchChooseLock()V
    .locals 5

    .line 394
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getChooseLockIntent()Landroid/content/Intent;

    move-result-object v0

    .line 395
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0}, Lcom/android/settings/Utils;->getFingerprintManagerOrNull(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/fingerprint/FingerprintManager;->preEnroll()J

    move-result-wide v1

    .line 396
    .local v1, "challenge":J
    const-string v3, "minimum_quality"

    const/high16 v4, 0x10000

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 398
    const/4 v3, 0x1

    const-string v4, "hide_disabled_prefs"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 399
    const-string v4, "has_challenge"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 400
    const-string v4, "challenge"

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 401
    const-string v4, "for_face"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 402
    iget v3, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mUserId:I

    const/16 v4, -0x2710

    if-eq v3, v4, :cond_0

    .line 403
    iget v3, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mUserId:I

    const-string v4, "android.intent.extra.USER_ID"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 405
    :cond_0
    const/4 v3, 0x7

    invoke-virtual {p0, v0, v3}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startActivityForResult(Landroid/content/Intent;I)V

    .line 406
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityResult requestCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " resultCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SetupFaceUnlockEnrollIntroduction"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mFromSetup:Z

    .line 273
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isKeyguardSecure()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mAlreadyHadLockScreenSetup:Z

    if-nez v2, :cond_0

    .line 274
    invoke-direct {p0, p3}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getMetricIntent(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p3

    .line 275
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->goToNextPage()V

    goto/16 :goto_0

    .line 276
    :cond_0
    const/4 v2, 0x4

    if-ne p1, v2, :cond_4

    if-eqz p2, :cond_4

    .line 285
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportCustomFingerprint()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isFaceAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportXCamera()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startOPFaceUnlockModeSettingsActivity()V

    goto :goto_0

    .line 288
    :cond_1
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isFaceAdded()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 289
    invoke-direct {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startSetupFingerprintEnrollIntroduction(Z)V

    goto :goto_0

    .line 291
    :cond_2
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportXCamera()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 292
    invoke-direct {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startSetupFingerprintEnrollIntroduction(Z)V

    .line 294
    :cond_3
    return-void

    .line 297
    :cond_4
    if-ne p1, v2, :cond_5

    if-nez p2, :cond_5

    .line 298
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isFaceAdded()Z

    move-result v0

    if-nez v0, :cond_a

    .line 299
    return-void

    .line 301
    :cond_5
    const/4 v2, 0x7

    if-ne p1, v2, :cond_6

    if-ne p2, v0, :cond_6

    .line 302
    const-string v0, "hw_auth_token"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mToken:[B

    .line 304
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->launchFaceUnlock()V

    goto :goto_0

    .line 305
    :cond_6
    const/4 v2, 0x6

    if-ne p1, v2, :cond_8

    if-eq p2, v1, :cond_7

    const/16 v1, 0xb

    if-eq p2, v1, :cond_7

    if-eq p2, v0, :cond_7

    const/4 v0, -0x1

    if-ne p2, v0, :cond_8

    .line 308
    :cond_7
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->goToNextPage()V

    goto :goto_0

    .line 309
    :cond_8
    const/16 v0, 0xd

    if-ne p1, v0, :cond_a

    .line 310
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->updatePasswordQuality()V

    .line 311
    iget-boolean v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mHasPassword:Z

    if-eqz v0, :cond_9

    if-eqz p2, :cond_9

    .line 312
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->goToNextPage()V

    .line 314
    :cond_9
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isFaceAdded()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 315
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->finish()V

    .line 318
    :cond_a
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;->onActivityResult(IILandroid/content/Intent;)V

    .line 319
    return-void
.end method

.method protected onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .locals 1
    .param p1, "theme"    # Landroid/content/res/Resources$Theme;
    .param p2, "resid"    # I
    .param p3, "first"    # Z

    .line 155
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/SetupWizardUtils;->getTheme(Landroid/content/Intent;)I

    move-result p2

    .line 156
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    .line 157
    return-void
.end method

.method protected onCancelButtonClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .line 422
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startSetupFingerprintEnrollIntroduction(Z)V

    .line 423
    const v0, 0x7f01005a

    const v1, 0x7f01005b

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->overridePendingTransition(II)V

    .line 425
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mNeedLaunchLock:Z

    .line 183
    invoke-super {p0, p1}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;->onCreate(Landroid/os/Bundle;)V

    .line 184
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1}, Lcom/oneplus/settings/utils/OPUtils;->setLightStatusBar(Landroid/view/Window;)V

    .line 185
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 186
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mFromSetup:Z

    .line 187
    if-nez p1, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isKeyguardSecure()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mAlreadyHadLockScreenSetup:Z

    goto :goto_0

    .line 190
    :cond_0
    const-string v1, "wasLockScreenPresent"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mAlreadyHadLockScreenSetup:Z

    .line 193
    :goto_0
    const v1, 0x7f0d01a2

    invoke-virtual {p0, v1}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->setContentView(I)V

    .line 194
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportXCamera()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    invoke-direct {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startSetupFingerprintEnrollIntroduction(Z)V

    .line 196
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->finish()V

    .line 198
    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 199
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 201
    :cond_2
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->adjustForGuideImage()V

    .line 202
    return-void
.end method

.method protected onNextButtonClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .line 369
    iget-boolean v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mHasPassword:Z

    if-nez v0, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->launchChooseLock()V

    goto :goto_0

    .line 374
    :cond_0
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->isFaceAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 375
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->launchFaceUnlock()V

    goto :goto_0

    .line 377
    :cond_1
    const-string v0, "SetupFaceUnlockEnrollIntroduction"

    const-string v1, "onNext already added face"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->startSetupFingerprintEnrollIntroduction(Z)V

    .line 381
    :goto_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->unbindFaceUnlockService()V

    .line 135
    invoke-super {p0}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;->onPause()V

    .line 136
    return-void
.end method

.method protected onResume()V
    .locals 0

    .line 125
    invoke-super {p0}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;->onResume()V

    .line 127
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->updatePasswordQuality()V

    .line 128
    invoke-direct {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->bindFaceUnlockService()V

    .line 130
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .line 207
    invoke-super {p0, p1}, Lcom/android/settings/biometrics/fingerprint/FingerprintEnrollIntroduction;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 208
    iget-boolean v0, p0, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->mAlreadyHadLockScreenSetup:Z

    const-string v1, "wasLockScreenPresent"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 209
    return-void
.end method

.method protected onSkipButtonClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .line 428
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->setResult(I)V

    .line 429
    invoke-virtual {p0}, Lcom/android/settings/biometrics/face/SetupFaceUnlockEnrollIntroduction;->finish()V

    .line 430
    return-void
.end method
