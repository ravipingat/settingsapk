.class Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;
.super Landroid/hardware/face/FaceManager$EnrollmentCallback;
.source "FaceEnrollSidecar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/biometrics/face/FaceEnrollSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;


# direct methods
.method constructor <init>(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    .line 60
    iput-object p1, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-direct {p0}, Landroid/hardware/face/FaceManager$EnrollmentCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnrollmentError(ILjava/lang/CharSequence;)V
    .locals 1
    .param p1, "errMsgId"    # I
    .param p2, "errString"    # Ljava/lang/CharSequence;

    .line 74
    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0, p1, p2}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->access$201(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;ILjava/lang/CharSequence;)V

    .line 75
    return-void
.end method

.method public onEnrollmentHelp(ILjava/lang/CharSequence;)V
    .locals 1
    .param p1, "helpMsgId"    # I
    .param p2, "helpString"    # Ljava/lang/CharSequence;

    .line 69
    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0, p1, p2}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->access$101(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;ILjava/lang/CharSequence;)V

    .line 70
    return-void
.end method

.method public onEnrollmentProgress(I)V
    .locals 1
    .param p1, "remaining"    # I

    .line 64
    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0, p1}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->access$001(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;I)V

    .line 65
    return-void
.end method
