.class public abstract Lcom/android/settings/biometrics/face/FaceSettingsPreferenceController;
.super Lcom/android/settings/core/TogglePreferenceController;
.source "FaceSettingsPreferenceController.java"


# instance fields
.field private mUserId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/TogglePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected getUserId()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/android/settings/biometrics/face/FaceSettingsPreferenceController;->mUserId:I

    return v0
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .line 35
    iput p1, p0, Lcom/android/settings/biometrics/face/FaceSettingsPreferenceController;->mUserId:I

    .line 36
    return-void
.end method
