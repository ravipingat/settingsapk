.class public Lcom/android/settings/ui/OPSuwPreferenceCategory;
.super Landroidx/preference/PreferenceCategory;
.source "OPSuwPreferenceCategory.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 14
    invoke-direct {p0, p1}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 16
    invoke-direct {p0, p1}, Lcom/android/settings/ui/OPSuwPreferenceCategory;->initViews(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 20
    invoke-direct {p0, p1, p2}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    invoke-direct {p0, p1}, Lcom/android/settings/ui/OPSuwPreferenceCategory;->initViews(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-direct {p0, p1}, Lcom/android/settings/ui/OPSuwPreferenceCategory;->initViews(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method private initViews(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    iput-object p1, p0, Lcom/android/settings/ui/OPSuwPreferenceCategory;->mContext:Landroid/content/Context;

    .line 35
    const v0, 0x7f0d0226

    invoke-virtual {p0, v0}, Lcom/android/settings/ui/OPSuwPreferenceCategory;->setLayoutResource(I)V

    .line 37
    return-void
.end method
