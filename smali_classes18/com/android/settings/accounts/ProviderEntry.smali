.class public Lcom/android/settings/accounts/ProviderEntry;
.super Ljava/lang/Object;
.source "ProviderEntry.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/android/settings/accounts/ProviderEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final name:Ljava/lang/CharSequence;

.field private final type:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 0
    .param p1, "providerName"    # Ljava/lang/CharSequence;
    .param p2, "accountType"    # Ljava/lang/String;

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/android/settings/accounts/ProviderEntry;->name:Ljava/lang/CharSequence;

    .line 27
    iput-object p2, p0, Lcom/android/settings/accounts/ProviderEntry;->type:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/settings/accounts/ProviderEntry;)I
    .locals 2
    .param p1, "another"    # Lcom/android/settings/accounts/ProviderEntry;

    .line 31
    iget-object v0, p0, Lcom/android/settings/accounts/ProviderEntry;->name:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 32
    const/4 v0, -0x1

    return v0

    .line 34
    :cond_0
    iget-object v1, p1, Lcom/android/settings/accounts/ProviderEntry;->name:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    .line 35
    const/4 v0, 0x1

    return v0

    .line 37
    :cond_1
    invoke-static {v0, v1}, Lcom/android/internal/util/CharSequences;->compareToIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 21
    check-cast p1, Lcom/android/settings/accounts/ProviderEntry;

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/ProviderEntry;->compareTo(Lcom/android/settings/accounts/ProviderEntry;)I

    move-result p1

    return p1
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/android/settings/accounts/ProviderEntry;->name:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/android/settings/accounts/ProviderEntry;->type:Ljava/lang/String;

    return-object v0
.end method
