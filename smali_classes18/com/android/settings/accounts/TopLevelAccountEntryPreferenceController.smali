.class public Lcom/android/settings/accounts/TopLevelAccountEntryPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "TopLevelAccountEntryPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 1

    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/android/settings/accounts/TopLevelAccountEntryPreferenceController;->mContext:Landroid/content/Context;

    const v1, 0x7f120fa0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
