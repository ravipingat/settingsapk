.class public Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "EnterpriseDisclosurePreferenceController.java"


# instance fields
.field private final mFeatureProvider:Lcom/android/settings/enterprise/EnterprisePrivacyFeatureProvider;

.field private mFooterPreferenceMixin:Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;

.field private mScreen:Landroidx/preference/PreferenceScreen;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 38
    const-string v0, "add_account_enterprise_disclosure_footer"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mContext:Landroid/content/Context;

    .line 41
    invoke-virtual {v0, v1}, Lcom/android/settings/overlay/FeatureFactory;->getEnterprisePrivacyFeatureProvider(Landroid/content/Context;)Lcom/android/settings/enterprise/EnterprisePrivacyFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mFeatureProvider:Lcom/android/settings/enterprise/EnterprisePrivacyFeatureProvider;

    .line 42
    return-void
.end method

.method private addEnterpriseDisclosure()V
    .locals 3

    .line 69
    invoke-virtual {p0}, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->getDisclosure()Ljava/lang/CharSequence;

    move-result-object v0

    .line 70
    .local v0, "disclosure":Ljava/lang/CharSequence;
    if-nez v0, :cond_0

    .line 71
    return-void

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mFooterPreferenceMixin:Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;

    .line 74
    invoke-virtual {v1}, Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;->createFooterPreference()Lcom/oneplus/settings/widget/OPFooterPreference;

    move-result-object v1

    .line 75
    .local v1, "enterpriseDisclosurePreference":Lcom/oneplus/settings/widget/OPFooterPreference;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/oneplus/settings/widget/OPFooterPreference;->setSelectable(Z)V

    .line 76
    invoke-virtual {v1, v0}, Lcom/oneplus/settings/widget/OPFooterPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v2, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mScreen:Landroidx/preference/PreferenceScreen;

    invoke-virtual {v2, v1}, Landroidx/preference/PreferenceScreen;->addPreference(Landroidx/preference/Preference;)Z

    .line 78
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 0
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 58
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 59
    iput-object p1, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mScreen:Landroidx/preference/PreferenceScreen;

    .line 60
    invoke-direct {p0}, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->addEnterpriseDisclosure()V

    .line 61
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->getDisclosure()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x3

    return v0

    .line 53
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method getDisclosure()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mFeatureProvider:Lcom/android/settings/enterprise/EnterprisePrivacyFeatureProvider;

    invoke-interface {v0}, Lcom/android/settings/enterprise/EnterprisePrivacyFeatureProvider;->getDeviceOwnerDisclosure()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setFooterPreferenceMixin(Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;)V
    .locals 0
    .param p1, "footerPreferenceMixin"    # Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;

    .line 45
    iput-object p1, p0, Lcom/android/settings/accounts/EnterpriseDisclosurePreferenceController;->mFooterPreferenceMixin:Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;

    .line 46
    return-void
.end method
