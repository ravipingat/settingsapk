.class public Lcom/android/settings/accounts/CrossProfileCalendarDisabledPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "CrossProfileCalendarDisabledPreferenceController.java"


# instance fields
.field private mManagedUser:Landroid/os/UserHandle;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/android/settings/accounts/CrossProfileCalendarDisabledPreferenceController;->mManagedUser:Landroid/os/UserHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/CrossProfileCalendarDisabledPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/accounts/CrossProfileCalendarDisabledPreferenceController;->mManagedUser:Landroid/os/UserHandle;

    .line 41
    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    .line 40
    invoke-static {v0, v1}, Lcom/android/settings/accounts/CrossProfileCalendarPreferenceController;->isCrossProfileCalendarDisallowedByAdmin(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    return v0

    .line 45
    :cond_0
    const/4 v0, 0x4

    return v0
.end method

.method public setManagedUser(Landroid/os/UserHandle;)V
    .locals 0
    .param p1, "managedUser"    # Landroid/os/UserHandle;

    .line 29
    iput-object p1, p0, Lcom/android/settings/accounts/CrossProfileCalendarDisabledPreferenceController;->mManagedUser:Landroid/os/UserHandle;

    .line 30
    return-void
.end method
