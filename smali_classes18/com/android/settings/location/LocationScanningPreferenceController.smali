.class public Lcom/android/settings/location/LocationScanningPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "LocationScanningPreferenceController.java"


# static fields
.field static final KEY_LOCATION_SCANNING:Ljava/lang/String; = "location_scanning"
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 33
    const-string v0, "location_scanning"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 34
    iput-object p1, p0, Lcom/android/settings/location/LocationScanningPreferenceController;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/android/settings/location/LocationScanningPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_0
    const/4 v0, 0x3

    .line 58
    :goto_0
    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/android/settings/location/LocationScanningPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "wifi_scan_always_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 41
    .local v0, "wifiScanOn":Z
    :goto_0
    iget-object v3, p0, Lcom/android/settings/location/LocationScanningPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "ble_scan_always_enabled"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v2, :cond_1

    move v1, v2

    .line 44
    .local v1, "bleScanOn":Z
    :cond_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 45
    const v2, 0x7f121269

    .local v2, "resId":I
    goto :goto_1

    .line 46
    .end local v2    # "resId":I
    :cond_2
    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    .line 47
    const v2, 0x7f121268

    .restart local v2    # "resId":I
    goto :goto_1

    .line 48
    .end local v2    # "resId":I
    :cond_3
    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    .line 49
    const v2, 0x7f121267

    .restart local v2    # "resId":I
    goto :goto_1

    .line 51
    .end local v2    # "resId":I
    :cond_4
    const v2, 0x7f121266

    .line 53
    .restart local v2    # "resId":I
    :goto_1
    iget-object v3, p0, Lcom/android/settings/location/LocationScanningPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
