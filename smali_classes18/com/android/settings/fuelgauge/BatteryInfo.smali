.class public Lcom/android/settings/fuelgauge/BatteryInfo;
.super Ljava/lang/Object;
.source "BatteryInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;,
        Lcom/android/settings/fuelgauge/BatteryInfo$Callback;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BatteryInfo"


# instance fields
.field public averageTimeToDischarge:J

.field public batteryLevel:I

.field public batteryPercentString:Ljava/lang/String;

.field public chargeLabel:Ljava/lang/CharSequence;

.field public discharging:Z

.field private mCharging:Z

.field private mStats:Landroid/os/BatteryStats;

.field public remainingLabel:Ljava/lang/CharSequence;

.field public remainingTimeUs:J

.field public statusLabel:Ljava/lang/String;

.field public suggestionLabel:Ljava/lang/String;

.field private timePeriod:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->discharging:Z

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingTimeUs:J

    .line 49
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->averageTimeToDischarge:J

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/fuelgauge/BatteryInfo;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/settings/fuelgauge/BatteryInfo;

    .line 42
    iget-wide v0, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->timePeriod:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/android/settings/fuelgauge/BatteryInfo;J)J
    .locals 0
    .param p0, "x0"    # Lcom/android/settings/fuelgauge/BatteryInfo;
    .param p1, "x1"    # J

    .line 42
    iput-wide p1, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->timePeriod:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/android/settings/fuelgauge/BatteryInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/fuelgauge/BatteryInfo;

    .line 42
    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->mCharging:Z

    return v0
.end method

.method public static getBatteryInfo(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;Lcom/android/settingslib/fuelgauge/Estimate;JZ)Lcom/android/settings/fuelgauge/BatteryInfo;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "batteryBroadcast"    # Landroid/content/Intent;
    .param p2, "stats"    # Landroid/os/BatteryStats;
    .param p3, "estimate"    # Lcom/android/settingslib/fuelgauge/Estimate;
    .param p4, "elapsedRealtimeUs"    # J
    .param p6, "shortString"    # Z
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 228
    move-object/from16 v6, p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 229
    .local v7, "startTime":J
    new-instance v0, Lcom/android/settings/fuelgauge/BatteryInfo;

    invoke-direct {v0}, Lcom/android/settings/fuelgauge/BatteryInfo;-><init>()V

    move-object v9, v0

    .line 230
    .local v9, "info":Lcom/android/settings/fuelgauge/BatteryInfo;
    move-object/from16 v10, p2

    iput-object v10, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->mStats:Landroid/os/BatteryStats;

    .line 231
    invoke-static/range {p1 .. p1}, Lcom/android/settings/Utils;->getBatteryLevel(Landroid/content/Intent;)I

    move-result v0

    iput v0, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryLevel:I

    .line 232
    iget v0, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryLevel:I

    invoke-static {v0}, Lcom/android/settings/Utils;->formatPercentage(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryPercentString:Ljava/lang/String;

    .line 233
    const/4 v0, 0x0

    const-string v1, "plugged"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->mCharging:Z

    .line 234
    invoke-virtual/range {p3 .. p3}, Lcom/android/settingslib/fuelgauge/Estimate;->getAverageDischargeTime()J

    move-result-wide v0

    iput-wide v0, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->averageTimeToDischarge:J

    .line 235
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 237
    .local v11, "resources":Landroid/content/res/Resources;
    invoke-static {v11, v6}, Lcom/android/settings/Utils;->getBatteryStatus(Landroid/content/res/Resources;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->statusLabel:Ljava/lang/String;

    .line 238
    iget-boolean v0, v9, Lcom/android/settings/fuelgauge/BatteryInfo;->mCharging:Z

    if-nez v0, :cond_1

    .line 239
    move-object v12, p0

    move-object/from16 v13, p3

    move/from16 v14, p6

    invoke-static {p0, v14, v13, v9}, Lcom/android/settings/fuelgauge/BatteryInfo;->updateBatteryInfoDischarging(Landroid/content/Context;ZLcom/android/settingslib/fuelgauge/Estimate;Lcom/android/settings/fuelgauge/BatteryInfo;)V

    goto :goto_0

    .line 241
    :cond_1
    move-object v12, p0

    move-object/from16 v13, p3

    move/from16 v14, p6

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p4

    move-object v5, v9

    invoke-static/range {v0 .. v5}, Lcom/android/settings/fuelgauge/BatteryInfo;->updateBatteryInfoCharging(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;JLcom/android/settings/fuelgauge/BatteryInfo;)V

    .line 243
    :goto_0
    const-string v0, "BatteryInfo"

    const-string v1, "time for getBatteryInfo"

    invoke-static {v0, v1, v7, v8}, Lcom/android/settings/fuelgauge/BatteryUtils;->logRuntime(Ljava/lang/String;Ljava/lang/String;J)V

    .line 244
    return-object v9
.end method

.method public static getBatteryInfo(Landroid/content/Context;Lcom/android/internal/os/BatteryStatsHelper;Z)Lcom/android/settings/fuelgauge/BatteryInfo;
    .locals 27
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "statsHelper"    # Lcom/android/internal/os/BatteryStatsHelper;
    .param p2, "shortString"    # Z

    .line 169
    move-object/from16 v7, p0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 170
    .local v8, "batteryStatsTime":J
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    .line 171
    new-instance v2, Lcom/android/internal/os/BatteryStatsHelper;

    invoke-direct {v2, v7, v1}, Lcom/android/internal/os/BatteryStatsHelper;-><init>(Landroid/content/Context;Z)V

    .line 173
    .local v2, "localStatsHelper":Lcom/android/internal/os/BatteryStatsHelper;
    move-object v3, v0

    check-cast v3, Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lcom/android/internal/os/BatteryStatsHelper;->create(Landroid/os/Bundle;)V

    .line 174
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsHelper;->getStats()Landroid/os/BatteryStats;

    move-result-object v2

    .line 175
    .local v2, "stats":Landroid/os/BatteryStats;
    move-object v10, v2

    goto :goto_0

    .line 176
    .end local v2    # "stats":Landroid/os/BatteryStats;
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/os/BatteryStatsHelper;->getStats()Landroid/os/BatteryStats;

    move-result-object v2

    move-object v10, v2

    .line 178
    .local v10, "stats":Landroid/os/BatteryStats;
    :goto_0
    const-string v2, "BatteryInfo"

    const-string v3, "time for getStats"

    invoke-static {v2, v3, v8, v9}, Lcom/android/settings/fuelgauge/BatteryUtils;->logRuntime(Ljava/lang/String;Ljava/lang/String;J)V

    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 181
    .local v11, "startTime":J
    nop

    .line 182
    invoke-static/range {p0 .. p0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/android/settings/overlay/FeatureFactory;->getPowerUsageFeatureProvider(Landroid/content/Context;)Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    move-result-object v13

    .line 184
    .local v13, "provider":Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/android/settingslib/utils/PowerUtil;->convertMsToUs(J)J

    move-result-wide v14

    .line 186
    .local v14, "elapsedRealtimeUs":J
    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v6

    .line 189
    .local v6, "batteryBroadcast":Landroid/content/Intent;
    const/4 v0, -0x1

    .line 190
    const-string v3, "plugged"

    invoke-virtual {v6, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    move/from16 v16, v1

    .line 192
    .local v16, "discharging":Z
    if-eqz v16, :cond_4

    if-eqz v13, :cond_4

    .line 193
    invoke-interface {v13, v7}, Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;->isEnhancedBatteryPredictionEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 194
    invoke-interface {v13, v7}, Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;->getEnhancedBatteryPrediction(Landroid/content/Context;)Lcom/android/settingslib/fuelgauge/Estimate;

    move-result-object v4

    .line 195
    .local v4, "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    if-eqz v4, :cond_2

    .line 196
    invoke-static {v7, v4}, Lcom/android/settingslib/fuelgauge/Estimate;->storeCachedEstimate(Landroid/content/Context;Lcom/android/settingslib/fuelgauge/Estimate;)V

    .line 197
    nop

    .line 198
    const-string v0, "time for enhanced BatteryInfo"

    invoke-static {v2, v0, v11, v12}, Lcom/android/settings/fuelgauge/BatteryUtils;->logRuntime(Ljava/lang/String;Ljava/lang/String;J)V

    .line 199
    move-object/from16 v0, p0

    move-object v1, v6

    move-object v2, v10

    move-object v3, v4

    move-object/from16 v17, v4

    .end local v4    # "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    .local v17, "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    move-wide v4, v14

    move-object/from16 v18, v6

    .end local v6    # "batteryBroadcast":Landroid/content/Intent;
    .local v18, "batteryBroadcast":Landroid/content/Intent;
    move/from16 v6, p2

    invoke-static/range {v0 .. v6}, Lcom/android/settings/fuelgauge/BatteryInfo;->getBatteryInfo(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;Lcom/android/settingslib/fuelgauge/Estimate;JZ)Lcom/android/settings/fuelgauge/BatteryInfo;

    move-result-object v0

    return-object v0

    .line 195
    .end local v17    # "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    .end local v18    # "batteryBroadcast":Landroid/content/Intent;
    .restart local v4    # "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    .restart local v6    # "batteryBroadcast":Landroid/content/Intent;
    :cond_2
    move-object/from16 v17, v4

    move-object/from16 v18, v6

    .end local v4    # "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    .end local v6    # "batteryBroadcast":Landroid/content/Intent;
    .restart local v17    # "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    .restart local v18    # "batteryBroadcast":Landroid/content/Intent;
    goto :goto_2

    .line 193
    .end local v17    # "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    .end local v18    # "batteryBroadcast":Landroid/content/Intent;
    .restart local v6    # "batteryBroadcast":Landroid/content/Intent;
    :cond_3
    move-object/from16 v18, v6

    .end local v6    # "batteryBroadcast":Landroid/content/Intent;
    .restart local v18    # "batteryBroadcast":Landroid/content/Intent;
    goto :goto_2

    .line 192
    .end local v18    # "batteryBroadcast":Landroid/content/Intent;
    .restart local v6    # "batteryBroadcast":Landroid/content/Intent;
    :cond_4
    move-object/from16 v18, v6

    .line 203
    .end local v6    # "batteryBroadcast":Landroid/content/Intent;
    .restart local v18    # "batteryBroadcast":Landroid/content/Intent;
    :goto_2
    if-eqz v16, :cond_5

    .line 204
    invoke-virtual {v10, v14, v15}, Landroid/os/BatteryStats;->computeBatteryTimeRemaining(J)J

    move-result-wide v0

    goto :goto_3

    :cond_5
    const-wide/16 v0, 0x0

    :goto_3
    move-wide/from16 v19, v0

    .line 205
    .local v19, "prediction":J
    new-instance v0, Lcom/android/settingslib/fuelgauge/Estimate;

    .line 206
    invoke-static/range {v19 .. v20}, Lcom/android/settingslib/utils/PowerUtil;->convertUsToMs(J)J

    move-result-wide v22

    const/16 v24, 0x0

    const-wide/16 v25, -0x1

    move-object/from16 v21, v0

    invoke-direct/range {v21 .. v26}, Lcom/android/settingslib/fuelgauge/Estimate;-><init>(JZJ)V

    move-object v3, v0

    .line 209
    .local v3, "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    const-string v0, "time for regular BatteryInfo"

    invoke-static {v2, v0, v11, v12}, Lcom/android/settings/fuelgauge/BatteryUtils;->logRuntime(Ljava/lang/String;Ljava/lang/String;J)V

    .line 210
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object v2, v10

    move-wide v4, v14

    move/from16 v6, p2

    invoke-static/range {v0 .. v6}, Lcom/android/settings/fuelgauge/BatteryInfo;->getBatteryInfo(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;Lcom/android/settingslib/fuelgauge/Estimate;JZ)Lcom/android/settings/fuelgauge/BatteryInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getBatteryInfo(Landroid/content/Context;Lcom/android/settings/fuelgauge/BatteryInfo$Callback;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callback"    # Lcom/android/settings/fuelgauge/BatteryInfo$Callback;

    .line 140
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/android/settings/fuelgauge/BatteryInfo;->getBatteryInfo(Landroid/content/Context;Lcom/android/settings/fuelgauge/BatteryInfo$Callback;Lcom/android/internal/os/BatteryStatsHelper;Z)V

    .line 142
    return-void
.end method

.method public static getBatteryInfo(Landroid/content/Context;Lcom/android/settings/fuelgauge/BatteryInfo$Callback;Lcom/android/internal/os/BatteryStatsHelper;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callback"    # Lcom/android/settings/fuelgauge/BatteryInfo$Callback;
    .param p2, "statsHelper"    # Lcom/android/internal/os/BatteryStatsHelper;
    .param p3, "shortString"    # Z

    .line 151
    new-instance v0, Lcom/android/settings/fuelgauge/BatteryInfo$2;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/android/settings/fuelgauge/BatteryInfo$2;-><init>(Landroid/content/Context;Lcom/android/internal/os/BatteryStatsHelper;ZLcom/android/settings/fuelgauge/BatteryInfo$Callback;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 163
    invoke-virtual {v0, v1}, Lcom/android/settings/fuelgauge/BatteryInfo$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 164
    return-void
.end method

.method public static getBatteryInfo(Landroid/content/Context;Lcom/android/settings/fuelgauge/BatteryInfo$Callback;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callback"    # Lcom/android/settings/fuelgauge/BatteryInfo$Callback;
    .param p2, "shortString"    # Z

    .line 146
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/android/settings/fuelgauge/BatteryInfo;->getBatteryInfo(Landroid/content/Context;Lcom/android/settings/fuelgauge/BatteryInfo$Callback;Lcom/android/internal/os/BatteryStatsHelper;Z)V

    .line 147
    return-void
.end method

.method public static getBatteryInfoOld(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;JZ)Lcom/android/settings/fuelgauge/BatteryInfo;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "batteryBroadcast"    # Landroid/content/Intent;
    .param p2, "stats"    # Landroid/os/BatteryStats;
    .param p3, "elapsedRealtimeUs"    # J
    .param p5, "shortString"    # Z
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 217
    new-instance v6, Lcom/android/settingslib/fuelgauge/Estimate;

    .line 218
    invoke-virtual {p2, p3, p4}, Landroid/os/BatteryStats;->computeBatteryTimeRemaining(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/settingslib/utils/PowerUtil;->convertUsToMs(J)J

    move-result-wide v1

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/settingslib/fuelgauge/Estimate;-><init>(JZJ)V

    move-object v3, v6

    .line 221
    .local v3, "estimate":Lcom/android/settingslib/fuelgauge/Estimate;
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/android/settings/fuelgauge/BatteryInfo;->getBatteryInfo(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;Lcom/android/settingslib/fuelgauge/Estimate;JZ)Lcom/android/settings/fuelgauge/BatteryInfo;

    move-result-object v0

    return-object v0
.end method

.method public static varargs parse(Landroid/os/BatteryStats;[Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;)V
    .locals 34
    .param p0, "stats"    # Landroid/os/BatteryStats;
    .param p1, "parsers"    # [Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;

    .line 310
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-wide/16 v2, 0x0

    .line 311
    .local v2, "startWalltime":J
    const-wide/16 v4, 0x0

    .line 312
    .local v4, "endWalltime":J
    const-wide/16 v6, 0x0

    .line 313
    .local v6, "historyStart":J
    const-wide/16 v8, 0x0

    .line 314
    .local v8, "historyEnd":J
    move-wide v10, v2

    .line 315
    .local v10, "curWalltime":J
    const-wide/16 v12, 0x0

    .line 316
    .local v12, "lastWallTime":J
    const-wide/16 v14, 0x0

    .line 317
    .local v14, "lastRealtime":J
    const/16 v16, 0x0

    .line 318
    .local v16, "lastInteresting":I
    const/16 v17, 0x0

    .line 319
    .local v17, "pos":I
    const/16 v18, 0x1

    .line 320
    .local v18, "first":Z
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->startIteratingHistoryLocked()Z

    move-result v19

    const-wide/16 v20, 0x0

    move-wide/from16 v22, v2

    .end local v2    # "startWalltime":J
    .local v22, "startWalltime":J
    const/4 v3, 0x5

    if-eqz v19, :cond_7

    .line 321
    new-instance v19, Landroid/os/BatteryStats$HistoryItem;

    invoke-direct/range {v19 .. v19}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    move-object/from16 v24, v19

    .line 322
    .local v24, "rec":Landroid/os/BatteryStats$HistoryItem;
    :goto_0
    move-object/from16 v2, v24

    .end local v24    # "rec":Landroid/os/BatteryStats$HistoryItem;
    .local v2, "rec":Landroid/os/BatteryStats$HistoryItem;
    invoke-virtual {v0, v2}, Landroid/os/BatteryStats;->getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 323
    add-int/lit8 v17, v17, 0x1

    .line 324
    if-eqz v18, :cond_0

    .line 325
    const/16 v18, 0x0

    .line 326
    iget-wide v6, v2, Landroid/os/BatteryStats$HistoryItem;->time:J

    .line 328
    :cond_0
    move-wide/from16 v24, v4

    .end local v4    # "endWalltime":J
    .local v24, "endWalltime":J
    iget-byte v4, v2, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    if-eq v4, v3, :cond_1

    iget-byte v4, v2, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v5, 0x7

    if-ne v4, v5, :cond_4

    .line 338
    :cond_1
    iget-wide v4, v2, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    const-wide v26, 0x39ef8b000L

    add-long v26, v12, v26

    cmp-long v4, v4, v26

    if-gtz v4, :cond_2

    iget-wide v4, v2, Landroid/os/BatteryStats$HistoryItem;->time:J

    const-wide/32 v26, 0x493e0

    add-long v26, v6, v26

    cmp-long v4, v4, v26

    if-gez v4, :cond_3

    .line 340
    :cond_2
    const-wide/16 v22, 0x0

    .line 342
    :cond_3
    iget-wide v12, v2, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    .line 343
    iget-wide v14, v2, Landroid/os/BatteryStats$HistoryItem;->time:J

    .line 344
    cmp-long v4, v22, v20

    if-nez v4, :cond_4

    .line 345
    sub-long v4, v14, v6

    sub-long v22, v12, v4

    .line 348
    :cond_4
    invoke-virtual {v2}, Landroid/os/BatteryStats$HistoryItem;->isDeltaData()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 349
    move/from16 v16, v17

    .line 350
    iget-wide v8, v2, Landroid/os/BatteryStats$HistoryItem;->time:J

    move-wide/from16 v4, v24

    move-object/from16 v24, v2

    goto :goto_0

    .line 348
    :cond_5
    move-wide/from16 v4, v24

    move-object/from16 v24, v2

    goto :goto_0

    .line 322
    .end local v24    # "endWalltime":J
    .restart local v4    # "endWalltime":J
    :cond_6
    move-wide/from16 v24, v4

    .end local v4    # "endWalltime":J
    .restart local v24    # "endWalltime":J
    move-wide/from16 v4, v22

    goto :goto_1

    .line 320
    .end local v2    # "rec":Landroid/os/BatteryStats$HistoryItem;
    .end local v24    # "endWalltime":J
    .restart local v4    # "endWalltime":J
    :cond_7
    move-wide/from16 v24, v4

    .end local v4    # "endWalltime":J
    .restart local v24    # "endWalltime":J
    move-wide/from16 v4, v22

    .line 354
    .end local v22    # "startWalltime":J
    .local v4, "startWalltime":J
    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->finishIteratingHistoryLocked()V

    .line 355
    add-long v22, v12, v8

    move-wide/from16 v26, v4

    .end local v4    # "startWalltime":J
    .local v26, "startWalltime":J
    sub-long v3, v22, v14

    .line 357
    .end local v24    # "endWalltime":J
    .local v3, "endWalltime":J
    const/4 v5, 0x0

    .line 358
    .local v5, "i":I
    move/from16 v22, v16

    .line 360
    .local v22, "N":I
    const/16 v23, 0x0

    move/from16 v2, v23

    .local v2, "j":I
    :goto_2
    move/from16 v23, v5

    .end local v5    # "i":I
    .local v23, "i":I
    array-length v5, v1

    if-ge v2, v5, :cond_8

    .line 361
    aget-object v5, v1, v2

    move-wide/from16 v28, v8

    move-wide/from16 v8, v26

    .end local v26    # "startWalltime":J
    .local v8, "startWalltime":J
    .local v28, "historyEnd":J
    invoke-interface {v5, v8, v9, v3, v4}, Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;->onParsingStarted(JJ)V

    .line 360
    add-int/lit8 v2, v2, 0x1

    move/from16 v5, v23

    move-wide/from16 v8, v28

    goto :goto_2

    .end local v28    # "historyEnd":J
    .local v8, "historyEnd":J
    .restart local v26    # "startWalltime":J
    :cond_8
    move-wide/from16 v28, v8

    move-wide/from16 v8, v26

    .line 363
    .end local v2    # "j":I
    .end local v26    # "startWalltime":J
    .local v8, "startWalltime":J
    .restart local v28    # "historyEnd":J
    cmp-long v2, v3, v8

    if-lez v2, :cond_14

    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->startIteratingHistoryLocked()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 364
    new-instance v2, Landroid/os/BatteryStats$HistoryItem;

    invoke-direct {v2}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    move-object v5, v2

    move-wide/from16 v25, v10

    move/from16 v10, v23

    .line 365
    .end local v23    # "i":I
    .local v5, "rec":Landroid/os/BatteryStats$HistoryItem;
    .local v10, "i":I
    .local v25, "curWalltime":J
    :goto_3
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats;->getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    move-result v2

    if-eqz v2, :cond_13

    move/from16 v11, v22

    .end local v22    # "N":I
    .local v11, "N":I
    if-ge v10, v11, :cond_12

    .line 366
    invoke-virtual {v5}, Landroid/os/BatteryStats$HistoryItem;->isDeltaData()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 367
    move-wide/from16 v30, v3

    .end local v3    # "endWalltime":J
    .local v30, "endWalltime":J
    iget-wide v2, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    sub-long/2addr v2, v14

    add-long v25, v25, v2

    .line 368
    iget-wide v2, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    .line 369
    .end local v14    # "lastRealtime":J
    .local v2, "lastRealtime":J
    sub-long v14, v25, v8

    .line 370
    .local v14, "x":J
    cmp-long v4, v14, v20

    if-gez v4, :cond_9

    .line 371
    const-wide/16 v14, 0x0

    .line 373
    :cond_9
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_4
    array-length v0, v1

    if-ge v4, v0, :cond_a

    .line 374
    aget-object v0, v1, v4

    invoke-interface {v0, v14, v15, v5}, Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;->onDataPoint(JLandroid/os/BatteryStats$HistoryItem;)V

    .line 373
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    goto :goto_4

    .line 376
    .end local v4    # "j":I
    .end local v14    # "x":J
    :cond_a
    move-wide v14, v2

    const/4 v2, 0x5

    goto :goto_9

    .line 377
    .end local v2    # "lastRealtime":J
    .end local v30    # "endWalltime":J
    .restart local v3    # "endWalltime":J
    .local v14, "lastRealtime":J
    :cond_b
    move-wide/from16 v30, v3

    .end local v3    # "endWalltime":J
    .restart local v30    # "endWalltime":J
    move-wide/from16 v3, v25

    .line 378
    .local v3, "lastWalltime":J
    iget-byte v0, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v2, 0x5

    if-eq v0, v2, :cond_d

    iget-byte v0, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v2, 0x7

    if-ne v0, v2, :cond_c

    goto :goto_5

    :cond_c
    move-wide/from16 v22, v3

    goto :goto_7

    :cond_d
    const/4 v2, 0x7

    .line 380
    :goto_5
    move-wide/from16 v22, v3

    .end local v3    # "lastWalltime":J
    .local v22, "lastWalltime":J
    iget-wide v2, v5, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    cmp-long v0, v2, v8

    if-ltz v0, :cond_e

    .line 381
    iget-wide v2, v5, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    move-wide/from16 v25, v2

    .end local v25    # "curWalltime":J
    .local v2, "curWalltime":J
    goto :goto_6

    .line 383
    .end local v2    # "curWalltime":J
    .restart local v25    # "curWalltime":J
    :cond_e
    iget-wide v2, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    sub-long/2addr v2, v6

    add-long/2addr v2, v8

    move-wide/from16 v25, v2

    .line 385
    :goto_6
    iget-wide v14, v5, Landroid/os/BatteryStats$HistoryItem;->time:J

    .line 388
    :goto_7
    iget-byte v0, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v2, 0x6

    if-eq v0, v2, :cond_10

    iget-byte v0, v5, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v2, 0x5

    if-ne v0, v2, :cond_f

    sub-long v3, v22, v25

    .line 390
    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    const-wide/32 v32, 0x36ee80

    cmp-long v0, v3, v32

    if-lez v0, :cond_11

    .line 391
    :cond_f
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_8
    array-length v3, v1

    if-ge v0, v3, :cond_11

    .line 392
    aget-object v3, v1, v0

    invoke-interface {v3}, Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;->onDataGap()V

    .line 391
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 388
    .end local v0    # "j":I
    :cond_10
    const/4 v2, 0x5

    .line 396
    .end local v22    # "lastWalltime":J
    :cond_11
    :goto_9
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v0, p0

    move/from16 v22, v11

    move-wide/from16 v3, v30

    goto/16 :goto_3

    .line 365
    .end local v30    # "endWalltime":J
    .local v3, "endWalltime":J
    :cond_12
    move-wide/from16 v30, v3

    .end local v3    # "endWalltime":J
    .restart local v30    # "endWalltime":J
    goto :goto_a

    .end local v11    # "N":I
    .end local v30    # "endWalltime":J
    .restart local v3    # "endWalltime":J
    .local v22, "N":I
    :cond_13
    move-wide/from16 v30, v3

    move/from16 v11, v22

    .line 400
    .end local v3    # "endWalltime":J
    .end local v5    # "rec":Landroid/os/BatteryStats$HistoryItem;
    .end local v22    # "N":I
    .restart local v11    # "N":I
    .restart local v30    # "endWalltime":J
    :goto_a
    move/from16 v23, v10

    move v0, v11

    goto :goto_b

    .line 363
    .end local v11    # "N":I
    .end local v25    # "curWalltime":J
    .end local v30    # "endWalltime":J
    .restart local v3    # "endWalltime":J
    .local v10, "curWalltime":J
    .restart local v22    # "N":I
    .restart local v23    # "i":I
    :cond_14
    move-wide/from16 v30, v3

    move/from16 v0, v22

    .line 400
    .end local v3    # "endWalltime":J
    .end local v22    # "N":I
    .local v0, "N":I
    .restart local v30    # "endWalltime":J
    move-wide/from16 v25, v10

    .end local v10    # "curWalltime":J
    .restart local v25    # "curWalltime":J
    :goto_b
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->finishIteratingHistoryLocked()V

    .line 402
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_c
    array-length v3, v1

    if-ge v2, v3, :cond_15

    .line 403
    aget-object v3, v1, v2

    invoke-interface {v3}, Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;->onParsingDone()V

    .line 402
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 405
    .end local v2    # "j":I
    :cond_15
    return-void
.end method

.method private static updateBatteryInfoCharging(Landroid/content/Context;Landroid/content/Intent;Landroid/os/BatteryStats;JLcom/android/settings/fuelgauge/BatteryInfo;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "batteryBroadcast"    # Landroid/content/Intent;
    .param p2, "stats"    # Landroid/os/BatteryStats;
    .param p3, "elapsedRealtimeUs"    # J
    .param p5, "info"    # Lcom/android/settings/fuelgauge/BatteryInfo;

    .line 249
    move-object v0, p0

    move-object/from16 v1, p5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 250
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-virtual/range {p2 .. p4}, Landroid/os/BatteryStats;->computeChargeTimeRemaining(J)J

    move-result-wide v3

    .line 251
    .local v3, "chargeTime":J
    const/4 v5, 0x1

    const-string v6, "status"

    move-object v7, p1

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 253
    .local v6, "status":I
    const/4 v8, 0x0

    iput-boolean v8, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->discharging:Z

    .line 254
    const/4 v9, 0x0

    iput-object v9, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->suggestionLabel:Ljava/lang/String;

    .line 255
    const-wide/16 v10, 0x0

    cmp-long v10, v3, v10

    const/4 v11, 0x2

    if-lez v10, :cond_0

    const/4 v10, 0x5

    if-eq v6, v10, :cond_0

    .line 256
    iput-wide v3, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingTimeUs:J

    .line 257
    iget-wide v9, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingTimeUs:J

    .line 258
    invoke-static {v9, v10}, Lcom/android/settingslib/utils/PowerUtil;->convertUsToMs(J)J

    move-result-wide v9

    long-to-double v9, v9

    .line 257
    invoke-static {p0, v9, v10, v8}, Lcom/android/settingslib/utils/StringUtil;->formatElapsedTime(Landroid/content/Context;DZ)Ljava/lang/CharSequence;

    move-result-object v9

    .line 259
    .local v9, "timeString":Ljava/lang/CharSequence;
    const v10, 0x7f1210f0

    .line 260
    .local v10, "resId":I
    const v12, 0x7f1210fd

    new-array v13, v5, [Ljava/lang/Object;

    aput-object v9, v13, v8

    invoke-virtual {p0, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingLabel:Ljava/lang/CharSequence;

    .line 262
    new-array v11, v11, [Ljava/lang/Object;

    iget-object v12, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryPercentString:Ljava/lang/String;

    aput-object v12, v11, v8

    aput-object v9, v11, v5

    invoke-virtual {p0, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->chargeLabel:Ljava/lang/CharSequence;

    .line 263
    .end local v9    # "timeString":Ljava/lang/CharSequence;
    .end local v10    # "resId":I
    goto :goto_1

    .line 264
    :cond_0
    const v10, 0x7f120251

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 266
    .local v10, "chargeStatusLabel":Ljava/lang/String;
    iput-object v9, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingLabel:Ljava/lang/CharSequence;

    .line 267
    iget v9, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryLevel:I

    const/16 v12, 0x64

    if-ne v9, v12, :cond_1

    iget-object v5, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryPercentString:Ljava/lang/String;

    goto :goto_0

    .line 268
    :cond_1
    const v9, 0x7f1210ef

    new-array v11, v11, [Ljava/lang/Object;

    iget-object v12, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryPercentString:Ljava/lang/String;

    aput-object v12, v11, v8

    aput-object v10, v11, v5

    invoke-virtual {v2, v9, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :goto_0
    iput-object v5, v1, Lcom/android/settings/fuelgauge/BatteryInfo;->chargeLabel:Ljava/lang/CharSequence;

    .line 271
    .end local v10    # "chargeStatusLabel":Ljava/lang/String;
    :goto_1
    return-void
.end method

.method private static updateBatteryInfoDischarging(Landroid/content/Context;ZLcom/android/settingslib/fuelgauge/Estimate;Lcom/android/settings/fuelgauge/BatteryInfo;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shortString"    # Z
    .param p2, "estimate"    # Lcom/android/settingslib/fuelgauge/Estimate;
    .param p3, "info"    # Lcom/android/settings/fuelgauge/BatteryInfo;

    .line 275
    invoke-virtual {p2}, Lcom/android/settingslib/fuelgauge/Estimate;->getEstimateMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/settingslib/utils/PowerUtil;->convertMsToUs(J)J

    move-result-wide v0

    .line 276
    .local v0, "drainTimeUs":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-lez v2, :cond_2

    .line 277
    iput-wide v0, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingTimeUs:J

    .line 278
    nop

    .line 280
    invoke-static {v0, v1}, Lcom/android/settingslib/utils/PowerUtil;->convertUsToMs(J)J

    move-result-wide v4

    .line 282
    invoke-virtual {p2}, Lcom/android/settingslib/fuelgauge/Estimate;->isBasedOnUsage()Z

    move-result v2

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    move v2, v6

    goto :goto_0

    :cond_0
    move v2, v7

    .line 278
    :goto_0
    invoke-static {p0, v4, v5, v3, v2}, Lcom/android/settingslib/utils/PowerUtil;->getBatteryRemainingStringFormatted(Landroid/content/Context;JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingLabel:Ljava/lang/CharSequence;

    .line 284
    nop

    .line 286
    invoke-static {v0, v1}, Lcom/android/settingslib/utils/PowerUtil;->convertUsToMs(J)J

    move-result-wide v2

    iget-object v4, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryPercentString:Ljava/lang/String;

    .line 288
    invoke-virtual {p2}, Lcom/android/settingslib/fuelgauge/Estimate;->isBasedOnUsage()Z

    move-result v5

    if-eqz v5, :cond_1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    move v6, v7

    .line 284
    :goto_1
    invoke-static {p0, v2, v3, v4, v6}, Lcom/android/settingslib/utils/PowerUtil;->getBatteryRemainingStringFormatted(Landroid/content/Context;JLjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->chargeLabel:Ljava/lang/CharSequence;

    .line 290
    nop

    .line 291
    invoke-static {v0, v1}, Lcom/android/settingslib/utils/PowerUtil;->convertUsToMs(J)J

    move-result-wide v2

    .line 290
    invoke-static {p0, v2, v3}, Lcom/android/settingslib/utils/PowerUtil;->getBatteryTipStringFormatted(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->suggestionLabel:Ljava/lang/String;

    goto :goto_2

    .line 293
    :cond_2
    iput-object v3, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingLabel:Ljava/lang/CharSequence;

    .line 294
    iput-object v3, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->suggestionLabel:Ljava/lang/String;

    .line 295
    iget-object v2, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->batteryPercentString:Ljava/lang/String;

    iput-object v2, p3, Lcom/android/settings/fuelgauge/BatteryInfo;->chargeLabel:Ljava/lang/CharSequence;

    .line 297
    :goto_2
    return-void
.end method


# virtual methods
.method public varargs bindHistory(Lcom/android/settings/widget/UsageView;[Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;)V
    .locals 13
    .param p1, "view"    # Lcom/android/settings/widget/UsageView;
    .param p2, "parsers"    # [Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;

    .line 63
    invoke-virtual {p1}, Lcom/android/settings/widget/UsageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 64
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/android/settings/fuelgauge/BatteryInfo$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/android/settings/fuelgauge/BatteryInfo$1;-><init>(Lcom/android/settings/fuelgauge/BatteryInfo;Lcom/android/settings/widget/UsageView;Landroid/content/Context;)V

    .line 123
    .local v1, "parser":Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;
    array-length v2, p2

    const/4 v3, 0x1

    add-int/2addr v2, v3

    new-array v2, v2, [Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;

    .line 124
    .local v2, "parserList":[Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, p2

    if-ge v4, v5, :cond_0

    .line 125
    aget-object v5, p2, v4

    aput-object v5, v2, v4

    .line 124
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 127
    .end local v4    # "i":I
    :cond_0
    array-length v4, p2

    aput-object v1, v2, v4

    .line 128
    iget-object v4, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->mStats:Landroid/os/BatteryStats;

    invoke-static {v4, v2}, Lcom/android/settings/fuelgauge/BatteryInfo;->parse(Landroid/os/BatteryStats;[Lcom/android/settings/fuelgauge/BatteryInfo$BatteryDataParser;)V

    .line 129
    const v4, 0x7f12041c

    new-array v5, v3, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->timePeriod:J

    .line 130
    invoke-static {v0, v6, v7}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    .line 129
    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, "timeString":Ljava/lang/String;
    const-string v5, ""

    .line 132
    .local v5, "remaining":Ljava/lang/String;
    iget-wide v8, p0, Lcom/android/settings/fuelgauge/BatteryInfo;->remainingTimeUs:J

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_1

    .line 133
    const v6, 0x7f1211f8

    new-array v10, v3, [Ljava/lang/Object;

    const-wide/16 v11, 0x3e8

    div-long/2addr v8, v11

    .line 134
    invoke-static {v0, v8, v9}, Landroid/text/format/Formatter;->formatShortElapsedTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v10, v7

    .line 133
    invoke-virtual {v0, v6, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 136
    :cond_1
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/CharSequence;

    aput-object v4, v6, v7

    aput-object v5, v6, v3

    invoke-virtual {p1, v6}, Lcom/android/settings/widget/UsageView;->setBottomLabels([Ljava/lang/CharSequence;)V

    .line 137
    return-void
.end method
