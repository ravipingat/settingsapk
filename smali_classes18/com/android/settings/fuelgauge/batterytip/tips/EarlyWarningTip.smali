.class public Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;
.super Lcom/android/settings/fuelgauge/batterytip/tips/BatteryTip;
.source "EarlyWarningTip.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private mPowerSaveModeOn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 104
    new-instance v0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip$1;

    invoke-direct {v0}, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip$1;-><init>()V

    sput-object v0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "powerSaveModeOn"    # Z

    .line 34
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/fuelgauge/batterytip/tips/BatteryTip;-><init>(IIZ)V

    .line 35
    iput-boolean p2, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mPowerSaveModeOn:Z

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .line 39
    invoke-direct {p0, p1}, Lcom/android/settings/fuelgauge/batterytip/tips/BatteryTip;-><init>(Landroid/os/Parcel;)V

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mPowerSaveModeOn:Z

    .line 41
    return-void
.end method


# virtual methods
.method public getIconId()I
    .locals 2

    .line 61
    iget v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 62
    const v0, 0x7f0801aa

    goto :goto_0

    .line 63
    :cond_0
    const v0, 0x7f0801a8

    .line 61
    :goto_0
    return v0
.end method

.method public getIconTintColorId()I
    .locals 2

    .line 68
    iget v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 69
    const v0, 0x7f060062

    goto :goto_0

    .line 70
    :cond_0
    const v0, 0x7f06005d

    .line 68
    :goto_0
    return v0
.end method

.method public getSummary(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 53
    nop

    .line 54
    iget v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 55
    const v0, 0x7f120296

    goto :goto_0

    .line 56
    :cond_0
    const v0, 0x7f120298

    .line 53
    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 45
    nop

    .line 46
    iget v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 47
    const v0, 0x7f120297

    goto :goto_0

    .line 48
    :cond_0
    const v0, 0x7f120299

    .line 45
    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPowerSaveModeOn()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mPowerSaveModeOn:Z

    return v0
.end method

.method public log(Landroid/content/Context;Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "metricsFeatureProvider"    # Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    .line 90
    iget v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    const/16 v1, 0x547

    invoke-virtual {p2, p1, v1, v0}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(Landroid/content/Context;II)V

    .line 92
    return-void
.end method

.method public updateState(Lcom/android/settings/fuelgauge/batterytip/tips/BatteryTip;)V
    .locals 3
    .param p1, "tip"    # Lcom/android/settings/fuelgauge/batterytip/tips/BatteryTip;

    .line 75
    move-object v0, p1

    check-cast v0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;

    .line 76
    .local v0, "earlyWarningTip":Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;
    iget v1, v0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    if-nez v1, :cond_0

    .line 78
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    goto :goto_0

    .line 79
    :cond_0
    iget v1, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    if-nez v1, :cond_2

    iget v1, v0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 81
    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mPowerSaveModeOn:Z

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    iput v2, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {v0}, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->getState()I

    move-result v1

    iput v1, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mState:I

    .line 85
    :goto_0
    iget-boolean v1, v0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mPowerSaveModeOn:Z

    iput-boolean v1, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mPowerSaveModeOn:Z

    .line 86
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 96
    invoke-super {p0, p1, p2}, Lcom/android/settings/fuelgauge/batterytip/tips/BatteryTip;->writeToParcel(Landroid/os/Parcel;I)V

    .line 97
    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/batterytip/tips/EarlyWarningTip;->mPowerSaveModeOn:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 98
    return-void
.end method
