.class public Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;
.super Landroid/app/job/JobService;
.source "AnomalyDetectionJobService.java"


# static fields
.field static final MAX_DELAY_MS:J
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final ON:I = 0x1

.field static final STATSD_UID_FILED:I = 0x1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "AnomalyDetectionService"

.field static final UID_NULL:I = -0x1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field mIsJobCanceled:Z
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation

    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 65
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->MAX_DELAY_MS:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 57
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mLock:Ljava/lang/Object;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mIsJobCanceled:Z

    return-void
.end method

.method public static scheduleAnomalyDetection(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .line 73
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 74
    .local v0, "jobScheduler":Landroid/app/job/JobScheduler;
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 76
    .local v1, "component":Landroid/content/ComponentName;
    new-instance v2, Landroid/app/job/JobInfo$Builder;

    const v3, 0x7f0b0016

    invoke-direct {v2, v3, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    sget-wide v3, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->MAX_DELAY_MS:J

    .line 78
    invoke-virtual {v2, v3, v4}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v2

    .line 80
    .local v2, "jobBuilder":Landroid/app/job/JobInfo$Builder;
    invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v3

    new-instance v4, Landroid/app/job/JobWorkItem;

    invoke-direct {v4, p1}, Landroid/app/job/JobWorkItem;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/job/JobScheduler;->enqueue(Landroid/app/job/JobInfo;Landroid/app/job/JobWorkItem;)I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 82
    const-string v3, "AnomalyDetectionService"

    const-string v4, "Anomaly detection job service enqueue failed."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    return-void
.end method


# virtual methods
.method completeWork(Landroid/app/job/JobParameters;Landroid/app/job/JobWorkItem;)V
    .locals 2
    .param p1, "parameters"    # Landroid/app/job/JobParameters;
    .param p2, "item"    # Landroid/app/job/JobWorkItem;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 229
    iget-object v0, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 230
    :try_start_0
    iget-boolean v1, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mIsJobCanceled:Z

    if-eqz v1, :cond_0

    .line 231
    monitor-exit v0

    return-void

    .line 234
    :cond_0
    invoke-virtual {p1, p2}, Landroid/app/job/JobParameters;->completeWork(Landroid/app/job/JobWorkItem;)V

    .line 235
    monitor-exit v0

    .line 236
    return-void

    .line 235
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method dequeueWork(Landroid/app/job/JobParameters;)Landroid/app/job/JobWorkItem;
    .locals 2
    .param p1, "parameters"    # Landroid/app/job/JobParameters;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 218
    iget-object v0, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 219
    :try_start_0
    iget-boolean v1, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mIsJobCanceled:Z

    if-eqz v1, :cond_0

    .line 220
    const/4 v1, 0x0

    monitor-exit v0

    return-object v1

    .line 223
    :cond_0
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->dequeueWork()Landroid/app/job/JobWorkItem;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 224
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method extractUidFromStatsDimensionsValue(Landroid/os/StatsDimensionsValue;)I
    .locals 5
    .param p1, "statsDimensionsValue"    # Landroid/os/StatsDimensionsValue;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 195
    const/4 v0, -0x1

    if-nez p1, :cond_0

    .line 196
    return v0

    .line 198
    :cond_0
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/os/StatsDimensionsValue;->isValueType(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    invoke-virtual {p1}, Landroid/os/StatsDimensionsValue;->getField()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 201
    invoke-virtual {p1}, Landroid/os/StatsDimensionsValue;->getIntValue()I

    move-result v0

    return v0

    .line 203
    :cond_1
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Landroid/os/StatsDimensionsValue;->isValueType(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 204
    invoke-virtual {p1}, Landroid/os/StatsDimensionsValue;->getTupleValueList()Ljava/util/List;

    move-result-object v1

    .line 205
    .local v1, "values":Ljava/util/List;, "Ljava/util/List<Landroid/os/StatsDimensionsValue;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v2, v3, :cond_3

    .line 206
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/StatsDimensionsValue;

    invoke-virtual {p0, v4}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->extractUidFromStatsDimensionsValue(Landroid/os/StatsDimensionsValue;)I

    move-result v4

    .line 207
    .local v4, "uid":I
    if-eq v4, v0, :cond_2

    .line 208
    return v4

    .line 205
    .end local v4    # "uid":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 213
    .end local v1    # "values":Ljava/util/List;, "Ljava/util/List<Landroid/os/StatsDimensionsValue;>;"
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_3
    return v0
.end method

.method public synthetic lambda$onStartJob$0$AnomalyDetectionJobService(Landroid/app/job/JobParameters;)V
    .locals 21
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 92
    move-object/from16 v11, p0

    move-object/from16 v1, p0

    .line 93
    .local v1, "context":Landroid/content/Context;
    nop

    .line 94
    invoke-static/range {p0 .. p0}, Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;

    move-result-object v12

    .line 95
    .local v12, "batteryDatabaseManager":Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;
    new-instance v5, Lcom/android/settings/fuelgauge/batterytip/BatteryTipPolicy;

    invoke-direct {v5, v11}, Lcom/android/settings/fuelgauge/batterytip/BatteryTipPolicy;-><init>(Landroid/content/Context;)V

    .line 96
    .local v5, "policy":Lcom/android/settings/fuelgauge/batterytip/BatteryTipPolicy;
    invoke-static/range {p0 .. p0}, Lcom/android/settings/fuelgauge/BatteryUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/fuelgauge/BatteryUtils;

    move-result-object v13

    .line 97
    .local v13, "batteryUtils":Lcom/android/settings/fuelgauge/BatteryUtils;
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    .line 98
    .local v14, "contentResolver":Landroid/content/ContentResolver;
    const-class v0, Landroid/os/UserManager;

    invoke-virtual {v11, v0}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Landroid/os/UserManager;

    .line 99
    .local v15, "userManager":Landroid/os/UserManager;
    nop

    .line 100
    invoke-static {v1}, Lcom/android/settingslib/fuelgauge/PowerWhitelistBackend;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/fuelgauge/PowerWhitelistBackend;

    move-result-object v16

    .line 101
    .local v16, "powerWhitelistBackend":Lcom/android/settingslib/fuelgauge/PowerWhitelistBackend;
    nop

    .line 102
    invoke-static/range {p0 .. p0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/android/settings/overlay/FeatureFactory;->getPowerUsageFeatureProvider(Landroid/content/Context;)Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    move-result-object v17

    .line 103
    .local v17, "powerUsageFeatureProvider":Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;
    nop

    .line 104
    invoke-static/range {p0 .. p0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/FeatureFactory;->getMetricsFeatureProvider()Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    move-result-object v18

    .line 106
    .local v18, "metricsFeatureProvider":Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;
    invoke-virtual/range {p0 .. p1}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->dequeueWork(Landroid/app/job/JobParameters;)Landroid/app/job/JobWorkItem;

    move-result-object v0

    move-object v10, v0

    .local v10, "item":Landroid/app/job/JobWorkItem;
    :goto_0
    if-eqz v10, :cond_0

    .line 107
    nop

    .line 110
    invoke-virtual {v10}, Landroid/app/job/JobWorkItem;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v19

    .line 107
    move-object/from16 v0, p0

    move-object v2, v15

    move-object v3, v12

    move-object v4, v13

    move-object/from16 v6, v16

    move-object v7, v14

    move-object/from16 v8, v17

    move-object/from16 v9, v18

    move-object/from16 v20, v12

    move-object v12, v10

    .end local v10    # "item":Landroid/app/job/JobWorkItem;
    .local v12, "item":Landroid/app/job/JobWorkItem;
    .local v20, "batteryDatabaseManager":Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;
    move-object/from16 v10, v19

    invoke-virtual/range {v0 .. v10}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->saveAnomalyToDatabase(Landroid/content/Context;Landroid/os/UserManager;Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;Lcom/android/settings/fuelgauge/BatteryUtils;Lcom/android/settings/fuelgauge/batterytip/BatteryTipPolicy;Lcom/android/settingslib/fuelgauge/PowerWhitelistBackend;Landroid/content/ContentResolver;Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;Landroid/os/Bundle;)V

    .line 112
    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v12}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->completeWork(Landroid/app/job/JobParameters;Landroid/app/job/JobWorkItem;)V

    .line 106
    invoke-virtual/range {p0 .. p1}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->dequeueWork(Landroid/app/job/JobParameters;)Landroid/app/job/JobWorkItem;

    move-result-object v10

    move-object/from16 v12, v20

    .end local v12    # "item":Landroid/app/job/JobWorkItem;
    .restart local v10    # "item":Landroid/app/job/JobWorkItem;
    goto :goto_0

    .line 114
    .end local v10    # "item":Landroid/app/job/JobWorkItem;
    .end local v20    # "batteryDatabaseManager":Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;
    .local v12, "batteryDatabaseManager":Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;
    :cond_0
    return-void
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 2
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 88
    iget-object v0, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 89
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mIsJobCanceled:Z

    .line 90
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    new-instance v0, Lcom/android/settings/fuelgauge/batterytip/-$$Lambda$AnomalyDetectionJobService$7JxJe3rza0cCkIc77iCS-ZKPfL4;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/fuelgauge/batterytip/-$$Lambda$AnomalyDetectionJobService$7JxJe3rza0cCkIc77iCS-ZKPfL4;-><init>(Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;Landroid/app/job/JobParameters;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 116
    const/4 v0, 0x1

    return v0

    .line 90
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 2
    .param p1, "jobParameters"    # Landroid/app/job/JobParameters;

    .line 121
    iget-object v0, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 122
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->mIsJobCanceled:Z

    .line 123
    monitor-exit v0

    .line 124
    return v1

    .line 123
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method saveAnomalyToDatabase(Landroid/content/Context;Landroid/os/UserManager;Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;Lcom/android/settings/fuelgauge/BatteryUtils;Lcom/android/settings/fuelgauge/batterytip/BatteryTipPolicy;Lcom/android/settingslib/fuelgauge/PowerWhitelistBackend;Landroid/content/ContentResolver;Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;Landroid/os/Bundle;)V
    .locals 29
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userManager"    # Landroid/os/UserManager;
    .param p3, "databaseManager"    # Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;
    .param p4, "batteryUtils"    # Lcom/android/settings/fuelgauge/BatteryUtils;
    .param p5, "policy"    # Lcom/android/settings/fuelgauge/batterytip/BatteryTipPolicy;
    .param p6, "powerWhitelistBackend"    # Lcom/android/settingslib/fuelgauge/PowerWhitelistBackend;
    .param p7, "contentResolver"    # Landroid/content/ContentResolver;
    .param p8, "powerUsageFeatureProvider"    # Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;
    .param p9, "metricsFeatureProvider"    # Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;
    .param p10, "bundle"    # Landroid/os/Bundle;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 134
    move-object/from16 v1, p4

    move-object/from16 v2, p7

    move-object/from16 v3, p10

    .line 135
    const-string v0, "android.app.extra.STATS_DIMENSIONS_VALUE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/os/StatsDimensionsValue;

    .line 136
    .local v4, "intentDimsValue":Landroid/os/StatsDimensionsValue;
    nop

    .line 137
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 136
    const-string v0, "key_anomaly_timestamp"

    invoke-virtual {v3, v0, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 138
    .local v5, "timeMs":J
    const-string v0, "android.app.extra.STATS_BROADCAST_SUBSCRIBER_COOKIES"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    .line 140
    .local v14, "cookies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;

    .line 141
    invoke-static {v14}, Lcom/android/internal/util/ArrayUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v7

    const/4 v8, 0x0

    if-nez v7, :cond_0

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v7, ""

    :goto_0
    invoke-direct {v0, v7}, Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;-><init>(Ljava/lang/String;)V

    move-object v15, v0

    .line 142
    .local v15, "anomalyInfo":Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Extra stats value: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Landroid/os/StatsDimensionsValue;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v12, "AnomalyDetectionService"

    invoke-static {v12, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    move-object/from16 v13, p0

    :try_start_0
    invoke-virtual {v13, v4}, Lcom/android/settings/fuelgauge/batterytip/AnomalyDetectionJobService;->extractUidFromStatsDimensionsValue(Landroid/os/StatsDimensionsValue;)I

    move-result v0

    .line 146
    .local v0, "uid":I
    invoke-interface/range {p8 .. p8}, Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;->isSmartBatterySupported()Z

    move-result v7

    const/4 v9, 0x1

    if-eqz v7, :cond_1

    .line 148
    const-string v7, "adaptive_battery_management_enabled"

    .line 147
    invoke-static {v2, v7, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v9, :cond_2

    move v8, v9

    goto :goto_1

    .line 150
    :cond_1
    const-string v7, "app_auto_restriction_enabled"

    .line 149
    invoke-static {v2, v7, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v9, :cond_2

    move v8, v9

    :cond_2
    :goto_1
    move/from16 v16, v8

    .line 151
    .local v16, "autoFeatureOn":Z
    invoke-virtual {v1, v0}, Lcom/android/settings/fuelgauge/BatteryUtils;->getPackageName(I)Ljava/lang/String;

    move-result-object v7

    move-object v11, v7

    .line 152
    .local v11, "packageName":Ljava/lang/String;
    invoke-virtual {v1, v11}, Lcom/android/settings/fuelgauge/BatteryUtils;->getAppLongVersionCode(Ljava/lang/String;)J

    move-result-wide v7

    .line 153
    .local v7, "versionCode":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "/"

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 154
    .local v22, "versionedPackage":Ljava/lang/String;
    move-object/from16 v10, p6

    invoke-virtual {v1, v10, v0, v15}, Lcom/android/settings/fuelgauge/BatteryUtils;->shouldHideAnomaly(Lcom/android/settingslib/fuelgauge/PowerWhitelistBackend;ILcom/android/settings/fuelgauge/batterytip/AnomalyInfo;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 155
    const/16 v19, 0x0

    const/16 v20, 0x56b

    const/16 v21, 0x0

    iget-object v9, v15, Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;->anomalyType:Ljava/lang/Integer;

    .line 159
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 155
    move-object/from16 v18, p9

    invoke-virtual/range {v18 .. v23}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V

    goto :goto_3

    .line 161
    :cond_3
    if-eqz v16, :cond_4

    iget-boolean v9, v15, Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;->autoRestriction:Z

    if-eqz v9, :cond_4

    .line 163
    const/4 v9, 0x1

    invoke-virtual {v1, v0, v11, v9}, Lcom/android/settings/fuelgauge/BatteryUtils;->setForceAppStandby(ILjava/lang/String;I)V

    .line 165
    iget-object v9, v15, Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;->anomalyType:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v17
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v18, 0x2

    move-wide/from16 v19, v7

    .end local v7    # "versionCode":J
    .local v19, "versionCode":J
    move-object/from16 v7, p3

    move v8, v0

    move-object v9, v11

    move/from16 v10, v17

    move-object/from16 v17, v11

    .end local v11    # "packageName":Ljava/lang/String;
    .local v17, "packageName":Ljava/lang/String;
    move/from16 v11, v18

    move-object v1, v12

    move-wide v12, v5

    :try_start_1
    invoke-virtual/range {v7 .. v13}, Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;->insertAnomaly(ILjava/lang/String;IIJ)Z

    goto :goto_2

    .line 161
    .end local v17    # "packageName":Ljava/lang/String;
    .end local v19    # "versionCode":J
    .restart local v7    # "versionCode":J
    .restart local v11    # "packageName":Ljava/lang/String;
    :cond_4
    move-wide/from16 v19, v7

    move-object/from16 v17, v11

    move-object v1, v12

    .line 169
    .end local v7    # "versionCode":J
    .end local v11    # "packageName":Ljava/lang/String;
    .restart local v17    # "packageName":Ljava/lang/String;
    .restart local v19    # "versionCode":J
    iget-object v7, v15, Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;->anomalyType:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x0

    move-object/from16 v7, p3

    move v8, v0

    move-object/from16 v9, v17

    move-wide v12, v5

    invoke-virtual/range {v7 .. v13}, Lcom/android/settings/fuelgauge/batterytip/BatteryDatabaseManager;->insertAnomaly(ILjava/lang/String;IIJ)Z

    .line 173
    :goto_2
    const/16 v24, 0x0

    const/16 v25, 0x557

    const/16 v26, 0x0

    iget-object v7, v15, Lcom/android/settings/fuelgauge/batterytip/AnomalyInfo;->anomalyType:Ljava/lang/Integer;

    .line 177
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v28

    .line 173
    move-object/from16 v23, p9

    move-object/from16 v27, v22

    invoke-virtual/range {v23 .. v28}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 182
    .end local v0    # "uid":I
    .end local v16    # "autoFeatureOn":Z
    .end local v17    # "packageName":Ljava/lang/String;
    .end local v19    # "versionCode":J
    .end local v22    # "versionedPackage":Ljava/lang/String;
    :goto_3
    goto :goto_5

    .line 180
    :catch_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v1, v12

    .line 181
    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_4
    const-string v7, "Parse stats dimensions value error."

    invoke-static {v1, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 183
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :goto_5
    return-void
.end method
