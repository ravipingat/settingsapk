.class public Lcom/android/settings/fuelgauge/batterysaver/BatterySaverSchedulePreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "BatterySaverSchedulePreferenceController.java"


# static fields
.field public static final KEY_BATTERY_SAVER_SCHEDULE:Ljava/lang/String; = "battery_saver_schedule"


# instance fields
.field mBatterySaverSchedulePreference:Landroidx/preference/Preference;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 46
    const-string v0, "battery_saver_schedule"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 47
    invoke-static {p1}, Lcom/android/settingslib/fuelgauge/BatterySaverUtils;->revertScheduleToNoneIfNeeded(Landroid/content/Context;)V

    .line 48
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 57
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 58
    const-string v0, "battery_saver_schedule"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/batterysaver/BatterySaverSchedulePreferenceController;->mBatterySaverSchedulePreference:Landroidx/preference/Preference;

    .line 59
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 52
    const-string v0, "battery_saver_schedule"

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 8

    .line 63
    iget-object v0, p0, Lcom/android/settings/fuelgauge/batterysaver/BatterySaverSchedulePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 64
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    const-string v2, "automatic_power_save_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 66
    .local v2, "mode":I
    if-nez v2, :cond_1

    .line 67
    nop

    .line 68
    const-string v3, "low_power_trigger_level"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 69
    .local v3, "threshold":I
    if-gtz v3, :cond_0

    .line 70
    iget-object v1, p0, Lcom/android/settings/fuelgauge/batterysaver/BatterySaverSchedulePreferenceController;->mContext:Landroid/content/Context;

    const v4, 0x7f120263

    invoke-virtual {v1, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1

    .line 72
    :cond_0
    iget-object v4, p0, Lcom/android/settings/fuelgauge/batterysaver/BatterySaverSchedulePreferenceController;->mContext:Landroid/content/Context;

    const v5, 0x7f120265

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    .line 73
    invoke-static {v3}, Lcom/android/settings/Utils;->formatPercentage(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 72
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 76
    .end local v3    # "threshold":I
    :cond_1
    iget-object v1, p0, Lcom/android/settings/fuelgauge/batterysaver/BatterySaverSchedulePreferenceController;->mContext:Landroid/content/Context;

    const v3, 0x7f120266

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method
