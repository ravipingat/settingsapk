.class Lcom/android/settings/slices/SliceDataConverter;
.super Ljava/lang/Object;
.source "SliceDataConverter.java"


# static fields
.field private static final NODE_NAME_PREFERENCE_SCREEN:Ljava/lang/String; = "PreferenceScreen"

.field private static final TAG:Ljava/lang/String; = "SliceDataConverter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mMetricsFeatureProvider:Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    .line 86
    invoke-static {p1}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/FeatureFactory;->getMetricsFeatureProvider()Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/slices/SliceDataConverter;->mMetricsFeatureProvider:Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    .line 87
    return-void
.end method

.method private getAccessibilitySliceData()Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/slices/SliceData;",
            ">;"
        }
    .end annotation

    .line 260
    move-object/from16 v1, p0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v0

    .line 262
    .local v2, "sliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    const-class v0, Lcom/android/settings/accessibility/AccessibilitySlicePreferenceController;

    .line 263
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 264
    .local v3, "accessibilityControllerClassName":Ljava/lang/String;
    const-class v0, Lcom/android/settings/accessibility/AccessibilitySettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    .line 265
    .local v4, "fragmentClassName":Ljava/lang/String;
    iget-object v0, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    const v5, 0x7f120084

    invoke-virtual {v0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    .line 267
    .local v5, "screenTitle":Ljava/lang/CharSequence;
    new-instance v0, Lcom/android/settings/slices/SliceData$Builder;

    invoke-direct {v0}, Lcom/android/settings/slices/SliceData$Builder;-><init>()V

    .line 268
    invoke-virtual {v0, v4}, Lcom/android/settings/slices/SliceData$Builder;->setFragmentName(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v0

    .line 269
    invoke-virtual {v0, v5}, Lcom/android/settings/slices/SliceData$Builder;->setScreenTitle(Ljava/lang/CharSequence;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v0

    .line 270
    invoke-virtual {v0, v3}, Lcom/android/settings/slices/SliceData$Builder;->setPreferenceControllerClassName(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v6

    .line 272
    .local v6, "sliceDataBuilder":Lcom/android/settings/slices/SliceData$Builder;
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    move-object v7, v0

    .line 273
    .local v7, "a11yServiceNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 274
    const v8, 0x7f030057

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 273
    invoke-static {v7, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/slices/SliceDataConverter;->getAccessibilityServiceInfoList()Ljava/util/List;

    move-result-object v8

    .line 276
    .local v8, "installedServices":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    iget-object v0, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 278
    .local v9, "packageManager":Landroid/content/pm/PackageManager;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 279
    .local v11, "a11yServiceInfo":Landroid/accessibilityservice/AccessibilityServiceInfo;
    invoke-virtual {v11}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v12

    .line 280
    .local v12, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v13, v12, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 281
    .local v13, "serviceInfo":Landroid/content/pm/ServiceInfo;
    iget-object v14, v13, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 282
    .local v14, "packageName":Ljava/lang/String;
    new-instance v0, Landroid/content/ComponentName;

    iget-object v15, v13, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v14, v15}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v15, v0

    .line 283
    .local v15, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v15}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "flattenedName":Ljava/lang/String;
    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    move-object/from16 v1, p0

    goto :goto_0

    .line 289
    :cond_0
    invoke-virtual {v12, v9}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object/from16 v16, v3

    .end local v3    # "accessibilityControllerClassName":Ljava/lang/String;
    .local v16, "accessibilityControllerClassName":Ljava/lang/String;
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 290
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {v12}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    move-result v0

    .line 291
    .local v0, "iconResource":I
    if-nez v0, :cond_1

    .line 292
    const v0, 0x7f08018e

    move-object/from16 v17, v4

    move v4, v0

    goto :goto_1

    .line 291
    :cond_1
    move-object/from16 v17, v4

    move v4, v0

    .line 295
    .end local v0    # "iconResource":I
    .local v4, "iconResource":I
    .local v17, "fragmentClassName":Ljava/lang/String;
    :goto_1
    invoke-virtual {v6, v1}, Lcom/android/settings/slices/SliceData$Builder;->setKey(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v0

    .line 296
    invoke-virtual {v0, v3}, Lcom/android/settings/slices/SliceData$Builder;->setTitle(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v0

    .line 297
    invoke-virtual {v0, v4}, Lcom/android/settings/slices/SliceData$Builder;->setIcon(I)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v0

    move-object/from16 v18, v3

    .end local v3    # "title":Ljava/lang/String;
    .local v18, "title":Ljava/lang/String;
    const/4 v3, 0x1

    .line 298
    invoke-virtual {v0, v3}, Lcom/android/settings/slices/SliceData$Builder;->setSliceType(I)Lcom/android/settings/slices/SliceData$Builder;

    .line 300
    :try_start_0
    invoke-virtual {v6}, Lcom/android/settings/slices/SliceData$Builder;->build()Lcom/android/settings/slices/SliceData;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/settings/slices/SliceData$InvalidSliceDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    goto :goto_2

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Lcom/android/settings/slices/SliceData$InvalidSliceDataException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v19, v4

    .end local v4    # "iconResource":I
    .local v19, "iconResource":I
    const-string v4, "Invalid data when building a11y SliceData for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "SliceDataConverter"

    invoke-static {v4, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 304
    .end local v0    # "e":Lcom/android/settings/slices/SliceData$InvalidSliceDataException;
    .end local v1    # "flattenedName":Ljava/lang/String;
    .end local v11    # "a11yServiceInfo":Landroid/accessibilityservice/AccessibilityServiceInfo;
    .end local v12    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v13    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    .end local v14    # "packageName":Ljava/lang/String;
    .end local v15    # "componentName":Landroid/content/ComponentName;
    .end local v18    # "title":Ljava/lang/String;
    .end local v19    # "iconResource":I
    :goto_2
    move-object/from16 v1, p0

    move-object/from16 v3, v16

    move-object/from16 v4, v17

    goto :goto_0

    .line 306
    .end local v16    # "accessibilityControllerClassName":Ljava/lang/String;
    .end local v17    # "fragmentClassName":Ljava/lang/String;
    .local v3, "accessibilityControllerClassName":Ljava/lang/String;
    .local v4, "fragmentClassName":Ljava/lang/String;
    :cond_2
    return-object v2
.end method

.method private getSliceDataFromProvider(Lcom/android/settings/search/Indexable$SearchIndexProvider;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "provider"    # Lcom/android/settings/search/Indexable$SearchIndexProvider;
    .param p2, "fragmentName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/settings/search/Indexable$SearchIndexProvider;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/slices/SliceData;",
            ">;"
        }
    .end annotation

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v0, "sliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    iget-object v1, p0, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    .line 132
    const/4 v2, 0x1

    invoke-interface {p1, v1, v2}, Lcom/android/settings/search/Indexable$SearchIndexProvider;->getXmlResourcesToIndex(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v1

    .line 134
    .local v1, "resList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/SearchIndexableResource;>;"
    if-nez v1, :cond_0

    .line 135
    return-object v0

    .line 140
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/SearchIndexableResource;

    .line 141
    .local v3, "resource":Landroid/provider/SearchIndexableResource;
    iget v4, v3, Landroid/provider/SearchIndexableResource;->xmlResId:I

    .line 142
    .local v4, "xmlResId":I
    if-nez v4, :cond_1

    .line 143
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " provides invalid XML (0) in search provider."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "SliceDataConverter"

    invoke-static {v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    goto :goto_0

    .line 147
    :cond_1
    invoke-direct {p0, v4, p2}, Lcom/android/settings/slices/SliceDataConverter;->getSliceDataFromXML(ILjava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 148
    .local v5, "xmlSliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 149
    .end local v3    # "resource":Landroid/provider/SearchIndexableResource;
    .end local v4    # "xmlResId":I
    .end local v5    # "xmlSliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    goto :goto_0

    .line 151
    :cond_2
    return-object v0
.end method

.method private getSliceDataFromXML(ILjava/lang/String;)Ljava/util/List;
    .locals 23
    .param p1, "xmlResId"    # I
    .param p2, "fragmentName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/slices/SliceData;",
            ">;"
        }
    .end annotation

    .line 155
    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v9, p2

    const-string v3, "SliceDataConverter"

    const/4 v4, 0x0

    .line 157
    .local v4, "parser":Landroid/content/res/XmlResourceParser;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v10, v0

    .line 158
    .local v10, "xmlSliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    const-string v5, ""

    .line 161
    .local v5, "controllerClassName":Ljava/lang/String;
    :try_start_0
    iget-object v0, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    move-object v4, v0

    .line 164
    :goto_0
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    move v6, v0

    .local v6, "type":I
    const/4 v7, 0x1

    if-eq v0, v7, :cond_0

    const/4 v0, 0x2

    if-eq v6, v0, :cond_0

    goto :goto_0

    .line 169
    :cond_0
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "nodeName":Ljava/lang/String;
    const-string v7, "PreferenceScreen"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 176
    invoke-static {v4}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v7

    .line 177
    .local v7, "attrs":Landroid/util/AttributeSet;
    iget-object v8, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    invoke-static {v8, v7}, Lcom/android/settings/core/PreferenceXmlParserUtils;->getDataTitle(Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;

    move-result-object v8

    .line 182
    .local v8, "screenTitle":Ljava/lang/String;
    iget-object v11, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    const/16 v12, 0x8fe

    invoke-static {v11, v2, v12}, Lcom/android/settings/core/PreferenceXmlParserUtils;->extractMetadata(Landroid/content/Context;II)Ljava/util/List;

    move-result-object v11

    .line 193
    .local v11, "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/Bundle;

    .line 196
    .local v13, "bundle":Landroid/os/Bundle;
    const-string v14, "controller"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object v5, v14

    .line 197
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 198
    move/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v11

    move-object/from16 v19, v12

    goto/16 :goto_2

    .line 201
    :cond_1
    const-string v14, "key"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 202
    .local v14, "key":Ljava/lang/String;
    const-string v15, "title"

    invoke-virtual {v13, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 203
    .local v15, "title":Ljava/lang/String;
    const-string v2, "summary"

    invoke-virtual {v13, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 204
    .local v2, "summary":Ljava/lang/String;
    move/from16 v16, v6

    .end local v6    # "type":I
    .local v16, "type":I
    const-string v6, "icon"

    invoke-virtual {v13, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 205
    .local v6, "iconResId":I
    move-object/from16 v17, v7

    .end local v7    # "attrs":Landroid/util/AttributeSet;
    .local v17, "attrs":Landroid/util/AttributeSet;
    iget-object v7, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    invoke-static {v7, v5, v14}, Lcom/android/settings/slices/SliceBuilderUtils;->getSliceType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 207
    .local v7, "sliceType":I
    move-object/from16 v18, v11

    .end local v11    # "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .local v18, "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const-string v11, "platform_slice"

    invoke-virtual {v13, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    .line 208
    .local v11, "isPlatformSlice":Z
    move-object/from16 v19, v12

    const-string v12, "unavailable_slice_subtitle"

    invoke-virtual {v13, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 211
    .local v12, "unavailableSliceSubtitle":Ljava/lang/String;
    move-object/from16 v20, v13

    .end local v13    # "bundle":Landroid/os/Bundle;
    .local v20, "bundle":Landroid/os/Bundle;
    new-instance v13, Lcom/android/settings/slices/SliceData$Builder;

    invoke-direct {v13}, Lcom/android/settings/slices/SliceData$Builder;-><init>()V

    .line 212
    invoke-virtual {v13, v14}, Lcom/android/settings/slices/SliceData$Builder;->setKey(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 213
    invoke-virtual {v13, v15}, Lcom/android/settings/slices/SliceData$Builder;->setTitle(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 214
    invoke-virtual {v13, v2}, Lcom/android/settings/slices/SliceData$Builder;->setSummary(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 215
    invoke-virtual {v13, v6}, Lcom/android/settings/slices/SliceData$Builder;->setIcon(I)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 216
    invoke-virtual {v13, v8}, Lcom/android/settings/slices/SliceData$Builder;->setScreenTitle(Ljava/lang/CharSequence;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 217
    invoke-virtual {v13, v5}, Lcom/android/settings/slices/SliceData$Builder;->setPreferenceControllerClassName(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 218
    invoke-virtual {v13, v9}, Lcom/android/settings/slices/SliceData$Builder;->setFragmentName(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 219
    invoke-virtual {v13, v7}, Lcom/android/settings/slices/SliceData$Builder;->setSliceType(I)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 220
    invoke-virtual {v13, v11}, Lcom/android/settings/slices/SliceData$Builder;->setPlatformDefined(Z)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 221
    invoke-virtual {v13, v12}, Lcom/android/settings/slices/SliceData$Builder;->setUnavailableSliceSubtitle(Ljava/lang/String;)Lcom/android/settings/slices/SliceData$Builder;

    move-result-object v13

    .line 222
    invoke-virtual {v13}, Lcom/android/settings/slices/SliceData$Builder;->build()Lcom/android/settings/slices/SliceData;

    move-result-object v13

    .line 224
    .local v13, "xmlSlice":Lcom/android/settings/slices/SliceData;
    move-object/from16 v21, v2

    .end local v2    # "summary":Ljava/lang/String;
    .local v21, "summary":Ljava/lang/String;
    iget-object v2, v1, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    .line 225
    invoke-static {v2, v13}, Lcom/android/settings/slices/SliceBuilderUtils;->getPreferenceController(Landroid/content/Context;Lcom/android/settings/slices/SliceData;)Lcom/android/settings/core/BasePreferenceController;

    move-result-object v2

    .line 228
    .local v2, "controller":Lcom/android/settings/core/BasePreferenceController;
    invoke-virtual {v2}, Lcom/android/settings/core/BasePreferenceController;->isSliceable()Z

    move-result v22

    if-eqz v22, :cond_2

    invoke-virtual {v2}, Lcom/android/settings/core/BasePreferenceController;->isAvailable()Z

    move-result v22

    if-eqz v22, :cond_2

    .line 229
    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/settings/slices/SliceData$InvalidSliceDataException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    .end local v2    # "controller":Lcom/android/settings/core/BasePreferenceController;
    .end local v6    # "iconResId":I
    .end local v7    # "sliceType":I
    .end local v11    # "isPlatformSlice":Z
    .end local v12    # "unavailableSliceSubtitle":Ljava/lang/String;
    .end local v13    # "xmlSlice":Lcom/android/settings/slices/SliceData;
    .end local v14    # "key":Ljava/lang/String;
    .end local v15    # "title":Ljava/lang/String;
    .end local v20    # "bundle":Landroid/os/Bundle;
    .end local v21    # "summary":Ljava/lang/String;
    :cond_2
    nop

    .line 193
    .end local v16    # "type":I
    .end local v17    # "attrs":Landroid/util/AttributeSet;
    .end local v18    # "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .local v6, "type":I
    .local v7, "attrs":Landroid/util/AttributeSet;
    .local v11, "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :goto_2
    move/from16 v2, p1

    move/from16 v6, v16

    move-object/from16 v7, v17

    move-object/from16 v11, v18

    move-object/from16 v12, v19

    .end local v6    # "type":I
    .end local v7    # "attrs":Landroid/util/AttributeSet;
    .end local v11    # "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local v16    # "type":I
    .restart local v17    # "attrs":Landroid/util/AttributeSet;
    .restart local v18    # "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    goto/16 :goto_1

    .end local v16    # "type":I
    .end local v17    # "attrs":Landroid/util/AttributeSet;
    .end local v18    # "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local v6    # "type":I
    .restart local v7    # "attrs":Landroid/util/AttributeSet;
    .restart local v11    # "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_3
    move/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v11

    .line 254
    .end local v0    # "nodeName":Ljava/lang/String;
    .end local v6    # "type":I
    .end local v7    # "attrs":Landroid/util/AttributeSet;
    .end local v8    # "screenTitle":Ljava/lang/String;
    .end local v11    # "metadata":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    nop

    :goto_3
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    goto/16 :goto_4

    .line 171
    .restart local v0    # "nodeName":Ljava/lang/String;
    .restart local v6    # "type":I
    :cond_4
    move/from16 v16, v6

    .end local v6    # "type":I
    .restart local v16    # "type":I
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "XML document must start with <PreferenceScreen> tag; found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .end local v4    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v5    # "controllerClassName":Ljava/lang/String;
    .end local v10    # "xmlSliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    .end local p0    # "this":Lcom/android/settings/slices/SliceDataConverter;
    .end local p1    # "xmlResId":I
    .end local p2    # "fragmentName":Ljava/lang/String;
    throw v2
    :try_end_1
    .catch Lcom/android/settings/slices/SliceData$InvalidSliceDataException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254
    .end local v0    # "nodeName":Ljava/lang/String;
    .end local v16    # "type":I
    .restart local v4    # "parser":Landroid/content/res/XmlResourceParser;
    .restart local v5    # "controllerClassName":Ljava/lang/String;
    .restart local v10    # "xmlSliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    .restart local p0    # "this":Lcom/android/settings/slices/SliceDataConverter;
    .restart local p1    # "xmlResId":I
    .restart local p2    # "fragmentName":Ljava/lang/String;
    :catchall_0
    move-exception v0

    goto/16 :goto_5

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "Get slice data from XML failed "

    invoke-static {v3, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 248
    iget-object v11, v1, Lcom/android/settings/slices/SliceDataConverter;->mMetricsFeatureProvider:Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    const/4 v12, 0x0

    const/16 v13, 0x6bf

    const/4 v14, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v11 .. v16}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 254
    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v4, :cond_6

    goto :goto_3

    .line 239
    :catch_1
    move-exception v0

    move-object v2, v4

    move-object v11, v5

    .line 240
    .end local v4    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v5    # "controllerClassName":Ljava/lang/String;
    .restart local v0    # "e":Ljava/lang/Exception;
    .local v2, "parser":Landroid/content/res/XmlResourceParser;
    .local v11, "controllerClassName":Ljava/lang/String;
    :try_start_3
    const-string v4, "Error parsing PreferenceScreen: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 241
    iget-object v3, v1, Lcom/android/settings/slices/SliceDataConverter;->mMetricsFeatureProvider:Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    const/4 v4, 0x0

    const/16 v5, 0x6be

    const/4 v6, 0x0

    const/4 v8, 0x1

    move-object/from16 v7, p2

    invoke-virtual/range {v3 .. v8}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 254
    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_5

    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->close()V

    .line 256
    :cond_5
    move-object v4, v2

    move-object v5, v11

    goto :goto_4

    .line 254
    :catchall_1
    move-exception v0

    move-object v4, v2

    move-object v5, v11

    goto :goto_5

    .line 232
    .end local v2    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v11    # "controllerClassName":Ljava/lang/String;
    .restart local v4    # "parser":Landroid/content/res/XmlResourceParser;
    .restart local v5    # "controllerClassName":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 233
    .local v0, "e":Lcom/android/settings/slices/SliceData$InvalidSliceDataException;
    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid data when building SliceData for "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 234
    iget-object v11, v1, Lcom/android/settings/slices/SliceDataConverter;->mMetricsFeatureProvider:Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    const/4 v12, 0x0

    const/16 v13, 0x6bd

    const/4 v14, 0x0

    const/16 v16, 0x1

    move-object v15, v5

    invoke-virtual/range {v11 .. v16}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 254
    .end local v0    # "e":Lcom/android/settings/slices/SliceData$InvalidSliceDataException;
    if-eqz v4, :cond_6

    goto/16 :goto_3

    .line 256
    :cond_6
    :goto_4
    return-object v10

    .line 254
    :goto_5
    if-eqz v4, :cond_7

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_7
    throw v0
.end method


# virtual methods
.method getAccessibilityServiceInfoList()Ljava/util/List;
    .locals 2
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    .line 311
    iget-object v0, p0, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    .line 313
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getSliceData()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/slices/SliceData;",
            ">;"
        }
    .end annotation

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v0, "sliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    iget-object v1, p0, Lcom/android/settings/slices/SliceDataConverter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v1

    .line 103
    invoke-virtual {v1}, Lcom/android/settings/overlay/FeatureFactory;->getSearchFeatureProvider()Lcom/android/settings/search/SearchFeatureProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/settings/search/SearchFeatureProvider;->getSearchIndexableResources()Lcom/android/settingslib/search/SearchIndexableResources;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/settingslib/search/SearchIndexableResources;->getProviderValues()Ljava/util/Collection;

    move-result-object v1

    .line 105
    .local v1, "indexableClasses":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Class;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    .line 106
    .local v3, "clazz":Ljava/lang/Class;
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "fragmentName":Ljava/lang/String;
    invoke-static {v3}, Lcom/android/settings/search/DatabaseIndexingUtils;->getSearchIndexProvider(Ljava/lang/Class;)Lcom/android/settings/search/Indexable$SearchIndexProvider;

    move-result-object v5

    .line 112
    .local v5, "provider":Lcom/android/settings/search/Indexable$SearchIndexProvider;
    if-nez v5, :cond_0

    .line 113
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " dose not implement Search Index Provider"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "SliceDataConverter"

    invoke-static {v7, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    goto :goto_0

    .line 117
    :cond_0
    invoke-direct {p0, v5, v4}, Lcom/android/settings/slices/SliceDataConverter;->getSliceDataFromProvider(Lcom/android/settings/search/Indexable$SearchIndexProvider;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 119
    .local v6, "providerSliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 120
    .end local v3    # "clazz":Ljava/lang/Class;
    .end local v4    # "fragmentName":Ljava/lang/String;
    .end local v5    # "provider":Lcom/android/settings/search/Indexable$SearchIndexProvider;
    .end local v6    # "providerSliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    goto :goto_0

    .line 122
    :cond_1
    invoke-direct {p0}, Lcom/android/settings/slices/SliceDataConverter;->getAccessibilitySliceData()Ljava/util/List;

    move-result-object v2

    .line 123
    .local v2, "a11ySliceData":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/slices/SliceData;>;"
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 124
    return-object v0
.end method
