.class public Lcom/android/settings/datausage/ChartDataUsagePreference;
.super Landroidx/preference/Preference;
.source "ChartDataUsagePreference.java"


# static fields
.field private static final RESOLUTION:J = 0x80000L


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEnd:J

.field private final mLimitColor:I

.field private mNetworkCycleChartData:Lcom/android/settingslib/net/NetworkCycleChartData;

.field private mPolicy:Landroid/net/NetworkPolicy;

.field private mSecondaryColor:I

.field private mSeriesColor:I

.field private mShowWifi:Z

.field private mStart:J

.field private mSubId:I

.field private final mWarningColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 71
    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSubId:I

    .line 61
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mShowWifi:Z

    .line 73
    iput-object p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mContext:Landroid/content/Context;

    .line 75
    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->setSelectable(Z)V

    .line 76
    const v0, 0x1010543

    invoke-static {p1, v0}, Lcom/android/settings/Utils;->getColorAttrDefaultColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mLimitColor:I

    .line 77
    const v0, 0x1010038

    invoke-static {p1, v0}, Lcom/android/settings/Utils;->getColorAttrDefaultColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mWarningColor:I

    .line 78
    const v0, 0x7f0d007b

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->setLayoutResource(I)V

    .line 79
    return-void
.end method

.method private bindNetworkPolicy(Lcom/android/settings/widget/UsageView;Landroid/net/NetworkPolicy;I)V
    .locals 10
    .param p1, "chart"    # Lcom/android/settings/widget/UsageView;
    .param p2, "policy"    # Landroid/net/NetworkPolicy;
    .param p3, "top"    # I

    .line 145
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    .line 146
    .local v0, "labels":[Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .line 147
    .local v1, "middleVisibility":I
    const/4 v2, 0x0

    .line 148
    .local v2, "topVisibility":I
    if-nez p2, :cond_0

    .line 149
    return-void

    .line 152
    :cond_0
    iget-wide v3, p2, Landroid/net/NetworkPolicy;->limitBytes:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    .line 153
    iget v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mLimitColor:I

    .line 154
    const/4 v3, 0x2

    iget-wide v7, p2, Landroid/net/NetworkPolicy;->limitBytes:J

    const v4, 0x7f12055f

    iget v9, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mLimitColor:I

    invoke-direct {p0, v7, v8, v4, v9}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getLabel(JII)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v0, v3

    .line 157
    :cond_1
    iget-wide v3, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    .line 158
    iget-wide v3, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    const-wide/32 v5, 0x80000

    div-long/2addr v3, v5

    long-to-int v3, v3

    invoke-virtual {p1, v3}, Lcom/android/settings/widget/UsageView;->setDividerLoc(I)V

    .line 159
    iget-wide v3, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    div-long/2addr v3, v5

    long-to-float v3, v3

    int-to-float v4, p3

    div-float/2addr v3, v4

    .line 160
    .local v3, "weight":F
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v3

    .line 161
    .local v4, "above":F
    invoke-virtual {p1, v4, v3}, Lcom/android/settings/widget/UsageView;->setSideLabelWeights(FF)V

    .line 162
    iget v1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mWarningColor:I

    .line 163
    const/4 v5, 0x1

    iget-wide v6, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    const v8, 0x7f120560

    iget v9, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mWarningColor:I

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getLabel(JII)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v0, v5

    .line 167
    .end local v3    # "weight":F
    .end local v4    # "above":F
    :cond_2
    invoke-virtual {p1, v0}, Lcom/android/settings/widget/UsageView;->setSideLabels([Ljava/lang/CharSequence;)V

    .line 168
    invoke-virtual {p1, v1, v2}, Lcom/android/settings/widget/UsageView;->setDividerColors(II)V

    .line 169
    return-void
.end method

.method private getLabel(JII)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "bytes"    # J
    .param p3, "str"    # I
    .param p4, "mLimitColor"    # I

    .line 173
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/16 v1, 0x9

    invoke-static {v0, p1, p2, v1}, Landroid/text/format/Formatter;->formatBytes(Landroid/content/res/Resources;JI)Landroid/text/format/Formatter$BytesResult;

    move-result-object v0

    .line 176
    .local v0, "result":Landroid/text/format/Formatter$BytesResult;
    iget-object v1, v0, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    .line 177
    .local v1, "unit":Ljava/lang/String;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-le v2, v3, :cond_0

    .line 178
    iget-object v2, v0, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    invoke-static {v2}, Lcom/oneplus/settings/utils/OPUtils;->replaceFileSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    iget-object v4, v0, Landroid/text/format/Formatter$BytesResult;->value:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 182
    .local v2, "label":Ljava/lang/CharSequence;
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, p4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v2, v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    return-object v3
.end method

.method private toInt(J)I
    .locals 2
    .param p1, "l"    # J

    .line 141
    const-wide/32 v0, 0xea60

    div-long v0, p1, v0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method calcPoints(Lcom/android/settings/widget/UsageView;Ljava/util/List;)V
    .locals 17
    .param p1, "chart"    # Lcom/android/settings/widget/UsageView;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/settings/widget/UsageView;",
            "Ljava/util/List<",
            "Lcom/android/settingslib/net/NetworkCycleData;",
            ">;)V"
        }
    .end annotation

    .line 110
    .local p2, "usageSummary":Ljava/util/List;, "Ljava/util/List<Lcom/android/settingslib/net/NetworkCycleData;>;"
    move-object/from16 v0, p0

    if-nez p2, :cond_0

    .line 111
    return-void

    .line 113
    :cond_0
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    .line 114
    .local v1, "points":Landroid/util/SparseIntArray;
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 116
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 117
    .local v2, "now":J
    const-wide/16 v4, 0x0

    .line 118
    .local v4, "totalData":J
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x1

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/settingslib/net/NetworkCycleData;

    .line 119
    .local v7, "data":Lcom/android/settingslib/net/NetworkCycleData;
    invoke-virtual {v7}, Lcom/android/settingslib/net/NetworkCycleData;->getStartTime()J

    move-result-wide v9

    .line 120
    .local v9, "startTime":J
    cmp-long v11, v9, v2

    if-lez v11, :cond_1

    .line 121
    move-wide v15, v2

    goto :goto_1

    .line 123
    :cond_1
    invoke-virtual {v7}, Lcom/android/settingslib/net/NetworkCycleData;->getEndTime()J

    move-result-wide v11

    .line 126
    .local v11, "endTime":J
    invoke-virtual {v7}, Lcom/android/settingslib/net/NetworkCycleData;->getTotalUsage()J

    move-result-wide v13

    add-long/2addr v4, v13

    .line 128
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v13

    if-ne v13, v8, :cond_2

    .line 129
    iget-wide v13, v0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mStart:J

    sub-long v13, v9, v13

    invoke-direct {v0, v13, v14}, Lcom/android/settings/datausage/ChartDataUsagePreference;->toInt(J)I

    move-result v13

    sub-int/2addr v13, v8

    const/4 v8, -0x1

    invoke-virtual {v1, v13, v8}, Landroid/util/SparseIntArray;->put(II)V

    .line 131
    :cond_2
    iget-wide v13, v0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mStart:J

    sub-long v13, v9, v13

    const-wide/16 v15, 0x1

    add-long/2addr v13, v15

    invoke-direct {v0, v13, v14}, Lcom/android/settings/datausage/ChartDataUsagePreference;->toInt(J)I

    move-result v8

    const-wide/32 v13, 0x80000

    move-wide v15, v2

    .end local v2    # "now":J
    .local v15, "now":J
    div-long v2, v4, v13

    long-to-int v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 132
    iget-wide v2, v0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mStart:J

    sub-long v2, v11, v2

    invoke-direct {v0, v2, v3}, Lcom/android/settings/datausage/ChartDataUsagePreference;->toInt(J)I

    move-result v2

    div-long v13, v4, v13

    long-to-int v3, v13

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 133
    .end local v7    # "data":Lcom/android/settingslib/net/NetworkCycleData;
    .end local v9    # "startTime":J
    .end local v11    # "endTime":J
    move-wide v2, v15

    goto :goto_0

    .line 118
    .end local v15    # "now":J
    .restart local v2    # "now":J
    :cond_3
    move-wide v15, v2

    .line 134
    .end local v2    # "now":J
    .restart local v15    # "now":J
    :goto_1
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-le v2, v8, :cond_4

    .line 135
    move-object/from16 v2, p1

    invoke-virtual {v2, v1}, Lcom/android/settings/widget/UsageView;->addPath(Landroid/util/SparseIntArray;)V

    goto :goto_2

    .line 134
    :cond_4
    move-object/from16 v2, p1

    .line 137
    :goto_2
    return-void
.end method

.method public getInspectEnd()J
    .locals 2

    .line 213
    iget-wide v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mEnd:J

    return-wide v0
.end method

.method public getInspectStart()J
    .locals 2

    .line 209
    iget-wide v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mStart:J

    return-wide v0
.end method

.method public getTop()I
    .locals 8

    .line 102
    iget-object v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mNetworkCycleChartData:Lcom/android/settingslib/net/NetworkCycleChartData;

    invoke-virtual {v0}, Lcom/android/settingslib/net/NetworkCycleChartData;->getTotalUsage()J

    move-result-wide v0

    .line 104
    .local v0, "totalData":J
    iget-object v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    if-eqz v2, :cond_0

    iget-wide v2, v2, Landroid/net/NetworkPolicy;->limitBytes:J

    iget-object v4, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    iget-wide v4, v4, Landroid/net/NetworkPolicy;->warningBytes:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    .line 105
    .local v2, "policyMax":J
    :goto_0
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    const-wide/32 v6, 0x80000

    div-long/2addr v4, v6

    long-to-int v4, v4

    return v4
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 7
    .param p1, "holder"    # Landroidx/preference/PreferenceViewHolder;

    .line 83
    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 84
    const v0, 0x7f0a01c1

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/UsageView;

    .line 85
    .local v0, "chart":Lcom/android/settings/widget/UsageView;
    iget-object v1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mNetworkCycleChartData:Lcom/android/settingslib/net/NetworkCycleChartData;

    if-nez v1, :cond_0

    .line 86
    return-void

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getTop()I

    move-result v1

    .line 90
    .local v1, "top":I
    invoke-virtual {v0}, Lcom/android/settings/widget/UsageView;->clearPaths()V

    .line 91
    iget-wide v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mEnd:J

    iget-wide v4, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mStart:J

    sub-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/android/settings/datausage/ChartDataUsagePreference;->toInt(J)I

    move-result v2

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/widget/UsageView;->configureGraph(II)V

    .line 92
    iget-object v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mNetworkCycleChartData:Lcom/android/settingslib/net/NetworkCycleChartData;

    invoke-virtual {v2}, Lcom/android/settingslib/net/NetworkCycleChartData;->getUsageBuckets()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/datausage/ChartDataUsagePreference;->calcPoints(Lcom/android/settings/widget/UsageView;Ljava/util/List;)V

    .line 93
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mStart:J

    invoke-static {v4, v5, v6, v5, v6}, Lcom/android/settings/Utils;->formatDateRange(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 95
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mEnd:J

    invoke-static {v4, v5, v6, v5, v6}, Lcom/android/settings/Utils;->formatDateRange(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 93
    invoke-virtual {v0, v2}, Lcom/android/settings/widget/UsageView;->setBottomLabels([Ljava/lang/CharSequence;)V

    .line 98
    iget-object v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    invoke-direct {p0, v0, v2, v1}, Lcom/android/settings/datausage/ChartDataUsagePreference;->bindNetworkPolicy(Lcom/android/settings/widget/UsageView;Landroid/net/NetworkPolicy;I)V

    .line 99
    return-void
.end method

.method public setColors(II)V
    .locals 0
    .param p1, "seriesColor"    # I
    .param p2, "secondaryColor"    # I

    .line 236
    iput p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSeriesColor:I

    .line 237
    iput p2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSecondaryColor:I

    .line 238
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->notifyChanged()V

    .line 239
    return-void
.end method

.method public setNetworkCycleData(Lcom/android/settingslib/net/NetworkCycleChartData;)V
    .locals 5
    .param p1, "data"    # Lcom/android/settingslib/net/NetworkCycleChartData;

    .line 217
    iput-object p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mNetworkCycleChartData:Lcom/android/settingslib/net/NetworkCycleChartData;

    .line 222
    iget-object v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSubId:I

    invoke-static {v0, v1}, Lcom/android/settings/datausage/OPDataUsageUtils;->getDataUsageSectionTimeMillByAccountDay(Landroid/content/Context;I)[J

    move-result-object v0

    .line 223
    .local v0, "section":[J
    const/4 v1, 0x0

    aget-wide v1, v0, v1

    const/4 v3, 0x1

    aget-wide v3, v0, v3

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/settings/datausage/ChartDataUsagePreference;->setVisibleRange(JJ)V

    .line 225
    return-void
.end method

.method public setNetworkPolicy(Landroid/net/NetworkPolicy;)V
    .locals 9
    .param p1, "policy"    # Landroid/net/NetworkPolicy;

    .line 186
    iput-object p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    .line 188
    iget-object v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    if-eqz v0, :cond_2

    .line 189
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSubId:I

    invoke-static {v0, v1}, Lcom/android/settings/datausage/OPDataUsageUtils;->getDataWarnState(Landroid/content/Context;I)I

    move-result v0

    .line 190
    .local v0, "warnState":I
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSubId:I

    invoke-static {v1, v2}, Lcom/android/settings/datausage/OPDataUsageUtils;->getDataWarnBytes(Landroid/content/Context;I)J

    move-result-wide v1

    .line 191
    .local v1, "warnBytes":J
    const-wide/16 v3, -0x1

    const/4 v5, 0x1

    if-ne v0, v5, :cond_0

    .line 192
    iget-object v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    iput-wide v1, v6, Landroid/net/NetworkPolicy;->warningBytes:J

    goto :goto_0

    .line 194
    :cond_0
    iget-object v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    iput-wide v3, v6, Landroid/net/NetworkPolicy;->warningBytes:J

    .line 196
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v6

    iget v7, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSubId:I

    invoke-static {v6, v7}, Lcom/android/settings/datausage/OPDataUsageUtils;->getDataLimitState(Landroid/content/Context;I)I

    move-result v6

    .line 197
    .local v6, "limitState":I
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v7

    iget v8, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSubId:I

    invoke-static {v7, v8}, Lcom/android/settings/datausage/OPDataUsageUtils;->getDataLimitBytes(Landroid/content/Context;I)J

    move-result-wide v7

    .line 198
    .local v7, "limitBytes":J
    if-ne v6, v5, :cond_1

    .line 199
    iget-object v3, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    iput-wide v7, v3, Landroid/net/NetworkPolicy;->limitBytes:J

    goto :goto_1

    .line 201
    :cond_1
    iget-object v5, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mPolicy:Landroid/net/NetworkPolicy;

    iput-wide v3, v5, Landroid/net/NetworkPolicy;->limitBytes:J

    .line 205
    .end local v0    # "warnState":I
    .end local v1    # "warnBytes":J
    .end local v6    # "limitState":I
    .end local v7    # "limitBytes":J
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->notifyChanged()V

    .line 206
    return-void
.end method

.method public setShowWifi(Z)V
    .locals 0
    .param p1, "showWifi"    # Z

    .line 66
    iput-boolean p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mShowWifi:Z

    .line 67
    return-void
.end method

.method public setSubId(I)V
    .locals 0
    .param p1, "mSubId"    # I

    .line 63
    iput p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mSubId:I

    .line 64
    return-void
.end method

.method public setVisibleRange(JJ)V
    .locals 0
    .param p1, "start"    # J
    .param p3, "end"    # J

    .line 229
    iput-wide p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mStart:J

    .line 230
    iput-wide p3, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->mEnd:J

    .line 231
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->notifyChanged()V

    .line 232
    return-void
.end method
