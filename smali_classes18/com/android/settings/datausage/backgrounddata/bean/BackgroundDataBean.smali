.class public Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;
.super Ljava/lang/Object;
.source "BackgroundDataBean.java"


# instance fields
.field private id:I

.field private package_name:Ljava/lang/String;

.field private type:I

.field private uid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;II)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "package_name"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "type"    # I

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->id:I

    .line 22
    iput-object p2, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->package_name:Ljava/lang/String;

    .line 23
    iput p3, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->uid:I

    .line 24
    iput p4, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->type:I

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "package_name"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "type"    # I

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->package_name:Ljava/lang/String;

    .line 29
    iput p2, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->uid:I

    .line 30
    iput p3, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->type:I

    .line 31
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 34
    iget v0, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->id:I

    return v0
.end method

.method public getPackage_name()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->package_name:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .line 58
    iget v0, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->type:I

    return v0
.end method

.method public getUid()I
    .locals 1

    .line 50
    iget v0, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->uid:I

    return v0
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .line 38
    iput p1, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->id:I

    .line 39
    return-void
.end method

.method public setPackage_name(Ljava/lang/String;)V
    .locals 0
    .param p1, "package_name"    # Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->package_name:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .line 62
    iput p1, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->type:I

    .line 63
    return-void
.end method

.method public setUid(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 54
    iput p1, p0, Lcom/android/settings/datausage/backgrounddata/bean/BackgroundDataBean;->uid:I

    .line 55
    return-void
.end method
