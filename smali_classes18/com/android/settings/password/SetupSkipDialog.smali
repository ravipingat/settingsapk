.class public Lcom/android/settings/password/SetupSkipDialog;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;
.source "SetupSkipDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final ARG_FRP_SUPPORTED:Ljava/lang/String; = "frp_supported"

.field private static final ARG_LOCK_TYPE_ALPHANUMERIC:Ljava/lang/String; = "lock_type_alphanumeric"

.field private static final ARG_LOCK_TYPE_PATTERN:Ljava/lang/String; = "lock_type_pattern"

.field public static final EXTRA_FRP_SUPPORTED:Ljava/lang/String; = ":settings:frp_supported"

.field public static final RESULT_SKIP:I = 0xb

.field private static final TAG_SKIP_DIALOG:Ljava/lang/String; = "skip_dialog"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(ZZZZZ)Lcom/android/settings/password/SetupSkipDialog;
    .locals 3
    .param p0, "isFrpSupported"    # Z
    .param p1, "isPatternMode"    # Z
    .param p2, "isAlphanumericMode"    # Z
    .param p3, "isFingerprintSupported"    # Z
    .param p4, "isFaceSupported"    # Z

    .line 49
    new-instance v0, Lcom/android/settings/password/SetupSkipDialog;

    invoke-direct {v0}, Lcom/android/settings/password/SetupSkipDialog;-><init>()V

    .line 50
    .local v0, "dialog":Lcom/android/settings/password/SetupSkipDialog;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v1, "args":Landroid/os/Bundle;
    const-string v2, "frp_supported"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 52
    const-string v2, "lock_type_pattern"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 53
    const-string v2, "lock_type_alphanumeric"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 54
    const-string v2, "for_fingerprint"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 55
    const-string v2, "for_face"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    invoke-virtual {v0, v1}, Lcom/android/settings/password/SetupSkipDialog;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 62
    const/16 v0, 0x23d

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "button"    # I

    .line 114
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/password/SetupSkipDialog;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    .line 117
    .local v0, "activity":Landroid/app/Activity;
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 118
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 121
    .end local v0    # "activity":Landroid/app/Activity;
    :goto_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 67
    invoke-virtual {p0}, Lcom/android/settings/password/SetupSkipDialog;->onCreateDialogBuilder()Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->create()Lcom/oneplus/lib/app/OPAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateDialogBuilder()Lcom/oneplus/lib/app/OPAlertDialog$Builder;
    .locals 7
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 74
    invoke-virtual {p0}, Lcom/android/settings/password/SetupSkipDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 75
    .local v0, "args":Landroid/os/Bundle;
    nop

    .line 76
    const-string v1, "for_face"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 77
    .local v1, "isFaceSupported":Z
    nop

    .line 78
    const-string v2, "for_fingerprint"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 79
    .local v2, "isFingerprintSupported":Z
    const/4 v3, 0x1

    if-nez v1, :cond_2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 100
    :cond_0
    new-instance v4, Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/password/SetupSkipDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 101
    invoke-virtual {v4, v3}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setOnlyLightTheme(Z)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f1213db

    .line 103
    invoke-virtual {v3, v4, p0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f120772

    .line 104
    invoke-virtual {v3, v4, p0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f120944

    .line 105
    invoke-virtual {v3, v4}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setTitle(I)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    .line 106
    const-string v4, "frp_supported"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 107
    const v4, 0x7f120943

    goto :goto_0

    .line 108
    :cond_1
    const v4, 0x7f120942

    .line 106
    :goto_0
    invoke-virtual {v3, v4}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setMessage(I)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    .line 100
    return-object v3

    .line 82
    :cond_2
    :goto_1
    const-string v4, "lock_type_pattern"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 83
    const v4, 0x7f120958

    .local v4, "titleId":I
    goto :goto_2

    .line 85
    .end local v4    # "titleId":I
    :cond_3
    const-string v4, "lock_type_alphanumeric"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 86
    const v4, 0x7f120957

    goto :goto_2

    :cond_4
    const v4, 0x7f120959

    .line 89
    .restart local v4    # "titleId":I
    :goto_2
    new-instance v5, Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/password/SetupSkipDialog;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 90
    invoke-virtual {v5, v3}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setOnlyLightTheme(Z)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f1213de

    .line 92
    invoke-virtual {v3, v5, p0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f1203d9

    .line 93
    invoke-virtual {v3, v5, p0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    .line 94
    invoke-virtual {v3, v4}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setTitle(I)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    .line 95
    if-eqz v1, :cond_5

    .line 96
    const v5, 0x7f1206d3

    goto :goto_3

    .line 97
    :cond_5
    const v5, 0x7f120709

    .line 95
    :goto_3
    invoke-virtual {v3, v5}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setMessage(I)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v3

    .line 89
    return-object v3
.end method

.method public show(Landroidx/fragment/app/FragmentManager;)V
    .locals 1
    .param p1, "manager"    # Landroidx/fragment/app/FragmentManager;

    .line 124
    const-string v0, "skip_dialog"

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/password/SetupSkipDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 125
    return-void
.end method
