.class Lcom/android/settings/applications/AppPermissionsPreferenceController$1;
.super Lcom/android/settingslib/applications/PermissionsSummaryHelper$PermissionsResultCallback;
.source "AppPermissionsPreferenceController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/applications/AppPermissionsPreferenceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/applications/AppPermissionsPreferenceController;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/AppPermissionsPreferenceController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/settings/applications/AppPermissionsPreferenceController;

    .line 45
    iput-object p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController$1;->this$0:Lcom/android/settings/applications/AppPermissionsPreferenceController;

    invoke-direct {p0}, Lcom/android/settingslib/applications/PermissionsSummaryHelper$PermissionsResultCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onPermissionSummaryResult(IIILjava/util/List;)V
    .locals 1
    .param p1, "standardGrantedPermissionCount"    # I
    .param p2, "requestedPermissionCount"    # I
    .param p3, "additionalGrantedPermissionCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 50
    .local p4, "grantedGroupLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    iget-object v0, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController$1;->this$0:Lcom/android/settings/applications/AppPermissionsPreferenceController;

    invoke-virtual {v0, p4}, Lcom/android/settings/applications/AppPermissionsPreferenceController;->updateSummary(Ljava/util/List;)V

    .line 51
    return-void
.end method
