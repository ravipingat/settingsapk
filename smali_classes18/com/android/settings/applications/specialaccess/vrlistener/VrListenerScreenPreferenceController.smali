.class public Lcom/android/settings/applications/specialaccess/vrlistener/VrListenerScreenPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "VrListenerScreenPreferenceController.java"


# instance fields
.field private final mActivityManager:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/vrlistener/VrListenerScreenPreferenceController;->mContext:Landroid/content/Context;

    const-class v1, Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/settings/applications/specialaccess/vrlistener/VrListenerScreenPreferenceController;->mActivityManager:Landroid/app/ActivityManager;

    .line 32
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/vrlistener/VrListenerScreenPreferenceController;->mContext:Landroid/content/Context;

    .line 36
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/vrlistener/VrListenerScreenPreferenceController;->mActivityManager:Landroid/app/ActivityManager;

    .line 37
    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_0
    const/4 v0, 0x3

    .line 36
    :goto_0
    return v0
.end method
