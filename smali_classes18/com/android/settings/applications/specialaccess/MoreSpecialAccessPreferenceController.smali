.class public Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "MoreSpecialAccessPreferenceController.java"


# instance fields
.field private final mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 37
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPermissionControllerPackageName()Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "packageName":Ljava/lang/String;
    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 39
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MANAGE_SPECIAL_APP_ACCESSES"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 41
    .local v3, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10000

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 43
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v4, :cond_0

    move-object v2, v3

    :cond_0
    iput-object v2, p0, Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;->mIntent:Landroid/content/Intent;

    .line 44
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    goto :goto_0

    .line 45
    :cond_1
    iput-object v2, p0, Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;->mIntent:Landroid/content/Intent;

    .line 47
    :goto_0
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0
.end method

.method public handlePreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 2
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 56
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;->mPreferenceKey:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/applications/specialaccess/MoreSpecialAccessPreferenceController;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 60
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 62
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
