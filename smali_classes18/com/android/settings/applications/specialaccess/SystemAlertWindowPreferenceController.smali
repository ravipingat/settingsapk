.class public Lcom/android/settings/applications/specialaccess/SystemAlertWindowPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "SystemAlertWindowPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 29
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/SystemAlertWindowPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/Utils;->isSystemAlertWindowEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    .line 33
    :goto_0
    return v0
.end method
