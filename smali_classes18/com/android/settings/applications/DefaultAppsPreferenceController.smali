.class public Lcom/android/settings/applications/DefaultAppsPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "DefaultAppsPreferenceController.java"


# instance fields
.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mRoleManager:Landroid/app/role/RoleManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/DefaultAppsPreferenceController;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 40
    const-class v0, Landroid/app/role/RoleManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/role/RoleManager;

    iput-object v0, p0, Lcom/android/settings/applications/DefaultAppsPreferenceController;->mRoleManager:Landroid/app/role/RoleManager;

    .line 41
    return-void
.end method

.method private getDefaultAppLabel(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "roleName"    # Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/android/settings/applications/DefaultAppsPreferenceController;->mRoleManager:Landroid/app/role/RoleManager;

    invoke-virtual {v0, p1}, Landroid/app/role/RoleManager;->getRoleHolders(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 71
    .local v0, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    const/4 v1, 0x0

    return-object v1

    .line 74
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 75
    .local v1, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/settings/applications/DefaultAppsPreferenceController;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-static {v2, v1}, Lcom/android/settingslib/applications/AppUtils;->getApplicationLabel(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 1

    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 5

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v0, "defaultAppLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    const-string v1, "android.app.role.BROWSER"

    invoke-direct {p0, v1}, Lcom/android/settings/applications/DefaultAppsPreferenceController;->getDefaultAppLabel(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 52
    .local v1, "defaultBrowserLabel":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 53
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_0
    const-string v2, "android.app.role.DIALER"

    invoke-direct {p0, v2}, Lcom/android/settings/applications/DefaultAppsPreferenceController;->getDefaultAppLabel(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 56
    .local v2, "defaultPhoneLabel":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 57
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_1
    const-string v3, "android.app.role.SMS"

    invoke-direct {p0, v3}, Lcom/android/settings/applications/DefaultAppsPreferenceController;->getDefaultAppLabel(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 60
    .local v3, "defaultSmsLabel":Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 61
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 64
    const/4 v4, 0x0

    return-object v4

    .line 66
    :cond_3
    invoke-static {}, Landroid/icu/text/ListFormatter;->getInstance()Landroid/icu/text/ListFormatter;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/icu/text/ListFormatter;->format(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
