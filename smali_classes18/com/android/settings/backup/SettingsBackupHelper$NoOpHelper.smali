.class Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
.super Ljava/lang/Object;
.source "SettingsBackupHelper.java"

# interfaces
.implements Landroid/app/backup/BackupHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/backup/SettingsBackupHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NoOpHelper"
.end annotation


# instance fields
.field private final VERSION_CODE:I


# direct methods
.method private static synthetic $closeResource(Ljava/lang/Throwable;Ljava/lang/AutoCloseable;)V
    .locals 1
    .param p0, "x0"    # Ljava/lang/Throwable;
    .param p1, "x1"    # Ljava/lang/AutoCloseable;

    .line 69
    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/lang/AutoCloseable;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/lang/AutoCloseable;->close()V

    :goto_0
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;->VERSION_CODE:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/SettingsBackupHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/settings/backup/SettingsBackupHelper$1;

    .line 52
    invoke-direct {p0}, Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;-><init>()V

    return-void
.end method

.method private getVersionCode(Landroid/os/ParcelFileDescriptor;)I
    .locals 4
    .param p1, "state"    # Landroid/os/ParcelFileDescriptor;

    .line 79
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 80
    return v0

    .line 82
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .local v1, "in":Ljava/io/FileInputStream;
    const/4 v2, 0x0

    .line 83
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->read()I

    move-result v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    :try_start_2
    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;->$closeResource(Ljava/lang/Throwable;Ljava/lang/AutoCloseable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 83
    return v3

    .line 82
    :catchall_0
    move-exception v2

    .end local v1    # "in":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .end local p1    # "state":Landroid/os/ParcelFileDescriptor;
    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 84
    .restart local v1    # "in":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .restart local p1    # "state":Landroid/os/ParcelFileDescriptor;
    :catchall_1
    move-exception v3

    :try_start_4
    invoke-static {v2, v1}, Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;->$closeResource(Ljava/lang/Throwable;Ljava/lang/AutoCloseable;)V

    .end local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .end local p1    # "state":Landroid/os/ParcelFileDescriptor;
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .restart local p1    # "state":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v1

    .line 85
    .local v1, "e":Ljava/io/IOException;
    return v0
.end method


# virtual methods
.method public performBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .locals 4
    .param p1, "oldState"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "data"    # Landroid/app/backup/BackupDataOutput;
    .param p3, "newState"    # Landroid/os/ParcelFileDescriptor;

    .line 60
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .local v0, "out":Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    .line 61
    :try_start_1
    invoke-direct {p0, p1}, Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;->getVersionCode(Landroid/os/ParcelFileDescriptor;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 62
    const-string v2, "dummy"

    invoke-virtual {p2, v2, v3}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    .line 63
    new-array v2, v3, [B

    invoke-virtual {p2, v2, v3}, Landroid/app/backup/BackupDataOutput;->writeEntityData([BI)I

    .line 67
    :cond_0
    invoke-virtual {v0, v3}, Ljava/io/FileOutputStream;->write(I)V

    .line 68
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :try_start_2
    invoke-static {v1, v0}, Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;->$closeResource(Ljava/lang/Throwable;Ljava/lang/AutoCloseable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 60
    :catchall_0
    move-exception v1

    .end local v0    # "out":Ljava/io/FileOutputStream;
    .end local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .end local p1    # "oldState":Landroid/os/ParcelFileDescriptor;
    .end local p2    # "data":Landroid/app/backup/BackupDataOutput;
    .end local p3    # "newState":Landroid/os/ParcelFileDescriptor;
    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 69
    .restart local v0    # "out":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .restart local p1    # "oldState":Landroid/os/ParcelFileDescriptor;
    .restart local p2    # "data":Landroid/app/backup/BackupDataOutput;
    .restart local p3    # "newState":Landroid/os/ParcelFileDescriptor;
    :catchall_1
    move-exception v2

    :try_start_4
    invoke-static {v1, v0}, Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;->$closeResource(Ljava/lang/Throwable;Ljava/lang/AutoCloseable;)V

    .end local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .end local p1    # "oldState":Landroid/os/ParcelFileDescriptor;
    .end local p2    # "data":Landroid/app/backup/BackupDataOutput;
    .end local p3    # "newState":Landroid/os/ParcelFileDescriptor;
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .end local v0    # "out":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/android/settings/backup/SettingsBackupHelper$NoOpHelper;
    .restart local p1    # "oldState":Landroid/os/ParcelFileDescriptor;
    .restart local p2    # "data":Landroid/app/backup/BackupDataOutput;
    .restart local p3    # "newState":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v0

    .line 70
    :goto_0
    return-void
.end method

.method public restoreEntity(Landroid/app/backup/BackupDataInputStream;)V
    .locals 0
    .param p1, "data"    # Landroid/app/backup/BackupDataInputStream;

    .line 73
    return-void
.end method

.method public writeNewStateDescription(Landroid/os/ParcelFileDescriptor;)V
    .locals 0
    .param p1, "newState"    # Landroid/os/ParcelFileDescriptor;

    .line 76
    return-void
.end method
