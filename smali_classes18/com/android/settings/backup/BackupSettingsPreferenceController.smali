.class public Lcom/android/settings/backup/BackupSettingsPreferenceController;
.super Lcom/android/settingslib/core/AbstractPreferenceController;
.source "BackupSettingsPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;


# static fields
.field private static final BACKUP_SETTINGS:Ljava/lang/String; = "backup_settings"

.field private static final MANUFACTURER_SETTINGS:Ljava/lang/String; = "manufacturer_backup"


# instance fields
.field private mBackupSettingsIntent:Landroid/content/Intent;

.field private mBackupSettingsSummary:Ljava/lang/String;

.field private mBackupSettingsTitle:Ljava/lang/CharSequence;

.field private mManufacturerIntent:Landroid/content/Intent;

.field private mManufacturerLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 40
    invoke-direct {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Lcom/android/settings/backup/BackupSettingsHelper;

    invoke-direct {v0, p1}, Lcom/android/settings/backup/BackupSettingsHelper;-><init>(Landroid/content/Context;)V

    .line 42
    .local v0, "settingsHelper":Lcom/android/settings/backup/BackupSettingsHelper;
    invoke-virtual {v0}, Lcom/android/settings/backup/BackupSettingsHelper;->getIntentForBackupSettings()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mBackupSettingsIntent:Landroid/content/Intent;

    .line 43
    invoke-virtual {v0}, Lcom/android/settings/backup/BackupSettingsHelper;->getLabelForBackupSettings()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mBackupSettingsTitle:Ljava/lang/CharSequence;

    .line 44
    invoke-virtual {v0}, Lcom/android/settings/backup/BackupSettingsHelper;->getSummaryForBackupSettings()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mBackupSettingsSummary:Ljava/lang/String;

    .line 45
    invoke-virtual {v0}, Lcom/android/settings/backup/BackupSettingsHelper;->getIntentProvidedByManufacturer()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mManufacturerIntent:Landroid/content/Intent;

    .line 46
    invoke-virtual {v0}, Lcom/android/settings/backup/BackupSettingsHelper;->getLabelProvidedByManufacturer()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mManufacturerLabel:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 3
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 51
    const-string v0, "backup_settings"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    .line 52
    .local v0, "backupSettings":Landroidx/preference/Preference;
    const-string v1, "manufacturer_backup"

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    .line 53
    .local v1, "manufacturerSettings":Landroidx/preference/Preference;
    iget-object v2, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mBackupSettingsIntent:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 54
    iget-object v2, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mBackupSettingsTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v2, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mBackupSettingsSummary:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v2, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mManufacturerIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroidx/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 57
    iget-object v2, p0, Lcom/android/settings/backup/BackupSettingsPreferenceController;->mManufacturerLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 58
    return-void
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAvailable()Z
    .locals 1

    .line 65
    const/4 v0, 0x1

    return v0
.end method
