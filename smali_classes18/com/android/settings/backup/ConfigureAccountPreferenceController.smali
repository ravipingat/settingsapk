.class public Lcom/android/settings/backup/ConfigureAccountPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "ConfigureAccountPreferenceController.java"


# instance fields
.field private mPSCD:Lcom/android/settings/backup/PrivacySettingsConfigData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    invoke-static {}, Lcom/android/settings/backup/PrivacySettingsConfigData;->getInstance()Lcom/android/settings/backup/PrivacySettingsConfigData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/ConfigureAccountPreferenceController;->mPSCD:Lcom/android/settings/backup/PrivacySettingsConfigData;

    .line 33
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/android/settings/backup/ConfigureAccountPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/backup/PrivacySettingsUtils;->isAdminUser(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x4

    return v0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/android/settings/backup/ConfigureAccountPreferenceController;->mContext:Landroid/content/Context;

    const-string v1, "configure_account"

    invoke-static {v0, v1}, Lcom/android/settings/backup/PrivacySettingsUtils;->isInvisibleKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    const/4 v0, 0x3

    return v0

    .line 43
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/android/settings/backup/ConfigureAccountPreferenceController;->mPSCD:Lcom/android/settings/backup/PrivacySettingsConfigData;

    invoke-virtual {v0}, Lcom/android/settings/backup/PrivacySettingsConfigData;->getConfigSummary()Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "configSummary":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 59
    move-object v1, v0

    goto :goto_0

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/android/settings/backup/ConfigureAccountPreferenceController;->mContext:Landroid/content/Context;

    const v2, 0x7f12021a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 58
    :goto_0
    return-object v1
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 2
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 48
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 49
    iget-object v0, p0, Lcom/android/settings/backup/ConfigureAccountPreferenceController;->mPSCD:Lcom/android/settings/backup/PrivacySettingsConfigData;

    invoke-virtual {v0}, Lcom/android/settings/backup/PrivacySettingsConfigData;->getConfigIntent()Landroid/content/Intent;

    move-result-object v0

    .line 50
    .local v0, "configIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/backup/ConfigureAccountPreferenceController;->mPSCD:Lcom/android/settings/backup/PrivacySettingsConfigData;

    invoke-virtual {v1}, Lcom/android/settings/backup/PrivacySettingsConfigData;->isBackupEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 51
    .local v1, "configureEnabled":Z
    :goto_0
    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    .line 52
    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 53
    return-void
.end method
