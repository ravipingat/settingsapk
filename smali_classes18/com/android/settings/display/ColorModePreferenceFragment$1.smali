.class Lcom/android/settings/display/ColorModePreferenceFragment$1;
.super Landroid/database/ContentObserver;
.source "ColorModePreferenceFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/display/ColorModePreferenceFragment;->onAttach(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/ColorModePreferenceFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/ColorModePreferenceFragment;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/settings/display/ColorModePreferenceFragment;
    .param p2, "x0"    # Landroid/os/Handler;

    .line 66
    iput-object p1, p0, Lcom/android/settings/display/ColorModePreferenceFragment$1;->this$0:Lcom/android/settings/display/ColorModePreferenceFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 69
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 70
    iget-object v0, p0, Lcom/android/settings/display/ColorModePreferenceFragment$1;->this$0:Lcom/android/settings/display/ColorModePreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/ColorModePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/hardware/display/ColorDisplayManager;->areAccessibilityTransformsEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/android/settings/display/ColorModePreferenceFragment$1;->this$0:Lcom/android/settings/display/ColorModePreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/ColorModePreferenceFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->finish()V

    .line 75
    :cond_0
    return-void
.end method
