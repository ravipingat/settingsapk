.class public Lcom/android/settings/display/AutoBrightnessDetailPreferenceController;
.super Lcom/android/settings/display/AutoBrightnessPreferenceController;
.source "AutoBrightnessDetailPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/AutoBrightnessPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/android/settings/display/AutoBrightnessDetailPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1110027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    .line 30
    :goto_0
    return v0
.end method

.method public isSliceable()Z
    .locals 1

    .line 37
    const/4 v0, 0x1

    return v0
.end method
