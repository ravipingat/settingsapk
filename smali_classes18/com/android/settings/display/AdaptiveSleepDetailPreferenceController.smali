.class public Lcom/android/settings/display/AdaptiveSleepDetailPreferenceController;
.super Lcom/android/settings/display/AdaptiveSleepPreferenceController;
.source "AdaptiveSleepDetailPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/AdaptiveSleepPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/android/settings/display/AdaptiveSleepDetailPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x111000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x0

    goto :goto_0

    .line 34
    :cond_0
    const/4 v0, 0x3

    .line 31
    :goto_0
    return v0
.end method

.method public isSliceable()Z
    .locals 1

    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 44
    invoke-super {p0, p1}, Lcom/android/settings/display/AdaptiveSleepPreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 45
    iget-boolean v0, p0, Lcom/android/settings/display/AdaptiveSleepPreferenceController;->hasSufficientPermissions:Z

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    .line 46
    return-void
.end method
