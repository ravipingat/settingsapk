.class public Lcom/android/settings/display/VrDisplayPreferenceController;
.super Lcom/android/settingslib/core/AbstractPreferenceController;
.source "VrDisplayPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;


# static fields
.field private static final KEY_VR_DISPLAY_PREF:Ljava/lang/String; = "vr_display_pref"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 33
    invoke-direct {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 44
    const-string v0, "vr_display_pref"

    return-object v0
.end method

.method public isAvailable()Z
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/android/settings/display/VrDisplayPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 39
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v1, "android.hardware.vr.high_performance"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 4
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 49
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    .line 50
    .local v0, "currentUser":I
    iget-object v1, p0, Lcom/android/settings/display/VrDisplayPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "vr_display_mode"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v0}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 53
    .local v1, "current":I
    if-nez v1, :cond_0

    .line 54
    const v2, 0x7f12060d

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_0

    .line 56
    :cond_0
    const v2, 0x7f12060e

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setSummary(I)V

    .line 58
    :goto_0
    return-void
.end method
