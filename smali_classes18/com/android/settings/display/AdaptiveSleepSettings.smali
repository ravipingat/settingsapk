.class public Lcom/android/settings/display/AdaptiveSleepSettings;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "AdaptiveSleepSettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field private static final TAG:Ljava/lang/String; = "AdaptiveSleepSettings"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    new-instance v0, Lcom/android/settings/display/AdaptiveSleepSettings$1;

    invoke-direct {v0}, Lcom/android/settings/display/AdaptiveSleepSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/display/AdaptiveSleepSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getHelpResource()I
    .locals 1

    .line 64
    const v0, 0x7f1207bc

    return v0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 54
    const-string v0, "AdaptiveSleepSettings"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 59
    const/16 v0, 0x65c

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 49
    const v0, 0x7f160010

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .line 40
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/android/settings/display/AdaptiveSleepSettings;->mFooterPreferenceMixin:Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;

    .line 42
    invoke-virtual {v0}, Lcom/oneplus/settings/widget/FooterPreferenceMixinCompat;->createFooterPreference()Lcom/oneplus/settings/widget/OPFooterPreference;

    move-result-object v0

    .line 43
    .local v0, "footerPreference":Lcom/oneplus/settings/widget/OPFooterPreference;
    const v1, 0x7f08031a

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/widget/OPFooterPreference;->setIcon(I)V

    .line 44
    const v1, 0x7f1200d6

    invoke-virtual {v0, v1}, Lcom/oneplus/settings/widget/OPFooterPreference;->setTitle(I)V

    .line 45
    return-void
.end method
