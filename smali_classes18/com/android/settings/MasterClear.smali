.class public Lcom/android/settings/MasterClear;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "MasterClear.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/oneplus/settings/ui/OPFactoryResetConfirmCategory$OnFactoryResetConfirmListener;


# static fields
.field static final CREDENTIAL_CONFIRM_REQUEST:I = 0x38
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field static final ERASE_ESIMS_EXTRA:Ljava/lang/String; = "erase_esim"

.field static final ERASE_EXTERNAL_EXTRA:Ljava/lang/String; = "erase_sd"

.field static final KEYGUARD_REQUEST:I = 0x37
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final KEY_SHOW_ESIM_RESET_CHECKBOX:Ljava/lang/String; = "masterclear.allow_retain_esim_profiles_after_fdr"

.field private static final TAG:Ljava/lang/String; = "MasterClear"


# instance fields
.field private mContentView:Landroid/view/View;

.field mEsimStorage:Landroid/widget/CheckBox;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field mEsimStorageContainer:Landroid/view/View;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field mExternalStorage:Landroid/widget/CheckBox;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private mExternalStorageContainer:Landroid/view/View;

.field mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field protected final mInitiateListener:Landroid/view/View$OnClickListener;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private mOPFactoryResetConfirmCategory:Lcom/oneplus/settings/ui/OPFactoryResetConfirmCategory;

.field private mOptionalSwitchPreference:Landroidx/preference/SwitchPreference;

.field mScrollView:Landroid/widget/ScrollView;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 93
    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    .line 284
    new-instance v0, Lcom/android/settings/MasterClear$1;

    invoke-direct {v0, p0}, Lcom/android/settings/MasterClear$1;-><init>(Lcom/android/settings/MasterClear;)V

    iput-object v0, p0, Lcom/android/settings/MasterClear;->mInitiateListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/MasterClear;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/MasterClear;
    .param p1, "x1"    # I

    .line 93
    invoke-direct {p0, p1}, Lcom/android/settings/MasterClear;->runKeyguardConfirmation(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/android/settings/MasterClear;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/settings/MasterClear;
    .param p1, "x1"    # Ljava/lang/String;

    .line 93
    invoke-direct {p0, p1}, Lcom/android/settings/MasterClear;->showFinalConfirmation(Ljava/lang/String;)V

    return-void
.end method

.method private getContentDescription(Landroid/view/View;Ljava/lang/StringBuffer;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "description"    # Ljava/lang/StringBuffer;

    .line 490
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    return-void

    .line 493
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 494
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    .line 495
    .local v0, "vGroup":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 496
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 497
    .local v2, "nextChild":Landroid/view/View;
    invoke-direct {p0, v2, p2}, Lcom/android/settings/MasterClear;->getContentDescription(Landroid/view/View;Ljava/lang/StringBuffer;)V

    .line 495
    .end local v2    # "nextChild":Landroid/view/View;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "vGroup":Landroid/view/ViewGroup;
    .end local v1    # "i":I
    :cond_1
    goto :goto_1

    .line 499
    :cond_2
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 500
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    .line 501
    .local v0, "vText":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    .line 502
    const-string v1, ","

    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 499
    .end local v0    # "vText":Landroid/widget/TextView;
    :cond_3
    :goto_1
    nop

    .line 504
    :goto_2
    return-void
.end method

.method private isExtStorageEncrypted()Z
    .locals 2

    .line 507
    invoke-static {}, Landroid/sysprop/VoldProperties;->decrypt()Ljava/util/Optional;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 508
    .local v0, "state":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    return v1
.end method

.method private loadAccountList(Landroid/os/UserManager;)V
    .locals 25
    .param p1, "um"    # Landroid/os/UserManager;

    .line 512
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/settings/MasterClear;->mContentView:Landroid/view/View;

    const v2, 0x7f0a0034

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 513
    .local v2, "accountsLabel":Landroid/view/View;
    iget-object v0, v1, Lcom/android/settings/MasterClear;->mContentView:Landroid/view/View;

    const v3, 0x7f0a0033

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/LinearLayout;

    .line 514
    .local v3, "contents":Landroid/widget/LinearLayout;
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 516
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v4

    .line 517
    .local v4, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    move-object/from16 v5, p1

    invoke-virtual {v5, v0}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v6

    .line 518
    .local v6, "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 520
    .local v7, "profilesSize":I
    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    .line 522
    .local v8, "mgr":Landroid/accounts/AccountManager;
    const-string v0, "layout_inflater"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/view/LayoutInflater;

    .line 525
    .local v9, "inflater":Landroid/view/LayoutInflater;
    const/4 v0, 0x0

    .line 526
    .local v0, "accountsCount":I
    const/4 v10, 0x0

    .local v10, "profileIndex":I
    :goto_0
    if-ge v10, v7, :cond_9

    .line 527
    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/pm/UserInfo;

    .line 528
    .local v13, "userInfo":Landroid/content/pm/UserInfo;
    iget v14, v13, Landroid/content/pm/UserInfo;->id:I

    .line 529
    .local v14, "profileId":I
    new-instance v15, Landroid/os/UserHandle;

    invoke-direct {v15, v14}, Landroid/os/UserHandle;-><init>(I)V

    .line 530
    .local v15, "userHandle":Landroid/os/UserHandle;
    invoke-virtual {v8, v14}, Landroid/accounts/AccountManager;->getAccountsAsUser(I)[Landroid/accounts/Account;

    move-result-object v12

    .line 531
    .local v12, "accounts":[Landroid/accounts/Account;
    array-length v11, v12

    .line 532
    .local v11, "N":I
    if-nez v11, :cond_0

    .line 533
    move-object/from16 v18, v6

    move-object/from16 v19, v8

    goto/16 :goto_c

    .line 535
    :cond_0
    add-int v17, v0, v11

    .line 537
    .end local v0    # "accountsCount":I
    .local v17, "accountsCount":I
    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 538
    invoke-virtual {v0, v14}, Landroid/accounts/AccountManager;->getAuthenticatorTypesAsUser(I)[Landroid/accounts/AuthenticatorDescription;

    move-result-object v5

    .line 539
    .local v5, "descs":[Landroid/accounts/AuthenticatorDescription;
    move-object/from16 v18, v6

    .end local v6    # "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    .local v18, "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    array-length v6, v5

    .line 541
    .local v6, "M":I
    move-object/from16 v19, v8

    .end local v8    # "mgr":Landroid/accounts/AccountManager;
    .local v19, "mgr":Landroid/accounts/AccountManager;
    const v8, 0x1020016

    const/4 v0, 0x1

    if-le v7, v0, :cond_2

    .line 542
    invoke-static {v9, v3}, Lcom/android/settings/Utils;->inflateCategoryHeader(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 543
    .local v0, "titleView":Landroid/view/View;
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    move-object/from16 v8, v16

    check-cast v8, Landroid/widget/TextView;

    .line 544
    .local v8, "titleText":Landroid/widget/TextView;
    invoke-virtual {v13}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v16

    if-eqz v16, :cond_1

    const v16, 0x7f1203fe

    move-object/from16 v20, v13

    move/from16 v13, v16

    goto :goto_1

    .line 545
    :cond_1
    const v16, 0x7f1203fd

    move-object/from16 v20, v13

    move/from16 v13, v16

    .line 544
    .end local v13    # "userInfo":Landroid/content/pm/UserInfo;
    .local v20, "userInfo":Landroid/content/pm/UserInfo;
    :goto_1
    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(I)V

    .line 546
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 541
    .end local v0    # "titleView":Landroid/view/View;
    .end local v8    # "titleText":Landroid/widget/TextView;
    .end local v20    # "userInfo":Landroid/content/pm/UserInfo;
    .restart local v13    # "userInfo":Landroid/content/pm/UserInfo;
    :cond_2
    move-object/from16 v20, v13

    .line 549
    .end local v13    # "userInfo":Landroid/content/pm/UserInfo;
    .restart local v20    # "userInfo":Landroid/content/pm/UserInfo;
    :goto_2
    const/4 v0, 0x0

    move v8, v0

    .local v8, "i":I
    :goto_3
    if-ge v8, v11, :cond_8

    .line 550
    aget-object v13, v12, v8

    .line 551
    .local v13, "account":Landroid/accounts/Account;
    const/4 v0, 0x0

    .line 552
    .local v0, "desc":Landroid/accounts/AuthenticatorDescription;
    const/16 v16, 0x0

    move-object/from16 v21, v0

    move/from16 v0, v16

    .local v0, "j":I
    .local v21, "desc":Landroid/accounts/AuthenticatorDescription;
    :goto_4
    if-ge v0, v6, :cond_4

    .line 553
    move/from16 v16, v6

    .end local v6    # "M":I
    .local v16, "M":I
    iget-object v6, v13, Landroid/accounts/Account;->type:Ljava/lang/String;

    move/from16 v22, v11

    .end local v11    # "N":I
    .local v22, "N":I
    aget-object v11, v5, v0

    iget-object v11, v11, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 554
    aget-object v6, v5, v0

    .line 555
    .end local v21    # "desc":Landroid/accounts/AuthenticatorDescription;
    .local v6, "desc":Landroid/accounts/AuthenticatorDescription;
    goto :goto_5

    .line 552
    .end local v6    # "desc":Landroid/accounts/AuthenticatorDescription;
    .restart local v21    # "desc":Landroid/accounts/AuthenticatorDescription;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    move/from16 v6, v16

    move/from16 v11, v22

    goto :goto_4

    .end local v16    # "M":I
    .end local v22    # "N":I
    .local v6, "M":I
    .restart local v11    # "N":I
    :cond_4
    move/from16 v16, v6

    move/from16 v22, v11

    .end local v6    # "M":I
    .end local v11    # "N":I
    .restart local v16    # "M":I
    .restart local v22    # "N":I
    move-object/from16 v6, v21

    .line 558
    .end local v0    # "j":I
    .end local v21    # "desc":Landroid/accounts/AuthenticatorDescription;
    .local v6, "desc":Landroid/accounts/AuthenticatorDescription;
    :goto_5
    const-string v11, "MasterClear"

    if-nez v6, :cond_5

    .line 559
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v21, v5

    .end local v5    # "descs":[Landroid/accounts/AuthenticatorDescription;
    .local v21, "descs":[Landroid/accounts/AuthenticatorDescription;
    const-string v5, "No descriptor for account name="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " type="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v13, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    move-object/from16 v24, v12

    goto/16 :goto_b

    .line 563
    .end local v21    # "descs":[Landroid/accounts/AuthenticatorDescription;
    .restart local v5    # "descs":[Landroid/accounts/AuthenticatorDescription;
    :cond_5
    move-object/from16 v21, v5

    .end local v5    # "descs":[Landroid/accounts/AuthenticatorDescription;
    .restart local v21    # "descs":[Landroid/accounts/AuthenticatorDescription;
    const/4 v5, 0x0

    .line 565
    .local v5, "icon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    iget v0, v6, Landroid/accounts/AuthenticatorDescription;->iconId:I

    if-eqz v0, :cond_6

    .line 566
    iget-object v0, v6, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    move-object/from16 v23, v5

    const/4 v5, 0x0

    .end local v5    # "icon":Landroid/graphics/drawable/Drawable;
    .local v23, "icon":Landroid/graphics/drawable/Drawable;
    :try_start_1
    invoke-virtual {v4, v0, v5, v15}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v0

    .line 568
    .local v0, "authContext":Landroid/content/Context;
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-object/from16 v24, v12

    .end local v12    # "accounts":[Landroid/accounts/Account;
    .local v24, "accounts":[Landroid/accounts/Account;
    :try_start_2
    iget v12, v6, Landroid/accounts/AuthenticatorDescription;->iconId:I

    .line 569
    invoke-virtual {v0, v12}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 568
    invoke-virtual {v5, v12, v15}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v5
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .end local v23    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v5    # "icon":Landroid/graphics/drawable/Drawable;
    goto :goto_6

    .line 573
    .end local v0    # "authContext":Landroid/content/Context;
    .end local v5    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v23    # "icon":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v0

    goto :goto_7

    .line 571
    :catch_1
    move-exception v0

    goto :goto_8

    .line 573
    .end local v24    # "accounts":[Landroid/accounts/Account;
    .restart local v12    # "accounts":[Landroid/accounts/Account;
    :catch_2
    move-exception v0

    move-object/from16 v24, v12

    .end local v12    # "accounts":[Landroid/accounts/Account;
    .restart local v24    # "accounts":[Landroid/accounts/Account;
    goto :goto_7

    .line 571
    .end local v24    # "accounts":[Landroid/accounts/Account;
    .restart local v12    # "accounts":[Landroid/accounts/Account;
    :catch_3
    move-exception v0

    move-object/from16 v24, v12

    .end local v12    # "accounts":[Landroid/accounts/Account;
    .restart local v24    # "accounts":[Landroid/accounts/Account;
    goto :goto_8

    .line 565
    .end local v23    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v24    # "accounts":[Landroid/accounts/Account;
    .restart local v5    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v12    # "accounts":[Landroid/accounts/Account;
    :cond_6
    move-object/from16 v23, v5

    move-object/from16 v24, v12

    .line 575
    .end local v12    # "accounts":[Landroid/accounts/Account;
    .restart local v24    # "accounts":[Landroid/accounts/Account;
    :goto_6
    goto :goto_a

    .line 573
    .end local v24    # "accounts":[Landroid/accounts/Account;
    .restart local v12    # "accounts":[Landroid/accounts/Account;
    :catch_4
    move-exception v0

    move-object/from16 v23, v5

    move-object/from16 v24, v12

    .line 574
    .end local v5    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v12    # "accounts":[Landroid/accounts/Account;
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v23    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v24    # "accounts":[Landroid/accounts/Account;
    :goto_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid icon id for account type "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v6, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v11, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    .line 571
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v23    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v24    # "accounts":[Landroid/accounts/Account;
    .restart local v5    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v12    # "accounts":[Landroid/accounts/Account;
    :catch_5
    move-exception v0

    move-object/from16 v23, v5

    move-object/from16 v24, v12

    .line 572
    .end local v5    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v12    # "accounts":[Landroid/accounts/Account;
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v23    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v24    # "accounts":[Landroid/accounts/Account;
    :goto_8
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Bad package name for account type "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v6, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v11, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    nop

    .line 576
    :goto_9
    move-object/from16 v5, v23

    .end local v23    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v5    # "icon":Landroid/graphics/drawable/Drawable;
    :goto_a
    if-nez v5, :cond_7

    .line 577
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 580
    :cond_7
    const v0, 0x7f0d0108

    const/4 v11, 0x0

    invoke-virtual {v9, v0, v3, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 581
    .local v0, "child":Landroid/view/View;
    const v11, 0x1020006

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 582
    const v11, 0x1020016

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    iget-object v11, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 583
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 549
    .end local v0    # "child":Landroid/view/View;
    .end local v5    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v6    # "desc":Landroid/accounts/AuthenticatorDescription;
    .end local v13    # "account":Landroid/accounts/Account;
    :goto_b
    add-int/lit8 v8, v8, 0x1

    move/from16 v6, v16

    move-object/from16 v5, v21

    move/from16 v11, v22

    move-object/from16 v12, v24

    goto/16 :goto_3

    .end local v16    # "M":I
    .end local v21    # "descs":[Landroid/accounts/AuthenticatorDescription;
    .end local v22    # "N":I
    .end local v24    # "accounts":[Landroid/accounts/Account;
    .local v5, "descs":[Landroid/accounts/AuthenticatorDescription;
    .local v6, "M":I
    .restart local v11    # "N":I
    .restart local v12    # "accounts":[Landroid/accounts/Account;
    :cond_8
    move-object/from16 v21, v5

    move/from16 v16, v6

    move/from16 v22, v11

    move-object/from16 v24, v12

    .end local v5    # "descs":[Landroid/accounts/AuthenticatorDescription;
    .end local v6    # "M":I
    .end local v11    # "N":I
    .end local v12    # "accounts":[Landroid/accounts/Account;
    .restart local v16    # "M":I
    .restart local v21    # "descs":[Landroid/accounts/AuthenticatorDescription;
    .restart local v22    # "N":I
    .restart local v24    # "accounts":[Landroid/accounts/Account;
    move/from16 v0, v17

    .line 526
    .end local v8    # "i":I
    .end local v14    # "profileId":I
    .end local v15    # "userHandle":Landroid/os/UserHandle;
    .end local v16    # "M":I
    .end local v17    # "accountsCount":I
    .end local v20    # "userInfo":Landroid/content/pm/UserInfo;
    .end local v21    # "descs":[Landroid/accounts/AuthenticatorDescription;
    .end local v22    # "N":I
    .end local v24    # "accounts":[Landroid/accounts/Account;
    .local v0, "accountsCount":I
    :goto_c
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v5, p1

    move-object/from16 v6, v18

    move-object/from16 v8, v19

    goto/16 :goto_0

    .end local v18    # "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    .end local v19    # "mgr":Landroid/accounts/AccountManager;
    .local v6, "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    .local v8, "mgr":Landroid/accounts/AccountManager;
    :cond_9
    move-object/from16 v18, v6

    move-object/from16 v19, v8

    const/4 v5, 0x1

    .line 587
    .end local v6    # "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    .end local v8    # "mgr":Landroid/accounts/AccountManager;
    .end local v10    # "profileIndex":I
    .restart local v18    # "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    .restart local v19    # "mgr":Landroid/accounts/AccountManager;
    if-lez v0, :cond_a

    .line 588
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 589
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_d

    .line 587
    :cond_a
    const/4 v6, 0x0

    .line 592
    :goto_d
    iget-object v8, v1, Lcom/android/settings/MasterClear;->mContentView:Landroid/view/View;

    const v10, 0x7f0a04f3

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 593
    .local v8, "otherUsers":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/os/UserManager;->getUserCount()I

    move-result v10

    sub-int/2addr v10, v7

    if-lez v10, :cond_b

    goto :goto_e

    :cond_b
    move v5, v6

    .line 594
    .local v5, "hasOtherUsers":Z
    :goto_e
    if-eqz v5, :cond_c

    goto :goto_f

    :cond_c
    const/16 v12, 0x8

    move v6, v12

    :goto_f
    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 595
    return-void
.end method

.method private runKeyguardConfirmation(I)Z
    .locals 3
    .param p1, "request"    # I

    .line 162
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 163
    .local v0, "res":Landroid/content/res/Resources;
    new-instance v1, Lcom/android/settings/password/ChooseLockSettingsHelper;

    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/android/settings/password/ChooseLockSettingsHelper;-><init>(Landroid/app/Activity;Landroidx/fragment/app/Fragment;)V

    .line 164
    const v2, 0x7f1209fe

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 163
    invoke-virtual {v1, p1, v2}, Lcom/android/settings/password/ChooseLockSettingsHelper;->launchConfirmationActivity(ILjava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method private setUpActionBarAndTitle()V
    .locals 4

    .line 140
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    .line 141
    .local v0, "activity":Landroid/app/Activity;
    const-string v1, "MasterClear"

    if-nez v0, :cond_0

    .line 142
    const-string v2, "No activity attached, skipping setUpActionBarAndTitle"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return-void

    .line 145
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    .line 146
    .local v2, "actionBar":Landroid/app/ActionBar;
    if-nez v2, :cond_1

    .line 147
    const-string v3, "No actionbar, skipping setUpActionBarAndTitle"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return-void

    .line 150
    :cond_1
    invoke-virtual {v2}, Landroid/app/ActionBar;->hide()V

    .line 151
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 152
    return-void
.end method

.method private setUpInitiateButton()V
    .locals 4

    .line 472
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;

    if-eqz v0, :cond_0

    .line 473
    return-void

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mContentView:Landroid/view/View;

    const v1, 0x7f0a060f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupdesign/GlifLayout;

    .line 477
    .local v0, "layout":Lcom/google/android/setupdesign/GlifLayout;
    const-class v1, Lcom/google/android/setupcompat/template/FooterBarMixin;

    invoke-virtual {v0, v1}, Lcom/google/android/setupdesign/GlifLayout;->getMixin(Ljava/lang/Class;)Lcom/google/android/setupcompat/template/Mixin;

    move-result-object v1

    check-cast v1, Lcom/google/android/setupcompat/template/FooterBarMixin;

    .line 478
    .local v1, "mixin":Lcom/google/android/setupcompat/template/FooterBarMixin;
    new-instance v2, Lcom/google/android/setupcompat/template/FooterButton$Builder;

    .line 479
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/setupcompat/template/FooterButton$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f1209ef

    .line 480
    invoke-virtual {v2, v3}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setText(I)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MasterClear;->mInitiateListener:Landroid/view/View$OnClickListener;

    .line 481
    invoke-virtual {v2, v3}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setListener(Landroid/view/View$OnClickListener;)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v2

    const/4 v3, 0x0

    .line 482
    invoke-virtual {v2, v3}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setButtonType(I)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v2

    const v3, 0x7f1303e2

    .line 483
    invoke-virtual {v2, v3}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setTheme(I)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v2

    .line 484
    invoke-virtual {v2}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->build()Lcom/google/android/setupcompat/template/FooterButton;

    move-result-object v2

    .line 478
    invoke-virtual {v1, v2}, Lcom/google/android/setupcompat/template/FooterBarMixin;->setPrimaryButton(Lcom/google/android/setupcompat/template/FooterButton;)V

    .line 486
    invoke-virtual {v1}, Lcom/google/android/setupcompat/template/FooterBarMixin;->getPrimaryButton()Lcom/google/android/setupcompat/template/FooterButton;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/MasterClear;->mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;

    .line 487
    return-void
.end method

.method private showFinalConfirmation(Ljava/lang/String;)V
    .locals 3
    .param p1, "psw"    # Ljava/lang/String;

    .line 210
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 211
    .local v0, "args":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/android/settings/MasterClear;->mOptionalSwitchPreference:Landroidx/preference/SwitchPreference;

    invoke-virtual {v1}, Landroidx/preference/SwitchPreference;->isChecked()Z

    move-result v1

    const-string v2, "erase_sd"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 212
    const-string v1, "power_on_psw"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    new-instance v1, Lcom/android/settings/core/SubSettingLauncher;

    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;-><init>(Landroid/content/Context;)V

    const-class v2, Lcom/android/settings/MasterClearConfirm;

    .line 216
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setDestination(Ljava/lang/String;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 217
    const/16 v2, 0x42

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setSourceMetricsCategory(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 218
    invoke-virtual {v1, v0}, Lcom/android/settings/core/SubSettingLauncher;->setArguments(Landroid/os/Bundle;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 219
    invoke-virtual {v1}, Lcom/android/settings/core/SubSettingLauncher;->launch()V

    .line 220
    return-void
.end method


# virtual methods
.method establishInitialState()V
    .locals 2
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 331
    const-string v0, "op_optional_reset"

    invoke-virtual {p0, v0}, Lcom/android/settings/MasterClear;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/MasterClear;->mOptionalSwitchPreference:Landroidx/preference/SwitchPreference;

    .line 333
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->isSupportUstMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mOptionalSwitchPreference:Landroidx/preference/SwitchPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mOptionalSwitchPreference:Landroidx/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .line 339
    :goto_0
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mOptionalSwitchPreference:Landroidx/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    .line 340
    const-string v0, "op_factory_reset_confirm"

    invoke-virtual {p0, v0}, Lcom/android/settings/MasterClear;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/ui/OPFactoryResetConfirmCategory;

    iput-object v0, p0, Lcom/android/settings/MasterClear;->mOPFactoryResetConfirmCategory:Lcom/oneplus/settings/ui/OPFactoryResetConfirmCategory;

    .line 341
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mOPFactoryResetConfirmCategory:Lcom/oneplus/settings/ui/OPFactoryResetConfirmCategory;

    invoke-virtual {v0, p0}, Lcom/oneplus/settings/ui/OPFactoryResetConfirmCategory;->setOnFactoryResetConfirmListener(Lcom/oneplus/settings/ui/OPFactoryResetConfirmCategory$OnFactoryResetConfirmListener;)V

    .line 427
    return-void
.end method

.method getAccountConfirmationIntent()Landroid/content/Intent;
    .locals 13
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 243
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    .line 244
    .local v0, "context":Landroid/content/Context;
    const v1, 0x7f1200cb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "accountType":Ljava/lang/String;
    const v2, 0x7f1200bd

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "packageName":Ljava/lang/String;
    const v3, 0x7f1200bc

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 247
    .local v3, "className":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    const-string v6, "MasterClear"

    if-nez v4, :cond_3

    .line 248
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 249
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    .line 253
    :cond_0
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    .line 254
    .local v4, "am":Landroid/accounts/AccountManager;
    invoke-virtual {v4, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 255
    .local v7, "accounts":[Landroid/accounts/Account;
    if-eqz v7, :cond_2

    array-length v8, v7

    if-lez v8, :cond_2

    .line 256
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 257
    invoke-virtual {v8, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    new-instance v9, Landroid/content/ComponentName;

    invoke-direct {v9, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v8

    .line 260
    .local v8, "requestAccountConfirmation":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 261
    .local v9, "pm":Landroid/content/pm/PackageManager;
    const/4 v10, 0x0

    invoke-virtual {v9, v8, v10}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v10

    .line 262
    .local v10, "resolution":Landroid/content/pm/ResolveInfo;
    if-eqz v10, :cond_1

    iget-object v11, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v11, :cond_1

    iget-object v11, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 264
    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 267
    return-object v8

    .line 269
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unable to resolve Activity: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    .end local v8    # "requestAccountConfirmation":Landroid/content/Intent;
    .end local v9    # "pm":Landroid/content/pm/PackageManager;
    .end local v10    # "resolution":Landroid/content/pm/ResolveInfo;
    goto :goto_0

    .line 272
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " accounts installed!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :goto_0
    return-object v5

    .line 250
    .end local v4    # "am":Landroid/accounts/AccountManager;
    .end local v7    # "accounts":[Landroid/accounts/Account;
    :cond_3
    :goto_1
    const-string v4, "Resources not set for account confirmation."

    invoke-static {v6, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    return-object v5
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 627
    const/16 v0, 0x42

    return v0
.end method

.method hasReachedBottom(Landroid/widget/ScrollView;)Z
    .locals 6
    .param p1, "scrollView"    # Landroid/widget/ScrollView;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 461
    invoke-virtual {p1}, Landroid/widget/ScrollView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 462
    return v1

    .line 465
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 466
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v4

    invoke-virtual {p1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v5

    add-int/2addr v4, v5

    sub-int/2addr v3, v4

    .line 468
    .local v3, "diff":I
    if-gtz v3, :cond_1

    move v0, v1

    :cond_1
    return v0
.end method

.method protected isEuiccEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 455
    const-string v0, "euicc"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/euicc/EuiccManager;

    .line 456
    .local v0, "euiccManager":Landroid/telephony/euicc/EuiccManager;
    invoke-virtual {v0}, Landroid/telephony/euicc/EuiccManager;->isEnabled()Z

    move-result v1

    return v1
.end method

.method isValidRequestCode(I)Z
    .locals 1
    .param p1, "requestCode"    # I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 169
    const/16 v0, 0x37

    if-eq p1, v0, :cond_1

    const/16 v0, 0x38

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 174
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/SettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 175
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/settings/MasterClear;->onActivityResultInternal(IILandroid/content/Intent;)V

    .line 176
    return-void
.end method

.method onActivityResultInternal(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 183
    invoke-virtual {p0, p1}, Lcom/android/settings/MasterClear;->isValidRequestCode(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    return-void

    .line 187
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->establishInitialState()V

    .line 189
    return-void

    .line 192
    :cond_1
    const/4 v0, 0x0

    .line 195
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x38

    if-eq v1, p1, :cond_2

    .line 196
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getAccountConfirmationIntent()Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    if-eqz v1, :cond_2

    .line 197
    invoke-virtual {p0, v0}, Lcom/android/settings/MasterClear;->showAccountCredentialConfirmation(Landroid/content/Intent;)V

    goto :goto_0

    .line 201
    :cond_2
    if-eqz p3, :cond_3

    .line 202
    const-string v1, "power_on_psw"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/MasterClear;->showFinalConfirmation(Ljava/lang/String;)V

    .line 206
    :cond_3
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 132
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 133
    const v0, 0x7f1600b1

    invoke-virtual {p0, v0}, Lcom/android/settings/MasterClear;->addPreferencesFromResource(I)V

    .line 134
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->establishInitialState()V

    .line 135
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f1209fe

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentActivity;->setTitle(I)V

    .line 136
    return-void
.end method

.method public onFactoryResetConfirmClick()V
    .locals 1

    .line 642
    const/16 v0, 0x37

    invoke-direct {p0, v0}, Lcom/android/settings/MasterClear;->runKeyguardConfirmation(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/android/settings/MasterClear;->showFinalConfirmation(Ljava/lang/String;)V

    .line 645
    :cond_0
    return-void
.end method

.method public onGlobalLayout()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;

    iget-object v1, p0, Lcom/android/settings/MasterClear;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {p0, v1}, Lcom/android/settings/MasterClear;->hasReachedBottom(Landroid/widget/ScrollView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/setupcompat/template/FooterButton;->setEnabled(Z)V

    .line 127
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 634
    iget-object v0, p0, Lcom/android/settings/MasterClear;->mOptionalSwitchPreference:Landroidx/preference/SwitchPreference;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    .line 635
    return v1

    .line 637
    :cond_0
    return v1
.end method

.method showAccountCredentialConfirmation(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 238
    const/16 v0, 0x38

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/MasterClear;->startActivityForResult(Landroid/content/Intent;I)V

    .line 239
    return-void
.end method

.method showFinalConfirmation()V
    .locals 3
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 225
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 226
    .local v0, "args":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/android/settings/MasterClear;->mExternalStorage:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    const-string v2, "erase_sd"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 227
    iget-object v1, p0, Lcom/android/settings/MasterClear;->mEsimStorage:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    const-string v2, "erase_esim"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 228
    new-instance v1, Lcom/android/settings/core/SubSettingLauncher;

    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;-><init>(Landroid/content/Context;)V

    const-class v2, Lcom/android/settings/MasterClearConfirm;

    .line 229
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setDestination(Ljava/lang/String;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 230
    invoke-virtual {v1, v0}, Lcom/android/settings/core/SubSettingLauncher;->setArguments(Landroid/os/Bundle;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 231
    const v2, 0x7f1209f0

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setTitleRes(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 232
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getMetricsCategory()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setSourceMetricsCategory(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 233
    invoke-virtual {v1}, Lcom/android/settings/core/SubSettingLauncher;->launch()V

    .line 234
    return-void
.end method

.method showWipeEuicc()Z
    .locals 4
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 437
    invoke-virtual {p0}, Lcom/android/settings/MasterClear;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 438
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p0, v0}, Lcom/android/settings/MasterClear;->isEuiccEnabled(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 439
    return v2

    .line 441
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 442
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v3, "euicc_provisioned"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_1

    .line 443
    const-string v3, "development_settings_enabled"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const/4 v2, 0x1

    .line 442
    :cond_2
    return v2
.end method

.method showWipeEuiccCheckbox()Z
    .locals 2
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 449
    nop

    .line 450
    const-string v0, "masterclear.allow_retain_esim_profiles_after_fdr"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 449
    return v0
.end method
