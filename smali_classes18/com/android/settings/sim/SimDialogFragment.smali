.class public abstract Lcom/android/settings/sim/SimDialogFragment;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;
.source "SimDialogFragment.java"

# interfaces
.implements Lcom/android/settings/network/SubscriptionsChangeListener$SubscriptionsChangeListenerClient;


# static fields
.field private static final KEY_DIALOG_TYPE:Ljava/lang/String; = "dialog_type"

.field private static final KEY_TITLE_ID:Ljava/lang/String; = "title_id"

.field private static final TAG:Ljava/lang/String; = "SimDialogFragment"


# instance fields
.field private mChangeListener:Lcom/android/settings/network/SubscriptionsChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method protected static initArguments(II)Landroid/os/Bundle;
    .locals 2
    .param p0, "dialogType"    # I
    .param p1, "titleResId"    # I

    .line 39
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "dialog_type"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    const-string v1, "title_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    return-object v0
.end method


# virtual methods
.method public getDialogType()I
    .locals 2

    .line 46
    invoke-virtual {p0}, Lcom/android/settings/sim/SimDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialog_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getTitleResId()I
    .locals 2

    .line 50
    invoke-virtual {p0}, Lcom/android/settings/sim/SimDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onAirplaneModeChanged(Z)V
    .locals 0
    .param p1, "airplaneModeEnabled"    # Z

    .line 84
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 55
    invoke-super {p0, p1}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;->onAttach(Landroid/content/Context;)V

    .line 56
    new-instance v0, Lcom/android/settings/network/SubscriptionsChangeListener;

    invoke-direct {v0, p1, p0}, Lcom/android/settings/network/SubscriptionsChangeListener;-><init>(Landroid/content/Context;Lcom/android/settings/network/SubscriptionsChangeListener$SubscriptionsChangeListenerClient;)V

    iput-object v0, p0, Lcom/android/settings/sim/SimDialogFragment;->mChangeListener:Lcom/android/settings/network/SubscriptionsChangeListener;

    .line 57
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 73
    invoke-super {p0, p1}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 74
    invoke-virtual {p0}, Lcom/android/settings/sim/SimDialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sim/SimDialogActivity;

    .line 75
    .local v0, "activity":Lcom/android/settings/sim/SimDialogActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/sim/SimDialogActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    invoke-virtual {v0, p0}, Lcom/android/settings/sim/SimDialogActivity;->onFragmentDismissed(Lcom/android/settings/sim/SimDialogFragment;)V

    .line 78
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .line 61
    invoke-super {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;->onPause()V

    .line 62
    iget-object v0, p0, Lcom/android/settings/sim/SimDialogFragment;->mChangeListener:Lcom/android/settings/network/SubscriptionsChangeListener;

    invoke-virtual {v0}, Lcom/android/settings/network/SubscriptionsChangeListener;->stop()V

    .line 63
    return-void
.end method

.method public onResume()V
    .locals 1

    .line 67
    invoke-super {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;->onResume()V

    .line 68
    iget-object v0, p0, Lcom/android/settings/sim/SimDialogFragment;->mChangeListener:Lcom/android/settings/network/SubscriptionsChangeListener;

    invoke-virtual {v0}, Lcom/android/settings/network/SubscriptionsChangeListener;->start()V

    .line 69
    return-void
.end method

.method public onSubscriptionsChanged()V
    .locals 0

    .line 88
    invoke-virtual {p0}, Lcom/android/settings/sim/SimDialogFragment;->updateDialog()V

    .line 89
    return-void
.end method

.method public abstract updateDialog()V
.end method
