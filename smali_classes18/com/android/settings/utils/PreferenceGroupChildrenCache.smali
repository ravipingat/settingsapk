.class public Lcom/android/settings/utils/PreferenceGroupChildrenCache;
.super Ljava/lang/Object;
.source "PreferenceGroupChildrenCache.java"


# instance fields
.field private mPreferenceCache:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroidx/preference/Preference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cacheRemoveAllPrefs(Landroidx/preference/PreferenceGroup;)V
    .locals 5
    .param p1, "group"    # Landroidx/preference/PreferenceGroup;

    .line 34
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/utils/PreferenceGroupChildrenCache;->mPreferenceCache:Landroid/util/ArrayMap;

    .line 35
    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    .line 36
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 37
    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->getPreference(I)Landroidx/preference/Preference;

    move-result-object v2

    .line 38
    .local v2, "p":Landroidx/preference/Preference;
    invoke-virtual {v2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 39
    goto :goto_1

    .line 41
    :cond_0
    iget-object v3, p0, Lcom/android/settings/utils/PreferenceGroupChildrenCache;->mPreferenceCache:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    .end local v2    # "p":Landroidx/preference/Preference;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public getCachedCount()I
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/android/settings/utils/PreferenceGroupChildrenCache;->mPreferenceCache:Landroid/util/ArrayMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getCachedPreference(Ljava/lang/String;)Landroidx/preference/Preference;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/android/settings/utils/PreferenceGroupChildrenCache;->mPreferenceCache:Landroid/util/ArrayMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/preference/Preference;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public removeCachedPrefs(Landroidx/preference/PreferenceGroup;)V
    .locals 2
    .param p1, "group"    # Landroidx/preference/PreferenceGroup;

    .line 46
    iget-object v0, p0, Lcom/android/settings/utils/PreferenceGroupChildrenCache;->mPreferenceCache:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/preference/Preference;

    .line 47
    .local v1, "p":Landroidx/preference/Preference;
    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    .line 48
    .end local v1    # "p":Landroidx/preference/Preference;
    goto :goto_0

    .line 49
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/utils/PreferenceGroupChildrenCache;->mPreferenceCache:Landroid/util/ArrayMap;

    .line 50
    return-void
.end method
