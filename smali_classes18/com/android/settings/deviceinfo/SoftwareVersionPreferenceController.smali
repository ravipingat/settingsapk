.class public Lcom/android/settings/deviceinfo/SoftwareVersionPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "SoftwareVersionPreferenceController.java"


# static fields
.field private static final PREF_KEY:Ljava/lang/String; = "software_version"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 43
    const-string v0, "software_version"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 2
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 48
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 49
    const-string v0, "software_version"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    .line 50
    .local v0, "preference":Landroidx/preference/Preference;
    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/SoftwareVersionPreferenceController;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    .line 51
    .local v1, "version":Ljava/lang/CharSequence;
    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/android/settings/deviceinfo/SoftwareVersionPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/Utils;->isSupportCTPA(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 71
    const-string v0, "software_version"

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/android/settings/deviceinfo/SoftwareVersionPreferenceController;->mContext:Landroid/content/Context;

    const v1, 0x7f1205cb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "summary":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/settings/deviceinfo/SoftwareVersionPreferenceController;->mContext:Landroid/content/Context;

    const-string v2, "ext_meta_software_version"

    invoke-static {v1, v2}, Lcom/android/settings/Utils;->getString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "softwareVersion":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    move-object v0, v1

    .line 61
    :cond_0
    return-object v0
.end method
