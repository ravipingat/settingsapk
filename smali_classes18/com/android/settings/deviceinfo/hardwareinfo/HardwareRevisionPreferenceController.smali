.class public Lcom/android/settings/deviceinfo/hardwareinfo/HardwareRevisionPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "HardwareRevisionPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public copy()V
    .locals 4

    .line 56
    iget-object v0, p0, Lcom/android/settings/deviceinfo/hardwareinfo/HardwareRevisionPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/hardwareinfo/HardwareRevisionPreferenceController;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/deviceinfo/hardwareinfo/HardwareRevisionPreferenceController;->mContext:Landroid/content/Context;

    .line 57
    const v3, 0x7f12078c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 56
    invoke-static {v0, v1, v2}, Lcom/android/settings/slices/Sliceable;->setCopyContent(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 58
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/android/settings/deviceinfo/hardwareinfo/HardwareRevisionPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    .line 35
    :goto_0
    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/android/settings/deviceinfo/hardwareinfo/HardwareRevisionPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/Utils;->isSupportCTPA(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/android/settings/deviceinfo/hardwareinfo/HardwareRevisionPreferenceController;->mContext:Landroid/content/Context;

    const-string v1, "ext_hardware_version"

    invoke-static {v0, v1}, Lcom/android/settings/Utils;->getString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "hardwareVersion":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    return-object v0

    .line 68
    .end local v0    # "hardwareVersion":Ljava/lang/String;
    :cond_0
    const-string v0, "ro.boot.hardware.revision"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isCopyableSlice()Z
    .locals 1

    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public isSliceable()Z
    .locals 1

    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public useDynamicSliceSummary()Z
    .locals 1

    .line 41
    const/4 v0, 0x1

    return v0
.end method
