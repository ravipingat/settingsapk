.class public Lcom/android/settings/deviceinfo/hardwareinfo/SerialNumberPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "SerialNumberPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method public copy()V
    .locals 4

    .line 55
    iget-object v0, p0, Lcom/android/settings/deviceinfo/hardwareinfo/SerialNumberPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/hardwareinfo/SerialNumberPreferenceController;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/deviceinfo/hardwareinfo/SerialNumberPreferenceController;->mContext:Landroid/content/Context;

    .line 56
    const v3, 0x7f12143e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 55
    invoke-static {v0, v1, v2}, Lcom/android/settings/slices/Sliceable;->setCopyContent(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/android/settings/deviceinfo/hardwareinfo/SerialNumberPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    .line 34
    :goto_0
    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    .line 61
    invoke-static {}, Landroid/os/Build;->getSerial()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isCopyableSlice()Z
    .locals 1

    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public isSliceable()Z
    .locals 1

    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public useDynamicSliceSummary()Z
    .locals 1

    .line 50
    const/4 v0, 0x1

    return v0
.end method
