.class public Lcom/android/settings/deviceinfo/hardwareinfo/DeviceModelPreferenceController;
.super Lcom/android/settings/deviceinfo/HardwareInfoPreferenceController;
.source "DeviceModelPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/android/settings/deviceinfo/HardwareInfoPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 1

    .line 36
    const/4 v0, 0x3

    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    .line 46
    invoke-static {}, Lcom/android/settings/deviceinfo/HardwareInfoPreferenceController;->getDeviceModel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSliceable()Z
    .locals 1

    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public useDynamicSliceSummary()Z
    .locals 1

    .line 51
    const/4 v0, 0x1

    return v0
.end method
