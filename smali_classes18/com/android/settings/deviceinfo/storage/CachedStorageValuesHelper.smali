.class public Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;
.super Ljava/lang/Object;
.source "CachedStorageValuesHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;
    }
.end annotation


# static fields
.field public static final CACHE_APPS_SIZE_KEY:Ljava/lang/String; = "cache_apps_size"

.field public static final EXTERNAL_APP_BYTES:Ljava/lang/String; = "external_apps_bytes"

.field public static final EXTERNAL_AUDIO_BYTES:Ljava/lang/String; = "external_audio_bytes"

.field public static final EXTERNAL_IMAGE_BYTES:Ljava/lang/String; = "external_image_bytes"

.field public static final EXTERNAL_TOTAL_BYTES:Ljava/lang/String; = "external_total_bytes"

.field public static final EXTERNAL_VIDEO_BYTES:Ljava/lang/String; = "external_video_bytes"

.field public static final FREE_BYTES_KEY:Ljava/lang/String; = "free_bytes"

.field public static final GAME_APPS_SIZE_KEY:Ljava/lang/String; = "game_apps_size"

.field public static final MUSIC_APPS_SIZE_KEY:Ljava/lang/String; = "music_apps_size"

.field public static final OTHER_APPS_SIZE_KEY:Ljava/lang/String; = "other_apps_size"

.field public static final PHOTO_APPS_SIZE_KEY:Ljava/lang/String; = "photo_apps_size"

.field public static final SHARED_PREFERENCES_NAME:Ljava/lang/String; = "CachedStorageValues"
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public static final TIMESTAMP_KEY:Ljava/lang/String; = "last_query_timestamp"

.field public static final TOTAL_BYTES_KEY:Ljava/lang/String; = "total_bytes"

.field public static final USER_ID_KEY:Ljava/lang/String; = "user_id"

.field public static final VIDEO_APPS_SIZE_KEY:Ljava/lang/String; = "video_apps_size"


# instance fields
.field private final mClobberThreshold:Ljava/lang/Long;

.field protected mClock:Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;

.field private final mUserId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # I

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    nop

    .line 58
    const-string v0, "CachedStorageValues"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 59
    new-instance v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;

    invoke-direct {v0}, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;-><init>()V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mClock:Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;

    .line 60
    iput p2, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mUserId:I

    .line 61
    nop

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    .line 65
    const-wide/16 v2, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    .line 62
    const-string v3, "storage_settings_clobber_threshold"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mClobberThreshold:Ljava/lang/Long;

    .line 66
    return-void
.end method

.method private isDataValid()Z
    .locals 11

    .line 157
    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "user_id"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 158
    .local v0, "cachedUserId":I
    iget v1, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mUserId:I

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    .line 159
    return v2

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-wide v3, 0x7fffffffffffffffL

    const-string v5, "last_query_timestamp"

    invoke-interface {v1, v5, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 163
    .local v3, "lastQueryTime":J
    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mClock:Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;

    invoke-virtual {v1}, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;->getCurrentTime()J

    move-result-wide v5

    .line 164
    .local v5, "currentTime":J
    sub-long v7, v5, v3

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mClobberThreshold:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v1, v7, v9

    if-gez v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method


# virtual methods
.method public cacheResult(Lcom/android/settingslib/deviceinfo/PrivateStorageInfo;Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;)V
    .locals 4
    .param p1, "storageInfo"    # Lcom/android/settingslib/deviceinfo/PrivateStorageInfo;
    .param p2, "result"    # Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;

    .line 136
    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 137
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p1, Lcom/android/settingslib/deviceinfo/PrivateStorageInfo;->freeBytes:J

    .line 138
    const-string v3, "free_bytes"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p1, Lcom/android/settingslib/deviceinfo/PrivateStorageInfo;->totalBytes:J

    .line 139
    const-string v3, "total_bytes"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->gamesSize:J

    .line 140
    const-string v3, "game_apps_size"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->musicAppsSize:J

    .line 141
    const-string v3, "music_apps_size"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->videoAppsSize:J

    .line 142
    const-string v3, "video_apps_size"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->photosAppsSize:J

    .line 143
    const-string v3, "photo_apps_size"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->otherAppsSize:J

    .line 144
    const-string v3, "other_apps_size"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->cacheSize:J

    .line 145
    const-string v3, "cache_apps_size"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->externalStats:Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;

    iget-wide v1, v1, Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;->totalBytes:J

    .line 146
    const-string v3, "external_total_bytes"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->externalStats:Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;

    iget-wide v1, v1, Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;->audioBytes:J

    .line 147
    const-string v3, "external_audio_bytes"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->externalStats:Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;

    iget-wide v1, v1, Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;->videoBytes:J

    .line 148
    const-string v3, "external_video_bytes"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->externalStats:Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;

    iget-wide v1, v1, Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;->imageBytes:J

    .line 149
    const-string v3, "external_image_bytes"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p2, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->externalStats:Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;

    iget-wide v1, v1, Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;->appBytes:J

    .line 150
    const-string v3, "external_apps_bytes"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mUserId:I

    .line 151
    const-string v2, "user_id"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mClock:Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;

    .line 152
    invoke-virtual {v1}, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper$Clock;->getCurrentTime()J

    move-result-wide v1

    const-string v3, "last_query_timestamp"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 153
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 154
    return-void
.end method

.method public getCachedAppsStorageResult()Landroid/util/SparseArray;
    .locals 38
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;",
            ">;"
        }
    .end annotation

    .line 82
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->isDataValid()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 83
    return-object v2

    .line 85
    :cond_0
    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-wide/16 v3, -0x1

    const-string v5, "game_apps_size"

    invoke-interface {v1, v5, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 86
    .local v5, "gamesSize":J
    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "music_apps_size"

    invoke-interface {v1, v7, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    .line 87
    .local v7, "musicAppsSize":J
    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "video_apps_size"

    invoke-interface {v1, v9, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    .line 88
    .local v9, "videoAppsSize":J
    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v11, "photo_apps_size"

    invoke-interface {v1, v11, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    .line 89
    .local v11, "photoAppSize":J
    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v13, "other_apps_size"

    invoke-interface {v1, v13, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    .line 90
    .local v13, "otherAppsSize":J
    iget-object v1, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v15, "cache_apps_size"

    invoke-interface {v1, v15, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    .line 91
    .local v1, "cacheSize":J
    const-wide/16 v17, 0x0

    cmp-long v15, v5, v17

    if-ltz v15, :cond_4

    cmp-long v15, v7, v17

    if-ltz v15, :cond_4

    cmp-long v15, v9, v17

    if-ltz v15, :cond_4

    cmp-long v15, v11, v17

    if-ltz v15, :cond_4

    cmp-long v15, v13, v17

    if-ltz v15, :cond_4

    cmp-long v15, v1, v17

    if-gez v15, :cond_1

    move-wide/from16 v20, v1

    move-wide/from16 v36, v9

    move-wide/from16 v34, v11

    move-wide/from16 v32, v13

    goto/16 :goto_1

    .line 100
    :cond_1
    iget-object v15, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-wide/from16 v19, v1

    .end local v1    # "cacheSize":J
    .local v19, "cacheSize":J
    const-string v1, "external_total_bytes"

    invoke-interface {v15, v1, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    .line 101
    .local v1, "externalTotalBytes":J
    iget-object v15, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-wide/from16 v32, v13

    .end local v13    # "otherAppsSize":J
    .local v32, "otherAppsSize":J
    const-string v13, "external_audio_bytes"

    invoke-interface {v15, v13, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    .line 102
    .local v13, "externalAudioBytes":J
    iget-object v15, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-wide/from16 v34, v11

    .end local v11    # "photoAppSize":J
    .local v34, "photoAppSize":J
    const-string v11, "external_video_bytes"

    invoke-interface {v15, v11, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    .line 103
    .local v11, "externalVideoBytes":J
    iget-object v15, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    move-wide/from16 v36, v9

    .end local v9    # "videoAppsSize":J
    .local v36, "videoAppsSize":J
    const-string v9, "external_image_bytes"

    invoke-interface {v15, v9, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    .line 104
    .local v9, "externalImageBytes":J
    iget-object v15, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v0, "external_apps_bytes"

    invoke-interface {v15, v0, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 105
    .local v3, "externalAppBytes":J
    cmp-long v0, v1, v17

    if-ltz v0, :cond_3

    cmp-long v0, v13, v17

    if-ltz v0, :cond_3

    cmp-long v0, v11, v17

    if-ltz v0, :cond_3

    cmp-long v0, v9, v17

    if-ltz v0, :cond_3

    cmp-long v0, v3, v17

    if-gez v0, :cond_2

    move-object/from16 v0, p0

    move-wide/from16 v17, v1

    move-wide/from16 v20, v19

    goto :goto_0

    .line 113
    :cond_2
    new-instance v0, Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;

    move-object/from16 v21, v0

    move-wide/from16 v22, v1

    move-wide/from16 v24, v13

    move-wide/from16 v26, v11

    move-wide/from16 v28, v9

    move-wide/from16 v30, v3

    invoke-direct/range {v21 .. v31}, Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;-><init>(JJJJJ)V

    .line 120
    .local v0, "externalStats":Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;
    new-instance v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;

    invoke-direct {v15}, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;-><init>()V

    .line 122
    .local v15, "result":Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;
    iput-wide v5, v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->gamesSize:J

    .line 123
    iput-wide v7, v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->musicAppsSize:J

    .line 124
    move-wide/from16 v17, v1

    move-wide/from16 v1, v36

    .end local v36    # "videoAppsSize":J
    .local v1, "videoAppsSize":J
    .local v17, "externalTotalBytes":J
    iput-wide v1, v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->videoAppsSize:J

    .line 125
    move-wide/from16 v1, v34

    .end local v34    # "photoAppSize":J
    .local v1, "photoAppSize":J
    .restart local v36    # "videoAppsSize":J
    iput-wide v1, v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->photosAppsSize:J

    .line 126
    move-wide/from16 v1, v32

    .end local v32    # "otherAppsSize":J
    .local v1, "otherAppsSize":J
    .restart local v34    # "photoAppSize":J
    iput-wide v1, v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->otherAppsSize:J

    .line 127
    move-wide/from16 v1, v19

    .end local v19    # "cacheSize":J
    .local v1, "cacheSize":J
    .restart local v32    # "otherAppsSize":J
    iput-wide v1, v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->cacheSize:J

    .line 128
    iput-object v0, v15, Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;->externalStats:Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;

    .line 129
    new-instance v16, Landroid/util/SparseArray;

    invoke-direct/range {v16 .. v16}, Landroid/util/SparseArray;-><init>()V

    move-object/from16 v19, v16

    .line 130
    .local v19, "resultArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;>;"
    move-object/from16 v16, v0

    move-wide/from16 v20, v1

    move-object/from16 v0, p0

    .end local v0    # "externalStats":Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;
    .end local v1    # "cacheSize":J
    .local v16, "externalStats":Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;
    .local v20, "cacheSize":J
    iget v1, v0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mUserId:I

    move-object/from16 v2, v19

    .end local v19    # "resultArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;>;"
    .local v2, "resultArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;>;"
    invoke-virtual {v2, v1, v15}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 131
    return-object v2

    .line 105
    .end local v2    # "resultArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;>;"
    .end local v15    # "result":Lcom/android/settings/deviceinfo/storage/StorageAsyncLoader$AppsStorageResult;
    .end local v16    # "externalStats":Lcom/android/settingslib/applications/StorageStatsSource$ExternalStorageStats;
    .end local v17    # "externalTotalBytes":J
    .end local v20    # "cacheSize":J
    .local v1, "externalTotalBytes":J
    .local v19, "cacheSize":J
    :cond_3
    move-object/from16 v0, p0

    move-wide/from16 v17, v1

    move-wide/from16 v20, v19

    .line 110
    .end local v1    # "externalTotalBytes":J
    .end local v19    # "cacheSize":J
    .restart local v17    # "externalTotalBytes":J
    .restart local v20    # "cacheSize":J
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 91
    .end local v3    # "externalAppBytes":J
    .end local v17    # "externalTotalBytes":J
    .end local v20    # "cacheSize":J
    .end local v32    # "otherAppsSize":J
    .end local v34    # "photoAppSize":J
    .end local v36    # "videoAppsSize":J
    .local v1, "cacheSize":J
    .local v9, "videoAppsSize":J
    .local v11, "photoAppSize":J
    .local v13, "otherAppsSize":J
    :cond_4
    move-wide/from16 v20, v1

    move-wide/from16 v36, v9

    move-wide/from16 v34, v11

    move-wide/from16 v32, v13

    .line 97
    .end local v1    # "cacheSize":J
    .end local v9    # "videoAppsSize":J
    .end local v11    # "photoAppSize":J
    .end local v13    # "otherAppsSize":J
    .restart local v20    # "cacheSize":J
    .restart local v32    # "otherAppsSize":J
    .restart local v34    # "photoAppSize":J
    .restart local v36    # "videoAppsSize":J
    :goto_1
    const/4 v1, 0x0

    return-object v1
.end method

.method public getCachedPrivateStorageInfo()Lcom/android/settingslib/deviceinfo/PrivateStorageInfo;
    .locals 8

    .line 69
    invoke-direct {p0}, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->isDataValid()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 70
    return-object v1

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-wide/16 v2, -0x1

    const-string v4, "free_bytes"

    invoke-interface {v0, v4, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 73
    .local v4, "freeBytes":J
    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/CachedStorageValuesHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v6, "total_bytes"

    invoke-interface {v0, v6, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 74
    .local v2, "totalBytes":J
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    cmp-long v0, v2, v6

    if-gez v0, :cond_1

    goto :goto_0

    .line 78
    :cond_1
    new-instance v0, Lcom/android/settingslib/deviceinfo/PrivateStorageInfo;

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/android/settingslib/deviceinfo/PrivateStorageInfo;-><init>(JJ)V

    return-object v0

    .line 75
    :cond_2
    :goto_0
    return-object v1
.end method
