.class public final Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ContextualCardProto.java"

# interfaces
.implements Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder<",
        "Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;",
        "Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;",
        ">;",
        "Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardListOrBuilder;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 260
    invoke-static {}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$000()Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 261
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/intelligence/ContextualCardProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/settings/intelligence/ContextualCardProto$1;

    .line 253
    invoke-direct {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public addAllCard(Ljava/lang/Iterable;)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;",
            ">;)",
            "Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;"
        }
    .end annotation

    .line 340
    .local p1, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;>;"
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 341
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$700(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;Ljava/lang/Iterable;)V

    .line 342
    return-object p0
.end method

.method public addCard(ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .param p1, "index"    # I
    .param p2, "builderForValue"    # Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;

    .line 331
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 332
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1, p2}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$600(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;)V

    .line 333
    return-object p0
.end method

.method public addCard(ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard;)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;

    .line 313
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 314
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1, p2}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$400(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard;)V

    .line 315
    return-object p0
.end method

.method public addCard(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .param p1, "builderForValue"    # Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;

    .line 322
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 323
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$500(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;)V

    .line 324
    return-object p0
.end method

.method public addCard(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .param p1, "value"    # Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;

    .line 304
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 305
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$300(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;)V

    .line 306
    return-object p0
.end method

.method public clearCard()Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1

    .line 348
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 349
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$800(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;)V

    .line 350
    return-object p0
.end method

.method public getCard(I)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;
    .locals 1
    .param p1, "index"    # I

    .line 280
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-virtual {v0, p1}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->getCard(I)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;

    move-result-object v0

    return-object v0
.end method

.method public getCardCount()I
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->getCardCount()I

    move-result v0

    return v0
.end method

.method public getCardList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;",
            ">;"
        }
    .end annotation

    .line 268
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    .line 269
    invoke-virtual {v0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->getCardList()Ljava/util/List;

    move-result-object v0

    .line 268
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public removeCard(I)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .param p1, "index"    # I

    .line 356
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 357
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$900(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;I)V

    .line 358
    return-object p0
.end method

.method public setCard(ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .param p1, "index"    # I
    .param p2, "builderForValue"    # Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;

    .line 296
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 297
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1, p2}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$200(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard$Builder;)V

    .line 298
    return-object p0
.end method

.method public setCard(ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard;)Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;
    .locals 1
    .param p1, "index"    # I
    .param p2, "value"    # Lcom/android/settings/intelligence/ContextualCardProto$ContextualCard;

    .line 287
    invoke-virtual {p0}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->copyOnWrite()V

    .line 288
    iget-object v0, p0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;

    invoke-static {v0, p1, p2}, Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;->access$100(Lcom/android/settings/intelligence/ContextualCardProto$ContextualCardList;ILcom/android/settings/intelligence/ContextualCardProto$ContextualCard;)V

    .line 289
    return-object p0
.end method
