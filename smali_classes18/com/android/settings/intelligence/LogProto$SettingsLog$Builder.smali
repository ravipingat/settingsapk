.class public final Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "LogProto.java"

# interfaces
.implements Lcom/android/settings/intelligence/LogProto$SettingsLogOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/intelligence/LogProto$SettingsLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder<",
        "Lcom/android/settings/intelligence/LogProto$SettingsLog;",
        "Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;",
        ">;",
        "Lcom/android/settings/intelligence/LogProto$SettingsLogOrBuilder;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 665
    invoke-static {}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$000()Lcom/android/settings/intelligence/LogProto$SettingsLog;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 666
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/intelligence/LogProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/settings/intelligence/LogProto$1;

    .line 658
    invoke-direct {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public clearAction()Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1

    .line 766
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 767
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$400(Lcom/android/settings/intelligence/LogProto$SettingsLog;)V

    .line 768
    return-object p0
.end method

.method public clearAttribution()Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1

    .line 717
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 718
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$200(Lcom/android/settings/intelligence/LogProto$SettingsLog;)V

    .line 719
    return-object p0
.end method

.method public clearChangedPreferenceIntValue()Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1

    .line 940
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 941
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$1100(Lcom/android/settings/intelligence/LogProto$SettingsLog;)V

    .line 942
    return-object p0
.end method

.method public clearChangedPreferenceKey()Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1

    .line 877
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 878
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$800(Lcom/android/settings/intelligence/LogProto$SettingsLog;)V

    .line 879
    return-object p0
.end method

.method public clearPageId()Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1

    .line 815
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 816
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$600(Lcom/android/settings/intelligence/LogProto$SettingsLog;)V

    .line 817
    return-object p0
.end method

.method public clearTimestamp()Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1

    .line 1002
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 1003
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$1300(Lcom/android/settings/intelligence/LogProto$SettingsLog;)V

    .line 1004
    return-object p0
.end method

.method public getAction()I
    .locals 1

    .line 742
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getAction()I

    move-result v0

    return v0
.end method

.method public getAttribution()I
    .locals 1

    .line 691
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getAttribution()I

    move-result v0

    return v0
.end method

.method public getChangedPreferenceIntValue()I
    .locals 1

    .line 916
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getChangedPreferenceIntValue()I

    move-result v0

    return v0
.end method

.method public getChangedPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 840
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getChangedPreferenceKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChangedPreferenceKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 852
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getChangedPreferenceKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getPageId()I
    .locals 1

    .line 791
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getPageId()I

    move-result v0

    return v0
.end method

.method public getTimestamp()Ljava/lang/String;
    .locals 1

    .line 965
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getTimestamp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimestampBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 977
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->getTimestampBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public hasAction()Z
    .locals 1

    .line 731
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->hasAction()Z

    move-result v0

    return v0
.end method

.method public hasAttribution()Z
    .locals 1

    .line 679
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->hasAttribution()Z

    move-result v0

    return v0
.end method

.method public hasChangedPreferenceIntValue()Z
    .locals 1

    .line 905
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->hasChangedPreferenceIntValue()Z

    move-result v0

    return v0
.end method

.method public hasChangedPreferenceKey()Z
    .locals 1

    .line 829
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->hasChangedPreferenceKey()Z

    move-result v0

    return v0
.end method

.method public hasPageId()Z
    .locals 1

    .line 780
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->hasPageId()Z

    move-result v0

    return v0
.end method

.method public hasTimestamp()Z
    .locals 1

    .line 954
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-virtual {v0}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->hasTimestamp()Z

    move-result v0

    return v0
.end method

.method public setAction(I)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # I

    .line 753
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 754
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$300(Lcom/android/settings/intelligence/LogProto$SettingsLog;I)V

    .line 755
    return-object p0
.end method

.method public setAttribution(I)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # I

    .line 703
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 704
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$100(Lcom/android/settings/intelligence/LogProto$SettingsLog;I)V

    .line 705
    return-object p0
.end method

.method public setChangedPreferenceIntValue(I)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # I

    .line 927
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 928
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$1000(Lcom/android/settings/intelligence/LogProto$SettingsLog;I)V

    .line 929
    return-object p0
.end method

.method public setChangedPreferenceKey(Ljava/lang/String;)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .line 864
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 865
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$700(Lcom/android/settings/intelligence/LogProto$SettingsLog;Ljava/lang/String;)V

    .line 866
    return-object p0
.end method

.method public setChangedPreferenceKeyBytes(Lcom/google/protobuf/ByteString;)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # Lcom/google/protobuf/ByteString;

    .line 891
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 892
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$900(Lcom/android/settings/intelligence/LogProto$SettingsLog;Lcom/google/protobuf/ByteString;)V

    .line 893
    return-object p0
.end method

.method public setPageId(I)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # I

    .line 802
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 803
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$500(Lcom/android/settings/intelligence/LogProto$SettingsLog;I)V

    .line 804
    return-object p0
.end method

.method public setTimestamp(Ljava/lang/String;)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .line 989
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 990
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$1200(Lcom/android/settings/intelligence/LogProto$SettingsLog;Ljava/lang/String;)V

    .line 991
    return-object p0
.end method

.method public setTimestampBytes(Lcom/google/protobuf/ByteString;)Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;
    .locals 1
    .param p1, "value"    # Lcom/google/protobuf/ByteString;

    .line 1016
    invoke-virtual {p0}, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->copyOnWrite()V

    .line 1017
    iget-object v0, p0, Lcom/android/settings/intelligence/LogProto$SettingsLog$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    check-cast v0, Lcom/android/settings/intelligence/LogProto$SettingsLog;

    invoke-static {v0, p1}, Lcom/android/settings/intelligence/LogProto$SettingsLog;->access$1400(Lcom/android/settings/intelligence/LogProto$SettingsLog;Lcom/google/protobuf/ByteString;)V

    .line 1018
    return-object p0
.end method
