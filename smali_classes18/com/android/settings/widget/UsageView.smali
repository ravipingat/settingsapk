.class public Lcom/android/settings/widget/UsageView;
.super Landroid/widget/FrameLayout;
.source "UsageView.java"


# instance fields
.field private final mBottomLabels:[Landroid/widget/TextView;

.field private final mLabels:[Landroid/widget/TextView;

.field private final mUsageGraph:Lcom/android/settings/widget/UsageGraph;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0330

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 41
    const v0, 0x7f0a0738

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/UsageGraph;

    iput-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    .line 42
    const/4 v0, 0x3

    new-array v1, v0, [Landroid/widget/TextView;

    .line 43
    const v2, 0x7f0a0352

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 44
    const v2, 0x7f0a0357

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 45
    const v2, 0x7f0a035a

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    iput-object v1, p0, Lcom/android/settings/widget/UsageView;->mLabels:[Landroid/widget/TextView;

    .line 47
    new-array v1, v5, [Landroid/widget/TextView;

    .line 48
    const v2, 0x7f0a0359

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v1, v3

    .line 49
    const v2, 0x7f0a0353

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v1, v4

    iput-object v1, p0, Lcom/android/settings/widget/UsageView;->mBottomLabels:[Landroid/widget/TextView;

    .line 51
    sget-object v1, Lcom/android/settingslib/R$styleable;->UsageView:[I

    invoke-virtual {p1, p2, v1, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 52
    .local v1, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/UsageView;->setSideLabels([Ljava/lang/CharSequence;)V

    .line 55
    :cond_0
    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/UsageView;->setBottomLabels([Ljava/lang/CharSequence;)V

    .line 58
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 59
    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 60
    .local v0, "color":I
    iget-object v2, p0, Lcom/android/settings/widget/UsageView;->mLabels:[Landroid/widget/TextView;

    array-length v5, v2

    move v6, v3

    :goto_0
    if-ge v6, v5, :cond_2

    aget-object v7, v2, v6

    .line 61
    .local v7, "v":Landroid/widget/TextView;
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 60
    .end local v7    # "v":Landroid/widget/TextView;
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 63
    :cond_2
    iget-object v2, p0, Lcom/android/settings/widget/UsageView;->mBottomLabels:[Landroid/widget/TextView;

    array-length v5, v2

    move v6, v3

    :goto_1
    if-ge v6, v5, :cond_3

    aget-object v7, v2, v6

    .line 64
    .restart local v7    # "v":Landroid/widget/TextView;
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    .end local v7    # "v":Landroid/widget/TextView;
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 67
    .end local v0    # "color":I
    :cond_3
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 68
    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 69
    .local v0, "gravity":I
    const v2, 0x800005

    if-ne v0, v2, :cond_4

    .line 70
    const v5, 0x7f0a02a7

    invoke-virtual {p0, v5}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 71
    .local v5, "layout":Landroid/widget/LinearLayout;
    const v6, 0x7f0a0355

    invoke-virtual {p0, v6}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 73
    .local v6, "labels":Landroid/widget/LinearLayout;
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 74
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 76
    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 78
    const v2, 0x7f0a00e8

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 79
    .local v2, "bottomLabels":Landroid/widget/LinearLayout;
    const v7, 0x7f0a00e9

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 80
    .local v7, "bottomSpace":Landroid/view/View;
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 81
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .end local v2    # "bottomLabels":Landroid/widget/LinearLayout;
    .end local v5    # "layout":Landroid/widget/LinearLayout;
    .end local v6    # "labels":Landroid/widget/LinearLayout;
    .end local v7    # "bottomSpace":Landroid/view/View;
    goto :goto_2

    .line 82
    :cond_4
    const v2, 0x800003

    if-ne v0, v2, :cond_5

    :goto_2
    goto :goto_3

    .line 83
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported gravity "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 86
    .end local v0    # "gravity":I
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/UsageGraph;->setAccentColor(I)V

    .line 87
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 88
    return-void
.end method

.method private setWeight(IF)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "weight"    # F

    .line 124
    invoke-virtual {p0, p1}, Lcom/android/settings/widget/UsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 125
    .local v0, "v":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 126
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    iput p2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 127
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    return-void
.end method


# virtual methods
.method public addPath(Landroid/util/SparseIntArray;)V
    .locals 1
    .param p1, "points"    # Landroid/util/SparseIntArray;

    .line 95
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/UsageGraph;->addPath(Landroid/util/SparseIntArray;)V

    .line 96
    return-void
.end method

.method public addProjectedPath(Landroid/util/SparseIntArray;)V
    .locals 1
    .param p1, "points"    # Landroid/util/SparseIntArray;

    .line 99
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/UsageGraph;->addProjectedPath(Landroid/util/SparseIntArray;)V

    .line 100
    return-void
.end method

.method public clearPaths()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v0}, Lcom/android/settings/widget/UsageGraph;->clearPaths()V

    .line 92
    return-void
.end method

.method public configureGraph(II)V
    .locals 1
    .param p1, "maxX"    # I
    .param p2, "maxY"    # I

    .line 103
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/widget/UsageGraph;->setMax(II)V

    .line 104
    return-void
.end method

.method public setAccentColor(I)V
    .locals 1
    .param p1, "color"    # I

    .line 107
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/UsageGraph;->setAccentColor(I)V

    .line 108
    return-void
.end method

.method public setBottomLabels([Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "labels"    # [Ljava/lang/CharSequence;

    .line 140
    array-length v0, p1

    iget-object v1, p0, Lcom/android/settings/widget/UsageView;->mBottomLabels:[Landroid/widget/TextView;

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 143
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/settings/widget/UsageView;->mBottomLabels:[Landroid/widget/TextView;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 144
    aget-object v1, v1, v0

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 141
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid number of labels"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDividerColors(II)V
    .locals 1
    .param p1, "middleColor"    # I
    .param p2, "topColor"    # I

    .line 115
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/widget/UsageGraph;->setDividerColors(II)V

    .line 116
    return-void
.end method

.method public setDividerLoc(I)V
    .locals 1
    .param p1, "dividerLoc"    # I

    .line 111
    iget-object v0, p0, Lcom/android/settings/widget/UsageView;->mUsageGraph:Lcom/android/settings/widget/UsageGraph;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/UsageGraph;->setDividerLoc(I)V

    .line 112
    return-void
.end method

.method public setSideLabelWeights(FF)V
    .locals 1
    .param p1, "before"    # F
    .param p2, "after"    # F

    .line 119
    const v0, 0x7f0a0644

    invoke-direct {p0, v0, p1}, Lcom/android/settings/widget/UsageView;->setWeight(IF)V

    .line 120
    const v0, 0x7f0a0645

    invoke-direct {p0, v0, p2}, Lcom/android/settings/widget/UsageView;->setWeight(IF)V

    .line 121
    return-void
.end method

.method public setSideLabels([Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "labels"    # [Ljava/lang/CharSequence;

    .line 131
    array-length v0, p1

    iget-object v1, p0, Lcom/android/settings/widget/UsageView;->mLabels:[Landroid/widget/TextView;

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 134
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/settings/widget/UsageView;->mLabels:[Landroid/widget/TextView;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 135
    aget-object v1, v1, v0

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 132
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid number of labels"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
