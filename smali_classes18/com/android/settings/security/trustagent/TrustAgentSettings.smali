.class public Lcom/android/settings/security/trustagent/TrustAgentSettings;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "TrustAgentSettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field private static final TAG:Ljava/lang/String; = "TrustAgentSettings"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    new-instance v0, Lcom/android/settings/security/trustagent/TrustAgentSettings$1;

    invoke-direct {v0}, Lcom/android/settings/security/trustagent/TrustAgentSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/security/trustagent/TrustAgentSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getHelpResource()I
    .locals 1

    .line 43
    const v0, 0x7f1207e6

    return v0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 48
    const-string v0, "TrustAgentSettings"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 38
    const/16 v0, 0x5b

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 53
    const v0, 0x7f1600fa

    return v0
.end method
