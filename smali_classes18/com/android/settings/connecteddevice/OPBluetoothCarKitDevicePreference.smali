.class public Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;
.super Lcom/oneplus/settings/ui/OPCarKitButtonPreference;
.source "OPBluetoothCarKitDevicePreference.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/CachedBluetoothDevice$Callback;


# static fields
.field private static sDimAlpha:I


# instance fields
.field private contentDescription:Ljava/lang/String;

.field private final mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field private mContext:Landroid/content/Context;

.field mResources:Landroid/content/res/Resources;

.field private final mShowDevicesWithoutNames:Z

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    const/high16 v0, -0x80000000

    sput v0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->sDimAlpha:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cachedDevice"    # Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .param p3, "showDeviceWithoutNames"    # Z

    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    iput-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->contentDescription:Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mContext:Landroid/content/Context;

    .line 35
    invoke-virtual {p0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mResources:Landroid/content/res/Resources;

    .line 36
    nop

    .line 37
    const-string v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mUserManager:Landroid/os/UserManager;

    .line 38
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 39
    iput-boolean p3, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mShowDevicesWithoutNames:Z

    .line 41
    sget v0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->sDimAlpha:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 42
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 43
    .local v0, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010033

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 45
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->sDimAlpha:I

    .line 48
    .end local v0    # "outValue":Landroid/util/TypedValue;
    :cond_0
    iput-object p2, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    .line 49
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->registerCallback(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice$Callback;)V

    .line 51
    invoke-virtual {p0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->onDeviceAttributesChanged()V

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;

    .line 20
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;

    .line 20
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;

    .line 20
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .line 159
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;

    if-nez v0, :cond_0

    goto :goto_0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-object v1, p1

    check-cast v1, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;

    iget-object v1, v1, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    .line 163
    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 162
    return v0

    .line 160
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getBluetoothDevice()Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    return-object v0
.end method

.method getCachedDevice()Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->hashCode()I

    move-result v0

    return v0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3
    .param p1, "view"    # Landroidx/preference/PreferenceViewHolder;

    .line 122
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getBondState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 123
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->isCarkit(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 125
    const v0, 0x7f0804fe

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setIcon(I)V

    .line 126
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mContext:Landroid/content/Context;

    const v2, 0x7f120e96

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setButtonString(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0, v1}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setButtonEnable(Z)V

    .line 128
    new-instance v0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference$1;-><init>(Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setOnButtonClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mContext:Landroid/content/Context;

    const v2, 0x7f120ba9

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setButtonString(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0, v1}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setButtonEnable(Z)V

    .line 142
    new-instance v0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference$2;

    invoke-direct {v0, p0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference$2;-><init>(Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setOnButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/oneplus/settings/ui/OPCarKitButtonPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 155
    return-void
.end method

.method public onDeviceAttributesChanged()V
    .locals 3

    .line 78
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setLeftTextTitle(Ljava/lang/String;)V

    .line 82
    nop

    .line 83
    invoke-virtual {p0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/BluetoothUtils;->getBtClassDrawableWithDescription(Landroid/content/Context;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Landroid/util/Pair;

    move-result-object v0

    .line 84
    .local v0, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/drawable/Drawable;Ljava/lang/String;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 86
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->contentDescription:Ljava/lang/String;

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isBusy()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setEnabled(Z)V

    .line 96
    iget-boolean v1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mShowDevicesWithoutNames:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    .line 97
    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->hasHumanReadableName()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    nop

    .line 96
    :goto_1
    invoke-virtual {p0, v2}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->setVisible(Z)V

    .line 100
    invoke-virtual {p0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->notifyHierarchyChanged()V

    .line 101
    return-void
.end method

.method public setButtonEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 116
    iput-boolean p1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mButtonEnable:Z

    .line 117
    return-void
.end method

.method public setButtonString(Ljava/lang/String;)V
    .locals 0
    .param p1, "buttonString"    # Ljava/lang/String;

    .line 106
    iput-object p1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mButtonString:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 111
    iput-object p1, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDevicePreference;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 112
    return-void
.end method
