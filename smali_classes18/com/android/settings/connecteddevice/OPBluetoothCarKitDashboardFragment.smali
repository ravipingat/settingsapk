.class public Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;
.super Lcom/android/settings/dashboard/DashboardFragment;
.source "OPBluetoothCarKitDashboardFragment.java"


# static fields
.field static final KEY_FOOTER_PREF:Ljava/lang/String; = "footer_preference"
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "OPBluetoothCarKitDashboardFragment"


# instance fields
.field mFooterPreference:Lcom/oneplus/settings/widget/OPFooterPreference;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getLogTag()Ljava/lang/String;
    .locals 1

    .line 52
    const-string v0, "OPBluetoothCarKitDashboardFragment"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    .line 65
    const/16 v0, 0x270f

    return v0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    .line 46
    const v0, 0x7f160086

    return v0
.end method

.method initPreferencesFromPreferenceScreen()V
    .locals 1

    .line 34
    const-string v0, "footer_preference"

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/oneplus/settings/widget/OPFooterPreference;

    iput-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->mFooterPreference:Lcom/oneplus/settings/widget/OPFooterPreference;

    .line 35
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->mFooterPreference:Lcom/oneplus/settings/widget/OPFooterPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->updateFooterPreference(Landroidx/preference/Preference;)V

    .line 36
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 57
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onAttach(Landroid/content/Context;)V

    .line 58
    const-class v0, Lcom/android/settings/connecteddevice/OPRecognizedBluetoothCarKitsDeviceGroupController;

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->use(Ljava/lang/Class;)Lcom/android/settingslib/core/AbstractPreferenceController;

    move-result-object v0

    check-cast v0, Lcom/android/settings/connecteddevice/OPRecognizedBluetoothCarKitsDeviceGroupController;

    invoke-virtual {v0, p0}, Lcom/android/settings/connecteddevice/OPRecognizedBluetoothCarKitsDeviceGroupController;->init(Lcom/android/settings/dashboard/DashboardFragment;)V

    .line 59
    const-class v0, Lcom/android/settings/connecteddevice/OPOtherPairedBluetoothDevicesGroupController;

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->use(Ljava/lang/Class;)Lcom/android/settingslib/core/AbstractPreferenceController;

    move-result-object v0

    check-cast v0, Lcom/android/settings/connecteddevice/OPOtherPairedBluetoothDevicesGroupController;

    invoke-virtual {v0, p0}, Lcom/android/settings/connecteddevice/OPOtherPairedBluetoothDevicesGroupController;->init(Lcom/android/settings/dashboard/DashboardFragment;)V

    .line 60
    const-class v0, Lcom/android/settings/connecteddevice/OPRecognizedBluetoothCarKitNoDevicesPreferenceController;

    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->use(Ljava/lang/Class;)Lcom/android/settingslib/core/AbstractPreferenceController;

    move-result-object v0

    check-cast v0, Lcom/android/settings/connecteddevice/OPRecognizedBluetoothCarKitNoDevicesPreferenceController;

    invoke-virtual {v0, p0}, Lcom/android/settings/connecteddevice/OPRecognizedBluetoothCarKitNoDevicesPreferenceController;->init(Lcom/android/settings/dashboard/DashboardFragment;)V

    .line 61
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 25
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->initPreferencesFromPreferenceScreen()V

    .line 27
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "need_search_icon_in_action_bar"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 29
    invoke-virtual {p0, v0}, Lcom/android/settings/connecteddevice/OPBluetoothCarKitDashboardFragment;->setArguments(Landroid/os/Bundle;)V

    .line 30
    invoke-static {p0}, Lcom/android/settings/search/actionbar/SearchMenuController;->init(Lcom/android/settings/core/InstrumentedPreferenceFragment;)V

    .line 31
    return-void
.end method

.method updateFooterPreference(Landroidx/preference/Preference;)V
    .locals 1
    .param p1, "mPreference"    # Landroidx/preference/Preference;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .line 40
    const v0, 0x7f120bab

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(I)V

    .line 41
    return-void
.end method
