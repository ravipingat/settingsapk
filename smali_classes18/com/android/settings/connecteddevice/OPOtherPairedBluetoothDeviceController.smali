.class public Lcom/android/settings/connecteddevice/OPOtherPairedBluetoothDeviceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPOtherPairedBluetoothDeviceController.java"


# static fields
.field private static final KEY:Ljava/lang/String; = "other_paired_bluetooth_devices"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferenceKey"    # Ljava/lang/String;

    .line 13
    const-string v0, "other_paired_bluetooth_devices"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/android/settings/connecteddevice/OPOtherPairedBluetoothDeviceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x0

    goto :goto_0

    .line 22
    :cond_0
    const/4 v0, 0x3

    .line 20
    :goto_0
    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 27
    const-string v0, "other_paired_bluetooth_devices"

    return-object v0
.end method
