.class public Lcom/android/settings/core/instrumentation/SettingsEventLogWriter;
.super Lcom/android/settingslib/core/instrumentation/EventLogWriter;
.source "SettingsEventLogWriter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/android/settingslib/core/instrumentation/EventLogWriter;-><init>()V

    return-void
.end method

.method private static shouldDisableGenericEventLogging()Z
    .locals 3

    .line 68
    const/4 v0, 0x1

    const-string v1, "settings_ui"

    const-string v2, "event_logging_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public action(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # I
    .param p3, "value"    # I

    .line 53
    invoke-static {}, Lcom/android/settings/core/instrumentation/SettingsEventLogWriter;->shouldDisableGenericEventLogging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    return-void

    .line 56
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settingslib/core/instrumentation/EventLogWriter;->action(Landroid/content/Context;II)V

    .line 57
    return-void
.end method

.method public action(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # I
    .param p3, "pkg"    # Ljava/lang/String;

    .line 45
    invoke-static {}, Lcom/android/settings/core/instrumentation/SettingsEventLogWriter;->shouldDisableGenericEventLogging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    return-void

    .line 48
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settingslib/core/instrumentation/EventLogWriter;->action(Landroid/content/Context;ILjava/lang/String;)V

    .line 49
    return-void
.end method

.method public action(Landroid/content/Context;IZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # I
    .param p3, "value"    # Z

    .line 61
    invoke-static {}, Lcom/android/settings/core/instrumentation/SettingsEventLogWriter;->shouldDisableGenericEventLogging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    return-void

    .line 64
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settingslib/core/instrumentation/EventLogWriter;->action(Landroid/content/Context;IZ)V

    .line 65
    return-void
.end method

.method public hidden(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "category"    # I

    .line 37
    invoke-static {}, Lcom/android/settings/core/instrumentation/SettingsEventLogWriter;->shouldDisableGenericEventLogging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    return-void

    .line 40
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/settingslib/core/instrumentation/EventLogWriter;->hidden(Landroid/content/Context;I)V

    .line 41
    return-void
.end method

.method public visible(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # I
    .param p3, "category"    # I

    .line 29
    invoke-static {}, Lcom/android/settings/core/instrumentation/SettingsEventLogWriter;->shouldDisableGenericEventLogging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    return-void

    .line 32
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settingslib/core/instrumentation/EventLogWriter;->visible(Landroid/content/Context;II)V

    .line 33
    return-void
.end method
