.class public Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;
.super Ljava/lang/Object;
.source "NotificationChannelSlice.java"

# interfaces
.implements Lcom/android/settings/slices/CustomSliceable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;
    }
.end annotation


# static fields
.field private static final CHANNEL_ID:Ljava/lang/String; = "channel_id"

.field private static final CHANNEL_STATE_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;",
            ">;"
        }
    .end annotation
.end field

.field static final DEFAULT_EXPANDED_ROW_COUNT:I = 0x3
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field

.field static final DURATION_END_DAYS:J
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field

.field static final DURATION_START_DAYS:J
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field

.field static final MIN_NOTIFICATION_SENT_COUNT:I = 0xa
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field private static final PACKAGE_UID:Ljava/lang/String; = "package_uid"

.field private static final TAG:Ljava/lang/String; = "NotifChannelSlice"

.field private static final TASK_TIMEOUT_MS:J = 0x64L


# instance fields
.field private mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

.field protected final mContext:Landroid/content/Context;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field

.field private mPackageName:Ljava/lang/String;

.field private mUid:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 84
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x7

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->DURATION_START_DAYS:J

    .line 86
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->DURATION_END_DAYS:J

    .line 117
    sget-object v0, Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$XUnbmGVSTgIKGjYAyN0s0g7vw5Q;->INSTANCE:Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$XUnbmGVSTgIKGjYAyN0s0g7vw5Q;

    sput-object v0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->CHANNEL_STATE_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    .line 147
    new-instance v0, Lcom/android/settings/notification/NotificationBackend;

    invoke-direct {v0}, Lcom/android/settings/notification/NotificationBackend;-><init>()V

    iput-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    .line 148
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 149
    return-void
.end method

.method private buildRowSliceAction(Landroid/app/NotificationChannel;Landroidx/core/graphics/drawable/IconCompat;)Landroidx/slice/builders/SliceAction;
    .locals 5
    .param p1, "channel"    # Landroid/app/NotificationChannel;
    .param p2, "icon"    # Landroidx/core/graphics/drawable/IconCompat;

    .line 256
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 257
    .local v0, "channelArgs":Landroid/os/Bundle;
    iget v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mUid:I

    const-string v2, "uid"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 258
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    const-string v2, "package"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.provider.extra.CHANNEL_ID"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v1, "fromSettings"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 262
    new-instance v1, Lcom/android/settings/core/SubSettingLauncher;

    iget-object v2, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;-><init>(Landroid/content/Context;)V

    const-class v2, Lcom/android/settings/notification/ChannelNotificationSettings;

    .line 263
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setDestination(Ljava/lang/String;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 264
    invoke-virtual {v1, v0}, Lcom/android/settings/core/SubSettingLauncher;->setArguments(Landroid/os/Bundle;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 265
    const v2, 0x7f120b30

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setTitleRes(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 266
    const/16 v2, 0x579

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setSourceMetricsCategory(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 267
    invoke-virtual {v1}, Lcom/android/settings/core/SubSettingLauncher;->toIntent()Landroid/content/Intent;

    move-result-object v1

    .line 269
    .local v1, "channelIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    .line 270
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->hashCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 271
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    .line 269
    invoke-static {v2, p2, v4, v3}, Landroidx/slice/builders/SliceAction;->createDeeplink(Landroid/app/PendingIntent;Landroidx/core/graphics/drawable/IconCompat;ILjava/lang/CharSequence;)Landroidx/slice/builders/SliceAction;

    move-result-object v2

    return-object v2
.end method

.method private getAppAndNotificationPageIntent()Landroid/content/Intent;
    .locals 5

    .line 436
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    const v1, 0x7f12015d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 437
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 439
    .local v0, "screenTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/settings/applications/AppAndNotificationDashboardFragment;

    .line 440
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 439
    const-string v3, ""

    const/16 v4, 0x579

    invoke-static {v1, v2, v3, v0, v4}, Lcom/android/settings/slices/SliceBuilderUtils;->buildSearchResultPageIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    .line 443
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/android/settings/SubSettings;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 444
    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 439
    return-object v1
.end method

.method private getDisplayableChannels(Lcom/android/settings/notification/NotificationBackend$AppRow;)Ljava/util/List;
    .locals 7
    .param p1, "appRow"    # Lcom/android/settings/notification/NotificationBackend$AppRow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/settings/notification/NotificationBackend$AppRow;",
            ")",
            "Ljava/util/List<",
            "Landroid/app/NotificationChannel;",
            ">;"
        }
    .end annotation

    .line 347
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p1, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget v2, p1, Lcom/android/settings/notification/NotificationBackend$AppRow;->uid:I

    .line 348
    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/NotificationBackend;->getGroups(Ljava/lang/String;I)Landroid/content/pm/ParceledListSlice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v0

    .line 349
    .local v0, "channelGroupList":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannelGroup;>;"
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$rjCvAykF4NUkEP8wQXOBsB2F5nw;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$rjCvAykF4NUkEP8wQXOBsB2F5nw;-><init>(Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;Lcom/android/settings/notification/NotificationBackend$AppRow;)V

    .line 350
    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->flatMap(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v1

    .line 352
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 355
    .local v1, "channels":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannel;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 356
    .local v2, "channelStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationChannel;

    .line 357
    .local v4, "channel":Landroid/app/NotificationChannel;
    iget-object v5, p1, Lcom/android/settings/notification/NotificationBackend$AppRow;->sentByChannel:Ljava/util/Map;

    invoke-virtual {v4}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;

    .line 358
    .local v5, "sentState":Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;
    if-nez v5, :cond_0

    .line 359
    new-instance v6, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;

    invoke-direct {v6}, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;-><init>()V

    move-object v5, v6

    .line 361
    :cond_0
    new-instance v6, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;

    invoke-direct {v6, v5, v4}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;-><init>(Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;Landroid/app/NotificationChannel;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    .end local v4    # "channel":Landroid/app/NotificationChannel;
    .end local v5    # "sentState":Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;
    goto :goto_0

    .line 365
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v3

    sget-object v4, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->CHANNEL_STATE_COMPARATOR:Ljava/util/Comparator;

    .line 366
    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->sorted(Ljava/util/Comparator;)Ljava/util/stream/Stream;

    move-result-object v3

    sget-object v4, Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$EcNouM_UqHGlYQGbnX7K4vNSbgE;->INSTANCE:Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$EcNouM_UqHGlYQGbnX7K4vNSbgE;

    .line 367
    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v3

    const-wide/16 v4, 0x3

    .line 368
    invoke-interface {v3, v4, v5}, Ljava/util/stream/Stream;->limit(J)Ljava/util/stream/Stream;

    move-result-object v3

    .line 369
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 365
    return-object v3
.end method

.method private getEligibleNotificationsPackage(Ljava/util/List;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/pm/PackageInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 373
    .local p1, "packageInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const/4 v0, 0x0

    return-object v0

    .line 378
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 379
    .local v0, "appRowTasks":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Lcom/android/settings/notification/NotificationBackend$AppRow;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    .line 380
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    new-instance v3, Lcom/android/settings/homepage/contextualcards/slices/NotificationMultiChannelAppRow;

    iget-object v4, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    invoke-direct {v3, v4, v5, v2}, Lcom/android/settings/homepage/contextualcards/slices/NotificationMultiChannelAppRow;-><init>(Landroid/content/Context;Lcom/android/settings/notification/NotificationBackend;Landroid/content/pm/PackageInfo;)V

    .line 382
    .local v3, "future":Lcom/android/settings/homepage/contextualcards/slices/NotificationMultiChannelAppRow;
    iget-object v4, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v4, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "future":Lcom/android/settings/homepage/contextualcards/slices/NotificationMultiChannelAppRow;
    goto :goto_0

    .line 386
    :cond_1
    const/4 v1, 0x0

    .line 387
    .local v1, "maxSentCount":I
    const/4 v2, 0x0

    .line 388
    .local v2, "maxSentCountPackage":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Future;

    .line 389
    .local v4, "appRowTask":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/android/settings/notification/NotificationBackend$AppRow;>;"
    const/4 v5, 0x0

    .line 391
    .local v5, "appRow":Lcom/android/settings/notification/NotificationBackend$AppRow;
    const-wide/16 v6, 0x64

    :try_start_0
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v6, v7, v8}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/settings/notification/NotificationBackend$AppRow;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v6

    .line 394
    goto :goto_2

    .line 392
    :catch_0
    move-exception v6

    .line 393
    .local v6, "e":Ljava/lang/Exception;
    const-string v7, "NotifChannelSlice"

    const-string v8, "Failed to get notification data."

    invoke-static {v7, v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 397
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_2
    if-eqz v5, :cond_2

    iget-boolean v6, v5, Lcom/android/settings/notification/NotificationBackend$AppRow;->banned:Z

    if-nez v6, :cond_2

    .line 398
    invoke-direct {p0, v5}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getDisplayableChannels(Lcom/android/settings/notification/NotificationBackend$AppRow;)Ljava/util/List;

    move-result-object v6

    .line 397
    invoke-direct {p0, v6}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->isAllChannelsBlocked(Ljava/util/List;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 399
    goto :goto_1

    .line 403
    :cond_3
    iget-object v6, v5, Lcom/android/settings/notification/NotificationBackend$AppRow;->sentByApp:Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;

    iget v6, v6, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;->sentCount:I

    .line 404
    .local v6, "sentCount":I
    const/16 v7, 0xa

    if-lt v6, v7, :cond_4

    if-le v6, v1, :cond_4

    .line 405
    move v1, v6

    .line 406
    iget-object v2, v5, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    .line 407
    iput-object v5, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    .line 409
    .end local v4    # "appRowTask":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/android/settings/notification/NotificationBackend$AppRow;>;"
    .end local v5    # "appRow":Lcom/android/settings/notification/NotificationBackend$AppRow;
    .end local v6    # "sentCount":I
    :cond_4
    goto :goto_1

    .line 411
    :cond_5
    return-object v2
.end method

.method private getNoSuggestedAppHeader()Landroidx/slice/builders/ListBuilder$HeaderBuilder;
    .locals 4

    .line 275
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    const v1, 0x7f080215

    invoke-static {v0, v1}, Landroidx/core/graphics/drawable/IconCompat;->createWithResource(Landroid/content/Context;I)Landroidx/core/graphics/drawable/IconCompat;

    move-result-object v0

    .line 277
    .local v0, "icon":Landroidx/core/graphics/drawable/IconCompat;
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    const v2, 0x7f120b16

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 278
    .local v1, "titleNoSuggestedApp":Ljava/lang/CharSequence;
    nop

    .line 279
    invoke-direct {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getAppAndNotificationPageIntent()Landroid/content/Intent;

    move-result-object v2

    .line 278
    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getPrimarySliceAction(Landroidx/core/graphics/drawable/IconCompat;Ljava/lang/CharSequence;Landroid/content/Intent;)Landroidx/slice/builders/SliceAction;

    move-result-object v2

    .line 281
    .local v2, "primarySliceActionForNoSuggestedApp":Landroidx/slice/builders/SliceAction;
    new-instance v3, Landroidx/slice/builders/ListBuilder$HeaderBuilder;

    invoke-direct {v3}, Landroidx/slice/builders/ListBuilder$HeaderBuilder;-><init>()V

    .line 282
    invoke-virtual {v3, v1}, Landroidx/slice/builders/ListBuilder$HeaderBuilder;->setTitle(Ljava/lang/CharSequence;)Landroidx/slice/builders/ListBuilder$HeaderBuilder;

    move-result-object v3

    .line 283
    invoke-virtual {v3, v2}, Landroidx/slice/builders/ListBuilder$HeaderBuilder;->setPrimaryAction(Landroidx/slice/builders/SliceAction;)Landroidx/slice/builders/ListBuilder$HeaderBuilder;

    move-result-object v3

    .line 281
    return-object v3
.end method

.method private getNotificationChannelRows(Landroidx/core/graphics/drawable/IconCompat;)Ljava/util/List;
    .locals 9
    .param p1, "icon"    # Landroidx/core/graphics/drawable/IconCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/core/graphics/drawable/IconCompat;",
            ")",
            "Ljava/util/List<",
            "Landroidx/slice/builders/ListBuilder$RowBuilder;",
            ">;"
        }
    .end annotation

    .line 287
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v0, "notificationChannelRows":Ljava/util/List;, "Ljava/util/List<Landroidx/slice/builders/ListBuilder$RowBuilder;>;"
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    invoke-direct {p0, v1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getDisplayableChannels(Lcom/android/settings/notification/NotificationBackend$AppRow;)Ljava/util/List;

    move-result-object v1

    .line 290
    .local v1, "displayableChannels":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannel;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationChannel;

    .line 291
    .local v3, "channel":Landroid/app/NotificationChannel;
    new-instance v4, Landroidx/slice/builders/ListBuilder$RowBuilder;

    invoke-direct {v4}, Landroidx/slice/builders/ListBuilder$RowBuilder;-><init>()V

    .line 292
    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getName()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroidx/slice/builders/ListBuilder$RowBuilder;->setTitle(Ljava/lang/CharSequence;)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mAppRow:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v6, v6, Lcom/android/settings/notification/NotificationBackend$AppRow;->sentByChannel:Ljava/util/Map;

    .line 294
    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;

    .line 293
    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/android/settings/notification/NotificationBackend;->getSentSummary(Landroid/content/Context;Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;Z)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroidx/slice/builders/ListBuilder$RowBuilder;->setSubtitle(Ljava/lang/CharSequence;)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v4

    .line 295
    invoke-direct {p0, v3, p1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->buildRowSliceAction(Landroid/app/NotificationChannel;Landroidx/core/graphics/drawable/IconCompat;)Landroidx/slice/builders/SliceAction;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroidx/slice/builders/ListBuilder$RowBuilder;->setPrimaryAction(Landroidx/slice/builders/SliceAction;)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v4

    .line 296
    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getToggleIntent(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v5

    const/4 v6, 0x0

    .line 297
    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v8

    if-eqz v8, :cond_0

    const/4 v7, 0x1

    .line 296
    :cond_0
    invoke-static {v5, v6, v7}, Landroidx/slice/builders/SliceAction;->createToggle(Landroid/app/PendingIntent;Ljava/lang/CharSequence;Z)Landroidx/slice/builders/SliceAction;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroidx/slice/builders/ListBuilder$RowBuilder;->addEndItem(Landroidx/slice/builders/SliceAction;)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v4

    .line 291
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    .end local v3    # "channel":Landroid/app/NotificationChannel;
    goto :goto_0

    .line 300
    :cond_1
    return-object v0
.end method

.method private getPrimarySliceAction(Landroidx/core/graphics/drawable/IconCompat;Ljava/lang/CharSequence;Landroid/content/Intent;)Landroidx/slice/builders/SliceAction;
    .locals 3
    .param p1, "icon"    # Landroidx/core/graphics/drawable/IconCompat;
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "intent"    # Landroid/content/Intent;

    .line 339
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    .line 340
    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p3, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 339
    invoke-static {v0, p1, v2, p2}, Landroidx/slice/builders/SliceAction;->createDeeplink(Landroid/app/PendingIntent;Landroidx/core/graphics/drawable/IconCompat;ILjava/lang/CharSequence;)Landroidx/slice/builders/SliceAction;

    move-result-object v0

    return-object v0
.end method

.method private getRecentlyInstalledPackages()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .line 315
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->DURATION_START_DAYS:J

    sub-long/2addr v0, v2

    .line 316
    .local v0, "startTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->DURATION_END_DAYS:J

    sub-long/2addr v2, v4

    .line 319
    .local v2, "endTime":J
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v4, "recentlyInstalledPackages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    iget-object v5, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    .line 321
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v5

    .line 322
    .local v5, "installedPackages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageInfo;

    .line 324
    .local v7, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v8}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 325
    invoke-virtual {p0, v8}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->isUserInteracted(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 326
    goto :goto_0

    .line 329
    :cond_1
    iget-wide v8, v7, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    cmp-long v8, v8, v0

    if-ltz v8, :cond_2

    iget-wide v8, v7, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    cmp-long v8, v8, v2

    if-gtz v8, :cond_2

    .line 331
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    .end local v7    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    goto :goto_0

    .line 335
    :cond_3
    return-object v4
.end method

.method private getToggleIntent(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p1, "channelId"    # Ljava/lang/String;

    .line 305
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/settings/slices/SliceBroadcastReceiver;

    .line 306
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    .line 307
    const-string v2, "package_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mUid:I

    .line 308
    const-string v2, "package_uid"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 309
    const-string v1, "channel_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 311
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private isAllChannelsBlocked(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/app/NotificationChannel;",
            ">;)Z"
        }
    .end annotation

    .line 415
    .local p1, "channels":Ljava/util/List;, "Ljava/util/List<Landroid/app/NotificationChannel;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationChannel;

    .line 416
    .local v1, "channel":Landroid/app/NotificationChannel;
    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v2

    if-eqz v2, :cond_0

    .line 417
    const/4 v0, 0x0

    return v0

    .line 419
    .end local v1    # "channel":Landroid/app/NotificationChannel;
    :cond_0
    goto :goto_0

    .line 420
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private isChannelBlockable(Landroid/app/NotificationChannel;Lcom/android/settings/notification/NotificationBackend$AppRow;)Z
    .locals 3
    .param p1, "channel"    # Landroid/app/NotificationChannel;
    .param p2, "appRow"    # Lcom/android/settings/notification/NotificationBackend$AppRow;

    .line 470
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 471
    iget-boolean v1, p2, Lcom/android/settings/notification/NotificationBackend$AppRow;->systemApp:Z

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 472
    return v2

    .line 475
    :cond_0
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->isBlockableSystem()Z

    move-result v1

    if-nez v1, :cond_1

    .line 476
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    move v0, v2

    .line 475
    :cond_2
    return v0

    .line 479
    :cond_3
    return v0
.end method

.method private isChannelConfigurable(Landroid/app/NotificationChannel;Lcom/android/settings/notification/NotificationBackend$AppRow;)Z
    .locals 1
    .param p1, "channel"    # Landroid/app/NotificationChannel;
    .param p2, "appRow"    # Lcom/android/settings/notification/NotificationBackend$AppRow;

    .line 461
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 462
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->isImportanceLockedByOEM()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    .line 465
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isChannelEnabled(Landroid/app/NotificationChannelGroup;Landroid/app/NotificationChannel;Lcom/android/settings/notification/NotificationBackend$AppRow;)Z
    .locals 3
    .param p1, "group"    # Landroid/app/NotificationChannelGroup;
    .param p2, "channel"    # Landroid/app/NotificationChannel;
    .param p3, "appRow"    # Lcom/android/settings/notification/NotificationBackend$AppRow;

    .line 449
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mUid:I

    .line 450
    invoke-static {v0, v1, v2}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfApplicationIsSuspended(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    .line 453
    .local v0, "suspendedAppsAdmin":Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;
    if-nez v0, :cond_0

    .line 454
    invoke-direct {p0, p2, p3}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->isChannelBlockable(Landroid/app/NotificationChannel;Lcom/android/settings/notification/NotificationBackend$AppRow;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    invoke-direct {p0, p2, p3}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->isChannelConfigurable(Landroid/app/NotificationChannel;Lcom/android/settings/notification/NotificationBackend$AppRow;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 456
    invoke-virtual {p1}, Landroid/app/NotificationChannelGroup;->isBlocked()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 453
    :goto_0
    return v1
.end method

.method static synthetic lambda$getDisplayableChannels$3(Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;)Landroid/app/NotificationChannel;
    .locals 1
    .param p0, "state"    # Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;

    .line 367
    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;->getNotificationChannel()Landroid/app/NotificationChannel;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$static$0(Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;)I
    .locals 6
    .param p0, "left"    # Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;
    .param p1, "right"    # Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;

    .line 119
    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;->getNotificationsSentState()Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;

    move-result-object v0

    .line 120
    .local v0, "leftState":Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;
    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;->getNotificationsSentState()Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;

    move-result-object v1

    .line 121
    .local v1, "rightState":Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;
    iget v2, v1, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;->avgSentWeekly:I

    iget v3, v0, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;->avgSentWeekly:I

    if-eq v2, v3, :cond_0

    .line 122
    iget v2, v1, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;->avgSentWeekly:I

    iget v3, v0, Lcom/android/settings/notification/NotificationBackend$NotificationsSentState;->avgSentWeekly:I

    sub-int/2addr v2, v3

    return v2

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;->getNotificationChannel()Landroid/app/NotificationChannel;

    move-result-object v2

    .line 126
    .local v2, "leftChannel":Landroid/app/NotificationChannel;
    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice$NotificationChannelState;->getNotificationChannel()Landroid/app/NotificationChannel;

    move-result-object v3

    .line 127
    .local v3, "rightChannel":Landroid/app/NotificationChannel;
    invoke-virtual {v2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v4

    const-string v5, "miscellaneous"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 128
    const/4 v4, 0x1

    return v4

    .line 129
    :cond_1
    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 131
    const/4 v4, -0x1

    return v4

    .line 134
    :cond_2
    invoke-virtual {v2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    return v4
.end method


# virtual methods
.method getApplicationIcon(Ljava/lang/String;)Landroidx/core/graphics/drawable/IconCompat;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation

    .line 237
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    nop

    .line 243
    invoke-static {v0}, Lcom/android/settings/Utils;->createIconWithDrawable(Landroid/graphics/drawable/Drawable;)Landroidx/core/graphics/drawable/IconCompat;

    move-result-object v1

    return-object v1

    .line 238
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "NotifChannelSlice"

    const-string v2, "No such package to get application icon."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    const/4 v1, 0x0

    return-object v1
.end method

.method getApplicationUid(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation

    .line 248
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    .line 249
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/android/settingslib/applications/ApplicationsState;->getInstance(Landroid/app/Application;)Lcom/android/settingslib/applications/ApplicationsState;

    move-result-object v0

    .line 250
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/settingslib/applications/ApplicationsState;->getEntry(Ljava/lang/String;I)Lcom/android/settingslib/applications/ApplicationsState$AppEntry;

    move-result-object v0

    .line 252
    .local v0, "appEntry":Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
    iget-object v1, v0, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    return v1
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 3

    .line 210
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 211
    .local v0, "args":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    const-string v2, "package"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mUid:I

    const-string v2, "uid"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    new-instance v1, Lcom/android/settings/core/SubSettingLauncher;

    iget-object v2, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;-><init>(Landroid/content/Context;)V

    const-class v2, Lcom/android/settings/notification/AppNotificationSettings;

    .line 215
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setDestination(Ljava/lang/String;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 216
    const v2, 0x7f120b8a

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setTitleRes(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 217
    invoke-virtual {v1, v0}, Lcom/android/settings/core/SubSettingLauncher;->setArguments(Landroid/os/Bundle;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 218
    const/16 v2, 0x579

    invoke-virtual {v1, v2}, Lcom/android/settings/core/SubSettingLauncher;->setSourceMetricsCategory(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object v1

    .line 219
    invoke-virtual {v1}, Lcom/android/settings/core/SubSettingLauncher;->toIntent()Landroid/content/Intent;

    move-result-object v1

    .line 214
    return-object v1
.end method

.method public getSlice()Landroidx/slice/Slice;
    .locals 7

    .line 153
    new-instance v0, Landroidx/slice/builders/ListBuilder;

    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    .line 154
    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getUri()Landroid/net/Uri;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Landroidx/slice/builders/ListBuilder;-><init>(Landroid/content/Context;Landroid/net/Uri;J)V

    .line 155
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroidx/slice/builders/ListBuilder;->setAccentColor(I)Landroidx/slice/builders/ListBuilder;

    move-result-object v0

    .line 162
    .local v0, "listBuilder":Landroidx/slice/builders/ListBuilder;
    invoke-direct {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getRecentlyInstalledPackages()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getEligibleNotificationsPackage(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    .line 163
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getNoSuggestedAppHeader()Landroidx/slice/builders/ListBuilder$HeaderBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/slice/builders/ListBuilder;->setHeader(Landroidx/slice/builders/ListBuilder$HeaderBuilder;)Landroidx/slice/builders/ListBuilder;

    move-result-object v1

    .line 166
    invoke-virtual {v1, v2}, Landroidx/slice/builders/ListBuilder;->setIsError(Z)Landroidx/slice/builders/ListBuilder;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/slice/builders/ListBuilder;->build()Landroidx/slice/Slice;

    move-result-object v1

    .line 165
    return-object v1

    .line 168
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getApplicationUid(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mUid:I

    .line 171
    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getApplicationIcon(Ljava/lang/String;)Landroidx/core/graphics/drawable/IconCompat;

    move-result-object v1

    .line 172
    .local v1, "icon":Landroidx/core/graphics/drawable/IconCompat;
    iget-object v3, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    const v4, 0x7f1209d5

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    .line 173
    invoke-static {v3, v5}, Lcom/android/settings/Utils;->getApplicationLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v2, v6

    .line 172
    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "title":Ljava/lang/CharSequence;
    new-instance v3, Landroidx/slice/builders/ListBuilder$RowBuilder;

    invoke-direct {v3}, Landroidx/slice/builders/ListBuilder$RowBuilder;-><init>()V

    .line 175
    invoke-virtual {v3, v1, v6}, Landroidx/slice/builders/ListBuilder$RowBuilder;->setTitleItem(Landroidx/core/graphics/drawable/IconCompat;I)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v3

    .line 176
    invoke-virtual {v3, v2}, Landroidx/slice/builders/ListBuilder$RowBuilder;->setTitle(Ljava/lang/CharSequence;)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mPackageName:Ljava/lang/String;

    iget v5, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mUid:I

    .line 177
    invoke-virtual {p0, v4, v5}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getSubTitle(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroidx/slice/builders/ListBuilder$RowBuilder;->setSubtitle(Ljava/lang/CharSequence;)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v3

    .line 178
    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-direct {p0, v1, v2, v4}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getPrimarySliceAction(Landroidx/core/graphics/drawable/IconCompat;Ljava/lang/CharSequence;Landroid/content/Intent;)Landroidx/slice/builders/SliceAction;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroidx/slice/builders/ListBuilder$RowBuilder;->setPrimaryAction(Landroidx/slice/builders/SliceAction;)Landroidx/slice/builders/ListBuilder$RowBuilder;

    move-result-object v3

    .line 174
    invoke-virtual {v0, v3}, Landroidx/slice/builders/ListBuilder;->addRow(Landroidx/slice/builders/ListBuilder$RowBuilder;)Landroidx/slice/builders/ListBuilder;

    .line 181
    invoke-direct {p0, v1}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->getNotificationChannelRows(Landroidx/core/graphics/drawable/IconCompat;)Ljava/util/List;

    move-result-object v3

    .line 182
    .local v3, "rows":Ljava/util/List;, "Ljava/util/List<Landroidx/slice/builders/ListBuilder$RowBuilder;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroidx/slice/builders/ListBuilder$RowBuilder;

    .line 183
    .local v5, "rowBuilder":Landroidx/slice/builders/ListBuilder$RowBuilder;
    invoke-virtual {v0, v5}, Landroidx/slice/builders/ListBuilder;->addRow(Landroidx/slice/builders/ListBuilder$RowBuilder;)Landroidx/slice/builders/ListBuilder;

    .line 184
    .end local v5    # "rowBuilder":Landroidx/slice/builders/ListBuilder$RowBuilder;
    goto :goto_0

    .line 186
    :cond_1
    invoke-virtual {v0}, Landroidx/slice/builders/ListBuilder;->build()Landroidx/slice/Slice;

    move-result-object v4

    return-object v4
.end method

.method protected getSubTitle(Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 424
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/notification/NotificationBackend;->getChannelCount(Ljava/lang/String;I)I

    move-result v0

    .line 426
    .local v0, "channelCount":I
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x3

    if-le v0, v3, :cond_0

    .line 427
    iget-object v3, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    const v4, 0x7f120b6a

    new-array v2, v2, [Ljava/lang/Object;

    .line 428
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    .line 427
    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 431
    :cond_0
    iget-object v3, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f10002f

    new-array v2, v2, [Ljava/lang/Object;

    .line 432
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    .line 431
    invoke-virtual {v3, v4, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .line 191
    sget-object v0, Lcom/android/settings/slices/CustomSliceRegistry;->NOTIFICATION_CHANNEL_SLICE_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected isUserInteracted(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$getDisplayableChannels$1$NotificationChannelSlice(Landroid/app/NotificationChannelGroup;Lcom/android/settings/notification/NotificationBackend$AppRow;Landroid/app/NotificationChannel;)Z
    .locals 1
    .param p1, "group"    # Landroid/app/NotificationChannelGroup;
    .param p2, "appRow"    # Lcom/android/settings/notification/NotificationBackend$AppRow;
    .param p3, "channel"    # Landroid/app/NotificationChannel;

    .line 351
    invoke-direct {p0, p1, p3, p2}, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->isChannelEnabled(Landroid/app/NotificationChannelGroup;Landroid/app/NotificationChannel;Lcom/android/settings/notification/NotificationBackend$AppRow;)Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$getDisplayableChannels$2$NotificationChannelSlice(Lcom/android/settings/notification/NotificationBackend$AppRow;Landroid/app/NotificationChannelGroup;)Ljava/util/stream/Stream;
    .locals 2
    .param p1, "appRow"    # Lcom/android/settings/notification/NotificationBackend$AppRow;
    .param p2, "group"    # Landroid/app/NotificationChannelGroup;

    .line 350
    invoke-virtual {p2}, Landroid/app/NotificationChannelGroup;->getChannels()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$zU3AAvzpcXINxpr9vtX78ZWmBdM;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/settings/homepage/contextualcards/slices/-$$Lambda$NotificationChannelSlice$zU3AAvzpcXINxpr9vtX78ZWmBdM;-><init>(Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;Landroid/app/NotificationChannelGroup;Lcom/android/settings/notification/NotificationBackend$AppRow;)V

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    return-object v0
.end method

.method public onNotifyChange(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .line 196
    const/4 v0, 0x0

    const-string v1, "android.app.slice.extra.TOGGLE_STATE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 197
    .local v1, "newState":Z
    const-string v2, "package_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 198
    .local v2, "packageName":Ljava/lang/String;
    const-string v3, "package_uid"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 199
    .local v3, "uid":I
    const-string v4, "channel_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 200
    .local v4, "channelId":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    invoke-virtual {v5, v2, v3, v4}, Lcom/android/settings/notification/NotificationBackend;->getChannel(Ljava/lang/String;ILjava/lang/String;)Landroid/app/NotificationChannel;

    move-result-object v5

    .line 202
    .local v5, "channel":Landroid/app/NotificationChannel;
    if-eqz v1, :cond_0

    const/4 v0, 0x2

    .line 203
    .local v0, "importance":I
    :cond_0
    invoke-virtual {v5, v0}, Landroid/app/NotificationChannel;->setImportance(I)V

    .line 204
    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/app/NotificationChannel;->lockFields(I)V

    .line 205
    iget-object v6, p0, Lcom/android/settings/homepage/contextualcards/slices/NotificationChannelSlice;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    invoke-virtual {v6, v2, v3, v5}, Lcom/android/settings/notification/NotificationBackend;->updateChannel(Ljava/lang/String;ILandroid/app/NotificationChannel;)V

    .line 206
    return-void
.end method
