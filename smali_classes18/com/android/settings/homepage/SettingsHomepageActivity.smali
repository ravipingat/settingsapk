.class public Lcom/android/settings/homepage/SettingsHomepageActivity;
.super Landroidx/fragment/app/FragmentActivity;
.source "SettingsHomepageActivity.java"


# static fields
.field private static final MSG_DELAY_LOAD_CARD:I = 0x1


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Landroidx/fragment/app/FragmentActivity;-><init>()V

    .line 46
    new-instance v0, Lcom/android/settings/homepage/SettingsHomepageActivity$1;

    invoke-direct {v0, p0}, Lcom/android/settings/homepage/SettingsHomepageActivity$1;-><init>(Lcom/android/settings/homepage/SettingsHomepageActivity;)V

    iput-object v0, p0, Lcom/android/settings/homepage/SettingsHomepageActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/homepage/SettingsHomepageActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/settings/homepage/SettingsHomepageActivity;

    .line 43
    invoke-direct {p0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->showContextualCardsFragment()V

    return-void
.end method

.method private showContextualCardsFragment()V
    .locals 2

    .line 93
    const-class v0, Landroid/app/ActivityManager;

    invoke-virtual {p0, v0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/android/settings/homepage/contextualcards/ContextualCardsFragment;

    invoke-direct {v0}, Lcom/android/settings/homepage/contextualcards/ContextualCardsFragment;-><init>()V

    const v1, 0x7f0a018f

    invoke-direct {p0, v0, v1}, Lcom/android/settings/homepage/SettingsHomepageActivity;->showFragment(Landroidx/fragment/app/Fragment;I)V

    .line 97
    :cond_0
    return-void
.end method

.method private showFragment(Landroidx/fragment/app/Fragment;I)V
    .locals 3
    .param p1, "fragment"    # Landroidx/fragment/app/Fragment;
    .param p2, "id"    # I

    .line 99
    invoke-virtual {p0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 100
    .local v0, "fragmentManager":Landroidx/fragment/app/FragmentManager;
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    .line 101
    .local v1, "fragmentTransaction":Landroidx/fragment/app/FragmentTransaction;
    invoke-virtual {v0, p2}, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;

    move-result-object v2

    .line 103
    .local v2, "showFragment":Landroidx/fragment/app/Fragment;
    if-nez v2, :cond_0

    .line 104
    invoke-virtual {v1, p2, p1}, Landroidx/fragment/app/FragmentTransaction;->add(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    goto :goto_0

    .line 106
    :cond_0
    invoke-virtual {v1, v2}, Landroidx/fragment/app/FragmentTransaction;->show(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    .line 109
    :goto_0
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 111
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 61
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f0d02b5

    invoke-virtual {p0, v0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->setContentView(I)V

    .line 68
    invoke-virtual {p0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/oneplus/settings/utils/OPUtils;->getThemeMode(Landroid/content/ContentResolver;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/oneplus/settings/utils/OPUtils;->setLightNavigationBar(Landroid/view/Window;I)V

    .line 81
    const v0, 0x7f0a05e2

    invoke-virtual {p0, v0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 82
    .local v0, "searchview":Landroid/view/View;
    invoke-static {p0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/overlay/FeatureFactory;->getSearchFeatureProvider()Lcom/android/settings/search/SearchFeatureProvider;

    move-result-object v1

    const/16 v2, 0x5de

    invoke-interface {v1, p0, v0, v2}, Lcom/android/settings/search/SearchFeatureProvider;->initSearchLayout(Landroid/app/Activity;Landroid/view/View;I)V

    .line 85
    iget-object v1, p0, Lcom/android/settings/homepage/SettingsHomepageActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 87
    new-instance v1, Lcom/android/settings/homepage/TopLevelSettings;

    invoke-direct {v1}, Lcom/android/settings/homepage/TopLevelSettings;-><init>()V

    const v2, 0x7f0a03b2

    invoke-direct {p0, v1, v2}, Lcom/android/settings/homepage/SettingsHomepageActivity;->showFragment(Landroidx/fragment/app/Fragment;I)V

    .line 88
    invoke-virtual {p0, v2}, Lcom/android/settings/homepage/SettingsHomepageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 89
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 90
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 115
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    .line 116
    iget-object v0, p0, Lcom/android/settings/homepage/SettingsHomepageActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 117
    return-void
.end method

.method setHomepageContainerPaddingTop()V
    .locals 5
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 122
    const v0, 0x7f0a02e5

    invoke-virtual {p0, v0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 124
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07051c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 125
    .local v1, "searchBarHeight":I
    invoke-virtual {p0}, Lcom/android/settings/homepage/SettingsHomepageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07051d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 128
    .local v2, "searchBarMargin":I
    mul-int/lit8 v3, v2, 0x2

    add-int/2addr v3, v1

    .line 129
    .local v3, "paddingTop":I
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 130
    return-void
.end method
