.class public Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;
.source "GestureNavigationNotAvailableDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GestureNavigationNotAvailableDialog"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method public static show(Lcom/android/settings/gestures/SystemNavigationGestureSettings;)V
    .locals 3
    .param p0, "parent"    # Lcom/android/settings/gestures/SystemNavigationGestureSettings;

    .line 35
    invoke-virtual {p0}, Lcom/android/settings/gestures/SystemNavigationGestureSettings;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    return-void

    .line 39
    :cond_0
    new-instance v0, Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;

    invoke-direct {v0}, Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;-><init>()V

    .line 41
    .local v0, "dialog":Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    .line 42
    invoke-virtual {p0}, Lcom/android/settings/gestures/SystemNavigationGestureSettings;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "GestureNavigationNotAvailableDialog"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 43
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 47
    const/16 v0, 0x6d3

    return v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    invoke-virtual {p0}, Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    .line 53
    .local v0, "context":Landroid/content/Context;
    nop

    .line 54
    invoke-static {v0}, Lcom/android/settings/gestures/SystemNavigationPreferenceController;->getDefaultHomeAppName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 55
    .local v1, "defaultHomeAppName":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const v3, 0x7f12075f

    invoke-virtual {p0, v3, v2}, Lcom/android/settings/gestures/GestureNavigationNotAvailableDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "message":Ljava/lang/String;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 59
    const v4, 0x7f120b98

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 60
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 57
    return-object v3
.end method
