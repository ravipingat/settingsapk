.class Lcom/android/settings/gestures/SystemNavigationGestureSettings$NavModeCandidateInfo;
.super Lcom/android/settingslib/widget/CandidateInfo;
.source "SystemNavigationGestureSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/gestures/SystemNavigationGestureSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NavModeCandidateInfo"
.end annotation


# instance fields
.field private final mKey:Ljava/lang/String;

.field private final mLabel:Ljava/lang/CharSequence;

.field private final mSummary:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "label"    # Ljava/lang/CharSequence;
    .param p2, "summary"    # Ljava/lang/CharSequence;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "enabled"    # Z

    .line 350
    invoke-direct {p0, p4}, Lcom/android/settingslib/widget/CandidateInfo;-><init>(Z)V

    .line 351
    iput-object p1, p0, Lcom/android/settings/gestures/SystemNavigationGestureSettings$NavModeCandidateInfo;->mLabel:Ljava/lang/CharSequence;

    .line 352
    iput-object p2, p0, Lcom/android/settings/gestures/SystemNavigationGestureSettings$NavModeCandidateInfo;->mSummary:Ljava/lang/CharSequence;

    .line 353
    iput-object p3, p0, Lcom/android/settings/gestures/SystemNavigationGestureSettings$NavModeCandidateInfo;->mKey:Ljava/lang/String;

    .line 354
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .line 372
    iget-object v0, p0, Lcom/android/settings/gestures/SystemNavigationGestureSettings$NavModeCandidateInfo;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public loadIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 367
    const/4 v0, 0x0

    return-object v0
.end method

.method public loadLabel()Ljava/lang/CharSequence;
    .locals 1

    .line 358
    iget-object v0, p0, Lcom/android/settings/gestures/SystemNavigationGestureSettings$NavModeCandidateInfo;->mLabel:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public loadSummary()Ljava/lang/CharSequence;
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/android/settings/gestures/SystemNavigationGestureSettings$NavModeCandidateInfo;->mSummary:Ljava/lang/CharSequence;

    return-object v0
.end method
