.class public Lcom/android/settings/development/DisableDevSettingsDialogFragment;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;
.source "DisableDevSettingsDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field public static final TAG:Ljava/lang/String; = "DisableDevSettingDlg"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method static newInstance()Lcom/android/settings/development/DisableDevSettingsDialogFragment;
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 41
    new-instance v0, Lcom/android/settings/development/DisableDevSettingsDialogFragment;

    invoke-direct {v0}, Lcom/android/settings/development/DisableDevSettingsDialogFragment;-><init>()V

    .line 42
    .local v0, "dialog":Lcom/android/settings/development/DisableDevSettingsDialogFragment;
    return-object v0
.end method

.method public static show(Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;)V
    .locals 3
    .param p0, "host"    # Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    .line 46
    new-instance v0, Lcom/android/settings/development/DisableDevSettingsDialogFragment;

    invoke-direct {v0}, Lcom/android/settings/development/DisableDevSettingsDialogFragment;-><init>()V

    .line 47
    .local v0, "dialog":Lcom/android/settings/development/DisableDevSettingsDialogFragment;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/development/DisableDevSettingsDialogFragment;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    .line 48
    invoke-virtual {p0}, Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    .line 49
    .local v1, "manager":Landroidx/fragment/app/FragmentManager;
    const-string v2, "DisableDevSettingDlg"

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/development/DisableDevSettingsDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 50
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 54
    const/16 v0, 0x637

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .line 73
    invoke-virtual {p0}, Lcom/android/settings/development/DisableDevSettingsDialogFragment;->getTargetFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    .line 74
    .local v0, "fragment":Landroidx/fragment/app/Fragment;
    instance-of v1, v0, Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    if-nez v1, :cond_0

    .line 75
    const-string v1, "DisableDevSettingDlg"

    const-string v2, "getTargetFragment return unexpected type"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    .line 80
    .local v1, "host":Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 81
    invoke-virtual {v1}, Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;->onDisableDevelopmentOptionsConfirmed()V

    .line 82
    invoke-virtual {p0}, Lcom/android/settings/development/DisableDevSettingsDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Landroid/os/PowerManager;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 83
    .local v2, "pm":Landroid/os/PowerManager;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 84
    .end local v2    # "pm":Landroid/os/PowerManager;
    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {v1}, Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;->onDisableDevelopmentOptionsRejected()V

    .line 87
    :goto_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 61
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/DisableDevSettingsDialogFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 62
    const v1, 0x7f1202f3

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 63
    const v1, 0x7f1202f4

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 64
    const v1, 0x7f1202f2

    invoke-virtual {v0, v1, p0}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 66
    const v1, 0x7f1202f1

    invoke-virtual {v0, v1, p0}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    .line 61
    return-object v0
.end method
