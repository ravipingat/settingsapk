.class public Lcom/android/settings/development/SimulateColorSpacePreferenceController;
.super Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;
.source "SimulateColorSpacePreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/PreferenceControllerMixin;


# static fields
.field static final SETTING_VALUE_OFF:I = 0x0
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field static final SETTING_VALUE_ON:I = 0x1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final SIMULATE_COLOR_SPACE:Ljava/lang/String; = "simulate_color_space"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 44
    invoke-direct {p0, p1}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method private updateSimulateColorSpace()V
    .locals 10

    .line 72
    iget-object v0, p0, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 73
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    const-string v2, "accessibility_display_daltonizer_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v1

    .line 76
    .local v2, "enabled":Z
    :goto_0
    iget-object v4, p0, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v4, Landroidx/preference/ListPreference;

    .line 77
    .local v4, "listPreference":Landroidx/preference/ListPreference;
    const/4 v5, -0x1

    if-eqz v2, :cond_2

    .line 78
    const-string v6, "accessibility_display_daltonizer"

    invoke-static {v0, v6, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 81
    .local v5, "mode":Ljava/lang/String;
    invoke-virtual {v4, v5}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v4, v5}, Landroidx/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v6

    .line 83
    .local v6, "index":I
    if-gez v6, :cond_1

    .line 84
    iget-object v7, p0, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 86
    .local v7, "res":Landroid/content/res/Resources;
    const v8, 0x7f1204ff

    new-array v3, v3, [Ljava/lang/Object;

    const v9, 0x7f120058

    .line 87
    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v3, v1

    .line 86
    invoke-virtual {v7, v8, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 88
    .end local v7    # "res":Landroid/content/res/Resources;
    goto :goto_1

    .line 89
    :cond_1
    const-string v1, "%s"

    invoke-virtual {v4, v1}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 91
    .end local v5    # "mode":Ljava/lang/String;
    .end local v6    # "index":I
    :goto_1
    goto :goto_2

    .line 92
    :cond_2
    nop

    .line 93
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {v4, v1}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 95
    :goto_2
    return-void
.end method

.method private usingDevelopmentColorSpace()Z
    .locals 6

    .line 115
    iget-object v0, p0, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 116
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    const-string v2, "accessibility_display_daltonizer_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v1

    .line 119
    .local v2, "enabled":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 120
    const/4 v4, -0x1

    const-string v5, "accessibility_display_daltonizer"

    invoke-static {v0, v5, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 123
    .local v4, "mode":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v5, Landroidx/preference/ListPreference;

    invoke-virtual {v5, v4}, Landroidx/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v5

    .line 124
    .local v5, "index":I
    if-ltz v5, :cond_1

    .line 126
    return v3

    .line 129
    .end local v4    # "mode":Ljava/lang/String;
    .end local v5    # "index":I
    :cond_1
    return v1
.end method

.method private writeSimulateColorSpace(Ljava/lang/Object;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 99
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 100
    .local v1, "newMode":I
    const-string v2, "accessibility_display_daltonizer_enabled"

    if-gez v1, :cond_0

    .line 101
    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 104
    :cond_0
    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 106
    const-string v2, "accessibility_display_daltonizer"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 108
    :goto_0
    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 49
    const-string v0, "simulate_color_space"

    return-object v0
.end method

.method public onDeveloperOptionsDisabled()V
    .locals 1

    .line 65
    invoke-super {p0}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;->onDeveloperOptionsDisabled()V

    .line 66
    invoke-direct {p0}, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->usingDevelopmentColorSpace()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->writeSimulateColorSpace(Ljava/lang/Object;)V

    .line 69
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 54
    invoke-direct {p0, p2}, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->writeSimulateColorSpace(Ljava/lang/Object;)V

    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 0
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 60
    invoke-direct {p0}, Lcom/android/settings/development/SimulateColorSpacePreferenceController;->updateSimulateColorSpace()V

    .line 61
    return-void
.end method
