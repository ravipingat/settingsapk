.class public Lcom/android/settings/development/LogPersistPreferenceController;
.super Lcom/android/settingslib/development/AbstractLogpersistPreferenceController;
.source "LogPersistPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;


# instance fields
.field private final mFragment:Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragment"    # Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;
    .param p3, "lifecycle"    # Lcom/android/settingslib/core/lifecycle/Lifecycle;

    .line 35
    invoke-direct {p0, p1, p3}, Lcom/android/settingslib/development/AbstractLogpersistPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    .line 37
    iput-object p2, p0, Lcom/android/settings/development/LogPersistPreferenceController;->mFragment:Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    .line 38
    return-void
.end method


# virtual methods
.method public dismissConfirmationDialog()V
    .locals 0

    .line 48
    return-void
.end method

.method public isConfirmationDialogShowing()Z
    .locals 1

    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method protected onDeveloperOptionsSwitchDisabled()V
    .locals 2

    .line 62
    invoke-super {p0}, Lcom/android/settingslib/development/AbstractLogpersistPreferenceController;->onDeveloperOptionsSwitchDisabled()V

    .line 63
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/LogPersistPreferenceController;->writeLogpersistOption(Ljava/lang/Object;Z)V

    .line 64
    return-void
.end method

.method public onDisableLogPersistDialogConfirmed()V
    .locals 1

    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/development/LogPersistPreferenceController;->setLogpersistOff(Z)V

    .line 68
    invoke-virtual {p0}, Lcom/android/settings/development/LogPersistPreferenceController;->updateLogpersistValues()V

    .line 69
    return-void
.end method

.method public onDisableLogPersistDialogRejected()V
    .locals 0

    .line 72
    invoke-virtual {p0}, Lcom/android/settings/development/LogPersistPreferenceController;->updateLogpersistValues()V

    .line 73
    return-void
.end method

.method public showConfirmationDialog(Landroidx/preference/Preference;)V
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 42
    iget-object v0, p0, Lcom/android/settings/development/LogPersistPreferenceController;->mFragment:Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    invoke-static {v0}, Lcom/android/settings/development/DisableLogPersistWarningDialog;->show(Lcom/android/settings/development/LogPersistDialogHost;)V

    .line 43
    return-void
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 0
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 57
    invoke-virtual {p0}, Lcom/android/settings/development/LogPersistPreferenceController;->updateLogpersistValues()V

    .line 58
    return-void
.end method
