.class public Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;
.super Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;
.source "BluetoothA2dpHwOffloadPreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/core/PreferenceControllerMixin;


# static fields
.field static final A2DP_OFFLOAD_DISABLED_PROPERTY:Ljava/lang/String; = "persist.bluetooth.a2dp_offload.disabled"

.field static final A2DP_OFFLOAD_SUPPORTED_PROPERTY:Ljava/lang/String; = "ro.bluetooth.a2dp_offload.supported"

.field private static final PREFERENCE_KEY:Ljava/lang/String; = "bluetooth_disable_a2dp_hw_offload"


# instance fields
.field private final mFragment:Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragment"    # Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    .line 39
    invoke-direct {p0, p1}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p2, p0, Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;->mFragment:Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    .line 41
    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 45
    const-string v0, "bluetooth_disable_a2dp_hw_offload"

    return-object v0
.end method

.method public isDefaultValue()Z
    .locals 4

    .line 86
    nop

    .line 87
    const/4 v0, 0x0

    const-string v1, "ro.bluetooth.a2dp_offload.supported"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 88
    .local v1, "offloadSupported":Z
    nop

    .line 89
    const-string v2, "persist.bluetooth.a2dp_offload.disabled"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 90
    .local v2, "offloadDisabled":Z
    const/4 v3, 0x1

    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move v3, v0

    :cond_1
    :goto_0
    return v3
.end method

.method public onA2dpHwDialogConfirmed()V
    .locals 3

    .line 94
    nop

    .line 95
    const-string v0, "persist.bluetooth.a2dp_offload.disabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 96
    .local v1, "offloadDisabled":Z
    xor-int/lit8 v2, v1, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method protected onDeveloperOptionsSwitchDisabled()V
    .locals 3

    .line 76
    invoke-super {p0}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;->onDeveloperOptionsSwitchDisabled()V

    .line 77
    nop

    .line 78
    const/4 v0, 0x0

    const-string v1, "ro.bluetooth.a2dp_offload.supported"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 79
    .local v1, "offloadSupported":Z
    if-eqz v1, :cond_0

    .line 80
    iget-object v2, p0, Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v2, Landroidx/preference/SwitchPreference;

    invoke-virtual {v2, v0}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .line 81
    const-string v0, "persist.bluetooth.a2dp_offload.disabled"

    const-string v2, "false"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 50
    iget-object v0, p0, Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;->mFragment:Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    invoke-static {v0, p0}, Lcom/android/settings/development/BluetoothA2dpHwOffloadRebootDialog;->show(Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;)V

    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 4
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 56
    invoke-super {p0, p1}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 57
    nop

    .line 58
    const/4 v0, 0x0

    const-string v1, "ro.bluetooth.a2dp_offload.supported"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 59
    .local v1, "offloadSupported":Z
    if-eqz v1, :cond_0

    .line 60
    nop

    .line 61
    const-string v2, "persist.bluetooth.a2dp_offload.disabled"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 62
    .local v2, "offloadDisabled":Z
    iget-object v3, p0, Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v3, Landroidx/preference/SwitchPreference;

    invoke-virtual {v3, v2}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .line 63
    .end local v2    # "offloadDisabled":Z
    goto :goto_0

    .line 64
    :cond_0
    iget-object v2, p0, Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v2, Landroidx/preference/SwitchPreference;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .line 69
    :goto_0
    iget-object v2, p0, Lcom/android/settings/development/BluetoothA2dpHwOffloadPreferenceController;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {v2, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    .line 72
    return-void
.end method
