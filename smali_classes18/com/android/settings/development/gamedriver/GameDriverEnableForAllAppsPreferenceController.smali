.class public Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "GameDriverEnableForAllAppsPreferenceController.java"

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/development/gamedriver/GameDriverContentObserver$OnGameDriverContentChangedListener;
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnStart;
.implements Lcom/android/settingslib/core/lifecycle/events/OnStop;


# static fields
.field public static final GAME_DRIVER_ALL_APPS:I = 0x1

.field public static final GAME_DRIVER_DEFAULT:I = 0x0

.field public static final GAME_DRIVER_OFF:I = 0x3

.field public static final GAME_DRIVER_PRERELEASE_ALL_APPS:I = 0x2


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field mGameDriverContentObserver:Lcom/android/settings/development/gamedriver/GameDriverContentObserver;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private mPreference:Landroidx/preference/ListPreference;

.field private final mPreferenceDefault:Ljava/lang/String;

.field private final mPreferenceGameDriver:Ljava/lang/String;

.field private final mPreferencePrereleaseDriver:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .line 62
    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 63
    iput-object p1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContext:Landroid/content/Context;

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContentResolver:Landroid/content/ContentResolver;

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f120749

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferenceDefault:Ljava/lang/String;

    .line 68
    nop

    .line 69
    const v1, 0x7f12074a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferenceGameDriver:Ljava/lang/String;

    .line 70
    nop

    .line 71
    const v1, 0x7f12074b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferencePrereleaseDriver:Ljava/lang/String;

    .line 72
    new-instance v1, Lcom/android/settings/development/gamedriver/GameDriverContentObserver;

    new-instance v2, Landroid/os/Handler;

    .line 73
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v1, v2, p0}, Lcom/android/settings/development/gamedriver/GameDriverContentObserver;-><init>(Landroid/os/Handler;Lcom/android/settings/development/gamedriver/GameDriverContentObserver$OnGameDriverContentChangedListener;)V

    iput-object v1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mGameDriverContentObserver:Lcom/android/settings/development/gamedriver/GameDriverContentObserver;

    .line 74
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 88
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 89
    invoke-virtual {p0}, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    .line 90
    iget-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/ListPreference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    .line 91
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContext:Landroid/content/Context;

    .line 78
    invoke-static {v0}, Lcom/android/settingslib/development/DevelopmentSettingsEnabler;->isDevelopmentSettingsEnabled(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContentResolver:Landroid/content/ContentResolver;

    .line 79
    const-string v2, "game_driver_all_apps"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 82
    goto :goto_0

    .line 83
    :cond_0
    const/4 v1, 0x2

    .line 78
    :goto_0
    return v1
.end method

.method public onGameDriverContentChanged()V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreference:Landroidx/preference/ListPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->updateState(Landroidx/preference/Preference;)V

    .line 149
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 123
    move-object v0, p1

    check-cast v0, Landroidx/preference/ListPreference;

    .line 124
    .local v0, "listPref":Landroidx/preference/ListPreference;
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    .local v1, "value":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "game_driver_all_apps"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 128
    .local v2, "currentChoice":I
    iget-object v4, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferenceGameDriver:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 129
    const/4 v4, 0x1

    .local v4, "userChoice":I
    goto :goto_0

    .line 130
    .end local v4    # "userChoice":I
    :cond_0
    iget-object v4, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferencePrereleaseDriver:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 131
    const/4 v4, 0x2

    .restart local v4    # "userChoice":I
    goto :goto_0

    .line 133
    .end local v4    # "userChoice":I
    :cond_1
    const/4 v4, 0x0

    .line 135
    .restart local v4    # "userChoice":I
    :goto_0
    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 138
    if-eq v4, v2, :cond_2

    .line 139
    iget-object v5, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v5, v3, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 143
    :cond_2
    const/4 v3, 0x1

    return v3
.end method

.method public onStart()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mGameDriverContentObserver:Lcom/android/settings/development/gamedriver/GameDriverContentObserver;

    iget-object v1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1}, Lcom/android/settings/development/gamedriver/GameDriverContentObserver;->register(Landroid/content/ContentResolver;)V

    .line 96
    return-void
.end method

.method public onStop()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mGameDriverContentObserver:Lcom/android/settings/development/gamedriver/GameDriverContentObserver;

    iget-object v1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1}, Lcom/android/settings/development/gamedriver/GameDriverContentObserver;->unregister(Landroid/content/ContentResolver;)V

    .line 101
    return-void
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 4
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 105
    move-object v0, p1

    check-cast v0, Landroidx/preference/ListPreference;

    .line 106
    .local v0, "listPref":Landroidx/preference/ListPreference;
    invoke-virtual {p0}, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->isAvailable()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/ListPreference;->setVisible(Z)V

    .line 107
    iget-object v1, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "game_driver_all_apps"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 109
    .local v1, "currentChoice":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferenceGameDriver:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 111
    iget-object v2, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferenceGameDriver:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 112
    :cond_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 113
    iget-object v2, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferencePrereleaseDriver:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 114
    iget-object v2, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferencePrereleaseDriver:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 116
    :cond_1
    iget-object v2, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferenceDefault:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 117
    iget-object v2, p0, Lcom/android/settings/development/gamedriver/GameDriverEnableForAllAppsPreferenceController;->mPreferenceDefault:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 119
    :goto_0
    return-void
.end method
