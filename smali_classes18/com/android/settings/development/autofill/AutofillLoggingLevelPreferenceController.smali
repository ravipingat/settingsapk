.class public final Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;
.super Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;
.source "AutofillLoggingLevelPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnDestroy;


# static fields
.field private static final AUTOFILL_LOGGING_LEVEL_KEY:Ljava/lang/String; = "autofill_logging_level"

.field private static final TAG:Ljava/lang/String; = "AutofillLoggingLevelPreferenceController"


# instance fields
.field private final mListSummaries:[Ljava/lang/String;

.field private final mListValues:[Ljava/lang/String;

.field private final mObserver:Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lifecycle"    # Lcom/android/settingslib/core/lifecycle/Lifecycle;

    .line 48
    invoke-direct {p0, p1}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f030013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mListValues:[Ljava/lang/String;

    .line 52
    const v1, 0x7f030012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mListSummaries:[Ljava/lang/String;

    .line 53
    new-instance v1, Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;

    iget-object v2, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/settings/development/autofill/-$$Lambda$AutofillLoggingLevelPreferenceController$93rEGR-Dx7YRc7YY0ip1MGrNqfU;

    invoke-direct {v3, p0}, Lcom/android/settings/development/autofill/-$$Lambda$AutofillLoggingLevelPreferenceController$93rEGR-Dx7YRc7YY0ip1MGrNqfU;-><init>(Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;)V

    invoke-direct {v1, v2, v3}, Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mObserver:Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;

    .line 54
    iget-object v1, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mObserver:Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;

    invoke-virtual {v1}, Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;->register()V

    .line 56
    if-eqz p2, :cond_0

    .line 57
    invoke-virtual {p2, p0}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 59
    :cond_0
    return-void
.end method

.method private updateOptions()V
    .locals 4

    .line 90
    iget-object v0, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mPreference:Landroidx/preference/Preference;

    if-nez v0, :cond_0

    .line 93
    const-string v0, "AutofillLoggingLevelPreferenceController"

    const-string v1, "ignoring Settings update because UI is gone"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget v1, Landroid/view/autofill/AutofillManager;->DEFAULT_LOGGING_LEVEL:I

    const-string v2, "autofill_logging_level"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 100
    .local v0, "level":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 101
    const/4 v1, 0x1

    .local v1, "index":I
    goto :goto_0

    .line 102
    .end local v1    # "index":I
    :cond_1
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 103
    const/4 v1, 0x2

    .restart local v1    # "index":I
    goto :goto_0

    .line 105
    .end local v1    # "index":I
    :cond_2
    const/4 v1, 0x0

    .line 107
    .restart local v1    # "index":I
    :goto_0
    iget-object v2, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v2, Landroidx/preference/ListPreference;

    .line 108
    .local v2, "listPreference":Landroidx/preference/ListPreference;
    iget-object v3, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mListValues:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 109
    iget-object v3, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mListSummaries:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method

.method private writeLevel(Ljava/lang/Object;)V
    .locals 3
    .param p1, "newValue"    # Ljava/lang/Object;

    .line 113
    const/4 v0, 0x0

    .line 114
    .local v0, "level":I
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 115
    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "autofill_logging_level"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 119
    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 68
    const-string v0, "autofill_logging_level"

    return-object v0
.end method

.method public synthetic lambda$new$0$AutofillLoggingLevelPreferenceController()V
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->updateOptions()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->mObserver:Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/development/autofill/AutofillDeveloperSettingsObserver;->unregister()V

    .line 64
    return-void
.end method

.method protected onDeveloperOptionsSwitchDisabled()V
    .locals 1

    .line 85
    invoke-super {p0}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;->onDeveloperOptionsSwitchDisabled()V

    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->writeLevel(Ljava/lang/Object;)V

    .line 87
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "preference"    # Landroidx/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .line 73
    invoke-direct {p0, p2}, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->writeLevel(Ljava/lang/Object;)V

    .line 74
    invoke-direct {p0}, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->updateOptions()V

    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 0
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 80
    invoke-direct {p0}, Lcom/android/settings/development/autofill/AutofillLoggingLevelPreferenceController;->updateOptions()V

    .line 81
    return-void
.end method
