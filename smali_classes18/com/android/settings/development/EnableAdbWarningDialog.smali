.class public Lcom/android/settings/development/EnableAdbWarningDialog;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;
.source "EnableAdbWarningDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field public static final TAG:Ljava/lang/String; = "EnableAdbDialog"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method public static show(Landroidx/fragment/app/Fragment;)V
    .locals 4
    .param p0, "host"    # Landroidx/fragment/app/Fragment;

    .line 37
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 38
    .local v0, "manager":Landroidx/fragment/app/FragmentManager;
    const-string v1, "EnableAdbDialog"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 39
    new-instance v2, Lcom/android/settings/development/EnableAdbWarningDialog;

    invoke-direct {v2}, Lcom/android/settings/development/EnableAdbWarningDialog;-><init>()V

    .line 40
    .local v2, "dialog":Lcom/android/settings/development/EnableAdbWarningDialog;
    const/4 v3, 0x0

    invoke-virtual {v2, p0, v3}, Lcom/android/settings/development/EnableAdbWarningDialog;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    .line 41
    invoke-virtual {v2, v0, v1}, Lcom/android/settings/development/EnableAdbWarningDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 43
    .end local v2    # "dialog":Lcom/android/settings/development/EnableAdbWarningDialog;
    :cond_0
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 47
    const/16 v0, 0x4c6

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .line 62
    invoke-virtual {p0}, Lcom/android/settings/development/EnableAdbWarningDialog;->getTargetFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/AdbDialogHost;

    .line 63
    .local v0, "host":Lcom/android/settings/development/AdbDialogHost;
    if-nez v0, :cond_0

    .line 64
    return-void

    .line 66
    :cond_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 67
    invoke-interface {v0}, Lcom/android/settings/development/AdbDialogHost;->onEnableAdbDialogConfirmed()V

    goto :goto_0

    .line 69
    :cond_1
    invoke-interface {v0}, Lcom/android/settings/development/AdbDialogHost;->onEnableAdbDialogDismissed()V

    .line 71
    :goto_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    new-instance v0, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/development/EnableAdbWarningDialog;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setTitle(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 54
    const v1, 0x7f1200de

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setMessage(I)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 55
    const v1, 0x1040013

    invoke-virtual {v0, v1, p0}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 56
    const v1, 0x1040009

    invoke-virtual {v0, v1, p0}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .line 75
    invoke-super {p0, p1}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 76
    invoke-virtual {p0}, Lcom/android/settings/development/EnableAdbWarningDialog;->getTargetFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/AdbDialogHost;

    .line 77
    .local v0, "host":Lcom/android/settings/development/AdbDialogHost;
    if-nez v0, :cond_0

    .line 78
    return-void

    .line 80
    :cond_0
    invoke-interface {v0}, Lcom/android/settings/development/AdbDialogHost;->onEnableAdbDialogDismissed()V

    .line 81
    return-void
.end method
