.class public Lcom/android/settings/wifi/tether/TetherDataObserver;
.super Landroid/database/ContentObserver;
.source "TetherDataObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/wifi/tether/TetherDataObserver$OnTetherDataChangeCallback;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/android/settings/wifi/tether/TetherDataObserver$OnTetherDataChangeCallback;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .line 20
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/android/settings/wifi/tether/TetherDataObserver$OnTetherDataChangeCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/android/settings/wifi/tether/TetherDataObserver$OnTetherDataChangeCallback;

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 25
    iput-object p1, p0, Lcom/android/settings/wifi/tether/TetherDataObserver;->mCallback:Lcom/android/settings/wifi/tether/TetherDataObserver$OnTetherDataChangeCallback;

    .line 26
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 30
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 31
    iget-object v0, p0, Lcom/android/settings/wifi/tether/TetherDataObserver;->mCallback:Lcom/android/settings/wifi/tether/TetherDataObserver$OnTetherDataChangeCallback;

    if-eqz v0, :cond_0

    .line 32
    invoke-interface {v0}, Lcom/android/settings/wifi/tether/TetherDataObserver$OnTetherDataChangeCallback;->onTetherDataChange()V

    .line 33
    :cond_0
    return-void
.end method
