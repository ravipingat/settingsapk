.class public Lcom/android/settings/wifi/tether/utils/TetherUtils;
.super Ljava/lang/Object;
.source "TetherUtils.java"


# static fields
.field public static final KEY_TETHER:Ljava/lang/String; = "TetheredData"

.field private static final TAG:Ljava/lang/String; = "TetherUtils"

.field public static final TYPE_DISABLE_ALL:I = 0x1

.field public static final TYPE_ENABLE_ALL:I = 0x3

.field public static final TYPE_ENABLE_WIFI:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static closeCursor(Landroid/database/Cursor;)V
    .locals 0
    .param p0, "cursor"    # Landroid/database/Cursor;

    .line 183
    if-eqz p0, :cond_0

    .line 184
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 186
    :cond_0
    return-void
.end method

.method public static getMccMnc(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 146
    if-nez p0, :cond_0

    .line 147
    const/4 v0, 0x0

    return-object v0

    .line 149
    :cond_0
    const/4 v0, 0x0

    .line 150
    .local v0, "mccmnc":Ljava/lang/String;
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 151
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_1

    .line 152
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "imsi":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x6

    if-le v3, v4, :cond_1

    .line 154
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 157
    .end local v2    # "imsi":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public static getTetherData(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 35
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x3

    const-string v2, "TetheredData"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 36
    .local v0, "tether_data":I
    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 37
    :cond_0
    const/4 v0, 0x3

    .line 39
    :cond_1
    return v0
.end method

.method public static isHaveProfile(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .line 67
    invoke-static {p0}, Lcom/android/settings/wifi/tether/utils/TetherUtils;->isSprintMccMnc(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 68
    return v1

    .line 70
    :cond_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 71
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 72
    return v2

    .line 74
    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    .line 75
    .local v3, "mccmnc":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 76
    return v2

    .line 78
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "apn"

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v7

    const/4 v4, 0x4

    new-array v9, v4, [Ljava/lang/String;

    const-string v4, "dun"

    aput-object v4, v9, v2

    aput-object v3, v9, v1

    const/4 v4, 0x2

    const-string v8, "0"

    aput-object v8, v9, v4

    const/4 v4, 0x3

    const-string v8, "3G_HOT"

    aput-object v8, v9, v4

    const/4 v10, 0x0

    const-string v8, "type = ? and numeric = ? and user_visible != ? and name != ?"

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 80
    .local v4, "cursor":Landroid/database/Cursor;
    if-eqz v4, :cond_3

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_3

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 82
    invoke-static {v4}, Lcom/android/settings/wifi/tether/utils/TetherUtils;->closeCursor(Landroid/database/Cursor;)V

    .line 83
    return v1

    .line 86
    :cond_3
    invoke-static {v4}, Lcom/android/settings/wifi/tether/utils/TetherUtils;->closeCursor(Landroid/database/Cursor;)V

    .line 87
    return v2
.end method

.method public static isNoSimCard(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 50
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    .line 52
    .local v1, "state":I
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_0

    const/16 v3, 0x8

    if-eq v1, v3, :cond_0

    const/4 v3, 0x6

    if-ne v1, v3, :cond_1

    .line 54
    :cond_0
    return v2

    .line 57
    .end local v1    # "state":I
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public static isSimStatusChange(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 114
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 115
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    .line 116
    .local v1, "state":I
    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    .line 125
    const/4 v2, 0x0

    return v2

    .line 118
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public static isSprintMccMnc(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 136
    invoke-static {p0}, Lcom/android/settings/wifi/tether/utils/TetherUtils;->getMccMnc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "mccmnc":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_2

    .line 138
    const-string v1, "310120"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "311870"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "311490"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 139
    const-string v1, "312530"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "310000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 138
    :cond_1
    return v2

    .line 141
    :cond_2
    return v2
.end method

.method public static isUssCDMA(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 167
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 168
    return v0

    .line 170
    :cond_0
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 171
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_2

    .line 172
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getVoiceNetworkType()I

    move-result v2

    .line 173
    .local v2, "type":I
    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    const/4 v3, 0x5

    if-eq v2, v3, :cond_1

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    const/4 v3, 0x7

    if-eq v2, v3, :cond_1

    const/16 v3, 0xc

    if-eq v2, v3, :cond_1

    const/16 v3, 0xe

    if-ne v2, v3, :cond_2

    .line 176
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 179
    .end local v2    # "type":I
    :cond_2
    return v0
.end method

.method public static showTertheringErrorDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .line 98
    new-instance v0, Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 99
    .local v0, "builder":Lcom/oneplus/lib/app/OPAlertDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    .line 100
    new-instance v1, Lcom/android/settings/wifi/tether/utils/TetherUtils$1;

    invoke-direct {v1}, Lcom/android/settings/wifi/tether/utils/TetherUtils$1;-><init>()V

    const v2, 0x7f120b98

    invoke-virtual {v0, v2, v1}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/oneplus/lib/app/OPAlertDialog$Builder;

    .line 105
    invoke-virtual {v0}, Lcom/oneplus/lib/app/OPAlertDialog$Builder;->create()Lcom/oneplus/lib/app/OPAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/oneplus/lib/app/OPAlertDialog;->show()V

    .line 106
    return-void
.end method
