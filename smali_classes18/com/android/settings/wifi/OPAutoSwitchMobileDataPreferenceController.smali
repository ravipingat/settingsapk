.class public Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;
.source "OPAutoSwitchMobileDataPreferenceController.java"

# interfaces
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnResume;
.implements Lcom/android/settingslib/core/lifecycle/events/OnPause;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;
    }
.end annotation


# static fields
.field private static final KEY_AUTO_SWITCH_MOBILE_DATA:Ljava/lang/String; = "auto_switch_mobile_data"

.field public static final WIFI_AUTO_CHANGE_TO_MOBILE_DATA:Ljava/lang/String; = "wifi_auto_change_to_mobile_data"


# instance fields
.field private mSettingObserver:Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lifecycle"    # Lcom/android/settingslib/core/lifecycle/Lifecycle;

    .line 53
    const-string v0, "auto_switch_mobile_data"

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2, p0}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 55
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 2
    .param p1, "screen"    # Landroidx/preference/PreferenceScreen;

    .line 59
    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    .line 60
    new-instance v0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;

    const-string v1, "auto_switch_mobile_data"

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;-><init>(Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;Landroidx/preference/Preference;)V

    iput-object v0, p0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;->mSettingObserver:Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;

    .line 61
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    .line 101
    const-string v0, "auto_switch_mobile_data"

    return-object v0
.end method

.method public handlePreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 84
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "auto_switch_mobile_data"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 85
    return v1

    .line 87
    :cond_0
    instance-of v0, p1, Landroidx/preference/SwitchPreference;

    if-nez v0, :cond_1

    .line 88
    return v1

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 92
    move-object v1, p1

    check-cast v1, Landroidx/preference/SwitchPreference;

    invoke-virtual {v1}, Landroidx/preference/SwitchPreference;->isChecked()Z

    move-result v1

    .line 90
    const-string v2, "wifi_auto_change_to_mobile_data"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 94
    invoke-static {}, Lcom/oneplus/settings/utils/OPUtils;->sendAppTrackerForDataAutoSwitch()V

    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 3

    .line 72
    iget-object v0, p0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;->mSettingObserver:Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;

    if-eqz v0, :cond_0

    .line 73
    iget-object v1, p0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;->register(Landroid/content/ContentResolver;Z)V

    .line 75
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;->mSettingObserver:Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;

    if-eqz v0, :cond_0

    .line 66
    iget-object v1, p0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController$SettingObserver;->register(Landroid/content/ContentResolver;Z)V

    .line 68
    :cond_0
    return-void
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 4
    .param p1, "preference"    # Landroidx/preference/Preference;

    .line 106
    instance-of v0, p1, Landroidx/preference/SwitchPreference;

    if-nez v0, :cond_0

    .line 107
    return-void

    .line 109
    :cond_0
    move-object v0, p1

    check-cast v0, Landroidx/preference/SwitchPreference;

    .line 111
    .local v0, "enableSwitchNetwork":Landroidx/preference/SwitchPreference;
    iget-object v1, p0, Lcom/android/settings/wifi/OPAutoSwitchMobileDataPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "wifi_auto_change_to_mobile_data"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    move v2, v3

    :cond_1
    invoke-virtual {v0, v2}, Landroidx/preference/SwitchPreference;->setChecked(Z)V

    .line 114
    return-void
.end method
