.class public Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;
.super Lcom/android/settings/core/InstrumentedActivity;
.source "WifiDppEnrolleeActivity.java"

# interfaces
.implements Lcom/android/settings/wifi/dpp/WifiDppQrCodeScannerFragment$OnScanWifiDppSuccessListener;


# static fields
.field public static final ACTION_ENROLLEE_QR_CODE_SCANNER:Ljava/lang/String; = "android.settings.WIFI_DPP_ENROLLEE_QR_CODE_SCANNER"

.field private static final TAG:Ljava/lang/String; = "WifiDppEnrolleeActivity"


# instance fields
.field private mFragmentManager:Landroidx/fragment/app/FragmentManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/android/settings/core/InstrumentedActivity;-><init>()V

    return-void
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .line 81
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const v3, 0x7c1bddca

    if-eq v1, v3, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    const-string v1, "android.settings.WIFI_DPP_ENROLLEE_QR_CODE_SCANNER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 87
    const-string v0, "WifiDppEnrolleeActivity"

    const-string v1, "Launch with an invalid action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->finish()V

    goto :goto_2

    .line 83
    :cond_2
    const-string v0, "ssid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "ssid":Ljava/lang/String;
    invoke-direct {p0, v2, v0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->showQrCodeScannerFragment(ZLjava/lang/String;)V

    .line 85
    nop

    .line 90
    .end local v0    # "ssid":Ljava/lang/String;
    :goto_2
    return-void
.end method

.method private showQrCodeScannerFragment(ZLjava/lang/String;)V
    .locals 4
    .param p1, "addToBackStack"    # Z
    .param p2, "ssid"    # Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->mFragmentManager:Landroidx/fragment/app/FragmentManager;

    .line 94
    const-string v1, "qr_code_scanner_fragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/dpp/WifiDppQrCodeScannerFragment;

    .line 97
    .local v0, "fragment":Lcom/android/settings/wifi/dpp/WifiDppQrCodeScannerFragment;
    if-nez v0, :cond_1

    .line 98
    new-instance v2, Lcom/android/settings/wifi/dpp/WifiDppQrCodeScannerFragment;

    invoke-direct {v2, p2}, Lcom/android/settings/wifi/dpp/WifiDppQrCodeScannerFragment;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    .line 109
    iget-object v2, p0, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->mFragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v2}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v2

    .line 111
    .local v2, "fragmentTransaction":Landroidx/fragment/app/FragmentTransaction;
    const v3, 0x7f0a0291

    invoke-virtual {v2, v3, v0, v1}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 113
    if-eqz p1, :cond_0

    .line 114
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroidx/fragment/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 116
    :cond_0
    invoke-virtual {v2}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    .line 117
    return-void

    .line 100
    .end local v2    # "fragmentTransaction":Landroidx/fragment/app/FragmentTransaction;
    :cond_1
    invoke-virtual {v0}, Lcom/android/settings/wifi/dpp/WifiDppQrCodeScannerFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 101
    return-void

    .line 106
    :cond_2
    iget-object v1, p0, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->mFragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->popBackStackImmediate()Z

    .line 107
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    .line 52
    const/16 v0, 0x63c

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 58
    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/oneplus/settings/utils/OPUtils;->getThemeMode(Landroid/content/ContentResolver;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/oneplus/settings/utils/OPUtils;->setLightNavigationBar(Landroid/view/Window;I)V

    .line 60
    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/setupcompat/util/WizardManagerHelper;->isAnySetupWizard(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const v0, 0x7f13012b

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->setTheme(I)V

    .line 66
    :cond_0
    const v0, 0x7f0d034c

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->setContentView(I)V

    .line 67
    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->mFragmentManager:Landroidx/fragment/app/FragmentManager;

    .line 69
    if-nez p1, :cond_1

    .line 70
    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->handleIntent(Landroid/content/Intent;)V

    .line 73
    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 74
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_2

    .line 75
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setElevation(F)V

    .line 76
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 78
    :cond_2
    return-void
.end method

.method public onNavigateUp()Z
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/android/settings/wifi/dpp/WifiDppEnrolleeActivity;->finish()V

    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public onScanWifiDppSuccess(Lcom/android/settings/wifi/dpp/WifiQrCode;)V
    .locals 0
    .param p1, "wifiQrCode"    # Lcom/android/settings/wifi/dpp/WifiQrCode;

    .line 128
    return-void
.end method
