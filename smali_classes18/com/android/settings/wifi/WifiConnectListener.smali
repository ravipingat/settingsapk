.class public Lcom/android/settings/wifi/WifiConnectListener;
.super Ljava/lang/Object;
.source "WifiConnectListener.java"

# interfaces
.implements Landroid/net/wifi/WifiManager$ActionListener;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/android/settings/wifi/WifiConnectListener;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 3
    .param p1, "reason"    # I

    .line 42
    iget-object v0, p0, Lcom/android/settings/wifi/WifiConnectListener;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 43
    const v1, 0x7f1217fc

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 47
    :cond_0
    return-void
.end method

.method public onSuccess()V
    .locals 0

    .line 38
    return-void
.end method
