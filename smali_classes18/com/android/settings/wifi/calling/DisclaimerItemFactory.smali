.class public final Lcom/android/settings/wifi/calling/DisclaimerItemFactory;
.super Ljava/lang/Object;
.source "DisclaimerItemFactory.java"


# annotations
.annotation build Lcom/android/internal/annotations/VisibleForTesting;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/content/Context;I)Ljava/util/List;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Lcom/android/settings/wifi/calling/DisclaimerItem;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-static {p0, p1}, Lcom/android/settings/wifi/calling/DisclaimerItemFactory;->getDisclaimerItemList(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    .line 42
    .local v0, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/wifi/calling/DisclaimerItem;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 43
    .local v1, "itr":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 44
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/wifi/calling/DisclaimerItem;

    .line 45
    .local v2, "item":Lcom/android/settings/wifi/calling/DisclaimerItem;
    invoke-virtual {v2}, Lcom/android/settings/wifi/calling/DisclaimerItem;->shouldShow()Z

    move-result v3

    if-nez v3, :cond_0

    .line 46
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 48
    .end local v2    # "item":Lcom/android/settings/wifi/calling/DisclaimerItem;
    :cond_0
    goto :goto_0

    .line 49
    :cond_1
    return-object v0
.end method

.method private static getDisclaimerItemList(Landroid/content/Context;I)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List<",
            "Lcom/android/settings/wifi/calling/DisclaimerItem;",
            ">;"
        }
    .end annotation

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v0, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/android/settings/wifi/calling/DisclaimerItem;>;"
    new-instance v1, Lcom/android/settings/wifi/calling/LocationPolicyDisclaimer;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/wifi/calling/LocationPolicyDisclaimer;-><init>(Landroid/content/Context;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    new-instance v1, Lcom/android/settings/wifi/calling/EmergencyCallLimitationDisclaimer;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/wifi/calling/EmergencyCallLimitationDisclaimer;-><init>(Landroid/content/Context;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    return-object v0
.end method
