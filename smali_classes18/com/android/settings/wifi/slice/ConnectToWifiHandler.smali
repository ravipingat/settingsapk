.class public Lcom/android/settings/wifi/slice/ConnectToWifiHandler;
.super Landroid/content/BroadcastReceiver;
.source "ConnectToWifiHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method connect(Landroid/content/Context;Lcom/android/settingslib/wifi/AccessPoint;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accessPoint"    # Lcom/android/settingslib/wifi/AccessPoint;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .line 61
    invoke-static {}, Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;->saveSession()V

    .line 62
    invoke-static {p2}, Lcom/android/settings/wifi/slice/WifiScanWorker;->saveClickedWifi(Lcom/android/settingslib/wifi/AccessPoint;)V

    .line 64
    new-instance v0, Lcom/android/settings/wifi/WifiConnectListener;

    invoke-direct {v0, p1}, Lcom/android/settings/wifi/WifiConnectListener;-><init>(Landroid/content/Context;)V

    .line 65
    .local v0, "connectListener":Lcom/android/settings/wifi/WifiConnectListener;
    invoke-static {p2}, Lcom/android/settings/wifi/WifiUtils;->getConnectingType(Lcom/android/settingslib/wifi/AccessPoint;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {p2, v0}, Lcom/android/settingslib/wifi/AccessPoint;->startOsuProvisioning(Landroid/net/wifi/WifiManager$ActionListener;)V

    .line 68
    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {p2}, Lcom/android/settingslib/wifi/AccessPoint;->generateOpenNetworkConfig()V

    .line 74
    :cond_2
    const-class v1, Landroid/net/wifi/WifiManager;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 75
    .local v1, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {p2}, Lcom/android/settingslib/wifi/AccessPoint;->getConfig()Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/net/wifi/WifiManager;->connect(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    .line 78
    .end local v1    # "wifiManager":Landroid/net/wifi/WifiManager;
    :goto_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 41
    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_2

    .line 45
    :cond_0
    const-string v0, "android.net.extra.NETWORK"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Network;

    .line 46
    .local v0, "network":Landroid/net/Network;
    const-string v1, "access_point_state"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 49
    .local v1, "accessPointState":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 50
    invoke-static {}, Lcom/android/settings/wifi/slice/WifiScanWorker;->clearClickedWifi()V

    .line 51
    const-class v2, Landroid/net/ConnectivityManager;

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 53
    .local v2, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v2, v0}, Landroid/net/ConnectivityManager;->startCaptivePortalApp(Landroid/net/Network;)V

    .end local v2    # "cm":Landroid/net/ConnectivityManager;
    goto :goto_0

    .line 54
    :cond_1
    if-eqz v1, :cond_2

    .line 55
    new-instance v2, Lcom/android/settingslib/wifi/AccessPoint;

    invoke-direct {v2, p1, v1}, Lcom/android/settingslib/wifi/AccessPoint;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {p0, p1, v2}, Lcom/android/settings/wifi/slice/ConnectToWifiHandler;->connect(Landroid/content/Context;Lcom/android/settingslib/wifi/AccessPoint;)V

    goto :goto_1

    .line 54
    :cond_2
    :goto_0
    nop

    .line 57
    :goto_1
    return-void

    .line 42
    .end local v0    # "network":Landroid/net/Network;
    .end local v1    # "accessPointState":Landroid/os/Bundle;
    :cond_3
    :goto_2
    return-void
.end method
