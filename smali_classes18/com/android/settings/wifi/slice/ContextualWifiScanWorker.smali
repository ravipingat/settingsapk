.class public Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;
.super Lcom/android/settings/wifi/slice/WifiScanWorker;
.source "ContextualWifiScanWorker.java"


# static fields
.field private static sActiveSession:J

.field private static sVisibleUiSessionToken:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/slice/WifiScanWorker;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 35
    return-void
.end method

.method public static newVisibleUiSession()V
    .locals 2

    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;->sVisibleUiSessionToken:J

    .line 46
    return-void
.end method

.method static saveSession()V
    .locals 2

    .line 49
    sget-wide v0, Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;->sVisibleUiSessionToken:J

    sput-wide v0, Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;->sActiveSession:J

    .line 50
    return-void
.end method


# virtual methods
.method protected clearClickedWifiOnSliceUnpinned()V
    .locals 0

    .line 55
    return-void
.end method

.method protected isSessionValid()Z
    .locals 4

    .line 59
    sget-wide v0, Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;->sVisibleUiSessionToken:J

    sget-wide v2, Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;->sActiveSession:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 60
    invoke-static {}, Lcom/android/settings/wifi/slice/ContextualWifiScanWorker;->clearClickedWifi()V

    .line 61
    const/4 v0, 0x0

    return v0

    .line 63
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
