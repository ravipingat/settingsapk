.class Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;
.super Landroid/widget/ArrayAdapter;
.source "NetworkRequestDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/wifi/NetworkRequestDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccessPointAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/android/settingslib/wifi/AccessPoint;",
        ">;"
    }
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mResourceId:I

.field final synthetic this$0:Lcom/android/settings/wifi/NetworkRequestDialogFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/wifi/NetworkRequestDialogFragment;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List<",
            "Lcom/android/settingslib/wifi/AccessPoint;",
            ">;)V"
        }
    .end annotation

    .line 360
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/android/settingslib/wifi/AccessPoint;>;"
    iput-object p1, p0, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->this$0:Lcom/android/settings/wifi/NetworkRequestDialogFragment;

    .line 361
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 362
    iput p3, p0, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->mResourceId:I

    .line 363
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 364
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 368
    const/16 v0, 0x8

    const/4 v1, 0x0

    if-nez p2, :cond_0

    .line 369
    iget-object v2, p0, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->mResourceId:I

    invoke-virtual {v2, v3, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 371
    const v2, 0x7f0a0725

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 373
    .local v2, "divider":Landroid/view/View;
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 376
    .end local v2    # "divider":Landroid/view/View;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settingslib/wifi/AccessPoint;

    .line 378
    .local v2, "accessPoint":Lcom/android/settingslib/wifi/AccessPoint;
    const v3, 0x1020016

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 379
    .local v3, "titleView":Landroid/widget/TextView;
    if-eqz v3, :cond_1

    .line 381
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 382
    invoke-virtual {v2}, Lcom/android/settingslib/wifi/AccessPoint;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    :cond_1
    const v4, 0x1020010

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 386
    .local v4, "summary":Landroid/widget/TextView;
    if-eqz v4, :cond_3

    .line 387
    invoke-virtual {v2}, Lcom/android/settingslib/wifi/AccessPoint;->getSettingsSummary()Ljava/lang/String;

    move-result-object v5

    .line 388
    .local v5, "summaryString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 389
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 391
    :cond_2
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 392
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    .end local v5    # "summaryString":Ljava/lang/String;
    :cond_3
    :goto_0
    const v0, 0x1020006

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/preference/internal/PreferenceImageView;

    .line 397
    .local v0, "imageView":Landroidx/preference/internal/PreferenceImageView;
    invoke-virtual {v2}, Lcom/android/settingslib/wifi/AccessPoint;->getLevel()I

    move-result v5

    .line 398
    .local v5, "level":I
    invoke-virtual {v2}, Lcom/android/settingslib/wifi/AccessPoint;->getWifiGeneration()I

    move-result v6

    .line 399
    .local v6, "generation":I
    invoke-virtual {v2}, Lcom/android/settingslib/wifi/AccessPoint;->isTwtSupported()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 400
    invoke-virtual {v2}, Lcom/android/settingslib/wifi/AccessPoint;->isVhtMax8SpatialStreamsSupported()Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    nop

    .line 401
    .local v1, "isReady":Z
    :goto_1
    if-eqz v0, :cond_5

    .line 402
    invoke-virtual {p0}, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 403
    invoke-static {v5, v6, v1}, Lcom/android/settingslib/Utils;->getWifiIconResource(IIZ)I

    move-result v8

    .line 402
    invoke-virtual {v7, v8}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 404
    .local v7, "drawable":Landroid/graphics/drawable/Drawable;
    nop

    .line 405
    invoke-virtual {p0}, Lcom/android/settings/wifi/NetworkRequestDialogFragment$AccessPointAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x1010429

    invoke-static {v8, v9}, Lcom/android/settingslib/Utils;->getColorAttr(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 404
    invoke-virtual {v7, v8}, Landroid/graphics/drawable/Drawable;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 406
    invoke-virtual {v0, v7}, Landroidx/preference/internal/PreferenceImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 409
    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    return-object p2
.end method
