.class public final Landroid/support/v7/appcompat/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static abc_action_bar_title_item:I

.field public static abc_action_bar_up_container:I

.field public static abc_action_menu_item_layout:I

.field public static abc_action_menu_layout:I

.field public static abc_action_mode_bar:I

.field public static abc_action_mode_close_item_material:I

.field public static abc_activity_chooser_view:I

.field public static abc_activity_chooser_view_list_item:I

.field public static abc_alert_dialog_button_bar_material:I

.field public static abc_alert_dialog_material:I

.field public static abc_alert_dialog_title_material:I

.field public static abc_cascading_menu_item_layout:I

.field public static abc_dialog_title_material:I

.field public static abc_expanded_menu_layout:I

.field public static abc_list_menu_item_checkbox:I

.field public static abc_list_menu_item_icon:I

.field public static abc_list_menu_item_layout:I

.field public static abc_list_menu_item_radio:I

.field public static abc_popup_menu_header_item_layout:I

.field public static abc_popup_menu_item_layout:I

.field public static abc_screen_content_include:I

.field public static abc_screen_simple:I

.field public static abc_screen_simple_overlay_action_mode:I

.field public static abc_screen_toolbar:I

.field public static abc_search_dropdown_item_icons_2line:I

.field public static abc_search_view:I

.field public static abc_select_dialog_material:I

.field public static abc_tooltip:I

.field public static breadcrumbs_in_fragment:I

.field public static breadcrumbs_in_fragment_material:I

.field public static design_bottom_navigation_item:I

.field public static notification_action:I

.field public static notification_action_tombstone:I

.field public static notification_media_action:I

.field public static notification_media_cancel_action:I

.field public static notification_template_big_media:I

.field public static notification_template_big_media_custom:I

.field public static notification_template_big_media_narrow:I

.field public static notification_template_big_media_narrow_custom:I

.field public static notification_template_custom_big:I

.field public static notification_template_icon_group:I

.field public static notification_template_lines_media:I

.field public static notification_template_media:I

.field public static notification_template_media_custom:I

.field public static notification_template_part_chronometer:I

.field public static notification_template_part_time:I

.field public static oneplus_preference_category_material_dark:I

.field public static oneplus_preference_category_material_light:I

.field public static oneplus_preference_material_dark:I

.field public static oneplus_preference_material_light:I

.field public static oneplus_preference_small_category_material:I

.field public static op_abc_action_bar_title_item:I

.field public static op_abc_action_menu_item_layout:I

.field public static op_abc_action_menu_layout:I

.field public static op_abc_action_mode_bar:I

.field public static op_abc_action_mode_close_item_material:I

.field public static op_abc_dialog_title_material:I

.field public static op_abc_expanded_menu_layout:I

.field public static op_abc_list_menu_item_checkbox:I

.field public static op_abc_list_menu_item_icon:I

.field public static op_abc_list_menu_item_layout:I

.field public static op_abc_list_menu_item_radio:I

.field public static op_abc_popup_menu_header_item_layout:I

.field public static op_abc_popup_menu_item_layout:I

.field public static op_abc_screen_content_include:I

.field public static op_abc_screen_simple:I

.field public static op_abc_screen_simple_overlay_action_mode:I

.field public static op_abc_screen_toolbar:I

.field public static op_abc_tooltip:I

.field public static op_alert_dialog_button_bar_material:I

.field public static op_alert_dialog_material:I

.field public static op_alert_dialog_title_material:I

.field public static op_alert_progress_dialog_button_bar_material:I

.field public static op_alert_progress_dialog_horizontal:I

.field public static op_alert_progress_dialog_material:I

.field public static op_alert_progress_dialog_spinner:I

.field public static op_alert_progress_dialog_title_material:I

.field public static op_ctrl_preference_blank:I

.field public static op_ctrl_preference_divider:I

.field public static op_date_picker_dialog:I

.field public static op_date_picker_header_material:I

.field public static op_date_picker_legacy:I

.field public static op_date_picker_legacy_holo:I

.field public static op_date_picker_material:I

.field public static op_date_picker_month_item_material:I

.field public static op_date_picker_view_animator_material:I

.field public static op_day_picker_content_material:I

.field public static op_design_layout_snackbar:I

.field public static op_design_layout_snackbar_include:I

.field public static op_design_text_input_password_icon:I

.field public static op_float_switch_button:I

.field public static op_layout_tab_icon:I

.field public static op_layout_tab_text:I

.field public static op_number_picker:I

.field public static op_number_picker_dialog:I

.field public static op_number_picker_material:I

.field public static op_preference:I

.field public static op_preference_category_material:I

.field public static op_preference_dialog_edittext_material:I

.field public static op_preference_dialog_seekbar_material:I

.field public static op_preference_header_item:I

.field public static op_preference_header_item_material:I

.field public static op_preference_information_material:I

.field public static op_preference_list_content:I

.field public static op_preference_list_content_material:I

.field public static op_preference_list_content_single:I

.field public static op_preference_list_fragment:I

.field public static op_preference_list_fragment_material:I

.field public static op_preference_material:I

.field public static op_preference_widget_checkbox:I

.field public static op_preference_widget_seekbar_material:I

.field public static op_preference_widget_switch:I

.field public static op_search_dropdown_item_icons_2line:I

.field public static op_search_view:I

.field public static op_select_dialog_item_material:I

.field public static op_select_dialog_material:I

.field public static op_select_dialog_multichoice_material:I

.field public static op_select_dialog_multichoice_material_light:I

.field public static op_select_dialog_singlechoice_material:I

.field public static op_select_dialog_singlechoice_material_light:I

.field public static op_simple_dropdown_hint:I

.field public static op_time_header_label:I

.field public static op_time_picker_dialog:I

.field public static op_time_picker_header_material:I

.field public static op_time_picker_holo:I

.field public static op_time_picker_legacy_material:I

.field public static op_time_picker_material:I

.field public static op_transient_notification:I

.field public static op_transient_notification_light:I

.field public static op_year_label_text_view:I

.field public static preference_widget_checkbox:I

.field public static preference_widget_seekbar:I

.field public static preference_widget_seekbar_material:I

.field public static select_dialog_item_material:I

.field public static select_dialog_multichoice_material:I

.field public static select_dialog_singlechoice_material:I

.field public static support_simple_spinner_dropdown_item:I

.field public static time_picker_text_input_material:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7427
    const/4 v0, 0x0

    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_action_bar_title_item:I

    .line 7428
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_action_bar_up_container:I

    .line 7429
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_action_menu_item_layout:I

    .line 7430
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_action_menu_layout:I

    .line 7431
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_action_mode_bar:I

    .line 7432
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_action_mode_close_item_material:I

    .line 7433
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_activity_chooser_view:I

    .line 7434
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_activity_chooser_view_list_item:I

    .line 7435
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_alert_dialog_button_bar_material:I

    .line 7436
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_alert_dialog_material:I

    .line 7437
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_alert_dialog_title_material:I

    .line 7438
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_cascading_menu_item_layout:I

    .line 7439
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_dialog_title_material:I

    .line 7440
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_expanded_menu_layout:I

    .line 7441
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_list_menu_item_checkbox:I

    .line 7442
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_list_menu_item_icon:I

    .line 7443
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_list_menu_item_layout:I

    .line 7444
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_list_menu_item_radio:I

    .line 7445
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_popup_menu_header_item_layout:I

    .line 7446
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_popup_menu_item_layout:I

    .line 7447
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_screen_content_include:I

    .line 7448
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_screen_simple:I

    .line 7449
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_screen_simple_overlay_action_mode:I

    .line 7450
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_screen_toolbar:I

    .line 7451
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_search_dropdown_item_icons_2line:I

    .line 7452
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_search_view:I

    .line 7453
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_select_dialog_material:I

    .line 7454
    sput v0, Landroid/support/v7/appcompat/R$layout;->abc_tooltip:I

    .line 7455
    sput v0, Landroid/support/v7/appcompat/R$layout;->breadcrumbs_in_fragment:I

    .line 7456
    sput v0, Landroid/support/v7/appcompat/R$layout;->breadcrumbs_in_fragment_material:I

    .line 7457
    sput v0, Landroid/support/v7/appcompat/R$layout;->design_bottom_navigation_item:I

    .line 7458
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_action:I

    .line 7459
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_action_tombstone:I

    .line 7460
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_media_action:I

    .line 7461
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_media_cancel_action:I

    .line 7462
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_big_media:I

    .line 7463
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_big_media_custom:I

    .line 7464
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_big_media_narrow:I

    .line 7465
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_big_media_narrow_custom:I

    .line 7466
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_custom_big:I

    .line 7467
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_icon_group:I

    .line 7468
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_lines_media:I

    .line 7469
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_media:I

    .line 7470
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_media_custom:I

    .line 7471
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_part_chronometer:I

    .line 7472
    sput v0, Landroid/support/v7/appcompat/R$layout;->notification_template_part_time:I

    .line 7473
    sput v0, Landroid/support/v7/appcompat/R$layout;->oneplus_preference_category_material_dark:I

    .line 7474
    sput v0, Landroid/support/v7/appcompat/R$layout;->oneplus_preference_category_material_light:I

    .line 7475
    sput v0, Landroid/support/v7/appcompat/R$layout;->oneplus_preference_material_dark:I

    .line 7476
    sput v0, Landroid/support/v7/appcompat/R$layout;->oneplus_preference_material_light:I

    .line 7477
    sput v0, Landroid/support/v7/appcompat/R$layout;->oneplus_preference_small_category_material:I

    .line 7478
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_action_bar_title_item:I

    .line 7479
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_action_menu_item_layout:I

    .line 7480
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_action_menu_layout:I

    .line 7481
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_action_mode_bar:I

    .line 7482
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_action_mode_close_item_material:I

    .line 7483
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_dialog_title_material:I

    .line 7484
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_expanded_menu_layout:I

    .line 7485
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_list_menu_item_checkbox:I

    .line 7486
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_list_menu_item_icon:I

    .line 7487
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_list_menu_item_layout:I

    .line 7488
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_list_menu_item_radio:I

    .line 7489
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_popup_menu_header_item_layout:I

    .line 7490
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_popup_menu_item_layout:I

    .line 7491
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_screen_content_include:I

    .line 7492
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_screen_simple:I

    .line 7493
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_screen_simple_overlay_action_mode:I

    .line 7494
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_screen_toolbar:I

    .line 7495
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_abc_tooltip:I

    .line 7496
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_dialog_button_bar_material:I

    .line 7497
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_dialog_material:I

    .line 7498
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_dialog_title_material:I

    .line 7499
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_progress_dialog_button_bar_material:I

    .line 7500
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_progress_dialog_horizontal:I

    .line 7501
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_progress_dialog_material:I

    .line 7502
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_progress_dialog_spinner:I

    .line 7503
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_alert_progress_dialog_title_material:I

    .line 7504
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_ctrl_preference_blank:I

    .line 7505
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_ctrl_preference_divider:I

    .line 7506
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_date_picker_dialog:I

    .line 7507
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_date_picker_header_material:I

    .line 7508
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_date_picker_legacy:I

    .line 7509
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_date_picker_legacy_holo:I

    .line 7510
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_date_picker_material:I

    .line 7511
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_date_picker_month_item_material:I

    .line 7512
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_date_picker_view_animator_material:I

    .line 7513
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_day_picker_content_material:I

    .line 7514
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_design_layout_snackbar:I

    .line 7515
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_design_layout_snackbar_include:I

    .line 7516
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_design_text_input_password_icon:I

    .line 7517
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_float_switch_button:I

    .line 7518
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_layout_tab_icon:I

    .line 7519
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_layout_tab_text:I

    .line 7520
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_number_picker:I

    .line 7521
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_number_picker_dialog:I

    .line 7522
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_number_picker_material:I

    .line 7523
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference:I

    .line 7524
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_category_material:I

    .line 7525
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_dialog_edittext_material:I

    .line 7526
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_dialog_seekbar_material:I

    .line 7527
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_header_item:I

    .line 7528
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_header_item_material:I

    .line 7529
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_information_material:I

    .line 7530
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_list_content:I

    .line 7531
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_list_content_material:I

    .line 7532
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_list_content_single:I

    .line 7533
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_list_fragment:I

    .line 7534
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_list_fragment_material:I

    .line 7535
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_material:I

    .line 7536
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_widget_checkbox:I

    .line 7537
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_widget_seekbar_material:I

    .line 7538
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_preference_widget_switch:I

    .line 7539
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_search_dropdown_item_icons_2line:I

    .line 7540
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_search_view:I

    .line 7541
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_select_dialog_item_material:I

    .line 7542
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_select_dialog_material:I

    .line 7543
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_select_dialog_multichoice_material:I

    .line 7544
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_select_dialog_multichoice_material_light:I

    .line 7545
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_select_dialog_singlechoice_material:I

    .line 7546
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_select_dialog_singlechoice_material_light:I

    .line 7547
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_simple_dropdown_hint:I

    .line 7548
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_time_header_label:I

    .line 7549
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_time_picker_dialog:I

    .line 7550
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_time_picker_header_material:I

    .line 7551
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_time_picker_holo:I

    .line 7552
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_time_picker_legacy_material:I

    .line 7553
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_time_picker_material:I

    .line 7554
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_transient_notification:I

    .line 7555
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_transient_notification_light:I

    .line 7556
    sput v0, Landroid/support/v7/appcompat/R$layout;->op_year_label_text_view:I

    .line 7557
    sput v0, Landroid/support/v7/appcompat/R$layout;->preference_widget_checkbox:I

    .line 7558
    sput v0, Landroid/support/v7/appcompat/R$layout;->preference_widget_seekbar:I

    .line 7559
    sput v0, Landroid/support/v7/appcompat/R$layout;->preference_widget_seekbar_material:I

    .line 7560
    sput v0, Landroid/support/v7/appcompat/R$layout;->select_dialog_item_material:I

    .line 7561
    sput v0, Landroid/support/v7/appcompat/R$layout;->select_dialog_multichoice_material:I

    .line 7562
    sput v0, Landroid/support/v7/appcompat/R$layout;->select_dialog_singlechoice_material:I

    .line 7563
    sput v0, Landroid/support/v7/appcompat/R$layout;->support_simple_spinner_dropdown_item:I

    .line 7564
    sput v0, Landroid/support/v7/appcompat/R$layout;->time_picker_text_input_material:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
