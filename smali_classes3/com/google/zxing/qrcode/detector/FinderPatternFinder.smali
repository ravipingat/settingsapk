.class public Lcom/google/zxing/qrcode/detector/FinderPatternFinder;
.super Ljava/lang/Object;
.source "FinderPatternFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/zxing/qrcode/detector/FinderPatternFinder$CenterComparator;,
        Lcom/google/zxing/qrcode/detector/FinderPatternFinder$FurthestFromAverageComparator;
    }
.end annotation


# static fields
.field private static final CENTER_QUORUM:I = 0x2

.field private static final INTEGER_MATH_SHIFT:I = 0x8

.field protected static final MAX_MODULES:I = 0x39

.field protected static final MIN_SKIP:I = 0x3


# instance fields
.field private final crossCheckStateCount:[I

.field private hasSkipped:Z

.field private final image:Lcom/google/zxing/common/BitMatrix;

.field private final possibleCenters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/zxing/qrcode/detector/FinderPattern;",
            ">;"
        }
    .end annotation
.end field

.field private final resultPointCallback:Lcom/google/zxing/ResultPointCallback;


# direct methods
.method public constructor <init>(Lcom/google/zxing/common/BitMatrix;)V
    .locals 1
    .param p1, "image"    # Lcom/google/zxing/common/BitMatrix;

    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;-><init>(Lcom/google/zxing/common/BitMatrix;Lcom/google/zxing/ResultPointCallback;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/google/zxing/common/BitMatrix;Lcom/google/zxing/ResultPointCallback;)V
    .locals 1
    .param p1, "image"    # Lcom/google/zxing/common/BitMatrix;
    .param p2, "resultPointCallback"    # Lcom/google/zxing/ResultPointCallback;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->image:Lcom/google/zxing/common/BitMatrix;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    .line 65
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->crossCheckStateCount:[I

    .line 66
    iput-object p2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->resultPointCallback:Lcom/google/zxing/ResultPointCallback;

    .line 67
    return-void
.end method

.method private static centerFromEnd([II)F
    .locals 3
    .param p0, "stateCount"    # [I
    .param p1, "end"    # I

    .line 191
    const/4 v0, 0x4

    aget v0, p0, v0

    sub-int v0, p1, v0

    const/4 v1, 0x3

    aget v1, p0, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x2

    aget v1, p0, v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method private crossCheckHorizontal(IIII)F
    .locals 11
    .param p1, "startJ"    # I
    .param p2, "centerI"    # I
    .param p3, "maxCount"    # I
    .param p4, "originalStateCountTotal"    # I

    .line 315
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->image:Lcom/google/zxing/common/BitMatrix;

    .line 317
    .local v0, "image":Lcom/google/zxing/common/BitMatrix;
    invoke-virtual {v0}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v1

    .line 318
    .local v1, "maxJ":I
    invoke-direct {p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->getCrossCheckStateCount()[I

    move-result-object v2

    .line 320
    .local v2, "stateCount":[I
    move v3, p1

    .line 321
    .local v3, "j":I
    :goto_0
    const/4 v4, 0x2

    const/4 v5, 0x1

    if-ltz v3, :cond_0

    invoke-virtual {v0, v3, p2}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 322
    aget v6, v2, v4

    add-int/2addr v6, v5

    aput v6, v2, v4

    .line 323
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 325
    :cond_0
    const/high16 v6, 0x7fc00000    # Float.NaN

    if-gez v3, :cond_1

    .line 326
    return v6

    .line 328
    :cond_1
    :goto_1
    if-ltz v3, :cond_2

    invoke-virtual {v0, v3, p2}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v7

    if-nez v7, :cond_2

    aget v7, v2, v5

    if-gt v7, p3, :cond_2

    .line 329
    aget v7, v2, v5

    add-int/2addr v7, v5

    aput v7, v2, v5

    .line 330
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 332
    :cond_2
    if-ltz v3, :cond_f

    aget v7, v2, v5

    if-le v7, p3, :cond_3

    goto/16 :goto_7

    .line 335
    :cond_3
    :goto_2
    const/4 v7, 0x0

    if-ltz v3, :cond_4

    invoke-virtual {v0, v3, p2}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v8

    if-eqz v8, :cond_4

    aget v8, v2, v7

    if-gt v8, p3, :cond_4

    .line 336
    aget v8, v2, v7

    add-int/2addr v8, v5

    aput v8, v2, v7

    .line 337
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 339
    :cond_4
    aget v8, v2, v7

    if-le v8, p3, :cond_5

    .line 340
    return v6

    .line 343
    :cond_5
    add-int/lit8 v3, p1, 0x1

    .line 344
    :goto_3
    if-ge v3, v1, :cond_6

    invoke-virtual {v0, v3, p2}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 345
    aget v8, v2, v4

    add-int/2addr v8, v5

    aput v8, v2, v4

    .line 346
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 348
    :cond_6
    if-ne v3, v1, :cond_7

    .line 349
    return v6

    .line 351
    :cond_7
    :goto_4
    const/4 v8, 0x3

    if-ge v3, v1, :cond_8

    invoke-virtual {v0, v3, p2}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v9

    if-nez v9, :cond_8

    aget v9, v2, v8

    if-ge v9, p3, :cond_8

    .line 352
    aget v9, v2, v8

    add-int/2addr v9, v5

    aput v9, v2, v8

    .line 353
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 355
    :cond_8
    if-eq v3, v1, :cond_e

    aget v9, v2, v8

    if-lt v9, p3, :cond_9

    goto :goto_6

    .line 358
    :cond_9
    :goto_5
    const/4 v9, 0x4

    if-ge v3, v1, :cond_a

    invoke-virtual {v0, v3, p2}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v10

    if-eqz v10, :cond_a

    aget v10, v2, v9

    if-ge v10, p3, :cond_a

    .line 359
    aget v10, v2, v9

    add-int/2addr v10, v5

    aput v10, v2, v9

    .line 360
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 362
    :cond_a
    aget v10, v2, v9

    if-lt v10, p3, :cond_b

    .line 363
    return v6

    .line 368
    :cond_b
    aget v7, v2, v7

    aget v5, v2, v5

    add-int/2addr v7, v5

    aget v4, v2, v4

    add-int/2addr v7, v4

    aget v4, v2, v8

    add-int/2addr v7, v4

    aget v4, v2, v9

    add-int/2addr v7, v4

    .line 370
    .local v7, "stateCountTotal":I
    sub-int v4, v7, p4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x5

    if-lt v4, p4, :cond_c

    .line 371
    return v6

    .line 374
    :cond_c
    invoke-static {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->foundPatternCross([I)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-static {v2, v3}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->centerFromEnd([II)F

    move-result v6

    :cond_d
    return v6

    .line 356
    .end local v7    # "stateCountTotal":I
    :cond_e
    :goto_6
    return v6

    .line 333
    :cond_f
    :goto_7
    return v6
.end method

.method private crossCheckVertical(IIII)F
    .locals 11
    .param p1, "startI"    # I
    .param p2, "centerJ"    # I
    .param p3, "maxCount"    # I
    .param p4, "originalStateCountTotal"    # I

    .line 243
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->image:Lcom/google/zxing/common/BitMatrix;

    .line 245
    .local v0, "image":Lcom/google/zxing/common/BitMatrix;
    invoke-virtual {v0}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v1

    .line 246
    .local v1, "maxI":I
    invoke-direct {p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->getCrossCheckStateCount()[I

    move-result-object v2

    .line 249
    .local v2, "stateCount":[I
    move v3, p1

    .line 250
    .local v3, "i":I
    :goto_0
    const/4 v4, 0x2

    const/4 v5, 0x1

    if-ltz v3, :cond_0

    invoke-virtual {v0, p2, v3}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 251
    aget v6, v2, v4

    add-int/2addr v6, v5

    aput v6, v2, v4

    .line 252
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 254
    :cond_0
    const/high16 v6, 0x7fc00000    # Float.NaN

    if-gez v3, :cond_1

    .line 255
    return v6

    .line 257
    :cond_1
    :goto_1
    if-ltz v3, :cond_2

    invoke-virtual {v0, p2, v3}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v7

    if-nez v7, :cond_2

    aget v7, v2, v5

    if-gt v7, p3, :cond_2

    .line 258
    aget v7, v2, v5

    add-int/2addr v7, v5

    aput v7, v2, v5

    .line 259
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 262
    :cond_2
    if-ltz v3, :cond_f

    aget v7, v2, v5

    if-le v7, p3, :cond_3

    goto/16 :goto_7

    .line 265
    :cond_3
    :goto_2
    const/4 v7, 0x0

    if-ltz v3, :cond_4

    invoke-virtual {v0, p2, v3}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v8

    if-eqz v8, :cond_4

    aget v8, v2, v7

    if-gt v8, p3, :cond_4

    .line 266
    aget v8, v2, v7

    add-int/2addr v8, v5

    aput v8, v2, v7

    .line 267
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 269
    :cond_4
    aget v8, v2, v7

    if-le v8, p3, :cond_5

    .line 270
    return v6

    .line 274
    :cond_5
    add-int/lit8 v3, p1, 0x1

    .line 275
    :goto_3
    if-ge v3, v1, :cond_6

    invoke-virtual {v0, p2, v3}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 276
    aget v8, v2, v4

    add-int/2addr v8, v5

    aput v8, v2, v4

    .line 277
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 279
    :cond_6
    if-ne v3, v1, :cond_7

    .line 280
    return v6

    .line 282
    :cond_7
    :goto_4
    const/4 v8, 0x3

    if-ge v3, v1, :cond_8

    invoke-virtual {v0, p2, v3}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v9

    if-nez v9, :cond_8

    aget v9, v2, v8

    if-ge v9, p3, :cond_8

    .line 283
    aget v9, v2, v8

    add-int/2addr v9, v5

    aput v9, v2, v8

    .line 284
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 286
    :cond_8
    if-eq v3, v1, :cond_e

    aget v9, v2, v8

    if-lt v9, p3, :cond_9

    goto :goto_6

    .line 289
    :cond_9
    :goto_5
    const/4 v9, 0x4

    if-ge v3, v1, :cond_a

    invoke-virtual {v0, p2, v3}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v10

    if-eqz v10, :cond_a

    aget v10, v2, v9

    if-ge v10, p3, :cond_a

    .line 290
    aget v10, v2, v9

    add-int/2addr v10, v5

    aput v10, v2, v9

    .line 291
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 293
    :cond_a
    aget v10, v2, v9

    if-lt v10, p3, :cond_b

    .line 294
    return v6

    .line 299
    :cond_b
    aget v7, v2, v7

    aget v5, v2, v5

    add-int/2addr v7, v5

    aget v4, v2, v4

    add-int/2addr v7, v4

    aget v4, v2, v8

    add-int/2addr v7, v4

    aget v4, v2, v9

    add-int/2addr v7, v4

    .line 301
    .local v7, "stateCountTotal":I
    sub-int v4, v7, p4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x5

    mul-int/lit8 v5, p4, 0x2

    if-lt v4, v5, :cond_c

    .line 302
    return v6

    .line 305
    :cond_c
    invoke-static {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->foundPatternCross([I)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-static {v2, v3}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->centerFromEnd([II)F

    move-result v6

    :cond_d
    return v6

    .line 287
    .end local v7    # "stateCountTotal":I
    :cond_e
    :goto_6
    return v6

    .line 263
    :cond_f
    :goto_7
    return v6
.end method

.method private findRowSkip()I
    .locals 8

    .line 433
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 434
    .local v0, "max":I
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_0

    .line 435
    return v1

    .line 437
    :cond_0
    const/4 v3, 0x0

    .line 438
    .local v3, "firstConfirmedCenter":Lcom/google/zxing/qrcode/detector/FinderPattern;
    iget-object v4, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 439
    .local v5, "center":Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-virtual {v5}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getCount()I

    move-result v6

    const/4 v7, 0x2

    if-lt v6, v7, :cond_2

    .line 440
    if-nez v3, :cond_1

    .line 441
    move-object v3, v5

    goto :goto_1

    .line 448
    :cond_1
    iput-boolean v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->hasSkipped:Z

    .line 449
    invoke-virtual {v3}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getX()F

    move-result v1

    invoke-virtual {v5}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v3}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getY()F

    move-result v2

    invoke-virtual {v5}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getY()F

    move-result v6

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    div-int/2addr v1, v7

    return v1

    .line 453
    .end local v5    # "center":Lcom/google/zxing/qrcode/detector/FinderPattern;
    :cond_2
    :goto_1
    goto :goto_0

    .line 454
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    return v1
.end method

.method protected static foundPatternCross([I)Z
    .locals 7
    .param p0, "stateCount"    # [I

    .line 200
    const/4 v0, 0x0

    .line 201
    .local v0, "totalModuleSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x5

    const/4 v3, 0x0

    if-ge v1, v2, :cond_1

    .line 202
    aget v2, p0, v1

    .line 203
    .local v2, "count":I
    if-nez v2, :cond_0

    .line 204
    return v3

    .line 206
    :cond_0
    add-int/2addr v0, v2

    .line 201
    .end local v2    # "count":I
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    .end local v1    # "i":I
    :cond_1
    const/4 v1, 0x7

    if-ge v0, v1, :cond_2

    .line 209
    return v3

    .line 211
    :cond_2
    shl-int/lit8 v2, v0, 0x8

    div-int/2addr v2, v1

    .line 212
    .local v2, "moduleSize":I
    div-int/lit8 v1, v2, 0x2

    .line 214
    .local v1, "maxVariance":I
    aget v4, p0, v3

    shl-int/lit8 v4, v4, 0x8

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v1, :cond_3

    aget v4, p0, v5

    shl-int/lit8 v4, v4, 0x8

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v4, v1, :cond_3

    mul-int/lit8 v4, v2, 0x3

    const/4 v6, 0x2

    aget v6, p0, v6

    shl-int/lit8 v6, v6, 0x8

    sub-int/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v6, v1, 0x3

    if-ge v4, v6, :cond_3

    const/4 v4, 0x3

    aget v4, p0, v4

    shl-int/lit8 v4, v4, 0x8

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v4, v1, :cond_3

    const/4 v4, 0x4

    aget v4, p0, v4

    shl-int/lit8 v4, v4, 0x8

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v4, v1, :cond_3

    move v3, v5

    :cond_3
    return v3
.end method

.method private getCrossCheckStateCount()[I
    .locals 3

    .line 222
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->crossCheckStateCount:[I

    const/4 v1, 0x0

    aput v1, v0, v1

    .line 223
    const/4 v2, 0x1

    aput v1, v0, v2

    .line 224
    const/4 v2, 0x2

    aput v1, v0, v2

    .line 225
    const/4 v2, 0x3

    aput v1, v0, v2

    .line 226
    const/4 v2, 0x4

    aput v1, v0, v2

    .line 227
    return-object v0
.end method

.method private haveMultiplyConfirmedCenters()Z
    .locals 9

    .line 463
    const/4 v0, 0x0

    .line 464
    .local v0, "confirmedCount":I
    const/4 v1, 0x0

    .line 465
    .local v1, "totalModuleSize":F
    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 466
    .local v2, "max":I
    iget-object v3, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 467
    .local v4, "pattern":Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-virtual {v4}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getCount()I

    move-result v5

    const/4 v6, 0x2

    if-lt v5, v6, :cond_0

    .line 468
    add-int/lit8 v0, v0, 0x1

    .line 469
    invoke-virtual {v4}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v5

    add-float/2addr v1, v5

    .line 471
    .end local v4    # "pattern":Lcom/google/zxing/qrcode/detector/FinderPattern;
    :cond_0
    goto :goto_0

    .line 472
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    const/4 v3, 0x3

    const/4 v4, 0x0

    if-ge v0, v3, :cond_2

    .line 473
    return v4

    .line 479
    :cond_2
    int-to-float v3, v2

    div-float v3, v1, v3

    .line 480
    .local v3, "average":F
    const/4 v5, 0x0

    .line 481
    .local v5, "totalDeviation":F
    iget-object v6, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 482
    .local v7, "pattern":Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-virtual {v7}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v8

    sub-float/2addr v8, v3

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    add-float/2addr v5, v8

    .line 483
    .end local v7    # "pattern":Lcom/google/zxing/qrcode/detector/FinderPattern;
    goto :goto_1

    .line 484
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_3
    const v6, 0x3d4ccccd    # 0.05f

    mul-float/2addr v6, v1

    cmpg-float v6, v5, v6

    if-gtz v6, :cond_4

    const/4 v4, 0x1

    :cond_4
    return v4
.end method

.method private selectBestPatterns()[Lcom/google/zxing/qrcode/detector/FinderPattern;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 495
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 496
    .local v0, "startSize":I
    const/4 v1, 0x3

    if-lt v0, v1, :cond_5

    .line 502
    const/4 v2, 0x0

    const/4 v3, 0x1

    if-le v0, v1, :cond_2

    .line 504
    const/4 v4, 0x0

    .line 505
    .local v4, "totalModuleSize":F
    const/4 v5, 0x0

    .line 506
    .local v5, "square":F
    iget-object v6, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 507
    .local v7, "center":Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-virtual {v7}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v8

    .line 508
    .local v8, "size":F
    add-float/2addr v4, v8

    .line 509
    mul-float v9, v8, v8

    add-float/2addr v5, v9

    .line 510
    .end local v7    # "center":Lcom/google/zxing/qrcode/detector/FinderPattern;
    .end local v8    # "size":F
    goto :goto_0

    .line 511
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_0
    int-to-float v6, v0

    div-float v6, v4, v6

    .line 512
    .local v6, "average":F
    int-to-float v7, v0

    div-float v7, v5, v7

    mul-float v8, v6, v6

    sub-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    double-to-float v7, v7

    .line 514
    .local v7, "stdDev":F
    iget-object v8, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    new-instance v9, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$FurthestFromAverageComparator;

    invoke-direct {v9, v6, v2}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$FurthestFromAverageComparator;-><init>(FLcom/google/zxing/qrcode/detector/FinderPatternFinder$1;)V

    invoke-static {v8, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 516
    const v8, 0x3e4ccccd    # 0.2f

    mul-float/2addr v8, v6

    invoke-static {v8, v7}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 518
    .local v8, "limit":F
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    iget-object v10, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_2

    iget-object v10, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-le v10, v1, :cond_2

    .line 519
    iget-object v10, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 520
    .local v10, "pattern":Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-virtual {v10}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v11

    sub-float/2addr v11, v6

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    cmpl-float v11, v11, v8

    if-lez v11, :cond_1

    .line 521
    iget-object v11, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v11, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 522
    add-int/lit8 v9, v9, -0x1

    .line 518
    .end local v10    # "pattern":Lcom/google/zxing/qrcode/detector/FinderPattern;
    :cond_1
    add-int/2addr v9, v3

    goto :goto_1

    .line 527
    .end local v4    # "totalModuleSize":F
    .end local v5    # "square":F
    .end local v6    # "average":F
    .end local v7    # "stdDev":F
    .end local v8    # "limit":F
    .end local v9    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v1, :cond_4

    .line 530
    const/4 v4, 0x0

    .line 531
    .restart local v4    # "totalModuleSize":F
    iget-object v5, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 532
    .local v6, "possibleCenter":Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-virtual {v6}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v7

    add-float/2addr v4, v7

    .line 533
    .end local v6    # "possibleCenter":Lcom/google/zxing/qrcode/detector/FinderPattern;
    goto :goto_2

    .line 535
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v5, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v4, v5

    .line 537
    .local v5, "average":F
    iget-object v6, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    new-instance v7, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$CenterComparator;

    invoke-direct {v7, v5, v2}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$CenterComparator;-><init>(FLcom/google/zxing/qrcode/detector/FinderPatternFinder$1;)V

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 539
    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v2, v1, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 542
    .end local v4    # "totalModuleSize":F
    .end local v5    # "average":F
    :cond_4
    new-array v1, v1, [Lcom/google/zxing/qrcode/detector/FinderPattern;

    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/zxing/qrcode/detector/FinderPattern;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/zxing/qrcode/detector/FinderPattern;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/zxing/qrcode/detector/FinderPattern;

    aput-object v2, v1, v3

    return-object v1

    .line 498
    :cond_5
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method final find(Ljava/util/Map;)Lcom/google/zxing/qrcode/detector/FinderPatternInfo;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/google/zxing/DecodeHintType;",
            "*>;)",
            "Lcom/google/zxing/qrcode/detector/FinderPatternInfo;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 78
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    sget-object v4, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v3

    .line 79
    .local v4, "tryHarder":Z
    :goto_0
    iget-object v5, v0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->image:Lcom/google/zxing/common/BitMatrix;

    invoke-virtual {v5}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v5

    .line 80
    .local v5, "maxI":I
    iget-object v6, v0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->image:Lcom/google/zxing/common/BitMatrix;

    invoke-virtual {v6}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v6

    .line 88
    .local v6, "maxJ":I
    mul-int/lit8 v7, v5, 0x3

    div-int/lit16 v7, v7, 0xe4

    .line 89
    .local v7, "iSkip":I
    const/4 v8, 0x3

    if-lt v7, v8, :cond_1

    if-eqz v4, :cond_2

    .line 90
    :cond_1
    const/4 v7, 0x3

    .line 93
    :cond_2
    const/4 v9, 0x0

    .line 94
    .local v9, "done":Z
    const/4 v10, 0x5

    new-array v10, v10, [I

    .line 95
    .local v10, "stateCount":[I
    add-int/lit8 v11, v7, -0x1

    .local v11, "i":I
    :goto_1
    if-ge v11, v5, :cond_d

    if-nez v9, :cond_d

    .line 97
    aput v3, v10, v3

    .line 98
    aput v3, v10, v2

    .line 99
    const/4 v12, 0x2

    aput v3, v10, v12

    .line 100
    aput v3, v10, v8

    .line 101
    const/4 v13, 0x4

    aput v3, v10, v13

    .line 102
    const/4 v14, 0x0

    .line 103
    .local v14, "currentState":I
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_2
    if-ge v15, v6, :cond_b

    .line 104
    iget-object v8, v0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->image:Lcom/google/zxing/common/BitMatrix;

    invoke-virtual {v8, v15, v11}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 106
    and-int/lit8 v8, v14, 0x1

    if-ne v8, v2, :cond_3

    .line 107
    add-int/lit8 v14, v14, 0x1

    .line 109
    :cond_3
    aget v8, v10, v14

    add-int/2addr v8, v2

    aput v8, v10, v14

    move v8, v2

    move v2, v13

    const/4 v13, 0x3

    goto/16 :goto_4

    .line 111
    :cond_4
    and-int/lit8 v8, v14, 0x1

    if-nez v8, :cond_a

    .line 112
    if-ne v14, v13, :cond_9

    .line 113
    invoke-static {v10}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->foundPatternCross([I)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 114
    invoke-virtual {v0, v10, v11, v15}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->handlePossibleCenter([III)Z

    move-result v8

    .line 115
    .local v8, "confirmed":Z
    if-eqz v8, :cond_7

    .line 118
    const/4 v7, 0x2

    .line 119
    iget-boolean v13, v0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->hasSkipped:Z

    if-eqz v13, :cond_5

    .line 120
    invoke-direct/range {p0 .. p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->haveMultiplyConfirmedCenters()Z

    move-result v9

    goto :goto_3

    .line 122
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->findRowSkip()I

    move-result v13

    .line 123
    .local v13, "rowSkip":I
    aget v2, v10, v12

    if-le v13, v2, :cond_6

    .line 132
    aget v2, v10, v12

    sub-int v2, v13, v2

    sub-int/2addr v2, v7

    add-int/2addr v11, v2

    .line 133
    add-int/lit8 v15, v6, -0x1

    .line 135
    .end local v13    # "rowSkip":I
    :cond_6
    nop

    .line 146
    :goto_3
    const/4 v2, 0x0

    .line 147
    .end local v14    # "currentState":I
    .local v2, "currentState":I
    aput v3, v10, v3

    .line 148
    const/4 v13, 0x1

    aput v3, v10, v13

    .line 149
    aput v3, v10, v12

    .line 150
    const/4 v13, 0x3

    aput v3, v10, v13

    .line 151
    const/16 v16, 0x4

    aput v3, v10, v16

    .line 152
    .end local v8    # "confirmed":Z
    move v14, v2

    move/from16 v2, v16

    const/4 v8, 0x1

    goto :goto_4

    .line 137
    .end local v2    # "currentState":I
    .restart local v8    # "confirmed":Z
    .restart local v14    # "currentState":I
    :cond_7
    move/from16 v16, v13

    const/4 v13, 0x3

    aget v2, v10, v12

    aput v2, v10, v3

    .line 138
    aget v2, v10, v13

    const/16 v17, 0x1

    aput v2, v10, v17

    .line 139
    aget v2, v10, v16

    aput v2, v10, v12

    .line 140
    aput v17, v10, v13

    .line 141
    aput v3, v10, v16

    .line 142
    const/4 v2, 0x3

    .line 143
    .end local v14    # "currentState":I
    .restart local v2    # "currentState":I
    move v14, v2

    const/4 v2, 0x4

    const/4 v8, 0x1

    goto :goto_4

    .line 153
    .end local v2    # "currentState":I
    .end local v8    # "confirmed":Z
    .restart local v14    # "currentState":I
    :cond_8
    const/4 v13, 0x3

    aget v2, v10, v12

    aput v2, v10, v3

    .line 154
    aget v2, v10, v13

    const/4 v8, 0x1

    aput v2, v10, v8

    .line 155
    const/4 v2, 0x4

    aget v16, v10, v2

    aput v16, v10, v12

    .line 156
    aput v8, v10, v13

    .line 157
    aput v3, v10, v2

    .line 158
    const/4 v14, 0x3

    goto :goto_4

    .line 161
    :cond_9
    move v8, v2

    move v2, v13

    const/4 v13, 0x3

    add-int/lit8 v14, v14, 0x1

    aget v16, v10, v14

    add-int/lit8 v16, v16, 0x1

    aput v16, v10, v14

    goto :goto_4

    .line 164
    :cond_a
    move v8, v2

    move v2, v13

    const/4 v13, 0x3

    aget v16, v10, v14

    add-int/lit8 v16, v16, 0x1

    aput v16, v10, v14

    .line 103
    :goto_4
    add-int/2addr v15, v8

    move/from16 v18, v13

    move v13, v2

    move v2, v8

    move/from16 v8, v18

    goto/16 :goto_2

    :cond_b
    move v13, v8

    move v8, v2

    .line 168
    .end local v15    # "j":I
    invoke-static {v10}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->foundPatternCross([I)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 169
    invoke-virtual {v0, v10, v11, v6}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->handlePossibleCenter([III)Z

    move-result v2

    .line 170
    .local v2, "confirmed":Z
    if-eqz v2, :cond_c

    .line 171
    aget v7, v10, v3

    .line 172
    iget-boolean v12, v0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->hasSkipped:Z

    if-eqz v12, :cond_c

    .line 174
    invoke-direct/range {p0 .. p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->haveMultiplyConfirmedCenters()Z

    move-result v9

    .line 95
    .end local v2    # "confirmed":Z
    .end local v14    # "currentState":I
    :cond_c
    add-int/2addr v11, v7

    move v2, v8

    move v8, v13

    goto/16 :goto_1

    .line 180
    .end local v11    # "i":I
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->selectBestPatterns()[Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v2

    .line 181
    .local v2, "patternInfo":[Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-static {v2}, Lcom/google/zxing/ResultPoint;->orderBestPatterns([Lcom/google/zxing/ResultPoint;)V

    .line 183
    new-instance v3, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    invoke-direct {v3, v2}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;-><init>([Lcom/google/zxing/qrcode/detector/FinderPattern;)V

    return-object v3
.end method

.method protected final getImage()Lcom/google/zxing/common/BitMatrix;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->image:Lcom/google/zxing/common/BitMatrix;

    return-object v0
.end method

.method protected final getPossibleCenters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/zxing/qrcode/detector/FinderPattern;",
            ">;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    return-object v0
.end method

.method protected final handlePossibleCenter([III)Z
    .locals 10
    .param p1, "stateCount"    # [I
    .param p2, "i"    # I
    .param p3, "j"    # I

    .line 394
    const/4 v0, 0x0

    aget v1, p1, v0

    const/4 v2, 0x1

    aget v3, p1, v2

    add-int/2addr v1, v3

    const/4 v3, 0x2

    aget v4, p1, v3

    add-int/2addr v1, v4

    const/4 v4, 0x3

    aget v4, p1, v4

    add-int/2addr v1, v4

    const/4 v4, 0x4

    aget v4, p1, v4

    add-int/2addr v1, v4

    .line 396
    .local v1, "stateCountTotal":I
    invoke-static {p1, p3}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->centerFromEnd([II)F

    move-result v4

    .line 397
    .local v4, "centerJ":F
    float-to-int v5, v4

    aget v6, p1, v3

    invoke-direct {p0, p2, v5, v6, v1}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->crossCheckVertical(IIII)F

    move-result v5

    .line 398
    .local v5, "centerI":F
    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-nez v6, :cond_3

    .line 400
    float-to-int v6, v4

    float-to-int v7, v5

    aget v3, p1, v3

    invoke-direct {p0, v6, v7, v3, v1}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->crossCheckHorizontal(IIII)F

    move-result v4

    .line 401
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-nez v3, :cond_3

    .line 402
    int-to-float v0, v1

    const/high16 v3, 0x40e00000    # 7.0f

    div-float/2addr v0, v3

    .line 403
    .local v0, "estimatedModuleSize":F
    const/4 v3, 0x0

    .line 404
    .local v3, "found":Z
    const/4 v6, 0x0

    .local v6, "index":I
    :goto_0
    iget-object v7, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 405
    iget-object v7, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 407
    .local v7, "center":Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-virtual {v7, v0, v5, v4}, Lcom/google/zxing/qrcode/detector/FinderPattern;->aboutEquals(FFF)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 408
    iget-object v8, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-virtual {v7, v5, v4, v0}, Lcom/google/zxing/qrcode/detector/FinderPattern;->combineEstimate(FFF)Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v9

    invoke-interface {v8, v6, v9}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 409
    const/4 v3, 0x1

    .line 410
    goto :goto_1

    .line 404
    .end local v7    # "center":Lcom/google/zxing/qrcode/detector/FinderPattern;
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 413
    .end local v6    # "index":I
    :cond_1
    :goto_1
    if-nez v3, :cond_2

    .line 414
    new-instance v6, Lcom/google/zxing/qrcode/detector/FinderPattern;

    invoke-direct {v6, v4, v5, v0}, Lcom/google/zxing/qrcode/detector/FinderPattern;-><init>(FFF)V

    .line 415
    .local v6, "point":Lcom/google/zxing/qrcode/detector/FinderPattern;
    iget-object v7, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->possibleCenters:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    iget-object v7, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->resultPointCallback:Lcom/google/zxing/ResultPointCallback;

    if-eqz v7, :cond_2

    .line 417
    invoke-interface {v7, v6}, Lcom/google/zxing/ResultPointCallback;->foundPossibleResultPoint(Lcom/google/zxing/ResultPoint;)V

    .line 420
    .end local v6    # "point":Lcom/google/zxing/qrcode/detector/FinderPattern;
    :cond_2
    return v2

    .line 423
    .end local v0    # "estimatedModuleSize":F
    .end local v3    # "found":Z
    :cond_3
    return v0
.end method
