.class final Lcom/google/zxing/qrcode/encoder/MaskUtil;
.super Ljava/lang/Object;
.source "MaskUtil.java"


# static fields
.field private static final N1:I = 0x3

.field private static final N2:I = 0x3

.field private static final N3:I = 0x28

.field private static final N4:I = 0xa


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method static applyMaskPenaltyRule1(Lcom/google/zxing/qrcode/encoder/ByteMatrix;)I
    .locals 2
    .param p0, "matrix"    # Lcom/google/zxing/qrcode/encoder/ByteMatrix;

    .line 41
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/zxing/qrcode/encoder/MaskUtil;->applyMaskPenaltyRule1Internal(Lcom/google/zxing/qrcode/encoder/ByteMatrix;Z)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/zxing/qrcode/encoder/MaskUtil;->applyMaskPenaltyRule1Internal(Lcom/google/zxing/qrcode/encoder/ByteMatrix;Z)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private static applyMaskPenaltyRule1Internal(Lcom/google/zxing/qrcode/encoder/ByteMatrix;Z)I
    .locals 10
    .param p0, "matrix"    # Lcom/google/zxing/qrcode/encoder/ByteMatrix;
    .param p1, "isHorizontal"    # Z

    .line 192
    const/4 v0, 0x0

    .line 193
    .local v0, "penalty":I
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v1

    .line 194
    .local v1, "iLimit":I
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v2

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result v2

    .line 195
    .local v2, "jLimit":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getArray()[[B

    move-result-object v3

    .line 196
    .local v3, "array":[[B
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v1, :cond_7

    .line 197
    const/4 v5, 0x0

    .line 198
    .local v5, "numSameBitCells":I
    const/4 v6, -0x1

    .line 199
    .local v6, "prevBit":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    const/4 v8, 0x5

    if-ge v7, v2, :cond_5

    .line 200
    if-eqz p1, :cond_2

    aget-object v9, v3, v4

    aget-byte v9, v9, v7

    goto :goto_4

    :cond_2
    aget-object v9, v3, v7

    aget-byte v9, v9, v4

    .line 201
    .local v9, "bit":I
    :goto_4
    if-ne v9, v6, :cond_3

    .line 202
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 204
    :cond_3
    if-lt v5, v8, :cond_4

    .line 205
    add-int/lit8 v8, v5, -0x5

    add-int/lit8 v8, v8, 0x3

    add-int/2addr v0, v8

    .line 207
    :cond_4
    const/4 v5, 0x1

    .line 208
    move v6, v9

    .line 199
    .end local v9    # "bit":I
    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 211
    .end local v7    # "j":I
    :cond_5
    if-lt v5, v8, :cond_6

    .line 212
    add-int/lit8 v7, v5, -0x5

    add-int/lit8 v7, v7, 0x3

    add-int/2addr v0, v7

    .line 196
    .end local v5    # "numSameBitCells":I
    .end local v6    # "prevBit":I
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 215
    .end local v4    # "i":I
    :cond_7
    return v0
.end method

.method static applyMaskPenaltyRule2(Lcom/google/zxing/qrcode/encoder/ByteMatrix;)I
    .locals 9
    .param p0, "matrix"    # Lcom/google/zxing/qrcode/encoder/ByteMatrix;

    .line 50
    const/4 v0, 0x0

    .line 51
    .local v0, "penalty":I
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getArray()[[B

    move-result-object v1

    .line 52
    .local v1, "array":[[B
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v2

    .line 53
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result v3

    .line 54
    .local v3, "height":I
    const/4 v4, 0x0

    .local v4, "y":I
    :goto_0
    add-int/lit8 v5, v3, -0x1

    if-ge v4, v5, :cond_2

    .line 55
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_1
    add-int/lit8 v6, v2, -0x1

    if-ge v5, v6, :cond_1

    .line 56
    aget-object v6, v1, v4

    aget-byte v6, v6, v5

    .line 57
    .local v6, "value":I
    aget-object v7, v1, v4

    add-int/lit8 v8, v5, 0x1

    aget-byte v7, v7, v8

    if-ne v6, v7, :cond_0

    add-int/lit8 v7, v4, 0x1

    aget-object v7, v1, v7

    aget-byte v7, v7, v5

    if-ne v6, v7, :cond_0

    add-int/lit8 v7, v4, 0x1

    aget-object v7, v1, v7

    add-int/lit8 v8, v5, 0x1

    aget-byte v7, v7, v8

    if-ne v6, v7, :cond_0

    .line 58
    add-int/lit8 v0, v0, 0x1

    .line 55
    .end local v6    # "value":I
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 54
    .end local v5    # "x":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 62
    .end local v4    # "y":I
    :cond_2
    mul-int/lit8 v4, v0, 0x3

    return v4
.end method

.method static applyMaskPenaltyRule3(Lcom/google/zxing/qrcode/encoder/ByteMatrix;)I
    .locals 9
    .param p0, "matrix"    # Lcom/google/zxing/qrcode/encoder/ByteMatrix;

    .line 71
    const/4 v0, 0x0

    .line 72
    .local v0, "penalty":I
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getArray()[[B

    move-result-object v1

    .line 73
    .local v1, "array":[[B
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v2

    .line 74
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result v3

    .line 75
    .local v3, "height":I
    const/4 v4, 0x0

    .local v4, "y":I
    :goto_0
    if-ge v4, v3, :cond_7

    .line 76
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_1
    if-ge v5, v2, :cond_6

    .line 78
    add-int/lit8 v6, v5, 0x6

    const/4 v7, 0x1

    if-ge v6, v2, :cond_2

    aget-object v6, v1, v4

    aget-byte v6, v6, v5

    if-ne v6, v7, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x1

    aget-byte v6, v6, v8

    if-nez v6, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x2

    aget-byte v6, v6, v8

    if-ne v6, v7, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x3

    aget-byte v6, v6, v8

    if-ne v6, v7, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x4

    aget-byte v6, v6, v8

    if-ne v6, v7, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x5

    aget-byte v6, v6, v8

    if-nez v6, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x6

    aget-byte v6, v6, v8

    if-ne v6, v7, :cond_2

    add-int/lit8 v6, v5, 0xa

    if-ge v6, v2, :cond_0

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x7

    aget-byte v6, v6, v8

    if-nez v6, :cond_0

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x8

    aget-byte v6, v6, v8

    if-nez v6, :cond_0

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0x9

    aget-byte v6, v6, v8

    if-nez v6, :cond_0

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, 0xa

    aget-byte v6, v6, v8

    if-eqz v6, :cond_1

    :cond_0
    add-int/lit8 v6, v5, -0x4

    if-ltz v6, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, -0x1

    aget-byte v6, v6, v8

    if-nez v6, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, -0x2

    aget-byte v6, v6, v8

    if-nez v6, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, -0x3

    aget-byte v6, v6, v8

    if-nez v6, :cond_2

    aget-object v6, v1, v4

    add-int/lit8 v8, v5, -0x4

    aget-byte v6, v6, v8

    if-nez v6, :cond_2

    .line 96
    :cond_1
    add-int/lit8 v0, v0, 0x28

    .line 98
    :cond_2
    add-int/lit8 v6, v4, 0x6

    if-ge v6, v3, :cond_5

    aget-object v6, v1, v4

    aget-byte v6, v6, v5

    if-ne v6, v7, :cond_5

    add-int/lit8 v6, v4, 0x1

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_5

    add-int/lit8 v6, v4, 0x2

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-ne v6, v7, :cond_5

    add-int/lit8 v6, v4, 0x3

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-ne v6, v7, :cond_5

    add-int/lit8 v6, v4, 0x4

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-ne v6, v7, :cond_5

    add-int/lit8 v6, v4, 0x5

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_5

    add-int/lit8 v6, v4, 0x6

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-ne v6, v7, :cond_5

    add-int/lit8 v6, v4, 0xa

    if-ge v6, v3, :cond_3

    add-int/lit8 v6, v4, 0x7

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_3

    add-int/lit8 v6, v4, 0x8

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_3

    add-int/lit8 v6, v4, 0x9

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_3

    add-int/lit8 v6, v4, 0xa

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-eqz v6, :cond_4

    :cond_3
    add-int/lit8 v6, v4, -0x4

    if-ltz v6, :cond_5

    add-int/lit8 v6, v4, -0x1

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_5

    add-int/lit8 v6, v4, -0x2

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_5

    add-int/lit8 v6, v4, -0x3

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_5

    add-int/lit8 v6, v4, -0x4

    aget-object v6, v1, v6

    aget-byte v6, v6, v5

    if-nez v6, :cond_5

    .line 116
    :cond_4
    add-int/lit8 v0, v0, 0x28

    .line 76
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 75
    .end local v5    # "x":I
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 120
    .end local v4    # "y":I
    :cond_7
    return v0
.end method

.method static applyMaskPenaltyRule4(Lcom/google/zxing/qrcode/encoder/ByteMatrix;)I
    .locals 11
    .param p0, "matrix"    # Lcom/google/zxing/qrcode/encoder/ByteMatrix;

    .line 128
    const/4 v0, 0x0

    .line 129
    .local v0, "numDarkCells":I
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getArray()[[B

    move-result-object v1

    .line 130
    .local v1, "array":[[B
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v2

    .line 131
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result v3

    .line 132
    .local v3, "height":I
    const/4 v4, 0x0

    .local v4, "y":I
    :goto_0
    if-ge v4, v3, :cond_2

    .line 133
    aget-object v5, v1, v4

    .line 134
    .local v5, "arrayY":[B
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_1
    if-ge v6, v2, :cond_1

    .line 135
    aget-byte v7, v5, v6

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 136
    add-int/lit8 v0, v0, 0x1

    .line 134
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 132
    .end local v5    # "arrayY":[B
    .end local v6    # "x":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 140
    .end local v4    # "y":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v5

    mul-int/2addr v4, v5

    .line 141
    .local v4, "numTotalCells":I
    int-to-double v5, v0

    int-to-double v7, v4

    div-double/2addr v5, v7

    .line 142
    .local v5, "darkRatio":D
    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    sub-double v7, v5, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v7

    const-wide/high16 v9, 0x4034000000000000L    # 20.0

    mul-double/2addr v7, v9

    double-to-int v7, v7

    .line 143
    .local v7, "fivePercentVariances":I
    mul-int/lit8 v8, v7, 0xa

    return v8
.end method

.method static getDataMaskBit(III)Z
    .locals 6
    .param p0, "maskPattern"    # I
    .param p1, "x"    # I
    .param p2, "y"    # I

    .line 153
    const/4 v0, 0x0

    const/4 v1, 0x1

    packed-switch p0, :pswitch_data_0

    move v1, v0

    .line 182
    .local v0, "temp":I
    .local v1, "intermediate":I
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid mask pattern: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 153
    .end local v0    # "temp":I
    .end local v1    # "intermediate":I
    :pswitch_0
    move v2, v0

    .local v2, "intermediate":I
    move v3, v0

    .line 178
    .local v3, "temp":I
    mul-int v3, p2, p1

    .line 179
    rem-int/lit8 v4, v3, 0x3

    add-int v5, p2, p1

    and-int/2addr v5, v1

    add-int/2addr v4, v5

    and-int/lit8 v2, v4, 0x1

    .line 180
    goto :goto_0

    .line 153
    .end local v2    # "intermediate":I
    .end local v3    # "temp":I
    :pswitch_1
    move v2, v0

    .restart local v2    # "intermediate":I
    move v3, v0

    .line 174
    .restart local v3    # "temp":I
    mul-int v3, p2, p1

    .line 175
    and-int/lit8 v4, v3, 0x1

    rem-int/lit8 v5, v3, 0x3

    add-int/2addr v4, v5

    and-int/lit8 v2, v4, 0x1

    .line 176
    goto :goto_0

    .line 153
    .end local v2    # "intermediate":I
    .end local v3    # "temp":I
    :pswitch_2
    move v2, v0

    .line 170
    .restart local v2    # "intermediate":I
    mul-int v3, p2, p1

    .line 171
    .restart local v3    # "temp":I
    and-int/lit8 v4, v3, 0x1

    rem-int/lit8 v5, v3, 0x3

    add-int v2, v4, v5

    .line 172
    goto :goto_0

    .line 153
    .end local v2    # "intermediate":I
    .end local v3    # "temp":I
    :pswitch_3
    move v2, v0

    .line 167
    .restart local v2    # "intermediate":I
    ushr-int/lit8 v3, p2, 0x1

    div-int/lit8 v4, p1, 0x3

    add-int/2addr v3, v4

    and-int/lit8 v2, v3, 0x1

    .line 168
    move v3, v0

    .restart local v3    # "temp":I
    goto :goto_0

    .line 153
    .end local v2    # "intermediate":I
    .end local v3    # "temp":I
    :pswitch_4
    move v2, v0

    .line 164
    .restart local v2    # "intermediate":I
    add-int v3, p2, p1

    rem-int/lit8 v2, v3, 0x3

    .line 165
    move v3, v0

    .restart local v3    # "temp":I
    goto :goto_0

    .line 153
    .end local v2    # "intermediate":I
    .end local v3    # "temp":I
    :pswitch_5
    move v2, v0

    .line 161
    .restart local v2    # "intermediate":I
    rem-int/lit8 v2, p1, 0x3

    .line 162
    move v3, v0

    .restart local v3    # "temp":I
    goto :goto_0

    .line 153
    .end local v2    # "intermediate":I
    .end local v3    # "temp":I
    :pswitch_6
    move v2, v0

    .line 158
    .restart local v2    # "intermediate":I
    and-int/lit8 v2, p2, 0x1

    .line 159
    move v3, v0

    .restart local v3    # "temp":I
    goto :goto_0

    .line 155
    .end local v2    # "intermediate":I
    .end local v3    # "temp":I
    :pswitch_7
    add-int v2, p2, p1

    and-int/2addr v2, v1

    .line 156
    .restart local v2    # "intermediate":I
    move v3, v0

    .line 184
    .restart local v3    # "temp":I
    :goto_0
    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
