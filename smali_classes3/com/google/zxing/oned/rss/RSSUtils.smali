.class public final Lcom/google/zxing/oned/rss/RSSUtils;
.super Ljava/lang/Object;
.source "RSSUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static combins(II)I
    .locals 5
    .param p0, "n"    # I
    .param p1, "r"    # I

    .line 104
    sub-int v0, p0, p1

    if-le v0, p1, :cond_0

    .line 105
    move v0, p1

    .line 106
    .local v0, "minDenom":I
    sub-int v1, p0, p1

    .local v1, "maxDenom":I
    goto :goto_0

    .line 108
    .end local v0    # "minDenom":I
    .end local v1    # "maxDenom":I
    :cond_0
    sub-int v0, p0, p1

    .line 109
    .restart local v0    # "minDenom":I
    move v1, p1

    .line 111
    .restart local v1    # "maxDenom":I
    :goto_0
    const/4 v2, 0x1

    .line 112
    .local v2, "val":I
    const/4 v3, 0x1

    .line 113
    .local v3, "j":I
    move v4, p0

    .local v4, "i":I
    :goto_1
    if-le v4, v1, :cond_2

    .line 114
    mul-int/2addr v2, v4

    .line 115
    if-gt v3, v0, :cond_1

    .line 116
    div-int/2addr v2, v3

    .line 117
    add-int/lit8 v3, v3, 0x1

    .line 113
    :cond_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 120
    .end local v4    # "i":I
    :cond_2
    :goto_2
    if-gt v3, v0, :cond_3

    .line 121
    div-int/2addr v2, v3

    .line 122
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 124
    :cond_3
    return v2
.end method

.method static elements([III)[I
    .locals 9
    .param p0, "eDist"    # [I
    .param p1, "N"    # I
    .param p2, "K"    # I

    .line 128
    array-length v0, p0

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    .line 129
    .local v0, "widths":[I
    shl-int/lit8 v1, p2, 0x1

    .line 130
    .local v1, "twoK":I
    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v2, v0, v3

    .line 132
    const/16 v3, 0xa

    .line 133
    .local v3, "minEven":I
    const/4 v4, 0x1

    .line 134
    .local v4, "barSum":I
    const/4 v5, 0x1

    .local v5, "i":I
    :goto_0
    add-int/lit8 v6, v1, -0x2

    if-ge v5, v6, :cond_1

    .line 135
    add-int/lit8 v6, v5, -0x1

    aget v6, p0, v6

    add-int/lit8 v7, v5, -0x1

    aget v7, v0, v7

    sub-int/2addr v6, v7

    aput v6, v0, v5

    .line 136
    add-int/lit8 v6, v5, 0x1

    aget v7, p0, v5

    aget v8, v0, v5

    sub-int/2addr v7, v8

    aput v7, v0, v6

    .line 137
    aget v6, v0, v5

    add-int/lit8 v7, v5, 0x1

    aget v7, v0, v7

    add-int/2addr v6, v7

    add-int/2addr v4, v6

    .line 138
    aget v6, v0, v5

    if-ge v6, v3, :cond_0

    .line 139
    aget v3, v0, v5

    .line 134
    :cond_0
    add-int/lit8 v5, v5, 0x2

    goto :goto_0

    .line 142
    :cond_1
    add-int/lit8 v6, v1, -0x1

    sub-int v7, p1, v4

    aput v7, v0, v6

    .line 143
    add-int/lit8 v6, v1, -0x1

    aget v6, v0, v6

    if-ge v6, v3, :cond_2

    .line 144
    add-int/lit8 v6, v1, -0x1

    aget v3, v0, v6

    .line 146
    :cond_2
    if-le v3, v2, :cond_3

    .line 147
    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v1, :cond_3

    .line 148
    aget v2, v0, v5

    add-int/lit8 v6, v3, -0x1

    add-int/2addr v2, v6

    aput v2, v0, v5

    .line 149
    add-int/lit8 v2, v5, 0x1

    aget v6, v0, v2

    add-int/lit8 v7, v3, -0x1

    sub-int/2addr v6, v7

    aput v6, v0, v2

    .line 147
    add-int/lit8 v5, v5, 0x2

    goto :goto_1

    .line 152
    :cond_3
    return-object v0
.end method

.method public static getRSSvalue([IIZ)I
    .locals 12
    .param p0, "widths"    # [I
    .param p1, "maxWidth"    # I
    .param p2, "noNarrow"    # Z

    .line 65
    array-length v0, p0

    .line 66
    .local v0, "elements":I
    const/4 v1, 0x0

    .line 67
    .local v1, "n":I
    move-object v2, p0

    .local v2, "arr$":[I
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v3, :cond_0

    aget v5, v2, v4

    .line 68
    .local v5, "width":I
    add-int/2addr v1, v5

    .line 67
    .end local v5    # "width":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 70
    .end local v2    # "arr$":[I
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_0
    const/4 v2, 0x0

    .line 71
    .local v2, "val":I
    const/4 v3, 0x0

    .line 72
    .local v3, "narrowMask":I
    const/4 v4, 0x0

    .local v4, "bar":I
    :goto_1
    add-int/lit8 v5, v0, -0x1

    if-ge v4, v5, :cond_6

    .line 74
    const/4 v5, 0x1

    .local v5, "elmWidth":I
    const/4 v6, 0x1

    shl-int v7, v6, v4

    or-int/2addr v3, v7

    .line 75
    :goto_2
    aget v7, p0, v4

    if-ge v5, v7, :cond_5

    .line 77
    sub-int v7, v1, v5

    sub-int/2addr v7, v6

    sub-int v8, v0, v4

    add-int/lit8 v8, v8, -0x2

    invoke-static {v7, v8}, Lcom/google/zxing/oned/rss/RSSUtils;->combins(II)I

    move-result v7

    .line 78
    .local v7, "subVal":I
    if-eqz p2, :cond_1

    if-nez v3, :cond_1

    sub-int v8, v1, v5

    sub-int v9, v0, v4

    sub-int/2addr v9, v6

    sub-int/2addr v8, v9

    sub-int v9, v0, v4

    sub-int/2addr v9, v6

    if-lt v8, v9, :cond_1

    .line 80
    sub-int v8, v1, v5

    sub-int v9, v0, v4

    sub-int/2addr v8, v9

    sub-int v9, v0, v4

    add-int/lit8 v9, v9, -0x2

    invoke-static {v8, v9}, Lcom/google/zxing/oned/rss/RSSUtils;->combins(II)I

    move-result v8

    sub-int/2addr v7, v8

    .line 83
    :cond_1
    sub-int v8, v0, v4

    sub-int/2addr v8, v6

    if-le v8, v6, :cond_3

    .line 84
    const/4 v8, 0x0

    .line 85
    .local v8, "lessVal":I
    sub-int v9, v1, v5

    sub-int v10, v0, v4

    add-int/lit8 v10, v10, -0x2

    sub-int/2addr v9, v10

    .line 86
    .local v9, "mxwElement":I
    :goto_3
    if-le v9, p1, :cond_2

    .line 87
    sub-int v10, v1, v5

    sub-int/2addr v10, v9

    sub-int/2addr v10, v6

    sub-int v11, v0, v4

    add-int/lit8 v11, v11, -0x3

    invoke-static {v10, v11}, Lcom/google/zxing/oned/rss/RSSUtils;->combins(II)I

    move-result v10

    add-int/2addr v8, v10

    .line 86
    add-int/lit8 v9, v9, -0x1

    goto :goto_3

    .line 90
    .end local v9    # "mxwElement":I
    :cond_2
    add-int/lit8 v9, v0, -0x1

    sub-int/2addr v9, v4

    mul-int/2addr v9, v8

    sub-int/2addr v7, v9

    .end local v8    # "lessVal":I
    goto :goto_4

    .line 91
    :cond_3
    sub-int v8, v1, v5

    if-le v8, p1, :cond_4

    .line 92
    add-int/lit8 v7, v7, -0x1

    goto :goto_5

    .line 91
    :cond_4
    :goto_4
    nop

    .line 94
    :goto_5
    add-int/2addr v2, v7

    .line 76
    .end local v7    # "subVal":I
    add-int/lit8 v5, v5, 0x1

    shl-int v7, v6, v4

    not-int v7, v7

    and-int/2addr v3, v7

    goto :goto_2

    .line 96
    :cond_5
    sub-int/2addr v1, v5

    .line 72
    .end local v5    # "elmWidth":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 98
    .end local v4    # "bar":I
    :cond_6
    return v2
.end method

.method static getRSSwidths(IIIIZ)[I
    .locals 10
    .param p0, "val"    # I
    .param p1, "n"    # I
    .param p2, "elements"    # I
    .param p3, "maxWidth"    # I
    .param p4, "noNarrow"    # Z

    .line 25
    new-array v0, p2, [I

    .line 27
    .local v0, "widths":[I
    const/4 v1, 0x0

    .line 28
    .local v1, "narrowMask":I
    const/4 v2, 0x0

    .local v2, "bar":I
    :goto_0
    add-int/lit8 v3, p2, -0x1

    if-ge v2, v3, :cond_5

    .line 29
    const/4 v3, 0x1

    shl-int v4, v3, v2

    or-int/2addr v1, v4

    .line 30
    const/4 v4, 0x1

    .line 33
    .local v4, "elmWidth":I
    :goto_1
    sub-int v5, p1, v4

    sub-int/2addr v5, v3

    sub-int v6, p2, v2

    add-int/lit8 v6, v6, -0x2

    invoke-static {v5, v6}, Lcom/google/zxing/oned/rss/RSSUtils;->combins(II)I

    move-result v5

    .line 34
    .local v5, "subVal":I
    if-eqz p4, :cond_0

    if-nez v1, :cond_0

    sub-int v6, p1, v4

    sub-int v7, p2, v2

    sub-int/2addr v7, v3

    sub-int/2addr v6, v7

    sub-int v7, p2, v2

    sub-int/2addr v7, v3

    if-lt v6, v7, :cond_0

    .line 36
    sub-int v6, p1, v4

    sub-int v7, p2, v2

    sub-int/2addr v6, v7

    sub-int v7, p2, v2

    add-int/lit8 v7, v7, -0x2

    invoke-static {v6, v7}, Lcom/google/zxing/oned/rss/RSSUtils;->combins(II)I

    move-result v6

    sub-int/2addr v5, v6

    .line 38
    :cond_0
    sub-int v6, p2, v2

    sub-int/2addr v6, v3

    if-le v6, v3, :cond_2

    .line 39
    const/4 v6, 0x0

    .line 40
    .local v6, "lessVal":I
    sub-int v7, p1, v4

    sub-int v8, p2, v2

    add-int/lit8 v8, v8, -0x2

    sub-int/2addr v7, v8

    .line 41
    .local v7, "mxwElement":I
    :goto_2
    if-le v7, p3, :cond_1

    .line 43
    sub-int v8, p1, v4

    sub-int/2addr v8, v7

    sub-int/2addr v8, v3

    sub-int v9, p2, v2

    add-int/lit8 v9, v9, -0x3

    invoke-static {v8, v9}, Lcom/google/zxing/oned/rss/RSSUtils;->combins(II)I

    move-result v8

    add-int/2addr v6, v8

    .line 42
    add-int/lit8 v7, v7, -0x1

    goto :goto_2

    .line 45
    .end local v7    # "mxwElement":I
    :cond_1
    add-int/lit8 v7, p2, -0x1

    sub-int/2addr v7, v2

    mul-int/2addr v7, v6

    sub-int/2addr v5, v7

    .end local v6    # "lessVal":I
    goto :goto_3

    .line 46
    :cond_2
    sub-int v6, p1, v4

    if-le v6, p3, :cond_3

    .line 47
    add-int/lit8 v5, v5, -0x1

    goto :goto_4

    .line 46
    :cond_3
    :goto_3
    nop

    .line 49
    :goto_4
    sub-int/2addr p0, v5

    .line 50
    if-gez p0, :cond_4

    .line 51
    nop

    .line 56
    add-int/2addr p0, v5

    .line 57
    sub-int/2addr p1, v4

    .line 58
    aput v4, v0, v2

    .line 28
    .end local v4    # "elmWidth":I
    .end local v5    # "subVal":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    .restart local v4    # "elmWidth":I
    .restart local v5    # "subVal":I
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 54
    shl-int v6, v3, v2

    not-int v6, v6

    and-int/2addr v1, v6

    goto :goto_1

    .line 60
    .end local v4    # "elmWidth":I
    .end local v5    # "subVal":I
    :cond_5
    aput p1, v0, v2

    .line 61
    return-object v0
.end method
