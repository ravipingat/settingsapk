.class public final Lcom/google/zxing/oned/Code128Writer;
.super Lcom/google/zxing/oned/OneDimensionalCodeWriter;
.source "Code128Writer.java"


# static fields
.field private static final CODE_CODE_B:I = 0x64

.field private static final CODE_CODE_C:I = 0x63

.field private static final CODE_FNC_1:I = 0x66

.field private static final CODE_FNC_2:I = 0x61

.field private static final CODE_FNC_3:I = 0x60

.field private static final CODE_FNC_4_B:I = 0x64

.field private static final CODE_START_B:I = 0x68

.field private static final CODE_START_C:I = 0x69

.field private static final CODE_STOP:I = 0x6a

.field private static final ESCAPE_FNC_1:C = '\u00f1'

.field private static final ESCAPE_FNC_2:C = '\u00f2'

.field private static final ESCAPE_FNC_3:C = '\u00f3'

.field private static final ESCAPE_FNC_4:C = '\u00f4'


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/google/zxing/oned/OneDimensionalCodeWriter;-><init>()V

    return-void
.end method

.method private static isDigits(Ljava/lang/CharSequence;II)Z
    .locals 6
    .param p0, "value"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "length"    # I

    .line 189
    add-int v0, p1, p2

    .line 190
    .local v0, "end":I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 191
    .local v1, "last":I
    move v2, p1

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x0

    if-ge v2, v0, :cond_3

    if-ge v2, v1, :cond_3

    .line 192
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 193
    .local v4, "c":C
    const/16 v5, 0x30

    if-lt v4, v5, :cond_0

    const/16 v5, 0x39

    if-le v4, v5, :cond_2

    .line 194
    :cond_0
    const/16 v5, 0xf1

    if-eq v4, v5, :cond_1

    .line 195
    return v3

    .line 197
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 191
    .end local v4    # "c":C
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 200
    .end local v2    # "i":I
    :cond_3
    if-gt v0, v1, :cond_4

    const/4 v3, 0x1

    :cond_4
    return v3
.end method


# virtual methods
.method public encode(Ljava/lang/String;Lcom/google/zxing/BarcodeFormat;IILjava/util/Map;)Lcom/google/zxing/common/BitMatrix;
    .locals 3
    .param p1, "contents"    # Ljava/lang/String;
    .param p2, "format"    # Lcom/google/zxing/BarcodeFormat;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/zxing/BarcodeFormat;",
            "II",
            "Ljava/util/Map<",
            "Lcom/google/zxing/EncodeHintType;",
            "*>;)",
            "Lcom/google/zxing/common/BitMatrix;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/WriterException;
        }
    .end annotation

    .line 58
    .local p5, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/EncodeHintType;*>;"
    sget-object v0, Lcom/google/zxing/BarcodeFormat;->CODE_128:Lcom/google/zxing/BarcodeFormat;

    if-ne p2, v0, :cond_0

    .line 61
    invoke-super/range {p0 .. p5}, Lcom/google/zxing/oned/OneDimensionalCodeWriter;->encode(Ljava/lang/String;Lcom/google/zxing/BarcodeFormat;IILjava/util/Map;)Lcom/google/zxing/common/BitMatrix;

    move-result-object v0

    return-object v0

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can only encode CODE_128, but got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public encode(Ljava/lang/String;)[Z
    .locals 14
    .param p1, "contents"    # Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 68
    .local v0, "length":I
    const/4 v1, 0x1

    if-lt v0, v1, :cond_e

    const/16 v2, 0x50

    if-gt v0, v2, :cond_e

    .line 73
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v3, 0x20

    if-ge v2, v0, :cond_2

    .line 74
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 75
    .local v4, "c":C
    if-lt v4, v3, :cond_0

    const/16 v3, 0x7e

    if-le v4, v3, :cond_1

    .line 76
    :cond_0
    packed-switch v4, :pswitch_data_0

    .line 83
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad character in input: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 81
    :pswitch_0
    nop

    .line 73
    .end local v4    # "c":C
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 88
    .end local v2    # "i":I
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v2, "patterns":Ljava/util/Collection;, "Ljava/util/Collection<[I>;"
    const/4 v4, 0x0

    .line 90
    .local v4, "checkSum":I
    const/4 v5, 0x1

    .line 91
    .local v5, "checkWeight":I
    const/4 v6, 0x0

    .line 92
    .local v6, "codeSet":I
    const/4 v7, 0x0

    .local v7, "position":I
    const/4 v8, 0x0

    .line 94
    :goto_1
    if-ge v7, v0, :cond_a

    .line 96
    const/16 v9, 0x63

    if-ne v6, v9, :cond_3

    const/4 v9, 0x2

    goto :goto_2

    :cond_3
    const/4 v9, 0x4

    .line 98
    .local v9, "requiredDigitCount":I
    :goto_2
    invoke-static {p1, v7, v9}, Lcom/google/zxing/oned/Code128Writer;->isDigits(Ljava/lang/CharSequence;II)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 99
    const/16 v10, 0x63

    .local v10, "newCodeSet":I
    goto :goto_3

    .line 101
    .end local v10    # "newCodeSet":I
    :cond_4
    const/16 v10, 0x64

    .line 106
    .restart local v10    # "newCodeSet":I
    :goto_3
    const/16 v11, 0x64

    if-ne v10, v6, :cond_6

    .line 108
    if-ne v6, v11, :cond_5

    .line 109
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    sub-int/2addr v8, v3

    .line 110
    .local v8, "patternIndex":I
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 112
    .end local v8    # "patternIndex":I
    :cond_5
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v11

    packed-switch v11, :pswitch_data_1

    .line 130
    .restart local v8    # "patternIndex":I
    add-int/lit8 v11, v7, 0x2

    invoke-virtual {p1, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 131
    add-int/lit8 v7, v7, 0x2

    .line 132
    goto :goto_5

    .line 126
    :pswitch_1
    const/16 v8, 0x64

    .line 127
    add-int/lit8 v7, v7, 0x1

    .line 128
    goto :goto_5

    .line 122
    :pswitch_2
    const/16 v8, 0x60

    .line 123
    add-int/lit8 v7, v7, 0x1

    .line 124
    goto :goto_5

    .line 118
    :pswitch_3
    const/16 v8, 0x61

    .line 119
    add-int/lit8 v7, v7, 0x1

    .line 120
    goto :goto_5

    .line 114
    .end local v8    # "patternIndex":I
    :pswitch_4
    const/16 v8, 0x66

    .line 115
    .restart local v8    # "patternIndex":I
    add-int/lit8 v7, v7, 0x1

    .line 116
    goto :goto_5

    .line 138
    .end local v8    # "patternIndex":I
    :cond_6
    if-nez v6, :cond_8

    .line 140
    if-ne v10, v11, :cond_7

    .line 141
    const/16 v8, 0x68

    .restart local v8    # "patternIndex":I
    goto :goto_4

    .line 144
    .end local v8    # "patternIndex":I
    :cond_7
    const/16 v8, 0x69

    .restart local v8    # "patternIndex":I
    goto :goto_4

    .line 148
    .end local v8    # "patternIndex":I
    :cond_8
    move v8, v10

    .line 150
    .restart local v8    # "patternIndex":I
    :goto_4
    move v6, v10

    .line 154
    :goto_5
    sget-object v11, Lcom/google/zxing/oned/Code128Reader;->CODE_PATTERNS:[[I

    aget-object v11, v11, v8

    invoke-interface {v2, v11}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 157
    mul-int v11, v8, v5

    add-int/2addr v4, v11

    .line 158
    if-eqz v7, :cond_9

    .line 159
    add-int/lit8 v5, v5, 0x1

    .line 161
    .end local v8    # "patternIndex":I
    .end local v9    # "requiredDigitCount":I
    .end local v10    # "newCodeSet":I
    :cond_9
    goto :goto_1

    .line 164
    :cond_a
    rem-int/lit8 v4, v4, 0x67

    .line 165
    sget-object v3, Lcom/google/zxing/oned/Code128Reader;->CODE_PATTERNS:[[I

    aget-object v3, v3, v4

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v3, Lcom/google/zxing/oned/Code128Reader;->CODE_PATTERNS:[[I

    const/16 v8, 0x6a

    aget-object v3, v3, v8

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 171
    const/4 v3, 0x0

    .line 172
    .local v3, "codeWidth":I
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [I

    .line 173
    .local v9, "pattern":[I
    move-object v10, v9

    .local v10, "arr$":[I
    array-length v11, v10

    .local v11, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_7
    if-ge v12, v11, :cond_b

    aget v13, v10, v12

    .line 174
    .local v13, "width":I
    add-int/2addr v3, v13

    .line 173
    .end local v13    # "width":I
    add-int/lit8 v12, v12, 0x1

    goto :goto_7

    .line 176
    .end local v9    # "pattern":[I
    .end local v10    # "arr$":[I
    .end local v11    # "len$":I
    .end local v12    # "i$":I
    :cond_b
    goto :goto_6

    .line 179
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_c
    new-array v8, v3, [Z

    .line 180
    .local v8, "result":[Z
    const/4 v9, 0x0

    .line 181
    .local v9, "pos":I
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [I

    .line 182
    .local v11, "pattern":[I
    invoke-static {v8, v9, v11, v1}, Lcom/google/zxing/oned/Code128Writer;->appendPattern([ZI[IZ)I

    move-result v12

    add-int/2addr v9, v12

    .line 183
    .end local v11    # "pattern":[I
    goto :goto_8

    .line 185
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_d
    return-object v8

    .line 69
    .end local v2    # "patterns":Ljava/util/Collection;, "Ljava/util/Collection<[I>;"
    .end local v3    # "codeWidth":I
    .end local v4    # "checkSum":I
    .end local v5    # "checkWeight":I
    .end local v6    # "codeSet":I
    .end local v7    # "position":I
    .end local v8    # "result":[Z
    .end local v9    # "pos":I
    :cond_e
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Contents length should be between 1 and 80 characters, but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0xf1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xf1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
