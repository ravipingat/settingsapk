.class public abstract Lcom/google/zxing/oned/OneDReader;
.super Ljava/lang/Object;
.source "OneDReader.java"

# interfaces
.implements Lcom/google/zxing/Reader;


# static fields
.field protected static final INTEGER_MATH_SHIFT:I = 0x8

.field protected static final PATTERN_MATCH_RESULT_SCALE_FACTOR:I = 0x100


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private doDecode(Lcom/google/zxing/BinaryBitmap;Ljava/util/Map;)Lcom/google/zxing/Result;
    .locals 24
    .param p1, "image"    # Lcom/google/zxing/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/zxing/BinaryBitmap;",
            "Ljava/util/Map<",
            "Lcom/google/zxing/DecodeHintType;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 108
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    move-object/from16 v0, p2

    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/BinaryBitmap;->getWidth()I

    move-result v1

    .line 109
    .local v1, "width":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/BinaryBitmap;->getHeight()I

    move-result v2

    .line 110
    .local v2, "height":I
    new-instance v3, Lcom/google/zxing/common/BitArray;

    invoke-direct {v3, v1}, Lcom/google/zxing/common/BitArray;-><init>(I)V

    .line 112
    .local v3, "row":Lcom/google/zxing/common/BitArray;
    shr-int/lit8 v4, v2, 0x1

    .line 113
    .local v4, "middle":I
    const/4 v6, 0x1

    if-eqz v0, :cond_0

    sget-object v7, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v0, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v7, v6

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    .line 114
    .local v7, "tryHarder":Z
    :goto_0
    if-eqz v7, :cond_1

    const/16 v8, 0x8

    goto :goto_1

    :cond_1
    const/4 v8, 0x5

    :goto_1
    shr-int v8, v2, v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 116
    .local v8, "rowStep":I
    if-eqz v7, :cond_2

    .line 117
    move v9, v2

    .local v9, "maxLines":I
    goto :goto_2

    .line 119
    .end local v9    # "maxLines":I
    :cond_2
    const/16 v9, 0xf

    .line 122
    .restart local v9    # "maxLines":I
    :goto_2
    const/4 v10, 0x0

    move-object v11, v0

    .end local p2    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .local v10, "x":I
    .local v11, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :goto_3
    if-ge v10, v9, :cond_b

    .line 125
    add-int/lit8 v0, v10, 0x1

    shr-int/lit8 v12, v0, 0x1

    .line 126
    .local v12, "rowStepsAboveOrBelow":I
    and-int/lit8 v0, v10, 0x1

    if-nez v0, :cond_3

    move v0, v6

    goto :goto_4

    :cond_3
    const/4 v0, 0x0

    :goto_4
    move v13, v0

    .line 127
    .local v13, "isAbove":Z
    if-eqz v13, :cond_4

    move v0, v12

    goto :goto_5

    :cond_4
    neg-int v0, v12

    :goto_5
    mul-int/2addr v0, v8

    add-int v14, v4, v0

    .line 128
    .local v14, "rowNumber":I
    if-ltz v14, :cond_a

    if-ge v14, v2, :cond_9

    .line 135
    move-object/from16 v15, p1

    :try_start_0
    invoke-virtual {v15, v14, v3}, Lcom/google/zxing/BinaryBitmap;->getBlackRow(ILcom/google/zxing/common/BitArray;)Lcom/google/zxing/common/BitArray;

    move-result-object v0
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_7

    move-object v3, v0

    .line 138
    nop

    .line 142
    const/4 v0, 0x0

    move-object/from16 v23, v11

    move v11, v0

    move-object/from16 v0, v23

    .local v0, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .local v11, "attempt":I
    :goto_6
    const/4 v5, 0x2

    if-ge v11, v5, :cond_8

    .line 143
    if-ne v11, v6, :cond_5

    .line 144
    invoke-virtual {v3}, Lcom/google/zxing/common/BitArray;->reverse()V

    .line 149
    if-eqz v0, :cond_5

    sget-object v5, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 150
    new-instance v5, Ljava/util/EnumMap;

    const-class v6, Lcom/google/zxing/DecodeHintType;

    invoke-direct {v5, v6}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 151
    .local v5, "newHints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;Ljava/lang/Object;>;"
    invoke-interface {v5, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 152
    sget-object v6, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    move-object v0, v5

    goto :goto_7

    .line 158
    .end local v5    # "newHints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;Ljava/lang/Object;>;"
    :cond_5
    move-object v5, v0

    .end local v0    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .local v5, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :goto_7
    move-object/from16 v6, p0

    :try_start_1
    invoke-virtual {v6, v14, v3, v5}, Lcom/google/zxing/oned/OneDReader;->decodeRow(ILcom/google/zxing/common/BitArray;Ljava/util/Map;)Lcom/google/zxing/Result;

    move-result-object v0
    :try_end_1
    .catch Lcom/google/zxing/ReaderException; {:try_start_1 .. :try_end_1} :catch_6

    .line 160
    .local v0, "result":Lcom/google/zxing/Result;
    move/from16 v18, v2

    const/4 v2, 0x1

    .end local v2    # "height":I
    .local v18, "height":I
    if-ne v11, v2, :cond_7

    .line 162
    :try_start_2
    sget-object v2, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;
    :try_end_2
    .catch Lcom/google/zxing/ReaderException; {:try_start_2 .. :try_end_2} :catch_5

    const/16 v19, 0xb4

    move-object/from16 p2, v3

    .end local v3    # "row":Lcom/google/zxing/common/BitArray;
    .local p2, "row":Lcom/google/zxing/common/BitArray;
    :try_start_3
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/zxing/Result;->putMetadata(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 164
    invoke-virtual {v0}, Lcom/google/zxing/Result;->getResultPoints()[Lcom/google/zxing/ResultPoint;

    move-result-object v2

    .line 165
    .local v2, "points":[Lcom/google/zxing/ResultPoint;
    if-eqz v2, :cond_6

    .line 166
    new-instance v3, Lcom/google/zxing/ResultPoint;
    :try_end_3
    .catch Lcom/google/zxing/ReaderException; {:try_start_3 .. :try_end_3} :catch_4

    move/from16 v19, v4

    .end local v4    # "middle":I
    .local v19, "middle":I
    int-to-float v4, v1

    const/16 v16, 0x0

    :try_start_4
    aget-object v20, v2, v16

    invoke-virtual/range {v20 .. v20}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v20

    sub-float v4, v4, v20

    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v4, v4, v20

    aget-object v21, v2, v16
    :try_end_4
    .catch Lcom/google/zxing/ReaderException; {:try_start_4 .. :try_end_4} :catch_3

    move-object/from16 v22, v5

    .end local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .local v22, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v3, v2, v16

    .line 167
    new-instance v3, Lcom/google/zxing/ResultPoint;
    :try_end_5
    .catch Lcom/google/zxing/ReaderException; {:try_start_5 .. :try_end_5} :catch_2

    int-to-float v4, v1

    const/4 v5, 0x1

    :try_start_6
    aget-object v17, v2, v5

    invoke-virtual/range {v17 .. v17}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v17

    sub-float v4, v4, v17

    sub-float v4, v4, v20

    aget-object v17, v2, v5
    :try_end_6
    .catch Lcom/google/zxing/ReaderException; {:try_start_6 .. :try_end_6} :catch_1

    :try_start_7
    invoke-virtual/range {v17 .. v17}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/zxing/ResultPoint;-><init>(FF)V
    :try_end_7
    .catch Lcom/google/zxing/ReaderException; {:try_start_7 .. :try_end_7} :catch_2

    const/4 v4, 0x1

    :try_start_8
    aput-object v3, v2, v4
    :try_end_8
    .catch Lcom/google/zxing/ReaderException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_8

    .line 171
    .end local v0    # "result":Lcom/google/zxing/Result;
    .end local v2    # "points":[Lcom/google/zxing/ResultPoint;
    :catch_0
    move-exception v0

    goto :goto_9

    :catch_1
    move-exception v0

    move v4, v5

    goto :goto_9

    :catch_2
    move-exception v0

    const/4 v4, 0x1

    goto :goto_9

    .end local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :catch_3
    move-exception v0

    move-object/from16 v22, v5

    const/4 v4, 0x1

    .end local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    goto :goto_9

    .line 165
    .end local v19    # "middle":I
    .end local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v0    # "result":Lcom/google/zxing/Result;
    .restart local v2    # "points":[Lcom/google/zxing/ResultPoint;
    .restart local v4    # "middle":I
    .restart local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :cond_6
    move/from16 v19, v4

    move-object/from16 v22, v5

    .end local v4    # "middle":I
    .end local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v19    # "middle":I
    .restart local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    goto :goto_8

    .line 171
    .end local v0    # "result":Lcom/google/zxing/Result;
    .end local v2    # "points":[Lcom/google/zxing/ResultPoint;
    .end local v19    # "middle":I
    .end local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v4    # "middle":I
    .restart local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :catch_4
    move-exception v0

    move/from16 v19, v4

    move-object/from16 v22, v5

    const/4 v4, 0x1

    const/16 v16, 0x0

    .end local v4    # "middle":I
    .end local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v19    # "middle":I
    .restart local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    goto :goto_9

    .end local v19    # "middle":I
    .end local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .end local p2    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v3    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v4    # "middle":I
    .restart local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :catch_5
    move-exception v0

    move-object/from16 p2, v3

    move/from16 v19, v4

    move-object/from16 v22, v5

    const/4 v4, 0x1

    const/16 v16, 0x0

    .end local v3    # "row":Lcom/google/zxing/common/BitArray;
    .end local v4    # "middle":I
    .end local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v19    # "middle":I
    .restart local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local p2    # "row":Lcom/google/zxing/common/BitArray;
    goto :goto_9

    .line 160
    .end local v19    # "middle":I
    .end local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .end local p2    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v0    # "result":Lcom/google/zxing/Result;
    .restart local v3    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v4    # "middle":I
    .restart local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :cond_7
    move-object/from16 p2, v3

    move/from16 v19, v4

    move-object/from16 v22, v5

    .line 170
    .end local v3    # "row":Lcom/google/zxing/common/BitArray;
    .end local v4    # "middle":I
    .end local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v19    # "middle":I
    .restart local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local p2    # "row":Lcom/google/zxing/common/BitArray;
    :goto_8
    return-object v0

    .line 171
    .end local v0    # "result":Lcom/google/zxing/Result;
    .end local v18    # "height":I
    .end local v19    # "middle":I
    .end local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .end local p2    # "row":Lcom/google/zxing/common/BitArray;
    .local v2, "height":I
    .restart local v3    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v4    # "middle":I
    .restart local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :catch_6
    move-exception v0

    move/from16 v18, v2

    move-object/from16 p2, v3

    move/from16 v19, v4

    move-object/from16 v22, v5

    const/4 v4, 0x1

    const/16 v16, 0x0

    .line 142
    .end local v2    # "height":I
    .end local v3    # "row":Lcom/google/zxing/common/BitArray;
    .end local v4    # "middle":I
    .end local v5    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v18    # "height":I
    .restart local v19    # "middle":I
    .restart local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local p2    # "row":Lcom/google/zxing/common/BitArray;
    :goto_9
    add-int/lit8 v11, v11, 0x1

    move-object/from16 v3, p2

    move v6, v4

    move/from16 v2, v18

    move/from16 v4, v19

    move-object/from16 v0, v22

    goto/16 :goto_6

    .end local v18    # "height":I
    .end local v19    # "middle":I
    .end local v22    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .end local p2    # "row":Lcom/google/zxing/common/BitArray;
    .local v0, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .restart local v2    # "height":I
    .restart local v3    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v4    # "middle":I
    :cond_8
    move/from16 v18, v2

    move-object/from16 p2, v3

    move/from16 v19, v4

    move v4, v6

    const/16 v16, 0x0

    move-object/from16 v6, p0

    .end local v2    # "height":I
    .end local v3    # "row":Lcom/google/zxing/common/BitArray;
    .end local v4    # "middle":I
    .restart local v18    # "height":I
    .restart local v19    # "middle":I
    .restart local p2    # "row":Lcom/google/zxing/common/BitArray;
    move-object v11, v0

    goto :goto_a

    .line 136
    .end local v0    # "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    .end local v18    # "height":I
    .end local v19    # "middle":I
    .end local p2    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v2    # "height":I
    .restart local v3    # "row":Lcom/google/zxing/common/BitArray;
    .restart local v4    # "middle":I
    .local v11, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :catch_7
    move-exception v0

    move/from16 v18, v2

    move/from16 v19, v4

    move v4, v6

    const/16 v16, 0x0

    move-object/from16 v6, p0

    move-object v2, v0

    .end local v2    # "height":I
    .end local v4    # "middle":I
    .restart local v18    # "height":I
    .restart local v19    # "middle":I
    move-object v0, v2

    .line 137
    .local v0, "ignored":Lcom/google/zxing/NotFoundException;
    nop

    .line 122
    .end local v0    # "ignored":Lcom/google/zxing/NotFoundException;
    .end local v12    # "rowStepsAboveOrBelow":I
    .end local v13    # "isAbove":Z
    .end local v14    # "rowNumber":I
    :goto_a
    add-int/lit8 v10, v10, 0x1

    move v6, v4

    move/from16 v2, v18

    move/from16 v4, v19

    goto/16 :goto_3

    .line 130
    .end local v18    # "height":I
    .end local v19    # "middle":I
    .restart local v2    # "height":I
    .restart local v4    # "middle":I
    .restart local v12    # "rowStepsAboveOrBelow":I
    .restart local v13    # "isAbove":Z
    .restart local v14    # "rowNumber":I
    :cond_9
    move-object/from16 v6, p0

    move-object/from16 v15, p1

    move/from16 v18, v2

    move/from16 v19, v4

    .end local v2    # "height":I
    .end local v4    # "middle":I
    .restart local v18    # "height":I
    .restart local v19    # "middle":I
    goto :goto_b

    .line 128
    .end local v18    # "height":I
    .end local v19    # "middle":I
    .restart local v2    # "height":I
    .restart local v4    # "middle":I
    :cond_a
    move-object/from16 v6, p0

    move-object/from16 v15, p1

    move/from16 v18, v2

    move/from16 v19, v4

    .end local v2    # "height":I
    .end local v4    # "middle":I
    .restart local v18    # "height":I
    .restart local v19    # "middle":I
    goto :goto_b

    .line 122
    .end local v12    # "rowStepsAboveOrBelow":I
    .end local v13    # "isAbove":Z
    .end local v14    # "rowNumber":I
    .end local v18    # "height":I
    .end local v19    # "middle":I
    .restart local v2    # "height":I
    .restart local v4    # "middle":I
    :cond_b
    move-object/from16 v6, p0

    move-object/from16 v15, p1

    move/from16 v18, v2

    move/from16 v19, v4

    .line 177
    .end local v2    # "height":I
    .end local v4    # "middle":I
    .end local v10    # "x":I
    .restart local v18    # "height":I
    .restart local v19    # "middle":I
    :goto_b
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method

.method protected static patternMatchVariance([I[II)I
    .locals 10
    .param p0, "counters"    # [I
    .param p1, "pattern"    # [I
    .param p2, "maxIndividualVariance"    # I

    .line 259
    array-length v0, p0

    .line 260
    .local v0, "numCounters":I
    const/4 v1, 0x0

    .line 261
    .local v1, "total":I
    const/4 v2, 0x0

    .line 262
    .local v2, "patternLength":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 263
    aget v4, p0, v3

    add-int/2addr v1, v4

    .line 264
    aget v4, p1, v3

    add-int/2addr v2, v4

    .line 262
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 266
    .end local v3    # "i":I
    :cond_0
    const v3, 0x7fffffff

    if-ge v1, v2, :cond_1

    .line 269
    return v3

    .line 274
    :cond_1
    shl-int/lit8 v4, v1, 0x8

    div-int/2addr v4, v2

    .line 275
    .local v4, "unitBarWidth":I
    mul-int v5, p2, v4

    shr-int/lit8 p2, v5, 0x8

    .line 277
    const/4 v5, 0x0

    .line 278
    .local v5, "totalVariance":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_1
    if-ge v6, v0, :cond_4

    .line 279
    aget v7, p0, v6

    shl-int/lit8 v7, v7, 0x8

    .line 280
    .local v7, "counter":I
    aget v8, p1, v6

    mul-int/2addr v8, v4

    .line 281
    .local v8, "scaledPattern":I
    if-le v7, v8, :cond_2

    sub-int v9, v7, v8

    goto :goto_2

    :cond_2
    sub-int v9, v8, v7

    .line 282
    .local v9, "variance":I
    :goto_2
    if-le v9, p2, :cond_3

    .line 283
    return v3

    .line 285
    :cond_3
    add-int/2addr v5, v9

    .line 278
    .end local v7    # "counter":I
    .end local v8    # "scaledPattern":I
    .end local v9    # "variance":I
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 287
    .end local v6    # "x":I
    :cond_4
    div-int v3, v5, v1

    return v3
.end method

.method protected static recordPattern(Lcom/google/zxing/common/BitArray;I[I)V
    .locals 8
    .param p0, "row"    # Lcom/google/zxing/common/BitArray;
    .param p1, "start"    # I
    .param p2, "counters"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 196
    array-length v0, p2

    .line 197
    .local v0, "numCounters":I
    const/4 v1, 0x0

    invoke-static {p2, v1, v0, v1}, Ljava/util/Arrays;->fill([IIII)V

    .line 198
    invoke-virtual {p0}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v2

    .line 199
    .local v2, "end":I
    if-ge p1, v2, :cond_6

    .line 202
    invoke-virtual {p0, p1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    .line 203
    .local v3, "isWhite":Z
    const/4 v5, 0x0

    .line 204
    .local v5, "counterPosition":I
    move v6, p1

    .line 205
    .local v6, "i":I
    :goto_0
    if-ge v6, v2, :cond_3

    .line 206
    invoke-virtual {p0, v6}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v7

    xor-int/2addr v7, v3

    if-eqz v7, :cond_0

    .line 207
    aget v7, p2, v5

    add-int/2addr v7, v4

    aput v7, p2, v5

    goto :goto_2

    .line 209
    :cond_0
    add-int/lit8 v5, v5, 0x1

    .line 210
    if-ne v5, v0, :cond_1

    .line 211
    goto :goto_3

    .line 213
    :cond_1
    aput v4, p2, v5

    .line 214
    if-nez v3, :cond_2

    move v7, v4

    goto :goto_1

    :cond_2
    move v7, v1

    :goto_1
    move v3, v7

    .line 217
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 221
    :cond_3
    :goto_3
    if-eq v5, v0, :cond_5

    add-int/lit8 v1, v0, -0x1

    if-ne v5, v1, :cond_4

    if-ne v6, v2, :cond_4

    goto :goto_4

    .line 222
    :cond_4
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 224
    :cond_5
    :goto_4
    return-void

    .line 200
    .end local v3    # "isWhite":Z
    .end local v5    # "counterPosition":I
    .end local v6    # "i":I
    :cond_6
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1
.end method

.method protected static recordPatternInReverse(Lcom/google/zxing/common/BitArray;I[I)V
    .locals 3
    .param p0, "row"    # Lcom/google/zxing/common/BitArray;
    .param p1, "start"    # I
    .param p2, "counters"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 229
    array-length v0, p2

    .line 230
    .local v0, "numTransitionsLeft":I
    invoke-virtual {p0, p1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v1

    .line 231
    .local v1, "last":Z
    :cond_0
    :goto_0
    if-lez p1, :cond_2

    if-ltz v0, :cond_2

    .line 232
    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p0, p1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v2

    if-eq v2, v1, :cond_0

    .line 233
    add-int/lit8 v0, v0, -0x1

    .line 234
    if-nez v1, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    move v1, v2

    goto :goto_0

    .line 237
    :cond_2
    if-gez v0, :cond_3

    .line 240
    add-int/lit8 v2, p1, 0x1

    invoke-static {p0, v2, p2}, Lcom/google/zxing/oned/OneDReader;->recordPattern(Lcom/google/zxing/common/BitArray;I[I)V

    .line 241
    return-void

    .line 238
    :cond_3
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v2

    throw v2
.end method


# virtual methods
.method public decode(Lcom/google/zxing/BinaryBitmap;)Lcom/google/zxing/Result;
    .locals 1
    .param p1, "image"    # Lcom/google/zxing/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/zxing/oned/OneDReader;->decode(Lcom/google/zxing/BinaryBitmap;Ljava/util/Map;)Lcom/google/zxing/Result;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/google/zxing/BinaryBitmap;Ljava/util/Map;)Lcom/google/zxing/Result;
    .locals 12
    .param p1, "image"    # Lcom/google/zxing/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/zxing/BinaryBitmap;",
            "Ljava/util/Map<",
            "Lcom/google/zxing/DecodeHintType;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation

    .line 57
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/zxing/oned/OneDReader;->doDecode(Lcom/google/zxing/BinaryBitmap;Ljava/util/Map;)Lcom/google/zxing/Result;

    move-result-object v0
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "nfe":Lcom/google/zxing/NotFoundException;
    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 60
    .local v1, "tryHarder":Z
    :goto_0
    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/zxing/BinaryBitmap;->isRotateSupported()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 61
    invoke-virtual {p1}, Lcom/google/zxing/BinaryBitmap;->rotateCounterClockwise()Lcom/google/zxing/BinaryBitmap;

    move-result-object v2

    .line 62
    .local v2, "rotatedImage":Lcom/google/zxing/BinaryBitmap;
    invoke-direct {p0, v2, p2}, Lcom/google/zxing/oned/OneDReader;->doDecode(Lcom/google/zxing/BinaryBitmap;Ljava/util/Map;)Lcom/google/zxing/Result;

    move-result-object v3

    .line 64
    .local v3, "result":Lcom/google/zxing/Result;
    invoke-virtual {v3}, Lcom/google/zxing/Result;->getResultMetadata()Ljava/util/Map;

    move-result-object v4

    .line 65
    .local v4, "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/ResultMetadataType;*>;"
    const/16 v5, 0x10e

    .line 66
    .local v5, "orientation":I
    if-eqz v4, :cond_1

    sget-object v6, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;

    invoke-interface {v4, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 68
    sget-object v6, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/2addr v6, v5

    rem-int/lit16 v5, v6, 0x168

    .line 71
    :cond_1
    sget-object v6, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/google/zxing/Result;->putMetadata(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 73
    invoke-virtual {v3}, Lcom/google/zxing/Result;->getResultPoints()[Lcom/google/zxing/ResultPoint;

    move-result-object v6

    .line 74
    .local v6, "points":[Lcom/google/zxing/ResultPoint;
    if-eqz v6, :cond_2

    .line 75
    invoke-virtual {v2}, Lcom/google/zxing/BinaryBitmap;->getHeight()I

    move-result v7

    .line 76
    .local v7, "height":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    array-length v9, v6

    if-ge v8, v9, :cond_2

    .line 77
    new-instance v9, Lcom/google/zxing/ResultPoint;

    int-to-float v10, v7

    aget-object v11, v6, v8

    invoke-virtual {v11}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v11

    sub-float/2addr v10, v11

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    aget-object v11, v6, v8

    invoke-virtual {v11}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v11

    invoke-direct {v9, v10, v11}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v9, v6, v8

    .line 76
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 80
    .end local v7    # "height":I
    .end local v8    # "i":I
    :cond_2
    return-object v3

    .line 82
    .end local v2    # "rotatedImage":Lcom/google/zxing/BinaryBitmap;
    .end local v3    # "result":Lcom/google/zxing/Result;
    .end local v4    # "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/ResultMetadataType;*>;"
    .end local v5    # "orientation":I
    .end local v6    # "points":[Lcom/google/zxing/ResultPoint;
    :cond_3
    throw v0
.end method

.method public abstract decodeRow(ILcom/google/zxing/common/BitArray;Ljava/util/Map;)Lcom/google/zxing/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/zxing/common/BitArray;",
            "Ljava/util/Map<",
            "Lcom/google/zxing/DecodeHintType;",
            "*>;)",
            "Lcom/google/zxing/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;,
            Lcom/google/zxing/ChecksumException;,
            Lcom/google/zxing/FormatException;
        }
    .end annotation
.end method

.method public reset()V
    .locals 0

    .line 90
    return-void
.end method
