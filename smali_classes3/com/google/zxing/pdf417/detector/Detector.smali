.class public final Lcom/google/zxing/pdf417/detector/Detector;
.super Ljava/lang/Object;
.source "Detector.java"


# static fields
.field private static final BARCODE_MIN_HEIGHT:I = 0xa

.field private static final INDEXES_START_PATTERN:[I

.field private static final INDEXES_STOP_PATTERN:[I

.field private static final INTEGER_MATH_SHIFT:I = 0x8

.field private static final MAX_AVG_VARIANCE:I = 0x6b

.field private static final MAX_INDIVIDUAL_VARIANCE:I = 0xcc

.field private static final MAX_PATTERN_DRIFT:I = 0x5

.field private static final MAX_PIXEL_DRIFT:I = 0x3

.field private static final PATTERN_MATCH_RESULT_SCALE_FACTOR:I = 0x100

.field private static final ROW_STEP:I = 0x5

.field private static final SKIPPED_ROW_COUNT_MAX:I = 0x19

.field private static final START_PATTERN:[I

.field private static final STOP_PATTERN:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 41
    const/4 v0, 0x4

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/google/zxing/pdf417/detector/Detector;->INDEXES_START_PATTERN:[I

    .line 42
    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/zxing/pdf417/detector/Detector;->INDEXES_STOP_PATTERN:[I

    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/zxing/pdf417/detector/Detector;->START_PATTERN:[I

    .line 52
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/zxing/pdf417/detector/Detector;->STOP_PATTERN:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x4
        0x1
        0x5
    .end array-data

    :array_1
    .array-data 4
        0x6
        0x2
        0x7
        0x3
    .end array-data

    :array_2
    .array-data 4
        0x8
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x3
    .end array-data

    :array_3
    .array-data 4
        0x7
        0x1
        0x1
        0x3
        0x1
        0x1
        0x1
        0x2
        0x1
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method private static copyToResult([Lcom/google/zxing/ResultPoint;[Lcom/google/zxing/ResultPoint;[I)V
    .locals 3
    .param p0, "result"    # [Lcom/google/zxing/ResultPoint;
    .param p1, "tmpResult"    # [Lcom/google/zxing/ResultPoint;
    .param p2, "destinationIndexes"    # [I

    .line 210
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 211
    aget v1, p2, v0

    aget-object v2, p1, v0

    aput-object v2, p0, v1

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public static detect(Lcom/google/zxing/BinaryBitmap;Ljava/util/Map;Z)Lcom/google/zxing/pdf417/detector/PDF417DetectorResult;
    .locals 3
    .param p0, "image"    # Lcom/google/zxing/BinaryBitmap;
    .param p2, "multiple"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/zxing/BinaryBitmap;",
            "Ljava/util/Map<",
            "Lcom/google/zxing/DecodeHintType;",
            "*>;Z)",
            "Lcom/google/zxing/pdf417/detector/PDF417DetectorResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 81
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    invoke-virtual {p0}, Lcom/google/zxing/BinaryBitmap;->getBlackMatrix()Lcom/google/zxing/common/BitMatrix;

    move-result-object v0

    .line 83
    .local v0, "bitMatrix":Lcom/google/zxing/common/BitMatrix;
    invoke-static {p2, v0}, Lcom/google/zxing/pdf417/detector/Detector;->detect(ZLcom/google/zxing/common/BitMatrix;)Ljava/util/List;

    move-result-object v1

    .line 84
    .local v1, "barcodeCoordinates":Ljava/util/List;, "Ljava/util/List<[Lcom/google/zxing/ResultPoint;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    invoke-static {v0}, Lcom/google/zxing/pdf417/detector/Detector;->rotate180(Lcom/google/zxing/common/BitMatrix;)V

    .line 86
    invoke-static {p2, v0}, Lcom/google/zxing/pdf417/detector/Detector;->detect(ZLcom/google/zxing/common/BitMatrix;)Ljava/util/List;

    move-result-object v1

    .line 88
    :cond_0
    new-instance v2, Lcom/google/zxing/pdf417/detector/PDF417DetectorResult;

    invoke-direct {v2, v0, v1}, Lcom/google/zxing/pdf417/detector/PDF417DetectorResult;-><init>(Lcom/google/zxing/common/BitMatrix;Ljava/util/List;)V

    return-object v2
.end method

.method private static detect(ZLcom/google/zxing/common/BitMatrix;)Ljava/util/List;
    .locals 10
    .param p0, "multiple"    # Z
    .param p1, "bitMatrix"    # Lcom/google/zxing/common/BitMatrix;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/zxing/common/BitMatrix;",
            ")",
            "Ljava/util/List<",
            "[",
            "Lcom/google/zxing/ResultPoint;",
            ">;"
        }
    .end annotation

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v0, "barcodeCoordinates":Ljava/util/List;, "Ljava/util/List<[Lcom/google/zxing/ResultPoint;>;"
    const/4 v1, 0x0

    .line 101
    .local v1, "row":I
    const/4 v2, 0x0

    .line 102
    .local v2, "column":I
    const/4 v3, 0x0

    .line 103
    .local v3, "foundBarcodeInRow":Z
    :goto_0
    invoke-virtual {p1}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 104
    invoke-static {p1, v1, v2}, Lcom/google/zxing/pdf417/detector/Detector;->findVertices(Lcom/google/zxing/common/BitMatrix;II)[Lcom/google/zxing/ResultPoint;

    move-result-object v4

    .line 106
    .local v4, "vertices":[Lcom/google/zxing/ResultPoint;
    const/4 v5, 0x0

    aget-object v5, v4, v5

    if-nez v5, :cond_4

    const/4 v5, 0x3

    aget-object v6, v4, v5

    if-nez v6, :cond_4

    .line 107
    if-nez v3, :cond_0

    .line 109
    goto :goto_3

    .line 113
    :cond_0
    const/4 v3, 0x0

    .line 114
    const/4 v2, 0x0

    .line 115
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/zxing/ResultPoint;

    .line 116
    .local v7, "barcodeCoordinate":[Lcom/google/zxing/ResultPoint;
    const/4 v8, 0x1

    aget-object v9, v7, v8

    if-eqz v9, :cond_1

    .line 117
    int-to-float v9, v1

    aget-object v8, v7, v8

    invoke-virtual {v8}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v8

    invoke-static {v9, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    float-to-int v1, v8

    .line 119
    :cond_1
    aget-object v8, v7, v5

    if-eqz v8, :cond_2

    .line 120
    aget-object v8, v7, v5

    invoke-virtual {v8}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v8

    float-to-int v8, v8

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 122
    .end local v7    # "barcodeCoordinate":[Lcom/google/zxing/ResultPoint;
    :cond_2
    goto :goto_1

    .line 123
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_3
    add-int/lit8 v1, v1, 0x5

    .line 124
    goto :goto_0

    .line 126
    :cond_4
    const/4 v3, 0x1

    .line 127
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    if-nez p0, :cond_5

    .line 129
    goto :goto_3

    .line 133
    :cond_5
    const/4 v5, 0x2

    aget-object v6, v4, v5

    if-eqz v6, :cond_6

    .line 134
    aget-object v6, v4, v5

    invoke-virtual {v6}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 135
    aget-object v5, v4, v5

    invoke-virtual {v5}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v5

    float-to-int v1, v5

    goto :goto_2

    .line 137
    :cond_6
    const/4 v5, 0x4

    aget-object v6, v4, v5

    invoke-virtual {v6}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 138
    aget-object v5, v4, v5

    invoke-virtual {v5}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v5

    float-to-int v1, v5

    .line 140
    .end local v4    # "vertices":[Lcom/google/zxing/ResultPoint;
    :goto_2
    goto :goto_0

    .line 141
    :cond_7
    :goto_3
    return-object v0
.end method

.method private static findGuardPattern(Lcom/google/zxing/common/BitMatrix;IIIZ[I[I)[I
    .locals 18
    .param p0, "matrix"    # Lcom/google/zxing/common/BitMatrix;
    .param p1, "column"    # I
    .param p2, "row"    # I
    .param p3, "width"    # I
    .param p4, "whiteFirst"    # Z
    .param p5, "pattern"    # [I
    .param p6, "counters"    # [I

    .line 295
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    array-length v4, v3

    const/4 v5, 0x0

    invoke-static {v3, v5, v4, v5}, Ljava/util/Arrays;->fill([IIII)V

    .line 296
    array-length v4, v2

    .line 297
    .local v4, "patternLength":I
    move/from16 v6, p4

    .line 298
    .local v6, "isWhite":Z
    move/from16 v7, p1

    .line 299
    .local v7, "patternStart":I
    const/4 v8, 0x0

    .line 302
    .local v8, "pixelDrift":I
    :goto_0
    invoke-virtual {v0, v7, v1}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v9

    if-eqz v9, :cond_0

    if-lez v7, :cond_0

    add-int/lit8 v9, v8, 0x1

    .end local v8    # "pixelDrift":I
    .local v9, "pixelDrift":I
    const/4 v10, 0x3

    if-ge v8, v10, :cond_1

    .line 303
    add-int/lit8 v7, v7, -0x1

    move v8, v9

    goto :goto_0

    .line 305
    .end local v9    # "pixelDrift":I
    .restart local v8    # "pixelDrift":I
    :cond_0
    move v9, v8

    .end local v8    # "pixelDrift":I
    .restart local v9    # "pixelDrift":I
    :cond_1
    move v8, v7

    .line 306
    .local v8, "x":I
    const/4 v10, 0x0

    .line 307
    .local v10, "counterPosition":I
    :goto_1
    const/16 v11, 0x6b

    const/16 v12, 0xcc

    const/4 v13, 0x2

    const/4 v14, 0x1

    move/from16 v15, p3

    if-ge v8, v15, :cond_6

    .line 308
    invoke-virtual {v0, v8, v1}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v16

    .line 309
    .local v16, "pixel":Z
    xor-int v17, v16, v6

    if-eqz v17, :cond_2

    .line 310
    aget v11, v3, v10

    add-int/2addr v11, v14

    aput v11, v3, v10

    goto :goto_4

    .line 312
    :cond_2
    add-int/lit8 v14, v4, -0x1

    if-ne v10, v14, :cond_4

    .line 313
    invoke-static {v3, v2, v12}, Lcom/google/zxing/pdf417/detector/Detector;->patternMatchVariance([I[II)I

    move-result v12

    if-ge v12, v11, :cond_3

    .line 314
    new-array v11, v13, [I

    aput v7, v11, v5

    const/4 v12, 0x1

    aput v8, v11, v12

    return-object v11

    .line 316
    :cond_3
    const/4 v12, 0x1

    aget v11, v3, v5

    aget v14, v3, v12

    add-int/2addr v11, v14

    add-int/2addr v7, v11

    .line 317
    add-int/lit8 v11, v4, -0x2

    invoke-static {v3, v13, v3, v5, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 318
    add-int/lit8 v11, v4, -0x2

    aput v5, v3, v11

    .line 319
    add-int/lit8 v11, v4, -0x1

    aput v5, v3, v11

    .line 320
    add-int/lit8 v10, v10, -0x1

    goto :goto_2

    .line 322
    :cond_4
    add-int/lit8 v10, v10, 0x1

    .line 324
    :goto_2
    const/4 v11, 0x1

    aput v11, v3, v10

    .line 325
    if-nez v6, :cond_5

    const/16 v17, 0x1

    goto :goto_3

    :cond_5
    move/from16 v17, v5

    :goto_3
    move/from16 v6, v17

    .line 307
    .end local v16    # "pixel":Z
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 328
    :cond_6
    add-int/lit8 v14, v4, -0x1

    if-ne v10, v14, :cond_7

    .line 329
    invoke-static {v3, v2, v12}, Lcom/google/zxing/pdf417/detector/Detector;->patternMatchVariance([I[II)I

    move-result v12

    if-ge v12, v11, :cond_7

    .line 330
    new-array v11, v13, [I

    aput v7, v11, v5

    add-int/lit8 v5, v8, -0x1

    const/4 v12, 0x1

    aput v5, v11, v12

    return-object v11

    .line 333
    :cond_7
    const/4 v5, 0x0

    return-object v5
.end method

.method private static findRowsWithPattern(Lcom/google/zxing/common/BitMatrix;IIII[I)[Lcom/google/zxing/ResultPoint;
    .locals 18
    .param p0, "matrix"    # Lcom/google/zxing/common/BitMatrix;
    .param p1, "height"    # I
    .param p2, "width"    # I
    .param p3, "startRow"    # I
    .param p4, "startColumn"    # I
    .param p5, "pattern"    # [I

    .line 221
    move/from16 v0, p1

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/google/zxing/ResultPoint;

    .line 222
    .local v1, "result":[Lcom/google/zxing/ResultPoint;
    const/4 v2, 0x0

    .line 223
    .local v2, "found":Z
    move-object/from16 v10, p5

    array-length v3, v10

    new-array v11, v3, [I

    move/from16 v12, p3

    .line 224
    .end local p3    # "startRow":I
    .local v11, "counters":[I
    .local v12, "startRow":I
    :goto_0
    const/4 v13, 0x0

    const/4 v14, 0x1

    if-ge v12, v0, :cond_3

    .line 225
    const/4 v7, 0x0

    move-object/from16 v3, p0

    move/from16 v4, p4

    move v5, v12

    move/from16 v6, p2

    move-object/from16 v8, p5

    move-object v9, v11

    invoke-static/range {v3 .. v9}, Lcom/google/zxing/pdf417/detector/Detector;->findGuardPattern(Lcom/google/zxing/common/BitMatrix;IIIZ[I[I)[I

    move-result-object v3

    .line 226
    .local v3, "loc":[I
    if-eqz v3, :cond_2

    move-object v15, v3

    .line 227
    .end local v3    # "loc":[I
    .local v15, "loc":[I
    :goto_1
    if-lez v12, :cond_1

    .line 228
    add-int/lit8 v12, v12, -0x1

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move/from16 v4, p4

    move v5, v12

    move/from16 v6, p2

    move-object/from16 v8, p5

    move-object v9, v11

    invoke-static/range {v3 .. v9}, Lcom/google/zxing/pdf417/detector/Detector;->findGuardPattern(Lcom/google/zxing/common/BitMatrix;IIIZ[I[I)[I

    move-result-object v3

    .line 229
    .local v3, "previousRowLoc":[I
    if-eqz v3, :cond_0

    .line 230
    move-object v15, v3

    .line 235
    .end local v3    # "previousRowLoc":[I
    goto :goto_1

    .line 232
    .restart local v3    # "previousRowLoc":[I
    :cond_0
    add-int/2addr v12, v14

    .line 236
    .end local v3    # "previousRowLoc":[I
    :cond_1
    new-instance v3, Lcom/google/zxing/ResultPoint;

    aget v4, v15, v13

    int-to-float v4, v4

    int-to-float v5, v12

    invoke-direct {v3, v4, v5}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v3, v1, v13

    .line 237
    new-instance v3, Lcom/google/zxing/ResultPoint;

    aget v4, v15, v14

    int-to-float v4, v4

    int-to-float v5, v12

    invoke-direct {v3, v4, v5}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v3, v1, v14

    .line 238
    const/4 v2, 0x1

    .line 239
    goto :goto_2

    .line 224
    .end local v15    # "loc":[I
    :cond_2
    add-int/lit8 v12, v12, 0x5

    goto :goto_0

    .line 242
    :cond_3
    :goto_2
    add-int/lit8 v3, v12, 0x1

    .line 244
    .local v3, "stopRow":I
    if-eqz v2, :cond_7

    .line 245
    const/4 v4, 0x0

    .line 246
    .local v4, "skippedRowCount":I
    const/4 v15, 0x2

    new-array v5, v15, [I

    aget-object v6, v1, v13

    invoke-virtual {v6}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v6

    float-to-int v6, v6

    aput v6, v5, v13

    aget-object v6, v1, v14

    invoke-virtual {v6}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v6

    float-to-int v6, v6

    aput v6, v5, v14

    move v9, v3

    move v8, v4

    move-object/from16 v16, v5

    .line 247
    .end local v3    # "stopRow":I
    .end local v4    # "skippedRowCount":I
    .local v8, "skippedRowCount":I
    .local v9, "stopRow":I
    .local v16, "previousRowLoc":[I
    :goto_3
    if-ge v9, v0, :cond_6

    .line 248
    aget v4, v16, v13

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move v5, v9

    move/from16 v6, p2

    move v15, v8

    .end local v8    # "skippedRowCount":I
    .local v15, "skippedRowCount":I
    move-object/from16 v8, p5

    move/from16 v17, v9

    .end local v9    # "stopRow":I
    .local v17, "stopRow":I
    move-object v9, v11

    invoke-static/range {v3 .. v9}, Lcom/google/zxing/pdf417/detector/Detector;->findGuardPattern(Lcom/google/zxing/common/BitMatrix;IIIZ[I[I)[I

    move-result-object v3

    .line 253
    .local v3, "loc":[I
    if-eqz v3, :cond_4

    aget v4, v16, v13

    aget v5, v3, v13

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/4 v5, 0x5

    if-ge v4, v5, :cond_4

    aget v4, v16, v14

    aget v6, v3, v14

    sub-int/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v4, v5, :cond_4

    .line 256
    move-object v4, v3

    .line 257
    .end local v16    # "previousRowLoc":[I
    .local v4, "previousRowLoc":[I
    const/4 v5, 0x0

    move-object/from16 v16, v4

    move v8, v5

    .end local v15    # "skippedRowCount":I
    .local v5, "skippedRowCount":I
    goto :goto_4

    .line 259
    .end local v4    # "previousRowLoc":[I
    .end local v5    # "skippedRowCount":I
    .restart local v15    # "skippedRowCount":I
    .restart local v16    # "previousRowLoc":[I
    :cond_4
    const/16 v4, 0x19

    if-le v15, v4, :cond_5

    .line 260
    goto :goto_5

    .line 262
    :cond_5
    add-int/lit8 v8, v15, 0x1

    .line 247
    .end local v3    # "loc":[I
    .end local v15    # "skippedRowCount":I
    .restart local v8    # "skippedRowCount":I
    :goto_4
    add-int/lit8 v9, v17, 0x1

    const/4 v15, 0x2

    .end local v17    # "stopRow":I
    .restart local v9    # "stopRow":I
    goto :goto_3

    :cond_6
    move v15, v8

    move/from16 v17, v9

    .line 266
    .end local v8    # "skippedRowCount":I
    .end local v9    # "stopRow":I
    .restart local v15    # "skippedRowCount":I
    .restart local v17    # "stopRow":I
    :goto_5
    add-int/lit8 v8, v15, 0x1

    sub-int v3, v17, v8

    .line 267
    .end local v17    # "stopRow":I
    .local v3, "stopRow":I
    new-instance v4, Lcom/google/zxing/ResultPoint;

    aget v5, v16, v13

    int-to-float v5, v5

    int-to-float v6, v3

    invoke-direct {v4, v5, v6}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    const/4 v5, 0x2

    aput-object v4, v1, v5

    .line 268
    const/4 v4, 0x3

    new-instance v5, Lcom/google/zxing/ResultPoint;

    aget v6, v16, v14

    int-to-float v6, v6

    int-to-float v7, v3

    invoke-direct {v5, v6, v7}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v5, v1, v4

    .line 270
    .end local v15    # "skippedRowCount":I
    .end local v16    # "previousRowLoc":[I
    :cond_7
    sub-int v4, v3, v12

    const/16 v5, 0xa

    if-ge v4, v5, :cond_8

    .line 271
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_6
    array-length v5, v1

    if-ge v4, v5, :cond_8

    .line 272
    const/4 v5, 0x0

    aput-object v5, v1, v4

    .line 271
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 275
    .end local v4    # "i":I
    :cond_8
    return-object v1
.end method

.method private static findVertices(Lcom/google/zxing/common/BitMatrix;II)[Lcom/google/zxing/ResultPoint;
    .locals 9
    .param p0, "matrix"    # Lcom/google/zxing/common/BitMatrix;
    .param p1, "startRow"    # I
    .param p2, "startColumn"    # I

    .line 193
    invoke-virtual {p0}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v6

    .line 194
    .local v6, "height":I
    invoke-virtual {p0}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v7

    .line 196
    .local v7, "width":I
    const/16 v0, 0x8

    new-array v8, v0, [Lcom/google/zxing/ResultPoint;

    .line 197
    .local v8, "result":[Lcom/google/zxing/ResultPoint;
    sget-object v5, Lcom/google/zxing/pdf417/detector/Detector;->START_PATTERN:[I

    move-object v0, p0

    move v1, v6

    move v2, v7

    move v3, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/zxing/pdf417/detector/Detector;->findRowsWithPattern(Lcom/google/zxing/common/BitMatrix;IIII[I)[Lcom/google/zxing/ResultPoint;

    move-result-object v0

    sget-object v1, Lcom/google/zxing/pdf417/detector/Detector;->INDEXES_START_PATTERN:[I

    invoke-static {v8, v0, v1}, Lcom/google/zxing/pdf417/detector/Detector;->copyToResult([Lcom/google/zxing/ResultPoint;[Lcom/google/zxing/ResultPoint;[I)V

    .line 200
    const/4 v0, 0x4

    aget-object v1, v8, v0

    if-eqz v1, :cond_0

    .line 201
    aget-object v1, v8, v0

    invoke-virtual {v1}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v1

    float-to-int p2, v1

    .line 202
    aget-object v0, v8, v0

    invoke-virtual {v0}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v0

    float-to-int p1, v0

    .line 204
    :cond_0
    sget-object v5, Lcom/google/zxing/pdf417/detector/Detector;->STOP_PATTERN:[I

    move-object v0, p0

    move v1, v6

    move v2, v7

    move v3, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/zxing/pdf417/detector/Detector;->findRowsWithPattern(Lcom/google/zxing/common/BitMatrix;IIII[I)[Lcom/google/zxing/ResultPoint;

    move-result-object v0

    sget-object v1, Lcom/google/zxing/pdf417/detector/Detector;->INDEXES_STOP_PATTERN:[I

    invoke-static {v8, v0, v1}, Lcom/google/zxing/pdf417/detector/Detector;->copyToResult([Lcom/google/zxing/ResultPoint;[Lcom/google/zxing/ResultPoint;[I)V

    .line 206
    return-object v8
.end method

.method static mirror(Lcom/google/zxing/common/BitArray;Lcom/google/zxing/common/BitArray;)Lcom/google/zxing/common/BitArray;
    .locals 3
    .param p0, "input"    # Lcom/google/zxing/common/BitArray;
    .param p1, "result"    # Lcom/google/zxing/common/BitArray;

    .line 167
    invoke-virtual {p1}, Lcom/google/zxing/common/BitArray;->clear()V

    .line 168
    invoke-virtual {p0}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v0

    .line 169
    .local v0, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 170
    invoke-virtual {p0, v1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    add-int/lit8 v2, v0, -0x1

    sub-int/2addr v2, v1

    invoke-virtual {p1, v2}, Lcom/google/zxing/common/BitArray;->set(I)V

    .line 169
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 174
    .end local v1    # "i":I
    :cond_1
    return-object p1
.end method

.method private static patternMatchVariance([I[II)I
    .locals 10
    .param p0, "counters"    # [I
    .param p1, "pattern"    # [I
    .param p2, "maxIndividualVariance"    # I

    .line 352
    array-length v0, p0

    .line 353
    .local v0, "numCounters":I
    const/4 v1, 0x0

    .line 354
    .local v1, "total":I
    const/4 v2, 0x0

    .line 355
    .local v2, "patternLength":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 356
    aget v4, p0, v3

    add-int/2addr v1, v4

    .line 357
    aget v4, p1, v3

    add-int/2addr v2, v4

    .line 355
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 359
    .end local v3    # "i":I
    :cond_0
    const v3, 0x7fffffff

    if-ge v1, v2, :cond_1

    .line 362
    return v3

    .line 367
    :cond_1
    shl-int/lit8 v4, v1, 0x8

    div-int/2addr v4, v2

    .line 368
    .local v4, "unitBarWidth":I
    mul-int v5, p2, v4

    shr-int/lit8 p2, v5, 0x8

    .line 370
    const/4 v5, 0x0

    .line 371
    .local v5, "totalVariance":I
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_1
    if-ge v6, v0, :cond_4

    .line 372
    aget v7, p0, v6

    shl-int/lit8 v7, v7, 0x8

    .line 373
    .local v7, "counter":I
    aget v8, p1, v6

    mul-int/2addr v8, v4

    .line 374
    .local v8, "scaledPattern":I
    if-le v7, v8, :cond_2

    sub-int v9, v7, v8

    goto :goto_2

    :cond_2
    sub-int v9, v8, v7

    .line 375
    .local v9, "variance":I
    :goto_2
    if-le v9, p2, :cond_3

    .line 376
    return v3

    .line 378
    :cond_3
    add-int/2addr v5, v9

    .line 371
    .end local v7    # "counter":I
    .end local v8    # "scaledPattern":I
    .end local v9    # "variance":I
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 380
    .end local v6    # "x":I
    :cond_4
    div-int v3, v5, v1

    return v3
.end method

.method static rotate180(Lcom/google/zxing/common/BitMatrix;)V
    .locals 8
    .param p0, "bitMatrix"    # Lcom/google/zxing/common/BitMatrix;

    .line 151
    invoke-virtual {p0}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v0

    .line 152
    .local v0, "width":I
    invoke-virtual {p0}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v1

    .line 153
    .local v1, "height":I
    new-instance v2, Lcom/google/zxing/common/BitArray;

    invoke-direct {v2, v0}, Lcom/google/zxing/common/BitArray;-><init>(I)V

    .line 154
    .local v2, "firstRowBitArray":Lcom/google/zxing/common/BitArray;
    new-instance v3, Lcom/google/zxing/common/BitArray;

    invoke-direct {v3, v0}, Lcom/google/zxing/common/BitArray;-><init>(I)V

    .line 155
    .local v3, "secondRowBitArray":Lcom/google/zxing/common/BitArray;
    new-instance v4, Lcom/google/zxing/common/BitArray;

    invoke-direct {v4, v0}, Lcom/google/zxing/common/BitArray;-><init>(I)V

    .line 156
    .local v4, "tmpBitArray":Lcom/google/zxing/common/BitArray;
    const/4 v5, 0x0

    .local v5, "y":I
    :goto_0
    add-int/lit8 v6, v1, 0x1

    shr-int/lit8 v6, v6, 0x1

    if-ge v5, v6, :cond_0

    .line 157
    invoke-virtual {p0, v5, v2}, Lcom/google/zxing/common/BitMatrix;->getRow(ILcom/google/zxing/common/BitArray;)Lcom/google/zxing/common/BitArray;

    move-result-object v2

    .line 158
    add-int/lit8 v6, v1, -0x1

    sub-int/2addr v6, v5

    invoke-virtual {p0, v6, v3}, Lcom/google/zxing/common/BitMatrix;->getRow(ILcom/google/zxing/common/BitArray;)Lcom/google/zxing/common/BitArray;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/google/zxing/pdf417/detector/Detector;->mirror(Lcom/google/zxing/common/BitArray;Lcom/google/zxing/common/BitArray;)Lcom/google/zxing/common/BitArray;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/zxing/common/BitMatrix;->setRow(ILcom/google/zxing/common/BitArray;)V

    .line 159
    add-int/lit8 v6, v1, -0x1

    sub-int/2addr v6, v5

    invoke-static {v2, v4}, Lcom/google/zxing/pdf417/detector/Detector;->mirror(Lcom/google/zxing/common/BitArray;Lcom/google/zxing/common/BitArray;)Lcom/google/zxing/common/BitArray;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/google/zxing/common/BitMatrix;->setRow(ILcom/google/zxing/common/BitArray;)V

    .line 156
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 161
    .end local v5    # "y":I
    :cond_0
    return-void
.end method
