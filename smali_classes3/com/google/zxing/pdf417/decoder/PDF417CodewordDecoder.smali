.class final Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;
.super Ljava/lang/Object;
.source "PDF417CodewordDecoder.java"


# static fields
.field private static final RATIOS_TABLE:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 27
    sget-object v0, Lcom/google/zxing/pdf417/PDF417Common;->SYMBOL_TABLE:[I

    array-length v0, v0

    const/16 v1, 0x8

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v2, F

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    sput-object v0, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->RATIOS_TABLE:[[F

    .line 32
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/google/zxing/pdf417/PDF417Common;->SYMBOL_TABLE:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 33
    sget-object v2, Lcom/google/zxing/pdf417/PDF417Common;->SYMBOL_TABLE:[I

    aget v2, v2, v0

    .line 34
    .local v2, "currentSymbol":I
    and-int/lit8 v3, v2, 0x1

    .line 35
    .local v3, "currentBit":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, v1, :cond_1

    .line 36
    const/4 v5, 0x0

    .line 37
    .local v5, "size":F
    :goto_2
    and-int/lit8 v6, v2, 0x1

    if-ne v6, v3, :cond_0

    .line 38
    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    .line 39
    shr-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 41
    :cond_0
    and-int/lit8 v3, v2, 0x1

    .line 42
    sget-object v6, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->RATIOS_TABLE:[[F

    aget-object v6, v6, v0

    rsub-int/lit8 v7, v4, 0x8

    add-int/lit8 v7, v7, -0x1

    const/high16 v8, 0x41880000    # 17.0f

    div-float v8, v5, v8

    aput v8, v6, v7

    .line 35
    .end local v5    # "size":F
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 32
    .end local v2    # "currentSymbol":I
    .end local v3    # "currentBit":I
    .end local v4    # "j":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method private static getBitValue([I)I
    .locals 9
    .param p0, "moduleBitCount"    # [I

    .line 82
    const-wide/16 v0, 0x0

    .line 83
    .local v0, "result":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_2

    .line 84
    const/4 v3, 0x0

    .local v3, "bit":I
    :goto_1
    aget v4, p0, v2

    if-ge v3, v4, :cond_1

    .line 85
    const/4 v4, 0x1

    shl-long v5, v0, v4

    rem-int/lit8 v7, v2, 0x2

    if-nez v7, :cond_0

    goto :goto_2

    :cond_0
    const/4 v4, 0x0

    :goto_2
    int-to-long v7, v4

    or-long v0, v5, v7

    .line 84
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 83
    .end local v3    # "bit":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 88
    .end local v2    # "i":I
    :cond_2
    long-to-int v2, v0

    return v2
.end method

.method private static getClosestDecodedValue([I)I
    .locals 10
    .param p0, "moduleBitCount"    # [I

    .line 92
    invoke-static {p0}, Lcom/google/zxing/pdf417/PDF417Common;->getBitCountSum([I)I

    move-result v0

    .line 93
    .local v0, "bitCountSum":I
    const/16 v1, 0x8

    new-array v2, v1, [F

    .line 94
    .local v2, "bitCountRatios":[F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 95
    aget v4, p0, v3

    int-to-float v4, v4

    int-to-float v5, v0

    div-float/2addr v4, v5

    aput v4, v2, v3

    .line 94
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 97
    .end local v3    # "i":I
    :cond_0
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    .line 98
    .local v3, "bestMatchError":F
    const/4 v4, -0x1

    .line 99
    .local v4, "bestMatch":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    sget-object v6, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->RATIOS_TABLE:[[F

    array-length v6, v6

    if-ge v5, v6, :cond_3

    .line 100
    const/4 v6, 0x0

    .line 101
    .local v6, "error":F
    const/4 v7, 0x0

    .local v7, "k":I
    :goto_2
    if-ge v7, v1, :cond_1

    .line 102
    sget-object v8, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->RATIOS_TABLE:[[F

    aget-object v8, v8, v5

    aget v8, v8, v7

    aget v9, v2, v7

    sub-float/2addr v8, v9

    .line 103
    .local v8, "diff":F
    mul-float v9, v8, v8

    add-float/2addr v6, v9

    .line 101
    .end local v8    # "diff":F
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 105
    .end local v7    # "k":I
    :cond_1
    cmpg-float v7, v6, v3

    if-gez v7, :cond_2

    .line 106
    move v3, v6

    .line 107
    sget-object v7, Lcom/google/zxing/pdf417/PDF417Common;->SYMBOL_TABLE:[I

    aget v4, v7, v5

    .line 99
    .end local v6    # "error":F
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 110
    .end local v5    # "j":I
    :cond_3
    return v4
.end method

.method private static getDecodedCodewordValue([I)I
    .locals 3
    .param p0, "moduleBitCount"    # [I

    .line 77
    invoke-static {p0}, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->getBitValue([I)I

    move-result v0

    .line 78
    .local v0, "decodedValue":I
    int-to-long v1, v0

    invoke-static {v1, v2}, Lcom/google/zxing/pdf417/PDF417Common;->getCodeword(J)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v0

    :goto_0
    return v2
.end method

.method static getDecodedValue([I)I
    .locals 2
    .param p0, "moduleBitCount"    # [I

    .line 51
    invoke-static {p0}, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->sampleBitCounts([I)[I

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->getDecodedCodewordValue([I)I

    move-result v0

    .line 52
    .local v0, "decodedValue":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 53
    return v0

    .line 55
    :cond_0
    invoke-static {p0}, Lcom/google/zxing/pdf417/decoder/PDF417CodewordDecoder;->getClosestDecodedValue([I)I

    move-result v1

    return v1
.end method

.method private static sampleBitCounts([I)[I
    .locals 8
    .param p0, "moduleBitCount"    # [I

    .line 59
    invoke-static {p0}, Lcom/google/zxing/pdf417/PDF417Common;->getBitCountSum([I)I

    move-result v0

    int-to-float v0, v0

    .line 60
    .local v0, "bitCountSum":F
    const/16 v1, 0x8

    new-array v1, v1, [I

    .line 61
    .local v1, "result":[I
    const/4 v2, 0x0

    .line 62
    .local v2, "bitCountIndex":I
    const/4 v3, 0x0

    .line 63
    .local v3, "sumPreviousBits":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/16 v5, 0x11

    if-ge v4, v5, :cond_1

    .line 64
    const/high16 v5, 0x42080000    # 34.0f

    div-float v5, v0, v5

    int-to-float v6, v4

    mul-float/2addr v6, v0

    const/high16 v7, 0x41880000    # 17.0f

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    .line 67
    .local v5, "sampleIndex":F
    aget v6, p0, v2

    add-int/2addr v6, v3

    int-to-float v6, v6

    cmpg-float v6, v6, v5

    if-gtz v6, :cond_0

    .line 68
    aget v6, p0, v2

    add-int/2addr v3, v6

    .line 69
    add-int/lit8 v2, v2, 0x1

    .line 71
    :cond_0
    aget v6, v1, v2

    add-int/lit8 v6, v6, 0x1

    aput v6, v1, v2

    .line 63
    .end local v5    # "sampleIndex":F
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 73
    .end local v4    # "i":I
    :cond_1
    return-object v1
.end method
