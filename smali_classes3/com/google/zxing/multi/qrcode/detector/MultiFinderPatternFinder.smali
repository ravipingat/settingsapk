.class final Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;
.super Lcom/google/zxing/qrcode/detector/FinderPatternFinder;
.source "MultiFinderPatternFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder$ModuleSizeComparator;
    }
.end annotation


# static fields
.field private static final DIFF_MODSIZE_CUTOFF:F = 0.5f

.field private static final DIFF_MODSIZE_CUTOFF_PERCENT:F = 0.05f

.field private static final EMPTY_RESULT_ARRAY:[Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

.field private static final MAX_MODULE_COUNT_PER_EDGE:F = 180.0f

.field private static final MIN_MODULE_COUNT_PER_EDGE:F = 9.0f


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    sput-object v0, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->EMPTY_RESULT_ARRAY:[Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    return-void
.end method

.method constructor <init>(Lcom/google/zxing/common/BitMatrix;)V
    .locals 0
    .param p1, "image"    # Lcom/google/zxing/common/BitMatrix;

    .line 93
    invoke-direct {p0, p1}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;-><init>(Lcom/google/zxing/common/BitMatrix;)V

    .line 94
    return-void
.end method

.method constructor <init>(Lcom/google/zxing/common/BitMatrix;Lcom/google/zxing/ResultPointCallback;)V
    .locals 0
    .param p1, "image"    # Lcom/google/zxing/common/BitMatrix;
    .param p2, "resultPointCallback"    # Lcom/google/zxing/ResultPointCallback;

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;-><init>(Lcom/google/zxing/common/BitMatrix;Lcom/google/zxing/ResultPointCallback;)V

    .line 98
    return-void
.end method

.method private selectMutipleBestPatterns()[[Lcom/google/zxing/qrcode/detector/FinderPattern;
    .locals 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 107
    invoke-virtual/range {p0 .. p0}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->getPossibleCenters()Ljava/util/List;

    move-result-object v0

    .line 108
    .local v0, "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 110
    .local v1, "size":I
    const/4 v2, 0x3

    if-lt v1, v2, :cond_e

    .line 118
    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v1, v2, :cond_0

    .line 119
    new-array v6, v5, [[Lcom/google/zxing/qrcode/detector/FinderPattern;

    new-array v2, v2, [Lcom/google/zxing/qrcode/detector/FinderPattern;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/zxing/qrcode/detector/FinderPattern;

    aput-object v7, v2, v4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/zxing/qrcode/detector/FinderPattern;

    aput-object v7, v2, v5

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/zxing/qrcode/detector/FinderPattern;

    aput-object v5, v2, v3

    aput-object v2, v6, v4

    return-object v6

    .line 129
    :cond_0
    new-instance v6, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder$ModuleSizeComparator;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder$ModuleSizeComparator;-><init>(Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder$1;)V

    invoke-static {v0, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 146
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v6, "results":Ljava/util/List;, "Ljava/util/List<[Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    const/4 v7, 0x0

    .local v7, "i1":I
    :goto_0
    add-int/lit8 v8, v1, -0x2

    if-ge v7, v8, :cond_c

    .line 149
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 150
    .local v8, "p1":Lcom/google/zxing/qrcode/detector/FinderPattern;
    if-nez v8, :cond_1

    .line 151
    move-object/from16 v20, v0

    move/from16 v23, v1

    move/from16 v18, v3

    move/from16 v19, v5

    goto/16 :goto_5

    .line 154
    :cond_1
    add-int/lit8 v9, v7, 0x1

    .local v9, "i2":I
    :goto_1
    add-int/lit8 v10, v1, -0x1

    if-ge v9, v10, :cond_b

    .line 155
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 156
    .local v10, "p2":Lcom/google/zxing/qrcode/detector/FinderPattern;
    if-nez v10, :cond_2

    .line 157
    move-object/from16 v20, v0

    move/from16 v23, v1

    move/from16 v18, v3

    move/from16 v19, v5

    goto/16 :goto_4

    .line 161
    :cond_2
    invoke-virtual {v8}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v11

    invoke-virtual {v10}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v12

    sub-float/2addr v11, v12

    invoke-virtual {v8}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v12

    invoke-virtual {v10}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v13

    invoke-static {v12, v13}, Ljava/lang/Math;->min(FF)F

    move-result v12

    div-float/2addr v11, v12

    .line 163
    .local v11, "vModSize12":F
    invoke-virtual {v8}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v12

    invoke-virtual {v10}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v13

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    .line 164
    .local v12, "vModSize12A":F
    const/high16 v13, 0x3f000000    # 0.5f

    cmpl-float v14, v12, v13

    const v15, 0x3d4ccccd    # 0.05f

    if-lez v14, :cond_3

    cmpl-float v14, v11, v15

    if-ltz v14, :cond_3

    .line 167
    move-object/from16 v20, v0

    move/from16 v23, v1

    move/from16 v18, v3

    move/from16 v19, v5

    goto/16 :goto_5

    .line 170
    :cond_3
    add-int/lit8 v14, v9, 0x1

    .local v14, "i3":I
    :goto_2
    if-ge v14, v1, :cond_a

    .line 171
    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/zxing/qrcode/detector/FinderPattern;

    .line 172
    .local v16, "p3":Lcom/google/zxing/qrcode/detector/FinderPattern;
    if-nez v16, :cond_4

    .line 173
    move-object/from16 v20, v0

    move/from16 v23, v1

    move/from16 v18, v3

    move/from16 v19, v5

    goto/16 :goto_3

    .line 177
    :cond_4
    invoke-virtual {v10}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v17

    invoke-virtual/range {v16 .. v16}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v18

    sub-float v17, v17, v18

    invoke-virtual {v10}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    div-float v17, v17, v3

    .line 179
    .local v17, "vModSize23":F
    invoke-virtual {v10}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v5

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 180
    .local v3, "vModSize23A":F
    cmpl-float v5, v3, v13

    if-lez v5, :cond_5

    cmpl-float v5, v17, v15

    if-ltz v5, :cond_5

    .line 183
    move-object/from16 v20, v0

    move/from16 v23, v1

    const/16 v18, 0x2

    const/16 v19, 0x1

    goto/16 :goto_4

    .line 186
    :cond_5
    new-array v5, v2, [Lcom/google/zxing/qrcode/detector/FinderPattern;

    aput-object v8, v5, v4

    const/16 v19, 0x1

    aput-object v10, v5, v19

    const/16 v18, 0x2

    aput-object v16, v5, v18

    .line 187
    .local v5, "test":[Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-static {v5}, Lcom/google/zxing/ResultPoint;->orderBestPatterns([Lcom/google/zxing/ResultPoint;)V

    .line 190
    new-instance v2, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    invoke-direct {v2, v5}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;-><init>([Lcom/google/zxing/qrcode/detector/FinderPattern;)V

    .line 191
    .local v2, "info":Lcom/google/zxing/qrcode/detector/FinderPatternInfo;
    invoke-virtual {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;->getTopLeft()Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;->getBottomLeft()Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v13

    invoke-static {v4, v13}, Lcom/google/zxing/ResultPoint;->distance(Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;)F

    move-result v4

    .line 192
    .local v4, "dA":F
    invoke-virtual {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;->getTopRight()Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v13

    invoke-virtual {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;->getBottomLeft()Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v15

    invoke-static {v13, v15}, Lcom/google/zxing/ResultPoint;->distance(Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;)F

    move-result v13

    .line 193
    .local v13, "dC":F
    invoke-virtual {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;->getTopLeft()Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v15

    move-object/from16 v20, v0

    .end local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .local v20, "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    invoke-virtual {v2}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;->getTopRight()Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v0

    invoke-static {v15, v0}, Lcom/google/zxing/ResultPoint;->distance(Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;)F

    move-result v0

    .line 196
    .local v0, "dB":F
    add-float v15, v4, v0

    invoke-virtual {v8}, Lcom/google/zxing/qrcode/detector/FinderPattern;->getEstimatedModuleSize()F

    move-result v21

    const/high16 v22, 0x40000000    # 2.0f

    mul-float v21, v21, v22

    div-float v15, v15, v21

    .line 197
    .local v15, "estimatedModuleCount":F
    const/high16 v21, 0x43340000    # 180.0f

    cmpl-float v21, v15, v21

    if-gtz v21, :cond_9

    const/high16 v21, 0x41100000    # 9.0f

    cmpg-float v21, v15, v21

    if-gez v21, :cond_6

    .line 199
    move/from16 v23, v1

    goto :goto_3

    .line 203
    :cond_6
    sub-float v21, v4, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v22

    div-float v21, v21, v22

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(F)F

    move-result v21

    .line 204
    .local v21, "vABBC":F
    const v22, 0x3dcccccd    # 0.1f

    cmpl-float v23, v21, v22

    if-ltz v23, :cond_7

    .line 205
    move/from16 v23, v1

    goto :goto_3

    .line 209
    :cond_7
    mul-float v23, v4, v4

    mul-float v24, v0, v0

    move/from16 v25, v0

    .end local v0    # "dB":F
    .local v25, "dB":F
    add-float v0, v23, v24

    move/from16 v23, v1

    .end local v1    # "size":I
    .local v23, "size":I
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 211
    .local v0, "dCpy":F
    sub-float v1, v13, v0

    invoke-static {v13, v0}, Ljava/lang/Math;->min(FF)F

    move-result v24

    div-float v1, v1, v24

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 213
    .local v1, "vPyC":F
    cmpl-float v22, v1, v22

    if-ltz v22, :cond_8

    .line 214
    goto :goto_3

    .line 218
    :cond_8
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 197
    .end local v21    # "vABBC":F
    .end local v23    # "size":I
    .end local v25    # "dB":F
    .local v0, "dB":F
    .local v1, "size":I
    :cond_9
    move/from16 v25, v0

    move/from16 v23, v1

    .line 170
    .end local v0    # "dB":F
    .end local v1    # "size":I
    .end local v2    # "info":Lcom/google/zxing/qrcode/detector/FinderPatternInfo;
    .end local v3    # "vModSize23A":F
    .end local v4    # "dA":F
    .end local v5    # "test":[Lcom/google/zxing/qrcode/detector/FinderPattern;
    .end local v13    # "dC":F
    .end local v15    # "estimatedModuleCount":F
    .end local v16    # "p3":Lcom/google/zxing/qrcode/detector/FinderPattern;
    .end local v17    # "vModSize23":F
    .restart local v23    # "size":I
    :goto_3
    add-int/lit8 v14, v14, 0x1

    move/from16 v3, v18

    move/from16 v5, v19

    move-object/from16 v0, v20

    move/from16 v1, v23

    const/4 v2, 0x3

    const/4 v4, 0x0

    const/high16 v13, 0x3f000000    # 0.5f

    const v15, 0x3d4ccccd    # 0.05f

    goto/16 :goto_2

    .end local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v23    # "size":I
    .local v0, "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v1    # "size":I
    :cond_a
    move-object/from16 v20, v0

    move/from16 v23, v1

    move/from16 v18, v3

    move/from16 v19, v5

    .line 154
    .end local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v1    # "size":I
    .end local v10    # "p2":Lcom/google/zxing/qrcode/detector/FinderPattern;
    .end local v11    # "vModSize12":F
    .end local v12    # "vModSize12A":F
    .end local v14    # "i3":I
    .restart local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v23    # "size":I
    :goto_4
    add-int/lit8 v9, v9, 0x1

    move/from16 v3, v18

    move/from16 v5, v19

    move-object/from16 v0, v20

    move/from16 v1, v23

    const/4 v2, 0x3

    const/4 v4, 0x0

    goto/16 :goto_1

    .end local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v23    # "size":I
    .restart local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v1    # "size":I
    :cond_b
    move-object/from16 v20, v0

    move/from16 v23, v1

    move/from16 v18, v3

    move/from16 v19, v5

    .line 148
    .end local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v1    # "size":I
    .end local v8    # "p1":Lcom/google/zxing/qrcode/detector/FinderPattern;
    .end local v9    # "i2":I
    .restart local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v23    # "size":I
    :goto_5
    add-int/lit8 v7, v7, 0x1

    move/from16 v3, v18

    move/from16 v5, v19

    move-object/from16 v0, v20

    move/from16 v1, v23

    const/4 v2, 0x3

    const/4 v4, 0x0

    goto/16 :goto_0

    .end local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v23    # "size":I
    .restart local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v1    # "size":I
    :cond_c
    move-object/from16 v20, v0

    move/from16 v23, v1

    .line 223
    .end local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v1    # "size":I
    .end local v7    # "i1":I
    .restart local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v23    # "size":I
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 224
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [[Lcom/google/zxing/qrcode/detector/FinderPattern;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/google/zxing/qrcode/detector/FinderPattern;

    return-object v0

    .line 228
    :cond_d
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 112
    .end local v6    # "results":Ljava/util/List;, "Ljava/util/List<[Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .end local v23    # "size":I
    .restart local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v1    # "size":I
    :cond_e
    move-object/from16 v20, v0

    .end local v0    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    .restart local v20    # "possibleCenters":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPattern;>;"
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public findMulti(Ljava/util/Map;)[Lcom/google/zxing/qrcode/detector/FinderPatternInfo;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/google/zxing/DecodeHintType;",
            "*>;)[",
            "Lcom/google/zxing/qrcode/detector/FinderPatternInfo;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 232
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    sget-object v4, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    move v4, v2

    .line 233
    .local v4, "tryHarder":Z
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->getImage()Lcom/google/zxing/common/BitMatrix;

    move-result-object v5

    .line 234
    .local v5, "image":Lcom/google/zxing/common/BitMatrix;
    invoke-virtual {v5}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v6

    .line 235
    .local v6, "maxI":I
    invoke-virtual {v5}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v7

    .line 243
    .local v7, "maxJ":I
    int-to-float v8, v6

    const/high16 v9, 0x43640000    # 228.0f

    div-float/2addr v8, v9

    const/high16 v9, 0x40400000    # 3.0f

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 244
    .local v8, "iSkip":I
    const/4 v9, 0x3

    if-lt v8, v9, :cond_1

    if-eqz v4, :cond_2

    .line 245
    :cond_1
    const/4 v8, 0x3

    .line 248
    :cond_2
    const/4 v10, 0x5

    new-array v10, v10, [I

    .line 249
    .local v10, "stateCount":[I
    add-int/lit8 v11, v8, -0x1

    .local v11, "i":I
    :goto_1
    if-ge v11, v6, :cond_a

    .line 251
    aput v2, v10, v2

    .line 252
    aput v2, v10, v3

    .line 253
    const/4 v12, 0x2

    aput v2, v10, v12

    .line 254
    aput v2, v10, v9

    .line 255
    const/4 v13, 0x4

    aput v2, v10, v13

    .line 256
    const/4 v14, 0x0

    .line 257
    .local v14, "currentState":I
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_2
    if-ge v15, v7, :cond_8

    .line 258
    invoke-virtual {v5, v15, v11}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 260
    and-int/lit8 v9, v14, 0x1

    if-ne v9, v3, :cond_3

    .line 261
    add-int/lit8 v14, v14, 0x1

    .line 263
    :cond_3
    aget v9, v10, v14

    add-int/2addr v9, v3

    aput v9, v10, v14

    const/4 v9, 0x3

    goto :goto_3

    .line 265
    :cond_4
    and-int/lit8 v9, v14, 0x1

    if-nez v9, :cond_7

    .line 266
    if-ne v14, v13, :cond_6

    .line 267
    invoke-static {v10}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->foundPatternCross([I)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v0, v10, v11, v15}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->handlePossibleCenter([III)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 269
    const/4 v9, 0x0

    .line 270
    .end local v14    # "currentState":I
    .local v9, "currentState":I
    aput v2, v10, v2

    .line 271
    aput v2, v10, v3

    .line 272
    aput v2, v10, v12

    .line 273
    const/4 v14, 0x3

    aput v2, v10, v14

    .line 274
    aput v2, v10, v13

    move v14, v9

    const/4 v9, 0x3

    goto :goto_3

    .line 276
    .end local v9    # "currentState":I
    .restart local v14    # "currentState":I
    :cond_5
    aget v9, v10, v12

    aput v9, v10, v2

    .line 277
    const/4 v9, 0x3

    aget v16, v10, v9

    aput v16, v10, v3

    .line 278
    aget v16, v10, v13

    aput v16, v10, v12

    .line 279
    aput v3, v10, v9

    .line 280
    aput v2, v10, v13

    .line 281
    const/4 v14, 0x3

    goto :goto_3

    .line 284
    :cond_6
    const/4 v9, 0x3

    add-int/lit8 v14, v14, 0x1

    aget v16, v10, v14

    add-int/lit8 v16, v16, 0x1

    aput v16, v10, v14

    goto :goto_3

    .line 287
    :cond_7
    const/4 v9, 0x3

    aget v16, v10, v14

    add-int/lit8 v16, v16, 0x1

    aput v16, v10, v14

    .line 257
    :goto_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 292
    .end local v15    # "j":I
    :cond_8
    invoke-static {v10}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->foundPatternCross([I)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 293
    invoke-virtual {v0, v10, v11, v7}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->handlePossibleCenter([III)Z

    .line 249
    .end local v14    # "currentState":I
    :cond_9
    add-int/2addr v11, v8

    goto :goto_1

    .line 296
    .end local v11    # "i":I
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->selectMutipleBestPatterns()[[Lcom/google/zxing/qrcode/detector/FinderPattern;

    move-result-object v2

    .line 297
    .local v2, "patternInfo":[[Lcom/google/zxing/qrcode/detector/FinderPattern;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/zxing/qrcode/detector/FinderPatternInfo;>;"
    move-object v9, v2

    .local v9, "arr$":[[Lcom/google/zxing/qrcode/detector/FinderPattern;
    array-length v11, v9

    .local v11, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_4
    if-ge v12, v11, :cond_b

    aget-object v13, v9, v12

    .line 299
    .local v13, "pattern":[Lcom/google/zxing/qrcode/detector/FinderPattern;
    invoke-static {v13}, Lcom/google/zxing/ResultPoint;->orderBestPatterns([Lcom/google/zxing/ResultPoint;)V

    .line 300
    new-instance v14, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    invoke-direct {v14, v13}, Lcom/google/zxing/qrcode/detector/FinderPatternInfo;-><init>([Lcom/google/zxing/qrcode/detector/FinderPattern;)V

    invoke-interface {v3, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    .end local v13    # "pattern":[Lcom/google/zxing/qrcode/detector/FinderPattern;
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 303
    .end local v9    # "arr$":[[Lcom/google/zxing/qrcode/detector/FinderPattern;
    .end local v11    # "len$":I
    .end local v12    # "i$":I
    :cond_b
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 304
    sget-object v9, Lcom/google/zxing/multi/qrcode/detector/MultiFinderPatternFinder;->EMPTY_RESULT_ARRAY:[Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    return-object v9

    .line 306
    :cond_c
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    invoke-interface {v3, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/google/zxing/qrcode/detector/FinderPatternInfo;

    return-object v9
.end method
