.class public final Landroid/support/asynclayoutinflater/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/asynclayoutinflater/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static AlertDialogTitleStyleDark:I

.field public static AlertDialogTitleStyleLight:I

.field public static AlertDialog_AppCompat:I

.field public static AlertDialog_AppCompat_Light:I

.field public static Animation_AppCompat_Dialog:I

.field public static Animation_AppCompat_DropDownUp:I

.field public static Animation_AppCompat_Tooltip:I

.field public static Animation_DropDownUp:I

.field public static Animation_Material:I

.field public static Animation_Material_Dialog:I

.field public static Animation_VoiceActivity:I

.field public static Base_AlertDialog_AppCompat:I

.field public static Base_AlertDialog_AppCompat_Light:I

.field public static Base_Animation_AppCompat_Dialog:I

.field public static Base_Animation_AppCompat_DropDownUp:I

.field public static Base_Animation_AppCompat_Tooltip:I

.field public static Base_DialogWindowTitleBackground_AppCompat:I

.field public static Base_DialogWindowTitle_AppCompat:I

.field public static Base_TextAppearance_AppCompat:I

.field public static Base_TextAppearance_AppCompat_Body1:I

.field public static Base_TextAppearance_AppCompat_Body2:I

.field public static Base_TextAppearance_AppCompat_Button:I

.field public static Base_TextAppearance_AppCompat_Caption:I

.field public static Base_TextAppearance_AppCompat_Display1:I

.field public static Base_TextAppearance_AppCompat_Display2:I

.field public static Base_TextAppearance_AppCompat_Display3:I

.field public static Base_TextAppearance_AppCompat_Display4:I

.field public static Base_TextAppearance_AppCompat_Headline:I

.field public static Base_TextAppearance_AppCompat_Inverse:I

.field public static Base_TextAppearance_AppCompat_Large:I

.field public static Base_TextAppearance_AppCompat_Large_Inverse:I

.field public static Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I

.field public static Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I

.field public static Base_TextAppearance_AppCompat_Medium:I

.field public static Base_TextAppearance_AppCompat_Medium_Inverse:I

.field public static Base_TextAppearance_AppCompat_Menu:I

.field public static Base_TextAppearance_AppCompat_SearchResult:I

.field public static Base_TextAppearance_AppCompat_SearchResult_Subtitle:I

.field public static Base_TextAppearance_AppCompat_SearchResult_Title:I

.field public static Base_TextAppearance_AppCompat_Small:I

.field public static Base_TextAppearance_AppCompat_Small_Inverse:I

.field public static Base_TextAppearance_AppCompat_Subhead:I

.field public static Base_TextAppearance_AppCompat_Subhead_Inverse:I

.field public static Base_TextAppearance_AppCompat_Title:I

.field public static Base_TextAppearance_AppCompat_Title_Inverse:I

.field public static Base_TextAppearance_AppCompat_Tooltip:I

.field public static Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I

.field public static Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I

.field public static Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I

.field public static Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I

.field public static Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I

.field public static Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I

.field public static Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I

.field public static Base_TextAppearance_AppCompat_Widget_Button:I

.field public static Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I

.field public static Base_TextAppearance_AppCompat_Widget_Button_Colored:I

.field public static Base_TextAppearance_AppCompat_Widget_Button_Inverse:I

.field public static Base_TextAppearance_AppCompat_Widget_DropDownItem:I

.field public static Base_TextAppearance_AppCompat_Widget_PopupMenu_Header:I

.field public static Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I

.field public static Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I

.field public static Base_TextAppearance_AppCompat_Widget_Switch:I

.field public static Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I

.field public static Base_TextAppearance_Widget_ActionBar_Title:I

.field public static Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I

.field public static Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I

.field public static Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I

.field public static Base_ThemeOverlay_AppCompat:I

.field public static Base_ThemeOverlay_AppCompat_ActionBar:I

.field public static Base_ThemeOverlay_AppCompat_Dark:I

.field public static Base_ThemeOverlay_AppCompat_Dark_ActionBar:I

.field public static Base_ThemeOverlay_AppCompat_Dialog:I

.field public static Base_ThemeOverlay_AppCompat_Dialog_Alert:I

.field public static Base_ThemeOverlay_AppCompat_Light:I

.field public static Base_Theme_AppCompat:I

.field public static Base_Theme_AppCompat_CompactMenu:I

.field public static Base_Theme_AppCompat_Dialog:I

.field public static Base_Theme_AppCompat_DialogWhenLarge:I

.field public static Base_Theme_AppCompat_Dialog_Alert:I

.field public static Base_Theme_AppCompat_Dialog_FixedSize:I

.field public static Base_Theme_AppCompat_Dialog_MinWidth:I

.field public static Base_Theme_AppCompat_Light:I

.field public static Base_Theme_AppCompat_Light_DarkActionBar:I

.field public static Base_Theme_AppCompat_Light_Dialog:I

.field public static Base_Theme_AppCompat_Light_DialogWhenLarge:I

.field public static Base_Theme_AppCompat_Light_Dialog_Alert:I

.field public static Base_Theme_AppCompat_Light_Dialog_FixedSize:I

.field public static Base_Theme_AppCompat_Light_Dialog_MinWidth:I

.field public static Base_V21_ThemeOverlay_AppCompat_Dialog:I

.field public static Base_V21_Theme_AppCompat:I

.field public static Base_V21_Theme_AppCompat_Dialog:I

.field public static Base_V21_Theme_AppCompat_Light:I

.field public static Base_V21_Theme_AppCompat_Light_Dialog:I

.field public static Base_V22_Theme_AppCompat:I

.field public static Base_V22_Theme_AppCompat_Light:I

.field public static Base_V23_Theme_AppCompat:I

.field public static Base_V23_Theme_AppCompat_Light:I

.field public static Base_V26_Theme_AppCompat:I

.field public static Base_V26_Theme_AppCompat_Light:I

.field public static Base_V26_Widget_AppCompat_Toolbar:I

.field public static Base_V28_Theme_AppCompat:I

.field public static Base_V28_Theme_AppCompat_Light:I

.field public static Base_V7_ThemeOverlay_AppCompat_Dialog:I

.field public static Base_V7_Theme_AppCompat:I

.field public static Base_V7_Theme_AppCompat_Dialog:I

.field public static Base_V7_Theme_AppCompat_Light:I

.field public static Base_V7_Theme_AppCompat_Light_Dialog:I

.field public static Base_V7_Widget_AppCompat_AutoCompleteTextView:I

.field public static Base_V7_Widget_AppCompat_EditText:I

.field public static Base_V7_Widget_AppCompat_Toolbar:I

.field public static Base_Widget_AppCompat_ActionBar:I

.field public static Base_Widget_AppCompat_ActionBar_Solid:I

.field public static Base_Widget_AppCompat_ActionBar_TabBar:I

.field public static Base_Widget_AppCompat_ActionBar_TabText:I

.field public static Base_Widget_AppCompat_ActionBar_TabView:I

.field public static Base_Widget_AppCompat_ActionButton:I

.field public static Base_Widget_AppCompat_ActionButton_CloseMode:I

.field public static Base_Widget_AppCompat_ActionButton_Overflow:I

.field public static Base_Widget_AppCompat_ActionMode:I

.field public static Base_Widget_AppCompat_ActivityChooserView:I

.field public static Base_Widget_AppCompat_AutoCompleteTextView:I

.field public static Base_Widget_AppCompat_Button:I

.field public static Base_Widget_AppCompat_ButtonBar:I

.field public static Base_Widget_AppCompat_ButtonBar_AlertDialog:I

.field public static Base_Widget_AppCompat_Button_Borderless:I

.field public static Base_Widget_AppCompat_Button_Borderless_Colored:I

.field public static Base_Widget_AppCompat_Button_ButtonBar_AlertDialog:I

.field public static Base_Widget_AppCompat_Button_Colored:I

.field public static Base_Widget_AppCompat_Button_Small:I

.field public static Base_Widget_AppCompat_CompoundButton_CheckBox:I

.field public static Base_Widget_AppCompat_CompoundButton_RadioButton:I

.field public static Base_Widget_AppCompat_CompoundButton_Switch:I

.field public static Base_Widget_AppCompat_DrawerArrowToggle:I

.field public static Base_Widget_AppCompat_DrawerArrowToggle_Common:I

.field public static Base_Widget_AppCompat_DropDownItem_Spinner:I

.field public static Base_Widget_AppCompat_EditText:I

.field public static Base_Widget_AppCompat_ImageButton:I

.field public static Base_Widget_AppCompat_Light_ActionBar:I

.field public static Base_Widget_AppCompat_Light_ActionBar_Solid:I

.field public static Base_Widget_AppCompat_Light_ActionBar_TabBar:I

.field public static Base_Widget_AppCompat_Light_ActionBar_TabText:I

.field public static Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I

.field public static Base_Widget_AppCompat_Light_ActionBar_TabView:I

.field public static Base_Widget_AppCompat_Light_PopupMenu:I

.field public static Base_Widget_AppCompat_Light_PopupMenu_Overflow:I

.field public static Base_Widget_AppCompat_ListMenuView:I

.field public static Base_Widget_AppCompat_ListPopupWindow:I

.field public static Base_Widget_AppCompat_ListView:I

.field public static Base_Widget_AppCompat_ListView_DropDown:I

.field public static Base_Widget_AppCompat_ListView_Menu:I

.field public static Base_Widget_AppCompat_PopupMenu:I

.field public static Base_Widget_AppCompat_PopupMenu_Overflow:I

.field public static Base_Widget_AppCompat_PopupWindow:I

.field public static Base_Widget_AppCompat_ProgressBar:I

.field public static Base_Widget_AppCompat_ProgressBar_Horizontal:I

.field public static Base_Widget_AppCompat_RatingBar:I

.field public static Base_Widget_AppCompat_RatingBar_Indicator:I

.field public static Base_Widget_AppCompat_RatingBar_Small:I

.field public static Base_Widget_AppCompat_SearchView:I

.field public static Base_Widget_AppCompat_SearchView_ActionBar:I

.field public static Base_Widget_AppCompat_SeekBar:I

.field public static Base_Widget_AppCompat_SeekBar_Discrete:I

.field public static Base_Widget_AppCompat_Spinner:I

.field public static Base_Widget_AppCompat_Spinner_Underlined:I

.field public static Base_Widget_AppCompat_TextView_SpinnerItem:I

.field public static Base_Widget_AppCompat_Toolbar:I

.field public static Base_Widget_AppCompat_Toolbar_Button_Navigation:I

.field public static Base_Widget_Design_AppBarLayout:I

.field public static CardView:I

.field public static CardView_Dark:I

.field public static CardView_Light:I

.field public static DialogWindowTitle:I

.field public static DialogWindowTitleBackground:I

.field public static DialogWindowTitleBackground_Material:I

.field public static DialogWindowTitleBackground_Material_Light:I

.field public static DialogWindowTitle_Material:I

.field public static DialogWindowTitle_Material_Light:I

.field public static OPAnimation_AppCompat_DropDownUp:I

.field public static OPAnimation_AppCompat_Tooltip:I

.field public static OPBase_Animation_AppCompat_Tooltip:I

.field public static OPBase_TextAppearance_AppCompat:I

.field public static OPBase_Widget_AppCompat_DrawerArrowToggle:I

.field public static OPBase_Widget_AppCompat_DrawerArrowToggle_Common:I

.field public static OPPreference:I

.field public static OPPreferenceActivity:I

.field public static OPPreferenceActivity_Material:I

.field public static OPPreferenceFragment:I

.field public static OPPreferenceFragmentList:I

.field public static OPPreferenceFragmentList_Material:I

.field public static OPPreferenceFragment_Material:I

.field public static OPPreferenceHeaderList:I

.field public static OPPreferenceHeaderList_Material:I

.field public static OPPreferenceHeaderPanel:I

.field public static OPPreferenceHeaderPanel_Material:I

.field public static OPPreferencePanel:I

.field public static OPPreferencePanel_Dialog:I

.field public static OPPreferencePanel_Material:I

.field public static OPPreferencePanel_Material_Dialog:I

.field public static OPPreference_Material:I

.field public static OPPreference_Material_Category:I

.field public static OPPreference_Material_CheckBoxPreference:I

.field public static OPPreference_Material_DialogPreference:I

.field public static OPPreference_Material_DialogPreference_EditTextPreference:I

.field public static OPPreference_Material_DialogPreference_SeekBarPreference:I

.field public static OPPreference_Material_DialogPreference_YesNoPreference:I

.field public static OPPreference_Material_Information:I

.field public static OPPreference_Material_PreferenceScreen:I

.field public static OPPreference_Material_RingtonePreference:I

.field public static OPPreference_Material_SeekBarPreference:I

.field public static OPPreference_Material_SwitchPreference:I

.field public static OPRtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I

.field public static OPRtlOverlay_Widget_AppCompat_PopupMenuItem:I

.field public static OPRtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I

.field public static OPRtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I

.field public static OPTextAppearance_AppCompat_Tooltip:I

.field public static OPTextAppearance_Design_CollapsingToolbar_Expanded:I

.field public static OPTextAppearance_Large_Inverse_NumberPickerInputText:I

.field public static OPTextAppearance_Material_DatePicker_DateLabel:I

.field public static OPTextAppearance_Material_DatePicker_List_YearLabel:I

.field public static OPTextAppearance_Material_DatePicker_List_YearLabel_Activated:I

.field public static OPTextAppearance_Material_DatePicker_YearLabel:I

.field public static OPTextAppearance_Material_NumberPicker:I

.field public static OPTextAppearance_Material_TimePicker_AmPmLabel:I

.field public static OPTextAppearance_Material_TimePicker_InputField:I

.field public static OPTextAppearance_Material_TimePicker_InputFieldPrompt:I

.field public static OPTextAppearance_Material_TimePicker_InputFieldUnActive:I

.field public static OPTextAppearance_Material_TimePicker_InputHeader:I

.field public static OPTextAppearance_Material_TimePicker_PromptLabel:I

.field public static OPTextAppearance_Material_TimePicker_TimeLabel:I

.field public static OPTextAppearance_Material_TimePicker_TimeLabelUnActivated:I

.field public static OPTextAppearance_Material_TimePicker_TimeSeparator:I

.field public static OPTheme_AppCompat_CompactMenu:I

.field public static OPWidget_AppCompat_DrawerArrowToggle:I

.field public static OPWidget_AppCompat_ListView_Menu:I

.field public static OPWidget_Material_NumberPicker:I

.field public static OPWidget_Material_Toolbar:I

.field public static OPWidget_Material_Toolbar_Button_Navigation:I

.field public static OPWidget_NumberPicker:I

.field public static OPWidget_Toolbar:I

.field public static OnePlusAlertDialog:I

.field public static OnePlusAlertDialog_Light:I

.field public static OnePlusAlertProgressDialog:I

.field public static OnePlusBottomNavigationViewStyle:I

.field public static OnePlusToast:I

.field public static OnePlusToastLight:I

.field public static OnePlus_DeviceDefault_Preference_Material:I

.field public static OnePlus_DeviceDefault_Preference_Material_CheckBoxPreference:I

.field public static OnePlus_DeviceDefault_Preference_Material_DialogPreference:I

.field public static OnePlus_DeviceDefault_Preference_Material_DialogPreference_EditTextPreference:I

.field public static OnePlus_DeviceDefault_Preference_Material_PreferenceScreen:I

.field public static OnePlus_DeviceDefault_Preference_Material_RingtonePreference:I

.field public static OnePlus_DeviceDefault_Preference_Material_SeekBarPreference:I

.field public static OnePlus_DeviceDefault_Preference_Material_SwitchPreference:I

.field public static OnePlus_DeviceDefault_Widget_Material_Button:I

.field public static OnePlus_DeviceDefault_Widget_Material_Button_Borderless:I

.field public static OnePlus_DeviceDefault_Widget_Material_Button_Borderless_Mini:I

.field public static OnePlus_DeviceDefault_Widget_Material_Button_Mini:I

.field public static OnePlus_Preference_Material:I

.field public static OnePlus_Preference_Material_CheckBoxPreference:I

.field public static OnePlus_Preference_Material_DialogPreference:I

.field public static OnePlus_Preference_Material_DialogPreference_EditTextPreference:I

.field public static OnePlus_Preference_Material_Light:I

.field public static OnePlus_Preference_Material_Light_CheckBoxPreference:I

.field public static OnePlus_Preference_Material_Light_DialogPreference:I

.field public static OnePlus_Preference_Material_Light_DialogPreference_EditTextPreference:I

.field public static OnePlus_Preference_Material_Light_PreferenceScreen:I

.field public static OnePlus_Preference_Material_Light_RingtonePreference:I

.field public static OnePlus_Preference_Material_Light_SeekBarPreference:I

.field public static OnePlus_Preference_Material_Light_SwitchPreference:I

.field public static OnePlus_Preference_Material_PreferenceScreen:I

.field public static OnePlus_Preference_Material_RingtonePreference:I

.field public static OnePlus_Preference_Material_SeekBarPreference:I

.field public static OnePlus_Preference_Material_SwitchPreference:I

.field public static OnePlus_TextAppearance_Material_Button:I

.field public static OnePlus_TextAppearance_Material_Light_Button:I

.field public static OnePlus_TextAppearance_Material_Raised_Button:I

.field public static OnePlus_TextAppearance_Material_Raised_Light_Button:I

.field public static OnePlus_Theme_DeviceDefault_Dialog_Picker:I

.field public static OnePlus_Widget_Design_FloatingActionButton:I

.field public static OnePlus_Widget_Design_RectangleFloatingActionButton:I

.field public static OnePlus_Widget_Design_Snackbar:I

.field public static OnePlus_Widget_Design_SwitchFloatingActionButton:I

.field public static OnePlus_Widget_Material_Button:I

.field public static OnePlus_Widget_Material_Button_AlertDialog:I

.field public static OnePlus_Widget_Material_Button_Borderless:I

.field public static OnePlus_Widget_Material_Button_Borderless_Mini:I

.field public static OnePlus_Widget_Material_Button_Mini:I

.field public static OnePlus_Widget_Material_Light_Button:I

.field public static OnePlus_Widget_Material_Light_Button_AlertDialog:I

.field public static OnePlus_Widget_Material_Light_Button_Borderless:I

.field public static OnePlus_Widget_Material_Light_Button_Borderless_Mini:I

.field public static OnePlus_Widget_Material_Light_Button_Mini:I

.field public static OneplusThemeBase:I

.field public static OneplusThemeLightBase:I

.field public static Oneplus_Animation_Material_Dialog:I

.field public static Oneplus_Base_Widget_Design_OPTabLayout:I

.field public static Oneplus_CardView:I

.field public static Oneplus_CardView_Dark:I

.field public static Oneplus_CardView_Light:I

.field public static Oneplus_DeviceDefault_OPToast:I

.field public static Oneplus_DeviceDefault_PreferenceFragmentList_Material:I

.field public static Oneplus_DeviceDefault_PreferenceFragment_Material:I

.field public static Oneplus_DeviceDefault_Preference_Material_Category:I

.field public static Oneplus_DeviceDefault_Theme_Material_Dialog:I

.field public static Oneplus_DeviceDefault_Theme_Material_Dialog_Alert:I

.field public static Oneplus_DeviceDefault_Theme_Material_Dialog_Picker:I

.field public static Oneplus_DeviceDefault_Widget_Material_ActionBar_Solid:I

.field public static Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox:I

.field public static Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox_Circle:I

.field public static Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox_Circle_Special:I

.field public static Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox_Special:I

.field public static Oneplus_DeviceDefault_Widget_Material_CompoundButton_RadioButton:I

.field public static Oneplus_DeviceDefault_Widget_Material_CompoundButton_Switch:I

.field public static Oneplus_DeviceDefault_Widget_Material_DatePicker:I

.field public static Oneplus_DeviceDefault_Widget_Material_EditText:I

.field public static Oneplus_DeviceDefault_Widget_Material_ImageCompoundButton_CheckBox:I

.field public static Oneplus_DeviceDefault_Widget_Material_ProgressBar:I

.field public static Oneplus_DeviceDefault_Widget_Material_ProgressBar_Horizontal:I

.field public static Oneplus_DeviceDefault_Widget_Material_ProgressBar_Large:I

.field public static Oneplus_DeviceDefault_Widget_Material_ProgressBar_Small:I

.field public static Oneplus_DeviceDefault_Widget_Material_SeekBar:I

.field public static Oneplus_DeviceDefault_Widget_Material_TimePicker:I

.field public static Oneplus_PreferenceFragmentList_Material:I

.field public static Oneplus_PreferenceFragmentList_Material_Light:I

.field public static Oneplus_PreferenceFragment_Material:I

.field public static Oneplus_PreferenceFragment_Material_Light:I

.field public static Oneplus_Preference_Material_Category:I

.field public static Oneplus_Preference_Material_Light_Category:I

.field public static Oneplus_TextAppearance_Design_Tab:I

.field public static Oneplus_TextAppearance_Material_Body1:I

.field public static Oneplus_TextAppearance_Material_Light_NumberPickerDialog_Minute:I

.field public static Oneplus_TextAppearance_Material_Light_Widget_Calendar_Day:I

.field public static Oneplus_TextAppearance_Material_Light_Widget_Calendar_DayOfWeek:I

.field public static Oneplus_TextAppearance_Material_Light_Widget_Calendar_Month:I

.field public static Oneplus_TextAppearance_Material_Light_Widget_TabWidget:I

.field public static Oneplus_TextAppearance_Material_Menu:I

.field public static Oneplus_TextAppearance_Material_Menu_Dark:I

.field public static Oneplus_TextAppearance_Material_Menu_Light:I

.field public static Oneplus_TextAppearance_Material_NumberPickerDialog_Minute:I

.field public static Oneplus_TextAppearance_Material_Widget_ActionBar_Subtitle:I

.field public static Oneplus_TextAppearance_Material_Widget_ActionBar_Title:I

.field public static Oneplus_TextAppearance_Material_Widget_Calendar_Day:I

.field public static Oneplus_TextAppearance_Material_Widget_Calendar_DayOfWeek:I

.field public static Oneplus_TextAppearance_Material_Widget_Calendar_Month:I

.field public static Oneplus_TextAppearance_Material_Widget_PopupMenu_Header:I

.field public static Oneplus_TextAppearance_Material_Widget_TabWidget:I

.field public static Oneplus_TextAppearance_Toast:I

.field public static Oneplus_ThemeOverlay:I

.field public static Oneplus_ThemeOverlay_Material:I

.field public static Oneplus_ThemeOverlay_Material_ActionBar:I

.field public static Oneplus_ThemeOverlay_Material_Dark:I

.field public static Oneplus_ThemeOverlay_Material_Dark_ActionBar:I

.field public static Oneplus_ThemeOverlay_Material_Dialog:I

.field public static Oneplus_ThemeOverlay_Material_Dialog_Alert:I

.field public static Oneplus_ThemeOverlay_Material_Light:I

.field public static Oneplus_Theme_DeviceDefault:I

.field public static Oneplus_Theme_DeviceDefault_BaseDialog:I

.field public static Oneplus_Theme_DeviceDefault_CompactMenu:I

.field public static Oneplus_Theme_DeviceDefault_DarkActionBar:I

.field public static Oneplus_Theme_DeviceDefault_Dialog:I

.field public static Oneplus_Theme_DeviceDefault_DialogWhenLarge:I

.field public static Oneplus_Theme_DeviceDefault_DialogWhenLarge_DarkActionBar:I

.field public static Oneplus_Theme_DeviceDefault_DialogWhenLarge_NoActionBar:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_Alert:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_BaseAlert:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_FixedSize:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_MinWidth:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_NoActionBar:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_NoActionBar_FixedSize:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_NoActionBar_MinWidth:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_NoFrame:I

.field public static Oneplus_Theme_DeviceDefault_Dialog_Presentation:I

.field public static Oneplus_Theme_DeviceDefault_InputMethod:I

.field public static Oneplus_Theme_DeviceDefault_LightStatusBar:I

.field public static Oneplus_Theme_DeviceDefault_NoActionBar:I

.field public static Oneplus_Theme_DeviceDefault_NoActionBar_Fullscreen:I

.field public static Oneplus_Theme_DeviceDefault_NoActionBar_Overscan:I

.field public static Oneplus_Theme_DeviceDefault_NoActionBar_TranslucentDecor:I

.field public static Oneplus_Theme_DeviceDefault_Panel:I

.field public static Oneplus_Theme_DeviceDefault_SearchBar:I

.field public static Oneplus_Theme_DeviceDefault_Settings:I

.field public static Oneplus_Theme_DeviceDefault_Settings_BaseDialog:I

.field public static Oneplus_Theme_DeviceDefault_Settings_CompactMenu:I

.field public static Oneplus_Theme_DeviceDefault_Settings_Dialog:I

.field public static Oneplus_Theme_DeviceDefault_Settings_DialogWhenLarge:I

.field public static Oneplus_Theme_DeviceDefault_Settings_DialogWhenLarge_NoActionBar:I

.field public static Oneplus_Theme_DeviceDefault_Settings_Dialog_Alert:I

.field public static Oneplus_Theme_DeviceDefault_Settings_Dialog_BaseAlert:I

.field public static Oneplus_Theme_DeviceDefault_Settings_Dialog_Presentation:I

.field public static Oneplus_Theme_DeviceDefault_Settings_NoActionBar:I

.field public static Oneplus_Theme_DeviceDefault_Settings_SearchBar:I

.field public static Oneplus_Theme_DeviceDefault_Voice:I

.field public static Oneplus_Theme_DeviceDefault_VoiceInteractionSession:I

.field public static Oneplus_Theme_DeviceDefault_Wallpaper:I

.field public static Oneplus_Theme_DeviceDefault_Wallpaper_NoTitleBar:I

.field public static Oneplus_Theme_Material:I

.field public static Oneplus_Theme_Material_AmberTypeA:I

.field public static Oneplus_Theme_Material_AmberTypeB:I

.field public static Oneplus_Theme_Material_BaseDialog:I

.field public static Oneplus_Theme_Material_BlueGreyTypeA:I

.field public static Oneplus_Theme_Material_BlueGreyTypeB:I

.field public static Oneplus_Theme_Material_BlueTypeA:I

.field public static Oneplus_Theme_Material_BlueTypeB:I

.field public static Oneplus_Theme_Material_BrownTypeA:I

.field public static Oneplus_Theme_Material_BrownTypeB:I

.field public static Oneplus_Theme_Material_CompactMenu:I

.field public static Oneplus_Theme_Material_CyanTypeA:I

.field public static Oneplus_Theme_Material_CyanTypeB:I

.field public static Oneplus_Theme_Material_DarkActionBar:I

.field public static Oneplus_Theme_Material_DeepOrangeTypeA:I

.field public static Oneplus_Theme_Material_DeepOrangeTypeB:I

.field public static Oneplus_Theme_Material_DeepPurpleTypeA:I

.field public static Oneplus_Theme_Material_DeepPurpleTypeB:I

.field public static Oneplus_Theme_Material_Dialog:I

.field public static Oneplus_Theme_Material_DialogWhenLarge:I

.field public static Oneplus_Theme_Material_DialogWhenLarge_DarkActionBar:I

.field public static Oneplus_Theme_Material_DialogWhenLarge_NoActionBar:I

.field public static Oneplus_Theme_Material_Dialog_Alert:I

.field public static Oneplus_Theme_Material_Dialog_BaseAlert:I

.field public static Oneplus_Theme_Material_Dialog_FixedSize:I

.field public static Oneplus_Theme_Material_Dialog_MinWidth:I

.field public static Oneplus_Theme_Material_Dialog_NoActionBar:I

.field public static Oneplus_Theme_Material_Dialog_NoActionBar_FixedSize:I

.field public static Oneplus_Theme_Material_Dialog_NoActionBar_MinWidth:I

.field public static Oneplus_Theme_Material_Dialog_NoFrame:I

.field public static Oneplus_Theme_Material_Dialog_Picker:I

.field public static Oneplus_Theme_Material_Dialog_Presentation:I

.field public static Oneplus_Theme_Material_GreenTypeA:I

.field public static Oneplus_Theme_Material_GreenTypeB:I

.field public static Oneplus_Theme_Material_GreyTypeA:I

.field public static Oneplus_Theme_Material_GreyTypeB:I

.field public static Oneplus_Theme_Material_IndigoTypeA:I

.field public static Oneplus_Theme_Material_IndigoTypeB:I

.field public static Oneplus_Theme_Material_InputMethod:I

.field public static Oneplus_Theme_Material_InputMethod_FromDark:I

.field public static Oneplus_Theme_Material_Light:I

.field public static Oneplus_Theme_Material_LightBlueTypeA:I

.field public static Oneplus_Theme_Material_LightBlueTypeB:I

.field public static Oneplus_Theme_Material_LightGreenTypeA:I

.field public static Oneplus_Theme_Material_LightGreenTypeB:I

.field public static Oneplus_Theme_Material_LightStatusBar:I

.field public static Oneplus_Theme_Material_Light_AmberTypeA:I

.field public static Oneplus_Theme_Material_Light_AmberTypeB:I

.field public static Oneplus_Theme_Material_Light_BaseDialog:I

.field public static Oneplus_Theme_Material_Light_BlueGreyTypeA:I

.field public static Oneplus_Theme_Material_Light_BlueGreyTypeB:I

.field public static Oneplus_Theme_Material_Light_BlueTypeA:I

.field public static Oneplus_Theme_Material_Light_BlueTypeB:I

.field public static Oneplus_Theme_Material_Light_BrownTypeA:I

.field public static Oneplus_Theme_Material_Light_BrownTypeB:I

.field public static Oneplus_Theme_Material_Light_CompactMenu:I

.field public static Oneplus_Theme_Material_Light_CyanTypeA:I

.field public static Oneplus_Theme_Material_Light_CyanTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_AmberTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_AmberTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_BlueGreyTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_BlueGreyTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_BlueTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_BlueTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_BrownTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_BrownTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_CyanTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_CyanTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_DeepOrangeTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_DeepOrangeTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_DeepPurpleTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_DeepPurpleTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_GreenTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_GreenTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_GreyTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_GreyTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_IndigoTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_IndigoTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_LightBlueTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_LightBlueTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_LightGreenTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_LightGreenTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_LimeTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_LimeTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_OrangeTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_OrangeTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_PinkTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_PinkTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_PurpleTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_PurpleTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_RedTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_RedTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_TealTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_TealTypeB:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_YellowTypeA:I

.field public static Oneplus_Theme_Material_Light_DarkActionBar_YellowTypeB:I

.field public static Oneplus_Theme_Material_Light_DeepOrangeTypeA:I

.field public static Oneplus_Theme_Material_Light_DeepOrangeTypeB:I

.field public static Oneplus_Theme_Material_Light_DeepPurpleTypeA:I

.field public static Oneplus_Theme_Material_Light_DeepPurpleTypeB:I

.field public static Oneplus_Theme_Material_Light_Dialog:I

.field public static Oneplus_Theme_Material_Light_DialogWhenLarge:I

.field public static Oneplus_Theme_Material_Light_DialogWhenLarge_DarkActionBar:I

.field public static Oneplus_Theme_Material_Light_DialogWhenLarge_NoActionBar:I

.field public static Oneplus_Theme_Material_Light_Dialog_Alert:I

.field public static Oneplus_Theme_Material_Light_Dialog_BaseAlert:I

.field public static Oneplus_Theme_Material_Light_Dialog_FixedSize:I

.field public static Oneplus_Theme_Material_Light_Dialog_MinWidth:I

.field public static Oneplus_Theme_Material_Light_Dialog_NoActionBar:I

.field public static Oneplus_Theme_Material_Light_Dialog_NoActionBar_FixedSize:I

.field public static Oneplus_Theme_Material_Light_Dialog_NoActionBar_MinWidth:I

.field public static Oneplus_Theme_Material_Light_Dialog_NoFrame:I

.field public static Oneplus_Theme_Material_Light_Dialog_Picker:I

.field public static Oneplus_Theme_Material_Light_Dialog_Presentation:I

.field public static Oneplus_Theme_Material_Light_GreenTypeA:I

.field public static Oneplus_Theme_Material_Light_GreenTypeB:I

.field public static Oneplus_Theme_Material_Light_GreyTypeA:I

.field public static Oneplus_Theme_Material_Light_GreyTypeB:I

.field public static Oneplus_Theme_Material_Light_IndigoTypeA:I

.field public static Oneplus_Theme_Material_Light_IndigoTypeB:I

.field public static Oneplus_Theme_Material_Light_LightBlueTypeA:I

.field public static Oneplus_Theme_Material_Light_LightBlueTypeB:I

.field public static Oneplus_Theme_Material_Light_LightGreenTypeA:I

.field public static Oneplus_Theme_Material_Light_LightGreenTypeB:I

.field public static Oneplus_Theme_Material_Light_LightStatusBar:I

.field public static Oneplus_Theme_Material_Light_LimeTypeA:I

.field public static Oneplus_Theme_Material_Light_LimeTypeB:I

.field public static Oneplus_Theme_Material_Light_NoActionBar:I

.field public static Oneplus_Theme_Material_Light_NoActionBar_Fullscreen:I

.field public static Oneplus_Theme_Material_Light_NoActionBar_Overscan:I

.field public static Oneplus_Theme_Material_Light_NoActionBar_TranslucentDecor:I

.field public static Oneplus_Theme_Material_Light_OrangeTypeA:I

.field public static Oneplus_Theme_Material_Light_OrangeTypeB:I

.field public static Oneplus_Theme_Material_Light_Panel:I

.field public static Oneplus_Theme_Material_Light_PinkTypeA:I

.field public static Oneplus_Theme_Material_Light_PinkTypeB:I

.field public static Oneplus_Theme_Material_Light_PurpleTypeA:I

.field public static Oneplus_Theme_Material_Light_PurpleTypeB:I

.field public static Oneplus_Theme_Material_Light_RedTypeA:I

.field public static Oneplus_Theme_Material_Light_RedTypeB:I

.field public static Oneplus_Theme_Material_Light_SearchBar:I

.field public static Oneplus_Theme_Material_Light_TealTypeA:I

.field public static Oneplus_Theme_Material_Light_TealTypeB:I

.field public static Oneplus_Theme_Material_Light_TypeB:I

.field public static Oneplus_Theme_Material_Light_Voice:I

.field public static Oneplus_Theme_Material_Light_Wallpaper:I

.field public static Oneplus_Theme_Material_Light_Wallpaper_NoTitleBar:I

.field public static Oneplus_Theme_Material_Light_YellowTypeA:I

.field public static Oneplus_Theme_Material_Light_YellowTypeB:I

.field public static Oneplus_Theme_Material_LimeTypeA:I

.field public static Oneplus_Theme_Material_LimeTypeB:I

.field public static Oneplus_Theme_Material_NoActionBar:I

.field public static Oneplus_Theme_Material_NoActionBar_Fullscreen:I

.field public static Oneplus_Theme_Material_NoActionBar_Overscan:I

.field public static Oneplus_Theme_Material_NoActionBar_TranslucentDecor:I

.field public static Oneplus_Theme_Material_OrangeTypeA:I

.field public static Oneplus_Theme_Material_OrangeTypeB:I

.field public static Oneplus_Theme_Material_Panel:I

.field public static Oneplus_Theme_Material_PinkTypeA:I

.field public static Oneplus_Theme_Material_PinkTypeB:I

.field public static Oneplus_Theme_Material_PurpleTypeA:I

.field public static Oneplus_Theme_Material_PurpleTypeB:I

.field public static Oneplus_Theme_Material_RedTypeA:I

.field public static Oneplus_Theme_Material_RedTypeB:I

.field public static Oneplus_Theme_Material_SearchBar:I

.field public static Oneplus_Theme_Material_Settings:I

.field public static Oneplus_Theme_Material_Settings_BaseDialog:I

.field public static Oneplus_Theme_Material_Settings_BaseDialog_FromDark:I

.field public static Oneplus_Theme_Material_Settings_CompactMenu:I

.field public static Oneplus_Theme_Material_Settings_CompactMenu_FromDark:I

.field public static Oneplus_Theme_Material_Settings_Dialog:I

.field public static Oneplus_Theme_Material_Settings_DialogWhenLarge:I

.field public static Oneplus_Theme_Material_Settings_DialogWhenLarge_FromDark:I

.field public static Oneplus_Theme_Material_Settings_DialogWhenLarge_NoActionBar:I

.field public static Oneplus_Theme_Material_Settings_DialogWhenLarge_NoActionBar_FromDark:I

.field public static Oneplus_Theme_Material_Settings_Dialog_Alert:I

.field public static Oneplus_Theme_Material_Settings_Dialog_Alert_FromDark:I

.field public static Oneplus_Theme_Material_Settings_Dialog_BaseAlert:I

.field public static Oneplus_Theme_Material_Settings_Dialog_BaseAlert_FromDark:I

.field public static Oneplus_Theme_Material_Settings_Dialog_FromDark:I

.field public static Oneplus_Theme_Material_Settings_Dialog_Presentation:I

.field public static Oneplus_Theme_Material_Settings_Dialog_Presentation_FromDark:I

.field public static Oneplus_Theme_Material_Settings_NoActionBar:I

.field public static Oneplus_Theme_Material_Settings_NoActionBar_FromDark:I

.field public static Oneplus_Theme_Material_Settings_SearchBar:I

.field public static Oneplus_Theme_Material_Settings_SearchBar_FromDark:I

.field public static Oneplus_Theme_Material_TealTypeA:I

.field public static Oneplus_Theme_Material_TealTypeB:I

.field public static Oneplus_Theme_Material_TypeB:I

.field public static Oneplus_Theme_Material_Voice:I

.field public static Oneplus_Theme_Material_VoiceInteractionSession:I

.field public static Oneplus_Theme_Material_VoiceInteractionSession_FromDark:I

.field public static Oneplus_Theme_Material_Wallpaper:I

.field public static Oneplus_Theme_Material_Wallpaper_NoTitleBar:I

.field public static Oneplus_Theme_Material_YellowTypeA:I

.field public static Oneplus_Theme_Material_YellowTypeB:I

.field public static Oneplus_Theme_Resolver:I

.field public static Oneplus_Theme_Resolver_Light:I

.field public static Oneplus_Widget_Design_OPTabLayout:I

.field public static Oneplus_Widget_Material_ActionBar:I

.field public static Oneplus_Widget_Material_ActionBar_Solid:I

.field public static Oneplus_Widget_Material_ActionBar_TabBar:I

.field public static Oneplus_Widget_Material_ActionBar_TabText:I

.field public static Oneplus_Widget_Material_ActionBar_TabView:I

.field public static Oneplus_Widget_Material_ActionButton:I

.field public static Oneplus_Widget_Material_ActionButton_CloseMode:I

.field public static Oneplus_Widget_Material_ActionButton_Overflow:I

.field public static Oneplus_Widget_Material_ActionMode:I

.field public static Oneplus_Widget_Material_ActionMode_Light:I

.field public static Oneplus_Widget_Material_CalendarView:I

.field public static Oneplus_Widget_Material_CompoundButton_CheckBox:I

.field public static Oneplus_Widget_Material_CompoundButton_CheckBox_Circle:I

.field public static Oneplus_Widget_Material_CompoundButton_CheckBox_Circle_Special:I

.field public static Oneplus_Widget_Material_CompoundButton_CheckBox_Special:I

.field public static Oneplus_Widget_Material_CompoundButton_RadioButton:I

.field public static Oneplus_Widget_Material_CompoundButton_Switch:I

.field public static Oneplus_Widget_Material_DatePicker:I

.field public static Oneplus_Widget_Material_DropDownItem_Spinner:I

.field public static Oneplus_Widget_Material_EditText:I

.field public static Oneplus_Widget_Material_ImageCompoundButton_CheckBox:I

.field public static Oneplus_Widget_Material_Item_Spinner:I

.field public static Oneplus_Widget_Material_Light_ActionBar:I

.field public static Oneplus_Widget_Material_Light_ActionBar_Solid:I

.field public static Oneplus_Widget_Material_Light_ActionBar_TabBar:I

.field public static Oneplus_Widget_Material_Light_ActionBar_TabText:I

.field public static Oneplus_Widget_Material_Light_ActionBar_TabView:I

.field public static Oneplus_Widget_Material_Light_ActionButton_CloseMode:I

.field public static Oneplus_Widget_Material_Light_CalendarView:I

.field public static Oneplus_Widget_Material_Light_CompoundButton_CheckBox:I

.field public static Oneplus_Widget_Material_Light_CompoundButton_CheckBox_Circle:I

.field public static Oneplus_Widget_Material_Light_CompoundButton_CheckBox_Circle_Special:I

.field public static Oneplus_Widget_Material_Light_CompoundButton_CheckBox_Special:I

.field public static Oneplus_Widget_Material_Light_CompoundButton_RadioButton:I

.field public static Oneplus_Widget_Material_Light_CompoundButton_Switch:I

.field public static Oneplus_Widget_Material_Light_DatePicker:I

.field public static Oneplus_Widget_Material_Light_EditText:I

.field public static Oneplus_Widget_Material_Light_ImageCompoundButton_CheckBox:I

.field public static Oneplus_Widget_Material_Light_ListView:I

.field public static Oneplus_Widget_Material_Light_NumberPicker:I

.field public static Oneplus_Widget_Material_Light_PopupMenu_Overflow:I

.field public static Oneplus_Widget_Material_Light_ProgressBar:I

.field public static Oneplus_Widget_Material_Light_ProgressBar_Horizontal:I

.field public static Oneplus_Widget_Material_Light_ProgressBar_Large:I

.field public static Oneplus_Widget_Material_Light_ProgressBar_Small:I

.field public static Oneplus_Widget_Material_Light_SeekBar:I

.field public static Oneplus_Widget_Material_Light_TimePicker:I

.field public static Oneplus_Widget_Material_ListView:I

.field public static Oneplus_Widget_Material_NewSearchView_ActionBar:I

.field public static Oneplus_Widget_Material_NumberPicker:I

.field public static Oneplus_Widget_Material_PopupMenu:I

.field public static Oneplus_Widget_Material_PopupMenu_Overflow:I

.field public static Oneplus_Widget_Material_ProgressBar:I

.field public static Oneplus_Widget_Material_ProgressBar_Horizontal:I

.field public static Oneplus_Widget_Material_ProgressBar_Large:I

.field public static Oneplus_Widget_Material_ProgressBar_Small:I

.field public static Oneplus_Widget_Material_ProgressDialog_ProgressBar:I

.field public static Oneplus_Widget_Material_ProgressDialog_ProgressBar_Horizontal:I

.field public static Oneplus_Widget_Material_SearchView_ActionBar:I

.field public static Oneplus_Widget_Material_SeekBar:I

.field public static Oneplus_Widget_Material_TimePicker:I

.field public static Oneplus_Widget_Material_Toolbar:I

.field public static Oneplus_Widget_Material_Toolbar_Button_Navigation:I

.field public static Oneplus_bottom_fullscreen:I

.field public static Oneplus_popup_bottom_animation:I

.field public static Platform_AppCompat:I

.field public static Platform_AppCompat_Light:I

.field public static Platform_ThemeOverlay_AppCompat:I

.field public static Platform_ThemeOverlay_AppCompat_Dark:I

.field public static Platform_ThemeOverlay_AppCompat_Light:I

.field public static Platform_V21_AppCompat:I

.field public static Platform_V21_AppCompat_Light:I

.field public static Platform_V25_AppCompat:I

.field public static Platform_V25_AppCompat_Light:I

.field public static Platform_Widget_AppCompat_Spinner:I

.field public static RtlOverlay_DialogWindowTitle_AppCompat:I

.field public static RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I

.field public static RtlOverlay_Widget_AppCompat_DialogTitle_Icon:I

.field public static RtlOverlay_Widget_AppCompat_PopupMenuItem:I

.field public static RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I

.field public static RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut:I

.field public static RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow:I

.field public static RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I

.field public static RtlOverlay_Widget_AppCompat_PopupMenuItem_Title:I

.field public static RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I

.field public static RtlOverlay_Widget_AppCompat_Search_DropDown:I

.field public static RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I

.field public static RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I

.field public static RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I

.field public static RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I

.field public static RtlUnderlay_Widget_AppCompat_ActionButton:I

.field public static RtlUnderlay_Widget_AppCompat_ActionButton_Overflow:I

.field public static TextAppearance_AppCompat:I

.field public static TextAppearance_AppCompat_Body1:I

.field public static TextAppearance_AppCompat_Body2:I

.field public static TextAppearance_AppCompat_Button:I

.field public static TextAppearance_AppCompat_Caption:I

.field public static TextAppearance_AppCompat_Display1:I

.field public static TextAppearance_AppCompat_Display2:I

.field public static TextAppearance_AppCompat_Display3:I

.field public static TextAppearance_AppCompat_Display4:I

.field public static TextAppearance_AppCompat_Headline:I

.field public static TextAppearance_AppCompat_Inverse:I

.field public static TextAppearance_AppCompat_Large:I

.field public static TextAppearance_AppCompat_Large_Inverse:I

.field public static TextAppearance_AppCompat_Light_SearchResult_Subtitle:I

.field public static TextAppearance_AppCompat_Light_SearchResult_Title:I

.field public static TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I

.field public static TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I

.field public static TextAppearance_AppCompat_Medium:I

.field public static TextAppearance_AppCompat_Medium_Inverse:I

.field public static TextAppearance_AppCompat_Menu:I

.field public static TextAppearance_AppCompat_SearchResult_Subtitle:I

.field public static TextAppearance_AppCompat_SearchResult_Title:I

.field public static TextAppearance_AppCompat_Small:I

.field public static TextAppearance_AppCompat_Small_Inverse:I

.field public static TextAppearance_AppCompat_Subhead:I

.field public static TextAppearance_AppCompat_Subhead_Inverse:I

.field public static TextAppearance_AppCompat_Title:I

.field public static TextAppearance_AppCompat_Title_Inverse:I

.field public static TextAppearance_AppCompat_Tooltip:I

.field public static TextAppearance_AppCompat_Widget_ActionBar_Menu:I

.field public static TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I

.field public static TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I

.field public static TextAppearance_AppCompat_Widget_ActionBar_Title:I

.field public static TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I

.field public static TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I

.field public static TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I

.field public static TextAppearance_AppCompat_Widget_ActionMode_Title:I

.field public static TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I

.field public static TextAppearance_AppCompat_Widget_Button:I

.field public static TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I

.field public static TextAppearance_AppCompat_Widget_Button_Colored:I

.field public static TextAppearance_AppCompat_Widget_Button_Inverse:I

.field public static TextAppearance_AppCompat_Widget_DropDownItem:I

.field public static TextAppearance_AppCompat_Widget_PopupMenu_Header:I

.field public static TextAppearance_AppCompat_Widget_PopupMenu_Large:I

.field public static TextAppearance_AppCompat_Widget_PopupMenu_Small:I

.field public static TextAppearance_AppCompat_Widget_Switch:I

.field public static TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I

.field public static TextAppearance_Compat_Notification:I

.field public static TextAppearance_Compat_Notification_Info:I

.field public static TextAppearance_Compat_Notification_Info_Media:I

.field public static TextAppearance_Compat_Notification_Line2:I

.field public static TextAppearance_Compat_Notification_Line2_Media:I

.field public static TextAppearance_Compat_Notification_Media:I

.field public static TextAppearance_Compat_Notification_Time:I

.field public static TextAppearance_Compat_Notification_Time_Media:I

.field public static TextAppearance_Compat_Notification_Title:I

.field public static TextAppearance_Compat_Notification_Title_Media:I

.field public static TextAppearance_Design_Counter:I

.field public static TextAppearance_Design_Counter_Overflow:I

.field public static TextAppearance_Design_Error:I

.field public static TextAppearance_Design_Hint:I

.field public static TextAppearance_Design_SnackBar_Action:I

.field public static TextAppearance_Design_SnackBar_Text:I

.field public static TextAppearance_DialogWindowTitle:I

.field public static TextAppearance_Material:I

.field public static TextAppearance_Material_Body2:I

.field public static TextAppearance_Material_Button:I

.field public static TextAppearance_Material_Caption:I

.field public static TextAppearance_Material_DialogWindowTitle:I

.field public static TextAppearance_Material_Title:I

.field public static TextAppearance_Material_Widget_Calendar_Day:I

.field public static TextAppearance_Material_Widget_Calendar_DayOfWeek:I

.field public static TextAppearance_Material_Widget_Calendar_Month:I

.field public static TextAppearance_Material_Widget_Switch:I

.field public static TextAppearance_Toast:I

.field public static TextAppearance_Widget_ActionBar_Title:I

.field public static TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I

.field public static TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I

.field public static TextAppearance_Widget_AppCompat_Toolbar_Title:I

.field public static ThemeOverlay_AppCompat:I

.field public static ThemeOverlay_AppCompat_ActionBar:I

.field public static ThemeOverlay_AppCompat_Dark:I

.field public static ThemeOverlay_AppCompat_Dark_ActionBar:I

.field public static ThemeOverlay_AppCompat_Dialog:I

.field public static ThemeOverlay_AppCompat_Dialog_Alert:I

.field public static ThemeOverlay_AppCompat_Light:I

.field public static Theme_AppCompat:I

.field public static Theme_AppCompat_CompactMenu:I

.field public static Theme_AppCompat_DayNight:I

.field public static Theme_AppCompat_DayNight_DarkActionBar:I

.field public static Theme_AppCompat_DayNight_Dialog:I

.field public static Theme_AppCompat_DayNight_DialogWhenLarge:I

.field public static Theme_AppCompat_DayNight_Dialog_Alert:I

.field public static Theme_AppCompat_DayNight_Dialog_MinWidth:I

.field public static Theme_AppCompat_DayNight_NoActionBar:I

.field public static Theme_AppCompat_Dialog:I

.field public static Theme_AppCompat_DialogWhenLarge:I

.field public static Theme_AppCompat_Dialog_Alert:I

.field public static Theme_AppCompat_Dialog_MinWidth:I

.field public static Theme_AppCompat_Light:I

.field public static Theme_AppCompat_Light_DarkActionBar:I

.field public static Theme_AppCompat_Light_Dialog:I

.field public static Theme_AppCompat_Light_DialogWhenLarge:I

.field public static Theme_AppCompat_Light_Dialog_Alert:I

.field public static Theme_AppCompat_Light_Dialog_MinWidth:I

.field public static Theme_AppCompat_Light_NoActionBar:I

.field public static Theme_AppCompat_NoActionBar:I

.field public static Widget_AppCompat_ActionBar:I

.field public static Widget_AppCompat_ActionBar_Solid:I

.field public static Widget_AppCompat_ActionBar_TabBar:I

.field public static Widget_AppCompat_ActionBar_TabText:I

.field public static Widget_AppCompat_ActionBar_TabView:I

.field public static Widget_AppCompat_ActionButton:I

.field public static Widget_AppCompat_ActionButton_CloseMode:I

.field public static Widget_AppCompat_ActionButton_Overflow:I

.field public static Widget_AppCompat_ActionMode:I

.field public static Widget_AppCompat_ActivityChooserView:I

.field public static Widget_AppCompat_AutoCompleteTextView:I

.field public static Widget_AppCompat_Button:I

.field public static Widget_AppCompat_ButtonBar:I

.field public static Widget_AppCompat_ButtonBar_AlertDialog:I

.field public static Widget_AppCompat_Button_Borderless:I

.field public static Widget_AppCompat_Button_Borderless_Colored:I

.field public static Widget_AppCompat_Button_ButtonBar_AlertDialog:I

.field public static Widget_AppCompat_Button_Colored:I

.field public static Widget_AppCompat_Button_Small:I

.field public static Widget_AppCompat_CompoundButton_CheckBox:I

.field public static Widget_AppCompat_CompoundButton_RadioButton:I

.field public static Widget_AppCompat_CompoundButton_Switch:I

.field public static Widget_AppCompat_DrawerArrowToggle:I

.field public static Widget_AppCompat_DropDownItem_Spinner:I

.field public static Widget_AppCompat_EditText:I

.field public static Widget_AppCompat_ImageButton:I

.field public static Widget_AppCompat_Light_ActionBar:I

.field public static Widget_AppCompat_Light_ActionBar_Solid:I

.field public static Widget_AppCompat_Light_ActionBar_Solid_Inverse:I

.field public static Widget_AppCompat_Light_ActionBar_TabBar:I

.field public static Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I

.field public static Widget_AppCompat_Light_ActionBar_TabText:I

.field public static Widget_AppCompat_Light_ActionBar_TabText_Inverse:I

.field public static Widget_AppCompat_Light_ActionBar_TabView:I

.field public static Widget_AppCompat_Light_ActionBar_TabView_Inverse:I

.field public static Widget_AppCompat_Light_ActionButton:I

.field public static Widget_AppCompat_Light_ActionButton_CloseMode:I

.field public static Widget_AppCompat_Light_ActionButton_Overflow:I

.field public static Widget_AppCompat_Light_ActionMode_Inverse:I

.field public static Widget_AppCompat_Light_ActivityChooserView:I

.field public static Widget_AppCompat_Light_AutoCompleteTextView:I

.field public static Widget_AppCompat_Light_DropDownItem_Spinner:I

.field public static Widget_AppCompat_Light_ListPopupWindow:I

.field public static Widget_AppCompat_Light_ListView_DropDown:I

.field public static Widget_AppCompat_Light_PopupMenu:I

.field public static Widget_AppCompat_Light_PopupMenu_Overflow:I

.field public static Widget_AppCompat_Light_SearchView:I

.field public static Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I

.field public static Widget_AppCompat_ListMenuView:I

.field public static Widget_AppCompat_ListPopupWindow:I

.field public static Widget_AppCompat_ListView:I

.field public static Widget_AppCompat_ListView_DropDown:I

.field public static Widget_AppCompat_ListView_Menu:I

.field public static Widget_AppCompat_PopupMenu:I

.field public static Widget_AppCompat_PopupMenu_Overflow:I

.field public static Widget_AppCompat_PopupWindow:I

.field public static Widget_AppCompat_ProgressBar:I

.field public static Widget_AppCompat_ProgressBar_Horizontal:I

.field public static Widget_AppCompat_RatingBar:I

.field public static Widget_AppCompat_RatingBar_Indicator:I

.field public static Widget_AppCompat_RatingBar_Small:I

.field public static Widget_AppCompat_SearchView:I

.field public static Widget_AppCompat_SearchView_ActionBar:I

.field public static Widget_AppCompat_SeekBar:I

.field public static Widget_AppCompat_SeekBar_Discrete:I

.field public static Widget_AppCompat_Spinner:I

.field public static Widget_AppCompat_Spinner_DropDown:I

.field public static Widget_AppCompat_Spinner_DropDown_ActionBar:I

.field public static Widget_AppCompat_Spinner_Underlined:I

.field public static Widget_AppCompat_TextView_SpinnerItem:I

.field public static Widget_AppCompat_Toolbar:I

.field public static Widget_AppCompat_Toolbar_Button_Navigation:I

.field public static Widget_Compat_NotificationActionContainer:I

.field public static Widget_Compat_NotificationActionText:I

.field public static Widget_Design_AppBarLayout:I

.field public static Widget_Design_BottomNavigationView:I

.field public static Widget_Design_CollapsingToolbar:I

.field public static Widget_Design_CoordinatorLayout:I

.field public static Widget_Design_OPTextInputLayout:I

.field public static Widget_Material:I

.field public static Widget_MaterialComponents_BottomNavigationView:I

.field public static Widget_MaterialComponents_BottomNavigationView_Colored:I

.field public static Widget_Material_Button:I

.field public static Widget_Material_Button_Borderless:I

.field public static Widget_Material_Button_Borderless_Colored:I

.field public static Widget_Material_Button_ButtonBar_AlertDialog:I

.field public static Widget_Material_CompoundButton:I

.field public static Widget_Material_CompoundButton_Switch:I

.field public static Widget_Material_Light_NumberPicker:I

.field public static Widget_Material_NumberPicker:I

.field public static Widget_Support_CoordinatorLayout:I

.field public static listitem_primary_text_font:I

.field public static listitem_secondary_text_font:I

.field public static listitem_stamp_font:I

.field public static oneplus_contorl_text_style_base:I

.field public static oneplus_contorl_text_style_body1:I

.field public static oneplus_contorl_text_style_body2:I

.field public static oneplus_contorl_text_style_button:I

.field public static oneplus_contorl_text_style_descriptions:I

.field public static oneplus_contorl_text_style_display1:I

.field public static oneplus_contorl_text_style_display2:I

.field public static oneplus_contorl_text_style_display3:I

.field public static oneplus_contorl_text_style_display4:I

.field public static oneplus_contorl_text_style_headline:I

.field public static oneplus_contorl_text_style_highlight:I

.field public static oneplus_contorl_text_style_menu:I

.field public static oneplus_contorl_text_style_subheading:I

.field public static oneplus_contorl_text_style_subtitle:I

.field public static oneplus_contorl_text_style_title:I

.field public static oneplus_contorl_text_style_title_extended:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7733
    const/4 v0, 0x0

    sput v0, Landroid/support/asynclayoutinflater/R$style;->AlertDialog_AppCompat:I

    .line 7734
    sput v0, Landroid/support/asynclayoutinflater/R$style;->AlertDialog_AppCompat_Light:I

    .line 7735
    sput v0, Landroid/support/asynclayoutinflater/R$style;->AlertDialogTitleStyleDark:I

    .line 7743
    sput v0, Landroid/support/asynclayoutinflater/R$style;->AlertDialogTitleStyleLight:I

    .line 7744
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Animation_AppCompat_Dialog:I

    .line 7745
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Animation_AppCompat_DropDownUp:I

    .line 7746
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Animation_AppCompat_Tooltip:I

    .line 7747
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Animation_DropDownUp:I

    .line 7748
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Animation_Material:I

    .line 7749
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Animation_Material_Dialog:I

    .line 7750
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Animation_VoiceActivity:I

    .line 7751
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_AlertDialog_AppCompat:I

    .line 7752
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_AlertDialog_AppCompat_Light:I

    .line 7753
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Animation_AppCompat_Dialog:I

    .line 7754
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Animation_AppCompat_DropDownUp:I

    .line 7755
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Animation_AppCompat_Tooltip:I

    .line 7756
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_DialogWindowTitle_AppCompat:I

    .line 7757
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_DialogWindowTitleBackground_AppCompat:I

    .line 7758
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat:I

    .line 7759
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Body1:I

    .line 7760
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Body2:I

    .line 7761
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Button:I

    .line 7762
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Caption:I

    .line 7763
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Display1:I

    .line 7764
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Display2:I

    .line 7765
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Display3:I

    .line 7766
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Display4:I

    .line 7767
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Headline:I

    .line 7768
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Inverse:I

    .line 7769
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Large:I

    .line 7770
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Large_Inverse:I

    .line 7771
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I

    .line 7772
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I

    .line 7773
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Medium:I

    .line 7774
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Medium_Inverse:I

    .line 7775
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Menu:I

    .line 7776
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_SearchResult:I

    .line 7777
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_SearchResult_Subtitle:I

    .line 7778
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_SearchResult_Title:I

    .line 7779
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Small:I

    .line 7780
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Small_Inverse:I

    .line 7781
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Subhead:I

    .line 7782
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Subhead_Inverse:I

    .line 7783
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Title:I

    .line 7784
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Title_Inverse:I

    .line 7785
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Tooltip:I

    .line 7786
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I

    .line 7787
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I

    .line 7788
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I

    .line 7789
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I

    .line 7790
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I

    .line 7791
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I

    .line 7792
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I

    .line 7793
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_Button:I

    .line 7794
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I

    .line 7795
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_Button_Colored:I

    .line 7796
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_Button_Inverse:I

    .line 7797
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_DropDownItem:I

    .line 7798
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_PopupMenu_Header:I

    .line 7799
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I

    .line 7800
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I

    .line 7801
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_Switch:I

    .line 7802
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I

    .line 7803
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_Widget_ActionBar_Title:I

    .line 7804
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I

    .line 7805
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I

    .line 7806
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I

    .line 7807
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat:I

    .line 7808
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_CompactMenu:I

    .line 7809
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Dialog:I

    .line 7810
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Dialog_Alert:I

    .line 7811
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Dialog_FixedSize:I

    .line 7812
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Dialog_MinWidth:I

    .line 7813
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_DialogWhenLarge:I

    .line 7814
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Light:I

    .line 7815
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Light_DarkActionBar:I

    .line 7816
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Light_Dialog:I

    .line 7817
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Light_Dialog_Alert:I

    .line 7818
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Light_Dialog_FixedSize:I

    .line 7819
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Light_Dialog_MinWidth:I

    .line 7820
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Theme_AppCompat_Light_DialogWhenLarge:I

    .line 7821
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_ThemeOverlay_AppCompat:I

    .line 7822
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_ThemeOverlay_AppCompat_ActionBar:I

    .line 7823
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_ThemeOverlay_AppCompat_Dark:I

    .line 7824
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_ThemeOverlay_AppCompat_Dark_ActionBar:I

    .line 7825
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_ThemeOverlay_AppCompat_Dialog:I

    .line 7826
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_ThemeOverlay_AppCompat_Dialog_Alert:I

    .line 7827
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_ThemeOverlay_AppCompat_Light:I

    .line 7828
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V21_Theme_AppCompat:I

    .line 7829
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V21_Theme_AppCompat_Dialog:I

    .line 7830
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V21_Theme_AppCompat_Light:I

    .line 7831
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V21_Theme_AppCompat_Light_Dialog:I

    .line 7832
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V21_ThemeOverlay_AppCompat_Dialog:I

    .line 7833
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V22_Theme_AppCompat:I

    .line 7834
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V22_Theme_AppCompat_Light:I

    .line 7835
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V23_Theme_AppCompat:I

    .line 7836
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V23_Theme_AppCompat_Light:I

    .line 7837
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V26_Theme_AppCompat:I

    .line 7838
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V26_Theme_AppCompat_Light:I

    .line 7839
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V26_Widget_AppCompat_Toolbar:I

    .line 7840
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V28_Theme_AppCompat:I

    .line 7841
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V28_Theme_AppCompat_Light:I

    .line 7842
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_Theme_AppCompat:I

    .line 7843
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_Theme_AppCompat_Dialog:I

    .line 7844
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_Theme_AppCompat_Light:I

    .line 7845
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_Theme_AppCompat_Light_Dialog:I

    .line 7846
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_ThemeOverlay_AppCompat_Dialog:I

    .line 7847
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_Widget_AppCompat_AutoCompleteTextView:I

    .line 7848
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_Widget_AppCompat_EditText:I

    .line 7849
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_V7_Widget_AppCompat_Toolbar:I

    .line 7850
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionBar:I

    .line 7851
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionBar_Solid:I

    .line 7852
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionBar_TabBar:I

    .line 7853
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionBar_TabText:I

    .line 7854
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionBar_TabView:I

    .line 7855
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionButton:I

    .line 7856
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionButton_CloseMode:I

    .line 7857
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionButton_Overflow:I

    .line 7858
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActionMode:I

    .line 7859
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ActivityChooserView:I

    .line 7860
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_AutoCompleteTextView:I

    .line 7861
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Button:I

    .line 7862
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Button_Borderless:I

    .line 7863
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Button_Borderless_Colored:I

    .line 7864
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Button_ButtonBar_AlertDialog:I

    .line 7865
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Button_Colored:I

    .line 7866
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Button_Small:I

    .line 7867
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ButtonBar:I

    .line 7868
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ButtonBar_AlertDialog:I

    .line 7869
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_CompoundButton_CheckBox:I

    .line 7870
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_CompoundButton_RadioButton:I

    .line 7871
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_CompoundButton_Switch:I

    .line 7872
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_DrawerArrowToggle:I

    .line 7873
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_DrawerArrowToggle_Common:I

    .line 7874
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_DropDownItem_Spinner:I

    .line 7875
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_EditText:I

    .line 7876
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ImageButton:I

    .line 7877
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_ActionBar:I

    .line 7878
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_ActionBar_Solid:I

    .line 7879
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_ActionBar_TabBar:I

    .line 7880
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_ActionBar_TabText:I

    .line 7881
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I

    .line 7882
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_ActionBar_TabView:I

    .line 7883
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_PopupMenu:I

    .line 7884
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Light_PopupMenu_Overflow:I

    .line 7885
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ListMenuView:I

    .line 7886
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ListPopupWindow:I

    .line 7887
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ListView:I

    .line 7888
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ListView_DropDown:I

    .line 7889
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ListView_Menu:I

    .line 7890
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_PopupMenu:I

    .line 7891
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_PopupMenu_Overflow:I

    .line 7892
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_PopupWindow:I

    .line 7893
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ProgressBar:I

    .line 7894
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_ProgressBar_Horizontal:I

    .line 7895
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_RatingBar:I

    .line 7896
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_RatingBar_Indicator:I

    .line 7897
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_RatingBar_Small:I

    .line 7898
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_SearchView:I

    .line 7899
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_SearchView_ActionBar:I

    .line 7900
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_SeekBar:I

    .line 7901
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_SeekBar_Discrete:I

    .line 7902
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Spinner:I

    .line 7903
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Spinner_Underlined:I

    .line 7904
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_TextView_SpinnerItem:I

    .line 7905
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Toolbar:I

    .line 7906
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_AppCompat_Toolbar_Button_Navigation:I

    .line 7907
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Base_Widget_Design_AppBarLayout:I

    .line 7911
    sput v0, Landroid/support/asynclayoutinflater/R$style;->CardView:I

    .line 7912
    sput v0, Landroid/support/asynclayoutinflater/R$style;->CardView_Dark:I

    .line 7913
    sput v0, Landroid/support/asynclayoutinflater/R$style;->CardView_Light:I

    .line 7914
    sput v0, Landroid/support/asynclayoutinflater/R$style;->DialogWindowTitle:I

    .line 7915
    sput v0, Landroid/support/asynclayoutinflater/R$style;->DialogWindowTitle_Material:I

    .line 7916
    sput v0, Landroid/support/asynclayoutinflater/R$style;->DialogWindowTitle_Material_Light:I

    .line 7917
    sput v0, Landroid/support/asynclayoutinflater/R$style;->DialogWindowTitleBackground:I

    .line 7918
    sput v0, Landroid/support/asynclayoutinflater/R$style;->DialogWindowTitleBackground_Material:I

    .line 7919
    sput v0, Landroid/support/asynclayoutinflater/R$style;->DialogWindowTitleBackground_Material_Light:I

    .line 7920
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPAnimation_AppCompat_DropDownUp:I

    .line 7921
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPAnimation_AppCompat_Tooltip:I

    .line 7922
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPBase_Animation_AppCompat_Tooltip:I

    .line 7923
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPBase_TextAppearance_AppCompat:I

    .line 7927
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPBase_Widget_AppCompat_DrawerArrowToggle:I

    .line 7931
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPBase_Widget_AppCompat_DrawerArrowToggle_Common:I

    .line 7932
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference:I

    .line 7933
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material:I

    .line 7934
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_Category:I

    .line 7935
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_CheckBoxPreference:I

    .line 7936
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_DialogPreference:I

    .line 7937
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_DialogPreference_EditTextPreference:I

    .line 7938
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_DialogPreference_SeekBarPreference:I

    .line 7939
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_DialogPreference_YesNoPreference:I

    .line 7940
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_Information:I

    .line 7941
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_PreferenceScreen:I

    .line 7942
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_RingtonePreference:I

    .line 7943
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_SeekBarPreference:I

    .line 7944
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreference_Material_SwitchPreference:I

    .line 7945
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceActivity:I

    .line 7946
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceActivity_Material:I

    .line 7947
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceFragment:I

    .line 7948
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceFragment_Material:I

    .line 7949
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceFragmentList:I

    .line 7950
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceFragmentList_Material:I

    .line 7951
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceHeaderList:I

    .line 7952
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceHeaderList_Material:I

    .line 7953
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceHeaderPanel:I

    .line 7954
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferenceHeaderPanel_Material:I

    .line 7955
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferencePanel:I

    .line 7956
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferencePanel_Dialog:I

    .line 7960
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferencePanel_Material:I

    .line 7965
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPPreferencePanel_Material_Dialog:I

    .line 7966
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPRtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I

    .line 7967
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPRtlOverlay_Widget_AppCompat_PopupMenuItem:I

    .line 7968
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPRtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I

    .line 7969
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPRtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I

    .line 7970
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_AppCompat_Tooltip:I

    .line 7971
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Design_CollapsingToolbar_Expanded:I

    .line 7972
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Large_Inverse_NumberPickerInputText:I

    .line 7973
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_DatePicker_DateLabel:I

    .line 7974
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_DatePicker_List_YearLabel:I

    .line 7975
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_DatePicker_List_YearLabel_Activated:I

    .line 7976
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_DatePicker_YearLabel:I

    .line 7977
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_NumberPicker:I

    .line 7978
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_AmPmLabel:I

    .line 7979
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_InputField:I

    .line 7980
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_InputFieldPrompt:I

    .line 7981
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_InputFieldUnActive:I

    .line 7982
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_InputHeader:I

    .line 7983
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_PromptLabel:I

    .line 7984
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_TimeLabel:I

    .line 7985
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_TimeLabelUnActivated:I

    .line 7986
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTextAppearance_Material_TimePicker_TimeSeparator:I

    .line 7987
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPTheme_AppCompat_CompactMenu:I

    .line 7988
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPWidget_AppCompat_DrawerArrowToggle:I

    .line 7989
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPWidget_AppCompat_ListView_Menu:I

    .line 7990
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPWidget_Material_NumberPicker:I

    .line 7991
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPWidget_Material_Toolbar:I

    .line 7992
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPWidget_Material_Toolbar_Button_Navigation:I

    .line 7993
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPWidget_NumberPicker:I

    .line 7997
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OPWidget_Toolbar:I

    .line 7998
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material:I

    .line 7999
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material_CheckBoxPreference:I

    .line 8000
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material_DialogPreference:I

    .line 8001
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material_DialogPreference_EditTextPreference:I

    .line 8002
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material_PreferenceScreen:I

    .line 8003
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material_RingtonePreference:I

    .line 8004
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material_SeekBarPreference:I

    .line 8005
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Preference_Material_SwitchPreference:I

    .line 8009
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Widget_Material_Button:I

    .line 8010
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Widget_Material_Button_Borderless:I

    .line 8011
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Widget_Material_Button_Borderless_Mini:I

    .line 8012
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_DeviceDefault_Widget_Material_Button_Mini:I

    .line 8016
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material:I

    .line 8020
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_CheckBoxPreference:I

    .line 8024
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_DialogPreference:I

    .line 8028
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_DialogPreference_EditTextPreference:I

    .line 8029
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light:I

    .line 8030
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light_CheckBoxPreference:I

    .line 8031
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light_DialogPreference:I

    .line 8032
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light_DialogPreference_EditTextPreference:I

    .line 8033
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light_PreferenceScreen:I

    .line 8034
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light_RingtonePreference:I

    .line 8035
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light_SeekBarPreference:I

    .line 8036
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_Light_SwitchPreference:I

    .line 8040
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_PreferenceScreen:I

    .line 8041
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_RingtonePreference:I

    .line 8042
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_SeekBarPreference:I

    .line 8046
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Preference_Material_SwitchPreference:I

    .line 8047
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_TextAppearance_Material_Button:I

    .line 8048
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_TextAppearance_Material_Light_Button:I

    .line 8049
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_TextAppearance_Material_Raised_Button:I

    .line 8050
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_TextAppearance_Material_Raised_Light_Button:I

    .line 8054
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Theme_DeviceDefault_Dialog_Picker:I

    .line 8055
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Design_FloatingActionButton:I

    .line 8056
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Design_RectangleFloatingActionButton:I

    .line 8060
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Design_Snackbar:I

    .line 8061
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Design_SwitchFloatingActionButton:I

    .line 8062
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Button:I

    .line 8063
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Button_AlertDialog:I

    .line 8064
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Button_Borderless:I

    .line 8065
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Button_Borderless_Mini:I

    .line 8066
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Button_Mini:I

    .line 8067
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Light_Button:I

    .line 8068
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Light_Button_AlertDialog:I

    .line 8069
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Light_Button_Borderless:I

    .line 8070
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Light_Button_Borderless_Mini:I

    .line 8071
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlus_Widget_Material_Light_Button_Mini:I

    .line 8072
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlusAlertDialog:I

    .line 8073
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlusAlertDialog_Light:I

    .line 8074
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlusAlertProgressDialog:I

    .line 8075
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlusBottomNavigationViewStyle:I

    .line 8076
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlusToast:I

    .line 8077
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OnePlusToastLight:I

    .line 8078
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Animation_Material_Dialog:I

    .line 8079
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Base_Widget_Design_OPTabLayout:I

    .line 8080
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_CardView:I

    .line 8081
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_CardView_Dark:I

    .line 8082
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_CardView_Light:I

    .line 8086
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_OPToast:I

    .line 8087
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Preference_Material_Category:I

    .line 8088
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_PreferenceFragment_Material:I

    .line 8089
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_PreferenceFragmentList_Material:I

    .line 8090
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Theme_Material_Dialog:I

    .line 8091
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Theme_Material_Dialog_Alert:I

    .line 8092
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Theme_Material_Dialog_Picker:I

    .line 8093
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_ActionBar_Solid:I

    .line 8094
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox:I

    .line 8095
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox_Circle:I

    .line 8096
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox_Circle_Special:I

    .line 8097
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_CompoundButton_CheckBox_Special:I

    .line 8098
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_CompoundButton_RadioButton:I

    .line 8099
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_CompoundButton_Switch:I

    .line 8100
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_DatePicker:I

    .line 8101
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_EditText:I

    .line 8102
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_ImageCompoundButton_CheckBox:I

    .line 8103
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_ProgressBar:I

    .line 8104
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_ProgressBar_Horizontal:I

    .line 8105
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_ProgressBar_Large:I

    .line 8106
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_ProgressBar_Small:I

    .line 8107
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_SeekBar:I

    .line 8108
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_DeviceDefault_Widget_Material_TimePicker:I

    .line 8109
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Preference_Material_Category:I

    .line 8110
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Preference_Material_Light_Category:I

    .line 8111
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_PreferenceFragment_Material:I

    .line 8112
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_PreferenceFragment_Material_Light:I

    .line 8113
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_PreferenceFragmentList_Material:I

    .line 8114
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_PreferenceFragmentList_Material_Light:I

    .line 8115
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Design_Tab:I

    .line 8116
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Body1:I

    .line 8117
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Light_NumberPickerDialog_Minute:I

    .line 8118
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Light_Widget_Calendar_Day:I

    .line 8119
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Light_Widget_Calendar_DayOfWeek:I

    .line 8120
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Light_Widget_Calendar_Month:I

    .line 8121
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Light_Widget_TabWidget:I

    .line 8125
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Menu:I

    .line 8126
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Menu_Dark:I

    .line 8127
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Menu_Light:I

    .line 8128
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_NumberPickerDialog_Minute:I

    .line 8129
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Widget_ActionBar_Subtitle:I

    .line 8130
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Widget_ActionBar_Title:I

    .line 8131
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Widget_Calendar_Day:I

    .line 8132
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Widget_Calendar_DayOfWeek:I

    .line 8133
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Widget_Calendar_Month:I

    .line 8134
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Widget_PopupMenu_Header:I

    .line 8135
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Material_Widget_TabWidget:I

    .line 8136
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_TextAppearance_Toast:I

    .line 8137
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault:I

    .line 8138
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_BaseDialog:I

    .line 8139
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_CompactMenu:I

    .line 8140
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_DarkActionBar:I

    .line 8141
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog:I

    .line 8142
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_Alert:I

    .line 8143
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_BaseAlert:I

    .line 8144
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_FixedSize:I

    .line 8145
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_MinWidth:I

    .line 8146
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_NoActionBar:I

    .line 8147
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_NoActionBar_FixedSize:I

    .line 8148
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_NoActionBar_MinWidth:I

    .line 8149
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_NoFrame:I

    .line 8150
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Dialog_Presentation:I

    .line 8151
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_DialogWhenLarge:I

    .line 8152
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_DialogWhenLarge_DarkActionBar:I

    .line 8153
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_DialogWhenLarge_NoActionBar:I

    .line 8154
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_InputMethod:I

    .line 8155
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_LightStatusBar:I

    .line 8156
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_NoActionBar:I

    .line 8157
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_NoActionBar_Fullscreen:I

    .line 8158
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_NoActionBar_Overscan:I

    .line 8159
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_NoActionBar_TranslucentDecor:I

    .line 8160
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Panel:I

    .line 8161
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_SearchBar:I

    .line 8162
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings:I

    .line 8163
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_BaseDialog:I

    .line 8164
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_CompactMenu:I

    .line 8165
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_Dialog:I

    .line 8166
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_Dialog_Alert:I

    .line 8167
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_Dialog_BaseAlert:I

    .line 8168
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_Dialog_Presentation:I

    .line 8169
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_DialogWhenLarge:I

    .line 8170
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_DialogWhenLarge_NoActionBar:I

    .line 8171
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_NoActionBar:I

    .line 8172
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Settings_SearchBar:I

    .line 8173
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Voice:I

    .line 8174
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_VoiceInteractionSession:I

    .line 8175
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Wallpaper:I

    .line 8176
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_DeviceDefault_Wallpaper_NoTitleBar:I

    .line 8177
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material:I

    .line 8178
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_AmberTypeA:I

    .line 8179
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_AmberTypeB:I

    .line 8180
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_BaseDialog:I

    .line 8181
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_BlueGreyTypeA:I

    .line 8182
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_BlueGreyTypeB:I

    .line 8183
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_BlueTypeA:I

    .line 8184
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_BlueTypeB:I

    .line 8185
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_BrownTypeA:I

    .line 8186
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_BrownTypeB:I

    .line 8187
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_CompactMenu:I

    .line 8188
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_CyanTypeA:I

    .line 8189
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_CyanTypeB:I

    .line 8193
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DarkActionBar:I

    .line 8194
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DeepOrangeTypeA:I

    .line 8195
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DeepOrangeTypeB:I

    .line 8196
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DeepPurpleTypeA:I

    .line 8197
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DeepPurpleTypeB:I

    .line 8205
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog:I

    .line 8213
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_Alert:I

    .line 8214
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_BaseAlert:I

    .line 8218
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_FixedSize:I

    .line 8223
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_MinWidth:I

    .line 8227
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_NoActionBar:I

    .line 8231
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_NoActionBar_FixedSize:I

    .line 8236
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_NoActionBar_MinWidth:I

    .line 8242
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_NoFrame:I

    .line 8243
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_Picker:I

    .line 8247
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Dialog_Presentation:I

    .line 8253
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DialogWhenLarge:I

    .line 8257
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DialogWhenLarge_DarkActionBar:I

    .line 8263
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_DialogWhenLarge_NoActionBar:I

    .line 8264
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_GreenTypeA:I

    .line 8265
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_GreenTypeB:I

    .line 8266
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_GreyTypeA:I

    .line 8267
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_GreyTypeB:I

    .line 8268
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_IndigoTypeA:I

    .line 8269
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_IndigoTypeB:I

    .line 8276
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_InputMethod:I

    .line 8280
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_InputMethod_FromDark:I

    .line 8281
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light:I

    .line 8282
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_AmberTypeA:I

    .line 8283
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_AmberTypeB:I

    .line 8287
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_BaseDialog:I

    .line 8288
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_BlueGreyTypeA:I

    .line 8289
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_BlueGreyTypeB:I

    .line 8290
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_BlueTypeA:I

    .line 8291
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_BlueTypeB:I

    .line 8292
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_BrownTypeA:I

    .line 8293
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_BrownTypeB:I

    .line 8294
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_CompactMenu:I

    .line 8295
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_CyanTypeA:I

    .line 8296
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_CyanTypeB:I

    .line 8302
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar:I

    .line 8303
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_AmberTypeA:I

    .line 8304
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_AmberTypeB:I

    .line 8305
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_BlueGreyTypeA:I

    .line 8306
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_BlueGreyTypeB:I

    .line 8307
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_BlueTypeA:I

    .line 8308
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_BlueTypeB:I

    .line 8309
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_BrownTypeA:I

    .line 8310
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_BrownTypeB:I

    .line 8311
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_CyanTypeA:I

    .line 8312
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_CyanTypeB:I

    .line 8313
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_DeepOrangeTypeA:I

    .line 8314
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_DeepOrangeTypeB:I

    .line 8315
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_DeepPurpleTypeA:I

    .line 8316
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_DeepPurpleTypeB:I

    .line 8317
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_GreenTypeA:I

    .line 8318
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_GreenTypeB:I

    .line 8319
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_GreyTypeA:I

    .line 8320
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_GreyTypeB:I

    .line 8321
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_IndigoTypeA:I

    .line 8322
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_IndigoTypeB:I

    .line 8323
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_LightBlueTypeA:I

    .line 8324
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_LightBlueTypeB:I

    .line 8325
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_LightGreenTypeA:I

    .line 8326
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_LightGreenTypeB:I

    .line 8327
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_LimeTypeA:I

    .line 8328
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_LimeTypeB:I

    .line 8329
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_OrangeTypeA:I

    .line 8330
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_OrangeTypeB:I

    .line 8331
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_PinkTypeA:I

    .line 8332
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_PinkTypeB:I

    .line 8333
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_PurpleTypeA:I

    .line 8334
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_PurpleTypeB:I

    .line 8335
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_RedTypeA:I

    .line 8336
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_RedTypeB:I

    .line 8337
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_TealTypeA:I

    .line 8338
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_TealTypeB:I

    .line 8339
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_YellowTypeA:I

    .line 8340
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DarkActionBar_YellowTypeB:I

    .line 8341
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DeepOrangeTypeA:I

    .line 8342
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DeepOrangeTypeB:I

    .line 8343
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DeepPurpleTypeA:I

    .line 8344
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DeepPurpleTypeB:I

    .line 8352
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog:I

    .line 8360
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_Alert:I

    .line 8361
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_BaseAlert:I

    .line 8365
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_FixedSize:I

    .line 8370
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_MinWidth:I

    .line 8374
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_NoActionBar:I

    .line 8378
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_NoActionBar_FixedSize:I

    .line 8383
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_NoActionBar_MinWidth:I

    .line 8387
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_NoFrame:I

    .line 8388
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_Picker:I

    .line 8392
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Dialog_Presentation:I

    .line 8398
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DialogWhenLarge:I

    .line 8404
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DialogWhenLarge_DarkActionBar:I

    .line 8410
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_DialogWhenLarge_NoActionBar:I

    .line 8411
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_GreenTypeA:I

    .line 8412
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_GreenTypeB:I

    .line 8413
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_GreyTypeA:I

    .line 8414
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_GreyTypeB:I

    .line 8415
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_IndigoTypeA:I

    .line 8416
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_IndigoTypeB:I

    .line 8417
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_LightBlueTypeA:I

    .line 8418
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_LightBlueTypeB:I

    .line 8419
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_LightGreenTypeA:I

    .line 8420
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_LightGreenTypeB:I

    .line 8425
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_LightStatusBar:I

    .line 8426
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_LimeTypeA:I

    .line 8427
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_LimeTypeB:I

    .line 8431
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_NoActionBar:I

    .line 8437
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_NoActionBar_Fullscreen:I

    .line 8444
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_NoActionBar_Overscan:I

    .line 8450
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_NoActionBar_TranslucentDecor:I

    .line 8451
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_OrangeTypeA:I

    .line 8452
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_OrangeTypeB:I

    .line 8459
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Panel:I

    .line 8460
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_PinkTypeA:I

    .line 8461
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_PinkTypeB:I

    .line 8462
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_PurpleTypeA:I

    .line 8463
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_PurpleTypeB:I

    .line 8464
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_RedTypeA:I

    .line 8465
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_RedTypeB:I

    .line 8466
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_SearchBar:I

    .line 8467
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_TealTypeA:I

    .line 8468
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_TealTypeB:I

    .line 8469
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_TypeB:I

    .line 8475
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Voice:I

    .line 8476
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Wallpaper:I

    .line 8477
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_Wallpaper_NoTitleBar:I

    .line 8478
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_YellowTypeA:I

    .line 8479
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Light_YellowTypeB:I

    .line 8480
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_LightBlueTypeA:I

    .line 8481
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_LightBlueTypeB:I

    .line 8482
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_LightGreenTypeA:I

    .line 8483
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_LightGreenTypeB:I

    .line 8487
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_LightStatusBar:I

    .line 8488
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_LimeTypeA:I

    .line 8489
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_LimeTypeB:I

    .line 8493
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_NoActionBar:I

    .line 8499
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_NoActionBar_Fullscreen:I

    .line 8506
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_NoActionBar_Overscan:I

    .line 8512
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_NoActionBar_TranslucentDecor:I

    .line 8513
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_OrangeTypeA:I

    .line 8514
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_OrangeTypeB:I

    .line 8521
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Panel:I

    .line 8522
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_PinkTypeA:I

    .line 8523
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_PinkTypeB:I

    .line 8524
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_PurpleTypeA:I

    .line 8525
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_PurpleTypeB:I

    .line 8526
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_RedTypeA:I

    .line 8527
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_RedTypeB:I

    .line 8531
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_SearchBar:I

    .line 8535
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings:I

    .line 8536
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_BaseDialog:I

    .line 8540
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_BaseDialog_FromDark:I

    .line 8541
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_CompactMenu:I

    .line 8545
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_CompactMenu_FromDark:I

    .line 8546
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog:I

    .line 8547
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog_Alert:I

    .line 8551
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog_Alert_FromDark:I

    .line 8552
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog_BaseAlert:I

    .line 8556
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog_BaseAlert_FromDark:I

    .line 8560
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog_FromDark:I

    .line 8561
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog_Presentation:I

    .line 8565
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_Dialog_Presentation_FromDark:I

    .line 8566
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_DialogWhenLarge:I

    .line 8570
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_DialogWhenLarge_FromDark:I

    .line 8571
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_DialogWhenLarge_NoActionBar:I

    .line 8575
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_DialogWhenLarge_NoActionBar_FromDark:I

    .line 8579
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_NoActionBar:I

    .line 8583
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_NoActionBar_FromDark:I

    .line 8584
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_SearchBar:I

    .line 8588
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Settings_SearchBar_FromDark:I

    .line 8589
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_TealTypeA:I

    .line 8590
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_TealTypeB:I

    .line 8591
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_TypeB:I

    .line 8597
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Voice:I

    .line 8604
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_VoiceInteractionSession:I

    .line 8608
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_VoiceInteractionSession_FromDark:I

    .line 8613
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Wallpaper:I

    .line 8618
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_Wallpaper_NoTitleBar:I

    .line 8619
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_YellowTypeA:I

    .line 8620
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Material_YellowTypeB:I

    .line 8621
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Resolver:I

    .line 8622
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Theme_Resolver_Light:I

    .line 8623
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay:I

    .line 8624
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay_Material:I

    .line 8629
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay_Material_ActionBar:I

    .line 8634
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay_Material_Dark:I

    .line 8640
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay_Material_Dark_ActionBar:I

    .line 8644
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay_Material_Dialog:I

    .line 8645
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay_Material_Dialog_Alert:I

    .line 8650
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_ThemeOverlay_Material_Light:I

    .line 8654
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Design_OPTabLayout:I

    .line 8658
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionBar:I

    .line 8659
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionBar_Solid:I

    .line 8660
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionBar_TabBar:I

    .line 8661
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionBar_TabText:I

    .line 8662
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionBar_TabView:I

    .line 8663
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionButton:I

    .line 8664
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionButton_CloseMode:I

    .line 8665
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionButton_Overflow:I

    .line 8666
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionMode:I

    .line 8667
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ActionMode_Light:I

    .line 8668
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_CalendarView:I

    .line 8672
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_CompoundButton_CheckBox:I

    .line 8673
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_CompoundButton_CheckBox_Circle:I

    .line 8674
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_CompoundButton_CheckBox_Circle_Special:I

    .line 8675
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_CompoundButton_CheckBox_Special:I

    .line 8679
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_CompoundButton_RadioButton:I

    .line 8683
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_CompoundButton_Switch:I

    .line 8684
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_DatePicker:I

    .line 8685
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_DropDownItem_Spinner:I

    .line 8686
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_EditText:I

    .line 8687
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ImageCompoundButton_CheckBox:I

    .line 8688
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Item_Spinner:I

    .line 8692
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ActionBar:I

    .line 8693
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ActionBar_Solid:I

    .line 8694
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ActionBar_TabBar:I

    .line 8695
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ActionBar_TabText:I

    .line 8696
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ActionBar_TabView:I

    .line 8697
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ActionButton_CloseMode:I

    .line 8698
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_CalendarView:I

    .line 8699
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_CompoundButton_CheckBox:I

    .line 8700
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_CompoundButton_CheckBox_Circle:I

    .line 8701
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_CompoundButton_CheckBox_Circle_Special:I

    .line 8702
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_CompoundButton_CheckBox_Special:I

    .line 8703
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_CompoundButton_RadioButton:I

    .line 8704
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_CompoundButton_Switch:I

    .line 8705
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_DatePicker:I

    .line 8706
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_EditText:I

    .line 8707
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ImageCompoundButton_CheckBox:I

    .line 8708
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ListView:I

    .line 8709
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_NumberPicker:I

    .line 8710
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_PopupMenu_Overflow:I

    .line 8711
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ProgressBar:I

    .line 8712
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ProgressBar_Horizontal:I

    .line 8713
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ProgressBar_Large:I

    .line 8714
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_ProgressBar_Small:I

    .line 8715
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_SeekBar:I

    .line 8716
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Light_TimePicker:I

    .line 8717
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ListView:I

    .line 8718
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_NewSearchView_ActionBar:I

    .line 8719
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_NumberPicker:I

    .line 8720
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_PopupMenu:I

    .line 8721
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_PopupMenu_Overflow:I

    .line 8722
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ProgressBar:I

    .line 8723
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ProgressBar_Horizontal:I

    .line 8724
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ProgressBar_Large:I

    .line 8725
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ProgressBar_Small:I

    .line 8726
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ProgressDialog_ProgressBar:I

    .line 8727
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_ProgressDialog_ProgressBar_Horizontal:I

    .line 8728
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_SearchView_ActionBar:I

    .line 8732
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_SeekBar:I

    .line 8733
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_TimePicker:I

    .line 8734
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Toolbar:I

    .line 8735
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_Widget_Material_Toolbar_Button_Navigation:I

    .line 8736
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OneplusThemeBase:I

    .line 8737
    sput v0, Landroid/support/asynclayoutinflater/R$style;->OneplusThemeLightBase:I

    .line 8738
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_bottom_fullscreen:I

    .line 8739
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Oneplus_popup_bottom_animation:I

    .line 8740
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_AppCompat:I

    .line 8741
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_AppCompat_Light:I

    .line 8742
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_ThemeOverlay_AppCompat:I

    .line 8743
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_ThemeOverlay_AppCompat_Dark:I

    .line 8744
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_ThemeOverlay_AppCompat_Light:I

    .line 8745
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_V21_AppCompat:I

    .line 8746
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_V21_AppCompat_Light:I

    .line 8747
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_V25_AppCompat:I

    .line 8748
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_V25_AppCompat_Light:I

    .line 8749
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Platform_Widget_AppCompat_Spinner:I

    .line 8750
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_DialogWindowTitle_AppCompat:I

    .line 8751
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I

    .line 8752
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_DialogTitle_Icon:I

    .line 8753
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_PopupMenuItem:I

    .line 8754
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I

    .line 8755
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut:I

    .line 8756
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow:I

    .line 8757
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I

    .line 8758
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_PopupMenuItem_Title:I

    .line 8759
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_Search_DropDown:I

    .line 8760
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I

    .line 8761
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I

    .line 8762
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I

    .line 8763
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I

    .line 8764
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I

    .line 8765
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlUnderlay_Widget_AppCompat_ActionButton:I

    .line 8766
    sput v0, Landroid/support/asynclayoutinflater/R$style;->RtlUnderlay_Widget_AppCompat_ActionButton_Overflow:I

    .line 8767
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat:I

    .line 8768
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Body1:I

    .line 8769
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Body2:I

    .line 8770
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Button:I

    .line 8771
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Caption:I

    .line 8772
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Display1:I

    .line 8773
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Display2:I

    .line 8774
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Display3:I

    .line 8775
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Display4:I

    .line 8776
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Headline:I

    .line 8777
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Inverse:I

    .line 8778
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Large:I

    .line 8779
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Large_Inverse:I

    .line 8780
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Light_SearchResult_Subtitle:I

    .line 8781
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Light_SearchResult_Title:I

    .line 8782
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I

    .line 8783
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I

    .line 8784
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Medium:I

    .line 8785
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Medium_Inverse:I

    .line 8786
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Menu:I

    .line 8787
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_SearchResult_Subtitle:I

    .line 8788
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_SearchResult_Title:I

    .line 8789
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Small:I

    .line 8790
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Small_Inverse:I

    .line 8791
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Subhead:I

    .line 8792
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Subhead_Inverse:I

    .line 8793
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Title:I

    .line 8794
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Title_Inverse:I

    .line 8795
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Tooltip:I

    .line 8796
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionBar_Menu:I

    .line 8797
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I

    .line 8798
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I

    .line 8799
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionBar_Title:I

    .line 8800
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I

    .line 8801
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I

    .line 8802
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I

    .line 8803
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionMode_Title:I

    .line 8804
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I

    .line 8805
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_Button:I

    .line 8806
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I

    .line 8807
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_Button_Colored:I

    .line 8808
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_Button_Inverse:I

    .line 8809
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_DropDownItem:I

    .line 8810
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_PopupMenu_Header:I

    .line 8811
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_PopupMenu_Large:I

    .line 8812
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_PopupMenu_Small:I

    .line 8813
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_Switch:I

    .line 8814
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I

    .line 8815
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification:I

    .line 8816
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Info:I

    .line 8817
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Info_Media:I

    .line 8818
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Line2:I

    .line 8819
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Line2_Media:I

    .line 8820
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Media:I

    .line 8821
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Time:I

    .line 8822
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Time_Media:I

    .line 8823
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Title:I

    .line 8824
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Compat_Notification_Title_Media:I

    .line 8825
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Design_Counter:I

    .line 8826
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Design_Counter_Overflow:I

    .line 8827
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Design_Error:I

    .line 8828
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Design_Hint:I

    .line 8829
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Design_SnackBar_Action:I

    .line 8830
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Design_SnackBar_Text:I

    .line 8831
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_DialogWindowTitle:I

    .line 8832
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material:I

    .line 8833
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Body2:I

    .line 8834
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Button:I

    .line 8835
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Caption:I

    .line 8836
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_DialogWindowTitle:I

    .line 8837
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Title:I

    .line 8838
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Widget_Calendar_Day:I

    .line 8839
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Widget_Calendar_DayOfWeek:I

    .line 8840
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Widget_Calendar_Month:I

    .line 8841
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Material_Widget_Switch:I

    .line 8842
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Toast:I

    .line 8843
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Widget_ActionBar_Title:I

    .line 8844
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I

    .line 8845
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I

    .line 8846
    sput v0, Landroid/support/asynclayoutinflater/R$style;->TextAppearance_Widget_AppCompat_Toolbar_Title:I

    .line 8847
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat:I

    .line 8848
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_CompactMenu:I

    .line 8849
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DayNight:I

    .line 8850
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DayNight_DarkActionBar:I

    .line 8851
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DayNight_Dialog:I

    .line 8852
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DayNight_Dialog_Alert:I

    .line 8853
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DayNight_Dialog_MinWidth:I

    .line 8854
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DayNight_DialogWhenLarge:I

    .line 8855
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DayNight_NoActionBar:I

    .line 8856
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Dialog:I

    .line 8857
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Dialog_Alert:I

    .line 8858
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Dialog_MinWidth:I

    .line 8859
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_DialogWhenLarge:I

    .line 8860
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Light:I

    .line 8861
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Light_DarkActionBar:I

    .line 8862
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Light_Dialog:I

    .line 8863
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Light_Dialog_Alert:I

    .line 8864
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Light_Dialog_MinWidth:I

    .line 8865
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Light_DialogWhenLarge:I

    .line 8866
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_Light_NoActionBar:I

    .line 8867
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Theme_AppCompat_NoActionBar:I

    .line 8868
    sput v0, Landroid/support/asynclayoutinflater/R$style;->ThemeOverlay_AppCompat:I

    .line 8869
    sput v0, Landroid/support/asynclayoutinflater/R$style;->ThemeOverlay_AppCompat_ActionBar:I

    .line 8870
    sput v0, Landroid/support/asynclayoutinflater/R$style;->ThemeOverlay_AppCompat_Dark:I

    .line 8871
    sput v0, Landroid/support/asynclayoutinflater/R$style;->ThemeOverlay_AppCompat_Dark_ActionBar:I

    .line 8872
    sput v0, Landroid/support/asynclayoutinflater/R$style;->ThemeOverlay_AppCompat_Dialog:I

    .line 8873
    sput v0, Landroid/support/asynclayoutinflater/R$style;->ThemeOverlay_AppCompat_Dialog_Alert:I

    .line 8874
    sput v0, Landroid/support/asynclayoutinflater/R$style;->ThemeOverlay_AppCompat_Light:I

    .line 8875
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionBar:I

    .line 8876
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionBar_Solid:I

    .line 8877
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionBar_TabBar:I

    .line 8878
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionBar_TabText:I

    .line 8879
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionBar_TabView:I

    .line 8880
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionButton:I

    .line 8881
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionButton_CloseMode:I

    .line 8882
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionButton_Overflow:I

    .line 8883
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActionMode:I

    .line 8884
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ActivityChooserView:I

    .line 8885
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_AutoCompleteTextView:I

    .line 8886
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Button:I

    .line 8887
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Button_Borderless:I

    .line 8888
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Button_Borderless_Colored:I

    .line 8889
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Button_ButtonBar_AlertDialog:I

    .line 8890
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Button_Colored:I

    .line 8891
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Button_Small:I

    .line 8892
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ButtonBar:I

    .line 8893
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ButtonBar_AlertDialog:I

    .line 8894
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_CompoundButton_CheckBox:I

    .line 8895
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_CompoundButton_RadioButton:I

    .line 8896
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_CompoundButton_Switch:I

    .line 8897
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_DrawerArrowToggle:I

    .line 8898
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_DropDownItem_Spinner:I

    .line 8899
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_EditText:I

    .line 8900
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ImageButton:I

    .line 8901
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar:I

    .line 8902
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_Solid:I

    .line 8903
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_Solid_Inverse:I

    .line 8904
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_TabBar:I

    .line 8905
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I

    .line 8906
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_TabText:I

    .line 8907
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_TabText_Inverse:I

    .line 8908
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_TabView:I

    .line 8909
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionBar_TabView_Inverse:I

    .line 8910
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionButton:I

    .line 8911
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionButton_CloseMode:I

    .line 8912
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionButton_Overflow:I

    .line 8913
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActionMode_Inverse:I

    .line 8914
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ActivityChooserView:I

    .line 8915
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_AutoCompleteTextView:I

    .line 8916
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_DropDownItem_Spinner:I

    .line 8917
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ListPopupWindow:I

    .line 8918
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_ListView_DropDown:I

    .line 8919
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_PopupMenu:I

    .line 8920
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_PopupMenu_Overflow:I

    .line 8921
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_SearchView:I

    .line 8922
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I

    .line 8923
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ListMenuView:I

    .line 8924
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ListPopupWindow:I

    .line 8925
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ListView:I

    .line 8926
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ListView_DropDown:I

    .line 8927
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ListView_Menu:I

    .line 8928
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_PopupMenu:I

    .line 8929
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_PopupMenu_Overflow:I

    .line 8930
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_PopupWindow:I

    .line 8931
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ProgressBar:I

    .line 8932
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_ProgressBar_Horizontal:I

    .line 8933
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_RatingBar:I

    .line 8934
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_RatingBar_Indicator:I

    .line 8935
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_RatingBar_Small:I

    .line 8936
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_SearchView:I

    .line 8937
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_SearchView_ActionBar:I

    .line 8938
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_SeekBar:I

    .line 8939
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_SeekBar_Discrete:I

    .line 8940
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Spinner:I

    .line 8941
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Spinner_DropDown:I

    .line 8942
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Spinner_DropDown_ActionBar:I

    .line 8943
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Spinner_Underlined:I

    .line 8944
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_TextView_SpinnerItem:I

    .line 8945
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Toolbar:I

    .line 8946
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_AppCompat_Toolbar_Button_Navigation:I

    .line 8947
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Compat_NotificationActionContainer:I

    .line 8948
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Compat_NotificationActionText:I

    .line 8949
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Design_AppBarLayout:I

    .line 8950
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Design_BottomNavigationView:I

    .line 8951
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Design_CollapsingToolbar:I

    .line 8952
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Design_CoordinatorLayout:I

    .line 8953
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Design_OPTextInputLayout:I

    .line 8957
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material:I

    .line 8961
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_Button:I

    .line 8965
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_Button_Borderless:I

    .line 8969
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_Button_Borderless_Colored:I

    .line 8973
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_Button_ButtonBar_AlertDialog:I

    .line 8974
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_CompoundButton:I

    .line 8975
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_CompoundButton_Switch:I

    .line 8976
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_Light_NumberPicker:I

    .line 8977
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Material_NumberPicker:I

    .line 8983
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_MaterialComponents_BottomNavigationView:I

    .line 8989
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_MaterialComponents_BottomNavigationView_Colored:I

    .line 8990
    sput v0, Landroid/support/asynclayoutinflater/R$style;->Widget_Support_CoordinatorLayout:I

    .line 8991
    sput v0, Landroid/support/asynclayoutinflater/R$style;->listitem_primary_text_font:I

    .line 8992
    sput v0, Landroid/support/asynclayoutinflater/R$style;->listitem_secondary_text_font:I

    .line 8993
    sput v0, Landroid/support/asynclayoutinflater/R$style;->listitem_stamp_font:I

    .line 8997
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_base:I

    .line 8998
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_body1:I

    .line 8999
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_body2:I

    .line 9000
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_button:I

    .line 9001
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_descriptions:I

    .line 9002
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_display1:I

    .line 9003
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_display2:I

    .line 9004
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_display3:I

    .line 9008
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_display4:I

    .line 9009
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_headline:I

    .line 9010
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_highlight:I

    .line 9011
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_menu:I

    .line 9012
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_subheading:I

    .line 9013
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_subtitle:I

    .line 9014
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_title:I

    .line 9015
    sput v0, Landroid/support/asynclayoutinflater/R$style;->oneplus_contorl_text_style_title_extended:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
