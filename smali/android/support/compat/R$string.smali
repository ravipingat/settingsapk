.class public final Landroid/support/compat/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/compat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static abc_action_bar_home_description:I

.field public static abc_action_bar_up_description:I

.field public static abc_action_menu_overflow_description:I

.field public static abc_action_mode_done:I

.field public static abc_activity_chooser_view_see_all:I

.field public static abc_activitychooserview_choose_application:I

.field public static abc_capital_off:I

.field public static abc_capital_on:I

.field public static abc_font_family_body_1_material:I

.field public static abc_font_family_body_2_material:I

.field public static abc_font_family_button_material:I

.field public static abc_font_family_caption_material:I

.field public static abc_font_family_display_1_material:I

.field public static abc_font_family_display_2_material:I

.field public static abc_font_family_display_3_material:I

.field public static abc_font_family_display_4_material:I

.field public static abc_font_family_headline_material:I

.field public static abc_font_family_menu_material:I

.field public static abc_font_family_subhead_material:I

.field public static abc_font_family_title_material:I

.field public static abc_menu_alt_shortcut_label:I

.field public static abc_menu_ctrl_shortcut_label:I

.field public static abc_menu_delete_shortcut_label:I

.field public static abc_menu_enter_shortcut_label:I

.field public static abc_menu_function_shortcut_label:I

.field public static abc_menu_meta_shortcut_label:I

.field public static abc_menu_shift_shortcut_label:I

.field public static abc_menu_space_shortcut_label:I

.field public static abc_menu_sym_shortcut_label:I

.field public static abc_prepend_shortcut_label:I

.field public static abc_search_hint:I

.field public static abc_searchview_description_clear:I

.field public static abc_searchview_description_query:I

.field public static abc_searchview_description_search:I

.field public static abc_searchview_description_submit:I

.field public static abc_searchview_description_voice:I

.field public static abc_shareactionprovider_share_with:I

.field public static abc_shareactionprovider_share_with_application:I

.field public static abc_toolbar_collapse_description:I

.field public static appbar_scrolling_view_behavior:I

.field public static back_button_label:I

.field public static capital_off:I

.field public static capital_on:I

.field public static date_picker_day_of_week_typeface:I

.field public static date_picker_day_typeface:I

.field public static date_picker_month_typeface:I

.field public static deleted_key:I

.field public static font_family_body_1_material:I

.field public static font_family_body_2_material:I

.field public static font_family_button_material:I

.field public static font_family_caption_material:I

.field public static font_family_title_material:I

.field public static hide_bottom_view_on_scroll_behavior:I

.field public static next_button_label:I

.field public static oneplus_contorl_font_family_body1:I

.field public static oneplus_contorl_font_family_body2:I

.field public static oneplus_contorl_font_family_button:I

.field public static oneplus_contorl_font_family_descriptions:I

.field public static oneplus_contorl_font_family_display1:I

.field public static oneplus_contorl_font_family_display2:I

.field public static oneplus_contorl_font_family_display3:I

.field public static oneplus_contorl_font_family_display4:I

.field public static oneplus_contorl_font_family_headline:I

.field public static oneplus_contorl_font_family_highlight:I

.field public static oneplus_contorl_font_family_subheading:I

.field public static oneplus_contorl_font_family_subtitle:I

.field public static oneplus_contorl_font_family_title:I

.field public static oneplus_contorl_font_family_title_extended:I

.field public static op_accessibility_quick_settings_page:I

.field public static op_appbar_scrolling_view_behavior:I

.field public static op_character_counter_pattern:I

.field public static op_password_toggle_content_description:I

.field public static op_path_password_eye:I

.field public static op_path_password_eye_mask_strike_through:I

.field public static op_path_password_eye_mask_visible:I

.field public static op_path_password_strike_through:I

.field public static search_menu_title:I

.field public static searchview_description_clear:I

.field public static searchview_description_search:I

.field public static searchview_description_submit:I

.field public static searchview_description_voice:I

.field public static select_day:I

.field public static select_hours:I

.field public static select_minutes:I

.field public static select_year:I

.field public static skip_button_label:I

.field public static status_bar_notification_info_overflow:I

.field public static time_picker_header_text:I

.field public static time_picker_hour_label:I

.field public static time_picker_input_error:I

.field public static time_picker_minute_label:I

.field public static time_picker_prompt_label:I

.field public static time_placeholder:I

.field public static timepicker_ampm_circle_radius_multiplier:I

.field public static timepicker_circle_radius_multiplier:I

.field public static timepicker_circle_radius_multiplier_24HourMode:I

.field public static timepicker_numbers_radius_multiplier_inner:I

.field public static timepicker_numbers_radius_multiplier_normal:I

.field public static timepicker_numbers_radius_multiplier_outer:I

.field public static timepicker_selection_radius_multiplier:I

.field public static timepicker_text_size_multiplier_inner:I

.field public static timepicker_text_size_multiplier_normal:I

.field public static timepicker_text_size_multiplier_outer:I

.field public static timepicker_transition_end_radius_multiplier:I

.field public static timepicker_transition_mid_radius_multiplier:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7567
    const/4 v0, 0x0

    sput v0, Landroid/support/compat/R$string;->abc_action_bar_home_description:I

    .line 7568
    sput v0, Landroid/support/compat/R$string;->abc_action_bar_up_description:I

    .line 7569
    sput v0, Landroid/support/compat/R$string;->abc_action_menu_overflow_description:I

    .line 7570
    sput v0, Landroid/support/compat/R$string;->abc_action_mode_done:I

    .line 7571
    sput v0, Landroid/support/compat/R$string;->abc_activity_chooser_view_see_all:I

    .line 7572
    sput v0, Landroid/support/compat/R$string;->abc_activitychooserview_choose_application:I

    .line 7573
    sput v0, Landroid/support/compat/R$string;->abc_capital_off:I

    .line 7574
    sput v0, Landroid/support/compat/R$string;->abc_capital_on:I

    .line 7575
    sput v0, Landroid/support/compat/R$string;->abc_font_family_body_1_material:I

    .line 7576
    sput v0, Landroid/support/compat/R$string;->abc_font_family_body_2_material:I

    .line 7577
    sput v0, Landroid/support/compat/R$string;->abc_font_family_button_material:I

    .line 7578
    sput v0, Landroid/support/compat/R$string;->abc_font_family_caption_material:I

    .line 7579
    sput v0, Landroid/support/compat/R$string;->abc_font_family_display_1_material:I

    .line 7580
    sput v0, Landroid/support/compat/R$string;->abc_font_family_display_2_material:I

    .line 7581
    sput v0, Landroid/support/compat/R$string;->abc_font_family_display_3_material:I

    .line 7582
    sput v0, Landroid/support/compat/R$string;->abc_font_family_display_4_material:I

    .line 7583
    sput v0, Landroid/support/compat/R$string;->abc_font_family_headline_material:I

    .line 7584
    sput v0, Landroid/support/compat/R$string;->abc_font_family_menu_material:I

    .line 7585
    sput v0, Landroid/support/compat/R$string;->abc_font_family_subhead_material:I

    .line 7586
    sput v0, Landroid/support/compat/R$string;->abc_font_family_title_material:I

    .line 7587
    sput v0, Landroid/support/compat/R$string;->abc_menu_alt_shortcut_label:I

    .line 7588
    sput v0, Landroid/support/compat/R$string;->abc_menu_ctrl_shortcut_label:I

    .line 7589
    sput v0, Landroid/support/compat/R$string;->abc_menu_delete_shortcut_label:I

    .line 7590
    sput v0, Landroid/support/compat/R$string;->abc_menu_enter_shortcut_label:I

    .line 7591
    sput v0, Landroid/support/compat/R$string;->abc_menu_function_shortcut_label:I

    .line 7592
    sput v0, Landroid/support/compat/R$string;->abc_menu_meta_shortcut_label:I

    .line 7593
    sput v0, Landroid/support/compat/R$string;->abc_menu_shift_shortcut_label:I

    .line 7594
    sput v0, Landroid/support/compat/R$string;->abc_menu_space_shortcut_label:I

    .line 7595
    sput v0, Landroid/support/compat/R$string;->abc_menu_sym_shortcut_label:I

    .line 7596
    sput v0, Landroid/support/compat/R$string;->abc_prepend_shortcut_label:I

    .line 7597
    sput v0, Landroid/support/compat/R$string;->abc_search_hint:I

    .line 7598
    sput v0, Landroid/support/compat/R$string;->abc_searchview_description_clear:I

    .line 7599
    sput v0, Landroid/support/compat/R$string;->abc_searchview_description_query:I

    .line 7600
    sput v0, Landroid/support/compat/R$string;->abc_searchview_description_search:I

    .line 7601
    sput v0, Landroid/support/compat/R$string;->abc_searchview_description_submit:I

    .line 7602
    sput v0, Landroid/support/compat/R$string;->abc_searchview_description_voice:I

    .line 7603
    sput v0, Landroid/support/compat/R$string;->abc_shareactionprovider_share_with:I

    .line 7604
    sput v0, Landroid/support/compat/R$string;->abc_shareactionprovider_share_with_application:I

    .line 7605
    sput v0, Landroid/support/compat/R$string;->abc_toolbar_collapse_description:I

    .line 7609
    sput v0, Landroid/support/compat/R$string;->appbar_scrolling_view_behavior:I

    .line 7615
    sput v0, Landroid/support/compat/R$string;->back_button_label:I

    .line 7616
    sput v0, Landroid/support/compat/R$string;->capital_off:I

    .line 7617
    sput v0, Landroid/support/compat/R$string;->capital_on:I

    .line 7621
    sput v0, Landroid/support/compat/R$string;->date_picker_day_of_week_typeface:I

    .line 7625
    sput v0, Landroid/support/compat/R$string;->date_picker_day_typeface:I

    .line 7629
    sput v0, Landroid/support/compat/R$string;->date_picker_month_typeface:I

    .line 7630
    sput v0, Landroid/support/compat/R$string;->deleted_key:I

    .line 7631
    sput v0, Landroid/support/compat/R$string;->font_family_body_1_material:I

    .line 7632
    sput v0, Landroid/support/compat/R$string;->font_family_body_2_material:I

    .line 7633
    sput v0, Landroid/support/compat/R$string;->font_family_button_material:I

    .line 7634
    sput v0, Landroid/support/compat/R$string;->font_family_caption_material:I

    .line 7635
    sput v0, Landroid/support/compat/R$string;->font_family_title_material:I

    .line 7639
    sput v0, Landroid/support/compat/R$string;->hide_bottom_view_on_scroll_behavior:I

    .line 7640
    sput v0, Landroid/support/compat/R$string;->next_button_label:I

    .line 7641
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_body1:I

    .line 7642
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_body2:I

    .line 7643
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_button:I

    .line 7644
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_descriptions:I

    .line 7645
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_display1:I

    .line 7646
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_display2:I

    .line 7647
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_display3:I

    .line 7648
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_display4:I

    .line 7649
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_headline:I

    .line 7650
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_highlight:I

    .line 7651
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_subheading:I

    .line 7652
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_subtitle:I

    .line 7653
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_title:I

    .line 7654
    sput v0, Landroid/support/compat/R$string;->oneplus_contorl_font_family_title_extended:I

    .line 7655
    sput v0, Landroid/support/compat/R$string;->op_accessibility_quick_settings_page:I

    .line 7656
    sput v0, Landroid/support/compat/R$string;->op_appbar_scrolling_view_behavior:I

    .line 7657
    sput v0, Landroid/support/compat/R$string;->op_character_counter_pattern:I

    .line 7658
    sput v0, Landroid/support/compat/R$string;->op_password_toggle_content_description:I

    .line 7659
    sput v0, Landroid/support/compat/R$string;->op_path_password_eye:I

    .line 7660
    sput v0, Landroid/support/compat/R$string;->op_path_password_eye_mask_strike_through:I

    .line 7664
    sput v0, Landroid/support/compat/R$string;->op_path_password_eye_mask_visible:I

    .line 7665
    sput v0, Landroid/support/compat/R$string;->op_path_password_strike_through:I

    .line 7666
    sput v0, Landroid/support/compat/R$string;->search_menu_title:I

    .line 7667
    sput v0, Landroid/support/compat/R$string;->searchview_description_clear:I

    .line 7668
    sput v0, Landroid/support/compat/R$string;->searchview_description_search:I

    .line 7669
    sput v0, Landroid/support/compat/R$string;->searchview_description_submit:I

    .line 7670
    sput v0, Landroid/support/compat/R$string;->searchview_description_voice:I

    .line 7674
    sput v0, Landroid/support/compat/R$string;->select_day:I

    .line 7678
    sput v0, Landroid/support/compat/R$string;->select_hours:I

    .line 7682
    sput v0, Landroid/support/compat/R$string;->select_minutes:I

    .line 7686
    sput v0, Landroid/support/compat/R$string;->select_year:I

    .line 7690
    sput v0, Landroid/support/compat/R$string;->skip_button_label:I

    .line 7691
    sput v0, Landroid/support/compat/R$string;->status_bar_notification_info_overflow:I

    .line 7695
    sput v0, Landroid/support/compat/R$string;->time_picker_header_text:I

    .line 7699
    sput v0, Landroid/support/compat/R$string;->time_picker_hour_label:I

    .line 7703
    sput v0, Landroid/support/compat/R$string;->time_picker_input_error:I

    .line 7707
    sput v0, Landroid/support/compat/R$string;->time_picker_minute_label:I

    .line 7711
    sput v0, Landroid/support/compat/R$string;->time_picker_prompt_label:I

    .line 7715
    sput v0, Landroid/support/compat/R$string;->time_placeholder:I

    .line 7716
    sput v0, Landroid/support/compat/R$string;->timepicker_ampm_circle_radius_multiplier:I

    .line 7720
    sput v0, Landroid/support/compat/R$string;->timepicker_circle_radius_multiplier:I

    .line 7721
    sput v0, Landroid/support/compat/R$string;->timepicker_circle_radius_multiplier_24HourMode:I

    .line 7722
    sput v0, Landroid/support/compat/R$string;->timepicker_numbers_radius_multiplier_inner:I

    .line 7723
    sput v0, Landroid/support/compat/R$string;->timepicker_numbers_radius_multiplier_normal:I

    .line 7724
    sput v0, Landroid/support/compat/R$string;->timepicker_numbers_radius_multiplier_outer:I

    .line 7725
    sput v0, Landroid/support/compat/R$string;->timepicker_selection_radius_multiplier:I

    .line 7726
    sput v0, Landroid/support/compat/R$string;->timepicker_text_size_multiplier_inner:I

    .line 7727
    sput v0, Landroid/support/compat/R$string;->timepicker_text_size_multiplier_normal:I

    .line 7728
    sput v0, Landroid/support/compat/R$string;->timepicker_text_size_multiplier_outer:I

    .line 7729
    sput v0, Landroid/support/compat/R$string;->timepicker_transition_end_radius_multiplier:I

    .line 7730
    sput v0, Landroid/support/compat/R$string;->timepicker_transition_mid_radius_multiplier:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
