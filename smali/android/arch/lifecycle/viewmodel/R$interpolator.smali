.class public final Landroid/arch/lifecycle/viewmodel/R$interpolator;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/arch/lifecycle/viewmodel/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "interpolator"
.end annotation


# static fields
.field public static accelerate_quad:I

.field public static decelerate_quad:I

.field public static ic_checkbox_checked_animation_interpolator_0:I

.field public static ic_checkbox_checked_animation_interpolator_1:I

.field public static ic_checkbox_unchecked_animation_interpolator_0:I

.field public static ic_checkbox_unchecked_animation_interpolator_1:I

.field public static oneplus_control_fast_out_linear_in:I

.field public static oneplus_control_fast_out_slow_in:I

.field public static oneplus_control_fast_out_slow_in_sine:I

.field public static oneplus_control_linear_out_slow_in:I

.field public static progress_indeterminate_horizontal_rect1_scalex:I

.field public static progress_indeterminate_horizontal_rect1_translatex:I

.field public static progress_indeterminate_horizontal_rect2_scalex:I

.field public static progress_indeterminate_horizontal_rect2_translatex:I

.field public static trim_end_interpolator:I

.field public static trim_start_interpolator:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7409
    const/4 v0, 0x0

    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->accelerate_quad:I

    .line 7410
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->decelerate_quad:I

    .line 7411
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->ic_checkbox_checked_animation_interpolator_0:I

    .line 7412
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->ic_checkbox_checked_animation_interpolator_1:I

    .line 7413
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->ic_checkbox_unchecked_animation_interpolator_0:I

    .line 7414
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->ic_checkbox_unchecked_animation_interpolator_1:I

    .line 7415
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->oneplus_control_fast_out_linear_in:I

    .line 7416
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->oneplus_control_fast_out_slow_in:I

    .line 7417
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->oneplus_control_fast_out_slow_in_sine:I

    .line 7418
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->oneplus_control_linear_out_slow_in:I

    .line 7419
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->progress_indeterminate_horizontal_rect1_scalex:I

    .line 7420
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->progress_indeterminate_horizontal_rect1_translatex:I

    .line 7421
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->progress_indeterminate_horizontal_rect2_scalex:I

    .line 7422
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->progress_indeterminate_horizontal_rect2_translatex:I

    .line 7423
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->trim_end_interpolator:I

    .line 7424
    sput v0, Landroid/arch/lifecycle/viewmodel/R$interpolator;->trim_start_interpolator:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
