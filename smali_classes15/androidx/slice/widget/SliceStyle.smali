.class public Landroidx/slice/widget/SliceStyle;
.super Ljava/lang/Object;
.source "SliceStyle.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x13
.end annotation

.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# instance fields
.field private mGridAllImagesHeight:I

.field private mGridBigPicMaxHeight:I

.field private mGridBigPicMinHeight:I

.field private mGridBottomPadding:I

.field private mGridImageTextHeight:I

.field private mGridMaxHeight:I

.field private mGridMinHeight:I

.field private mGridSubtitleSize:I

.field private mGridTitleSize:I

.field private mGridTopPadding:I

.field private mHeaderSubtitleSize:I

.field private mHeaderTitleSize:I

.field private mListLargeHeight:I

.field private mListMinScrollHeight:I

.field private mRowMaxHeight:I

.field private mRowMinHeight:I

.field private mRowRangeHeight:I

.field private mRowSelectionHeight:I

.field private mRowSingleTextWithRangeHeight:I

.field private mRowSingleTextWithSelectionHeight:I

.field private mRowStyle:Landroidx/slice/widget/RowStyle;

.field private mRowTextWithRangeHeight:I

.field private mRowTextWithSelectionHeight:I

.field private mSubtitleColor:I

.field private mSubtitleSize:I

.field private mTintColor:I

.field private mTitleColor:I

.field private mTitleSize:I

.field private mVerticalGridTextPadding:I

.field private mVerticalHeaderTextPadding:I

.field private mVerticalTextPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mTintColor:I

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Landroidx/slice/view/R$styleable;->SliceView:[I

    invoke-virtual {v1, p2, v2, p3, p4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 86
    .local v1, "a":Landroid/content/res/TypedArray;
    :try_start_0
    sget v2, Landroidx/slice/view/R$styleable;->SliceView_tintColor:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 87
    .local v2, "themeColor":I
    if-eq v2, v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mTintColor:I

    :goto_0
    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mTintColor:I

    .line 88
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_titleColor:I

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mTitleColor:I

    .line 89
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_subtitleColor:I

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mSubtitleColor:I

    .line 91
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_headerTitleSize:I

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mHeaderTitleSize:I

    .line 93
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_headerSubtitleSize:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mHeaderSubtitleSize:I

    .line 95
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_headerTextVerticalPadding:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mVerticalHeaderTextPadding:I

    .line 98
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_titleSize:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mTitleSize:I

    .line 99
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_subtitleSize:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mSubtitleSize:I

    .line 101
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_textVerticalPadding:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mVerticalTextPadding:I

    .line 104
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_gridTitleSize:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mGridTitleSize:I

    .line 105
    sget v0, Landroidx/slice/view/R$styleable;->SliceView_gridSubtitleSize:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroidx/slice/widget/SliceStyle;->mGridSubtitleSize:I

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Landroidx/slice/view/R$dimen;->abc_slice_grid_text_inner_padding:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 109
    .local v0, "defaultVerticalGridPadding":I
    sget v5, Landroidx/slice/view/R$styleable;->SliceView_gridTextVerticalPadding:I

    int-to-float v6, v0

    invoke-virtual {v1, v5, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Landroidx/slice/widget/SliceStyle;->mVerticalGridTextPadding:I

    .line 111
    sget v5, Landroidx/slice/view/R$styleable;->SliceView_gridTopPadding:I

    invoke-virtual {v1, v5, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Landroidx/slice/widget/SliceStyle;->mGridTopPadding:I

    .line 112
    sget v5, Landroidx/slice/view/R$styleable;->SliceView_gridBottomPadding:I

    invoke-virtual {v1, v5, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Landroidx/slice/widget/SliceStyle;->mGridBottomPadding:I

    .line 114
    sget v4, Landroidx/slice/view/R$styleable;->SliceView_rowStyle:I

    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 115
    .local v3, "rowStyleRes":I
    if-eqz v3, :cond_1

    .line 116
    new-instance v4, Landroidx/slice/widget/RowStyle;

    invoke-direct {v4, p1, v3}, Landroidx/slice/widget/RowStyle;-><init>(Landroid/content/Context;I)V

    iput-object v4, p0, Landroidx/slice/widget/SliceStyle;->mRowStyle:Landroidx/slice/widget/RowStyle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    .end local v0    # "defaultVerticalGridPadding":I
    .end local v2    # "themeColor":I
    .end local v3    # "rowStyleRes":I
    :cond_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 120
    nop

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 126
    .local v0, "r":Landroid/content/res/Resources;
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_max_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowMaxHeight:I

    .line 127
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_range_multi_text_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowTextWithRangeHeight:I

    .line 129
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_range_single_text_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowSingleTextWithRangeHeight:I

    .line 131
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_min_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowMinHeight:I

    .line 132
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_range_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowRangeHeight:I

    .line 133
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_selection_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowSelectionHeight:I

    .line 134
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_selection_multi_text_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowTextWithSelectionHeight:I

    .line 136
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_selection_single_text_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mRowSingleTextWithSelectionHeight:I

    .line 139
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_big_pic_min_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mGridBigPicMinHeight:I

    .line 140
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_big_pic_max_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mGridBigPicMaxHeight:I

    .line 141
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_grid_image_only_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mGridAllImagesHeight:I

    .line 142
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_grid_image_text_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mGridImageTextHeight:I

    .line 143
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_grid_min_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mGridMinHeight:I

    .line 144
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_grid_max_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mGridMaxHeight:I

    .line 146
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_row_min_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mListMinScrollHeight:I

    .line 147
    sget v2, Landroidx/slice/view/R$dimen;->abc_slice_large_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Landroidx/slice/widget/SliceStyle;->mListLargeHeight:I

    .line 148
    return-void

    .line 119
    .end local v0    # "r":Landroid/content/res/Resources;
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public getGridBottomPadding()I
    .locals 1

    .line 211
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mGridBottomPadding:I

    return v0
.end method

.method public getGridHeight(Landroidx/slice/widget/GridContent;Landroidx/slice/widget/SliceViewPolicy;)I
    .locals 7
    .param p1, "grid"    # Landroidx/slice/widget/GridContent;
    .param p2, "policy"    # Landroidx/slice/widget/SliceViewPolicy;

    .line 254
    invoke-virtual {p2}, Landroidx/slice/widget/SliceViewPolicy;->getMode()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 255
    .local v0, "isSmall":Z
    :goto_0
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->isValid()Z

    move-result v3

    if-nez v3, :cond_1

    .line 256
    return v1

    .line 258
    :cond_1
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->getLargestImageMode()I

    move-result v3

    .line 260
    .local v3, "largestImageMode":I
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->isAllImages()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 261
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->getGridContent()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v2, :cond_3

    if-eqz v0, :cond_2

    iget v2, p0, Landroidx/slice/widget/SliceStyle;->mGridBigPicMinHeight:I

    goto :goto_1

    :cond_2
    iget v2, p0, Landroidx/slice/widget/SliceStyle;->mGridBigPicMaxHeight:I

    goto :goto_1

    :cond_3
    if-nez v3, :cond_4

    iget v2, p0, Landroidx/slice/widget/SliceStyle;->mGridMinHeight:I

    goto :goto_1

    :cond_4
    iget v2, p0, Landroidx/slice/widget/SliceStyle;->mGridAllImagesHeight:I

    .local v2, "height":I
    :goto_1
    goto :goto_5

    .line 266
    .end local v2    # "height":I
    :cond_5
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->getMaxCellLineCount()I

    move-result v4

    if-le v4, v2, :cond_6

    move v4, v2

    goto :goto_2

    :cond_6
    move v4, v1

    .line 267
    .local v4, "twoLines":Z
    :goto_2
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->hasImage()Z

    move-result v5

    .line 268
    .local v5, "hasImage":Z
    if-eqz v3, :cond_8

    const/4 v6, 0x3

    if-ne v3, v6, :cond_7

    goto :goto_3

    :cond_7
    move v2, v1

    .line 270
    .local v2, "iconImagesOrNone":Z
    :cond_8
    :goto_3
    if-eqz v4, :cond_9

    if-nez v0, :cond_9

    if-eqz v5, :cond_a

    iget v6, p0, Landroidx/slice/widget/SliceStyle;->mGridMaxHeight:I

    goto :goto_4

    :cond_9
    if-eqz v2, :cond_b

    :cond_a
    iget v6, p0, Landroidx/slice/widget/SliceStyle;->mGridMinHeight:I

    goto :goto_4

    :cond_b
    iget v6, p0, Landroidx/slice/widget/SliceStyle;->mGridImageTextHeight:I

    :goto_4
    move v2, v6

    .line 274
    .end local v4    # "twoLines":Z
    .end local v5    # "hasImage":Z
    .local v2, "height":I
    :goto_5
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->isAllImages()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->getRowIndex()I

    move-result v4

    if-nez v4, :cond_c

    iget v4, p0, Landroidx/slice/widget/SliceStyle;->mGridTopPadding:I

    goto :goto_6

    :cond_c
    move v4, v1

    .line 276
    .local v4, "topPadding":I
    :goto_6
    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->isAllImages()Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-virtual {p1}, Landroidx/slice/widget/GridContent;->getIsLastIndex()Z

    move-result v5

    if-eqz v5, :cond_d

    iget v1, p0, Landroidx/slice/widget/SliceStyle;->mGridBottomPadding:I

    .line 278
    .local v1, "bottomPadding":I
    :cond_d
    add-int v5, v2, v4

    add-int/2addr v5, v1

    return v5
.end method

.method public getGridSubtitleSize()I
    .locals 1

    .line 199
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mGridSubtitleSize:I

    return v0
.end method

.method public getGridTitleSize()I
    .locals 1

    .line 195
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mGridTitleSize:I

    return v0
.end method

.method public getGridTopPadding()I
    .locals 1

    .line 207
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mGridTopPadding:I

    return v0
.end method

.method public getHeaderSubtitleSize()I
    .locals 1

    .line 175
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mHeaderSubtitleSize:I

    return v0
.end method

.method public getHeaderTitleSize()I
    .locals 1

    .line 171
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mHeaderTitleSize:I

    return v0
.end method

.method public getListHeight(Landroidx/slice/widget/ListContent;Landroidx/slice/widget/SliceViewPolicy;)I
    .locals 7
    .param p1, "list"    # Landroidx/slice/widget/ListContent;
    .param p2, "policy"    # Landroidx/slice/widget/SliceViewPolicy;

    .line 282
    invoke-virtual {p2}, Landroidx/slice/widget/SliceViewPolicy;->getMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 283
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getHeader()Landroidx/slice/widget/RowContent;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Landroidx/slice/widget/RowContent;->getHeight(Landroidx/slice/widget/SliceStyle;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v0

    return v0

    .line 285
    :cond_0
    invoke-virtual {p2}, Landroidx/slice/widget/SliceViewPolicy;->getMaxHeight()I

    move-result v0

    .line 286
    .local v0, "maxHeight":I
    invoke-virtual {p2}, Landroidx/slice/widget/SliceViewPolicy;->isScrollable()Z

    move-result v2

    .line 288
    .local v2, "scrollable":Z
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p0, v3, p2}, Landroidx/slice/widget/SliceStyle;->getListItemsHeight(Ljava/util/List;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v3

    .line 289
    .local v3, "desiredHeight":I
    if-lez v0, :cond_1

    .line 291
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getHeader()Landroidx/slice/widget/RowContent;

    move-result-object v4

    invoke-virtual {v4, p0, p2}, Landroidx/slice/widget/RowContent;->getHeight(Landroidx/slice/widget/SliceStyle;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v4

    .line 292
    .local v4, "smallHeight":I
    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 294
    .end local v4    # "smallHeight":I
    :cond_1
    if-lez v0, :cond_2

    move v4, v0

    goto :goto_0

    :cond_2
    iget v4, p0, Landroidx/slice/widget/SliceStyle;->mListLargeHeight:I

    .line 298
    .local v4, "maxLargeHeight":I
    :goto_0
    sub-int v5, v3, v4

    iget v6, p0, Landroidx/slice/widget/SliceStyle;->mListMinScrollHeight:I

    if-lt v5, v6, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    .line 301
    .local v1, "bigEnoughToScroll":Z
    :goto_1
    if-eqz v1, :cond_4

    move v5, v4

    goto :goto_2

    :cond_4
    if-gtz v0, :cond_5

    move v5, v3

    goto :goto_2

    .line 303
    :cond_5
    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v5

    :goto_2
    nop

    .line 304
    .local v5, "height":I
    if-nez v2, :cond_6

    .line 305
    invoke-virtual {p0, p1, v5, p2}, Landroidx/slice/widget/SliceStyle;->getListItemsForNonScrollingList(Landroidx/slice/widget/ListContent;ILandroidx/slice/widget/SliceViewPolicy;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {p0, v6, p2}, Landroidx/slice/widget/SliceStyle;->getListItemsHeight(Ljava/util/List;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v5

    .line 308
    :cond_6
    return v5
.end method

.method public getListItemsForNonScrollingList(Landroidx/slice/widget/ListContent;ILandroidx/slice/widget/SliceViewPolicy;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "list"    # Landroidx/slice/widget/ListContent;
    .param p2, "availableHeight"    # I
    .param p3, "policy"    # Landroidx/slice/widget/SliceViewPolicy;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/slice/widget/ListContent;",
            "I",
            "Landroidx/slice/widget/SliceViewPolicy;",
            ")",
            "Ljava/util/ArrayList<",
            "Landroidx/slice/widget/SliceContent;",
            ">;"
        }
    .end annotation

    .line 343
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 344
    .local v0, "visibleItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroidx/slice/widget/SliceContent;>;"
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_3

    .line 347
    :cond_0
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    .line 348
    .local v1, "minItemCountForSeeMore":I
    :goto_0
    const/4 v2, 0x0

    .line 350
    .local v2, "visibleHeight":I
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getSeeMoreItem()Landroidx/slice/widget/SliceContent;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 351
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getSeeMoreItem()Landroidx/slice/widget/SliceContent;

    move-result-object v3

    invoke-virtual {v3, p0, p3}, Landroidx/slice/widget/SliceContent;->getHeight(Landroidx/slice/widget/SliceStyle;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v3

    add-int/2addr v2, v3

    .line 353
    :cond_2
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 354
    .local v3, "rowCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v3, :cond_4

    .line 355
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroidx/slice/widget/SliceContent;

    invoke-virtual {v5, p0, p3}, Landroidx/slice/widget/SliceContent;->getHeight(Landroidx/slice/widget/SliceStyle;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v5

    .line 356
    .local v5, "itemHeight":I
    if-lez p2, :cond_3

    add-int v6, v2, v5

    if-le v6, p2, :cond_3

    .line 357
    goto :goto_2

    .line 359
    :cond_3
    add-int/2addr v2, v5

    .line 360
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    .end local v5    # "itemHeight":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 363
    .end local v4    # "i":I
    :cond_4
    :goto_2
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getSeeMoreItem()Landroidx/slice/widget/SliceContent;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v4, v1, :cond_5

    .line 364
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v4, v3, :cond_5

    .line 366
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getSeeMoreItem()Landroidx/slice/widget/SliceContent;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_6

    .line 370
    invoke-virtual {p1}, Landroidx/slice/widget/ListContent;->getRowItems()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    :cond_6
    return-object v0

    .line 345
    .end local v1    # "minItemCountForSeeMore":I
    .end local v2    # "visibleHeight":I
    .end local v3    # "rowCount":I
    :cond_7
    :goto_3
    return-object v0
.end method

.method public getListItemsHeight(Ljava/util/List;Landroidx/slice/widget/SliceViewPolicy;)I
    .locals 4
    .param p2, "policy"    # Landroidx/slice/widget/SliceViewPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroidx/slice/widget/SliceContent;",
            ">;",
            "Landroidx/slice/widget/SliceViewPolicy;",
            ")I"
        }
    .end annotation

    .line 312
    .local p1, "listItems":Ljava/util/List;, "Ljava/util/List<Landroidx/slice/widget/SliceContent;>;"
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 313
    return v0

    .line 315
    :cond_0
    const/4 v1, 0x0

    .line 316
    .local v1, "height":I
    const/4 v2, 0x0

    .line 317
    .local v2, "maybeHeader":Landroidx/slice/widget/SliceContent;
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 318
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroidx/slice/widget/SliceContent;

    .line 320
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    invoke-virtual {v2}, Landroidx/slice/widget/SliceContent;->getSliceItem()Landroidx/slice/SliceItem;

    move-result-object v0

    const-string v3, "horizontal"

    invoke-virtual {v0, v3}, Landroidx/slice/SliceItem;->hasHint(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 321
    invoke-virtual {v2, p0, p2}, Landroidx/slice/widget/SliceContent;->getHeight(Landroidx/slice/widget/SliceStyle;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v0

    return v0

    .line 323
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 324
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroidx/slice/widget/SliceContent;

    invoke-virtual {v3, p0, p2}, Landroidx/slice/widget/SliceContent;->getHeight(Landroidx/slice/widget/SliceStyle;Landroidx/slice/widget/SliceViewPolicy;)I

    move-result v3

    add-int/2addr v1, v3

    .line 323
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 326
    .end local v0    # "i":I
    :cond_3
    return v1
.end method

.method public getRowHeight(Landroidx/slice/widget/RowContent;Landroidx/slice/widget/SliceViewPolicy;)I
    .locals 3
    .param p1, "row"    # Landroidx/slice/widget/RowContent;
    .param p2, "policy"    # Landroidx/slice/widget/SliceViewPolicy;

    .line 227
    invoke-virtual {p2}, Landroidx/slice/widget/SliceViewPolicy;->getMaxSmallHeight()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p2}, Landroidx/slice/widget/SliceViewPolicy;->getMaxSmallHeight()I

    move-result v0

    goto :goto_0

    :cond_0
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mRowMaxHeight:I

    .line 229
    .local v0, "maxHeight":I
    :goto_0
    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getRange()Landroidx/slice/SliceItem;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getSelection()Landroidx/slice/SliceItem;

    move-result-object v1

    if-nez v1, :cond_1

    .line 230
    invoke-virtual {p2}, Landroidx/slice/widget/SliceViewPolicy;->getMode()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 231
    return v0

    .line 234
    :cond_1
    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getRange()Landroidx/slice/SliceItem;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    .line 237
    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getLineCount()I

    move-result v1

    if-le v1, v2, :cond_2

    iget v1, p0, Landroidx/slice/widget/SliceStyle;->mRowTextWithRangeHeight:I

    goto :goto_1

    :cond_2
    iget v1, p0, Landroidx/slice/widget/SliceStyle;->mRowSingleTextWithRangeHeight:I

    .line 239
    .local v1, "textAreaHeight":I
    :goto_1
    iget v2, p0, Landroidx/slice/widget/SliceStyle;->mRowRangeHeight:I

    add-int/2addr v2, v1

    return v2

    .line 242
    .end local v1    # "textAreaHeight":I
    :cond_3
    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getSelection()Landroidx/slice/SliceItem;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 245
    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getLineCount()I

    move-result v1

    if-le v1, v2, :cond_4

    iget v1, p0, Landroidx/slice/widget/SliceStyle;->mRowTextWithSelectionHeight:I

    goto :goto_2

    :cond_4
    iget v1, p0, Landroidx/slice/widget/SliceStyle;->mRowSingleTextWithSelectionHeight:I

    .line 247
    .restart local v1    # "textAreaHeight":I
    :goto_2
    iget v2, p0, Landroidx/slice/widget/SliceStyle;->mRowSelectionHeight:I

    add-int/2addr v2, v1

    return v2

    .line 250
    .end local v1    # "textAreaHeight":I
    :cond_5
    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getLineCount()I

    move-result v1

    if-gt v1, v2, :cond_7

    invoke-virtual {p1}, Landroidx/slice/widget/RowContent;->getIsHeader()Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_3

    :cond_6
    iget v1, p0, Landroidx/slice/widget/SliceStyle;->mRowMinHeight:I

    goto :goto_4

    :cond_7
    :goto_3
    move v1, v0

    :goto_4
    return v1
.end method

.method public getRowMaxHeight()I
    .locals 1

    .line 151
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mRowMaxHeight:I

    return v0
.end method

.method public getRowRangeHeight()I
    .locals 1

    .line 219
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mRowRangeHeight:I

    return v0
.end method

.method public getRowSelectionHeight()I
    .locals 1

    .line 223
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mRowSelectionHeight:I

    return v0
.end method

.method public getRowStyle()Landroidx/slice/widget/RowStyle;
    .locals 1

    .line 215
    iget-object v0, p0, Landroidx/slice/widget/SliceStyle;->mRowStyle:Landroidx/slice/widget/RowStyle;

    return-object v0
.end method

.method public getSubtitleColor()I
    .locals 1

    .line 167
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mSubtitleColor:I

    return v0
.end method

.method public getSubtitleSize()I
    .locals 1

    .line 187
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mSubtitleSize:I

    return v0
.end method

.method public getTintColor()I
    .locals 1

    .line 159
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mTintColor:I

    return v0
.end method

.method public getTitleColor()I
    .locals 1

    .line 163
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mTitleColor:I

    return v0
.end method

.method public getTitleSize()I
    .locals 1

    .line 183
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mTitleSize:I

    return v0
.end method

.method public getVerticalGridTextPadding()I
    .locals 1

    .line 203
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mVerticalGridTextPadding:I

    return v0
.end method

.method public getVerticalHeaderTextPadding()I
    .locals 1

    .line 179
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mVerticalHeaderTextPadding:I

    return v0
.end method

.method public getVerticalTextPadding()I
    .locals 1

    .line 191
    iget v0, p0, Landroidx/slice/widget/SliceStyle;->mVerticalTextPadding:I

    return v0
.end method

.method public setTintColor(I)V
    .locals 0
    .param p1, "tint"    # I

    .line 155
    iput p1, p0, Landroidx/slice/widget/SliceStyle;->mTintColor:I

    .line 156
    return-void
.end method
