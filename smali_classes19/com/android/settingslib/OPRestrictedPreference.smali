.class public Lcom/android/settingslib/OPRestrictedPreference;
.super Lcom/android/settingslib/TwoTargetPreference;
.source "OPRestrictedPreference.java"


# instance fields
.field private mDivider:Landroid/view/View;

.field mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

.field private mHideDivider:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settingslib/OPRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/OPRestrictedPreference;->setIconSpaceReserved(Z)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 53
    const v0, 0x7f0403b1

    const v1, 0x101008e

    invoke-static {p1, v0, v1}, Landroidx/core/content/res/TypedArrayUtils;->getAttr(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settingslib/OPRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/OPRestrictedPreference;->setIconSpaceReserved(Z)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settingslib/OPRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 49
    invoke-virtual {p0, v0}, Lcom/android/settingslib/OPRestrictedPreference;->setIconSpaceReserved(Z)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/TwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHideDivider:Z

    .line 42
    const v1, 0x7f0d01f6

    invoke-virtual {p0, v1}, Lcom/android/settingslib/OPRestrictedPreference;->setLayoutResource(I)V

    .line 43
    new-instance v1, Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-direct {v1, p1, p0, p2}, Lcom/android/settingslib/RestrictedPreferenceHelper;-><init>(Landroid/content/Context;Landroidx/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    .line 44
    invoke-virtual {p0, v0}, Lcom/android/settingslib/OPRestrictedPreference;->setIconSpaceReserved(Z)V

    .line 45
    return-void
.end method


# virtual methods
.method public checkRestrictionAndSetDisabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "userRestriction"    # Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/settingslib/RestrictedPreferenceHelper;->checkRestrictionAndSetDisabled(Ljava/lang/String;I)V

    .line 108
    return-void
.end method

.method public checkRestrictionAndSetDisabled(Ljava/lang/String;I)V
    .locals 1
    .param p1, "userRestriction"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 111
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-virtual {v0, p1, p2}, Lcom/android/settingslib/RestrictedPreferenceHelper;->checkRestrictionAndSetDisabled(Ljava/lang/String;I)V

    .line 112
    return-void
.end method

.method protected getSecondTargetResId()I
    .locals 1

    .line 65
    const v0, 0x7f0d0293

    return v0
.end method

.method public hideDivider()V
    .locals 2

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHideDivider:Z

    .line 142
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mDivider:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 143
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    :cond_0
    return-void
.end method

.method public isDisabledByAdmin()Z
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-virtual {v0}, Lcom/android/settingslib/RestrictedPreferenceHelper;->isDisabledByAdmin()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToHierarchy(Landroidx/preference/PreferenceManager;)V
    .locals 1
    .param p1, "preferenceManager"    # Landroidx/preference/PreferenceManager;

    .line 102
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-virtual {v0}, Lcom/android/settingslib/RestrictedPreferenceHelper;->onAttachedToHierarchy()V

    .line 103
    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->onAttachedToHierarchy(Landroidx/preference/PreferenceManager;)V

    .line 104
    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroidx/preference/PreferenceViewHolder;

    .line 75
    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 76
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/RestrictedPreferenceHelper;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    .line 77
    const v0, 0x7f0a05a5

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 78
    .local v0, "restrictedIcon":Landroid/view/View;
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/android/settingslib/OPRestrictedPreference;->isDisabledByAdmin()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 81
    :cond_1
    const v2, 0x7f0a0758

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settingslib/OPRestrictedPreference;->mDivider:Landroid/view/View;

    .line 82
    iget-boolean v2, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHideDivider:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settingslib/OPRestrictedPreference;->mDivider:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 83
    const/4 v1, 0x4

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 85
    :cond_2
    iget-object v2, p0, Lcom/android/settingslib/OPRestrictedPreference;->mDivider:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 87
    :goto_1
    return-void
.end method

.method public performClick()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-virtual {v0}, Lcom/android/settingslib/RestrictedPreferenceHelper;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    invoke-super {p0}, Lcom/android/settingslib/TwoTargetPreference;->performClick()V

    .line 94
    :cond_0
    return-void
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V
    .locals 1
    .param p1, "admin"    # Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    .line 124
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/RestrictedPreferenceHelper;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/android/settingslib/OPRestrictedPreference;->notifyChanged()V

    .line 127
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 116
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/OPRestrictedPreference;->isDisabledByAdmin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedPreferenceHelper;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)Z

    .line 118
    return-void

    .line 120
    :cond_0
    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->setEnabled(Z)V

    .line 121
    return-void
.end method

.method protected shouldHideSecondTarget()Z
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/android/settingslib/OPRestrictedPreference;->isDisabledByAdmin()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public showDivider()V
    .locals 2

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHideDivider:Z

    .line 135
    iget-object v1, p0, Lcom/android/settingslib/OPRestrictedPreference;->mDivider:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 136
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 138
    :cond_0
    return-void
.end method

.method public useAdminDisabledSummary(Z)V
    .locals 1
    .param p1, "useSummary"    # Z

    .line 97
    iget-object v0, p0, Lcom/android/settingslib/OPRestrictedPreference;->mHelper:Lcom/android/settingslib/RestrictedPreferenceHelper;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/RestrictedPreferenceHelper;->useAdminDisabledSummary(Z)V

    .line 98
    return-void
.end method
