.class public Lcom/android/settingslib/location/RecentLocationApps;
.super Ljava/lang/Object;
.source "RecentLocationApps.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settingslib/location/RecentLocationApps$Request;
    }
.end annotation


# static fields
.field static final ANDROID_SYSTEM_PACKAGE_NAME:Ljava/lang/String; = "android"
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field static final LOCATION_PERMISSION_OPS:[I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field static final LOCATION_REQUEST_OPS:[I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final RECENT_TIME_INTERVAL_MILLIS:J = 0x5265c00L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDrawableFactory:Landroid/util/IconDrawableFactory;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 45
    const-class v0, Lcom/android/settingslib/location/RecentLocationApps;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settingslib/location/RecentLocationApps;->TAG:Ljava/lang/String;

    .line 53
    const/4 v0, 0x2

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/settingslib/location/RecentLocationApps;->LOCATION_REQUEST_OPS:[I

    .line 58
    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/settingslib/location/RecentLocationApps;->LOCATION_PERMISSION_OPS:[I

    return-void

    :array_0
    .array-data 4
        0x29
        0x2a
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/android/settingslib/location/RecentLocationApps;->mContext:Landroid/content/Context;

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/location/RecentLocationApps;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 70
    invoke-static {p1}, Landroid/util/IconDrawableFactory;->newInstance(Landroid/content/Context;)Landroid/util/IconDrawableFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/location/RecentLocationApps;->mDrawableFactory:Landroid/util/IconDrawableFactory;

    .line 71
    return-void
.end method

.method private getRequestFromOps(JLandroid/app/AppOpsManager$PackageOps;)Lcom/android/settingslib/location/RecentLocationApps$Request;
    .locals 29
    .param p1, "now"    # J
    .param p3, "ops"    # Landroid/app/AppOpsManager$PackageOps;

    .line 170
    move-object/from16 v1, p0

    const-string v2, ", userId "

    invoke-virtual/range {p3 .. p3}, Landroid/app/AppOpsManager$PackageOps;->getPackageName()Ljava/lang/String;

    move-result-object v14

    .line 171
    .local v14, "packageName":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/app/AppOpsManager$PackageOps;->getOps()Ljava/util/List;

    move-result-object v15

    .line 172
    .local v15, "entries":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$OpEntry;>;"
    const/4 v0, 0x0

    .line 173
    .local v0, "highBattery":Z
    const/4 v3, 0x0

    .line 174
    .local v3, "normalBattery":Z
    const-wide/16 v4, 0x0

    .line 176
    .local v4, "locationRequestFinishTime":J
    const-wide/32 v6, 0x5265c00

    sub-long v16, p1, v6

    .line 177
    .local v16, "recentLocationCutoffTime":J
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move/from16 v18, v0

    move/from16 v19, v3

    move-wide/from16 v20, v4

    .end local v0    # "highBattery":Z
    .end local v3    # "normalBattery":Z
    .end local v4    # "locationRequestFinishTime":J
    .local v18, "highBattery":Z
    .local v19, "normalBattery":Z
    .local v20, "locationRequestFinishTime":J
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager$OpEntry;

    .line 178
    .local v0, "entry":Landroid/app/AppOpsManager$OpEntry;
    invoke-virtual {v0}, Landroid/app/AppOpsManager$OpEntry;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/app/AppOpsManager$OpEntry;->getTime()J

    move-result-wide v3

    cmp-long v3, v3, v16

    if-ltz v3, :cond_3

    .line 180
    :cond_0
    invoke-virtual {v0}, Landroid/app/AppOpsManager$OpEntry;->getTime()J

    move-result-wide v3

    invoke-virtual {v0}, Landroid/app/AppOpsManager$OpEntry;->getDuration()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    add-long v20, v3, v7

    .line 181
    invoke-virtual {v0}, Landroid/app/AppOpsManager$OpEntry;->getOp()I

    move-result v3

    const/16 v4, 0x29

    if-eq v3, v4, :cond_2

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_1

    goto :goto_1

    .line 186
    :cond_1
    const/16 v18, 0x1

    .line 187
    goto :goto_1

    .line 183
    :cond_2
    const/4 v3, 0x1

    .line 184
    .end local v19    # "normalBattery":Z
    .restart local v3    # "normalBattery":Z
    move/from16 v19, v3

    .line 192
    .end local v0    # "entry":Landroid/app/AppOpsManager$OpEntry;
    .end local v3    # "normalBattery":Z
    .restart local v19    # "normalBattery":Z
    :cond_3
    :goto_1
    goto :goto_0

    .line 194
    :cond_4
    const/4 v0, 0x0

    if-nez v18, :cond_6

    if-nez v19, :cond_6

    .line 195
    sget-object v2, Lcom/android/settingslib/location/RecentLocationApps;->TAG:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 196
    sget-object v2, Lcom/android/settingslib/location/RecentLocationApps;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " hadn\'t used location within the time interval."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_5
    return-object v0

    .line 202
    :cond_6
    invoke-virtual/range {p3 .. p3}, Landroid/app/AppOpsManager$PackageOps;->getUid()I

    move-result v22

    .line 203
    .local v22, "uid":I
    invoke-static/range {v22 .. v22}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v13

    .line 205
    .local v13, "userId":I
    const/16 v23, 0x0

    .line 207
    .local v23, "request":Lcom/android/settingslib/location/RecentLocationApps$Request;
    :try_start_0
    iget-object v3, v1, Lcom/android/settingslib/location/RecentLocationApps;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v4, 0x80

    invoke-virtual {v3, v14, v4, v13}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-object v11, v3

    .line 209
    .local v11, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v11, :cond_7

    .line 210
    :try_start_1
    sget-object v3, Lcom/android/settingslib/location/RecentLocationApps;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Null application info retrieved for package "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 212
    return-object v0

    .line 228
    .end local v11    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v0

    move v1, v13

    goto :goto_3

    .line 215
    .restart local v11    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_7
    :try_start_2
    new-instance v0, Landroid/os/UserHandle;

    invoke-direct {v0, v13}, Landroid/os/UserHandle;-><init>(I)V

    .line 216
    .local v0, "userHandle":Landroid/os/UserHandle;
    iget-object v3, v1, Lcom/android/settingslib/location/RecentLocationApps;->mDrawableFactory:Landroid/util/IconDrawableFactory;

    invoke-virtual {v3, v11, v13}, Landroid/util/IconDrawableFactory;->getBadgedIcon(Landroid/content/pm/ApplicationInfo;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 217
    .local v7, "icon":Landroid/graphics/drawable/Drawable;
    iget-object v3, v1, Lcom/android/settingslib/location/RecentLocationApps;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v11}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v12, v3

    .line 218
    .local v12, "appLabel":Ljava/lang/CharSequence;
    iget-object v3, v1, Lcom/android/settingslib/location/RecentLocationApps;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v12, v0}, Landroid/content/pm/PackageManager;->getUserBadgedLabel(Ljava/lang/CharSequence;Landroid/os/UserHandle;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 219
    .local v3, "badgedAppLabel":Ljava/lang/CharSequence;
    invoke-interface {v12}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 222
    const/4 v3, 0x0

    move-object/from16 v24, v3

    goto :goto_2

    .line 219
    :cond_8
    move-object/from16 v24, v3

    .line 226
    .end local v3    # "badgedAppLabel":Ljava/lang/CharSequence;
    .local v24, "badgedAppLabel":Ljava/lang/CharSequence;
    :goto_2
    new-instance v25, Lcom/android/settingslib/location/RecentLocationApps$Request;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v26, 0x0

    move-object/from16 v3, v25

    move-object v4, v14

    move/from16 v5, v22

    move-object v6, v0

    move-object v8, v12

    move/from16 v9, v18

    move-object/from16 v10, v24

    move-object/from16 v27, v11

    move-object/from16 v28, v12

    .end local v11    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "appLabel":Ljava/lang/CharSequence;
    .local v27, "appInfo":Landroid/content/pm/ApplicationInfo;
    .local v28, "appLabel":Ljava/lang/CharSequence;
    move-wide/from16 v11, v20

    move v1, v13

    .end local v13    # "userId":I
    .local v1, "userId":I
    move-object/from16 v13, v26

    :try_start_3
    invoke-direct/range {v3 .. v13}, Lcom/android/settingslib/location/RecentLocationApps$Request;-><init>(Ljava/lang/String;ILandroid/os/UserHandle;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;JLcom/android/settingslib/location/RecentLocationApps$1;)V
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    move-object/from16 v23, v25

    .line 230
    .end local v0    # "userHandle":Landroid/os/UserHandle;
    .end local v7    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v24    # "badgedAppLabel":Ljava/lang/CharSequence;
    .end local v27    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v28    # "appLabel":Ljava/lang/CharSequence;
    goto :goto_4

    .line 228
    :catch_1
    move-exception v0

    goto :goto_3

    .end local v1    # "userId":I
    .restart local v13    # "userId":I
    :catch_2
    move-exception v0

    move v1, v13

    .line 229
    .end local v13    # "userId":I
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "userId":I
    :goto_3
    sget-object v3, Lcom/android/settingslib/location/RecentLocationApps;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "package name not found for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_4
    return-object v23
.end method


# virtual methods
.method public getAppList(Z)Ljava/util/List;
    .locals 25
    .param p1, "showSystemApps"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/android/settingslib/location/RecentLocationApps$Request;",
            ">;"
        }
    .end annotation

    .line 79
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/settingslib/location/RecentLocationApps;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 81
    .local v1, "pm":Landroid/content/pm/PackageManager;
    iget-object v2, v0, Lcom/android/settingslib/location/RecentLocationApps;->mContext:Landroid/content/Context;

    .line 82
    const-string v3, "appops"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AppOpsManager;

    .line 83
    .local v2, "aoManager":Landroid/app/AppOpsManager;
    sget-object v3, Lcom/android/settingslib/location/RecentLocationApps;->LOCATION_REQUEST_OPS:[I

    invoke-virtual {v2, v3}, Landroid/app/AppOpsManager;->getPackagesForOps([I)Ljava/util/List;

    move-result-object v3

    .line 85
    .local v3, "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    .line 88
    .local v5, "appOpsCount":I
    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    .local v6, "requests":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/settingslib/location/RecentLocationApps$Request;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 90
    .local v7, "now":J
    iget-object v9, v0, Lcom/android/settingslib/location/RecentLocationApps;->mContext:Landroid/content/Context;

    const-string v10, "user"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/UserManager;

    .line 91
    .local v9, "um":Landroid/os/UserManager;
    invoke-virtual {v9}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v10

    .line 93
    .local v10, "profiles":Ljava/util/List;, "Ljava/util/List<Landroid/os/UserHandle;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v5, :cond_8

    .line 94
    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/AppOpsManager$PackageOps;

    .line 95
    .local v12, "ops":Landroid/app/AppOpsManager$PackageOps;
    invoke-virtual {v12}, Landroid/app/AppOpsManager$PackageOps;->getPackageName()Ljava/lang/String;

    move-result-object v13

    .line 96
    .local v13, "packageName":Ljava/lang/String;
    invoke-virtual {v12}, Landroid/app/AppOpsManager$PackageOps;->getUid()I

    move-result v14

    .line 97
    .local v14, "uid":I
    invoke-static {v14}, Landroid/os/UserHandle;->getUserHandleForUid(I)Landroid/os/UserHandle;

    move-result-object v15

    .line 100
    .local v15, "user":Landroid/os/UserHandle;
    invoke-interface {v10, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 101
    move-object/from16 v23, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move/from16 v24, v5

    goto/16 :goto_5

    .line 104
    :cond_1
    const-string v4, "com.opos.ads"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 105
    move-object/from16 v23, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move/from16 v24, v5

    goto/16 :goto_5

    .line 109
    :cond_2
    const/4 v4, 0x1

    .line 110
    .local v4, "showApp":Z
    if-nez p1, :cond_6

    .line 111
    move-object/from16 v17, v2

    .end local v2    # "aoManager":Landroid/app/AppOpsManager;
    .local v17, "aoManager":Landroid/app/AppOpsManager;
    sget-object v2, Lcom/android/settingslib/location/RecentLocationApps;->LOCATION_PERMISSION_OPS:[I

    move-object/from16 v18, v3

    .end local v3    # "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    .local v18, "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    array-length v3, v2

    move/from16 v19, v4

    const/4 v4, 0x0

    .end local v4    # "showApp":Z
    .local v19, "showApp":Z
    :goto_2
    if-ge v4, v3, :cond_5

    aget v20, v2, v4

    .line 112
    .local v20, "op":I
    move-object/from16 v21, v2

    invoke-static/range {v20 .. v20}, Landroid/app/AppOpsManager;->opToPermission(I)Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "permission":Ljava/lang/String;
    move/from16 v22, v3

    invoke-virtual {v1, v2, v13, v15}, Landroid/content/pm/PackageManager;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)I

    move-result v3

    .line 115
    .local v3, "permissionFlags":I
    move-object/from16 v23, v1

    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    .local v23, "pm":Landroid/content/pm/PackageManager;
    iget-object v1, v0, Lcom/android/settingslib/location/RecentLocationApps;->mContext:Landroid/content/Context;

    move/from16 v24, v5

    .end local v5    # "appOpsCount":I
    .local v24, "appOpsCount":I
    const/4 v5, -0x1

    invoke-static {v1, v2, v5, v14, v13}, Landroid/content/PermissionChecker;->checkPermission(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 118
    and-int/lit16 v1, v3, 0x100

    if-nez v1, :cond_4

    .line 121
    const/4 v4, 0x0

    .line 122
    .end local v19    # "showApp":Z
    .restart local v4    # "showApp":Z
    goto :goto_4

    .line 125
    .end local v4    # "showApp":Z
    .restart local v19    # "showApp":Z
    :cond_3
    and-int/lit16 v1, v3, 0x200

    if-nez v1, :cond_4

    .line 127
    const/4 v4, 0x0

    .line 128
    .end local v19    # "showApp":Z
    .restart local v4    # "showApp":Z
    goto :goto_4

    .line 111
    .end local v2    # "permission":Ljava/lang/String;
    .end local v3    # "permissionFlags":I
    .end local v4    # "showApp":Z
    .end local v20    # "op":I
    .restart local v19    # "showApp":Z
    :cond_4
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v2, v21

    move/from16 v3, v22

    move-object/from16 v1, v23

    move/from16 v5, v24

    goto :goto_2

    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v24    # "appOpsCount":I
    .restart local v1    # "pm":Landroid/content/pm/PackageManager;
    .restart local v5    # "appOpsCount":I
    :cond_5
    move-object/from16 v23, v1

    move/from16 v24, v5

    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "appOpsCount":I
    .restart local v23    # "pm":Landroid/content/pm/PackageManager;
    .restart local v24    # "appOpsCount":I
    goto :goto_3

    .line 110
    .end local v17    # "aoManager":Landroid/app/AppOpsManager;
    .end local v18    # "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    .end local v19    # "showApp":Z
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v24    # "appOpsCount":I
    .restart local v1    # "pm":Landroid/content/pm/PackageManager;
    .local v2, "aoManager":Landroid/app/AppOpsManager;
    .local v3, "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    .restart local v4    # "showApp":Z
    .restart local v5    # "appOpsCount":I
    :cond_6
    move-object/from16 v23, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move/from16 v19, v4

    move/from16 v24, v5

    .line 133
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    .end local v2    # "aoManager":Landroid/app/AppOpsManager;
    .end local v3    # "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    .end local v4    # "showApp":Z
    .end local v5    # "appOpsCount":I
    .restart local v17    # "aoManager":Landroid/app/AppOpsManager;
    .restart local v18    # "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    .restart local v19    # "showApp":Z
    .restart local v23    # "pm":Landroid/content/pm/PackageManager;
    .restart local v24    # "appOpsCount":I
    :goto_3
    move/from16 v4, v19

    .end local v19    # "showApp":Z
    .restart local v4    # "showApp":Z
    :goto_4
    if-eqz v4, :cond_7

    .line 134
    invoke-direct {v0, v7, v8, v12}, Lcom/android/settingslib/location/RecentLocationApps;->getRequestFromOps(JLandroid/app/AppOpsManager$PackageOps;)Lcom/android/settingslib/location/RecentLocationApps$Request;

    move-result-object v1

    .line 135
    .local v1, "request":Lcom/android/settingslib/location/RecentLocationApps$Request;
    if-eqz v1, :cond_7

    .line 136
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    .end local v1    # "request":Lcom/android/settingslib/location/RecentLocationApps$Request;
    .end local v4    # "showApp":Z
    .end local v12    # "ops":Landroid/app/AppOpsManager$PackageOps;
    .end local v13    # "packageName":Ljava/lang/String;
    .end local v14    # "uid":I
    .end local v15    # "user":Landroid/os/UserHandle;
    :cond_7
    :goto_5
    add-int/lit8 v11, v11, 0x1

    move-object/from16 v2, v17

    move-object/from16 v3, v18

    move-object/from16 v1, v23

    move/from16 v5, v24

    goto/16 :goto_1

    .line 140
    .end local v11    # "i":I
    .end local v17    # "aoManager":Landroid/app/AppOpsManager;
    .end local v18    # "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v24    # "appOpsCount":I
    .local v1, "pm":Landroid/content/pm/PackageManager;
    .restart local v2    # "aoManager":Landroid/app/AppOpsManager;
    .restart local v3    # "appOps":Ljava/util/List;, "Ljava/util/List<Landroid/app/AppOpsManager$PackageOps;>;"
    .restart local v5    # "appOpsCount":I
    :cond_8
    return-object v6
.end method

.method public getAppListSorted(Z)Ljava/util/List;
    .locals 2
    .param p1, "showSystemApps"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/android/settingslib/location/RecentLocationApps$Request;",
            ">;"
        }
    .end annotation

    .line 150
    invoke-virtual {p0, p1}, Lcom/android/settingslib/location/RecentLocationApps;->getAppList(Z)Ljava/util/List;

    move-result-object v0

    .line 152
    .local v0, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/android/settingslib/location/RecentLocationApps$Request;>;"
    new-instance v1, Lcom/android/settingslib/location/RecentLocationApps$1;

    invoke-direct {v1, p0}, Lcom/android/settingslib/location/RecentLocationApps$1;-><init>(Lcom/android/settingslib/location/RecentLocationApps;)V

    invoke-static {v1}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 158
    return-object v0
.end method
