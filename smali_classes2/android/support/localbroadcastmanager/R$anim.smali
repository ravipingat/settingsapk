.class public final Landroid/support/localbroadcastmanager/R$anim;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/localbroadcastmanager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "anim"
.end annotation


# static fields
.field public static abc_fade_in:I

.field public static abc_fade_out:I

.field public static abc_grow_fade_in_from_bottom:I

.field public static abc_popup_enter:I

.field public static abc_popup_exit:I

.field public static abc_shrink_fade_out_from_bottom:I

.field public static abc_slide_in_bottom:I

.field public static abc_slide_in_top:I

.field public static abc_slide_out_bottom:I

.field public static abc_slide_out_top:I

.field public static abc_tooltip_enter:I

.field public static abc_tooltip_exit:I

.field public static button_state_list_anim_material:I

.field public static grow_fade_in_from_bottom:I

.field public static ic_checkbox_to_checked_box_inner_merged_animation:I

.field public static ic_checkbox_to_checked_box_outer_merged_animation:I

.field public static ic_checkbox_to_checked_icon_null_animation:I

.field public static ic_checkbox_to_unchecked_box_inner_merged_animation:I

.field public static ic_checkbox_to_unchecked_check_path_merged_animation:I

.field public static ic_checkbox_to_unchecked_icon_null_animation:I

.field public static input_method_extract_enter:I

.field public static input_method_extract_exit:I

.field public static oneplus_control_bottom_navigation_anim_in:I

.field public static oneplus_control_bottom_navigation_anim_out:I

.field public static oneplus_control_text_error_message_anim:I

.field public static op_abc_grow_fade_in_from_bottom:I

.field public static op_abc_shrink_fade_out_from_bottom:I

.field public static op_abc_tooltip_enter:I

.field public static op_abc_tooltip_exit:I

.field public static op_date_picker_fade_in_material:I

.field public static op_date_picker_fade_out_material:I

.field public static op_design_appbar_state_list_animator:I

.field public static op_design_snackbar_in:I

.field public static op_design_snackbar_out:I

.field public static op_major_a_b_dot_01_animation:I

.field public static op_major_a_b_dot_animation:I

.field public static op_major_b_a_dot_01_animation:I

.field public static op_major_b_a_dot_animation:I

.field public static op_major_b_c_dot_01_animation:I

.field public static op_major_b_c_dot_animation:I

.field public static op_major_c_b_dot_01_animation:I

.field public static op_major_c_b_dot_animation:I

.field public static op_minor_a_b_dot_02_animation:I

.field public static op_minor_b_a_dot_02_animation:I

.field public static op_minor_b_c_dot_02_animation:I

.field public static op_minor_c_b_dot_02_animation:I

.field public static popup_bottom_enter:I

.field public static popup_bottom_exit:I

.field public static popup_enter_material:I

.field public static popup_exit_material:I

.field public static progress_indeterminate_horizontal_rect1:I

.field public static progress_indeterminate_horizontal_rect2:I

.field public static progress_indeterminate_material:I

.field public static progress_indeterminate_rotation_material:I

.field public static seekbar_thumb_pressed_to_unpressed_thumb_animation:I

.field public static seekbar_thumb_unpressed_to_pressed_thumb_0_animation:I

.field public static shrink_fade_out_from_bottom:I

.field public static voice_activity_close_exit:I

.field public static voice_activity_open_enter:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    const/4 v0, 0x0

    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_fade_in:I

    .line 13
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_fade_out:I

    .line 14
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_grow_fade_in_from_bottom:I

    .line 15
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_popup_enter:I

    .line 16
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_popup_exit:I

    .line 17
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_shrink_fade_out_from_bottom:I

    .line 18
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_slide_in_bottom:I

    .line 19
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_slide_in_top:I

    .line 20
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_slide_out_bottom:I

    .line 21
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_slide_out_top:I

    .line 22
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_tooltip_enter:I

    .line 23
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->abc_tooltip_exit:I

    .line 24
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->button_state_list_anim_material:I

    .line 25
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->grow_fade_in_from_bottom:I

    .line 26
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->ic_checkbox_to_checked_box_inner_merged_animation:I

    .line 27
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->ic_checkbox_to_checked_box_outer_merged_animation:I

    .line 28
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->ic_checkbox_to_checked_icon_null_animation:I

    .line 29
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->ic_checkbox_to_unchecked_box_inner_merged_animation:I

    .line 30
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->ic_checkbox_to_unchecked_check_path_merged_animation:I

    .line 31
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->ic_checkbox_to_unchecked_icon_null_animation:I

    .line 32
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->input_method_extract_enter:I

    .line 33
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->input_method_extract_exit:I

    .line 34
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->oneplus_control_bottom_navigation_anim_in:I

    .line 35
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->oneplus_control_bottom_navigation_anim_out:I

    .line 36
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->oneplus_control_text_error_message_anim:I

    .line 37
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_abc_grow_fade_in_from_bottom:I

    .line 38
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_abc_shrink_fade_out_from_bottom:I

    .line 39
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_abc_tooltip_enter:I

    .line 40
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_abc_tooltip_exit:I

    .line 41
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_date_picker_fade_in_material:I

    .line 42
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_date_picker_fade_out_material:I

    .line 43
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_design_appbar_state_list_animator:I

    .line 44
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_design_snackbar_in:I

    .line 45
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_design_snackbar_out:I

    .line 46
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_a_b_dot_01_animation:I

    .line 47
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_a_b_dot_animation:I

    .line 48
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_b_a_dot_01_animation:I

    .line 49
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_b_a_dot_animation:I

    .line 50
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_b_c_dot_01_animation:I

    .line 51
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_b_c_dot_animation:I

    .line 52
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_c_b_dot_01_animation:I

    .line 53
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_major_c_b_dot_animation:I

    .line 54
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_minor_a_b_dot_02_animation:I

    .line 55
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_minor_b_a_dot_02_animation:I

    .line 56
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_minor_b_c_dot_02_animation:I

    .line 57
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->op_minor_c_b_dot_02_animation:I

    .line 58
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->popup_bottom_enter:I

    .line 59
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->popup_bottom_exit:I

    .line 60
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->popup_enter_material:I

    .line 61
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->popup_exit_material:I

    .line 62
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->progress_indeterminate_horizontal_rect1:I

    .line 63
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->progress_indeterminate_horizontal_rect2:I

    .line 64
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->progress_indeterminate_material:I

    .line 65
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->progress_indeterminate_rotation_material:I

    .line 66
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->seekbar_thumb_pressed_to_unpressed_thumb_animation:I

    .line 67
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->seekbar_thumb_unpressed_to_pressed_thumb_0_animation:I

    .line 68
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->shrink_fade_out_from_bottom:I

    .line 69
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->voice_activity_close_exit:I

    .line 70
    sput v0, Landroid/support/localbroadcastmanager/R$anim;->voice_activity_open_enter:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
