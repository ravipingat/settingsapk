.class public final Landroid/support/loader/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/loader/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static OPAlertDialogStyle:I

.field public static OPEditTextStyle:I

.field public static OPFloatingActionButtonStyle:I

.field public static OPImageCheckboxStyle:I

.field public static OPListViewStyle:I

.field public static OPProgressBarStyle:I

.field public static OPRectangleFloatingActionButtonStyle:I

.field public static OPSeekBarStyle:I

.field public static OPSwitchFloatingActionButtonStyle:I

.field public static OPTabLayoutStyle:I

.field public static OPToastStyle:I

.field public static actionBarDivider:I

.field public static actionBarItemBackground:I

.field public static actionBarPopupTheme:I

.field public static actionBarSize:I

.field public static actionBarSplitStyle:I

.field public static actionBarStyle:I

.field public static actionBarTabBarStyle:I

.field public static actionBarTabStyle:I

.field public static actionBarTabTextStyle:I

.field public static actionBarTheme:I

.field public static actionBarWidgetTheme:I

.field public static actionButtonStyle:I

.field public static actionDropDownStyle:I

.field public static actionIconSpace:I

.field public static actionLayout:I

.field public static actionMenuTextAppearance:I

.field public static actionMenuTextColor:I

.field public static actionModeBackground:I

.field public static actionModeCloseButtonStyle:I

.field public static actionModeCloseDrawable:I

.field public static actionModeCopyDrawable:I

.field public static actionModeCutDrawable:I

.field public static actionModeFindDrawable:I

.field public static actionModePasteDrawable:I

.field public static actionModePopupWindowStyle:I

.field public static actionModeSelectAllDrawable:I

.field public static actionModeShareDrawable:I

.field public static actionModeSplitBackground:I

.field public static actionModeStyle:I

.field public static actionModeWebSearchDrawable:I

.field public static actionOverflowButtonStyle:I

.field public static actionOverflowMenuStyle:I

.field public static actionProviderClass:I

.field public static actionViewClass:I

.field public static activityChooserViewStyle:I

.field public static alertDialogButtonGroupStyle:I

.field public static alertDialogCenterButtons:I

.field public static alertDialogStyle:I

.field public static alertDialogTheme:I

.field public static allowStacking:I

.field public static alpha:I

.field public static alphabeticModifiers:I

.field public static arrowHeadLength:I

.field public static arrowShaftLength:I

.field public static autoCompleteTextViewStyle:I

.field public static autoSizeMaxTextSize:I

.field public static autoSizeMinTextSize:I

.field public static autoSizePresetSizes:I

.field public static autoSizeStepGranularity:I

.field public static autoSizeTextType:I

.field public static background:I

.field public static backgroundSplit:I

.field public static backgroundStacked:I

.field public static backgroundTint:I

.field public static backgroundTintMode:I

.field public static barLength:I

.field public static borderlessButtonStyle:I

.field public static bottomNavigationStyle:I

.field public static buttonBarButtonStyle:I

.field public static buttonBarNegativeButtonStyle:I

.field public static buttonBarNeutralButtonStyle:I

.field public static buttonBarPositiveButtonStyle:I

.field public static buttonBarStyle:I

.field public static buttonGravity:I

.field public static buttonIconDimen:I

.field public static buttonPanelSideLayout:I

.field public static buttonStyle:I

.field public static buttonStyleSmall:I

.field public static buttonTint:I

.field public static buttonTintMode:I

.field public static calendarTextColor:I

.field public static cardBackgroundColor:I

.field public static cardBackgroundColorMask:I

.field public static cardCornerRadius:I

.field public static cardElevation:I

.field public static cardMaxElevation:I

.field public static cardPreventCornerOverlap:I

.field public static cardUseCompatPadding:I

.field public static checkboxStyle:I

.field public static checkedTextViewStyle:I

.field public static closeIcon:I

.field public static closeItemLayout:I

.field public static collapseContentDescription:I

.field public static collapseIcon:I

.field public static color:I

.field public static colorAccent:I

.field public static colorBackgroundFloating:I

.field public static colorButtonNormal:I

.field public static colorControlActivated:I

.field public static colorControlHighlight:I

.field public static colorControlNormal:I

.field public static colorError:I

.field public static colorPrimary:I

.field public static colorPrimaryDark:I

.field public static colorSecondary:I

.field public static colorSwitchThumbNormal:I

.field public static commitIcon:I

.field public static contentDescription:I

.field public static contentInsetEnd:I

.field public static contentInsetEndWithActions:I

.field public static contentInsetLeft:I

.field public static contentInsetRight:I

.field public static contentInsetStart:I

.field public static contentInsetStartWithNavigation:I

.field public static contentPadding:I

.field public static contentPaddingBottom:I

.field public static contentPaddingLeft:I

.field public static contentPaddingRight:I

.field public static contentPaddingTop:I

.field public static controlBackground:I

.field public static coordinatorLayoutStyle:I

.field public static customNavigationLayout:I

.field public static dateTextAppearance:I

.field public static daySelectorColor:I

.field public static defaultQueryHint:I

.field public static dialogCornerRadius:I

.field public static dialogMode:I

.field public static dialogPreferredPadding:I

.field public static dialogTheme:I

.field public static displayOptions:I

.field public static divider:I

.field public static dividerHorizontal:I

.field public static dividerPadding:I

.field public static dividerVertical:I

.field public static drawableSize:I

.field public static drawerArrowStyle:I

.field public static dropDownListViewStyle:I

.field public static dropdownListPreferredItemHeight:I

.field public static editTextBackground:I

.field public static editTextColor:I

.field public static editTextStyle:I

.field public static elevation:I

.field public static enforceMaterialTheme:I

.field public static enforceTextAppearance:I

.field public static expandActivityOverflowButtonDrawable:I

.field public static firstBaselineToTopHeight:I

.field public static font:I

.field public static fontFamily:I

.field public static fontProviderAuthority:I

.field public static fontProviderCerts:I

.field public static fontProviderFetchStrategy:I

.field public static fontProviderFetchTimeout:I

.field public static fontProviderPackage:I

.field public static fontProviderQuery:I

.field public static fontStyle:I

.field public static fontVariationSettings:I

.field public static fontWeight:I

.field public static gapBetweenBars:I

.field public static goIcon:I

.field public static headerAmPmTextAppearance:I

.field public static headerRemoveIconIfEmpty:I

.field public static headerSelectedTextColor:I

.field public static headerTextColor:I

.field public static height:I

.field public static hideOnContentScroll:I

.field public static hideWheelUntilFocused:I

.field public static homeAsUpIndicator:I

.field public static homeLayout:I

.field public static icon:I

.field public static iconTint:I

.field public static iconTintMode:I

.field public static iconifiedByDefault:I

.field public static imageButtonStyle:I

.field public static indeterminate:I

.field public static indeterminateProgressStyle:I

.field public static initialActivityCount:I

.field public static internalLayout:I

.field public static internalMaxHeight:I

.field public static internalMaxWidth:I

.field public static internalMinHeight:I

.field public static internalMinWidth:I

.field public static interpolatorX:I

.field public static interpolatorY:I

.field public static interpolatorZ:I

.field public static isLightTheme:I

.field public static itemBackground:I

.field public static itemHorizontalTranslationEnabled:I

.field public static itemIconSize:I

.field public static itemIconTint:I

.field public static itemPadding:I

.field public static itemTextAppearanceActive:I

.field public static itemTextAppearanceInactive:I

.field public static itemTextColor:I

.field public static keylines:I

.field public static labelVisibilityMode:I

.field public static lastBaselineToBottomHeight:I

.field public static layout:I

.field public static layout_anchor:I

.field public static layout_anchorGravity:I

.field public static layout_behavior:I

.field public static layout_dodgeInsetEdges:I

.field public static layout_insetEdge:I

.field public static layout_keyline:I

.field public static legacyLayout:I

.field public static lineHeight:I

.field public static listChoiceBackgroundIndicator:I

.field public static listDividerAlertDialog:I

.field public static listItemLayout:I

.field public static listLayout:I

.field public static listMenuViewStyle:I

.field public static listPopupWindowStyle:I

.field public static listPreferredItemHeight:I

.field public static listPreferredItemHeightLarge:I

.field public static listPreferredItemHeightSmall:I

.field public static listPreferredItemPaddingLeft:I

.field public static listPreferredItemPaddingRight:I

.field public static logo:I

.field public static logoDescription:I

.field public static maxActionInlineWidth:I

.field public static maxButtonHeight:I

.field public static measureWithLargestChild:I

.field public static menu:I

.field public static monthTextAppearance:I

.field public static multiChoiceItemLayout:I

.field public static navigationContentDescription:I

.field public static navigationIcon:I

.field public static navigationMode:I

.field public static numberPickerStyle:I

.field public static numbersInnerTextColor:I

.field public static numbersTextColor:I

.field public static numericModifiers:I

.field public static onePlusAccentColor:I

.field public static onePlusActionbarBackground:I

.field public static onePlusActionbarBackgroundColor:I

.field public static onePlusActionbarIconAlpha:I

.field public static onePlusActionbarIconColor:I

.field public static onePlusActionbarLineColor:I

.field public static onePlusActionbarSubtitleAlpha:I

.field public static onePlusActionbarSubtitleColor:I

.field public static onePlusActionbarTitleAlpha:I

.field public static onePlusActionbarTitleColor:I

.field public static onePlusBaseColor:I

.field public static onePlusColorButtonNormal:I

.field public static onePlusColorControlHighlight:I

.field public static onePlusDialogPrimaryColor:I

.field public static onePlusDisabledColor:I

.field public static onePlusDisabledTextColor:I

.field public static onePlusPrimaryColor:I

.field public static onePlusSearchIconColor:I

.field public static onePlusSearchIconInActiveColor:I

.field public static onePlusSecondaryTextColor:I

.field public static onePlusTabIndicatorColor:I

.field public static onePlusTabTextAlpha:I

.field public static onePlusTabTextColor:I

.field public static onePlusTabTextSelectedAlpha:I

.field public static onePlusTabTextSelectedColor:I

.field public static onePlusTextColor:I

.field public static oneplusHeaderLayout:I

.field public static opButtonGravity:I

.field public static opCollapsedTitleGravity:I

.field public static opCollapsedTitleMarginBottom:I

.field public static opCollapsedTitleTextAppearance:I

.field public static opColorError:I

.field public static opContentScrim:I

.field public static opCounterEnabled:I

.field public static opCounterMaxLength:I

.field public static opCounterOverflowTextAppearance:I

.field public static opCounterTextAppearance:I

.field public static opDrawerArrowStyle:I

.field public static opDropdownListItemSelectedColor:I

.field public static opDropdownListPreferredItemHeight:I

.field public static opErrorEnabled:I

.field public static opErrorTextAppearance:I

.field public static opExpanded:I

.field public static opExpandedTitleGravity:I

.field public static opExpandedTitleMargin:I

.field public static opExpandedTitleMarginBottom:I

.field public static opExpandedTitleMarginEnd:I

.field public static opExpandedTitleMarginStart:I

.field public static opExpandedTitleMarginTop:I

.field public static opExpandedTitleTextAppearance:I

.field public static opHintAnimationEnabled:I

.field public static opHintEnabled:I

.field public static opHintTextAppearance:I

.field public static opKeylines:I

.field public static opNavigationButtonStyle:I

.field public static opOnlyDarkTheme:I

.field public static opPanelMenuListWidth:I

.field public static opPasswordToggleContentDescription:I

.field public static opPasswordToggleDrawable:I

.field public static opPasswordToggleEnabled:I

.field public static opPasswordToggleTint:I

.field public static opPasswordToggleTintMode:I

.field public static opPickerColorActivated:I

.field public static opPickerColorUnActivated:I

.field public static opPickerInputLabelUnActivated:I

.field public static opScrimAnimationDuration:I

.field public static opScrimVisibleHeightTrigger:I

.field public static opStatusBarBackground:I

.field public static opStatusBarScrim:I

.field public static opTextAppearancePopupMenuHeader:I

.field public static opTextColorCalendarDayActivated:I

.field public static opTextColorError:I

.field public static opTextColorPrimaryActivated:I

.field public static opTextColorSecondaryActivated:I

.field public static opTitleEnabled:I

.field public static opToolbarId:I

.field public static opUseAvatarIcon:I

.field public static op_allowStacking:I

.field public static op_backgroundTint:I

.field public static op_backgroundTintMode:I

.field public static op_behavior_autoHide:I

.field public static op_behavior_overlapTop:I

.field public static op_borderWidth:I

.field public static op_buttonPanelSideLayout:I

.field public static op_checkBoxPreferenceStyle:I

.field public static op_collapsible:I

.field public static op_dialogPreferenceStyle:I

.field public static op_displayOptions:I

.field public static op_editTextPreferenceStyle:I

.field public static op_elevation:I

.field public static op_fabSize:I

.field public static op_horizontalSpacing:I

.field public static op_image:I

.field public static op_layoutManager:I

.field public static op_layout_anchor:I

.field public static op_layout_anchorGravity:I

.field public static op_layout_behavior:I

.field public static op_layout_collapseMode:I

.field public static op_layout_collapseParallaxMultiplier:I

.field public static op_layout_dodgeInsetEdges:I

.field public static op_layout_insetEdge:I

.field public static op_layout_keyline:I

.field public static op_layout_scrollFlags:I

.field public static op_layout_scrollInterpolator:I

.field public static op_listItemLayout:I

.field public static op_listLayout:I

.field public static op_multiChoiceItemLayout:I

.field public static op_navigationMode:I

.field public static op_preferenceActivityStyle:I

.field public static op_preferenceCategoryStyle:I

.field public static op_preferenceFragmentListStyle:I

.field public static op_preferenceFragmentStyle:I

.field public static op_preferenceHeaderPanelStyle:I

.field public static op_preferenceListStyle:I

.field public static op_preferencePanelStyle:I

.field public static op_preferenceScreenStyle:I

.field public static op_preferenceStyle:I

.field public static op_pressedTranslationZ:I

.field public static op_reverseLayout:I

.field public static op_ringtonePreferenceStyle:I

.field public static op_rippleColor:I

.field public static op_seekBarPreferenceStyle:I

.field public static op_singleChoiceItemLayout:I

.field public static op_spanCount:I

.field public static op_stackFromEnd:I

.field public static op_state_collapsed:I

.field public static op_state_collapsible:I

.field public static op_switchPreferenceStyle:I

.field public static op_tabBackground:I

.field public static op_tabContentStart:I

.field public static op_tabGravity:I

.field public static op_tabIndicatorColor:I

.field public static op_tabIndicatorHeight:I

.field public static op_tabMaxWidth:I

.field public static op_tabMinWidth:I

.field public static op_tabMode:I

.field public static op_tabPadding:I

.field public static op_tabPaddingBottom:I

.field public static op_tabPaddingEnd:I

.field public static op_tabPaddingStart:I

.field public static op_tabPaddingTop:I

.field public static op_tabSelectedTextColor:I

.field public static op_tabTextAppearance:I

.field public static op_tabTextColor:I

.field public static op_tint_color:I

.field public static opsolidColor:I

.field public static overlapAnchor:I

.field public static paddingBottomNoButtons:I

.field public static paddingEnd:I

.field public static paddingLeftWithNavIcon:I

.field public static paddingStart:I

.field public static paddingTopNoTitle:I

.field public static panelBackground:I

.field public static panelMenuListTheme:I

.field public static panelMenuListWidth:I

.field public static popupMenuStyle:I

.field public static popupTheme:I

.field public static popupWindowStyle:I

.field public static preserveIconSpacing:I

.field public static progressBarPadding:I

.field public static progressBarStyle:I

.field public static queryBackground:I

.field public static queryHint:I

.field public static radioButtonStyle:I

.field public static ratingBarStyle:I

.field public static ratingBarStyleIndicator:I

.field public static ratingBarStyleSmall:I

.field public static realPaddingBottom:I

.field public static realTitleMarginBottom:I

.field public static searchHintIcon:I

.field public static searchIcon:I

.field public static searchViewStyle:I

.field public static seekBarStyle:I

.field public static selectableItemBackground:I

.field public static selectableItemBackgroundBorderless:I

.field public static selectionDivider:I

.field public static selectionDividerHeight:I

.field public static selectionDividerWidth:I

.field public static selectionDividersDistance:I

.field public static selectionOtherNumberColor:I

.field public static showAsAction:I

.field public static showDividers:I

.field public static showText:I

.field public static showTitle:I

.field public static singleChoiceItemLayout:I

.field public static spinBars:I

.field public static spinnerDropDownItemStyle:I

.field public static spinnerStyle:I

.field public static splitTrack:I

.field public static srcCompat:I

.field public static state_above_anchor:I

.field public static state_indeterminate:I

.field public static statusBarBackground:I

.field public static subMenuArrow:I

.field public static subTitleMarginBottom:I

.field public static submitBackground:I

.field public static subtitle:I

.field public static subtitleTextAppearance:I

.field public static subtitleTextColor:I

.field public static subtitleTextStyle:I

.field public static suggestionRowLayout:I

.field public static switchMinWidth:I

.field public static switchPadding:I

.field public static switchStyle:I

.field public static switchTextAppearance:I

.field public static textAllCaps:I

.field public static textAppearanceLargePopupMenu:I

.field public static textAppearanceListItem:I

.field public static textAppearanceListItemSecondary:I

.field public static textAppearanceListItemSmall:I

.field public static textAppearanceOPNumberPickerUnit:I

.field public static textAppearancePopupMenuHeader:I

.field public static textAppearanceSearchResultSubtitle:I

.field public static textAppearanceSearchResultTitle:I

.field public static textAppearanceSmallPopupMenu:I

.field public static textColorAlertDialogListItem:I

.field public static textColorSearchUrl:I

.field public static theme:I

.field public static thickness:I

.field public static threeState:I

.field public static thumbTextPadding:I

.field public static thumbTint:I

.field public static thumbTintMode:I

.field public static tickMark:I

.field public static tickMarkTint:I

.field public static tickMarkTintMode:I

.field public static timePickerStyle:I

.field public static tint:I

.field public static tintMode:I

.field public static title:I

.field public static titleMargin:I

.field public static titleMarginBottom:I

.field public static titleMarginEnd:I

.field public static titleMarginStart:I

.field public static titleMarginTop:I

.field public static titleMargins:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static titleTextAppearance:I

.field public static titleTextColor:I

.field public static titleTextStyle:I

.field public static toolbarNavigationButtonStyle:I

.field public static toolbarStyle:I

.field public static tooltipForegroundColor:I

.field public static tooltipFrameBackground:I

.field public static tooltipText:I

.field public static track:I

.field public static trackTint:I

.field public static trackTintMode:I

.field public static ttcIndex:I

.field public static useDisabledAlpha:I

.field public static viewInflaterClass:I

.field public static virtualButtonPressedDrawable:I

.field public static voiceIcon:I

.field public static weekDayTextAppearance:I

.field public static windowActionBar:I

.field public static windowActionBarOverlay:I

.field public static windowActionModeOverlay:I

.field public static windowFixedHeightMajor:I

.field public static windowFixedHeightMinor:I

.field public static windowFixedWidthMajor:I

.field public static windowFixedWidthMinor:I

.field public static windowMinWidthMajor:I

.field public static windowMinWidthMinor:I

.field public static windowNoTitle:I

.field public static yearListItemActivatedTextAppearance:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 80
    const/4 v0, 0x0

    sput v0, Landroid/support/loader/R$attr;->OPAlertDialogStyle:I

    .line 88
    sput v0, Landroid/support/loader/R$attr;->OPEditTextStyle:I

    .line 96
    sput v0, Landroid/support/loader/R$attr;->OPFloatingActionButtonStyle:I

    .line 104
    sput v0, Landroid/support/loader/R$attr;->OPImageCheckboxStyle:I

    .line 112
    sput v0, Landroid/support/loader/R$attr;->OPListViewStyle:I

    .line 119
    sput v0, Landroid/support/loader/R$attr;->OPProgressBarStyle:I

    .line 126
    sput v0, Landroid/support/loader/R$attr;->OPRectangleFloatingActionButtonStyle:I

    .line 133
    sput v0, Landroid/support/loader/R$attr;->OPSeekBarStyle:I

    .line 141
    sput v0, Landroid/support/loader/R$attr;->OPSwitchFloatingActionButtonStyle:I

    .line 149
    sput v0, Landroid/support/loader/R$attr;->OPTabLayoutStyle:I

    .line 157
    sput v0, Landroid/support/loader/R$attr;->OPToastStyle:I

    .line 165
    sput v0, Landroid/support/loader/R$attr;->actionBarDivider:I

    .line 173
    sput v0, Landroid/support/loader/R$attr;->actionBarItemBackground:I

    .line 182
    sput v0, Landroid/support/loader/R$attr;->actionBarPopupTheme:I

    .line 200
    sput v0, Landroid/support/loader/R$attr;->actionBarSize:I

    .line 211
    sput v0, Landroid/support/loader/R$attr;->actionBarSplitStyle:I

    .line 219
    sput v0, Landroid/support/loader/R$attr;->actionBarStyle:I

    .line 226
    sput v0, Landroid/support/loader/R$attr;->actionBarTabBarStyle:I

    .line 234
    sput v0, Landroid/support/loader/R$attr;->actionBarTabStyle:I

    .line 241
    sput v0, Landroid/support/loader/R$attr;->actionBarTabTextStyle:I

    .line 251
    sput v0, Landroid/support/loader/R$attr;->actionBarTheme:I

    .line 265
    sput v0, Landroid/support/loader/R$attr;->actionBarWidgetTheme:I

    .line 273
    sput v0, Landroid/support/loader/R$attr;->actionButtonStyle:I

    .line 281
    sput v0, Landroid/support/loader/R$attr;->actionDropDownStyle:I

    .line 289
    sput v0, Landroid/support/loader/R$attr;->actionIconSpace:I

    .line 299
    sput v0, Landroid/support/loader/R$attr;->actionLayout:I

    .line 308
    sput v0, Landroid/support/loader/R$attr;->actionMenuTextAppearance:I

    .line 319
    sput v0, Landroid/support/loader/R$attr;->actionMenuTextColor:I

    .line 327
    sput v0, Landroid/support/loader/R$attr;->actionModeBackground:I

    .line 334
    sput v0, Landroid/support/loader/R$attr;->actionModeCloseButtonStyle:I

    .line 342
    sput v0, Landroid/support/loader/R$attr;->actionModeCloseDrawable:I

    .line 350
    sput v0, Landroid/support/loader/R$attr;->actionModeCopyDrawable:I

    .line 358
    sput v0, Landroid/support/loader/R$attr;->actionModeCutDrawable:I

    .line 366
    sput v0, Landroid/support/loader/R$attr;->actionModeFindDrawable:I

    .line 374
    sput v0, Landroid/support/loader/R$attr;->actionModePasteDrawable:I

    .line 382
    sput v0, Landroid/support/loader/R$attr;->actionModePopupWindowStyle:I

    .line 390
    sput v0, Landroid/support/loader/R$attr;->actionModeSelectAllDrawable:I

    .line 398
    sput v0, Landroid/support/loader/R$attr;->actionModeShareDrawable:I

    .line 406
    sput v0, Landroid/support/loader/R$attr;->actionModeSplitBackground:I

    .line 413
    sput v0, Landroid/support/loader/R$attr;->actionModeStyle:I

    .line 421
    sput v0, Landroid/support/loader/R$attr;->actionModeWebSearchDrawable:I

    .line 428
    sput v0, Landroid/support/loader/R$attr;->actionOverflowButtonStyle:I

    .line 435
    sput v0, Landroid/support/loader/R$attr;->actionOverflowMenuStyle:I

    .line 444
    sput v0, Landroid/support/loader/R$attr;->actionProviderClass:I

    .line 452
    sput v0, Landroid/support/loader/R$attr;->actionViewClass:I

    .line 460
    sput v0, Landroid/support/loader/R$attr;->activityChooserViewStyle:I

    .line 467
    sput v0, Landroid/support/loader/R$attr;->alertDialogButtonGroupStyle:I

    .line 472
    sput v0, Landroid/support/loader/R$attr;->alertDialogCenterButtons:I

    .line 479
    sput v0, Landroid/support/loader/R$attr;->alertDialogStyle:I

    .line 487
    sput v0, Landroid/support/loader/R$attr;->alertDialogTheme:I

    .line 494
    sput v0, Landroid/support/loader/R$attr;->allowStacking:I

    .line 498
    sput v0, Landroid/support/loader/R$attr;->alpha:I

    .line 516
    sput v0, Landroid/support/loader/R$attr;->alphabeticModifiers:I

    .line 525
    sput v0, Landroid/support/loader/R$attr;->arrowHeadLength:I

    .line 534
    sput v0, Landroid/support/loader/R$attr;->arrowShaftLength:I

    .line 542
    sput v0, Landroid/support/loader/R$attr;->autoCompleteTextViewStyle:I

    .line 551
    sput v0, Landroid/support/loader/R$attr;->autoSizeMaxTextSize:I

    .line 560
    sput v0, Landroid/support/loader/R$attr;->autoSizeMinTextSize:I

    .line 570
    sput v0, Landroid/support/loader/R$attr;->autoSizePresetSizes:I

    .line 581
    sput v0, Landroid/support/loader/R$attr;->autoSizeStepGranularity:I

    .line 596
    sput v0, Landroid/support/loader/R$attr;->autoSizeTextType:I

    .line 604
    sput v0, Landroid/support/loader/R$attr;->background:I

    .line 615
    sput v0, Landroid/support/loader/R$attr;->backgroundSplit:I

    .line 626
    sput v0, Landroid/support/loader/R$attr;->backgroundStacked:I

    .line 633
    sput v0, Landroid/support/loader/R$attr;->backgroundTint:I

    .line 655
    sput v0, Landroid/support/loader/R$attr;->backgroundTintMode:I

    .line 664
    sput v0, Landroid/support/loader/R$attr;->barLength:I

    .line 672
    sput v0, Landroid/support/loader/R$attr;->borderlessButtonStyle:I

    .line 680
    sput v0, Landroid/support/loader/R$attr;->bottomNavigationStyle:I

    .line 688
    sput v0, Landroid/support/loader/R$attr;->buttonBarButtonStyle:I

    .line 696
    sput v0, Landroid/support/loader/R$attr;->buttonBarNegativeButtonStyle:I

    .line 704
    sput v0, Landroid/support/loader/R$attr;->buttonBarNeutralButtonStyle:I

    .line 712
    sput v0, Landroid/support/loader/R$attr;->buttonBarPositiveButtonStyle:I

    .line 720
    sput v0, Landroid/support/loader/R$attr;->buttonBarStyle:I

    .line 732
    sput v0, Landroid/support/loader/R$attr;->buttonGravity:I

    .line 740
    sput v0, Landroid/support/loader/R$attr;->buttonIconDimen:I

    .line 747
    sput v0, Landroid/support/loader/R$attr;->buttonPanelSideLayout:I

    .line 755
    sput v0, Landroid/support/loader/R$attr;->buttonStyle:I

    .line 763
    sput v0, Landroid/support/loader/R$attr;->buttonStyleSmall:I

    .line 770
    sput v0, Landroid/support/loader/R$attr;->buttonTint:I

    .line 792
    sput v0, Landroid/support/loader/R$attr;->buttonTintMode:I

    .line 799
    sput v0, Landroid/support/loader/R$attr;->calendarTextColor:I

    .line 806
    sput v0, Landroid/support/loader/R$attr;->cardBackgroundColor:I

    .line 813
    sput v0, Landroid/support/loader/R$attr;->cardBackgroundColorMask:I

    .line 822
    sput v0, Landroid/support/loader/R$attr;->cardCornerRadius:I

    .line 831
    sput v0, Landroid/support/loader/R$attr;->cardElevation:I

    .line 840
    sput v0, Landroid/support/loader/R$attr;->cardMaxElevation:I

    .line 846
    sput v0, Landroid/support/loader/R$attr;->cardPreventCornerOverlap:I

    .line 852
    sput v0, Landroid/support/loader/R$attr;->cardUseCompatPadding:I

    .line 860
    sput v0, Landroid/support/loader/R$attr;->checkboxStyle:I

    .line 868
    sput v0, Landroid/support/loader/R$attr;->checkedTextViewStyle:I

    .line 876
    sput v0, Landroid/support/loader/R$attr;->closeIcon:I

    .line 884
    sput v0, Landroid/support/loader/R$attr;->closeItemLayout:I

    .line 890
    sput v0, Landroid/support/loader/R$attr;->collapseContentDescription:I

    .line 898
    sput v0, Landroid/support/loader/R$attr;->collapseIcon:I

    .line 905
    sput v0, Landroid/support/loader/R$attr;->color:I

    .line 913
    sput v0, Landroid/support/loader/R$attr;->colorAccent:I

    .line 920
    sput v0, Landroid/support/loader/R$attr;->colorBackgroundFloating:I

    .line 927
    sput v0, Landroid/support/loader/R$attr;->colorButtonNormal:I

    .line 934
    sput v0, Landroid/support/loader/R$attr;->colorControlActivated:I

    .line 941
    sput v0, Landroid/support/loader/R$attr;->colorControlHighlight:I

    .line 948
    sput v0, Landroid/support/loader/R$attr;->colorControlNormal:I

    .line 958
    sput v0, Landroid/support/loader/R$attr;->colorError:I

    .line 966
    sput v0, Landroid/support/loader/R$attr;->colorPrimary:I

    .line 974
    sput v0, Landroid/support/loader/R$attr;->colorPrimaryDark:I

    .line 981
    sput v0, Landroid/support/loader/R$attr;->colorSecondary:I

    .line 988
    sput v0, Landroid/support/loader/R$attr;->colorSwitchThumbNormal:I

    .line 996
    sput v0, Landroid/support/loader/R$attr;->commitIcon:I

    .line 1002
    sput v0, Landroid/support/loader/R$attr;->contentDescription:I

    .line 1012
    sput v0, Landroid/support/loader/R$attr;->contentInsetEnd:I

    .line 1020
    sput v0, Landroid/support/loader/R$attr;->contentInsetEndWithActions:I

    .line 1030
    sput v0, Landroid/support/loader/R$attr;->contentInsetLeft:I

    .line 1040
    sput v0, Landroid/support/loader/R$attr;->contentInsetRight:I

    .line 1048
    sput v0, Landroid/support/loader/R$attr;->contentInsetStart:I

    .line 1056
    sput v0, Landroid/support/loader/R$attr;->contentInsetStartWithNavigation:I

    .line 1065
    sput v0, Landroid/support/loader/R$attr;->contentPadding:I

    .line 1074
    sput v0, Landroid/support/loader/R$attr;->contentPaddingBottom:I

    .line 1083
    sput v0, Landroid/support/loader/R$attr;->contentPaddingLeft:I

    .line 1092
    sput v0, Landroid/support/loader/R$attr;->contentPaddingRight:I

    .line 1101
    sput v0, Landroid/support/loader/R$attr;->contentPaddingTop:I

    .line 1109
    sput v0, Landroid/support/loader/R$attr;->controlBackground:I

    .line 1116
    sput v0, Landroid/support/loader/R$attr;->coordinatorLayoutStyle:I

    .line 1124
    sput v0, Landroid/support/loader/R$attr;->customNavigationLayout:I

    .line 1132
    sput v0, Landroid/support/loader/R$attr;->dateTextAppearance:I

    .line 1138
    sput v0, Landroid/support/loader/R$attr;->daySelectorColor:I

    .line 1146
    sput v0, Landroid/support/loader/R$attr;->defaultQueryHint:I

    .line 1155
    sput v0, Landroid/support/loader/R$attr;->dialogCornerRadius:I

    .line 1163
    sput v0, Landroid/support/loader/R$attr;->dialogMode:I

    .line 1172
    sput v0, Landroid/support/loader/R$attr;->dialogPreferredPadding:I

    .line 1180
    sput v0, Landroid/support/loader/R$attr;->dialogTheme:I

    .line 1198
    sput v0, Landroid/support/loader/R$attr;->displayOptions:I

    .line 1206
    sput v0, Landroid/support/loader/R$attr;->divider:I

    .line 1214
    sput v0, Landroid/support/loader/R$attr;->dividerHorizontal:I

    .line 1223
    sput v0, Landroid/support/loader/R$attr;->dividerPadding:I

    .line 1231
    sput v0, Landroid/support/loader/R$attr;->dividerVertical:I

    .line 1240
    sput v0, Landroid/support/loader/R$attr;->drawableSize:I

    .line 1247
    sput v0, Landroid/support/loader/R$attr;->drawerArrowStyle:I

    .line 1255
    sput v0, Landroid/support/loader/R$attr;->dropDownListViewStyle:I

    .line 1264
    sput v0, Landroid/support/loader/R$attr;->dropdownListPreferredItemHeight:I

    .line 1272
    sput v0, Landroid/support/loader/R$attr;->editTextBackground:I

    .line 1283
    sput v0, Landroid/support/loader/R$attr;->editTextColor:I

    .line 1291
    sput v0, Landroid/support/loader/R$attr;->editTextStyle:I

    .line 1300
    sput v0, Landroid/support/loader/R$attr;->elevation:I

    .line 1312
    sput v0, Landroid/support/loader/R$attr;->enforceMaterialTheme:I

    .line 1319
    sput v0, Landroid/support/loader/R$attr;->enforceTextAppearance:I

    .line 1331
    sput v0, Landroid/support/loader/R$attr;->expandActivityOverflowButtonDrawable:I

    .line 1341
    sput v0, Landroid/support/loader/R$attr;->firstBaselineToTopHeight:I

    .line 1350
    sput v0, Landroid/support/loader/R$attr;->font:I

    .line 1356
    sput v0, Landroid/support/loader/R$attr;->fontFamily:I

    .line 1362
    sput v0, Landroid/support/loader/R$attr;->fontProviderAuthority:I

    .line 1374
    sput v0, Landroid/support/loader/R$attr;->fontProviderCerts:I

    .line 1401
    sput v0, Landroid/support/loader/R$attr;->fontProviderFetchStrategy:I

    .line 1415
    sput v0, Landroid/support/loader/R$attr;->fontProviderFetchTimeout:I

    .line 1422
    sput v0, Landroid/support/loader/R$attr;->fontProviderPackage:I

    .line 1429
    sput v0, Landroid/support/loader/R$attr;->fontProviderQuery:I

    .line 1444
    sput v0, Landroid/support/loader/R$attr;->fontStyle:I

    .line 1453
    sput v0, Landroid/support/loader/R$attr;->fontVariationSettings:I

    .line 1462
    sput v0, Landroid/support/loader/R$attr;->fontWeight:I

    .line 1471
    sput v0, Landroid/support/loader/R$attr;->gapBetweenBars:I

    .line 1479
    sput v0, Landroid/support/loader/R$attr;->goIcon:I

    .line 1486
    sput v0, Landroid/support/loader/R$attr;->headerAmPmTextAppearance:I

    .line 1493
    sput v0, Landroid/support/loader/R$attr;->headerRemoveIconIfEmpty:I

    .line 1499
    sput v0, Landroid/support/loader/R$attr;->headerSelectedTextColor:I

    .line 1509
    sput v0, Landroid/support/loader/R$attr;->headerTextColor:I

    .line 1517
    sput v0, Landroid/support/loader/R$attr;->height:I

    .line 1523
    sput v0, Landroid/support/loader/R$attr;->hideOnContentScroll:I

    .line 1529
    sput v0, Landroid/support/loader/R$attr;->hideWheelUntilFocused:I

    .line 1537
    sput v0, Landroid/support/loader/R$attr;->homeAsUpIndicator:I

    .line 1545
    sput v0, Landroid/support/loader/R$attr;->homeLayout:I

    .line 1553
    sput v0, Landroid/support/loader/R$attr;->icon:I

    .line 1560
    sput v0, Landroid/support/loader/R$attr;->iconTint:I

    .line 1582
    sput v0, Landroid/support/loader/R$attr;->iconTintMode:I

    .line 1589
    sput v0, Landroid/support/loader/R$attr;->iconifiedByDefault:I

    .line 1597
    sput v0, Landroid/support/loader/R$attr;->imageButtonStyle:I

    .line 1602
    sput v0, Landroid/support/loader/R$attr;->indeterminate:I

    .line 1610
    sput v0, Landroid/support/loader/R$attr;->indeterminateProgressStyle:I

    .line 1616
    sput v0, Landroid/support/loader/R$attr;->initialActivityCount:I

    .line 1624
    sput v0, Landroid/support/loader/R$attr;->internalLayout:I

    .line 1633
    sput v0, Landroid/support/loader/R$attr;->internalMaxHeight:I

    .line 1642
    sput v0, Landroid/support/loader/R$attr;->internalMaxWidth:I

    .line 1651
    sput v0, Landroid/support/loader/R$attr;->internalMinHeight:I

    .line 1660
    sput v0, Landroid/support/loader/R$attr;->internalMinWidth:I

    .line 1667
    sput v0, Landroid/support/loader/R$attr;->interpolatorX:I

    .line 1674
    sput v0, Landroid/support/loader/R$attr;->interpolatorY:I

    .line 1681
    sput v0, Landroid/support/loader/R$attr;->interpolatorZ:I

    .line 1686
    sput v0, Landroid/support/loader/R$attr;->isLightTheme:I

    .line 1695
    sput v0, Landroid/support/loader/R$attr;->itemBackground:I

    .line 1701
    sput v0, Landroid/support/loader/R$attr;->itemHorizontalTranslationEnabled:I

    .line 1710
    sput v0, Landroid/support/loader/R$attr;->itemIconSize:I

    .line 1718
    sput v0, Landroid/support/loader/R$attr;->itemIconTint:I

    .line 1728
    sput v0, Landroid/support/loader/R$attr;->itemPadding:I

    .line 1738
    sput v0, Landroid/support/loader/R$attr;->itemTextAppearanceActive:I

    .line 1749
    sput v0, Landroid/support/loader/R$attr;->itemTextAppearanceInactive:I

    .line 1758
    sput v0, Landroid/support/loader/R$attr;->itemTextColor:I

    .line 1770
    sput v0, Landroid/support/loader/R$attr;->keylines:I

    .line 1788
    sput v0, Landroid/support/loader/R$attr;->labelVisibilityMode:I

    .line 1798
    sput v0, Landroid/support/loader/R$attr;->lastBaselineToBottomHeight:I

    .line 1806
    sput v0, Landroid/support/loader/R$attr;->layout:I

    .line 1814
    sput v0, Landroid/support/loader/R$attr;->layout_anchor:I

    .line 1846
    sput v0, Landroid/support/loader/R$attr;->layout_anchorGravity:I

    .line 1853
    sput v0, Landroid/support/loader/R$attr;->layout_behavior:I

    .line 1872
    sput v0, Landroid/support/loader/R$attr;->layout_dodgeInsetEdges:I

    .line 1891
    sput v0, Landroid/support/loader/R$attr;->layout_insetEdge:I

    .line 1898
    sput v0, Landroid/support/loader/R$attr;->layout_keyline:I

    .line 1906
    sput v0, Landroid/support/loader/R$attr;->legacyLayout:I

    .line 1916
    sput v0, Landroid/support/loader/R$attr;->lineHeight:I

    .line 1924
    sput v0, Landroid/support/loader/R$attr;->listChoiceBackgroundIndicator:I

    .line 1932
    sput v0, Landroid/support/loader/R$attr;->listDividerAlertDialog:I

    .line 1939
    sput v0, Landroid/support/loader/R$attr;->listItemLayout:I

    .line 1946
    sput v0, Landroid/support/loader/R$attr;->listLayout:I

    .line 1954
    sput v0, Landroid/support/loader/R$attr;->listMenuViewStyle:I

    .line 1961
    sput v0, Landroid/support/loader/R$attr;->listPopupWindowStyle:I

    .line 1970
    sput v0, Landroid/support/loader/R$attr;->listPreferredItemHeight:I

    .line 1979
    sput v0, Landroid/support/loader/R$attr;->listPreferredItemHeightLarge:I

    .line 1988
    sput v0, Landroid/support/loader/R$attr;->listPreferredItemHeightSmall:I

    .line 1997
    sput v0, Landroid/support/loader/R$attr;->listPreferredItemPaddingLeft:I

    .line 2006
    sput v0, Landroid/support/loader/R$attr;->listPreferredItemPaddingRight:I

    .line 2014
    sput v0, Landroid/support/loader/R$attr;->logo:I

    .line 2021
    sput v0, Landroid/support/loader/R$attr;->logoDescription:I

    .line 2029
    sput v0, Landroid/support/loader/R$attr;->maxActionInlineWidth:I

    .line 2037
    sput v0, Landroid/support/loader/R$attr;->maxButtonHeight:I

    .line 2045
    sput v0, Landroid/support/loader/R$attr;->measureWithLargestChild:I

    .line 2054
    sput v0, Landroid/support/loader/R$attr;->menu:I

    .line 2062
    sput v0, Landroid/support/loader/R$attr;->monthTextAppearance:I

    .line 2069
    sput v0, Landroid/support/loader/R$attr;->multiChoiceItemLayout:I

    .line 2076
    sput v0, Landroid/support/loader/R$attr;->navigationContentDescription:I

    .line 2085
    sput v0, Landroid/support/loader/R$attr;->navigationIcon:I

    .line 2099
    sput v0, Landroid/support/loader/R$attr;->navigationMode:I

    .line 2107
    sput v0, Landroid/support/loader/R$attr;->numberPickerStyle:I

    .line 2116
    sput v0, Landroid/support/loader/R$attr;->numbersInnerTextColor:I

    .line 2125
    sput v0, Landroid/support/loader/R$attr;->numbersTextColor:I

    .line 2143
    sput v0, Landroid/support/loader/R$attr;->numericModifiers:I

    .line 2153
    sput v0, Landroid/support/loader/R$attr;->onePlusAccentColor:I

    .line 2160
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarBackground:I

    .line 2170
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarBackgroundColor:I

    .line 2174
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarIconAlpha:I

    .line 2184
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarIconColor:I

    .line 2194
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarLineColor:I

    .line 2198
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarSubtitleAlpha:I

    .line 2208
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarSubtitleColor:I

    .line 2212
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarTitleAlpha:I

    .line 2223
    sput v0, Landroid/support/loader/R$attr;->onePlusActionbarTitleColor:I

    .line 2234
    sput v0, Landroid/support/loader/R$attr;->onePlusBaseColor:I

    .line 2244
    sput v0, Landroid/support/loader/R$attr;->onePlusColorButtonNormal:I

    .line 2251
    sput v0, Landroid/support/loader/R$attr;->onePlusColorControlHighlight:I

    .line 2261
    sput v0, Landroid/support/loader/R$attr;->onePlusDialogPrimaryColor:I

    .line 2271
    sput v0, Landroid/support/loader/R$attr;->onePlusDisabledColor:I

    .line 2281
    sput v0, Landroid/support/loader/R$attr;->onePlusDisabledTextColor:I

    .line 2291
    sput v0, Landroid/support/loader/R$attr;->onePlusPrimaryColor:I

    .line 2301
    sput v0, Landroid/support/loader/R$attr;->onePlusSearchIconColor:I

    .line 2311
    sput v0, Landroid/support/loader/R$attr;->onePlusSearchIconInActiveColor:I

    .line 2321
    sput v0, Landroid/support/loader/R$attr;->onePlusSecondaryTextColor:I

    .line 2331
    sput v0, Landroid/support/loader/R$attr;->onePlusTabIndicatorColor:I

    .line 2335
    sput v0, Landroid/support/loader/R$attr;->onePlusTabTextAlpha:I

    .line 2345
    sput v0, Landroid/support/loader/R$attr;->onePlusTabTextColor:I

    .line 2349
    sput v0, Landroid/support/loader/R$attr;->onePlusTabTextSelectedAlpha:I

    .line 2359
    sput v0, Landroid/support/loader/R$attr;->onePlusTabTextSelectedColor:I

    .line 2369
    sput v0, Landroid/support/loader/R$attr;->onePlusTextColor:I

    .line 2377
    sput v0, Landroid/support/loader/R$attr;->oneplusHeaderLayout:I

    .line 2389
    sput v0, Landroid/support/loader/R$attr;->opButtonGravity:I

    .line 2410
    sput v0, Landroid/support/loader/R$attr;->opCollapsedTitleGravity:I

    .line 2418
    sput v0, Landroid/support/loader/R$attr;->opCollapsedTitleMarginBottom:I

    .line 2427
    sput v0, Landroid/support/loader/R$attr;->opCollapsedTitleTextAppearance:I

    .line 2437
    sput v0, Landroid/support/loader/R$attr;->opColorError:I

    .line 2445
    sput v0, Landroid/support/loader/R$attr;->opContentScrim:I

    .line 2451
    sput v0, Landroid/support/loader/R$attr;->opCounterEnabled:I

    .line 2456
    sput v0, Landroid/support/loader/R$attr;->opCounterMaxLength:I

    .line 2464
    sput v0, Landroid/support/loader/R$attr;->opCounterOverflowTextAppearance:I

    .line 2472
    sput v0, Landroid/support/loader/R$attr;->opCounterTextAppearance:I

    .line 2479
    sput v0, Landroid/support/loader/R$attr;->opDrawerArrowStyle:I

    .line 2485
    sput v0, Landroid/support/loader/R$attr;->opDropdownListItemSelectedColor:I

    .line 2493
    sput v0, Landroid/support/loader/R$attr;->opDropdownListPreferredItemHeight:I

    .line 2499
    sput v0, Landroid/support/loader/R$attr;->opErrorEnabled:I

    .line 2507
    sput v0, Landroid/support/loader/R$attr;->opErrorTextAppearance:I

    .line 2514
    sput v0, Landroid/support/loader/R$attr;->opExpanded:I

    .line 2535
    sput v0, Landroid/support/loader/R$attr;->opExpandedTitleGravity:I

    .line 2545
    sput v0, Landroid/support/loader/R$attr;->opExpandedTitleMargin:I

    .line 2555
    sput v0, Landroid/support/loader/R$attr;->opExpandedTitleMarginBottom:I

    .line 2565
    sput v0, Landroid/support/loader/R$attr;->opExpandedTitleMarginEnd:I

    .line 2575
    sput v0, Landroid/support/loader/R$attr;->opExpandedTitleMarginStart:I

    .line 2585
    sput v0, Landroid/support/loader/R$attr;->opExpandedTitleMarginTop:I

    .line 2594
    sput v0, Landroid/support/loader/R$attr;->opExpandedTitleTextAppearance:I

    .line 2600
    sput v0, Landroid/support/loader/R$attr;->opHintAnimationEnabled:I

    .line 2606
    sput v0, Landroid/support/loader/R$attr;->opHintEnabled:I

    .line 2613
    sput v0, Landroid/support/loader/R$attr;->opHintTextAppearance:I

    .line 2625
    sput v0, Landroid/support/loader/R$attr;->opKeylines:I

    .line 2632
    sput v0, Landroid/support/loader/R$attr;->opNavigationButtonStyle:I

    .line 2637
    sput v0, Landroid/support/loader/R$attr;->opOnlyDarkTheme:I

    .line 2645
    sput v0, Landroid/support/loader/R$attr;->opPanelMenuListWidth:I

    .line 2651
    sput v0, Landroid/support/loader/R$attr;->opPasswordToggleContentDescription:I

    .line 2659
    sput v0, Landroid/support/loader/R$attr;->opPasswordToggleDrawable:I

    .line 2665
    sput v0, Landroid/support/loader/R$attr;->opPasswordToggleEnabled:I

    .line 2672
    sput v0, Landroid/support/loader/R$attr;->opPasswordToggleTint:I

    .line 2692
    sput v0, Landroid/support/loader/R$attr;->opPasswordToggleTintMode:I

    .line 2698
    sput v0, Landroid/support/loader/R$attr;->opPickerColorActivated:I

    .line 2704
    sput v0, Landroid/support/loader/R$attr;->opPickerColorUnActivated:I

    .line 2710
    sput v0, Landroid/support/loader/R$attr;->opPickerInputLabelUnActivated:I

    .line 2715
    sput v0, Landroid/support/loader/R$attr;->opScrimAnimationDuration:I

    .line 2725
    sput v0, Landroid/support/loader/R$attr;->opScrimVisibleHeightTrigger:I

    .line 2733
    sput v0, Landroid/support/loader/R$attr;->opStatusBarBackground:I

    .line 2742
    sput v0, Landroid/support/loader/R$attr;->opStatusBarScrim:I

    .line 2749
    sput v0, Landroid/support/loader/R$attr;->opTextAppearancePopupMenuHeader:I

    .line 2755
    sput v0, Landroid/support/loader/R$attr;->opTextColorCalendarDayActivated:I

    .line 2780
    sput v0, Landroid/support/loader/R$attr;->opTextColorError:I

    .line 2786
    sput v0, Landroid/support/loader/R$attr;->opTextColorPrimaryActivated:I

    .line 2792
    sput v0, Landroid/support/loader/R$attr;->opTextColorSecondaryActivated:I

    .line 2798
    sput v0, Landroid/support/loader/R$attr;->opTitleEnabled:I

    .line 2808
    sput v0, Landroid/support/loader/R$attr;->opToolbarId:I

    .line 2814
    sput v0, Landroid/support/loader/R$attr;->opUseAvatarIcon:I

    .line 2821
    sput v0, Landroid/support/loader/R$attr;->op_allowStacking:I

    .line 2831
    sput v0, Landroid/support/loader/R$attr;->op_backgroundTint:I

    .line 2847
    sput v0, Landroid/support/loader/R$attr;->op_backgroundTintMode:I

    .line 2853
    sput v0, Landroid/support/loader/R$attr;->op_behavior_autoHide:I

    .line 2862
    sput v0, Landroid/support/loader/R$attr;->op_behavior_overlapTop:I

    .line 2874
    sput v0, Landroid/support/loader/R$attr;->op_borderWidth:I

    .line 2881
    sput v0, Landroid/support/loader/R$attr;->op_buttonPanelSideLayout:I

    .line 2889
    sput v0, Landroid/support/loader/R$attr;->op_checkBoxPreferenceStyle:I

    .line 2894
    sput v0, Landroid/support/loader/R$attr;->op_collapsible:I

    .line 2902
    sput v0, Landroid/support/loader/R$attr;->op_dialogPreferenceStyle:I

    .line 2920
    sput v0, Landroid/support/loader/R$attr;->op_displayOptions:I

    .line 2928
    sput v0, Landroid/support/loader/R$attr;->op_editTextPreferenceStyle:I

    .line 2940
    sput v0, Landroid/support/loader/R$attr;->op_elevation:I

    .line 2953
    sput v0, Landroid/support/loader/R$attr;->op_fabSize:I

    .line 2962
    sput v0, Landroid/support/loader/R$attr;->op_horizontalSpacing:I

    .line 2969
    sput v0, Landroid/support/loader/R$attr;->op_image:I

    .line 2983
    sput v0, Landroid/support/loader/R$attr;->op_layoutManager:I

    .line 2991
    sput v0, Landroid/support/loader/R$attr;->op_layout_anchor:I

    .line 3023
    sput v0, Landroid/support/loader/R$attr;->op_layout_anchorGravity:I

    .line 3030
    sput v0, Landroid/support/loader/R$attr;->op_layout_behavior:I

    .line 3044
    sput v0, Landroid/support/loader/R$attr;->op_layout_collapseMode:I

    .line 3050
    sput v0, Landroid/support/loader/R$attr;->op_layout_collapseParallaxMultiplier:I

    .line 3069
    sput v0, Landroid/support/loader/R$attr;->op_layout_dodgeInsetEdges:I

    .line 3088
    sput v0, Landroid/support/loader/R$attr;->op_layout_insetEdge:I

    .line 3095
    sput v0, Landroid/support/loader/R$attr;->op_layout_keyline:I

    .line 3119
    sput v0, Landroid/support/loader/R$attr;->op_layout_scrollFlags:I

    .line 3128
    sput v0, Landroid/support/loader/R$attr;->op_layout_scrollInterpolator:I

    .line 3135
    sput v0, Landroid/support/loader/R$attr;->op_listItemLayout:I

    .line 3142
    sput v0, Landroid/support/loader/R$attr;->op_listLayout:I

    .line 3149
    sput v0, Landroid/support/loader/R$attr;->op_multiChoiceItemLayout:I

    .line 3162
    sput v0, Landroid/support/loader/R$attr;->op_navigationMode:I

    .line 3170
    sput v0, Landroid/support/loader/R$attr;->op_preferenceActivityStyle:I

    .line 3178
    sput v0, Landroid/support/loader/R$attr;->op_preferenceCategoryStyle:I

    .line 3186
    sput v0, Landroid/support/loader/R$attr;->op_preferenceFragmentListStyle:I

    .line 3194
    sput v0, Landroid/support/loader/R$attr;->op_preferenceFragmentStyle:I

    .line 3202
    sput v0, Landroid/support/loader/R$attr;->op_preferenceHeaderPanelStyle:I

    .line 3210
    sput v0, Landroid/support/loader/R$attr;->op_preferenceListStyle:I

    .line 3218
    sput v0, Landroid/support/loader/R$attr;->op_preferencePanelStyle:I

    .line 3226
    sput v0, Landroid/support/loader/R$attr;->op_preferenceScreenStyle:I

    .line 3234
    sput v0, Landroid/support/loader/R$attr;->op_preferenceStyle:I

    .line 3246
    sput v0, Landroid/support/loader/R$attr;->op_pressedTranslationZ:I

    .line 3251
    sput v0, Landroid/support/loader/R$attr;->op_reverseLayout:I

    .line 3259
    sput v0, Landroid/support/loader/R$attr;->op_ringtonePreferenceStyle:I

    .line 3269
    sput v0, Landroid/support/loader/R$attr;->op_rippleColor:I

    .line 3277
    sput v0, Landroid/support/loader/R$attr;->op_seekBarPreferenceStyle:I

    .line 3284
    sput v0, Landroid/support/loader/R$attr;->op_singleChoiceItemLayout:I

    .line 3288
    sput v0, Landroid/support/loader/R$attr;->op_spanCount:I

    .line 3293
    sput v0, Landroid/support/loader/R$attr;->op_stackFromEnd:I

    .line 3300
    sput v0, Landroid/support/loader/R$attr;->op_state_collapsed:I

    .line 3307
    sput v0, Landroid/support/loader/R$attr;->op_state_collapsible:I

    .line 3315
    sput v0, Landroid/support/loader/R$attr;->op_switchPreferenceStyle:I

    .line 3322
    sput v0, Landroid/support/loader/R$attr;->op_tabBackground:I

    .line 3330
    sput v0, Landroid/support/loader/R$attr;->op_tabContentStart:I

    .line 3345
    sput v0, Landroid/support/loader/R$attr;->op_tabGravity:I

    .line 3351
    sput v0, Landroid/support/loader/R$attr;->op_tabIndicatorColor:I

    .line 3359
    sput v0, Landroid/support/loader/R$attr;->op_tabIndicatorHeight:I

    .line 3367
    sput v0, Landroid/support/loader/R$attr;->op_tabMaxWidth:I

    .line 3375
    sput v0, Landroid/support/loader/R$attr;->op_tabMinWidth:I

    .line 3387
    sput v0, Landroid/support/loader/R$attr;->op_tabMode:I

    .line 3395
    sput v0, Landroid/support/loader/R$attr;->op_tabPadding:I

    .line 3403
    sput v0, Landroid/support/loader/R$attr;->op_tabPaddingBottom:I

    .line 3411
    sput v0, Landroid/support/loader/R$attr;->op_tabPaddingEnd:I

    .line 3419
    sput v0, Landroid/support/loader/R$attr;->op_tabPaddingStart:I

    .line 3427
    sput v0, Landroid/support/loader/R$attr;->op_tabPaddingTop:I

    .line 3433
    sput v0, Landroid/support/loader/R$attr;->op_tabSelectedTextColor:I

    .line 3440
    sput v0, Landroid/support/loader/R$attr;->op_tabTextAppearance:I

    .line 3446
    sput v0, Landroid/support/loader/R$attr;->op_tabTextColor:I

    .line 3452
    sput v0, Landroid/support/loader/R$attr;->op_tint_color:I

    .line 3459
    sput v0, Landroid/support/loader/R$attr;->opsolidColor:I

    .line 3465
    sput v0, Landroid/support/loader/R$attr;->overlapAnchor:I

    .line 3474
    sput v0, Landroid/support/loader/R$attr;->paddingBottomNoButtons:I

    .line 3483
    sput v0, Landroid/support/loader/R$attr;->paddingEnd:I

    .line 3491
    sput v0, Landroid/support/loader/R$attr;->paddingLeftWithNavIcon:I

    .line 3500
    sput v0, Landroid/support/loader/R$attr;->paddingStart:I

    .line 3509
    sput v0, Landroid/support/loader/R$attr;->paddingTopNoTitle:I

    .line 3517
    sput v0, Landroid/support/loader/R$attr;->panelBackground:I

    .line 3525
    sput v0, Landroid/support/loader/R$attr;->panelMenuListTheme:I

    .line 3534
    sput v0, Landroid/support/loader/R$attr;->panelMenuListWidth:I

    .line 3542
    sput v0, Landroid/support/loader/R$attr;->popupMenuStyle:I

    .line 3551
    sput v0, Landroid/support/loader/R$attr;->popupTheme:I

    .line 3559
    sput v0, Landroid/support/loader/R$attr;->popupWindowStyle:I

    .line 3565
    sput v0, Landroid/support/loader/R$attr;->preserveIconSpacing:I

    .line 3574
    sput v0, Landroid/support/loader/R$attr;->progressBarPadding:I

    .line 3582
    sput v0, Landroid/support/loader/R$attr;->progressBarStyle:I

    .line 3590
    sput v0, Landroid/support/loader/R$attr;->queryBackground:I

    .line 3596
    sput v0, Landroid/support/loader/R$attr;->queryHint:I

    .line 3604
    sput v0, Landroid/support/loader/R$attr;->radioButtonStyle:I

    .line 3612
    sput v0, Landroid/support/loader/R$attr;->ratingBarStyle:I

    .line 3620
    sput v0, Landroid/support/loader/R$attr;->ratingBarStyleIndicator:I

    .line 3628
    sput v0, Landroid/support/loader/R$attr;->ratingBarStyleSmall:I

    .line 3637
    sput v0, Landroid/support/loader/R$attr;->realPaddingBottom:I

    .line 3645
    sput v0, Landroid/support/loader/R$attr;->realTitleMarginBottom:I

    .line 3653
    sput v0, Landroid/support/loader/R$attr;->searchHintIcon:I

    .line 3661
    sput v0, Landroid/support/loader/R$attr;->searchIcon:I

    .line 3669
    sput v0, Landroid/support/loader/R$attr;->searchViewStyle:I

    .line 3677
    sput v0, Landroid/support/loader/R$attr;->seekBarStyle:I

    .line 3687
    sput v0, Landroid/support/loader/R$attr;->selectableItemBackground:I

    .line 3695
    sput v0, Landroid/support/loader/R$attr;->selectableItemBackgroundBorderless:I

    .line 3703
    sput v0, Landroid/support/loader/R$attr;->selectionDivider:I

    .line 3712
    sput v0, Landroid/support/loader/R$attr;->selectionDividerHeight:I

    .line 3721
    sput v0, Landroid/support/loader/R$attr;->selectionDividerWidth:I

    .line 3730
    sput v0, Landroid/support/loader/R$attr;->selectionDividersDistance:I

    .line 3736
    sput v0, Landroid/support/loader/R$attr;->selectionOtherNumberColor:I

    .line 3761
    sput v0, Landroid/support/loader/R$attr;->showAsAction:I

    .line 3776
    sput v0, Landroid/support/loader/R$attr;->showDividers:I

    .line 3782
    sput v0, Landroid/support/loader/R$attr;->showText:I

    .line 3787
    sput v0, Landroid/support/loader/R$attr;->showTitle:I

    .line 3794
    sput v0, Landroid/support/loader/R$attr;->singleChoiceItemLayout:I

    .line 3800
    sput v0, Landroid/support/loader/R$attr;->spinBars:I

    .line 3808
    sput v0, Landroid/support/loader/R$attr;->spinnerDropDownItemStyle:I

    .line 3816
    sput v0, Landroid/support/loader/R$attr;->spinnerStyle:I

    .line 3822
    sput v0, Landroid/support/loader/R$attr;->splitTrack:I

    .line 3831
    sput v0, Landroid/support/loader/R$attr;->srcCompat:I

    .line 3837
    sput v0, Landroid/support/loader/R$attr;->state_above_anchor:I

    .line 3842
    sput v0, Landroid/support/loader/R$attr;->state_indeterminate:I

    .line 3853
    sput v0, Landroid/support/loader/R$attr;->statusBarBackground:I

    .line 3861
    sput v0, Landroid/support/loader/R$attr;->subMenuArrow:I

    .line 3869
    sput v0, Landroid/support/loader/R$attr;->subTitleMarginBottom:I

    .line 3877
    sput v0, Landroid/support/loader/R$attr;->submitBackground:I

    .line 3883
    sput v0, Landroid/support/loader/R$attr;->subtitle:I

    .line 3890
    sput v0, Landroid/support/loader/R$attr;->subtitleTextAppearance:I

    .line 3897
    sput v0, Landroid/support/loader/R$attr;->subtitleTextColor:I

    .line 3905
    sput v0, Landroid/support/loader/R$attr;->subtitleTextStyle:I

    .line 3913
    sput v0, Landroid/support/loader/R$attr;->suggestionRowLayout:I

    .line 3922
    sput v0, Landroid/support/loader/R$attr;->switchMinWidth:I

    .line 3931
    sput v0, Landroid/support/loader/R$attr;->switchPadding:I

    .line 3939
    sput v0, Landroid/support/loader/R$attr;->switchStyle:I

    .line 3947
    sput v0, Landroid/support/loader/R$attr;->switchTextAppearance:I

    .line 3957
    sput v0, Landroid/support/loader/R$attr;->textAllCaps:I

    .line 3965
    sput v0, Landroid/support/loader/R$attr;->textAppearanceLargePopupMenu:I

    .line 3973
    sput v0, Landroid/support/loader/R$attr;->textAppearanceListItem:I

    .line 3981
    sput v0, Landroid/support/loader/R$attr;->textAppearanceListItemSecondary:I

    .line 3989
    sput v0, Landroid/support/loader/R$attr;->textAppearanceListItemSmall:I

    .line 3997
    sput v0, Landroid/support/loader/R$attr;->textAppearanceOPNumberPickerUnit:I

    .line 4005
    sput v0, Landroid/support/loader/R$attr;->textAppearancePopupMenuHeader:I

    .line 4013
    sput v0, Landroid/support/loader/R$attr;->textAppearanceSearchResultSubtitle:I

    .line 4021
    sput v0, Landroid/support/loader/R$attr;->textAppearanceSearchResultTitle:I

    .line 4029
    sput v0, Landroid/support/loader/R$attr;->textAppearanceSmallPopupMenu:I

    .line 4040
    sput v0, Landroid/support/loader/R$attr;->textColorAlertDialogListItem:I

    .line 4051
    sput v0, Landroid/support/loader/R$attr;->textColorSearchUrl:I

    .line 4058
    sput v0, Landroid/support/loader/R$attr;->theme:I

    .line 4067
    sput v0, Landroid/support/loader/R$attr;->thickness:I

    .line 4072
    sput v0, Landroid/support/loader/R$attr;->threeState:I

    .line 4081
    sput v0, Landroid/support/loader/R$attr;->thumbTextPadding:I

    .line 4088
    sput v0, Landroid/support/loader/R$attr;->thumbTint:I

    .line 4110
    sput v0, Landroid/support/loader/R$attr;->thumbTintMode:I

    .line 4118
    sput v0, Landroid/support/loader/R$attr;->tickMark:I

    .line 4125
    sput v0, Landroid/support/loader/R$attr;->tickMarkTint:I

    .line 4147
    sput v0, Landroid/support/loader/R$attr;->tickMarkTintMode:I

    .line 4154
    sput v0, Landroid/support/loader/R$attr;->timePickerStyle:I

    .line 4161
    sput v0, Landroid/support/loader/R$attr;->tint:I

    .line 4183
    sput v0, Landroid/support/loader/R$attr;->tintMode:I

    .line 4188
    sput v0, Landroid/support/loader/R$attr;->title:I

    .line 4198
    sput v0, Landroid/support/loader/R$attr;->titleMargin:I

    .line 4209
    sput v0, Landroid/support/loader/R$attr;->titleMarginBottom:I

    .line 4220
    sput v0, Landroid/support/loader/R$attr;->titleMarginEnd:I

    .line 4231
    sput v0, Landroid/support/loader/R$attr;->titleMarginStart:I

    .line 4242
    sput v0, Landroid/support/loader/R$attr;->titleMarginTop:I

    .line 4252
    sput v0, Landroid/support/loader/R$attr;->titleMargins:I

    .line 4259
    sput v0, Landroid/support/loader/R$attr;->titleTextAppearance:I

    .line 4266
    sput v0, Landroid/support/loader/R$attr;->titleTextColor:I

    .line 4274
    sput v0, Landroid/support/loader/R$attr;->titleTextStyle:I

    .line 4282
    sput v0, Landroid/support/loader/R$attr;->toolbarNavigationButtonStyle:I

    .line 4289
    sput v0, Landroid/support/loader/R$attr;->toolbarStyle:I

    .line 4300
    sput v0, Landroid/support/loader/R$attr;->tooltipForegroundColor:I

    .line 4308
    sput v0, Landroid/support/loader/R$attr;->tooltipFrameBackground:I

    .line 4314
    sput v0, Landroid/support/loader/R$attr;->tooltipText:I

    .line 4322
    sput v0, Landroid/support/loader/R$attr;->track:I

    .line 4329
    sput v0, Landroid/support/loader/R$attr;->trackTint:I

    .line 4351
    sput v0, Landroid/support/loader/R$attr;->trackTintMode:I

    .line 4357
    sput v0, Landroid/support/loader/R$attr;->ttcIndex:I

    .line 4366
    sput v0, Landroid/support/loader/R$attr;->useDisabledAlpha:I

    .line 4371
    sput v0, Landroid/support/loader/R$attr;->viewInflaterClass:I

    .line 4379
    sput v0, Landroid/support/loader/R$attr;->virtualButtonPressedDrawable:I

    .line 4387
    sput v0, Landroid/support/loader/R$attr;->voiceIcon:I

    .line 4395
    sput v0, Landroid/support/loader/R$attr;->weekDayTextAppearance:I

    .line 4402
    sput v0, Landroid/support/loader/R$attr;->windowActionBar:I

    .line 4410
    sput v0, Landroid/support/loader/R$attr;->windowActionBarOverlay:I

    .line 4417
    sput v0, Landroid/support/loader/R$attr;->windowActionModeOverlay:I

    .line 4432
    sput v0, Landroid/support/loader/R$attr;->windowFixedHeightMajor:I

    .line 4447
    sput v0, Landroid/support/loader/R$attr;->windowFixedHeightMinor:I

    .line 4462
    sput v0, Landroid/support/loader/R$attr;->windowFixedWidthMajor:I

    .line 4477
    sput v0, Landroid/support/loader/R$attr;->windowFixedWidthMinor:I

    .line 4493
    sput v0, Landroid/support/loader/R$attr;->windowMinWidthMajor:I

    .line 4509
    sput v0, Landroid/support/loader/R$attr;->windowMinWidthMinor:I

    .line 4515
    sput v0, Landroid/support/loader/R$attr;->windowNoTitle:I

    .line 4523
    sput v0, Landroid/support/loader/R$attr;->yearListItemActivatedTextAppearance:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
