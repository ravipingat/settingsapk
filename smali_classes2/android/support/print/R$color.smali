.class public final Landroid/support/print/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/print/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static abc_background_cache_hint_selector_material_dark:I

.field public static abc_background_cache_hint_selector_material_light:I

.field public static abc_btn_colored_borderless_text_material:I

.field public static abc_btn_colored_text_material:I

.field public static abc_color_highlight_material:I

.field public static abc_hint_foreground_material_dark:I

.field public static abc_hint_foreground_material_light:I

.field public static abc_input_method_navigation_guard:I

.field public static abc_primary_text_disable_only_material_dark:I

.field public static abc_primary_text_disable_only_material_light:I

.field public static abc_primary_text_material_dark:I

.field public static abc_primary_text_material_light:I

.field public static abc_search_url_text:I

.field public static abc_search_url_text_normal:I

.field public static abc_search_url_text_pressed:I

.field public static abc_search_url_text_selected:I

.field public static abc_secondary_text_material_dark:I

.field public static abc_secondary_text_material_light:I

.field public static abc_tint_btn_checkable:I

.field public static abc_tint_default:I

.field public static abc_tint_edittext:I

.field public static abc_tint_seek_thumb:I

.field public static abc_tint_spinner:I

.field public static abc_tint_switch_track:I

.field public static accent_charm_purple_dark:I

.field public static accent_charm_purple_light:I

.field public static accent_default_dark:I

.field public static accent_default_light:I

.field public static accent_fashion_pink_dark:I

.field public static accent_fashion_pink_light:I

.field public static accent_golden_dark:I

.field public static accent_golden_light:I

.field public static accent_grass_green_dark:I

.field public static accent_grass_green_light:I

.field public static accent_lemon_yellow_dark:I

.field public static accent_lemon_yellow_light:I

.field public static accent_material_dark:I

.field public static accent_material_light:I

.field public static accent_sky_blue_dark:I

.field public static accent_sky_blue_light:I

.field public static accent_star_wars:I

.field public static accent_vigour_red_dark:I

.field public static accent_vigour_red_light:I

.field public static actionBarSearchViewIconNormal:I

.field public static action_bar_subtitle_color_dark:I

.field public static action_bar_subtitle_color_light:I

.field public static action_bar_title_color_dark:I

.field public static action_bar_title_color_light:I

.field public static actionbar_background_color_dark:I

.field public static actionbar_blue:I

.field public static actionbar_coffee:I

.field public static actionbar_contacts:I

.field public static actionbar_flint:I

.field public static actionbar_green:I

.field public static actionbar_iris:I

.field public static actionbar_lilac:I

.field public static actionbar_message:I

.field public static actionbar_olive:I

.field public static actionbar_punch:I

.field public static actionbar_red:I

.field public static actionbar_sage:I

.field public static actionbar_settings:I

.field public static actionbar_wood:I

.field public static alert_dialog_color_accent_dark:I

.field public static alert_dialog_color_accent_light:I

.field public static alert_dialog_color_background_dark:I

.field public static alert_dialog_color_background_light:I

.field public static alert_dialog_title_style_font_color_dark:I

.field public static alert_dialog_title_style_font_color_light:I

.field public static amber_100:I

.field public static amber_200:I

.field public static amber_300:I

.field public static amber_400:I

.field public static amber_50:I

.field public static amber_500:I

.field public static amber_600:I

.field public static amber_700:I

.field public static amber_800:I

.field public static amber_900:I

.field public static amber_a100:I

.field public static amber_a200:I

.field public static amber_a400:I

.field public static amber_a700:I

.field public static android_ripple_material_dark:I

.field public static android_ripple_material_light:I

.field public static background_cache_hint_selector_material_dark:I

.field public static background_cache_hint_selector_material_light:I

.field public static background_floating_material_dark:I

.field public static background_floating_material_light:I

.field public static background_material_dark:I

.field public static background_material_light:I

.field public static bg_ab:I

.field public static bg_ab_dark:I

.field public static bg_ab_light:I

.field public static bg_dialog:I

.field public static bg_dialog_dark:I

.field public static bg_dialog_light:I

.field public static bg_interface:I

.field public static bg_interface_dark:I

.field public static bg_interface_light:I

.field public static black:I

.field public static blue_01:I

.field public static blue_01_dark:I

.field public static blue_01_light:I

.field public static blue_02:I

.field public static blue_02_dark:I

.field public static blue_02_light:I

.field public static blue_03:I

.field public static blue_03_dark:I

.field public static blue_03_light:I

.field public static blue_04:I

.field public static blue_04_dark:I

.field public static blue_04_light:I

.field public static blue_05:I

.field public static blue_05_dark:I

.field public static blue_05_light:I

.field public static blue_100:I

.field public static blue_200:I

.field public static blue_300:I

.field public static blue_400:I

.field public static blue_50:I

.field public static blue_500:I

.field public static blue_600:I

.field public static blue_700:I

.field public static blue_800:I

.field public static blue_900:I

.field public static blue_a100:I

.field public static blue_a200:I

.field public static blue_a400:I

.field public static blue_a700:I

.field public static bluegrey_100:I

.field public static bluegrey_200:I

.field public static bluegrey_300:I

.field public static bluegrey_400:I

.field public static bluegrey_50:I

.field public static bluegrey_500:I

.field public static bluegrey_600:I

.field public static bluegrey_700:I

.field public static bluegrey_800:I

.field public static bluegrey_900:I

.field public static bottom_navigation_badge_background_color:I

.field public static bottom_navigation_badge_text_color:I

.field public static bright_foreground_disabled_material_dark:I

.field public static bright_foreground_disabled_material_light:I

.field public static bright_foreground_inverse_material_dark:I

.field public static bright_foreground_inverse_material_light:I

.field public static bright_foreground_material_dark:I

.field public static bright_foreground_material_light:I

.field public static brown_100:I

.field public static brown_200:I

.field public static brown_300:I

.field public static brown_400:I

.field public static brown_50:I

.field public static brown_500:I

.field public static brown_600:I

.field public static brown_700:I

.field public static brown_800:I

.field public static brown_900:I

.field public static btn_colorControlHighlight_dark:I

.field public static btn_colorControlHighlight_light:I

.field public static btn_colored_text_material:I

.field public static btn_default_material_dark:I

.field public static btn_default_material_light:I

.field public static btn_foreground_material:I

.field public static btn_foreground_material_dark:I

.field public static btn_foreground_material_light:I

.field public static button_disable_color_dark:I

.field public static button_disable_color_light:I

.field public static button_material_dark:I

.field public static button_material_light:I

.field public static button_normal_color_dark:I

.field public static button_normal_color_light:I

.field public static cardview_dark_background:I

.field public static cardview_light_background:I

.field public static cardview_shadow_end_color:I

.field public static cardview_shadow_start_color:I

.field public static checkbox_button_disable_color_dark:I

.field public static checkbox_button_disable_color_light:I

.field public static checkbox_button_normal_color_dark:I

.field public static checkbox_button_normal_color_light:I

.field public static colorButtonNormal:I

.field public static colorpicker_disabled_cell_color:I

.field public static colorpicker_ripple_effect_color:I

.field public static control_disable_text_color_dark:I

.field public static control_disable_text_color_light:I

.field public static control_highlight_material:I

.field public static control_normal_color:I

.field public static control_normal_color_light:I

.field public static control_primary_text_color_dark:I

.field public static control_primary_text_color_light:I

.field public static control_secondary_text_color_dark:I

.field public static control_secondary_text_color_light:I

.field public static cyan_100:I

.field public static cyan_200:I

.field public static cyan_300:I

.field public static cyan_400:I

.field public static cyan_50:I

.field public static cyan_500:I

.field public static cyan_600:I

.field public static cyan_700:I

.field public static cyan_800:I

.field public static cyan_900:I

.field public static cyan_a100:I

.field public static cyan_a200:I

.field public static cyan_a400:I

.field public static cyan_a700:I

.field public static datepicker_default_circle_background_color_material_dark:I

.field public static datepicker_default_circle_background_color_material_light:I

.field public static datepicker_default_disabled_text_color_material_dark:I

.field public static datepicker_default_disabled_text_color_material_light:I

.field public static datepicker_default_header_dayofweek_background_color_material_dark:I

.field public static datepicker_default_header_dayofweek_background_color_material_light:I

.field public static datepicker_default_header_selector_background_material_dark:I

.field public static datepicker_default_header_selector_background_material_light:I

.field public static datepicker_default_normal_text_color_material_dark:I

.field public static datepicker_default_normal_text_color_material_light:I

.field public static datepicker_default_pressed_text_color_material_dark:I

.field public static datepicker_default_pressed_text_color_material_light:I

.field public static datepicker_default_selected_text_color_material_dark:I

.field public static datepicker_default_selected_text_color_material_light:I

.field public static datepicker_default_view_animator_color_material_dark:I

.field public static datepicker_default_view_animator_color_material_light:I

.field public static datepicker_timepicker_header_selected_activated_text_color_dark:I

.field public static datepicker_timepicker_header_selected_text_color_dark:I

.field public static deeporange_100:I

.field public static deeporange_200:I

.field public static deeporange_300:I

.field public static deeporange_400:I

.field public static deeporange_50:I

.field public static deeporange_500:I

.field public static deeporange_600:I

.field public static deeporange_700:I

.field public static deeporange_800:I

.field public static deeporange_900:I

.field public static deeporange_a100:I

.field public static deeporange_a200:I

.field public static deeporange_a400:I

.field public static deeporange_a700:I

.field public static deeppurple_100:I

.field public static deeppurple_200:I

.field public static deeppurple_300:I

.field public static deeppurple_400:I

.field public static deeppurple_50:I

.field public static deeppurple_500:I

.field public static deeppurple_600:I

.field public static deeppurple_700:I

.field public static deeppurple_800:I

.field public static deeppurple_900:I

.field public static deeppurple_a100:I

.field public static deeppurple_a200:I

.field public static deeppurple_a400:I

.field public static deeppurple_a700:I

.field public static design_bottom_navigation_shadow_color:I

.field public static design_fab_shadow_end_color:I

.field public static design_fab_shadow_mid_color:I

.field public static design_fab_shadow_start_color:I

.field public static design_fab_stroke_end_inner_color:I

.field public static design_fab_stroke_end_outer_color:I

.field public static design_fab_stroke_top_inner_color:I

.field public static design_fab_stroke_top_outer_color:I

.field public static dialog_color_background:I

.field public static dialog_color_background_dark:I

.field public static dialog_header_color_background:I

.field public static dialog_header_color_background_dark:I

.field public static dim_foreground_disabled_material_dark:I

.field public static dim_foreground_disabled_material_light:I

.field public static dim_foreground_material_dark:I

.field public static dim_foreground_material_light:I

.field public static divider_crude:I

.field public static divider_crude_dark:I

.field public static divider_crude_light:I

.field public static divider_fine:I

.field public static divider_fine_dark:I

.field public static divider_fine_light:I

.field public static edittext_color:I

.field public static edittext_color_dark:I

.field public static edittext_error_color:I

.field public static edittext_error_color_dark:I

.field public static edittext_hint_color:I

.field public static edittext_hint_color_dark:I

.field public static edittext_normal_color:I

.field public static edittext_normal_color_dark:I

.field public static error_color_material_dark:I

.field public static error_color_material_light:I

.field public static font_ab_title:I

.field public static font_ab_title_dark:I

.field public static font_ab_title_light:I

.field public static font_dialog_text:I

.field public static font_dialog_text_dark:I

.field public static font_dialog_text_light:I

.field public static font_dialog_title:I

.field public static font_dialog_title_dark:I

.field public static font_dialog_title_light:I

.field public static font_list_settings_title:I

.field public static font_list_settings_title_dark:I

.field public static font_list_settings_title_light:I

.field public static font_list_subtitle:I

.field public static font_list_subtitle_dark:I

.field public static font_list_subtitle_light:I

.field public static font_list_text:I

.field public static font_list_text_dark:I

.field public static font_list_text_light:I

.field public static font_tab_off:I

.field public static font_tab_off_dark:I

.field public static font_tab_off_light:I

.field public static font_tab_on:I

.field public static font_tab_on_dark:I

.field public static font_tab_on_light:I

.field public static foreground_material_dark:I

.field public static foreground_material_light:I

.field public static gradation_01:I

.field public static gradation_01_dark:I

.field public static gradation_01_light:I

.field public static gradation_02:I

.field public static gradation_02_dark:I

.field public static gradation_02_light:I

.field public static gradation_03:I

.field public static gradation_03_dark:I

.field public static gradation_03_light:I

.field public static gradation_04:I

.field public static gradation_04_dark:I

.field public static gradation_04_light:I

.field public static gradation_05:I

.field public static gradation_05_dark:I

.field public static gradation_05_light:I

.field public static gradation_06:I

.field public static gradation_06_dark:I

.field public static gradation_06_light:I

.field public static gradation_07:I

.field public static gradation_07_dark:I

.field public static gradation_07_light:I

.field public static gradation_08:I

.field public static gradation_08_dark:I

.field public static gradation_08_light:I

.field public static gradation_09:I

.field public static gradation_09_dark:I

.field public static gradation_09_light:I

.field public static gradation_10:I

.field public static gradation_10_dark:I

.field public static gradation_10_light:I

.field public static gradation_11:I

.field public static gradation_11_dark:I

.field public static gradation_11_light:I

.field public static green_01:I

.field public static green_01_dark:I

.field public static green_01_light:I

.field public static green_02:I

.field public static green_02_dark:I

.field public static green_02_light:I

.field public static green_03:I

.field public static green_03_dark:I

.field public static green_03_light:I

.field public static green_100:I

.field public static green_200:I

.field public static green_300:I

.field public static green_400:I

.field public static green_50:I

.field public static green_500:I

.field public static green_600:I

.field public static green_700:I

.field public static green_800:I

.field public static green_900:I

.field public static green_a100:I

.field public static green_a200:I

.field public static green_a400:I

.field public static green_a700:I

.field public static grey:I

.field public static grey_01:I

.field public static grey_01_dark:I

.field public static grey_01_light:I

.field public static grey_02:I

.field public static grey_02_dark:I

.field public static grey_02_light:I

.field public static grey_03:I

.field public static grey_03_dark:I

.field public static grey_03_light:I

.field public static grey_04:I

.field public static grey_04_dark:I

.field public static grey_04_light:I

.field public static grey_05:I

.field public static grey_05_dark:I

.field public static grey_05_light:I

.field public static grey_06:I

.field public static grey_06_dark:I

.field public static grey_06_light:I

.field public static grey_07:I

.field public static grey_07_dark:I

.field public static grey_07_light:I

.field public static grey_100:I

.field public static grey_1000:I

.field public static grey_200:I

.field public static grey_300:I

.field public static grey_400:I

.field public static grey_50:I

.field public static grey_500:I

.field public static grey_600:I

.field public static grey_700:I

.field public static grey_800:I

.field public static grey_900:I

.field public static grey_a100:I

.field public static grey_dark:I

.field public static grey_light:I

.field public static highlighted_text_material:I

.field public static highlighted_text_material_dark:I

.field public static highlighted_text_material_light:I

.field public static indigo_100:I

.field public static indigo_200:I

.field public static indigo_300:I

.field public static indigo_400:I

.field public static indigo_50:I

.field public static indigo_500:I

.field public static indigo_600:I

.field public static indigo_700:I

.field public static indigo_800:I

.field public static indigo_900:I

.field public static indigo_a100:I

.field public static indigo_a200:I

.field public static indigo_a400:I

.field public static indigo_a700:I

.field public static lightblue_100:I

.field public static lightblue_200:I

.field public static lightblue_300:I

.field public static lightblue_400:I

.field public static lightblue_50:I

.field public static lightblue_500:I

.field public static lightblue_600:I

.field public static lightblue_700:I

.field public static lightblue_800:I

.field public static lightblue_900:I

.field public static lightblue_a100:I

.field public static lightblue_a200:I

.field public static lightblue_a400:I

.field public static lightblue_a700:I

.field public static lightgreen_100:I

.field public static lightgreen_200:I

.field public static lightgreen_300:I

.field public static lightgreen_400:I

.field public static lightgreen_50:I

.field public static lightgreen_500:I

.field public static lightgreen_600:I

.field public static lightgreen_700:I

.field public static lightgreen_800:I

.field public static lightgreen_900:I

.field public static lightgreen_a100:I

.field public static lightgreen_a200:I

.field public static lightgreen_a400:I

.field public static lightgreen_a700:I

.field public static lime_100:I

.field public static lime_200:I

.field public static lime_300:I

.field public static lime_400:I

.field public static lime_50:I

.field public static lime_500:I

.field public static lime_600:I

.field public static lime_700:I

.field public static lime_800:I

.field public static lime_900:I

.field public static lime_a100:I

.field public static lime_a200:I

.field public static lime_a400:I

.field public static lime_a700:I

.field public static listitem_primarytext_color:I

.field public static listitem_secondarttext_color:I

.field public static listitem_stamptext_color:I

.field public static listview_divider_color:I

.field public static listview_divider_color_dark:I

.field public static material_blue_grey_800:I

.field public static material_blue_grey_900:I

.field public static material_blue_grey_950:I

.field public static material_deep_teal_200:I

.field public static material_deep_teal_500:I

.field public static material_grey_100:I

.field public static material_grey_300:I

.field public static material_grey_50:I

.field public static material_grey_600:I

.field public static material_grey_800:I

.field public static material_grey_850:I

.field public static material_grey_900:I

.field public static mtrl_bottom_nav_colored_item_tint:I

.field public static mtrl_bottom_nav_item_tint:I

.field public static notification_action_color_filter:I

.field public static notification_icon_bg_color:I

.field public static notification_material_background_media_default_color:I

.field public static numberpicker_divider_color_dark:I

.field public static numberpicker_divider_color_light:I

.field public static numberpicker_minute_text_color_dark:I

.field public static numberpicker_minute_text_color_light:I

.field public static onePlusAccentColor_device_default_dark:I

.field public static onePlusAccentColor_device_default_light:I

.field public static onePlusPrimaryColor_device_default:I

.field public static onePlusPrimaryColor_device_default_dark:I

.field public static onePlusPrimaryColor_device_default_light:I

.field public static oneplus_accent_color:I

.field public static oneplus_accent_theme_2:I

.field public static oneplus_alert_progress_dialog_background_color:I

.field public static oneplus_contorl_bg_color_appbar_dark:I

.field public static oneplus_contorl_bg_color_appbar_default:I

.field public static oneplus_contorl_bg_color_appbar_light:I

.field public static oneplus_contorl_bg_color_bottomsheets_dark:I

.field public static oneplus_contorl_bg_color_bottomsheets_default:I

.field public static oneplus_contorl_bg_color_bottomsheets_light:I

.field public static oneplus_contorl_bg_color_cardview_dark:I

.field public static oneplus_contorl_bg_color_cardview_default:I

.field public static oneplus_contorl_bg_color_cardview_light:I

.field public static oneplus_contorl_bg_color_dark:I

.field public static oneplus_contorl_bg_color_default:I

.field public static oneplus_contorl_bg_color_dialogs_dark:I

.field public static oneplus_contorl_bg_color_dialogs_default:I

.field public static oneplus_contorl_bg_color_dialogs_light:I

.field public static oneplus_contorl_bg_color_light:I

.field public static oneplus_contorl_bg_color_listbg_dark:I

.field public static oneplus_contorl_bg_color_listbg_default:I

.field public static oneplus_contorl_bg_color_listbg_light:I

.field public static oneplus_contorl_bg_color_menu_dark:I

.field public static oneplus_contorl_bg_color_menu_default:I

.field public static oneplus_contorl_bg_color_menu_light:I

.field public static oneplus_contorl_bg_color_statusbar_dark:I

.field public static oneplus_contorl_bg_color_statusbar_default:I

.field public static oneplus_contorl_bg_color_statusbar_light:I

.field public static oneplus_contorl_bg_color_steppers_dark:I

.field public static oneplus_contorl_bg_color_steppers_default:I

.field public static oneplus_contorl_bg_color_steppers_light:I

.field public static oneplus_contorl_divider_color_dark:I

.field public static oneplus_contorl_divider_color_default:I

.field public static oneplus_contorl_divider_color_light:I

.field public static oneplus_contorl_icon_color_accent_active_dark:I

.field public static oneplus_contorl_icon_color_accent_active_default:I

.field public static oneplus_contorl_icon_color_accent_active_light:I

.field public static oneplus_contorl_icon_color_accent_inactive_dark:I

.field public static oneplus_contorl_icon_color_accent_inactive_default:I

.field public static oneplus_contorl_icon_color_accent_inactive_light:I

.field public static oneplus_contorl_icon_color_active_dark:I

.field public static oneplus_contorl_icon_color_active_default:I

.field public static oneplus_contorl_icon_color_active_light:I

.field public static oneplus_contorl_icon_color_inactive_dark:I

.field public static oneplus_contorl_icon_color_inactive_default:I

.field public static oneplus_contorl_icon_color_inactive_light:I

.field public static oneplus_contorl_text_color_disable_dark:I

.field public static oneplus_contorl_text_color_disable_default:I

.field public static oneplus_contorl_text_color_disable_light:I

.field public static oneplus_contorl_text_color_hint_dark:I

.field public static oneplus_contorl_text_color_hint_default:I

.field public static oneplus_contorl_text_color_hint_light:I

.field public static oneplus_contorl_text_color_label_dark:I

.field public static oneplus_contorl_text_color_label_default:I

.field public static oneplus_contorl_text_color_label_light:I

.field public static oneplus_contorl_text_color_primary_dark:I

.field public static oneplus_contorl_text_color_primary_default:I

.field public static oneplus_contorl_text_color_primary_light:I

.field public static oneplus_contorl_text_color_secondary_dark:I

.field public static oneplus_contorl_text_color_secondary_default:I

.field public static oneplus_contorl_text_color_secondary_light:I

.field public static oneplus_numberpicker_divider_color_dark:I

.field public static oneplus_numberpicker_divider_color_default:I

.field public static oneplus_numberpicker_divider_color_light:I

.field public static oneplus_preference_subtitle_text_color_dark:I

.field public static oneplus_preference_subtitle_text_color_light:I

.field public static oneplus_preference_title_text_color_dark:I

.field public static oneplus_preference_title_text_color_light:I

.field public static oneplus_red_color:I

.field public static oneplus_tab_indicator_color_colored:I

.field public static oneplus_tab_indicator_color_dark:I

.field public static oneplus_tab_indicator_color_light:I

.field public static oneplus_white_color:I

.field public static op_abc_tint_switch_thumb:I

.field public static op_actionbar_subtitle_text_material:I

.field public static op_actionbar_title_text_material:I

.field public static op_btn_colored_raised_text_material_dark:I

.field public static op_btn_colored_raised_text_material_light:I

.field public static op_btn_colored_text_material_dark:I

.field public static op_btn_colored_text_material_light:I

.field public static op_btn_default_material_dark:I

.field public static op_btn_default_material_light:I

.field public static op_btn_ripple_material_dark:I

.field public static op_btn_ripple_material_light:I

.field public static op_button_material_disable_dark:I

.field public static op_button_material_disable_light:I

.field public static op_button_text:I

.field public static op_cc_text_color_primary:I

.field public static op_color_bottom_navigation_item_tint:I

.field public static op_control_checkable_material:I

.field public static op_date_time_picker_header_text_color_dark:I

.field public static op_date_time_picker_header_text_color_light:I

.field public static op_design_error:I

.field public static op_design_snackbar_background_color:I

.field public static op_design_tint_password_toggle:I

.field public static op_drop_down_item_selected_color_dark:I

.field public static op_drop_down_item_selected_color_light:I

.field public static op_edittext_hint_color_material_dark:I

.field public static op_edittext_hint_color_material_light:I

.field public static op_error_color_material_dark:I

.field public static op_error_color_material_default:I

.field public static op_error_color_material_light:I

.field public static op_hint_foreground_material_dark:I

.field public static op_hint_foreground_material_light:I

.field public static op_menu_background_dark:I

.field public static op_menu_background_light:I

.field public static op_menu_text_dark:I

.field public static op_menu_text_light:I

.field public static op_menu_text_material_dark:I

.field public static op_menu_text_material_light:I

.field public static op_primary_text_color_dark:I

.field public static op_primary_text_color_dark_disabled:I

.field public static op_primary_text_color_light:I

.field public static op_primary_text_color_light_disabled:I

.field public static op_primary_text_inverse_when_activated_material_dark:I

.field public static op_primary_text_inverse_when_activated_material_light:I

.field public static op_primary_text_material_dark:I

.field public static op_primary_text_material_light:I

.field public static op_primary_text_secondary_when_activated_material:I

.field public static op_primary_text_secondary_when_activated_material_inverse:I

.field public static op_ripple_material_dark:I

.field public static op_ripple_material_light:I

.field public static op_secondary_text_color_dark:I

.field public static op_secondary_text_color_dark_disabled:I

.field public static op_secondary_text_color_light:I

.field public static op_secondary_text_color_light_disabled:I

.field public static op_secondary_text_material_dark:I

.field public static op_secondary_text_material_light:I

.field public static op_switch_thumb_material_light_selector:I

.field public static op_switch_thumb_material_selector:I

.field public static op_switch_track_material:I

.field public static op_switch_track_material_light:I

.field public static op_tab_highlight_material:I

.field public static op_tab_indicator_material:I

.field public static op_tab_indicator_text_material:I

.field public static op_tab_indicator_text_material_light:I

.field public static op_tablayout_title_text_material:I

.field public static op_tablayout_title_text_selected_material:I

.field public static op_text_selection_highlight_dark:I

.field public static op_text_selection_highlight_light:I

.field public static op_time_picker_intput_label_text_color:I

.field public static op_timepicker_text_inverse_when_activated_material_dark:I

.field public static op_timepicker_text_inverse_when_activated_material_light:I

.field public static op_toast_text_color_dark:I

.field public static op_toast_text_color_light:I

.field public static opcardview_light_background_light:I

.field public static opcardview_light_background_mask_light:I

.field public static orange_100:I

.field public static orange_200:I

.field public static orange_300:I

.field public static orange_400:I

.field public static orange_50:I

.field public static orange_500:I

.field public static orange_600:I

.field public static orange_700:I

.field public static orange_800:I

.field public static orange_900:I

.field public static orange_a100:I

.field public static orange_a200:I

.field public static orange_a400:I

.field public static orange_a700:I

.field public static pink_100:I

.field public static pink_200:I

.field public static pink_300:I

.field public static pink_400:I

.field public static pink_50:I

.field public static pink_500:I

.field public static pink_600:I

.field public static pink_700:I

.field public static pink_800:I

.field public static pink_900:I

.field public static pink_a100:I

.field public static pink_a200:I

.field public static pink_a400:I

.field public static pink_a700:I

.field public static preference_category_color_dark:I

.field public static preference_category_color_light:I

.field public static preference_divider_color_dark:I

.field public static preference_divider_color_light:I

.field public static primary_charm_purple_dark:I

.field public static primary_charm_purple_light:I

.field public static primary_dark_material_dark:I

.field public static primary_dark_material_light:I

.field public static primary_dark_material_light_light_status_bar:I

.field public static primary_default_dark:I

.field public static primary_default_light:I

.field public static primary_fashion_pink_dark:I

.field public static primary_fashion_pink_light:I

.field public static primary_golden_dark:I

.field public static primary_golden_light:I

.field public static primary_grass_green_dark:I

.field public static primary_grass_green_light:I

.field public static primary_lemon_yellow_dark:I

.field public static primary_lemon_yellow_light:I

.field public static primary_material_dark:I

.field public static primary_material_light:I

.field public static primary_sky_blue_dark:I

.field public static primary_sky_blue_light:I

.field public static primary_text_default_material_dark:I

.field public static primary_text_default_material_light:I

.field public static primary_text_disable_only_material_dark:I

.field public static primary_text_disable_only_material_light:I

.field public static primary_text_disabled_material_dark:I

.field public static primary_text_disabled_material_light:I

.field public static primary_text_material_dark:I

.field public static primary_text_material_light:I

.field public static primary_vigour_red_dark:I

.field public static primary_vigour_red_light:I

.field public static progressbar_normal_color_dark:I

.field public static progressbar_normal_color_light:I

.field public static purple_100:I

.field public static purple_200:I

.field public static purple_300:I

.field public static purple_400:I

.field public static purple_50:I

.field public static purple_500:I

.field public static purple_600:I

.field public static purple_700:I

.field public static purple_800:I

.field public static purple_900:I

.field public static purple_a100:I

.field public static purple_a200:I

.field public static purple_a400:I

.field public static purple_a700:I

.field public static radio_button_disable_color_dark:I

.field public static radio_button_disable_color_light:I

.field public static radio_button_normal_color_dark:I

.field public static radio_button_normal_color_light:I

.field public static red_01:I

.field public static red_01_dark:I

.field public static red_01_light:I

.field public static red_02:I

.field public static red_02_dark:I

.field public static red_02_light:I

.field public static red_03:I

.field public static red_03_dark:I

.field public static red_03_light:I

.field public static red_04:I

.field public static red_04_dark:I

.field public static red_04_light:I

.field public static red_05:I

.field public static red_05_dark:I

.field public static red_05_light:I

.field public static red_100:I

.field public static red_200:I

.field public static red_300:I

.field public static red_400:I

.field public static red_50:I

.field public static red_500:I

.field public static red_600:I

.field public static red_700:I

.field public static red_800:I

.field public static red_900:I

.field public static red_a100:I

.field public static red_a200:I

.field public static red_a400:I

.field public static red_a700:I

.field public static ripple_material_dark:I

.field public static ripple_material_light:I

.field public static search_url_text_material_dark:I

.field public static search_url_text_material_light:I

.field public static search_url_text_normal:I

.field public static search_url_text_pressed:I

.field public static search_url_text_selected:I

.field public static secondary_text_default_material_dark:I

.field public static secondary_text_default_material_light:I

.field public static secondary_text_disabled_material_dark:I

.field public static secondary_text_disabled_material_light:I

.field public static secondary_text_material_dark:I

.field public static secondary_text_material_light:I

.field public static seekbar_normal_color_dark:I

.field public static seekbar_normal_color_light:I

.field public static selection_control_disable:I

.field public static selection_control_disable_dark:I

.field public static selection_control_disable_light:I

.field public static selection_control_off:I

.field public static selection_control_off_dark:I

.field public static selection_control_off_light:I

.field public static selection_control_on:I

.field public static selection_control_on_dark:I

.field public static selection_control_on_light:I

.field public static switch_thumb_disable_color_dark:I

.field public static switch_thumb_disable_color_light:I

.field public static switch_thumb_disabled_material_dark:I

.field public static switch_thumb_disabled_material_light:I

.field public static switch_thumb_material_dark:I

.field public static switch_thumb_material_light:I

.field public static switch_thumb_normal_color_dark:I

.field public static switch_thumb_normal_color_light:I

.field public static switch_thumb_normal_material_dark:I

.field public static switch_thumb_normal_material_light:I

.field public static switch_track_activated_color_dark:I

.field public static switch_track_activated_color_light:I

.field public static switch_track_disable_color_dark:I

.field public static switch_track_disable_color_light:I

.field public static switch_track_material:I

.field public static switch_track_normal_color_dark:I

.field public static switch_track_normal_color_light:I

.field public static switch_white_disabled_checked_material:I

.field public static switch_white_disabled_material:I

.field public static switch_white_disabled_material_light:I

.field public static system_blue_purple_100:I

.field public static system_blue_purple_200:I

.field public static system_blue_purple_300:I

.field public static system_blue_purple_400:I

.field public static system_blue_purple_50:I

.field public static system_blue_purple_500:I

.field public static system_blue_purple_600:I

.field public static system_blue_purple_700:I

.field public static system_blue_purple_800:I

.field public static system_blue_purple_900:I

.field public static system_blue_purple_a100:I

.field public static system_blue_purple_a200:I

.field public static system_blue_purple_a400:I

.field public static system_blue_purple_a700:I

.field public static system_default_blue_100:I

.field public static system_default_blue_200:I

.field public static system_default_blue_300:I

.field public static system_default_blue_400:I

.field public static system_default_blue_50:I

.field public static system_default_blue_500:I

.field public static system_default_blue_600:I

.field public static system_default_blue_700:I

.field public static system_default_blue_800:I

.field public static system_default_blue_900:I

.field public static system_default_blue_a100:I

.field public static system_default_blue_a200:I

.field public static system_default_blue_a400:I

.field public static system_default_blue_a700:I

.field public static system_focus:I

.field public static system_focus_dark:I

.field public static system_focus_light:I

.field public static system_warning:I

.field public static system_warning_dark:I

.field public static system_warning_light:I

.field public static teal_100:I

.field public static teal_200:I

.field public static teal_300:I

.field public static teal_400:I

.field public static teal_50:I

.field public static teal_500:I

.field public static teal_600:I

.field public static teal_700:I

.field public static teal_800:I

.field public static teal_900:I

.field public static teal_a100:I

.field public static teal_a200:I

.field public static teal_a400:I

.field public static teal_a700:I

.field public static timepicker_ampmlabel_text_color_dark:I

.field public static timepicker_ampmlabel_text_color_light:I

.field public static timepicker_default_ampm_selected_background_color_material:I

.field public static timepicker_default_ampm_unselected_background_color_material:I

.field public static timepicker_default_background_material:I

.field public static timepicker_default_numbers_background_color_material:I

.field public static timepicker_default_selector_color_material:I

.field public static timepicker_default_text_color_material:I

.field public static timepicker_numbers_background_color_dark:I

.field public static timepicker_numbers_background_color_light:I

.field public static timepicker_numbers_inner_text_color:I

.field public static timepicker_numbers_inner_text_color_dark:I

.field public static timepicker_numbers_selector_color_dark:I

.field public static timepicker_numbers_selector_color_light:I

.field public static timepicker_numbers_text_color_dark:I

.field public static timepicker_numbers_text_color_light:I

.field public static tooltip_background_dark:I

.field public static tooltip_background_light:I

.field public static transparent:I

.field public static white:I

.field public static white_disabled_material:I

.field public static yellow_100:I

.field public static yellow_200:I

.field public static yellow_300:I

.field public static yellow_400:I

.field public static yellow_50:I

.field public static yellow_500:I

.field public static yellow_600:I

.field public static yellow_700:I

.field public static yellow_800:I

.field public static yellow_900:I

.field public static yellow_a100:I

.field public static yellow_a200:I

.field public static yellow_a400:I

.field public static yellow_a700:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4543
    const/4 v0, 0x0

    sput v0, Landroid/support/print/R$color;->abc_background_cache_hint_selector_material_dark:I

    .line 4544
    sput v0, Landroid/support/print/R$color;->abc_background_cache_hint_selector_material_light:I

    .line 4545
    sput v0, Landroid/support/print/R$color;->abc_btn_colored_borderless_text_material:I

    .line 4546
    sput v0, Landroid/support/print/R$color;->abc_btn_colored_text_material:I

    .line 4547
    sput v0, Landroid/support/print/R$color;->abc_color_highlight_material:I

    .line 4548
    sput v0, Landroid/support/print/R$color;->abc_hint_foreground_material_dark:I

    .line 4549
    sput v0, Landroid/support/print/R$color;->abc_hint_foreground_material_light:I

    .line 4550
    sput v0, Landroid/support/print/R$color;->abc_input_method_navigation_guard:I

    .line 4551
    sput v0, Landroid/support/print/R$color;->abc_primary_text_disable_only_material_dark:I

    .line 4552
    sput v0, Landroid/support/print/R$color;->abc_primary_text_disable_only_material_light:I

    .line 4553
    sput v0, Landroid/support/print/R$color;->abc_primary_text_material_dark:I

    .line 4554
    sput v0, Landroid/support/print/R$color;->abc_primary_text_material_light:I

    .line 4555
    sput v0, Landroid/support/print/R$color;->abc_search_url_text:I

    .line 4556
    sput v0, Landroid/support/print/R$color;->abc_search_url_text_normal:I

    .line 4557
    sput v0, Landroid/support/print/R$color;->abc_search_url_text_pressed:I

    .line 4558
    sput v0, Landroid/support/print/R$color;->abc_search_url_text_selected:I

    .line 4559
    sput v0, Landroid/support/print/R$color;->abc_secondary_text_material_dark:I

    .line 4560
    sput v0, Landroid/support/print/R$color;->abc_secondary_text_material_light:I

    .line 4561
    sput v0, Landroid/support/print/R$color;->abc_tint_btn_checkable:I

    .line 4562
    sput v0, Landroid/support/print/R$color;->abc_tint_default:I

    .line 4563
    sput v0, Landroid/support/print/R$color;->abc_tint_edittext:I

    .line 4564
    sput v0, Landroid/support/print/R$color;->abc_tint_seek_thumb:I

    .line 4565
    sput v0, Landroid/support/print/R$color;->abc_tint_spinner:I

    .line 4566
    sput v0, Landroid/support/print/R$color;->abc_tint_switch_track:I

    .line 4567
    sput v0, Landroid/support/print/R$color;->accent_charm_purple_dark:I

    .line 4568
    sput v0, Landroid/support/print/R$color;->accent_charm_purple_light:I

    .line 4572
    sput v0, Landroid/support/print/R$color;->accent_default_dark:I

    .line 4576
    sput v0, Landroid/support/print/R$color;->accent_default_light:I

    .line 4577
    sput v0, Landroid/support/print/R$color;->accent_fashion_pink_dark:I

    .line 4578
    sput v0, Landroid/support/print/R$color;->accent_fashion_pink_light:I

    .line 4579
    sput v0, Landroid/support/print/R$color;->accent_golden_dark:I

    .line 4580
    sput v0, Landroid/support/print/R$color;->accent_golden_light:I

    .line 4581
    sput v0, Landroid/support/print/R$color;->accent_grass_green_dark:I

    .line 4582
    sput v0, Landroid/support/print/R$color;->accent_grass_green_light:I

    .line 4583
    sput v0, Landroid/support/print/R$color;->accent_lemon_yellow_dark:I

    .line 4584
    sput v0, Landroid/support/print/R$color;->accent_lemon_yellow_light:I

    .line 4585
    sput v0, Landroid/support/print/R$color;->accent_material_dark:I

    .line 4586
    sput v0, Landroid/support/print/R$color;->accent_material_light:I

    .line 4587
    sput v0, Landroid/support/print/R$color;->accent_sky_blue_dark:I

    .line 4588
    sput v0, Landroid/support/print/R$color;->accent_sky_blue_light:I

    .line 4592
    sput v0, Landroid/support/print/R$color;->accent_star_wars:I

    .line 4593
    sput v0, Landroid/support/print/R$color;->accent_vigour_red_dark:I

    .line 4594
    sput v0, Landroid/support/print/R$color;->accent_vigour_red_light:I

    .line 4598
    sput v0, Landroid/support/print/R$color;->actionBarSearchViewIconNormal:I

    .line 4599
    sput v0, Landroid/support/print/R$color;->action_bar_subtitle_color_dark:I

    .line 4600
    sput v0, Landroid/support/print/R$color;->action_bar_subtitle_color_light:I

    .line 4601
    sput v0, Landroid/support/print/R$color;->action_bar_title_color_dark:I

    .line 4605
    sput v0, Landroid/support/print/R$color;->action_bar_title_color_light:I

    .line 4609
    sput v0, Landroid/support/print/R$color;->actionbar_background_color_dark:I

    .line 4610
    sput v0, Landroid/support/print/R$color;->actionbar_blue:I

    .line 4611
    sput v0, Landroid/support/print/R$color;->actionbar_coffee:I

    .line 4612
    sput v0, Landroid/support/print/R$color;->actionbar_contacts:I

    .line 4613
    sput v0, Landroid/support/print/R$color;->actionbar_flint:I

    .line 4617
    sput v0, Landroid/support/print/R$color;->actionbar_green:I

    .line 4618
    sput v0, Landroid/support/print/R$color;->actionbar_iris:I

    .line 4619
    sput v0, Landroid/support/print/R$color;->actionbar_lilac:I

    .line 4620
    sput v0, Landroid/support/print/R$color;->actionbar_message:I

    .line 4621
    sput v0, Landroid/support/print/R$color;->actionbar_olive:I

    .line 4622
    sput v0, Landroid/support/print/R$color;->actionbar_punch:I

    .line 4623
    sput v0, Landroid/support/print/R$color;->actionbar_red:I

    .line 4624
    sput v0, Landroid/support/print/R$color;->actionbar_sage:I

    .line 4625
    sput v0, Landroid/support/print/R$color;->actionbar_settings:I

    .line 4626
    sput v0, Landroid/support/print/R$color;->actionbar_wood:I

    .line 4627
    sput v0, Landroid/support/print/R$color;->alert_dialog_color_accent_dark:I

    .line 4628
    sput v0, Landroid/support/print/R$color;->alert_dialog_color_accent_light:I

    .line 4629
    sput v0, Landroid/support/print/R$color;->alert_dialog_color_background_dark:I

    .line 4630
    sput v0, Landroid/support/print/R$color;->alert_dialog_color_background_light:I

    .line 4631
    sput v0, Landroid/support/print/R$color;->alert_dialog_title_style_font_color_dark:I

    .line 4632
    sput v0, Landroid/support/print/R$color;->alert_dialog_title_style_font_color_light:I

    .line 4633
    sput v0, Landroid/support/print/R$color;->amber_100:I

    .line 4634
    sput v0, Landroid/support/print/R$color;->amber_200:I

    .line 4635
    sput v0, Landroid/support/print/R$color;->amber_300:I

    .line 4636
    sput v0, Landroid/support/print/R$color;->amber_400:I

    .line 4637
    sput v0, Landroid/support/print/R$color;->amber_50:I

    .line 4638
    sput v0, Landroid/support/print/R$color;->amber_500:I

    .line 4639
    sput v0, Landroid/support/print/R$color;->amber_600:I

    .line 4640
    sput v0, Landroid/support/print/R$color;->amber_700:I

    .line 4641
    sput v0, Landroid/support/print/R$color;->amber_800:I

    .line 4642
    sput v0, Landroid/support/print/R$color;->amber_900:I

    .line 4643
    sput v0, Landroid/support/print/R$color;->amber_a100:I

    .line 4644
    sput v0, Landroid/support/print/R$color;->amber_a200:I

    .line 4645
    sput v0, Landroid/support/print/R$color;->amber_a400:I

    .line 4646
    sput v0, Landroid/support/print/R$color;->amber_a700:I

    .line 4647
    sput v0, Landroid/support/print/R$color;->android_ripple_material_dark:I

    .line 4648
    sput v0, Landroid/support/print/R$color;->android_ripple_material_light:I

    .line 4649
    sput v0, Landroid/support/print/R$color;->background_cache_hint_selector_material_dark:I

    .line 4650
    sput v0, Landroid/support/print/R$color;->background_cache_hint_selector_material_light:I

    .line 4651
    sput v0, Landroid/support/print/R$color;->background_floating_material_dark:I

    .line 4652
    sput v0, Landroid/support/print/R$color;->background_floating_material_light:I

    .line 4653
    sput v0, Landroid/support/print/R$color;->background_material_dark:I

    .line 4654
    sput v0, Landroid/support/print/R$color;->background_material_light:I

    .line 4655
    sput v0, Landroid/support/print/R$color;->bg_ab:I

    .line 4659
    sput v0, Landroid/support/print/R$color;->bg_ab_dark:I

    .line 4660
    sput v0, Landroid/support/print/R$color;->bg_ab_light:I

    .line 4664
    sput v0, Landroid/support/print/R$color;->bg_dialog:I

    .line 4665
    sput v0, Landroid/support/print/R$color;->bg_dialog_dark:I

    .line 4669
    sput v0, Landroid/support/print/R$color;->bg_dialog_light:I

    .line 4670
    sput v0, Landroid/support/print/R$color;->bg_interface:I

    .line 4671
    sput v0, Landroid/support/print/R$color;->bg_interface_dark:I

    .line 4672
    sput v0, Landroid/support/print/R$color;->bg_interface_light:I

    .line 4673
    sput v0, Landroid/support/print/R$color;->black:I

    .line 4674
    sput v0, Landroid/support/print/R$color;->blue_01:I

    .line 4675
    sput v0, Landroid/support/print/R$color;->blue_01_dark:I

    .line 4676
    sput v0, Landroid/support/print/R$color;->blue_01_light:I

    .line 4677
    sput v0, Landroid/support/print/R$color;->blue_02:I

    .line 4678
    sput v0, Landroid/support/print/R$color;->blue_02_dark:I

    .line 4679
    sput v0, Landroid/support/print/R$color;->blue_02_light:I

    .line 4680
    sput v0, Landroid/support/print/R$color;->blue_03:I

    .line 4681
    sput v0, Landroid/support/print/R$color;->blue_03_dark:I

    .line 4682
    sput v0, Landroid/support/print/R$color;->blue_03_light:I

    .line 4683
    sput v0, Landroid/support/print/R$color;->blue_04:I

    .line 4684
    sput v0, Landroid/support/print/R$color;->blue_04_dark:I

    .line 4685
    sput v0, Landroid/support/print/R$color;->blue_04_light:I

    .line 4686
    sput v0, Landroid/support/print/R$color;->blue_05:I

    .line 4687
    sput v0, Landroid/support/print/R$color;->blue_05_dark:I

    .line 4688
    sput v0, Landroid/support/print/R$color;->blue_05_light:I

    .line 4689
    sput v0, Landroid/support/print/R$color;->blue_100:I

    .line 4690
    sput v0, Landroid/support/print/R$color;->blue_200:I

    .line 4691
    sput v0, Landroid/support/print/R$color;->blue_300:I

    .line 4692
    sput v0, Landroid/support/print/R$color;->blue_400:I

    .line 4693
    sput v0, Landroid/support/print/R$color;->blue_50:I

    .line 4694
    sput v0, Landroid/support/print/R$color;->blue_500:I

    .line 4695
    sput v0, Landroid/support/print/R$color;->blue_600:I

    .line 4696
    sput v0, Landroid/support/print/R$color;->blue_700:I

    .line 4697
    sput v0, Landroid/support/print/R$color;->blue_800:I

    .line 4698
    sput v0, Landroid/support/print/R$color;->blue_900:I

    .line 4699
    sput v0, Landroid/support/print/R$color;->blue_a100:I

    .line 4700
    sput v0, Landroid/support/print/R$color;->blue_a200:I

    .line 4701
    sput v0, Landroid/support/print/R$color;->blue_a400:I

    .line 4702
    sput v0, Landroid/support/print/R$color;->blue_a700:I

    .line 4703
    sput v0, Landroid/support/print/R$color;->bluegrey_100:I

    .line 4704
    sput v0, Landroid/support/print/R$color;->bluegrey_200:I

    .line 4705
    sput v0, Landroid/support/print/R$color;->bluegrey_300:I

    .line 4706
    sput v0, Landroid/support/print/R$color;->bluegrey_400:I

    .line 4707
    sput v0, Landroid/support/print/R$color;->bluegrey_50:I

    .line 4708
    sput v0, Landroid/support/print/R$color;->bluegrey_500:I

    .line 4709
    sput v0, Landroid/support/print/R$color;->bluegrey_600:I

    .line 4710
    sput v0, Landroid/support/print/R$color;->bluegrey_700:I

    .line 4711
    sput v0, Landroid/support/print/R$color;->bluegrey_800:I

    .line 4712
    sput v0, Landroid/support/print/R$color;->bluegrey_900:I

    .line 4716
    sput v0, Landroid/support/print/R$color;->bottom_navigation_badge_background_color:I

    .line 4717
    sput v0, Landroid/support/print/R$color;->bottom_navigation_badge_text_color:I

    .line 4718
    sput v0, Landroid/support/print/R$color;->bright_foreground_disabled_material_dark:I

    .line 4719
    sput v0, Landroid/support/print/R$color;->bright_foreground_disabled_material_light:I

    .line 4720
    sput v0, Landroid/support/print/R$color;->bright_foreground_inverse_material_dark:I

    .line 4721
    sput v0, Landroid/support/print/R$color;->bright_foreground_inverse_material_light:I

    .line 4722
    sput v0, Landroid/support/print/R$color;->bright_foreground_material_dark:I

    .line 4723
    sput v0, Landroid/support/print/R$color;->bright_foreground_material_light:I

    .line 4724
    sput v0, Landroid/support/print/R$color;->brown_100:I

    .line 4725
    sput v0, Landroid/support/print/R$color;->brown_200:I

    .line 4726
    sput v0, Landroid/support/print/R$color;->brown_300:I

    .line 4727
    sput v0, Landroid/support/print/R$color;->brown_400:I

    .line 4728
    sput v0, Landroid/support/print/R$color;->brown_50:I

    .line 4729
    sput v0, Landroid/support/print/R$color;->brown_500:I

    .line 4730
    sput v0, Landroid/support/print/R$color;->brown_600:I

    .line 4731
    sput v0, Landroid/support/print/R$color;->brown_700:I

    .line 4732
    sput v0, Landroid/support/print/R$color;->brown_800:I

    .line 4733
    sput v0, Landroid/support/print/R$color;->brown_900:I

    .line 4734
    sput v0, Landroid/support/print/R$color;->btn_colorControlHighlight_dark:I

    .line 4738
    sput v0, Landroid/support/print/R$color;->btn_colorControlHighlight_light:I

    .line 4739
    sput v0, Landroid/support/print/R$color;->btn_colored_text_material:I

    .line 4740
    sput v0, Landroid/support/print/R$color;->btn_default_material_dark:I

    .line 4741
    sput v0, Landroid/support/print/R$color;->btn_default_material_light:I

    .line 4745
    sput v0, Landroid/support/print/R$color;->btn_foreground_material:I

    .line 4746
    sput v0, Landroid/support/print/R$color;->btn_foreground_material_dark:I

    .line 4747
    sput v0, Landroid/support/print/R$color;->btn_foreground_material_light:I

    .line 4751
    sput v0, Landroid/support/print/R$color;->button_disable_color_dark:I

    .line 4755
    sput v0, Landroid/support/print/R$color;->button_disable_color_light:I

    .line 4756
    sput v0, Landroid/support/print/R$color;->button_material_dark:I

    .line 4757
    sput v0, Landroid/support/print/R$color;->button_material_light:I

    .line 4761
    sput v0, Landroid/support/print/R$color;->button_normal_color_dark:I

    .line 4765
    sput v0, Landroid/support/print/R$color;->button_normal_color_light:I

    .line 4769
    sput v0, Landroid/support/print/R$color;->cardview_dark_background:I

    .line 4773
    sput v0, Landroid/support/print/R$color;->cardview_light_background:I

    .line 4777
    sput v0, Landroid/support/print/R$color;->cardview_shadow_end_color:I

    .line 4781
    sput v0, Landroid/support/print/R$color;->cardview_shadow_start_color:I

    .line 4782
    sput v0, Landroid/support/print/R$color;->checkbox_button_disable_color_dark:I

    .line 4783
    sput v0, Landroid/support/print/R$color;->checkbox_button_disable_color_light:I

    .line 4787
    sput v0, Landroid/support/print/R$color;->checkbox_button_normal_color_dark:I

    .line 4788
    sput v0, Landroid/support/print/R$color;->checkbox_button_normal_color_light:I

    .line 4792
    sput v0, Landroid/support/print/R$color;->colorButtonNormal:I

    .line 4793
    sput v0, Landroid/support/print/R$color;->colorpicker_disabled_cell_color:I

    .line 4797
    sput v0, Landroid/support/print/R$color;->colorpicker_ripple_effect_color:I

    .line 4798
    sput v0, Landroid/support/print/R$color;->control_disable_text_color_dark:I

    .line 4799
    sput v0, Landroid/support/print/R$color;->control_disable_text_color_light:I

    .line 4800
    sput v0, Landroid/support/print/R$color;->control_highlight_material:I

    .line 4804
    sput v0, Landroid/support/print/R$color;->control_normal_color:I

    .line 4805
    sput v0, Landroid/support/print/R$color;->control_normal_color_light:I

    .line 4806
    sput v0, Landroid/support/print/R$color;->control_primary_text_color_dark:I

    .line 4807
    sput v0, Landroid/support/print/R$color;->control_primary_text_color_light:I

    .line 4808
    sput v0, Landroid/support/print/R$color;->control_secondary_text_color_dark:I

    .line 4809
    sput v0, Landroid/support/print/R$color;->control_secondary_text_color_light:I

    .line 4810
    sput v0, Landroid/support/print/R$color;->cyan_100:I

    .line 4811
    sput v0, Landroid/support/print/R$color;->cyan_200:I

    .line 4812
    sput v0, Landroid/support/print/R$color;->cyan_300:I

    .line 4813
    sput v0, Landroid/support/print/R$color;->cyan_400:I

    .line 4814
    sput v0, Landroid/support/print/R$color;->cyan_50:I

    .line 4815
    sput v0, Landroid/support/print/R$color;->cyan_500:I

    .line 4816
    sput v0, Landroid/support/print/R$color;->cyan_600:I

    .line 4817
    sput v0, Landroid/support/print/R$color;->cyan_700:I

    .line 4818
    sput v0, Landroid/support/print/R$color;->cyan_800:I

    .line 4819
    sput v0, Landroid/support/print/R$color;->cyan_900:I

    .line 4820
    sput v0, Landroid/support/print/R$color;->cyan_a100:I

    .line 4821
    sput v0, Landroid/support/print/R$color;->cyan_a200:I

    .line 4822
    sput v0, Landroid/support/print/R$color;->cyan_a400:I

    .line 4823
    sput v0, Landroid/support/print/R$color;->cyan_a700:I

    .line 4824
    sput v0, Landroid/support/print/R$color;->datepicker_default_circle_background_color_material_dark:I

    .line 4825
    sput v0, Landroid/support/print/R$color;->datepicker_default_circle_background_color_material_light:I

    .line 4826
    sput v0, Landroid/support/print/R$color;->datepicker_default_disabled_text_color_material_dark:I

    .line 4827
    sput v0, Landroid/support/print/R$color;->datepicker_default_disabled_text_color_material_light:I

    .line 4828
    sput v0, Landroid/support/print/R$color;->datepicker_default_header_dayofweek_background_color_material_dark:I

    .line 4829
    sput v0, Landroid/support/print/R$color;->datepicker_default_header_dayofweek_background_color_material_light:I

    .line 4830
    sput v0, Landroid/support/print/R$color;->datepicker_default_header_selector_background_material_dark:I

    .line 4831
    sput v0, Landroid/support/print/R$color;->datepicker_default_header_selector_background_material_light:I

    .line 4832
    sput v0, Landroid/support/print/R$color;->datepicker_default_normal_text_color_material_dark:I

    .line 4833
    sput v0, Landroid/support/print/R$color;->datepicker_default_normal_text_color_material_light:I

    .line 4834
    sput v0, Landroid/support/print/R$color;->datepicker_default_pressed_text_color_material_dark:I

    .line 4835
    sput v0, Landroid/support/print/R$color;->datepicker_default_pressed_text_color_material_light:I

    .line 4836
    sput v0, Landroid/support/print/R$color;->datepicker_default_selected_text_color_material_dark:I

    .line 4837
    sput v0, Landroid/support/print/R$color;->datepicker_default_selected_text_color_material_light:I

    .line 4838
    sput v0, Landroid/support/print/R$color;->datepicker_default_view_animator_color_material_dark:I

    .line 4839
    sput v0, Landroid/support/print/R$color;->datepicker_default_view_animator_color_material_light:I

    .line 4843
    sput v0, Landroid/support/print/R$color;->datepicker_timepicker_header_selected_activated_text_color_dark:I

    .line 4844
    sput v0, Landroid/support/print/R$color;->datepicker_timepicker_header_selected_text_color_dark:I

    .line 4845
    sput v0, Landroid/support/print/R$color;->deeporange_100:I

    .line 4846
    sput v0, Landroid/support/print/R$color;->deeporange_200:I

    .line 4847
    sput v0, Landroid/support/print/R$color;->deeporange_300:I

    .line 4848
    sput v0, Landroid/support/print/R$color;->deeporange_400:I

    .line 4849
    sput v0, Landroid/support/print/R$color;->deeporange_50:I

    .line 4850
    sput v0, Landroid/support/print/R$color;->deeporange_500:I

    .line 4851
    sput v0, Landroid/support/print/R$color;->deeporange_600:I

    .line 4852
    sput v0, Landroid/support/print/R$color;->deeporange_700:I

    .line 4853
    sput v0, Landroid/support/print/R$color;->deeporange_800:I

    .line 4854
    sput v0, Landroid/support/print/R$color;->deeporange_900:I

    .line 4855
    sput v0, Landroid/support/print/R$color;->deeporange_a100:I

    .line 4856
    sput v0, Landroid/support/print/R$color;->deeporange_a200:I

    .line 4857
    sput v0, Landroid/support/print/R$color;->deeporange_a400:I

    .line 4858
    sput v0, Landroid/support/print/R$color;->deeporange_a700:I

    .line 4859
    sput v0, Landroid/support/print/R$color;->deeppurple_100:I

    .line 4860
    sput v0, Landroid/support/print/R$color;->deeppurple_200:I

    .line 4861
    sput v0, Landroid/support/print/R$color;->deeppurple_300:I

    .line 4862
    sput v0, Landroid/support/print/R$color;->deeppurple_400:I

    .line 4863
    sput v0, Landroid/support/print/R$color;->deeppurple_50:I

    .line 4864
    sput v0, Landroid/support/print/R$color;->deeppurple_500:I

    .line 4865
    sput v0, Landroid/support/print/R$color;->deeppurple_600:I

    .line 4866
    sput v0, Landroid/support/print/R$color;->deeppurple_700:I

    .line 4867
    sput v0, Landroid/support/print/R$color;->deeppurple_800:I

    .line 4868
    sput v0, Landroid/support/print/R$color;->deeppurple_900:I

    .line 4869
    sput v0, Landroid/support/print/R$color;->deeppurple_a100:I

    .line 4870
    sput v0, Landroid/support/print/R$color;->deeppurple_a200:I

    .line 4871
    sput v0, Landroid/support/print/R$color;->deeppurple_a400:I

    .line 4872
    sput v0, Landroid/support/print/R$color;->deeppurple_a700:I

    .line 4876
    sput v0, Landroid/support/print/R$color;->design_bottom_navigation_shadow_color:I

    .line 4880
    sput v0, Landroid/support/print/R$color;->design_fab_shadow_end_color:I

    .line 4884
    sput v0, Landroid/support/print/R$color;->design_fab_shadow_mid_color:I

    .line 4888
    sput v0, Landroid/support/print/R$color;->design_fab_shadow_start_color:I

    .line 4892
    sput v0, Landroid/support/print/R$color;->design_fab_stroke_end_inner_color:I

    .line 4896
    sput v0, Landroid/support/print/R$color;->design_fab_stroke_end_outer_color:I

    .line 4900
    sput v0, Landroid/support/print/R$color;->design_fab_stroke_top_inner_color:I

    .line 4904
    sput v0, Landroid/support/print/R$color;->design_fab_stroke_top_outer_color:I

    .line 4905
    sput v0, Landroid/support/print/R$color;->dialog_color_background:I

    .line 4906
    sput v0, Landroid/support/print/R$color;->dialog_color_background_dark:I

    .line 4907
    sput v0, Landroid/support/print/R$color;->dialog_header_color_background:I

    .line 4908
    sput v0, Landroid/support/print/R$color;->dialog_header_color_background_dark:I

    .line 4909
    sput v0, Landroid/support/print/R$color;->dim_foreground_disabled_material_dark:I

    .line 4910
    sput v0, Landroid/support/print/R$color;->dim_foreground_disabled_material_light:I

    .line 4911
    sput v0, Landroid/support/print/R$color;->dim_foreground_material_dark:I

    .line 4912
    sput v0, Landroid/support/print/R$color;->dim_foreground_material_light:I

    .line 4913
    sput v0, Landroid/support/print/R$color;->divider_crude:I

    .line 4914
    sput v0, Landroid/support/print/R$color;->divider_crude_dark:I

    .line 4915
    sput v0, Landroid/support/print/R$color;->divider_crude_light:I

    .line 4919
    sput v0, Landroid/support/print/R$color;->divider_fine:I

    .line 4923
    sput v0, Landroid/support/print/R$color;->divider_fine_dark:I

    .line 4927
    sput v0, Landroid/support/print/R$color;->divider_fine_light:I

    .line 4928
    sput v0, Landroid/support/print/R$color;->edittext_color:I

    .line 4929
    sput v0, Landroid/support/print/R$color;->edittext_color_dark:I

    .line 4930
    sput v0, Landroid/support/print/R$color;->edittext_error_color:I

    .line 4931
    sput v0, Landroid/support/print/R$color;->edittext_error_color_dark:I

    .line 4932
    sput v0, Landroid/support/print/R$color;->edittext_hint_color:I

    .line 4933
    sput v0, Landroid/support/print/R$color;->edittext_hint_color_dark:I

    .line 4937
    sput v0, Landroid/support/print/R$color;->edittext_normal_color:I

    .line 4938
    sput v0, Landroid/support/print/R$color;->edittext_normal_color_dark:I

    .line 4939
    sput v0, Landroid/support/print/R$color;->error_color_material_dark:I

    .line 4940
    sput v0, Landroid/support/print/R$color;->error_color_material_light:I

    .line 4944
    sput v0, Landroid/support/print/R$color;->font_ab_title:I

    .line 4948
    sput v0, Landroid/support/print/R$color;->font_ab_title_dark:I

    .line 4952
    sput v0, Landroid/support/print/R$color;->font_ab_title_light:I

    .line 4953
    sput v0, Landroid/support/print/R$color;->font_dialog_text:I

    .line 4954
    sput v0, Landroid/support/print/R$color;->font_dialog_text_dark:I

    .line 4955
    sput v0, Landroid/support/print/R$color;->font_dialog_text_light:I

    .line 4956
    sput v0, Landroid/support/print/R$color;->font_dialog_title:I

    .line 4957
    sput v0, Landroid/support/print/R$color;->font_dialog_title_dark:I

    .line 4958
    sput v0, Landroid/support/print/R$color;->font_dialog_title_light:I

    .line 4959
    sput v0, Landroid/support/print/R$color;->font_list_settings_title:I

    .line 4960
    sput v0, Landroid/support/print/R$color;->font_list_settings_title_dark:I

    .line 4961
    sput v0, Landroid/support/print/R$color;->font_list_settings_title_light:I

    .line 4962
    sput v0, Landroid/support/print/R$color;->font_list_subtitle:I

    .line 4963
    sput v0, Landroid/support/print/R$color;->font_list_subtitle_dark:I

    .line 4964
    sput v0, Landroid/support/print/R$color;->font_list_subtitle_light:I

    .line 4965
    sput v0, Landroid/support/print/R$color;->font_list_text:I

    .line 4966
    sput v0, Landroid/support/print/R$color;->font_list_text_dark:I

    .line 4967
    sput v0, Landroid/support/print/R$color;->font_list_text_light:I

    .line 4968
    sput v0, Landroid/support/print/R$color;->font_tab_off:I

    .line 4969
    sput v0, Landroid/support/print/R$color;->font_tab_off_dark:I

    .line 4970
    sput v0, Landroid/support/print/R$color;->font_tab_off_light:I

    .line 4971
    sput v0, Landroid/support/print/R$color;->font_tab_on:I

    .line 4972
    sput v0, Landroid/support/print/R$color;->font_tab_on_dark:I

    .line 4973
    sput v0, Landroid/support/print/R$color;->font_tab_on_light:I

    .line 4974
    sput v0, Landroid/support/print/R$color;->foreground_material_dark:I

    .line 4975
    sput v0, Landroid/support/print/R$color;->foreground_material_light:I

    .line 4979
    sput v0, Landroid/support/print/R$color;->gradation_01:I

    .line 4983
    sput v0, Landroid/support/print/R$color;->gradation_01_dark:I

    .line 4987
    sput v0, Landroid/support/print/R$color;->gradation_01_light:I

    .line 4988
    sput v0, Landroid/support/print/R$color;->gradation_02:I

    .line 4989
    sput v0, Landroid/support/print/R$color;->gradation_02_dark:I

    .line 4990
    sput v0, Landroid/support/print/R$color;->gradation_02_light:I

    .line 4991
    sput v0, Landroid/support/print/R$color;->gradation_03:I

    .line 4992
    sput v0, Landroid/support/print/R$color;->gradation_03_dark:I

    .line 4993
    sput v0, Landroid/support/print/R$color;->gradation_03_light:I

    .line 4994
    sput v0, Landroid/support/print/R$color;->gradation_04:I

    .line 4995
    sput v0, Landroid/support/print/R$color;->gradation_04_dark:I

    .line 4996
    sput v0, Landroid/support/print/R$color;->gradation_04_light:I

    .line 4997
    sput v0, Landroid/support/print/R$color;->gradation_05:I

    .line 4998
    sput v0, Landroid/support/print/R$color;->gradation_05_dark:I

    .line 4999
    sput v0, Landroid/support/print/R$color;->gradation_05_light:I

    .line 5000
    sput v0, Landroid/support/print/R$color;->gradation_06:I

    .line 5001
    sput v0, Landroid/support/print/R$color;->gradation_06_dark:I

    .line 5002
    sput v0, Landroid/support/print/R$color;->gradation_06_light:I

    .line 5003
    sput v0, Landroid/support/print/R$color;->gradation_07:I

    .line 5004
    sput v0, Landroid/support/print/R$color;->gradation_07_dark:I

    .line 5005
    sput v0, Landroid/support/print/R$color;->gradation_07_light:I

    .line 5006
    sput v0, Landroid/support/print/R$color;->gradation_08:I

    .line 5007
    sput v0, Landroid/support/print/R$color;->gradation_08_dark:I

    .line 5008
    sput v0, Landroid/support/print/R$color;->gradation_08_light:I

    .line 5009
    sput v0, Landroid/support/print/R$color;->gradation_09:I

    .line 5010
    sput v0, Landroid/support/print/R$color;->gradation_09_dark:I

    .line 5011
    sput v0, Landroid/support/print/R$color;->gradation_09_light:I

    .line 5012
    sput v0, Landroid/support/print/R$color;->gradation_10:I

    .line 5013
    sput v0, Landroid/support/print/R$color;->gradation_10_dark:I

    .line 5014
    sput v0, Landroid/support/print/R$color;->gradation_10_light:I

    .line 5015
    sput v0, Landroid/support/print/R$color;->gradation_11:I

    .line 5016
    sput v0, Landroid/support/print/R$color;->gradation_11_dark:I

    .line 5017
    sput v0, Landroid/support/print/R$color;->gradation_11_light:I

    .line 5018
    sput v0, Landroid/support/print/R$color;->green_01:I

    .line 5019
    sput v0, Landroid/support/print/R$color;->green_01_dark:I

    .line 5020
    sput v0, Landroid/support/print/R$color;->green_01_light:I

    .line 5021
    sput v0, Landroid/support/print/R$color;->green_02:I

    .line 5022
    sput v0, Landroid/support/print/R$color;->green_02_dark:I

    .line 5023
    sput v0, Landroid/support/print/R$color;->green_02_light:I

    .line 5024
    sput v0, Landroid/support/print/R$color;->green_03:I

    .line 5025
    sput v0, Landroid/support/print/R$color;->green_03_dark:I

    .line 5026
    sput v0, Landroid/support/print/R$color;->green_03_light:I

    .line 5027
    sput v0, Landroid/support/print/R$color;->green_100:I

    .line 5028
    sput v0, Landroid/support/print/R$color;->green_200:I

    .line 5029
    sput v0, Landroid/support/print/R$color;->green_300:I

    .line 5030
    sput v0, Landroid/support/print/R$color;->green_400:I

    .line 5031
    sput v0, Landroid/support/print/R$color;->green_50:I

    .line 5032
    sput v0, Landroid/support/print/R$color;->green_500:I

    .line 5033
    sput v0, Landroid/support/print/R$color;->green_600:I

    .line 5034
    sput v0, Landroid/support/print/R$color;->green_700:I

    .line 5035
    sput v0, Landroid/support/print/R$color;->green_800:I

    .line 5036
    sput v0, Landroid/support/print/R$color;->green_900:I

    .line 5037
    sput v0, Landroid/support/print/R$color;->green_a100:I

    .line 5038
    sput v0, Landroid/support/print/R$color;->green_a200:I

    .line 5039
    sput v0, Landroid/support/print/R$color;->green_a400:I

    .line 5040
    sput v0, Landroid/support/print/R$color;->green_a700:I

    .line 5041
    sput v0, Landroid/support/print/R$color;->grey:I

    .line 5042
    sput v0, Landroid/support/print/R$color;->grey_01:I

    .line 5043
    sput v0, Landroid/support/print/R$color;->grey_01_dark:I

    .line 5044
    sput v0, Landroid/support/print/R$color;->grey_01_light:I

    .line 5045
    sput v0, Landroid/support/print/R$color;->grey_02:I

    .line 5046
    sput v0, Landroid/support/print/R$color;->grey_02_dark:I

    .line 5047
    sput v0, Landroid/support/print/R$color;->grey_02_light:I

    .line 5048
    sput v0, Landroid/support/print/R$color;->grey_03:I

    .line 5049
    sput v0, Landroid/support/print/R$color;->grey_03_dark:I

    .line 5050
    sput v0, Landroid/support/print/R$color;->grey_03_light:I

    .line 5051
    sput v0, Landroid/support/print/R$color;->grey_04:I

    .line 5052
    sput v0, Landroid/support/print/R$color;->grey_04_dark:I

    .line 5053
    sput v0, Landroid/support/print/R$color;->grey_04_light:I

    .line 5054
    sput v0, Landroid/support/print/R$color;->grey_05:I

    .line 5055
    sput v0, Landroid/support/print/R$color;->grey_05_dark:I

    .line 5056
    sput v0, Landroid/support/print/R$color;->grey_05_light:I

    .line 5057
    sput v0, Landroid/support/print/R$color;->grey_06:I

    .line 5058
    sput v0, Landroid/support/print/R$color;->grey_06_dark:I

    .line 5059
    sput v0, Landroid/support/print/R$color;->grey_06_light:I

    .line 5060
    sput v0, Landroid/support/print/R$color;->grey_07:I

    .line 5061
    sput v0, Landroid/support/print/R$color;->grey_07_dark:I

    .line 5062
    sput v0, Landroid/support/print/R$color;->grey_07_light:I

    .line 5063
    sput v0, Landroid/support/print/R$color;->grey_100:I

    .line 5064
    sput v0, Landroid/support/print/R$color;->grey_1000:I

    .line 5065
    sput v0, Landroid/support/print/R$color;->grey_200:I

    .line 5066
    sput v0, Landroid/support/print/R$color;->grey_300:I

    .line 5067
    sput v0, Landroid/support/print/R$color;->grey_400:I

    .line 5068
    sput v0, Landroid/support/print/R$color;->grey_50:I

    .line 5069
    sput v0, Landroid/support/print/R$color;->grey_500:I

    .line 5070
    sput v0, Landroid/support/print/R$color;->grey_600:I

    .line 5071
    sput v0, Landroid/support/print/R$color;->grey_700:I

    .line 5072
    sput v0, Landroid/support/print/R$color;->grey_800:I

    .line 5073
    sput v0, Landroid/support/print/R$color;->grey_900:I

    .line 5074
    sput v0, Landroid/support/print/R$color;->grey_a100:I

    .line 5075
    sput v0, Landroid/support/print/R$color;->grey_dark:I

    .line 5076
    sput v0, Landroid/support/print/R$color;->grey_light:I

    .line 5077
    sput v0, Landroid/support/print/R$color;->highlighted_text_material:I

    .line 5078
    sput v0, Landroid/support/print/R$color;->highlighted_text_material_dark:I

    .line 5079
    sput v0, Landroid/support/print/R$color;->highlighted_text_material_light:I

    .line 5080
    sput v0, Landroid/support/print/R$color;->indigo_100:I

    .line 5081
    sput v0, Landroid/support/print/R$color;->indigo_200:I

    .line 5082
    sput v0, Landroid/support/print/R$color;->indigo_300:I

    .line 5083
    sput v0, Landroid/support/print/R$color;->indigo_400:I

    .line 5084
    sput v0, Landroid/support/print/R$color;->indigo_50:I

    .line 5085
    sput v0, Landroid/support/print/R$color;->indigo_500:I

    .line 5086
    sput v0, Landroid/support/print/R$color;->indigo_600:I

    .line 5087
    sput v0, Landroid/support/print/R$color;->indigo_700:I

    .line 5088
    sput v0, Landroid/support/print/R$color;->indigo_800:I

    .line 5089
    sput v0, Landroid/support/print/R$color;->indigo_900:I

    .line 5090
    sput v0, Landroid/support/print/R$color;->indigo_a100:I

    .line 5091
    sput v0, Landroid/support/print/R$color;->indigo_a200:I

    .line 5092
    sput v0, Landroid/support/print/R$color;->indigo_a400:I

    .line 5093
    sput v0, Landroid/support/print/R$color;->indigo_a700:I

    .line 5094
    sput v0, Landroid/support/print/R$color;->lightblue_100:I

    .line 5095
    sput v0, Landroid/support/print/R$color;->lightblue_200:I

    .line 5096
    sput v0, Landroid/support/print/R$color;->lightblue_300:I

    .line 5097
    sput v0, Landroid/support/print/R$color;->lightblue_400:I

    .line 5098
    sput v0, Landroid/support/print/R$color;->lightblue_50:I

    .line 5099
    sput v0, Landroid/support/print/R$color;->lightblue_500:I

    .line 5100
    sput v0, Landroid/support/print/R$color;->lightblue_600:I

    .line 5101
    sput v0, Landroid/support/print/R$color;->lightblue_700:I

    .line 5102
    sput v0, Landroid/support/print/R$color;->lightblue_800:I

    .line 5103
    sput v0, Landroid/support/print/R$color;->lightblue_900:I

    .line 5104
    sput v0, Landroid/support/print/R$color;->lightblue_a100:I

    .line 5105
    sput v0, Landroid/support/print/R$color;->lightblue_a200:I

    .line 5106
    sput v0, Landroid/support/print/R$color;->lightblue_a400:I

    .line 5107
    sput v0, Landroid/support/print/R$color;->lightblue_a700:I

    .line 5108
    sput v0, Landroid/support/print/R$color;->lightgreen_100:I

    .line 5109
    sput v0, Landroid/support/print/R$color;->lightgreen_200:I

    .line 5110
    sput v0, Landroid/support/print/R$color;->lightgreen_300:I

    .line 5111
    sput v0, Landroid/support/print/R$color;->lightgreen_400:I

    .line 5112
    sput v0, Landroid/support/print/R$color;->lightgreen_50:I

    .line 5113
    sput v0, Landroid/support/print/R$color;->lightgreen_500:I

    .line 5114
    sput v0, Landroid/support/print/R$color;->lightgreen_600:I

    .line 5115
    sput v0, Landroid/support/print/R$color;->lightgreen_700:I

    .line 5116
    sput v0, Landroid/support/print/R$color;->lightgreen_800:I

    .line 5117
    sput v0, Landroid/support/print/R$color;->lightgreen_900:I

    .line 5118
    sput v0, Landroid/support/print/R$color;->lightgreen_a100:I

    .line 5119
    sput v0, Landroid/support/print/R$color;->lightgreen_a200:I

    .line 5120
    sput v0, Landroid/support/print/R$color;->lightgreen_a400:I

    .line 5121
    sput v0, Landroid/support/print/R$color;->lightgreen_a700:I

    .line 5122
    sput v0, Landroid/support/print/R$color;->lime_100:I

    .line 5123
    sput v0, Landroid/support/print/R$color;->lime_200:I

    .line 5124
    sput v0, Landroid/support/print/R$color;->lime_300:I

    .line 5125
    sput v0, Landroid/support/print/R$color;->lime_400:I

    .line 5126
    sput v0, Landroid/support/print/R$color;->lime_50:I

    .line 5127
    sput v0, Landroid/support/print/R$color;->lime_500:I

    .line 5128
    sput v0, Landroid/support/print/R$color;->lime_600:I

    .line 5129
    sput v0, Landroid/support/print/R$color;->lime_700:I

    .line 5130
    sput v0, Landroid/support/print/R$color;->lime_800:I

    .line 5131
    sput v0, Landroid/support/print/R$color;->lime_900:I

    .line 5132
    sput v0, Landroid/support/print/R$color;->lime_a100:I

    .line 5133
    sput v0, Landroid/support/print/R$color;->lime_a200:I

    .line 5134
    sput v0, Landroid/support/print/R$color;->lime_a400:I

    .line 5135
    sput v0, Landroid/support/print/R$color;->lime_a700:I

    .line 5139
    sput v0, Landroid/support/print/R$color;->listitem_primarytext_color:I

    .line 5140
    sput v0, Landroid/support/print/R$color;->listitem_secondarttext_color:I

    .line 5141
    sput v0, Landroid/support/print/R$color;->listitem_stamptext_color:I

    .line 5145
    sput v0, Landroid/support/print/R$color;->listview_divider_color:I

    .line 5146
    sput v0, Landroid/support/print/R$color;->listview_divider_color_dark:I

    .line 5147
    sput v0, Landroid/support/print/R$color;->material_blue_grey_800:I

    .line 5148
    sput v0, Landroid/support/print/R$color;->material_blue_grey_900:I

    .line 5149
    sput v0, Landroid/support/print/R$color;->material_blue_grey_950:I

    .line 5150
    sput v0, Landroid/support/print/R$color;->material_deep_teal_200:I

    .line 5151
    sput v0, Landroid/support/print/R$color;->material_deep_teal_500:I

    .line 5152
    sput v0, Landroid/support/print/R$color;->material_grey_100:I

    .line 5153
    sput v0, Landroid/support/print/R$color;->material_grey_300:I

    .line 5154
    sput v0, Landroid/support/print/R$color;->material_grey_50:I

    .line 5155
    sput v0, Landroid/support/print/R$color;->material_grey_600:I

    .line 5156
    sput v0, Landroid/support/print/R$color;->material_grey_800:I

    .line 5157
    sput v0, Landroid/support/print/R$color;->material_grey_850:I

    .line 5158
    sput v0, Landroid/support/print/R$color;->material_grey_900:I

    .line 5159
    sput v0, Landroid/support/print/R$color;->mtrl_bottom_nav_colored_item_tint:I

    .line 5160
    sput v0, Landroid/support/print/R$color;->mtrl_bottom_nav_item_tint:I

    .line 5161
    sput v0, Landroid/support/print/R$color;->notification_action_color_filter:I

    .line 5162
    sput v0, Landroid/support/print/R$color;->notification_icon_bg_color:I

    .line 5163
    sput v0, Landroid/support/print/R$color;->notification_material_background_media_default_color:I

    .line 5164
    sput v0, Landroid/support/print/R$color;->numberpicker_divider_color_dark:I

    .line 5165
    sput v0, Landroid/support/print/R$color;->numberpicker_divider_color_light:I

    .line 5169
    sput v0, Landroid/support/print/R$color;->numberpicker_minute_text_color_dark:I

    .line 5170
    sput v0, Landroid/support/print/R$color;->numberpicker_minute_text_color_light:I

    .line 5171
    sput v0, Landroid/support/print/R$color;->onePlusAccentColor_device_default_dark:I

    .line 5172
    sput v0, Landroid/support/print/R$color;->onePlusAccentColor_device_default_light:I

    .line 5176
    sput v0, Landroid/support/print/R$color;->onePlusPrimaryColor_device_default:I

    .line 5177
    sput v0, Landroid/support/print/R$color;->onePlusPrimaryColor_device_default_dark:I

    .line 5178
    sput v0, Landroid/support/print/R$color;->onePlusPrimaryColor_device_default_light:I

    .line 5182
    sput v0, Landroid/support/print/R$color;->oneplus_accent_color:I

    .line 5183
    sput v0, Landroid/support/print/R$color;->oneplus_accent_theme_2:I

    .line 5187
    sput v0, Landroid/support/print/R$color;->oneplus_alert_progress_dialog_background_color:I

    .line 5188
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_appbar_dark:I

    .line 5189
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_appbar_default:I

    .line 5190
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_appbar_light:I

    .line 5191
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_bottomsheets_dark:I

    .line 5192
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_bottomsheets_default:I

    .line 5193
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_bottomsheets_light:I

    .line 5194
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_cardview_dark:I

    .line 5195
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_cardview_default:I

    .line 5196
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_cardview_light:I

    .line 5197
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_dark:I

    .line 5198
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_default:I

    .line 5199
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_dialogs_dark:I

    .line 5200
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_dialogs_default:I

    .line 5201
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_dialogs_light:I

    .line 5202
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_light:I

    .line 5203
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_listbg_dark:I

    .line 5204
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_listbg_default:I

    .line 5205
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_listbg_light:I

    .line 5206
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_menu_dark:I

    .line 5207
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_menu_default:I

    .line 5208
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_menu_light:I

    .line 5209
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_statusbar_dark:I

    .line 5210
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_statusbar_default:I

    .line 5214
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_statusbar_light:I

    .line 5215
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_steppers_dark:I

    .line 5216
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_steppers_default:I

    .line 5217
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_bg_color_steppers_light:I

    .line 5218
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_divider_color_dark:I

    .line 5219
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_divider_color_default:I

    .line 5223
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_divider_color_light:I

    .line 5224
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_accent_active_dark:I

    .line 5225
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_accent_active_default:I

    .line 5226
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_accent_active_light:I

    .line 5227
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_accent_inactive_dark:I

    .line 5228
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_accent_inactive_default:I

    .line 5229
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_accent_inactive_light:I

    .line 5230
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_active_dark:I

    .line 5231
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_active_default:I

    .line 5235
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_active_light:I

    .line 5236
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_inactive_dark:I

    .line 5237
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_inactive_default:I

    .line 5238
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_icon_color_inactive_light:I

    .line 5239
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_disable_dark:I

    .line 5240
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_disable_default:I

    .line 5241
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_disable_light:I

    .line 5242
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_hint_dark:I

    .line 5243
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_hint_default:I

    .line 5244
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_hint_light:I

    .line 5245
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_label_dark:I

    .line 5246
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_label_default:I

    .line 5247
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_label_light:I

    .line 5248
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_primary_dark:I

    .line 5249
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_primary_default:I

    .line 5253
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_primary_light:I

    .line 5254
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_secondary_dark:I

    .line 5255
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_secondary_default:I

    .line 5256
    sput v0, Landroid/support/print/R$color;->oneplus_contorl_text_color_secondary_light:I

    .line 5257
    sput v0, Landroid/support/print/R$color;->oneplus_numberpicker_divider_color_dark:I

    .line 5258
    sput v0, Landroid/support/print/R$color;->oneplus_numberpicker_divider_color_default:I

    .line 5262
    sput v0, Landroid/support/print/R$color;->oneplus_numberpicker_divider_color_light:I

    .line 5263
    sput v0, Landroid/support/print/R$color;->oneplus_preference_subtitle_text_color_dark:I

    .line 5264
    sput v0, Landroid/support/print/R$color;->oneplus_preference_subtitle_text_color_light:I

    .line 5265
    sput v0, Landroid/support/print/R$color;->oneplus_preference_title_text_color_dark:I

    .line 5266
    sput v0, Landroid/support/print/R$color;->oneplus_preference_title_text_color_light:I

    .line 5267
    sput v0, Landroid/support/print/R$color;->oneplus_red_color:I

    .line 5268
    sput v0, Landroid/support/print/R$color;->oneplus_tab_indicator_color_colored:I

    .line 5269
    sput v0, Landroid/support/print/R$color;->oneplus_tab_indicator_color_dark:I

    .line 5273
    sput v0, Landroid/support/print/R$color;->oneplus_tab_indicator_color_light:I

    .line 5274
    sput v0, Landroid/support/print/R$color;->oneplus_white_color:I

    .line 5275
    sput v0, Landroid/support/print/R$color;->op_abc_tint_switch_thumb:I

    .line 5276
    sput v0, Landroid/support/print/R$color;->op_actionbar_subtitle_text_material:I

    .line 5277
    sput v0, Landroid/support/print/R$color;->op_actionbar_title_text_material:I

    .line 5278
    sput v0, Landroid/support/print/R$color;->op_btn_colored_raised_text_material_dark:I

    .line 5279
    sput v0, Landroid/support/print/R$color;->op_btn_colored_raised_text_material_light:I

    .line 5280
    sput v0, Landroid/support/print/R$color;->op_btn_colored_text_material_dark:I

    .line 5281
    sput v0, Landroid/support/print/R$color;->op_btn_colored_text_material_light:I

    .line 5282
    sput v0, Landroid/support/print/R$color;->op_btn_default_material_dark:I

    .line 5283
    sput v0, Landroid/support/print/R$color;->op_btn_default_material_light:I

    .line 5284
    sput v0, Landroid/support/print/R$color;->op_btn_ripple_material_dark:I

    .line 5285
    sput v0, Landroid/support/print/R$color;->op_btn_ripple_material_light:I

    .line 5289
    sput v0, Landroid/support/print/R$color;->op_button_material_disable_dark:I

    .line 5293
    sput v0, Landroid/support/print/R$color;->op_button_material_disable_light:I

    .line 5297
    sput v0, Landroid/support/print/R$color;->op_button_text:I

    .line 5298
    sput v0, Landroid/support/print/R$color;->op_cc_text_color_primary:I

    .line 5299
    sput v0, Landroid/support/print/R$color;->op_color_bottom_navigation_item_tint:I

    .line 5300
    sput v0, Landroid/support/print/R$color;->op_control_checkable_material:I

    .line 5301
    sput v0, Landroid/support/print/R$color;->op_date_time_picker_header_text_color_dark:I

    .line 5302
    sput v0, Landroid/support/print/R$color;->op_date_time_picker_header_text_color_light:I

    .line 5303
    sput v0, Landroid/support/print/R$color;->op_design_error:I

    .line 5307
    sput v0, Landroid/support/print/R$color;->op_design_snackbar_background_color:I

    .line 5308
    sput v0, Landroid/support/print/R$color;->op_design_tint_password_toggle:I

    .line 5309
    sput v0, Landroid/support/print/R$color;->op_drop_down_item_selected_color_dark:I

    .line 5310
    sput v0, Landroid/support/print/R$color;->op_drop_down_item_selected_color_light:I

    .line 5311
    sput v0, Landroid/support/print/R$color;->op_edittext_hint_color_material_dark:I

    .line 5312
    sput v0, Landroid/support/print/R$color;->op_edittext_hint_color_material_light:I

    .line 5313
    sput v0, Landroid/support/print/R$color;->op_error_color_material_dark:I

    .line 5314
    sput v0, Landroid/support/print/R$color;->op_error_color_material_default:I

    .line 5318
    sput v0, Landroid/support/print/R$color;->op_error_color_material_light:I

    .line 5319
    sput v0, Landroid/support/print/R$color;->op_hint_foreground_material_dark:I

    .line 5320
    sput v0, Landroid/support/print/R$color;->op_hint_foreground_material_light:I

    .line 5321
    sput v0, Landroid/support/print/R$color;->op_menu_background_dark:I

    .line 5322
    sput v0, Landroid/support/print/R$color;->op_menu_background_light:I

    .line 5323
    sput v0, Landroid/support/print/R$color;->op_menu_text_dark:I

    .line 5327
    sput v0, Landroid/support/print/R$color;->op_menu_text_light:I

    .line 5328
    sput v0, Landroid/support/print/R$color;->op_menu_text_material_dark:I

    .line 5329
    sput v0, Landroid/support/print/R$color;->op_menu_text_material_light:I

    .line 5330
    sput v0, Landroid/support/print/R$color;->op_primary_text_color_dark:I

    .line 5331
    sput v0, Landroid/support/print/R$color;->op_primary_text_color_dark_disabled:I

    .line 5335
    sput v0, Landroid/support/print/R$color;->op_primary_text_color_light:I

    .line 5336
    sput v0, Landroid/support/print/R$color;->op_primary_text_color_light_disabled:I

    .line 5337
    sput v0, Landroid/support/print/R$color;->op_primary_text_inverse_when_activated_material_dark:I

    .line 5338
    sput v0, Landroid/support/print/R$color;->op_primary_text_inverse_when_activated_material_light:I

    .line 5339
    sput v0, Landroid/support/print/R$color;->op_primary_text_material_dark:I

    .line 5340
    sput v0, Landroid/support/print/R$color;->op_primary_text_material_light:I

    .line 5341
    sput v0, Landroid/support/print/R$color;->op_primary_text_secondary_when_activated_material:I

    .line 5342
    sput v0, Landroid/support/print/R$color;->op_primary_text_secondary_when_activated_material_inverse:I

    .line 5343
    sput v0, Landroid/support/print/R$color;->op_ripple_material_dark:I

    .line 5344
    sput v0, Landroid/support/print/R$color;->op_ripple_material_light:I

    .line 5345
    sput v0, Landroid/support/print/R$color;->op_secondary_text_color_dark:I

    .line 5346
    sput v0, Landroid/support/print/R$color;->op_secondary_text_color_dark_disabled:I

    .line 5350
    sput v0, Landroid/support/print/R$color;->op_secondary_text_color_light:I

    .line 5351
    sput v0, Landroid/support/print/R$color;->op_secondary_text_color_light_disabled:I

    .line 5352
    sput v0, Landroid/support/print/R$color;->op_secondary_text_material_dark:I

    .line 5353
    sput v0, Landroid/support/print/R$color;->op_secondary_text_material_light:I

    .line 5354
    sput v0, Landroid/support/print/R$color;->op_switch_thumb_material_light_selector:I

    .line 5355
    sput v0, Landroid/support/print/R$color;->op_switch_thumb_material_selector:I

    .line 5356
    sput v0, Landroid/support/print/R$color;->op_switch_track_material:I

    .line 5357
    sput v0, Landroid/support/print/R$color;->op_switch_track_material_light:I

    .line 5358
    sput v0, Landroid/support/print/R$color;->op_tab_highlight_material:I

    .line 5359
    sput v0, Landroid/support/print/R$color;->op_tab_indicator_material:I

    .line 5360
    sput v0, Landroid/support/print/R$color;->op_tab_indicator_text_material:I

    .line 5361
    sput v0, Landroid/support/print/R$color;->op_tab_indicator_text_material_light:I

    .line 5362
    sput v0, Landroid/support/print/R$color;->op_tablayout_title_text_material:I

    .line 5363
    sput v0, Landroid/support/print/R$color;->op_tablayout_title_text_selected_material:I

    .line 5364
    sput v0, Landroid/support/print/R$color;->op_text_selection_highlight_dark:I

    .line 5365
    sput v0, Landroid/support/print/R$color;->op_text_selection_highlight_light:I

    .line 5366
    sput v0, Landroid/support/print/R$color;->op_time_picker_intput_label_text_color:I

    .line 5367
    sput v0, Landroid/support/print/R$color;->op_timepicker_text_inverse_when_activated_material_dark:I

    .line 5368
    sput v0, Landroid/support/print/R$color;->op_timepicker_text_inverse_when_activated_material_light:I

    .line 5372
    sput v0, Landroid/support/print/R$color;->op_toast_text_color_dark:I

    .line 5373
    sput v0, Landroid/support/print/R$color;->op_toast_text_color_light:I

    .line 5374
    sput v0, Landroid/support/print/R$color;->opcardview_light_background_light:I

    .line 5378
    sput v0, Landroid/support/print/R$color;->opcardview_light_background_mask_light:I

    .line 5379
    sput v0, Landroid/support/print/R$color;->orange_100:I

    .line 5380
    sput v0, Landroid/support/print/R$color;->orange_200:I

    .line 5381
    sput v0, Landroid/support/print/R$color;->orange_300:I

    .line 5382
    sput v0, Landroid/support/print/R$color;->orange_400:I

    .line 5383
    sput v0, Landroid/support/print/R$color;->orange_50:I

    .line 5384
    sput v0, Landroid/support/print/R$color;->orange_500:I

    .line 5385
    sput v0, Landroid/support/print/R$color;->orange_600:I

    .line 5386
    sput v0, Landroid/support/print/R$color;->orange_700:I

    .line 5387
    sput v0, Landroid/support/print/R$color;->orange_800:I

    .line 5388
    sput v0, Landroid/support/print/R$color;->orange_900:I

    .line 5389
    sput v0, Landroid/support/print/R$color;->orange_a100:I

    .line 5390
    sput v0, Landroid/support/print/R$color;->orange_a200:I

    .line 5391
    sput v0, Landroid/support/print/R$color;->orange_a400:I

    .line 5392
    sput v0, Landroid/support/print/R$color;->orange_a700:I

    .line 5393
    sput v0, Landroid/support/print/R$color;->pink_100:I

    .line 5394
    sput v0, Landroid/support/print/R$color;->pink_200:I

    .line 5395
    sput v0, Landroid/support/print/R$color;->pink_300:I

    .line 5396
    sput v0, Landroid/support/print/R$color;->pink_400:I

    .line 5397
    sput v0, Landroid/support/print/R$color;->pink_50:I

    .line 5398
    sput v0, Landroid/support/print/R$color;->pink_500:I

    .line 5399
    sput v0, Landroid/support/print/R$color;->pink_600:I

    .line 5400
    sput v0, Landroid/support/print/R$color;->pink_700:I

    .line 5401
    sput v0, Landroid/support/print/R$color;->pink_800:I

    .line 5402
    sput v0, Landroid/support/print/R$color;->pink_900:I

    .line 5403
    sput v0, Landroid/support/print/R$color;->pink_a100:I

    .line 5404
    sput v0, Landroid/support/print/R$color;->pink_a200:I

    .line 5405
    sput v0, Landroid/support/print/R$color;->pink_a400:I

    .line 5406
    sput v0, Landroid/support/print/R$color;->pink_a700:I

    .line 5410
    sput v0, Landroid/support/print/R$color;->preference_category_color_dark:I

    .line 5411
    sput v0, Landroid/support/print/R$color;->preference_category_color_light:I

    .line 5412
    sput v0, Landroid/support/print/R$color;->preference_divider_color_dark:I

    .line 5413
    sput v0, Landroid/support/print/R$color;->preference_divider_color_light:I

    .line 5414
    sput v0, Landroid/support/print/R$color;->primary_charm_purple_dark:I

    .line 5415
    sput v0, Landroid/support/print/R$color;->primary_charm_purple_light:I

    .line 5416
    sput v0, Landroid/support/print/R$color;->primary_dark_material_dark:I

    .line 5417
    sput v0, Landroid/support/print/R$color;->primary_dark_material_light:I

    .line 5418
    sput v0, Landroid/support/print/R$color;->primary_dark_material_light_light_status_bar:I

    .line 5422
    sput v0, Landroid/support/print/R$color;->primary_default_dark:I

    .line 5426
    sput v0, Landroid/support/print/R$color;->primary_default_light:I

    .line 5427
    sput v0, Landroid/support/print/R$color;->primary_fashion_pink_dark:I

    .line 5428
    sput v0, Landroid/support/print/R$color;->primary_fashion_pink_light:I

    .line 5429
    sput v0, Landroid/support/print/R$color;->primary_golden_dark:I

    .line 5430
    sput v0, Landroid/support/print/R$color;->primary_golden_light:I

    .line 5431
    sput v0, Landroid/support/print/R$color;->primary_grass_green_dark:I

    .line 5432
    sput v0, Landroid/support/print/R$color;->primary_grass_green_light:I

    .line 5433
    sput v0, Landroid/support/print/R$color;->primary_lemon_yellow_dark:I

    .line 5434
    sput v0, Landroid/support/print/R$color;->primary_lemon_yellow_light:I

    .line 5435
    sput v0, Landroid/support/print/R$color;->primary_material_dark:I

    .line 5436
    sput v0, Landroid/support/print/R$color;->primary_material_light:I

    .line 5437
    sput v0, Landroid/support/print/R$color;->primary_sky_blue_dark:I

    .line 5438
    sput v0, Landroid/support/print/R$color;->primary_sky_blue_light:I

    .line 5442
    sput v0, Landroid/support/print/R$color;->primary_text_default_material_dark:I

    .line 5446
    sput v0, Landroid/support/print/R$color;->primary_text_default_material_light:I

    .line 5447
    sput v0, Landroid/support/print/R$color;->primary_text_disable_only_material_dark:I

    .line 5448
    sput v0, Landroid/support/print/R$color;->primary_text_disable_only_material_light:I

    .line 5449
    sput v0, Landroid/support/print/R$color;->primary_text_disabled_material_dark:I

    .line 5450
    sput v0, Landroid/support/print/R$color;->primary_text_disabled_material_light:I

    .line 5451
    sput v0, Landroid/support/print/R$color;->primary_text_material_dark:I

    .line 5452
    sput v0, Landroid/support/print/R$color;->primary_text_material_light:I

    .line 5453
    sput v0, Landroid/support/print/R$color;->primary_vigour_red_dark:I

    .line 5454
    sput v0, Landroid/support/print/R$color;->primary_vigour_red_light:I

    .line 5458
    sput v0, Landroid/support/print/R$color;->progressbar_normal_color_dark:I

    .line 5459
    sput v0, Landroid/support/print/R$color;->progressbar_normal_color_light:I

    .line 5460
    sput v0, Landroid/support/print/R$color;->purple_100:I

    .line 5461
    sput v0, Landroid/support/print/R$color;->purple_200:I

    .line 5462
    sput v0, Landroid/support/print/R$color;->purple_300:I

    .line 5463
    sput v0, Landroid/support/print/R$color;->purple_400:I

    .line 5464
    sput v0, Landroid/support/print/R$color;->purple_50:I

    .line 5465
    sput v0, Landroid/support/print/R$color;->purple_500:I

    .line 5466
    sput v0, Landroid/support/print/R$color;->purple_600:I

    .line 5467
    sput v0, Landroid/support/print/R$color;->purple_700:I

    .line 5468
    sput v0, Landroid/support/print/R$color;->purple_800:I

    .line 5469
    sput v0, Landroid/support/print/R$color;->purple_900:I

    .line 5470
    sput v0, Landroid/support/print/R$color;->purple_a100:I

    .line 5471
    sput v0, Landroid/support/print/R$color;->purple_a200:I

    .line 5472
    sput v0, Landroid/support/print/R$color;->purple_a400:I

    .line 5473
    sput v0, Landroid/support/print/R$color;->purple_a700:I

    .line 5474
    sput v0, Landroid/support/print/R$color;->radio_button_disable_color_dark:I

    .line 5475
    sput v0, Landroid/support/print/R$color;->radio_button_disable_color_light:I

    .line 5479
    sput v0, Landroid/support/print/R$color;->radio_button_normal_color_dark:I

    .line 5480
    sput v0, Landroid/support/print/R$color;->radio_button_normal_color_light:I

    .line 5484
    sput v0, Landroid/support/print/R$color;->red_01:I

    .line 5488
    sput v0, Landroid/support/print/R$color;->red_01_dark:I

    .line 5492
    sput v0, Landroid/support/print/R$color;->red_01_light:I

    .line 5493
    sput v0, Landroid/support/print/R$color;->red_02:I

    .line 5494
    sput v0, Landroid/support/print/R$color;->red_02_dark:I

    .line 5495
    sput v0, Landroid/support/print/R$color;->red_02_light:I

    .line 5496
    sput v0, Landroid/support/print/R$color;->red_03:I

    .line 5497
    sput v0, Landroid/support/print/R$color;->red_03_dark:I

    .line 5498
    sput v0, Landroid/support/print/R$color;->red_03_light:I

    .line 5499
    sput v0, Landroid/support/print/R$color;->red_04:I

    .line 5500
    sput v0, Landroid/support/print/R$color;->red_04_dark:I

    .line 5501
    sput v0, Landroid/support/print/R$color;->red_04_light:I

    .line 5502
    sput v0, Landroid/support/print/R$color;->red_05:I

    .line 5503
    sput v0, Landroid/support/print/R$color;->red_05_dark:I

    .line 5504
    sput v0, Landroid/support/print/R$color;->red_05_light:I

    .line 5505
    sput v0, Landroid/support/print/R$color;->red_100:I

    .line 5506
    sput v0, Landroid/support/print/R$color;->red_200:I

    .line 5507
    sput v0, Landroid/support/print/R$color;->red_300:I

    .line 5508
    sput v0, Landroid/support/print/R$color;->red_400:I

    .line 5509
    sput v0, Landroid/support/print/R$color;->red_50:I

    .line 5510
    sput v0, Landroid/support/print/R$color;->red_500:I

    .line 5511
    sput v0, Landroid/support/print/R$color;->red_600:I

    .line 5512
    sput v0, Landroid/support/print/R$color;->red_700:I

    .line 5513
    sput v0, Landroid/support/print/R$color;->red_800:I

    .line 5514
    sput v0, Landroid/support/print/R$color;->red_900:I

    .line 5515
    sput v0, Landroid/support/print/R$color;->red_a100:I

    .line 5516
    sput v0, Landroid/support/print/R$color;->red_a200:I

    .line 5517
    sput v0, Landroid/support/print/R$color;->red_a400:I

    .line 5518
    sput v0, Landroid/support/print/R$color;->red_a700:I

    .line 5519
    sput v0, Landroid/support/print/R$color;->ripple_material_dark:I

    .line 5520
    sput v0, Landroid/support/print/R$color;->ripple_material_light:I

    .line 5521
    sput v0, Landroid/support/print/R$color;->search_url_text_material_dark:I

    .line 5522
    sput v0, Landroid/support/print/R$color;->search_url_text_material_light:I

    .line 5523
    sput v0, Landroid/support/print/R$color;->search_url_text_normal:I

    .line 5524
    sput v0, Landroid/support/print/R$color;->search_url_text_pressed:I

    .line 5525
    sput v0, Landroid/support/print/R$color;->search_url_text_selected:I

    .line 5529
    sput v0, Landroid/support/print/R$color;->secondary_text_default_material_dark:I

    .line 5533
    sput v0, Landroid/support/print/R$color;->secondary_text_default_material_light:I

    .line 5534
    sput v0, Landroid/support/print/R$color;->secondary_text_disabled_material_dark:I

    .line 5535
    sput v0, Landroid/support/print/R$color;->secondary_text_disabled_material_light:I

    .line 5536
    sput v0, Landroid/support/print/R$color;->secondary_text_material_dark:I

    .line 5537
    sput v0, Landroid/support/print/R$color;->secondary_text_material_light:I

    .line 5541
    sput v0, Landroid/support/print/R$color;->seekbar_normal_color_dark:I

    .line 5542
    sput v0, Landroid/support/print/R$color;->seekbar_normal_color_light:I

    .line 5543
    sput v0, Landroid/support/print/R$color;->selection_control_disable:I

    .line 5544
    sput v0, Landroid/support/print/R$color;->selection_control_disable_dark:I

    .line 5545
    sput v0, Landroid/support/print/R$color;->selection_control_disable_light:I

    .line 5546
    sput v0, Landroid/support/print/R$color;->selection_control_off:I

    .line 5547
    sput v0, Landroid/support/print/R$color;->selection_control_off_dark:I

    .line 5548
    sput v0, Landroid/support/print/R$color;->selection_control_off_light:I

    .line 5552
    sput v0, Landroid/support/print/R$color;->selection_control_on:I

    .line 5556
    sput v0, Landroid/support/print/R$color;->selection_control_on_dark:I

    .line 5560
    sput v0, Landroid/support/print/R$color;->selection_control_on_light:I

    .line 5561
    sput v0, Landroid/support/print/R$color;->switch_thumb_disable_color_dark:I

    .line 5562
    sput v0, Landroid/support/print/R$color;->switch_thumb_disable_color_light:I

    .line 5563
    sput v0, Landroid/support/print/R$color;->switch_thumb_disabled_material_dark:I

    .line 5564
    sput v0, Landroid/support/print/R$color;->switch_thumb_disabled_material_light:I

    .line 5565
    sput v0, Landroid/support/print/R$color;->switch_thumb_material_dark:I

    .line 5566
    sput v0, Landroid/support/print/R$color;->switch_thumb_material_light:I

    .line 5567
    sput v0, Landroid/support/print/R$color;->switch_thumb_normal_color_dark:I

    .line 5568
    sput v0, Landroid/support/print/R$color;->switch_thumb_normal_color_light:I

    .line 5569
    sput v0, Landroid/support/print/R$color;->switch_thumb_normal_material_dark:I

    .line 5570
    sput v0, Landroid/support/print/R$color;->switch_thumb_normal_material_light:I

    .line 5571
    sput v0, Landroid/support/print/R$color;->switch_track_activated_color_dark:I

    .line 5572
    sput v0, Landroid/support/print/R$color;->switch_track_activated_color_light:I

    .line 5573
    sput v0, Landroid/support/print/R$color;->switch_track_disable_color_dark:I

    .line 5574
    sput v0, Landroid/support/print/R$color;->switch_track_disable_color_light:I

    .line 5575
    sput v0, Landroid/support/print/R$color;->switch_track_material:I

    .line 5576
    sput v0, Landroid/support/print/R$color;->switch_track_normal_color_dark:I

    .line 5577
    sput v0, Landroid/support/print/R$color;->switch_track_normal_color_light:I

    .line 5578
    sput v0, Landroid/support/print/R$color;->switch_white_disabled_checked_material:I

    .line 5579
    sput v0, Landroid/support/print/R$color;->switch_white_disabled_material:I

    .line 5580
    sput v0, Landroid/support/print/R$color;->switch_white_disabled_material_light:I

    .line 5581
    sput v0, Landroid/support/print/R$color;->system_blue_purple_100:I

    .line 5582
    sput v0, Landroid/support/print/R$color;->system_blue_purple_200:I

    .line 5583
    sput v0, Landroid/support/print/R$color;->system_blue_purple_300:I

    .line 5584
    sput v0, Landroid/support/print/R$color;->system_blue_purple_400:I

    .line 5585
    sput v0, Landroid/support/print/R$color;->system_blue_purple_50:I

    .line 5586
    sput v0, Landroid/support/print/R$color;->system_blue_purple_500:I

    .line 5587
    sput v0, Landroid/support/print/R$color;->system_blue_purple_600:I

    .line 5588
    sput v0, Landroid/support/print/R$color;->system_blue_purple_700:I

    .line 5589
    sput v0, Landroid/support/print/R$color;->system_blue_purple_800:I

    .line 5590
    sput v0, Landroid/support/print/R$color;->system_blue_purple_900:I

    .line 5591
    sput v0, Landroid/support/print/R$color;->system_blue_purple_a100:I

    .line 5592
    sput v0, Landroid/support/print/R$color;->system_blue_purple_a200:I

    .line 5593
    sput v0, Landroid/support/print/R$color;->system_blue_purple_a400:I

    .line 5594
    sput v0, Landroid/support/print/R$color;->system_blue_purple_a700:I

    .line 5595
    sput v0, Landroid/support/print/R$color;->system_default_blue_100:I

    .line 5596
    sput v0, Landroid/support/print/R$color;->system_default_blue_200:I

    .line 5597
    sput v0, Landroid/support/print/R$color;->system_default_blue_300:I

    .line 5598
    sput v0, Landroid/support/print/R$color;->system_default_blue_400:I

    .line 5599
    sput v0, Landroid/support/print/R$color;->system_default_blue_50:I

    .line 5600
    sput v0, Landroid/support/print/R$color;->system_default_blue_500:I

    .line 5601
    sput v0, Landroid/support/print/R$color;->system_default_blue_600:I

    .line 5602
    sput v0, Landroid/support/print/R$color;->system_default_blue_700:I

    .line 5603
    sput v0, Landroid/support/print/R$color;->system_default_blue_800:I

    .line 5604
    sput v0, Landroid/support/print/R$color;->system_default_blue_900:I

    .line 5605
    sput v0, Landroid/support/print/R$color;->system_default_blue_a100:I

    .line 5606
    sput v0, Landroid/support/print/R$color;->system_default_blue_a200:I

    .line 5607
    sput v0, Landroid/support/print/R$color;->system_default_blue_a400:I

    .line 5608
    sput v0, Landroid/support/print/R$color;->system_default_blue_a700:I

    .line 5612
    sput v0, Landroid/support/print/R$color;->system_focus:I

    .line 5616
    sput v0, Landroid/support/print/R$color;->system_focus_dark:I

    .line 5620
    sput v0, Landroid/support/print/R$color;->system_focus_light:I

    .line 5621
    sput v0, Landroid/support/print/R$color;->system_warning:I

    .line 5622
    sput v0, Landroid/support/print/R$color;->system_warning_dark:I

    .line 5623
    sput v0, Landroid/support/print/R$color;->system_warning_light:I

    .line 5624
    sput v0, Landroid/support/print/R$color;->teal_100:I

    .line 5625
    sput v0, Landroid/support/print/R$color;->teal_200:I

    .line 5626
    sput v0, Landroid/support/print/R$color;->teal_300:I

    .line 5627
    sput v0, Landroid/support/print/R$color;->teal_400:I

    .line 5628
    sput v0, Landroid/support/print/R$color;->teal_50:I

    .line 5629
    sput v0, Landroid/support/print/R$color;->teal_500:I

    .line 5630
    sput v0, Landroid/support/print/R$color;->teal_600:I

    .line 5631
    sput v0, Landroid/support/print/R$color;->teal_700:I

    .line 5632
    sput v0, Landroid/support/print/R$color;->teal_800:I

    .line 5633
    sput v0, Landroid/support/print/R$color;->teal_900:I

    .line 5634
    sput v0, Landroid/support/print/R$color;->teal_a100:I

    .line 5635
    sput v0, Landroid/support/print/R$color;->teal_a200:I

    .line 5636
    sput v0, Landroid/support/print/R$color;->teal_a400:I

    .line 5637
    sput v0, Landroid/support/print/R$color;->teal_a700:I

    .line 5638
    sput v0, Landroid/support/print/R$color;->timepicker_ampmlabel_text_color_dark:I

    .line 5639
    sput v0, Landroid/support/print/R$color;->timepicker_ampmlabel_text_color_light:I

    .line 5640
    sput v0, Landroid/support/print/R$color;->timepicker_default_ampm_selected_background_color_material:I

    .line 5641
    sput v0, Landroid/support/print/R$color;->timepicker_default_ampm_unselected_background_color_material:I

    .line 5642
    sput v0, Landroid/support/print/R$color;->timepicker_default_background_material:I

    .line 5643
    sput v0, Landroid/support/print/R$color;->timepicker_default_numbers_background_color_material:I

    .line 5644
    sput v0, Landroid/support/print/R$color;->timepicker_default_selector_color_material:I

    .line 5645
    sput v0, Landroid/support/print/R$color;->timepicker_default_text_color_material:I

    .line 5649
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_background_color_dark:I

    .line 5650
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_background_color_light:I

    .line 5651
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_inner_text_color:I

    .line 5652
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_inner_text_color_dark:I

    .line 5653
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_selector_color_dark:I

    .line 5654
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_selector_color_light:I

    .line 5655
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_text_color_dark:I

    .line 5656
    sput v0, Landroid/support/print/R$color;->timepicker_numbers_text_color_light:I

    .line 5657
    sput v0, Landroid/support/print/R$color;->tooltip_background_dark:I

    .line 5658
    sput v0, Landroid/support/print/R$color;->tooltip_background_light:I

    .line 5659
    sput v0, Landroid/support/print/R$color;->transparent:I

    .line 5660
    sput v0, Landroid/support/print/R$color;->white:I

    .line 5661
    sput v0, Landroid/support/print/R$color;->white_disabled_material:I

    .line 5662
    sput v0, Landroid/support/print/R$color;->yellow_100:I

    .line 5663
    sput v0, Landroid/support/print/R$color;->yellow_200:I

    .line 5664
    sput v0, Landroid/support/print/R$color;->yellow_300:I

    .line 5665
    sput v0, Landroid/support/print/R$color;->yellow_400:I

    .line 5666
    sput v0, Landroid/support/print/R$color;->yellow_50:I

    .line 5667
    sput v0, Landroid/support/print/R$color;->yellow_500:I

    .line 5668
    sput v0, Landroid/support/print/R$color;->yellow_600:I

    .line 5669
    sput v0, Landroid/support/print/R$color;->yellow_700:I

    .line 5670
    sput v0, Landroid/support/print/R$color;->yellow_800:I

    .line 5671
    sput v0, Landroid/support/print/R$color;->yellow_900:I

    .line 5672
    sput v0, Landroid/support/print/R$color;->yellow_a100:I

    .line 5673
    sput v0, Landroid/support/print/R$color;->yellow_a200:I

    .line 5674
    sput v0, Landroid/support/print/R$color;->yellow_a400:I

    .line 5675
    sput v0, Landroid/support/print/R$color;->yellow_a700:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 4542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
