.class public final Landroid/support/print/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/print/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static abc_config_activityDefaultDur:I

.field public static abc_config_activityShortDur:I

.field public static app_bar_elevation_anim_duration:I

.field public static bottom_sheet_slide_duration:I

.field public static button_pressed_animation_delay:I

.field public static button_pressed_animation_duration:I

.field public static cancel_button_image_alpha:I

.field public static config_activityDefaultDur:I

.field public static config_activityShortDur:I

.field public static config_toastDefaultGravity:I

.field public static config_tooltipAnimTime:I

.field public static date_picker_header_max_lines_material:I

.field public static date_picker_mode:I

.field public static date_picker_mode_material:I

.field public static oneplus_contorl_time_part1:I

.field public static oneplus_contorl_time_part2:I

.field public static oneplus_contorl_time_part3:I

.field public static oneplus_contorl_time_part4:I

.field public static oneplus_contorl_time_part5:I

.field public static oneplus_contorl_time_part6:I

.field public static oneplus_contorl_time_part7:I

.field public static oneplus_selector_button_frame_druation:I

.field public static op_config_tooltipAnimTime:I

.field public static op_date_picker_mode:I

.field public static op_design_snackbar_text_max_lines:I

.field public static op_hide_password_duration:I

.field public static op_pageIndicator_alpha_material:I

.field public static op_pageIndicator_alpha_material_dark:I

.field public static op_pageIndicator_alpha_material_light:I

.field public static op_show_password_duration:I

.field public static preference_fragment_scrollbarStyle:I

.field public static preference_screen_header_scrollbarStyle:I

.field public static preferences_left_pane_weight:I

.field public static preferences_right_pane_weight:I

.field public static status_bar_notification_info_maxnum:I

.field public static time_picker_mode:I

.field public static time_picker_mode_material:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7349
    const/4 v0, 0x0

    sput v0, Landroid/support/print/R$integer;->abc_config_activityDefaultDur:I

    .line 7350
    sput v0, Landroid/support/print/R$integer;->abc_config_activityShortDur:I

    .line 7351
    sput v0, Landroid/support/print/R$integer;->app_bar_elevation_anim_duration:I

    .line 7352
    sput v0, Landroid/support/print/R$integer;->bottom_sheet_slide_duration:I

    .line 7353
    sput v0, Landroid/support/print/R$integer;->button_pressed_animation_delay:I

    .line 7354
    sput v0, Landroid/support/print/R$integer;->button_pressed_animation_duration:I

    .line 7355
    sput v0, Landroid/support/print/R$integer;->cancel_button_image_alpha:I

    .line 7356
    sput v0, Landroid/support/print/R$integer;->config_activityDefaultDur:I

    .line 7357
    sput v0, Landroid/support/print/R$integer;->config_activityShortDur:I

    .line 7358
    sput v0, Landroid/support/print/R$integer;->config_toastDefaultGravity:I

    .line 7359
    sput v0, Landroid/support/print/R$integer;->config_tooltipAnimTime:I

    .line 7360
    sput v0, Landroid/support/print/R$integer;->date_picker_header_max_lines_material:I

    .line 7361
    sput v0, Landroid/support/print/R$integer;->date_picker_mode:I

    .line 7365
    sput v0, Landroid/support/print/R$integer;->date_picker_mode_material:I

    .line 7369
    sput v0, Landroid/support/print/R$integer;->oneplus_contorl_time_part1:I

    .line 7370
    sput v0, Landroid/support/print/R$integer;->oneplus_contorl_time_part2:I

    .line 7371
    sput v0, Landroid/support/print/R$integer;->oneplus_contorl_time_part3:I

    .line 7372
    sput v0, Landroid/support/print/R$integer;->oneplus_contorl_time_part4:I

    .line 7373
    sput v0, Landroid/support/print/R$integer;->oneplus_contorl_time_part5:I

    .line 7374
    sput v0, Landroid/support/print/R$integer;->oneplus_contorl_time_part6:I

    .line 7375
    sput v0, Landroid/support/print/R$integer;->oneplus_contorl_time_part7:I

    .line 7376
    sput v0, Landroid/support/print/R$integer;->oneplus_selector_button_frame_druation:I

    .line 7380
    sput v0, Landroid/support/print/R$integer;->op_config_tooltipAnimTime:I

    .line 7381
    sput v0, Landroid/support/print/R$integer;->op_date_picker_mode:I

    .line 7385
    sput v0, Landroid/support/print/R$integer;->op_design_snackbar_text_max_lines:I

    .line 7386
    sput v0, Landroid/support/print/R$integer;->op_hide_password_duration:I

    .line 7387
    sput v0, Landroid/support/print/R$integer;->op_pageIndicator_alpha_material:I

    .line 7388
    sput v0, Landroid/support/print/R$integer;->op_pageIndicator_alpha_material_dark:I

    .line 7392
    sput v0, Landroid/support/print/R$integer;->op_pageIndicator_alpha_material_light:I

    .line 7393
    sput v0, Landroid/support/print/R$integer;->op_show_password_duration:I

    .line 7394
    sput v0, Landroid/support/print/R$integer;->preference_fragment_scrollbarStyle:I

    .line 7395
    sput v0, Landroid/support/print/R$integer;->preference_screen_header_scrollbarStyle:I

    .line 7399
    sput v0, Landroid/support/print/R$integer;->preferences_left_pane_weight:I

    .line 7400
    sput v0, Landroid/support/print/R$integer;->preferences_right_pane_weight:I

    .line 7401
    sput v0, Landroid/support/print/R$integer;->status_bar_notification_info_maxnum:I

    .line 7402
    sput v0, Landroid/support/print/R$integer;->time_picker_mode:I

    .line 7406
    sput v0, Landroid/support/print/R$integer;->time_picker_mode_material:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
