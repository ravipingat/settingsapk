.class public final Landroid/support/interpolator/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/interpolator/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static ab_double_subhead:I

.field public static ab_double_title:I

.field public static ab_single_title:I

.field public static abc_action_bar_content_inset_material:I

.field public static abc_action_bar_content_inset_with_nav:I

.field public static abc_action_bar_default_height_material:I

.field public static abc_action_bar_default_padding_end_material:I

.field public static abc_action_bar_default_padding_start_material:I

.field public static abc_action_bar_elevation_material:I

.field public static abc_action_bar_icon_vertical_padding_material:I

.field public static abc_action_bar_overflow_padding_end_material:I

.field public static abc_action_bar_overflow_padding_start_material:I

.field public static abc_action_bar_stacked_max_height:I

.field public static abc_action_bar_stacked_tab_max_width:I

.field public static abc_action_bar_subtitle_bottom_margin_material:I

.field public static abc_action_bar_subtitle_top_margin_material:I

.field public static abc_action_button_min_height_material:I

.field public static abc_action_button_min_width_material:I

.field public static abc_action_button_min_width_overflow_material:I

.field public static abc_action_menu_icon_size:I

.field public static abc_alert_dialog_button_bar_height:I

.field public static abc_alert_dialog_button_dimen:I

.field public static abc_button_inset_horizontal_material:I

.field public static abc_button_inset_vertical_material:I

.field public static abc_button_padding_horizontal_material:I

.field public static abc_button_padding_vertical_material:I

.field public static abc_cascading_menus_min_smallest_width:I

.field public static abc_config_prefDialogWidth:I

.field public static abc_control_corner_material:I

.field public static abc_control_inset_material:I

.field public static abc_control_padding_material:I

.field public static abc_dialog_corner_radius_material:I

.field public static abc_dialog_fixed_height_major:I

.field public static abc_dialog_fixed_height_minor:I

.field public static abc_dialog_fixed_width_major:I

.field public static abc_dialog_fixed_width_minor:I

.field public static abc_dialog_list_padding_bottom_no_buttons:I

.field public static abc_dialog_list_padding_top_no_title:I

.field public static abc_dialog_min_width_major:I

.field public static abc_dialog_min_width_minor:I

.field public static abc_dialog_padding_material:I

.field public static abc_dialog_padding_top_material:I

.field public static abc_dialog_title_divider_material:I

.field public static abc_disabled_alpha_material_dark:I

.field public static abc_disabled_alpha_material_light:I

.field public static abc_dropdownitem_icon_width:I

.field public static abc_dropdownitem_text_padding_left:I

.field public static abc_dropdownitem_text_padding_right:I

.field public static abc_edit_text_inset_bottom_material:I

.field public static abc_edit_text_inset_horizontal_material:I

.field public static abc_edit_text_inset_top_material:I

.field public static abc_floating_window_z:I

.field public static abc_list_item_padding_horizontal_material:I

.field public static abc_panel_menu_list_width:I

.field public static abc_progress_bar_height_material:I

.field public static abc_search_view_preferred_height:I

.field public static abc_search_view_preferred_width:I

.field public static abc_seekbar_track_background_height_material:I

.field public static abc_seekbar_track_progress_height_material:I

.field public static abc_select_dialog_padding_start_material:I

.field public static abc_switch_padding:I

.field public static abc_text_size_body_1_material:I

.field public static abc_text_size_body_2_material:I

.field public static abc_text_size_button_material:I

.field public static abc_text_size_caption_material:I

.field public static abc_text_size_display_1_material:I

.field public static abc_text_size_display_2_material:I

.field public static abc_text_size_display_3_material:I

.field public static abc_text_size_display_4_material:I

.field public static abc_text_size_headline_material:I

.field public static abc_text_size_large_material:I

.field public static abc_text_size_medium_material:I

.field public static abc_text_size_menu_header_material:I

.field public static abc_text_size_menu_material:I

.field public static abc_text_size_small_material:I

.field public static abc_text_size_subhead_material:I

.field public static abc_text_size_subtitle_material_toolbar:I

.field public static abc_text_size_title_material:I

.field public static abc_text_size_title_material_toolbar:I

.field public static action_bar_content_inset_material:I

.field public static action_bar_default_height_material:I

.field public static action_bar_default_padding_end_material:I

.field public static action_bar_default_padding_start_material:I

.field public static action_bar_elevation_material:I

.field public static action_bar_icon_vertical_padding_material:I

.field public static action_bar_overflow_padding_end_material:I

.field public static action_bar_overflow_padding_start_material:I

.field public static action_bar_subtitle_bottom_margin_material:I

.field public static action_bar_subtitle_top_margin_material:I

.field public static action_button_min_height_material:I

.field public static action_button_min_width_material:I

.field public static action_button_min_width_overflow_material:I

.field public static actionbar_elevation:I

.field public static actionbar_height:I

.field public static actionbar_searchview_icon_alpha:I

.field public static actionbar_searchview_icon_alpha_disable:I

.field public static alert_dialog_round_padding:I

.field public static alert_dialog_title_style_font_size:I

.field public static appbar_layout_default_height_material:I

.field public static bottom_navigation_badge_mini_width:I

.field public static bottom_navigation_badge_size_indeterminate:I

.field public static bottom_navigation_badge_text_horizontal_padding:I

.field public static bottom_navigation_badge_text_size:I

.field public static btn_highlight_alpha_material:I

.field public static btn_highlight_alpha_material_dark:I

.field public static btn_highlight_alpha_material_light:I

.field public static button:I

.field public static button_elevation_material:I

.field public static button_inset_horizontal_material:I

.field public static button_inset_vertical_material:I

.field public static button_padding_horizontal_material:I

.field public static button_padding_vertical_material:I

.field public static button_pressed_z_material:I

.field public static cardview_compat_inset_shadow:I

.field public static cardview_default_elevation:I

.field public static cardview_default_radius:I

.field public static compat_button_inset_horizontal_material:I

.field public static compat_button_inset_vertical_material:I

.field public static compat_button_padding_horizontal_material:I

.field public static compat_button_padding_vertical_material:I

.field public static compat_control_corner_material:I

.field public static compat_notification_large_icon_max_height:I

.field public static compat_notification_large_icon_max_width:I

.field public static compat_op_notification_large_icon_max_height:I

.field public static compat_op_notification_large_icon_max_width:I

.field public static control_corner_material:I

.field public static control_inset_material:I

.field public static control_padding_material:I

.field public static date_picker_date_label_size:I

.field public static date_picker_day_height:I

.field public static date_picker_day_of_week_height:I

.field public static date_picker_day_of_week_text_size:I

.field public static date_picker_day_selector_radius:I

.field public static date_picker_day_text_size:I

.field public static date_picker_day_width:I

.field public static date_picker_month_height:I

.field public static date_picker_month_text_size:I

.field public static date_picker_year_label_size:I

.field public static datepicker_component_width:I

.field public static datepicker_day_number_select_circle_radius:I

.field public static datepicker_day_number_size:I

.field public static datepicker_dialog_width:I

.field public static datepicker_header_height:I

.field public static datepicker_header_text_size:I

.field public static datepicker_list_year_activated_label_size:I

.field public static datepicker_list_year_label_size:I

.field public static datepicker_month_day_label_text_size:I

.field public static datepicker_month_label_size:I

.field public static datepicker_month_list_item_header_height:I

.field public static datepicker_selected_date_day_size:I

.field public static datepicker_selected_date_month_size:I

.field public static datepicker_selected_date_year_size:I

.field public static datepicker_view_animator_height:I

.field public static datepicker_year_label_height:I

.field public static datepicker_year_label_text_size:I

.field public static datepicker_year_picker_padding_top:I

.field public static day_picker_button_margin_top:I

.field public static day_picker_padding_horizontal:I

.field public static day_picker_padding_top:I

.field public static design_appbar_elevation:I

.field public static design_bottom_navigation_active_item_max_width:I

.field public static design_bottom_navigation_active_item_min_width:I

.field public static design_bottom_navigation_active_text_size:I

.field public static design_bottom_navigation_elevation:I

.field public static design_bottom_navigation_height:I

.field public static design_bottom_navigation_icon_size:I

.field public static design_bottom_navigation_item_max_width:I

.field public static design_bottom_navigation_item_min_width:I

.field public static design_bottom_navigation_margin:I

.field public static design_bottom_navigation_shadow_height:I

.field public static design_bottom_navigation_text_size:I

.field public static design_fab_border_width:I

.field public static design_fab_content_size:I

.field public static design_fab_elevation:I

.field public static design_fab_size_mini:I

.field public static design_fab_size_normal:I

.field public static design_fab_translation_z_pressed:I

.field public static dialog_fixed_height_major:I

.field public static dialog_fixed_height_minor:I

.field public static dialog_fixed_width_major:I

.field public static dialog_fixed_width_minor:I

.field public static dialog_list_padding_vertical_material:I

.field public static dialog_min_width_major:I

.field public static dialog_min_width_minor:I

.field public static dialog_padding_material:I

.field public static dialog_padding_top_material:I

.field public static dialog_text:I

.field public static dialog_title:I

.field public static disabledAlpha_dark:I

.field public static disabledAlpha_light:I

.field public static disabled_alpha_material_dark:I

.field public static disabled_alpha_material_light:I

.field public static dropdownitem_icon_width:I

.field public static dropdownitem_text_padding_left:I

.field public static dropdownitem_text_padding_right:I

.field public static edit_text_inset_bottom_material:I

.field public static edit_text_inset_horizontal_material:I

.field public static edit_text_inset_top_material:I

.field public static floating_window_margin_bottom:I

.field public static floating_window_margin_left:I

.field public static floating_window_margin_right:I

.field public static floating_window_margin_top:I

.field public static floating_window_z:I

.field public static highlight_alpha_material_colored:I

.field public static highlight_alpha_material_dark:I

.field public static highlight_alpha_material_light:I

.field public static hint_alpha_material_dark:I

.field public static hint_alpha_material_light:I

.field public static hint_pressed_alpha_material_dark:I

.field public static hint_pressed_alpha_material_light:I

.field public static item_touch_helper_max_drag_scroll_per_frame:I

.field public static list_date:I

.field public static list_item_padding_horizontal_material:I

.field public static list_module_title:I

.field public static list_text:I

.field public static list_title:I

.field public static listitem_actionbutton_size:I

.field public static listitem_height:I

.field public static listitem_height_tall:I

.field public static listitem_icon_size:I

.field public static listitem_primatytext_size:I

.field public static listitem_secondarytext_size:I

.field public static listitem_stamp_size:I

.field public static listview_divider_height:I

.field public static margin_m1:I

.field public static notification_action_icon_size:I

.field public static notification_action_text_size:I

.field public static notification_big_circle_margin:I

.field public static notification_content_margin_start:I

.field public static notification_large_icon_height:I

.field public static notification_large_icon_width:I

.field public static notification_main_column_padding_top:I

.field public static notification_media_narrow_margin:I

.field public static notification_right_icon_size:I

.field public static notification_right_side_padding_top:I

.field public static notification_small_icon_background_padding:I

.field public static notification_small_icon_size_as_large:I

.field public static notification_subtext_size:I

.field public static notification_top_pad:I

.field public static notification_top_pad_large_text:I

.field public static oneplus_appbar_with_tab_size1:I

.field public static oneplus_appbar_with_tab_size2:I

.field public static oneplus_button_default_height:I

.field public static oneplus_button_mini_height:I

.field public static oneplus_button_mini_width:I

.field public static oneplus_contorl_alpha1:I

.field public static oneplus_contorl_alpha10:I

.field public static oneplus_contorl_alpha11:I

.field public static oneplus_contorl_alpha2:I

.field public static oneplus_contorl_alpha3:I

.field public static oneplus_contorl_alpha4:I

.field public static oneplus_contorl_alpha5:I

.field public static oneplus_contorl_alpha6:I

.field public static oneplus_contorl_alpha7:I

.field public static oneplus_contorl_alpha8:I

.field public static oneplus_contorl_alpha9:I

.field public static oneplus_contorl_appbar_size_extended1:I

.field public static oneplus_contorl_appbar_size_extended2:I

.field public static oneplus_contorl_appbar_size_landscape:I

.field public static oneplus_contorl_appbar_size_standard:I

.field public static oneplus_contorl_avatar_mini:I

.field public static oneplus_contorl_avatar_standard:I

.field public static oneplus_contorl_button_item_height_accent:I

.field public static oneplus_contorl_button_item_height_standard:I

.field public static oneplus_contorl_dialog_item_width_1:I

.field public static oneplus_contorl_dialog_item_width_2:I

.field public static oneplus_contorl_dialog_item_width_3:I

.field public static oneplus_contorl_dialog_item_width_4:I

.field public static oneplus_contorl_dialog_item_width_5:I

.field public static oneplus_contorl_divider_height_bold:I

.field public static oneplus_contorl_divider_height_standard:I

.field public static oneplus_contorl_fab_size_mini:I

.field public static oneplus_contorl_fab_size_scrolling:I

.field public static oneplus_contorl_fab_size_standard:I

.field public static oneplus_contorl_icon_accent_active_alpha:I

.field public static oneplus_contorl_icon_accent_inactive_alpha:I

.field public static oneplus_contorl_icon_active_alpha:I

.field public static oneplus_contorl_icon_inactive_alpha:I

.field public static oneplus_contorl_icon_size_button:I

.field public static oneplus_contorl_icon_size_display1:I

.field public static oneplus_contorl_icon_size_display2:I

.field public static oneplus_contorl_icon_size_display3:I

.field public static oneplus_contorl_icon_size_display4:I

.field public static oneplus_contorl_icon_size_display5:I

.field public static oneplus_contorl_icon_size_indicator:I

.field public static oneplus_contorl_icon_size_list:I

.field public static oneplus_contorl_layout_margin_bottom1:I

.field public static oneplus_contorl_layout_margin_bottom2:I

.field public static oneplus_contorl_layout_margin_bottom3:I

.field public static oneplus_contorl_layout_margin_bottom4:I

.field public static oneplus_contorl_layout_margin_bottom5:I

.field public static oneplus_contorl_layout_margin_left1:I

.field public static oneplus_contorl_layout_margin_left2:I

.field public static oneplus_contorl_layout_margin_left3:I

.field public static oneplus_contorl_layout_margin_left4:I

.field public static oneplus_contorl_layout_margin_left5:I

.field public static oneplus_contorl_layout_margin_left6:I

.field public static oneplus_contorl_layout_margin_left7:I

.field public static oneplus_contorl_layout_margin_left8:I

.field public static oneplus_contorl_layout_margin_right1:I

.field public static oneplus_contorl_layout_margin_right2:I

.field public static oneplus_contorl_layout_margin_right3:I

.field public static oneplus_contorl_layout_margin_right4:I

.field public static oneplus_contorl_layout_margin_right5:I

.field public static oneplus_contorl_layout_margin_right6:I

.field public static oneplus_contorl_layout_margin_right7:I

.field public static oneplus_contorl_layout_margin_right8:I

.field public static oneplus_contorl_layout_margin_top1:I

.field public static oneplus_contorl_layout_margin_top2:I

.field public static oneplus_contorl_layout_margin_top3:I

.field public static oneplus_contorl_layout_margin_top4:I

.field public static oneplus_contorl_layout_margin_top5:I

.field public static oneplus_contorl_list_item_height_one_line1:I

.field public static oneplus_contorl_list_item_height_one_line2:I

.field public static oneplus_contorl_list_item_height_three_line1:I

.field public static oneplus_contorl_list_item_height_two_line1:I

.field public static oneplus_contorl_margin_avatar_bottom1:I

.field public static oneplus_contorl_margin_avatar_bottom2:I

.field public static oneplus_contorl_margin_avatar_bottom3:I

.field public static oneplus_contorl_margin_avatar_bottom4:I

.field public static oneplus_contorl_margin_avatar_left1:I

.field public static oneplus_contorl_margin_avatar_left2:I

.field public static oneplus_contorl_margin_avatar_left3:I

.field public static oneplus_contorl_margin_avatar_left4:I

.field public static oneplus_contorl_margin_avatar_right1:I

.field public static oneplus_contorl_margin_avatar_right2:I

.field public static oneplus_contorl_margin_avatar_right3:I

.field public static oneplus_contorl_margin_avatar_right4:I

.field public static oneplus_contorl_margin_avatar_top1:I

.field public static oneplus_contorl_margin_avatar_top2:I

.field public static oneplus_contorl_margin_avatar_top3:I

.field public static oneplus_contorl_margin_avatar_top4:I

.field public static oneplus_contorl_margin_bottom1:I

.field public static oneplus_contorl_margin_bottom2:I

.field public static oneplus_contorl_margin_bottom3:I

.field public static oneplus_contorl_margin_bottom4:I

.field public static oneplus_contorl_margin_bottom5:I

.field public static oneplus_contorl_margin_left1:I

.field public static oneplus_contorl_margin_left2:I

.field public static oneplus_contorl_margin_left3:I

.field public static oneplus_contorl_margin_left4:I

.field public static oneplus_contorl_margin_left5:I

.field public static oneplus_contorl_margin_left6:I

.field public static oneplus_contorl_margin_left7:I

.field public static oneplus_contorl_margin_left8:I

.field public static oneplus_contorl_margin_right1:I

.field public static oneplus_contorl_margin_right2:I

.field public static oneplus_contorl_margin_right3:I

.field public static oneplus_contorl_margin_right4:I

.field public static oneplus_contorl_margin_right5:I

.field public static oneplus_contorl_margin_right6:I

.field public static oneplus_contorl_margin_right7:I

.field public static oneplus_contorl_margin_right8:I

.field public static oneplus_contorl_margin_text_bottom1:I

.field public static oneplus_contorl_margin_text_bottom2:I

.field public static oneplus_contorl_margin_text_bottom3:I

.field public static oneplus_contorl_margin_text_bottom4:I

.field public static oneplus_contorl_margin_text_bottom5:I

.field public static oneplus_contorl_margin_text_left1:I

.field public static oneplus_contorl_margin_text_left2:I

.field public static oneplus_contorl_margin_text_left3:I

.field public static oneplus_contorl_margin_text_left4:I

.field public static oneplus_contorl_margin_text_right1:I

.field public static oneplus_contorl_margin_text_right2:I

.field public static oneplus_contorl_margin_text_right3:I

.field public static oneplus_contorl_margin_text_right4:I

.field public static oneplus_contorl_margin_text_top1:I

.field public static oneplus_contorl_margin_text_top2:I

.field public static oneplus_contorl_margin_text_top3:I

.field public static oneplus_contorl_margin_text_top4:I

.field public static oneplus_contorl_margin_top1:I

.field public static oneplus_contorl_margin_top2:I

.field public static oneplus_contorl_margin_top3:I

.field public static oneplus_contorl_margin_top4:I

.field public static oneplus_contorl_margin_top5:I

.field public static oneplus_contorl_menu_item_width_1:I

.field public static oneplus_contorl_menu_item_width_2:I

.field public static oneplus_contorl_menu_item_width_3:I

.field public static oneplus_contorl_menu_item_width_4:I

.field public static oneplus_contorl_menu_item_width_5:I

.field public static oneplus_contorl_padding_space1:I

.field public static oneplus_contorl_padding_space2:I

.field public static oneplus_contorl_padding_space3:I

.field public static oneplus_contorl_padding_space4:I

.field public static oneplus_contorl_padding_space5:I

.field public static oneplus_contorl_padding_space6:I

.field public static oneplus_contorl_radius_r0:I

.field public static oneplus_contorl_radius_r12:I

.field public static oneplus_contorl_radius_r16:I

.field public static oneplus_contorl_radius_r2:I

.field public static oneplus_contorl_radius_r4:I

.field public static oneplus_contorl_radius_r8:I

.field public static oneplus_contorl_rotate_angle1:I

.field public static oneplus_contorl_rotate_angle2:I

.field public static oneplus_contorl_rotate_angle3:I

.field public static oneplus_contorl_rotate_angle4:I

.field public static oneplus_contorl_rotate_angle5:I

.field public static oneplus_contorl_rotate_angle6:I

.field public static oneplus_contorl_rotate_angle7:I

.field public static oneplus_contorl_rotate_angle8:I

.field public static oneplus_contorl_shadow_z0:I

.field public static oneplus_contorl_shadow_z1:I

.field public static oneplus_contorl_shadow_z2:I

.field public static oneplus_contorl_shadow_z3:I

.field public static oneplus_contorl_shadow_z4:I

.field public static oneplus_contorl_shadow_z5:I

.field public static oneplus_contorl_tab_item_height_extended:I

.field public static oneplus_contorl_tab_item_height_landscape:I

.field public static oneplus_contorl_tab_item_height_standard:I

.field public static oneplus_contorl_text_disable_alpha:I

.field public static oneplus_contorl_text_hint_alpha:I

.field public static oneplus_contorl_text_label_alpha:I

.field public static oneplus_contorl_text_primary_alpha:I

.field public static oneplus_contorl_text_secondary_alpha:I

.field public static oneplus_contorl_text_size_body1:I

.field public static oneplus_contorl_text_size_body2:I

.field public static oneplus_contorl_text_size_button:I

.field public static oneplus_contorl_text_size_descriptions:I

.field public static oneplus_contorl_text_size_display1:I

.field public static oneplus_contorl_text_size_display2:I

.field public static oneplus_contorl_text_size_display3:I

.field public static oneplus_contorl_text_size_display4:I

.field public static oneplus_contorl_text_size_extended:I

.field public static oneplus_contorl_text_size_headline:I

.field public static oneplus_contorl_text_size_highlight:I

.field public static oneplus_contorl_text_size_subheading:I

.field public static oneplus_contorl_text_size_subtitle:I

.field public static oneplus_contorl_text_size_title:I

.field public static oneplus_contorl_zoom_ratio1:I

.field public static oneplus_contorl_zoom_ratio10:I

.field public static oneplus_contorl_zoom_ratio2:I

.field public static oneplus_contorl_zoom_ratio3:I

.field public static oneplus_contorl_zoom_ratio4:I

.field public static oneplus_contorl_zoom_ratio5:I

.field public static oneplus_contorl_zoom_ratio6:I

.field public static oneplus_contorl_zoom_ratio7:I

.field public static oneplus_contorl_zoom_ratio8:I

.field public static oneplus_contorl_zoom_ratio9:I

.field public static op_abc_action_bar_stacked_max_height:I

.field public static op_abc_action_bar_stacked_tab_max_width:I

.field public static op_abc_cascading_menus_min_smallest_width:I

.field public static op_abc_config_prefDialogWidth:I

.field public static op_action_bar_content_inset_with_nav:I

.field public static op_action_bar_default_height_material:I

.field public static op_background_underline_height:I

.field public static op_bottom_navigation_height_with_bottom_softkey_navigation:I

.field public static op_bottom_navigation_max_height:I

.field public static op_bottom_navigation_min_height:I

.field public static op_design_snackbar_action_inline_max_width:I

.field public static op_design_snackbar_background_corner_radius:I

.field public static op_design_snackbar_elevation:I

.field public static op_design_snackbar_extra_spacing_horizontal:I

.field public static op_design_snackbar_max_height:I

.field public static op_design_snackbar_max_width:I

.field public static op_design_snackbar_min_height:I

.field public static op_design_snackbar_min_width:I

.field public static op_design_snackbar_padding_bottom:I

.field public static op_design_snackbar_padding_left:I

.field public static op_design_snackbar_padding_right:I

.field public static op_design_snackbar_padding_top:I

.field public static op_design_snackbar_padding_vertical_2lines:I

.field public static op_design_snackbar_text_size:I

.field public static op_dialog_background_inset:I

.field public static op_dialog_picker_background_inset:I

.field public static op_disabled_alpha_material_dark:I

.field public static op_disabled_alpha_material_light:I

.field public static op_highlight_alpha_material_dark:I

.field public static op_highlight_alpha_material_light:I

.field public static op_notification_action_icon_size:I

.field public static op_notification_action_text_size:I

.field public static op_notification_big_circle_margin:I

.field public static op_notification_content_margin_start:I

.field public static op_notification_large_icon_height:I

.field public static op_notification_large_icon_width:I

.field public static op_notification_main_column_padding_top:I

.field public static op_notification_media_narrow_margin:I

.field public static op_notification_right_icon_size:I

.field public static op_notification_right_side_padding_top:I

.field public static op_notification_small_icon_background_padding:I

.field public static op_notification_small_icon_size_as_large:I

.field public static op_notification_subtext_size:I

.field public static op_notification_top_pad:I

.field public static op_notification_top_pad_large_text:I

.field public static op_progress_dialog_right_padding:I

.field public static op_qs_page_indicator_height:I

.field public static op_qs_page_indicator_width:I

.field public static op_reduce_padding_space2:I

.field public static op_tabs_padding_end:I

.field public static op_tabs_padding_start:I

.field public static op_text_size_menu_material:I

.field public static op_tooltip_corner_radius:I

.field public static op_tooltip_horizontal_padding:I

.field public static op_tooltip_margin:I

.field public static op_tooltip_precise_anchor_extra_offset:I

.field public static op_tooltip_precise_anchor_threshold:I

.field public static op_tooltip_vertical_padding:I

.field public static op_tooltip_y_offset_non_touch:I

.field public static op_tooltip_y_offset_touch:I

.field public static opcardview_margin:I

.field public static opgridview_margin:I

.field public static picker_bottom_margin:I

.field public static picker_top_margin:I

.field public static preference_breadcrumbs_padding_end_material:I

.field public static preference_breadcrumbs_padding_start_material:I

.field public static preference_category_font_size:I

.field public static preference_divider_height:I

.field public static preference_fragment_padding_bottom:I

.field public static preference_fragment_padding_side:I

.field public static preference_fragment_padding_side_material:I

.field public static preference_icon_minWidth:I

.field public static preference_screen_header_padding_side_material:I

.field public static preference_screen_header_vertical_padding_material:I

.field public static preference_screen_side_margin_material:I

.field public static preference_screen_side_margin_negative_material:I

.field public static preference_widget_width:I

.field public static preferences_icon_top_padding:I

.field public static preferences_primary_text_top_padding:I

.field public static preferences_right_icon_left_padding:I

.field public static preferences_secondary_text_bottom_padding:I

.field public static progress_bar_height_material:I

.field public static ripple_radius:I

.field public static searchResultListItemHeight:I

.field public static search_view_preferred_height:I

.field public static search_view_preferred_width:I

.field public static search_view_text_min_width:I

.field public static seekbar_thumb_optical_inset:I

.field public static seekbar_thumb_optical_inset_disabled:I

.field public static seekbar_track_background_height_material:I

.field public static seekbar_track_progress_height_material:I

.field public static select_dialog_padding_start_material:I

.field public static spinner_material_padding_left:I

.field public static spinner_material_padding_right:I

.field public static subtitle_corner_radius:I

.field public static subtitle_outline_width:I

.field public static subtitle_shadow_offset:I

.field public static subtitle_shadow_radius:I

.field public static switch_ripple_radius:I

.field public static tab_layout_default_height_material:I

.field public static tab_selected:I

.field public static tab_unselected:I

.field public static text_primary:I

.field public static text_secondary:I

.field public static text_selection_highlight_alpha_material:I

.field public static text_size_body_1_material:I

.field public static text_size_body_2_material:I

.field public static text_size_button_material:I

.field public static text_size_caption_material:I

.field public static text_size_display_1_material:I

.field public static text_size_display_2_material:I

.field public static text_size_display_3_material:I

.field public static text_size_display_4_material:I

.field public static text_size_headline_material:I

.field public static text_size_large_material:I

.field public static text_size_medium_material:I

.field public static text_size_menu_material:I

.field public static text_size_small_material:I

.field public static text_size_subhead_material:I

.field public static text_size_subtitle_material_toolbar:I

.field public static text_size_title_material:I

.field public static text_size_title_material_toolbar:I

.field public static text_subhead:I

.field public static timepicker_am_top_padding:I

.field public static timepicker_ampm_horizontal_padding:I

.field public static timepicker_ampm_label_size:I

.field public static timepicker_ampm_vertical_padding:I

.field public static timepicker_center_dot_radius:I

.field public static timepicker_edit_text_size:I

.field public static timepicker_extra_time_label_margin:I

.field public static timepicker_header_height:I

.field public static timepicker_input_field_width:I

.field public static timepicker_minimum_margin_sides:I

.field public static timepicker_minimum_margin_top_bottom:I

.field public static timepicker_pm_top_padding:I

.field public static timepicker_radial_picker_dimen:I

.field public static timepicker_radial_picker_left_margin:I

.field public static timepicker_radial_picker_right_margin:I

.field public static timepicker_radial_picker_top_margin:I

.field public static timepicker_selector_dot_radius:I

.field public static timepicker_selector_radius:I

.field public static timepicker_selector_stroke:I

.field public static timepicker_separator_padding:I

.field public static timepicker_text_inset_inner:I

.field public static timepicker_text_inset_normal:I

.field public static timepicker_text_size_inner:I

.field public static timepicker_text_size_normal:I

.field public static timepicker_time_label_size:I

.field public static toast_y_offset:I

.field public static toolbar_elevation:I

.field public static toolbar_icon_min_width:I

.field public static toolbar_text_size_subtitle:I

.field public static toolbar_text_size_title:I

.field public static tooltip_corner_radius:I

.field public static tooltip_horizontal_padding:I

.field public static tooltip_margin:I

.field public static tooltip_precise_anchor_extra_offset:I

.field public static tooltip_precise_anchor_threshold:I

.field public static tooltip_vertical_padding:I

.field public static tooltip_y_offset_non_touch:I

.field public static tooltip_y_offset_touch:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5678
    const/4 v0, 0x0

    sput v0, Landroid/support/interpolator/R$dimen;->ab_double_subhead:I

    .line 5679
    sput v0, Landroid/support/interpolator/R$dimen;->ab_double_title:I

    .line 5683
    sput v0, Landroid/support/interpolator/R$dimen;->ab_single_title:I

    .line 5684
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_content_inset_material:I

    .line 5685
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_content_inset_with_nav:I

    .line 5686
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_default_height_material:I

    .line 5687
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_default_padding_end_material:I

    .line 5688
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_default_padding_start_material:I

    .line 5689
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_elevation_material:I

    .line 5690
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_icon_vertical_padding_material:I

    .line 5691
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_overflow_padding_end_material:I

    .line 5692
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_overflow_padding_start_material:I

    .line 5693
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_stacked_max_height:I

    .line 5694
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_stacked_tab_max_width:I

    .line 5695
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_subtitle_bottom_margin_material:I

    .line 5696
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_bar_subtitle_top_margin_material:I

    .line 5697
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_button_min_height_material:I

    .line 5698
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_button_min_width_material:I

    .line 5699
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_button_min_width_overflow_material:I

    .line 5700
    sput v0, Landroid/support/interpolator/R$dimen;->abc_action_menu_icon_size:I

    .line 5701
    sput v0, Landroid/support/interpolator/R$dimen;->abc_alert_dialog_button_bar_height:I

    .line 5702
    sput v0, Landroid/support/interpolator/R$dimen;->abc_alert_dialog_button_dimen:I

    .line 5703
    sput v0, Landroid/support/interpolator/R$dimen;->abc_button_inset_horizontal_material:I

    .line 5704
    sput v0, Landroid/support/interpolator/R$dimen;->abc_button_inset_vertical_material:I

    .line 5705
    sput v0, Landroid/support/interpolator/R$dimen;->abc_button_padding_horizontal_material:I

    .line 5706
    sput v0, Landroid/support/interpolator/R$dimen;->abc_button_padding_vertical_material:I

    .line 5707
    sput v0, Landroid/support/interpolator/R$dimen;->abc_cascading_menus_min_smallest_width:I

    .line 5708
    sput v0, Landroid/support/interpolator/R$dimen;->abc_config_prefDialogWidth:I

    .line 5709
    sput v0, Landroid/support/interpolator/R$dimen;->abc_control_corner_material:I

    .line 5710
    sput v0, Landroid/support/interpolator/R$dimen;->abc_control_inset_material:I

    .line 5711
    sput v0, Landroid/support/interpolator/R$dimen;->abc_control_padding_material:I

    .line 5712
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_corner_radius_material:I

    .line 5713
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_fixed_height_major:I

    .line 5714
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_fixed_height_minor:I

    .line 5715
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_fixed_width_major:I

    .line 5716
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_fixed_width_minor:I

    .line 5717
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_list_padding_bottom_no_buttons:I

    .line 5718
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_list_padding_top_no_title:I

    .line 5719
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_min_width_major:I

    .line 5720
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_min_width_minor:I

    .line 5721
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_padding_material:I

    .line 5722
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_padding_top_material:I

    .line 5723
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dialog_title_divider_material:I

    .line 5724
    sput v0, Landroid/support/interpolator/R$dimen;->abc_disabled_alpha_material_dark:I

    .line 5725
    sput v0, Landroid/support/interpolator/R$dimen;->abc_disabled_alpha_material_light:I

    .line 5726
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dropdownitem_icon_width:I

    .line 5727
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dropdownitem_text_padding_left:I

    .line 5728
    sput v0, Landroid/support/interpolator/R$dimen;->abc_dropdownitem_text_padding_right:I

    .line 5729
    sput v0, Landroid/support/interpolator/R$dimen;->abc_edit_text_inset_bottom_material:I

    .line 5730
    sput v0, Landroid/support/interpolator/R$dimen;->abc_edit_text_inset_horizontal_material:I

    .line 5731
    sput v0, Landroid/support/interpolator/R$dimen;->abc_edit_text_inset_top_material:I

    .line 5732
    sput v0, Landroid/support/interpolator/R$dimen;->abc_floating_window_z:I

    .line 5733
    sput v0, Landroid/support/interpolator/R$dimen;->abc_list_item_padding_horizontal_material:I

    .line 5734
    sput v0, Landroid/support/interpolator/R$dimen;->abc_panel_menu_list_width:I

    .line 5735
    sput v0, Landroid/support/interpolator/R$dimen;->abc_progress_bar_height_material:I

    .line 5736
    sput v0, Landroid/support/interpolator/R$dimen;->abc_search_view_preferred_height:I

    .line 5737
    sput v0, Landroid/support/interpolator/R$dimen;->abc_search_view_preferred_width:I

    .line 5738
    sput v0, Landroid/support/interpolator/R$dimen;->abc_seekbar_track_background_height_material:I

    .line 5739
    sput v0, Landroid/support/interpolator/R$dimen;->abc_seekbar_track_progress_height_material:I

    .line 5740
    sput v0, Landroid/support/interpolator/R$dimen;->abc_select_dialog_padding_start_material:I

    .line 5741
    sput v0, Landroid/support/interpolator/R$dimen;->abc_switch_padding:I

    .line 5742
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_body_1_material:I

    .line 5743
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_body_2_material:I

    .line 5744
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_button_material:I

    .line 5745
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_caption_material:I

    .line 5746
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_display_1_material:I

    .line 5747
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_display_2_material:I

    .line 5748
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_display_3_material:I

    .line 5749
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_display_4_material:I

    .line 5750
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_headline_material:I

    .line 5751
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_large_material:I

    .line 5752
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_medium_material:I

    .line 5753
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_menu_header_material:I

    .line 5754
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_menu_material:I

    .line 5755
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_small_material:I

    .line 5756
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_subhead_material:I

    .line 5757
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_subtitle_material_toolbar:I

    .line 5758
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_title_material:I

    .line 5759
    sput v0, Landroid/support/interpolator/R$dimen;->abc_text_size_title_material_toolbar:I

    .line 5763
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_content_inset_material:I

    .line 5767
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_default_height_material:I

    .line 5771
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_default_padding_end_material:I

    .line 5775
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_default_padding_start_material:I

    .line 5776
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_elevation_material:I

    .line 5780
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_icon_vertical_padding_material:I

    .line 5784
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_overflow_padding_end_material:I

    .line 5788
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_overflow_padding_start_material:I

    .line 5792
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_subtitle_bottom_margin_material:I

    .line 5796
    sput v0, Landroid/support/interpolator/R$dimen;->action_bar_subtitle_top_margin_material:I

    .line 5797
    sput v0, Landroid/support/interpolator/R$dimen;->action_button_min_height_material:I

    .line 5798
    sput v0, Landroid/support/interpolator/R$dimen;->action_button_min_width_material:I

    .line 5799
    sput v0, Landroid/support/interpolator/R$dimen;->action_button_min_width_overflow_material:I

    .line 5800
    sput v0, Landroid/support/interpolator/R$dimen;->actionbar_elevation:I

    .line 5804
    sput v0, Landroid/support/interpolator/R$dimen;->actionbar_height:I

    .line 5805
    sput v0, Landroid/support/interpolator/R$dimen;->actionbar_searchview_icon_alpha:I

    .line 5806
    sput v0, Landroid/support/interpolator/R$dimen;->actionbar_searchview_icon_alpha_disable:I

    .line 5810
    sput v0, Landroid/support/interpolator/R$dimen;->alert_dialog_round_padding:I

    .line 5811
    sput v0, Landroid/support/interpolator/R$dimen;->alert_dialog_title_style_font_size:I

    .line 5812
    sput v0, Landroid/support/interpolator/R$dimen;->appbar_layout_default_height_material:I

    .line 5813
    sput v0, Landroid/support/interpolator/R$dimen;->bottom_navigation_badge_mini_width:I

    .line 5817
    sput v0, Landroid/support/interpolator/R$dimen;->bottom_navigation_badge_size_indeterminate:I

    .line 5818
    sput v0, Landroid/support/interpolator/R$dimen;->bottom_navigation_badge_text_horizontal_padding:I

    .line 5822
    sput v0, Landroid/support/interpolator/R$dimen;->bottom_navigation_badge_text_size:I

    .line 5826
    sput v0, Landroid/support/interpolator/R$dimen;->btn_highlight_alpha_material:I

    .line 5827
    sput v0, Landroid/support/interpolator/R$dimen;->btn_highlight_alpha_material_dark:I

    .line 5831
    sput v0, Landroid/support/interpolator/R$dimen;->btn_highlight_alpha_material_light:I

    .line 5832
    sput v0, Landroid/support/interpolator/R$dimen;->button:I

    .line 5836
    sput v0, Landroid/support/interpolator/R$dimen;->button_elevation_material:I

    .line 5837
    sput v0, Landroid/support/interpolator/R$dimen;->button_inset_horizontal_material:I

    .line 5841
    sput v0, Landroid/support/interpolator/R$dimen;->button_inset_vertical_material:I

    .line 5842
    sput v0, Landroid/support/interpolator/R$dimen;->button_padding_horizontal_material:I

    .line 5846
    sput v0, Landroid/support/interpolator/R$dimen;->button_padding_vertical_material:I

    .line 5850
    sput v0, Landroid/support/interpolator/R$dimen;->button_pressed_z_material:I

    .line 5854
    sput v0, Landroid/support/interpolator/R$dimen;->cardview_compat_inset_shadow:I

    .line 5858
    sput v0, Landroid/support/interpolator/R$dimen;->cardview_default_elevation:I

    .line 5862
    sput v0, Landroid/support/interpolator/R$dimen;->cardview_default_radius:I

    .line 5863
    sput v0, Landroid/support/interpolator/R$dimen;->compat_button_inset_horizontal_material:I

    .line 5864
    sput v0, Landroid/support/interpolator/R$dimen;->compat_button_inset_vertical_material:I

    .line 5865
    sput v0, Landroid/support/interpolator/R$dimen;->compat_button_padding_horizontal_material:I

    .line 5869
    sput v0, Landroid/support/interpolator/R$dimen;->compat_button_padding_vertical_material:I

    .line 5873
    sput v0, Landroid/support/interpolator/R$dimen;->compat_control_corner_material:I

    .line 5874
    sput v0, Landroid/support/interpolator/R$dimen;->compat_notification_large_icon_max_height:I

    .line 5875
    sput v0, Landroid/support/interpolator/R$dimen;->compat_notification_large_icon_max_width:I

    .line 5879
    sput v0, Landroid/support/interpolator/R$dimen;->compat_op_notification_large_icon_max_height:I

    .line 5883
    sput v0, Landroid/support/interpolator/R$dimen;->compat_op_notification_large_icon_max_width:I

    .line 5887
    sput v0, Landroid/support/interpolator/R$dimen;->control_corner_material:I

    .line 5891
    sput v0, Landroid/support/interpolator/R$dimen;->control_inset_material:I

    .line 5895
    sput v0, Landroid/support/interpolator/R$dimen;->control_padding_material:I

    .line 5896
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_date_label_size:I

    .line 5897
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_day_height:I

    .line 5898
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_day_of_week_height:I

    .line 5899
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_day_of_week_text_size:I

    .line 5900
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_day_selector_radius:I

    .line 5901
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_day_text_size:I

    .line 5902
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_day_width:I

    .line 5903
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_month_height:I

    .line 5907
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_month_text_size:I

    .line 5908
    sput v0, Landroid/support/interpolator/R$dimen;->date_picker_year_label_size:I

    .line 5909
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_component_width:I

    .line 5910
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_day_number_select_circle_radius:I

    .line 5914
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_day_number_size:I

    .line 5915
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_dialog_width:I

    .line 5916
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_header_height:I

    .line 5917
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_header_text_size:I

    .line 5918
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_list_year_activated_label_size:I

    .line 5919
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_list_year_label_size:I

    .line 5920
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_month_day_label_text_size:I

    .line 5921
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_month_label_size:I

    .line 5922
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_month_list_item_header_height:I

    .line 5923
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_selected_date_day_size:I

    .line 5924
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_selected_date_month_size:I

    .line 5925
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_selected_date_year_size:I

    .line 5926
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_view_animator_height:I

    .line 5930
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_year_label_height:I

    .line 5934
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_year_label_text_size:I

    .line 5935
    sput v0, Landroid/support/interpolator/R$dimen;->datepicker_year_picker_padding_top:I

    .line 5936
    sput v0, Landroid/support/interpolator/R$dimen;->day_picker_button_margin_top:I

    .line 5937
    sput v0, Landroid/support/interpolator/R$dimen;->day_picker_padding_horizontal:I

    .line 5938
    sput v0, Landroid/support/interpolator/R$dimen;->day_picker_padding_top:I

    .line 5939
    sput v0, Landroid/support/interpolator/R$dimen;->design_appbar_elevation:I

    .line 5940
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_active_item_max_width:I

    .line 5941
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_active_item_min_width:I

    .line 5942
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_active_text_size:I

    .line 5943
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_elevation:I

    .line 5944
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_height:I

    .line 5945
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_icon_size:I

    .line 5946
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_item_max_width:I

    .line 5947
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_item_min_width:I

    .line 5948
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_margin:I

    .line 5949
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_shadow_height:I

    .line 5950
    sput v0, Landroid/support/interpolator/R$dimen;->design_bottom_navigation_text_size:I

    .line 5951
    sput v0, Landroid/support/interpolator/R$dimen;->design_fab_border_width:I

    .line 5952
    sput v0, Landroid/support/interpolator/R$dimen;->design_fab_content_size:I

    .line 5956
    sput v0, Landroid/support/interpolator/R$dimen;->design_fab_elevation:I

    .line 5957
    sput v0, Landroid/support/interpolator/R$dimen;->design_fab_size_mini:I

    .line 5958
    sput v0, Landroid/support/interpolator/R$dimen;->design_fab_size_normal:I

    .line 5959
    sput v0, Landroid/support/interpolator/R$dimen;->design_fab_translation_z_pressed:I

    .line 5960
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_fixed_height_major:I

    .line 5961
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_fixed_height_minor:I

    .line 5962
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_fixed_width_major:I

    .line 5963
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_fixed_width_minor:I

    .line 5967
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_list_padding_vertical_material:I

    .line 5968
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_min_width_major:I

    .line 5969
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_min_width_minor:I

    .line 5970
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_padding_material:I

    .line 5971
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_padding_top_material:I

    .line 5972
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_text:I

    .line 5973
    sput v0, Landroid/support/interpolator/R$dimen;->dialog_title:I

    .line 5974
    sput v0, Landroid/support/interpolator/R$dimen;->disabledAlpha_dark:I

    .line 5978
    sput v0, Landroid/support/interpolator/R$dimen;->disabledAlpha_light:I

    .line 5979
    sput v0, Landroid/support/interpolator/R$dimen;->disabled_alpha_material_dark:I

    .line 5980
    sput v0, Landroid/support/interpolator/R$dimen;->disabled_alpha_material_light:I

    .line 5984
    sput v0, Landroid/support/interpolator/R$dimen;->dropdownitem_icon_width:I

    .line 5985
    sput v0, Landroid/support/interpolator/R$dimen;->dropdownitem_text_padding_left:I

    .line 5986
    sput v0, Landroid/support/interpolator/R$dimen;->dropdownitem_text_padding_right:I

    .line 5987
    sput v0, Landroid/support/interpolator/R$dimen;->edit_text_inset_bottom_material:I

    .line 5988
    sput v0, Landroid/support/interpolator/R$dimen;->edit_text_inset_horizontal_material:I

    .line 5989
    sput v0, Landroid/support/interpolator/R$dimen;->edit_text_inset_top_material:I

    .line 5990
    sput v0, Landroid/support/interpolator/R$dimen;->floating_window_margin_bottom:I

    .line 5991
    sput v0, Landroid/support/interpolator/R$dimen;->floating_window_margin_left:I

    .line 5992
    sput v0, Landroid/support/interpolator/R$dimen;->floating_window_margin_right:I

    .line 5993
    sput v0, Landroid/support/interpolator/R$dimen;->floating_window_margin_top:I

    .line 5994
    sput v0, Landroid/support/interpolator/R$dimen;->floating_window_z:I

    .line 5995
    sput v0, Landroid/support/interpolator/R$dimen;->highlight_alpha_material_colored:I

    .line 5996
    sput v0, Landroid/support/interpolator/R$dimen;->highlight_alpha_material_dark:I

    .line 5997
    sput v0, Landroid/support/interpolator/R$dimen;->highlight_alpha_material_light:I

    .line 5998
    sput v0, Landroid/support/interpolator/R$dimen;->hint_alpha_material_dark:I

    .line 5999
    sput v0, Landroid/support/interpolator/R$dimen;->hint_alpha_material_light:I

    .line 6000
    sput v0, Landroid/support/interpolator/R$dimen;->hint_pressed_alpha_material_dark:I

    .line 6001
    sput v0, Landroid/support/interpolator/R$dimen;->hint_pressed_alpha_material_light:I

    .line 6005
    sput v0, Landroid/support/interpolator/R$dimen;->item_touch_helper_max_drag_scroll_per_frame:I

    .line 6006
    sput v0, Landroid/support/interpolator/R$dimen;->list_date:I

    .line 6011
    sput v0, Landroid/support/interpolator/R$dimen;->list_item_padding_horizontal_material:I

    .line 6012
    sput v0, Landroid/support/interpolator/R$dimen;->list_module_title:I

    .line 6013
    sput v0, Landroid/support/interpolator/R$dimen;->list_text:I

    .line 6014
    sput v0, Landroid/support/interpolator/R$dimen;->list_title:I

    .line 6015
    sput v0, Landroid/support/interpolator/R$dimen;->listitem_actionbutton_size:I

    .line 6016
    sput v0, Landroid/support/interpolator/R$dimen;->listitem_height:I

    .line 6017
    sput v0, Landroid/support/interpolator/R$dimen;->listitem_height_tall:I

    .line 6021
    sput v0, Landroid/support/interpolator/R$dimen;->listitem_icon_size:I

    .line 6022
    sput v0, Landroid/support/interpolator/R$dimen;->listitem_primatytext_size:I

    .line 6023
    sput v0, Landroid/support/interpolator/R$dimen;->listitem_secondarytext_size:I

    .line 6024
    sput v0, Landroid/support/interpolator/R$dimen;->listitem_stamp_size:I

    .line 6028
    sput v0, Landroid/support/interpolator/R$dimen;->listview_divider_height:I

    .line 6032
    sput v0, Landroid/support/interpolator/R$dimen;->margin_m1:I

    .line 6033
    sput v0, Landroid/support/interpolator/R$dimen;->notification_action_icon_size:I

    .line 6034
    sput v0, Landroid/support/interpolator/R$dimen;->notification_action_text_size:I

    .line 6035
    sput v0, Landroid/support/interpolator/R$dimen;->notification_big_circle_margin:I

    .line 6036
    sput v0, Landroid/support/interpolator/R$dimen;->notification_content_margin_start:I

    .line 6037
    sput v0, Landroid/support/interpolator/R$dimen;->notification_large_icon_height:I

    .line 6038
    sput v0, Landroid/support/interpolator/R$dimen;->notification_large_icon_width:I

    .line 6039
    sput v0, Landroid/support/interpolator/R$dimen;->notification_main_column_padding_top:I

    .line 6040
    sput v0, Landroid/support/interpolator/R$dimen;->notification_media_narrow_margin:I

    .line 6041
    sput v0, Landroid/support/interpolator/R$dimen;->notification_right_icon_size:I

    .line 6042
    sput v0, Landroid/support/interpolator/R$dimen;->notification_right_side_padding_top:I

    .line 6043
    sput v0, Landroid/support/interpolator/R$dimen;->notification_small_icon_background_padding:I

    .line 6044
    sput v0, Landroid/support/interpolator/R$dimen;->notification_small_icon_size_as_large:I

    .line 6045
    sput v0, Landroid/support/interpolator/R$dimen;->notification_subtext_size:I

    .line 6046
    sput v0, Landroid/support/interpolator/R$dimen;->notification_top_pad:I

    .line 6047
    sput v0, Landroid/support/interpolator/R$dimen;->notification_top_pad_large_text:I

    .line 6048
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_appbar_with_tab_size1:I

    .line 6049
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_appbar_with_tab_size2:I

    .line 6050
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_button_default_height:I

    .line 6051
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_button_mini_height:I

    .line 6052
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_button_mini_width:I

    .line 6056
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha1:I

    .line 6057
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha10:I

    .line 6058
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha11:I

    .line 6059
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha2:I

    .line 6060
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha3:I

    .line 6061
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha4:I

    .line 6062
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha5:I

    .line 6063
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha6:I

    .line 6064
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha7:I

    .line 6065
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha8:I

    .line 6066
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_alpha9:I

    .line 6067
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_appbar_size_extended1:I

    .line 6068
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_appbar_size_extended2:I

    .line 6069
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_appbar_size_landscape:I

    .line 6073
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_appbar_size_standard:I

    .line 6074
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_avatar_mini:I

    .line 6079
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_avatar_standard:I

    .line 6083
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_button_item_height_accent:I

    .line 6084
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_button_item_height_standard:I

    .line 6088
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_dialog_item_width_1:I

    .line 6089
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_dialog_item_width_2:I

    .line 6090
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_dialog_item_width_3:I

    .line 6091
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_dialog_item_width_4:I

    .line 6092
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_dialog_item_width_5:I

    .line 6093
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_divider_height_bold:I

    .line 6097
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_divider_height_standard:I

    .line 6098
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_fab_size_mini:I

    .line 6099
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_fab_size_scrolling:I

    .line 6104
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_fab_size_standard:I

    .line 6105
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_accent_active_alpha:I

    .line 6106
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_accent_inactive_alpha:I

    .line 6107
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_active_alpha:I

    .line 6108
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_inactive_alpha:I

    .line 6109
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_button:I

    .line 6110
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_display1:I

    .line 6111
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_display2:I

    .line 6112
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_display3:I

    .line 6113
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_display4:I

    .line 6118
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_display5:I

    .line 6119
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_indicator:I

    .line 6120
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_icon_size_list:I

    .line 6121
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_bottom1:I

    .line 6122
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_bottom2:I

    .line 6123
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_bottom3:I

    .line 6124
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_bottom4:I

    .line 6125
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_bottom5:I

    .line 6129
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left1:I

    .line 6130
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left2:I

    .line 6131
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left3:I

    .line 6132
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left4:I

    .line 6133
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left5:I

    .line 6134
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left6:I

    .line 6135
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left7:I

    .line 6136
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_left8:I

    .line 6137
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right1:I

    .line 6138
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right2:I

    .line 6139
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right3:I

    .line 6140
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right4:I

    .line 6141
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right5:I

    .line 6142
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right6:I

    .line 6143
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right7:I

    .line 6144
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_right8:I

    .line 6145
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_top1:I

    .line 6146
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_top2:I

    .line 6147
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_top3:I

    .line 6148
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_top4:I

    .line 6149
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_layout_margin_top5:I

    .line 6153
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_list_item_height_one_line1:I

    .line 6154
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_list_item_height_one_line2:I

    .line 6155
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_list_item_height_three_line1:I

    .line 6156
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_list_item_height_two_line1:I

    .line 6157
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_bottom1:I

    .line 6158
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_bottom2:I

    .line 6159
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_bottom3:I

    .line 6160
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_bottom4:I

    .line 6164
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_left1:I

    .line 6165
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_left2:I

    .line 6166
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_left3:I

    .line 6167
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_left4:I

    .line 6168
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_right1:I

    .line 6169
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_right2:I

    .line 6170
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_right3:I

    .line 6171
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_right4:I

    .line 6172
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_top1:I

    .line 6173
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_top2:I

    .line 6174
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_top3:I

    .line 6175
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_avatar_top4:I

    .line 6176
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_bottom1:I

    .line 6177
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_bottom2:I

    .line 6178
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_bottom3:I

    .line 6179
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_bottom4:I

    .line 6180
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_bottom5:I

    .line 6181
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left1:I

    .line 6182
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left2:I

    .line 6183
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left3:I

    .line 6184
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left4:I

    .line 6185
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left5:I

    .line 6186
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left6:I

    .line 6187
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left7:I

    .line 6188
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_left8:I

    .line 6189
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right1:I

    .line 6190
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right2:I

    .line 6191
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right3:I

    .line 6192
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right4:I

    .line 6193
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right5:I

    .line 6194
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right6:I

    .line 6195
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right7:I

    .line 6196
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_right8:I

    .line 6197
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_bottom1:I

    .line 6198
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_bottom2:I

    .line 6199
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_bottom3:I

    .line 6200
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_bottom4:I

    .line 6201
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_bottom5:I

    .line 6205
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_left1:I

    .line 6206
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_left2:I

    .line 6207
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_left3:I

    .line 6208
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_left4:I

    .line 6209
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_right1:I

    .line 6210
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_right2:I

    .line 6211
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_right3:I

    .line 6212
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_right4:I

    .line 6213
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_top1:I

    .line 6214
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_top2:I

    .line 6215
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_top3:I

    .line 6216
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_text_top4:I

    .line 6217
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_top1:I

    .line 6218
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_top2:I

    .line 6219
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_top3:I

    .line 6220
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_top4:I

    .line 6221
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_margin_top5:I

    .line 6225
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_menu_item_width_1:I

    .line 6226
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_menu_item_width_2:I

    .line 6227
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_menu_item_width_3:I

    .line 6228
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_menu_item_width_4:I

    .line 6229
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_menu_item_width_5:I

    .line 6233
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_padding_space1:I

    .line 6234
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_padding_space2:I

    .line 6235
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_padding_space3:I

    .line 6236
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_padding_space4:I

    .line 6237
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_padding_space5:I

    .line 6238
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_padding_space6:I

    .line 6239
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_radius_r0:I

    .line 6240
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_radius_r12:I

    .line 6244
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_radius_r16:I

    .line 6245
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_radius_r2:I

    .line 6246
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_radius_r4:I

    .line 6247
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_radius_r8:I

    .line 6251
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle1:I

    .line 6252
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle2:I

    .line 6253
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle3:I

    .line 6254
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle4:I

    .line 6255
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle5:I

    .line 6256
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle6:I

    .line 6257
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle7:I

    .line 6258
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_rotate_angle8:I

    .line 6259
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_shadow_z0:I

    .line 6260
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_shadow_z1:I

    .line 6261
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_shadow_z2:I

    .line 6262
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_shadow_z3:I

    .line 6263
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_shadow_z4:I

    .line 6267
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_shadow_z5:I

    .line 6268
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_tab_item_height_extended:I

    .line 6269
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_tab_item_height_landscape:I

    .line 6273
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_tab_item_height_standard:I

    .line 6274
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_disable_alpha:I

    .line 6275
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_hint_alpha:I

    .line 6276
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_label_alpha:I

    .line 6277
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_primary_alpha:I

    .line 6278
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_secondary_alpha:I

    .line 6279
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_body1:I

    .line 6280
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_body2:I

    .line 6281
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_button:I

    .line 6282
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_descriptions:I

    .line 6283
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_display1:I

    .line 6284
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_display2:I

    .line 6285
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_display3:I

    .line 6286
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_display4:I

    .line 6287
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_extended:I

    .line 6288
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_headline:I

    .line 6289
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_highlight:I

    .line 6290
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_subheading:I

    .line 6291
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_subtitle:I

    .line 6292
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_text_size_title:I

    .line 6296
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio1:I

    .line 6297
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio10:I

    .line 6298
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio2:I

    .line 6299
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio3:I

    .line 6300
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio4:I

    .line 6301
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio5:I

    .line 6302
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio6:I

    .line 6303
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio7:I

    .line 6304
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio8:I

    .line 6305
    sput v0, Landroid/support/interpolator/R$dimen;->oneplus_contorl_zoom_ratio9:I

    .line 6306
    sput v0, Landroid/support/interpolator/R$dimen;->op_abc_action_bar_stacked_max_height:I

    .line 6307
    sput v0, Landroid/support/interpolator/R$dimen;->op_abc_action_bar_stacked_tab_max_width:I

    .line 6308
    sput v0, Landroid/support/interpolator/R$dimen;->op_abc_cascading_menus_min_smallest_width:I

    .line 6309
    sput v0, Landroid/support/interpolator/R$dimen;->op_abc_config_prefDialogWidth:I

    .line 6310
    sput v0, Landroid/support/interpolator/R$dimen;->op_action_bar_content_inset_with_nav:I

    .line 6311
    sput v0, Landroid/support/interpolator/R$dimen;->op_action_bar_default_height_material:I

    .line 6312
    sput v0, Landroid/support/interpolator/R$dimen;->op_background_underline_height:I

    .line 6313
    sput v0, Landroid/support/interpolator/R$dimen;->op_bottom_navigation_height_with_bottom_softkey_navigation:I

    .line 6314
    sput v0, Landroid/support/interpolator/R$dimen;->op_bottom_navigation_max_height:I

    .line 6315
    sput v0, Landroid/support/interpolator/R$dimen;->op_bottom_navigation_min_height:I

    .line 6320
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_action_inline_max_width:I

    .line 6321
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_background_corner_radius:I

    .line 6322
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_elevation:I

    .line 6326
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_extra_spacing_horizontal:I

    .line 6327
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_max_height:I

    .line 6328
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_max_width:I

    .line 6329
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_min_height:I

    .line 6333
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_min_width:I

    .line 6334
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_padding_bottom:I

    .line 6335
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_padding_left:I

    .line 6336
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_padding_right:I

    .line 6337
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_padding_top:I

    .line 6338
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_padding_vertical_2lines:I

    .line 6339
    sput v0, Landroid/support/interpolator/R$dimen;->op_design_snackbar_text_size:I

    .line 6340
    sput v0, Landroid/support/interpolator/R$dimen;->op_dialog_background_inset:I

    .line 6341
    sput v0, Landroid/support/interpolator/R$dimen;->op_dialog_picker_background_inset:I

    .line 6342
    sput v0, Landroid/support/interpolator/R$dimen;->op_disabled_alpha_material_dark:I

    .line 6343
    sput v0, Landroid/support/interpolator/R$dimen;->op_disabled_alpha_material_light:I

    .line 6344
    sput v0, Landroid/support/interpolator/R$dimen;->op_highlight_alpha_material_dark:I

    .line 6348
    sput v0, Landroid/support/interpolator/R$dimen;->op_highlight_alpha_material_light:I

    .line 6352
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_action_icon_size:I

    .line 6356
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_action_text_size:I

    .line 6360
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_big_circle_margin:I

    .line 6364
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_content_margin_start:I

    .line 6368
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_large_icon_height:I

    .line 6372
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_large_icon_width:I

    .line 6376
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_main_column_padding_top:I

    .line 6380
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_media_narrow_margin:I

    .line 6384
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_right_icon_size:I

    .line 6388
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_right_side_padding_top:I

    .line 6392
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_small_icon_background_padding:I

    .line 6396
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_small_icon_size_as_large:I

    .line 6401
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_subtext_size:I

    .line 6405
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_top_pad:I

    .line 6409
    sput v0, Landroid/support/interpolator/R$dimen;->op_notification_top_pad_large_text:I

    .line 6410
    sput v0, Landroid/support/interpolator/R$dimen;->op_progress_dialog_right_padding:I

    .line 6411
    sput v0, Landroid/support/interpolator/R$dimen;->op_qs_page_indicator_height:I

    .line 6412
    sput v0, Landroid/support/interpolator/R$dimen;->op_qs_page_indicator_width:I

    .line 6413
    sput v0, Landroid/support/interpolator/R$dimen;->op_reduce_padding_space2:I

    .line 6414
    sput v0, Landroid/support/interpolator/R$dimen;->op_tabs_padding_end:I

    .line 6418
    sput v0, Landroid/support/interpolator/R$dimen;->op_tabs_padding_start:I

    .line 6422
    sput v0, Landroid/support/interpolator/R$dimen;->op_text_size_menu_material:I

    .line 6426
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_corner_radius:I

    .line 6430
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_horizontal_padding:I

    .line 6434
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_margin:I

    .line 6438
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_precise_anchor_extra_offset:I

    .line 6443
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_precise_anchor_threshold:I

    .line 6447
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_vertical_padding:I

    .line 6451
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_y_offset_non_touch:I

    .line 6455
    sput v0, Landroid/support/interpolator/R$dimen;->op_tooltip_y_offset_touch:I

    .line 6456
    sput v0, Landroid/support/interpolator/R$dimen;->opcardview_margin:I

    .line 6457
    sput v0, Landroid/support/interpolator/R$dimen;->opgridview_margin:I

    .line 6458
    sput v0, Landroid/support/interpolator/R$dimen;->picker_bottom_margin:I

    .line 6462
    sput v0, Landroid/support/interpolator/R$dimen;->picker_top_margin:I

    .line 6466
    sput v0, Landroid/support/interpolator/R$dimen;->preference_breadcrumbs_padding_end_material:I

    .line 6470
    sput v0, Landroid/support/interpolator/R$dimen;->preference_breadcrumbs_padding_start_material:I

    .line 6474
    sput v0, Landroid/support/interpolator/R$dimen;->preference_category_font_size:I

    .line 6475
    sput v0, Landroid/support/interpolator/R$dimen;->preference_divider_height:I

    .line 6476
    sput v0, Landroid/support/interpolator/R$dimen;->preference_fragment_padding_bottom:I

    .line 6480
    sput v0, Landroid/support/interpolator/R$dimen;->preference_fragment_padding_side:I

    .line 6481
    sput v0, Landroid/support/interpolator/R$dimen;->preference_fragment_padding_side_material:I

    .line 6482
    sput v0, Landroid/support/interpolator/R$dimen;->preference_icon_minWidth:I

    .line 6483
    sput v0, Landroid/support/interpolator/R$dimen;->preference_screen_header_padding_side_material:I

    .line 6487
    sput v0, Landroid/support/interpolator/R$dimen;->preference_screen_header_vertical_padding_material:I

    .line 6491
    sput v0, Landroid/support/interpolator/R$dimen;->preference_screen_side_margin_material:I

    .line 6495
    sput v0, Landroid/support/interpolator/R$dimen;->preference_screen_side_margin_negative_material:I

    .line 6496
    sput v0, Landroid/support/interpolator/R$dimen;->preference_widget_width:I

    .line 6497
    sput v0, Landroid/support/interpolator/R$dimen;->preferences_icon_top_padding:I

    .line 6501
    sput v0, Landroid/support/interpolator/R$dimen;->preferences_primary_text_top_padding:I

    .line 6502
    sput v0, Landroid/support/interpolator/R$dimen;->preferences_right_icon_left_padding:I

    .line 6503
    sput v0, Landroid/support/interpolator/R$dimen;->preferences_secondary_text_bottom_padding:I

    .line 6504
    sput v0, Landroid/support/interpolator/R$dimen;->progress_bar_height_material:I

    .line 6508
    sput v0, Landroid/support/interpolator/R$dimen;->ripple_radius:I

    .line 6509
    sput v0, Landroid/support/interpolator/R$dimen;->searchResultListItemHeight:I

    .line 6510
    sput v0, Landroid/support/interpolator/R$dimen;->search_view_preferred_height:I

    .line 6511
    sput v0, Landroid/support/interpolator/R$dimen;->search_view_preferred_width:I

    .line 6512
    sput v0, Landroid/support/interpolator/R$dimen;->search_view_text_min_width:I

    .line 6516
    sput v0, Landroid/support/interpolator/R$dimen;->seekbar_thumb_optical_inset:I

    .line 6517
    sput v0, Landroid/support/interpolator/R$dimen;->seekbar_thumb_optical_inset_disabled:I

    .line 6518
    sput v0, Landroid/support/interpolator/R$dimen;->seekbar_track_background_height_material:I

    .line 6519
    sput v0, Landroid/support/interpolator/R$dimen;->seekbar_track_progress_height_material:I

    .line 6523
    sput v0, Landroid/support/interpolator/R$dimen;->select_dialog_padding_start_material:I

    .line 6527
    sput v0, Landroid/support/interpolator/R$dimen;->spinner_material_padding_left:I

    .line 6528
    sput v0, Landroid/support/interpolator/R$dimen;->spinner_material_padding_right:I

    .line 6529
    sput v0, Landroid/support/interpolator/R$dimen;->subtitle_corner_radius:I

    .line 6530
    sput v0, Landroid/support/interpolator/R$dimen;->subtitle_outline_width:I

    .line 6531
    sput v0, Landroid/support/interpolator/R$dimen;->subtitle_shadow_offset:I

    .line 6532
    sput v0, Landroid/support/interpolator/R$dimen;->subtitle_shadow_radius:I

    .line 6533
    sput v0, Landroid/support/interpolator/R$dimen;->switch_ripple_radius:I

    .line 6534
    sput v0, Landroid/support/interpolator/R$dimen;->tab_layout_default_height_material:I

    .line 6535
    sput v0, Landroid/support/interpolator/R$dimen;->tab_selected:I

    .line 6536
    sput v0, Landroid/support/interpolator/R$dimen;->tab_unselected:I

    .line 6537
    sput v0, Landroid/support/interpolator/R$dimen;->text_primary:I

    .line 6538
    sput v0, Landroid/support/interpolator/R$dimen;->text_secondary:I

    .line 6539
    sput v0, Landroid/support/interpolator/R$dimen;->text_selection_highlight_alpha_material:I

    .line 6540
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_body_1_material:I

    .line 6541
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_body_2_material:I

    .line 6542
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_button_material:I

    .line 6543
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_caption_material:I

    .line 6544
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_display_1_material:I

    .line 6545
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_display_2_material:I

    .line 6546
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_display_3_material:I

    .line 6547
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_display_4_material:I

    .line 6548
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_headline_material:I

    .line 6549
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_large_material:I

    .line 6550
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_medium_material:I

    .line 6551
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_menu_material:I

    .line 6552
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_small_material:I

    .line 6553
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_subhead_material:I

    .line 6554
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_subtitle_material_toolbar:I

    .line 6555
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_title_material:I

    .line 6556
    sput v0, Landroid/support/interpolator/R$dimen;->text_size_title_material_toolbar:I

    .line 6557
    sput v0, Landroid/support/interpolator/R$dimen;->text_subhead:I

    .line 6558
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_am_top_padding:I

    .line 6559
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_ampm_horizontal_padding:I

    .line 6560
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_ampm_label_size:I

    .line 6561
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_ampm_vertical_padding:I

    .line 6562
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_center_dot_radius:I

    .line 6563
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_edit_text_size:I

    .line 6564
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_extra_time_label_margin:I

    .line 6565
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_header_height:I

    .line 6566
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_input_field_width:I

    .line 6567
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_minimum_margin_sides:I

    .line 6568
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_minimum_margin_top_bottom:I

    .line 6569
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_pm_top_padding:I

    .line 6570
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_radial_picker_dimen:I

    .line 6571
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_radial_picker_left_margin:I

    .line 6572
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_radial_picker_right_margin:I

    .line 6573
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_radial_picker_top_margin:I

    .line 6574
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_selector_dot_radius:I

    .line 6578
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_selector_radius:I

    .line 6579
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_selector_stroke:I

    .line 6580
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_separator_padding:I

    .line 6581
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_text_inset_inner:I

    .line 6582
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_text_inset_normal:I

    .line 6583
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_text_size_inner:I

    .line 6584
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_text_size_normal:I

    .line 6589
    sput v0, Landroid/support/interpolator/R$dimen;->timepicker_time_label_size:I

    .line 6590
    sput v0, Landroid/support/interpolator/R$dimen;->toast_y_offset:I

    .line 6591
    sput v0, Landroid/support/interpolator/R$dimen;->toolbar_elevation:I

    .line 6592
    sput v0, Landroid/support/interpolator/R$dimen;->toolbar_icon_min_width:I

    .line 6593
    sput v0, Landroid/support/interpolator/R$dimen;->toolbar_text_size_subtitle:I

    .line 6594
    sput v0, Landroid/support/interpolator/R$dimen;->toolbar_text_size_title:I

    .line 6595
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_corner_radius:I

    .line 6596
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_horizontal_padding:I

    .line 6597
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_margin:I

    .line 6598
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_precise_anchor_extra_offset:I

    .line 6599
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_precise_anchor_threshold:I

    .line 6600
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_vertical_padding:I

    .line 6601
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_y_offset_non_touch:I

    .line 6602
    sput v0, Landroid/support/interpolator/R$dimen;->tooltip_y_offset_touch:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
