.class public final Lcom/google/zxing/aztec/encoder/Encoder;
.super Ljava/lang/Object;
.source "Encoder.java"


# static fields
.field public static final DEFAULT_AZTEC_LAYERS:I = 0x0

.field public static final DEFAULT_EC_PERCENT:I = 0x21

.field private static final MAX_NB_BITS:I = 0x20

.field private static final MAX_NB_BITS_COMPACT:I = 0x4

.field private static final WORD_SIZE:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    const/16 v0, 0x21

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/zxing/aztec/encoder/Encoder;->WORD_SIZE:[I

    return-void

    :array_0
    .array-data 4
        0x4
        0x6
        0x6
        0x8
        0x8
        0x8
        0x8
        0x8
        0x8
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xa
        0xc
        0xc
        0xc
        0xc
        0xc
        0xc
        0xc
        0xc
        0xc
        0xc
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method private static bitsToWords(Lcom/google/zxing/common/BitArray;II)[I
    .locals 7
    .param p0, "stuffedBits"    # Lcom/google/zxing/common/BitArray;
    .param p1, "wordSize"    # I
    .param p2, "totalWords"    # I

    .line 288
    new-array v0, p2, [I

    .line 291
    .local v0, "message":[I
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v2

    div-int/2addr v2, p1

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 292
    const/4 v3, 0x0

    .line 293
    .local v3, "value":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, p1, :cond_1

    .line 294
    mul-int v5, v1, p1

    add-int/2addr v5, v4

    invoke-virtual {p0, v5}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v5

    if-eqz v5, :cond_0

    sub-int v5, p1, v4

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    shl-int v5, v6, v5

    goto :goto_2

    :cond_0
    const/4 v5, 0x0

    :goto_2
    or-int/2addr v3, v5

    .line 293
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 296
    .end local v4    # "j":I
    :cond_1
    aput v3, v0, v1

    .line 291
    .end local v3    # "value":I
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 298
    :cond_2
    return-object v0
.end method

.method private static drawBullsEye(Lcom/google/zxing/common/BitMatrix;II)V
    .locals 3
    .param p0, "matrix"    # Lcom/google/zxing/common/BitMatrix;
    .param p1, "center"    # I
    .param p2, "size"    # I

    .line 204
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 205
    sub-int v1, p1, v0

    .local v1, "j":I
    :goto_1
    add-int v2, p1, v0

    if-gt v1, v2, :cond_0

    .line 206
    sub-int v2, p1, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 207
    add-int v2, p1, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 208
    sub-int v2, p1, v0

    invoke-virtual {p0, v2, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 209
    add-int v2, p1, v0

    invoke-virtual {p0, v2, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 204
    .end local v1    # "j":I
    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 212
    .end local v0    # "i":I
    :cond_1
    sub-int v0, p1, p2

    sub-int v1, p1, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 213
    sub-int v0, p1, p2

    add-int/lit8 v0, v0, 0x1

    sub-int v1, p1, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 214
    sub-int v0, p1, p2

    sub-int v1, p1, p2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 215
    add-int v0, p1, p2

    sub-int v1, p1, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 216
    add-int v0, p1, p2

    sub-int v1, p1, p2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 217
    add-int v0, p1, p2

    add-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 218
    return-void
.end method

.method private static drawModeMessage(Lcom/google/zxing/common/BitMatrix;ZILcom/google/zxing/common/BitArray;)V
    .locals 4
    .param p0, "matrix"    # Lcom/google/zxing/common/BitMatrix;
    .param p1, "compact"    # Z
    .param p2, "matrixSize"    # I
    .param p3, "modeMessage"    # Lcom/google/zxing/common/BitArray;

    .line 235
    div-int/lit8 v0, p2, 0x2

    .line 236
    .local v0, "center":I
    if-eqz p1, :cond_5

    .line 237
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_4

    .line 238
    add-int/lit8 v2, v0, -0x3

    add-int/2addr v2, v1

    .line 239
    .local v2, "offset":I
    invoke-virtual {p3, v1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 240
    add-int/lit8 v3, v0, -0x5

    invoke-virtual {p0, v2, v3}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 242
    :cond_0
    add-int/lit8 v3, v1, 0x7

    invoke-virtual {p3, v3}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 243
    add-int/lit8 v3, v0, 0x5

    invoke-virtual {p0, v3, v2}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 245
    :cond_1
    rsub-int/lit8 v3, v1, 0x14

    invoke-virtual {p3, v3}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 246
    add-int/lit8 v3, v0, 0x5

    invoke-virtual {p0, v2, v3}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 248
    :cond_2
    rsub-int/lit8 v3, v1, 0x1b

    invoke-virtual {p3, v3}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 249
    add-int/lit8 v3, v0, -0x5

    invoke-virtual {p0, v3, v2}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 237
    .end local v2    # "offset":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v1    # "i":I
    :cond_4
    goto :goto_2

    .line 253
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    const/16 v2, 0xa

    if-ge v1, v2, :cond_a

    .line 254
    add-int/lit8 v2, v0, -0x5

    add-int/2addr v2, v1

    div-int/lit8 v3, v1, 0x5

    add-int/2addr v2, v3

    .line 255
    .restart local v2    # "offset":I
    invoke-virtual {p3, v1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 256
    add-int/lit8 v3, v0, -0x7

    invoke-virtual {p0, v2, v3}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 258
    :cond_6
    add-int/lit8 v3, v1, 0xa

    invoke-virtual {p3, v3}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 259
    add-int/lit8 v3, v0, 0x7

    invoke-virtual {p0, v3, v2}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 261
    :cond_7
    rsub-int/lit8 v3, v1, 0x1d

    invoke-virtual {p3, v3}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 262
    add-int/lit8 v3, v0, 0x7

    invoke-virtual {p0, v2, v3}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 264
    :cond_8
    rsub-int/lit8 v3, v1, 0x27

    invoke-virtual {p3, v3}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 265
    add-int/lit8 v3, v0, -0x7

    invoke-virtual {p0, v3, v2}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 253
    .end local v2    # "offset":I
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 269
    .end local v1    # "i":I
    :cond_a
    :goto_2
    return-void
.end method

.method public static encode([B)Lcom/google/zxing/aztec/encoder/AztecCode;
    .locals 2
    .param p0, "data"    # [B

    .line 51
    const/16 v0, 0x21

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/zxing/aztec/encoder/Encoder;->encode([BII)Lcom/google/zxing/aztec/encoder/AztecCode;

    move-result-object v0

    return-object v0
.end method

.method public static encode([BII)Lcom/google/zxing/aztec/encoder/AztecCode;
    .locals 26
    .param p0, "data"    # [B
    .param p1, "minECCPercent"    # I
    .param p2, "userSpecifiedLayers"    # I

    .line 65
    new-instance v0, Lcom/google/zxing/aztec/encoder/HighLevelEncoder;

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/zxing/aztec/encoder/HighLevelEncoder;-><init>([B)V

    invoke-virtual {v0}, Lcom/google/zxing/aztec/encoder/HighLevelEncoder;->encode()Lcom/google/zxing/common/BitArray;

    move-result-object v0

    .line 68
    .local v0, "bits":Lcom/google/zxing/common/BitArray;
    invoke-virtual {v0}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v2

    mul-int v2, v2, p1

    div-int/lit8 v2, v2, 0x64

    add-int/lit8 v2, v2, 0xb

    .line 69
    .local v2, "eccBits":I
    invoke-virtual {v0}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v3

    add-int/2addr v3, v2

    .line 75
    .local v3, "totalSizeBits":I
    const/16 v4, 0x20

    const/4 v5, 0x0

    const/4 v7, 0x1

    if-eqz p2, :cond_6

    .line 76
    if-gez p2, :cond_0

    move v8, v7

    goto :goto_0

    :cond_0
    move v8, v5

    .line 77
    .local v8, "compact":Z
    :goto_0
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    move-result v9

    .line 78
    .local v9, "layers":I
    if-eqz v8, :cond_1

    const/4 v4, 0x4

    :cond_1
    if-gt v9, v4, :cond_5

    .line 82
    invoke-static {v9, v8}, Lcom/google/zxing/aztec/encoder/Encoder;->totalBitsInLayer(IZ)I

    move-result v4

    .line 83
    .local v4, "totalBitsInLayer":I
    sget-object v5, Lcom/google/zxing/aztec/encoder/Encoder;->WORD_SIZE:[I

    aget v5, v5, v9

    .line 84
    .local v5, "wordSize":I
    rem-int v10, v4, v5

    sub-int v10, v4, v10

    .line 85
    .local v10, "usableBitsInLayers":I
    invoke-static {v0, v5}, Lcom/google/zxing/aztec/encoder/Encoder;->stuffBits(Lcom/google/zxing/common/BitArray;I)Lcom/google/zxing/common/BitArray;

    move-result-object v11

    .line 86
    .local v11, "stuffedBits":Lcom/google/zxing/common/BitArray;
    invoke-virtual {v11}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v12

    add-int/2addr v12, v2

    const-string v13, "Data to large for user specified layer"

    if-gt v12, v10, :cond_4

    .line 89
    if-eqz v8, :cond_3

    invoke-virtual {v11}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v12

    mul-int/lit8 v14, v5, 0x40

    if-gt v12, v14, :cond_2

    goto :goto_1

    .line 91
    :cond_2
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 93
    .end local v10    # "usableBitsInLayers":I
    :cond_3
    :goto_1
    move v13, v4

    move v12, v9

    move-object v9, v11

    goto/16 :goto_5

    .line 87
    .restart local v10    # "usableBitsInLayers":I
    :cond_4
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 79
    .end local v4    # "totalBitsInLayer":I
    .end local v5    # "wordSize":I
    .end local v10    # "usableBitsInLayers":I
    .end local v11    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    :cond_5
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    const-string v5, "Illegal value %s for layers"

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 94
    .end local v8    # "compact":Z
    .end local v9    # "layers":I
    :cond_6
    const/4 v8, 0x0

    .line 95
    .local v8, "wordSize":I
    const/4 v9, 0x0

    .line 99
    .local v9, "stuffedBits":Lcom/google/zxing/common/BitArray;
    const/4 v10, 0x0

    .line 100
    .local v10, "i":I
    :goto_2
    if-gt v10, v4, :cond_1c

    .line 103
    const/4 v11, 0x3

    if-gt v10, v11, :cond_7

    move v11, v7

    goto :goto_3

    :cond_7
    move v11, v5

    .line 104
    .local v11, "compact":Z
    :goto_3
    if-eqz v11, :cond_8

    add-int/lit8 v12, v10, 0x1

    goto :goto_4

    :cond_8
    move v12, v10

    .line 105
    .local v12, "layers":I
    :goto_4
    invoke-static {v12, v11}, Lcom/google/zxing/aztec/encoder/Encoder;->totalBitsInLayer(IZ)I

    move-result v13

    .line 106
    .local v13, "totalBitsInLayer":I
    if-le v3, v13, :cond_9

    .line 107
    move-object/from16 v21, v0

    move/from16 v20, v2

    move/from16 v24, v3

    const/16 v17, 0x4

    goto/16 :goto_12

    .line 111
    :cond_9
    sget-object v14, Lcom/google/zxing/aztec/encoder/Encoder;->WORD_SIZE:[I

    aget v15, v14, v12

    if-eq v8, v15, :cond_a

    .line 112
    aget v8, v14, v12

    .line 113
    invoke-static {v0, v8}, Lcom/google/zxing/aztec/encoder/Encoder;->stuffBits(Lcom/google/zxing/common/BitArray;I)Lcom/google/zxing/common/BitArray;

    move-result-object v9

    .line 115
    :cond_a
    rem-int v14, v13, v8

    sub-int v14, v13, v14

    .line 116
    .local v14, "usableBitsInLayers":I
    if-eqz v11, :cond_b

    invoke-virtual {v9}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v15

    mul-int/lit8 v4, v8, 0x40

    if-le v15, v4, :cond_b

    .line 118
    move-object/from16 v21, v0

    move/from16 v20, v2

    move/from16 v24, v3

    const/16 v17, 0x4

    goto/16 :goto_12

    .line 120
    :cond_b
    invoke-virtual {v9}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v4

    add-int/2addr v4, v2

    if-gt v4, v14, :cond_1b

    .line 121
    move v5, v8

    move v8, v11

    .line 125
    .end local v10    # "i":I
    .end local v11    # "compact":Z
    .end local v14    # "usableBitsInLayers":I
    .restart local v5    # "wordSize":I
    .local v8, "compact":Z
    :goto_5
    invoke-static {v9, v13, v5}, Lcom/google/zxing/aztec/encoder/Encoder;->generateCheckWords(Lcom/google/zxing/common/BitArray;II)Lcom/google/zxing/common/BitArray;

    move-result-object v4

    .line 128
    .local v4, "messageBits":Lcom/google/zxing/common/BitArray;
    invoke-virtual {v9}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v10

    div-int/2addr v10, v5

    .line 129
    .local v10, "messageSizeInWords":I
    invoke-static {v8, v12, v10}, Lcom/google/zxing/aztec/encoder/Encoder;->generateModeMessage(ZII)Lcom/google/zxing/common/BitArray;

    move-result-object v11

    .line 132
    .local v11, "modeMessage":Lcom/google/zxing/common/BitArray;
    if-eqz v8, :cond_c

    mul-int/lit8 v14, v12, 0x4

    add-int/lit8 v14, v14, 0xb

    goto :goto_6

    :cond_c
    mul-int/lit8 v14, v12, 0x4

    add-int/lit8 v14, v14, 0xe

    .line 133
    .local v14, "baseMatrixSize":I
    :goto_6
    new-array v15, v14, [I

    .line 135
    .local v15, "alignmentMap":[I
    if-eqz v8, :cond_e

    .line 137
    move/from16 v16, v14

    .line 138
    .local v16, "matrixSize":I
    const/16 v18, 0x0

    move/from16 v6, v18

    .local v6, "i":I
    :goto_7
    array-length v7, v15

    if-ge v6, v7, :cond_d

    .line 139
    aput v6, v15, v6

    .line 138
    add-int/lit8 v6, v6, 0x1

    const/4 v7, 0x1

    goto :goto_7

    :cond_d
    move-object/from16 v21, v0

    move/from16 v0, v16

    .end local v6    # "i":I
    goto :goto_9

    .line 142
    .end local v16    # "matrixSize":I
    :cond_e
    add-int/lit8 v6, v14, 0x1

    div-int/lit8 v7, v14, 0x2

    const/16 v16, 0x1

    add-int/lit8 v7, v7, -0x1

    div-int/lit8 v7, v7, 0xf

    const/16 v16, 0x2

    mul-int/lit8 v7, v7, 0x2

    add-int v16, v6, v7

    .line 143
    .restart local v16    # "matrixSize":I
    div-int/lit8 v6, v14, 0x2

    .line 144
    .local v6, "origCenter":I
    div-int/lit8 v7, v16, 0x2

    .line 145
    .local v7, "center":I
    const/16 v20, 0x0

    move-object/from16 v21, v0

    move/from16 v0, v20

    .local v0, "i":I
    .local v21, "bits":Lcom/google/zxing/common/BitArray;
    :goto_8
    if-ge v0, v6, :cond_f

    .line 146
    div-int/lit8 v20, v0, 0xf

    add-int v20, v0, v20

    .line 147
    .local v20, "newOffset":I
    sub-int v22, v6, v0

    const/16 v18, 0x1

    add-int/lit8 v22, v22, -0x1

    sub-int v23, v7, v20

    add-int/lit8 v23, v23, -0x1

    aput v23, v15, v22

    .line 148
    add-int v22, v6, v0

    add-int v23, v7, v20

    add-int/lit8 v23, v23, 0x1

    aput v23, v15, v22

    .line 145
    .end local v20    # "newOffset":I
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_f
    move/from16 v0, v16

    .line 151
    .end local v6    # "origCenter":I
    .end local v7    # "center":I
    .end local v16    # "matrixSize":I
    .local v0, "matrixSize":I
    :goto_9
    new-instance v6, Lcom/google/zxing/common/BitMatrix;

    invoke-direct {v6, v0}, Lcom/google/zxing/common/BitMatrix;-><init>(I)V

    .line 154
    .local v6, "matrix":Lcom/google/zxing/common/BitMatrix;
    const/4 v7, 0x0

    .local v7, "i":I
    const/16 v16, 0x0

    .local v16, "rowOffset":I
    :goto_a
    if-ge v7, v12, :cond_17

    .line 155
    if-eqz v8, :cond_10

    sub-int v20, v12, v7

    const/16 v17, 0x4

    mul-int/lit8 v20, v20, 0x4

    add-int/lit8 v20, v20, 0x9

    goto :goto_b

    :cond_10
    const/16 v17, 0x4

    sub-int v20, v12, v7

    mul-int/lit8 v20, v20, 0x4

    add-int/lit8 v20, v20, 0xc

    :goto_b
    move/from16 v22, v20

    .line 156
    .local v22, "rowSize":I
    const/16 v20, 0x0

    move/from16 v1, v20

    .local v1, "j":I
    :goto_c
    move/from16 v20, v2

    move/from16 v2, v22

    .end local v22    # "rowSize":I
    .local v2, "rowSize":I
    .local v20, "eccBits":I
    if-ge v1, v2, :cond_16

    .line 157
    mul-int/lit8 v22, v1, 0x2

    .line 158
    .local v22, "columnOffset":I
    const/16 v23, 0x0

    move/from16 v24, v3

    move/from16 v3, v23

    .local v3, "k":I
    .local v24, "totalSizeBits":I
    :goto_d
    move/from16 v23, v5

    const/4 v5, 0x2

    .end local v5    # "wordSize":I
    .local v23, "wordSize":I
    if-ge v3, v5, :cond_15

    .line 159
    add-int v19, v16, v22

    add-int v5, v19, v3

    invoke-virtual {v4, v5}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 160
    mul-int/lit8 v5, v7, 0x2

    add-int/2addr v5, v3

    aget v5, v15, v5

    mul-int/lit8 v19, v7, 0x2

    add-int v19, v19, v1

    move-object/from16 v25, v9

    .end local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .local v25, "stuffedBits":Lcom/google/zxing/common/BitArray;
    aget v9, v15, v19

    invoke-virtual {v6, v5, v9}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    goto :goto_e

    .line 159
    .end local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .restart local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    :cond_11
    move-object/from16 v25, v9

    .line 162
    .end local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .restart local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    :goto_e
    mul-int/lit8 v5, v2, 0x2

    add-int v5, v16, v5

    add-int v5, v5, v22

    add-int/2addr v5, v3

    invoke-virtual {v4, v5}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 163
    mul-int/lit8 v5, v7, 0x2

    add-int/2addr v5, v1

    aget v5, v15, v5

    add-int/lit8 v9, v14, -0x1

    mul-int/lit8 v19, v7, 0x2

    sub-int v9, v9, v19

    sub-int/2addr v9, v3

    aget v9, v15, v9

    invoke-virtual {v6, v5, v9}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 165
    :cond_12
    mul-int/lit8 v5, v2, 0x4

    add-int v5, v16, v5

    add-int v5, v5, v22

    add-int/2addr v5, v3

    invoke-virtual {v4, v5}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 166
    add-int/lit8 v5, v14, -0x1

    mul-int/lit8 v9, v7, 0x2

    sub-int/2addr v5, v9

    sub-int/2addr v5, v3

    aget v5, v15, v5

    add-int/lit8 v9, v14, -0x1

    mul-int/lit8 v19, v7, 0x2

    sub-int v9, v9, v19

    sub-int/2addr v9, v1

    aget v9, v15, v9

    invoke-virtual {v6, v5, v9}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 168
    :cond_13
    mul-int/lit8 v5, v2, 0x6

    add-int v5, v16, v5

    add-int v5, v5, v22

    add-int/2addr v5, v3

    invoke-virtual {v4, v5}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 169
    add-int/lit8 v5, v14, -0x1

    mul-int/lit8 v9, v7, 0x2

    sub-int/2addr v5, v9

    sub-int/2addr v5, v1

    aget v5, v15, v5

    mul-int/lit8 v9, v7, 0x2

    add-int/2addr v9, v3

    aget v9, v15, v9

    invoke-virtual {v6, v5, v9}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 158
    :cond_14
    add-int/lit8 v3, v3, 0x1

    move/from16 v5, v23

    move-object/from16 v9, v25

    goto/16 :goto_d

    .end local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .restart local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    :cond_15
    move-object/from16 v25, v9

    .line 156
    .end local v3    # "k":I
    .end local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .end local v22    # "columnOffset":I
    .restart local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    add-int/lit8 v1, v1, 0x1

    move/from16 v22, v2

    move/from16 v2, v20

    move/from16 v5, v23

    move/from16 v3, v24

    goto/16 :goto_c

    .end local v23    # "wordSize":I
    .end local v24    # "totalSizeBits":I
    .end local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .local v3, "totalSizeBits":I
    .restart local v5    # "wordSize":I
    .restart local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    :cond_16
    move/from16 v24, v3

    move/from16 v23, v5

    move-object/from16 v25, v9

    .line 173
    .end local v1    # "j":I
    .end local v3    # "totalSizeBits":I
    .end local v5    # "wordSize":I
    .end local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .restart local v23    # "wordSize":I
    .restart local v24    # "totalSizeBits":I
    .restart local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    mul-int/lit8 v22, v2, 0x8

    add-int v16, v16, v22

    .line 154
    .end local v2    # "rowSize":I
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v1, p0

    move/from16 v2, v20

    goto/16 :goto_a

    .end local v20    # "eccBits":I
    .end local v23    # "wordSize":I
    .end local v24    # "totalSizeBits":I
    .end local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .local v2, "eccBits":I
    .restart local v3    # "totalSizeBits":I
    .restart local v5    # "wordSize":I
    .restart local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    :cond_17
    move/from16 v20, v2

    move/from16 v24, v3

    move/from16 v23, v5

    move-object/from16 v25, v9

    .line 177
    .end local v2    # "eccBits":I
    .end local v3    # "totalSizeBits":I
    .end local v5    # "wordSize":I
    .end local v7    # "i":I
    .end local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .end local v16    # "rowOffset":I
    .restart local v20    # "eccBits":I
    .restart local v23    # "wordSize":I
    .restart local v24    # "totalSizeBits":I
    .restart local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    invoke-static {v6, v8, v0, v11}, Lcom/google/zxing/aztec/encoder/Encoder;->drawModeMessage(Lcom/google/zxing/common/BitMatrix;ZILcom/google/zxing/common/BitArray;)V

    .line 180
    if-eqz v8, :cond_18

    .line 181
    div-int/lit8 v1, v0, 0x2

    const/4 v2, 0x5

    invoke-static {v6, v1, v2}, Lcom/google/zxing/aztec/encoder/Encoder;->drawBullsEye(Lcom/google/zxing/common/BitMatrix;II)V

    goto :goto_11

    .line 183
    :cond_18
    div-int/lit8 v1, v0, 0x2

    const/4 v2, 0x7

    invoke-static {v6, v1, v2}, Lcom/google/zxing/aztec/encoder/Encoder;->drawBullsEye(Lcom/google/zxing/common/BitMatrix;II)V

    .line 184
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_f
    div-int/lit8 v3, v14, 0x2

    const/4 v7, 0x1

    sub-int/2addr v3, v7

    if-ge v1, v3, :cond_1a

    .line 185
    div-int/lit8 v3, v0, 0x2

    and-int/2addr v3, v7

    .local v3, "k":I
    :goto_10
    if-ge v3, v0, :cond_19

    .line 186
    div-int/lit8 v5, v0, 0x2

    sub-int/2addr v5, v2

    invoke-virtual {v6, v5, v3}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 187
    div-int/lit8 v5, v0, 0x2

    add-int/2addr v5, v2

    invoke-virtual {v6, v5, v3}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 188
    div-int/lit8 v5, v0, 0x2

    sub-int/2addr v5, v2

    invoke-virtual {v6, v3, v5}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 189
    div-int/lit8 v5, v0, 0x2

    add-int/2addr v5, v2

    invoke-virtual {v6, v3, v5}, Lcom/google/zxing/common/BitMatrix;->set(II)V

    .line 185
    add-int/lit8 v3, v3, 0x2

    goto :goto_10

    .line 184
    .end local v3    # "k":I
    :cond_19
    add-int/lit8 v1, v1, 0xf

    add-int/lit8 v2, v2, 0x10

    goto :goto_f

    .line 194
    .end local v1    # "i":I
    .end local v2    # "j":I
    :cond_1a
    :goto_11
    new-instance v1, Lcom/google/zxing/aztec/encoder/AztecCode;

    invoke-direct {v1}, Lcom/google/zxing/aztec/encoder/AztecCode;-><init>()V

    .line 195
    .local v1, "aztec":Lcom/google/zxing/aztec/encoder/AztecCode;
    invoke-virtual {v1, v8}, Lcom/google/zxing/aztec/encoder/AztecCode;->setCompact(Z)V

    .line 196
    invoke-virtual {v1, v0}, Lcom/google/zxing/aztec/encoder/AztecCode;->setSize(I)V

    .line 197
    invoke-virtual {v1, v12}, Lcom/google/zxing/aztec/encoder/AztecCode;->setLayers(I)V

    .line 198
    invoke-virtual {v1, v10}, Lcom/google/zxing/aztec/encoder/AztecCode;->setCodeWords(I)V

    .line 199
    invoke-virtual {v1, v6}, Lcom/google/zxing/aztec/encoder/AztecCode;->setMatrix(Lcom/google/zxing/common/BitMatrix;)V

    .line 200
    return-object v1

    .line 120
    .end local v1    # "aztec":Lcom/google/zxing/aztec/encoder/AztecCode;
    .end local v4    # "messageBits":Lcom/google/zxing/common/BitArray;
    .end local v6    # "matrix":Lcom/google/zxing/common/BitMatrix;
    .end local v15    # "alignmentMap":[I
    .end local v20    # "eccBits":I
    .end local v21    # "bits":Lcom/google/zxing/common/BitArray;
    .end local v23    # "wordSize":I
    .end local v24    # "totalSizeBits":I
    .end local v25    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .local v0, "bits":Lcom/google/zxing/common/BitArray;
    .local v2, "eccBits":I
    .local v3, "totalSizeBits":I
    .local v8, "wordSize":I
    .restart local v9    # "stuffedBits":Lcom/google/zxing/common/BitArray;
    .local v10, "i":I
    .local v11, "compact":Z
    .local v14, "usableBitsInLayers":I
    :cond_1b
    move-object/from16 v21, v0

    move/from16 v20, v2

    move/from16 v24, v3

    const/16 v17, 0x4

    .line 99
    .end local v0    # "bits":Lcom/google/zxing/common/BitArray;
    .end local v2    # "eccBits":I
    .end local v3    # "totalSizeBits":I
    .end local v14    # "usableBitsInLayers":I
    .restart local v20    # "eccBits":I
    .restart local v21    # "bits":Lcom/google/zxing/common/BitArray;
    .restart local v24    # "totalSizeBits":I
    :goto_12
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v1, p0

    move/from16 v2, v20

    move-object/from16 v0, v21

    move/from16 v3, v24

    const/16 v4, 0x20

    goto/16 :goto_2

    .line 101
    .end local v11    # "compact":Z
    .end local v12    # "layers":I
    .end local v13    # "totalBitsInLayer":I
    .end local v20    # "eccBits":I
    .end local v21    # "bits":Lcom/google/zxing/common/BitArray;
    .end local v24    # "totalSizeBits":I
    .restart local v0    # "bits":Lcom/google/zxing/common/BitArray;
    .restart local v2    # "eccBits":I
    .restart local v3    # "totalSizeBits":I
    :cond_1c
    move-object/from16 v21, v0

    .end local v0    # "bits":Lcom/google/zxing/common/BitArray;
    .restart local v21    # "bits":Lcom/google/zxing/common/BitArray;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Data too large for an Aztec code"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static generateCheckWords(Lcom/google/zxing/common/BitArray;II)Lcom/google/zxing/common/BitArray;
    .locals 10
    .param p0, "bitArray"    # Lcom/google/zxing/common/BitArray;
    .param p1, "totalBits"    # I
    .param p2, "wordSize"    # I

    .line 273
    invoke-virtual {p0}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v0

    div-int/2addr v0, p2

    .line 274
    .local v0, "messageSizeInWords":I
    new-instance v1, Lcom/google/zxing/common/reedsolomon/ReedSolomonEncoder;

    invoke-static {p2}, Lcom/google/zxing/aztec/encoder/Encoder;->getGF(I)Lcom/google/zxing/common/reedsolomon/GenericGF;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/zxing/common/reedsolomon/ReedSolomonEncoder;-><init>(Lcom/google/zxing/common/reedsolomon/GenericGF;)V

    .line 275
    .local v1, "rs":Lcom/google/zxing/common/reedsolomon/ReedSolomonEncoder;
    div-int v2, p1, p2

    .line 276
    .local v2, "totalWords":I
    invoke-static {p0, p2, v2}, Lcom/google/zxing/aztec/encoder/Encoder;->bitsToWords(Lcom/google/zxing/common/BitArray;II)[I

    move-result-object v3

    .line 277
    .local v3, "messageWords":[I
    sub-int v4, v2, v0

    invoke-virtual {v1, v3, v4}, Lcom/google/zxing/common/reedsolomon/ReedSolomonEncoder;->encode([II)V

    .line 278
    rem-int v4, p1, p2

    .line 279
    .local v4, "startPad":I
    new-instance v5, Lcom/google/zxing/common/BitArray;

    invoke-direct {v5}, Lcom/google/zxing/common/BitArray;-><init>()V

    .line 280
    .local v5, "messageBits":Lcom/google/zxing/common/BitArray;
    const/4 v6, 0x0

    invoke-virtual {v5, v6, v4}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 281
    move-object v6, v3

    .local v6, "arr$":[I
    array-length v7, v6

    .local v7, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v7, :cond_0

    aget v9, v6, v8

    .line 282
    .local v9, "messageWord":I
    invoke-virtual {v5, v9, p2}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 281
    .end local v9    # "messageWord":I
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 284
    .end local v6    # "arr$":[I
    .end local v7    # "len$":I
    .end local v8    # "i$":I
    :cond_0
    return-object v5
.end method

.method static generateModeMessage(ZII)Lcom/google/zxing/common/BitArray;
    .locals 4
    .param p0, "compact"    # Z
    .param p1, "layers"    # I
    .param p2, "messageSizeInWords"    # I

    .line 221
    new-instance v0, Lcom/google/zxing/common/BitArray;

    invoke-direct {v0}, Lcom/google/zxing/common/BitArray;-><init>()V

    .line 222
    .local v0, "modeMessage":Lcom/google/zxing/common/BitArray;
    const/4 v1, 0x4

    if-eqz p0, :cond_0

    .line 223
    add-int/lit8 v2, p1, -0x1

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 224
    add-int/lit8 v2, p2, -0x1

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 225
    const/16 v2, 0x1c

    invoke-static {v0, v2, v1}, Lcom/google/zxing/aztec/encoder/Encoder;->generateCheckWords(Lcom/google/zxing/common/BitArray;II)Lcom/google/zxing/common/BitArray;

    move-result-object v0

    goto :goto_0

    .line 227
    :cond_0
    add-int/lit8 v2, p1, -0x1

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 228
    add-int/lit8 v2, p2, -0x1

    const/16 v3, 0xb

    invoke-virtual {v0, v2, v3}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 229
    const/16 v2, 0x28

    invoke-static {v0, v2, v1}, Lcom/google/zxing/aztec/encoder/Encoder;->generateCheckWords(Lcom/google/zxing/common/BitArray;II)Lcom/google/zxing/common/BitArray;

    move-result-object v0

    .line 231
    :goto_0
    return-object v0
.end method

.method private static getGF(I)Lcom/google/zxing/common/reedsolomon/GenericGF;
    .locals 1
    .param p0, "wordSize"    # I

    .line 302
    const/4 v0, 0x4

    if-eq p0, v0, :cond_4

    const/4 v0, 0x6

    if-eq p0, v0, :cond_3

    const/16 v0, 0x8

    if-eq p0, v0, :cond_2

    const/16 v0, 0xa

    if-eq p0, v0, :cond_1

    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    .line 314
    const/4 v0, 0x0

    return-object v0

    .line 312
    :cond_0
    sget-object v0, Lcom/google/zxing/common/reedsolomon/GenericGF;->AZTEC_DATA_12:Lcom/google/zxing/common/reedsolomon/GenericGF;

    return-object v0

    .line 310
    :cond_1
    sget-object v0, Lcom/google/zxing/common/reedsolomon/GenericGF;->AZTEC_DATA_10:Lcom/google/zxing/common/reedsolomon/GenericGF;

    return-object v0

    .line 308
    :cond_2
    sget-object v0, Lcom/google/zxing/common/reedsolomon/GenericGF;->AZTEC_DATA_8:Lcom/google/zxing/common/reedsolomon/GenericGF;

    return-object v0

    .line 306
    :cond_3
    sget-object v0, Lcom/google/zxing/common/reedsolomon/GenericGF;->AZTEC_DATA_6:Lcom/google/zxing/common/reedsolomon/GenericGF;

    return-object v0

    .line 304
    :cond_4
    sget-object v0, Lcom/google/zxing/common/reedsolomon/GenericGF;->AZTEC_PARAM:Lcom/google/zxing/common/reedsolomon/GenericGF;

    return-object v0
.end method

.method static stuffBits(Lcom/google/zxing/common/BitArray;I)Lcom/google/zxing/common/BitArray;
    .locals 8
    .param p0, "bits"    # Lcom/google/zxing/common/BitArray;
    .param p1, "wordSize"    # I

    .line 319
    new-instance v0, Lcom/google/zxing/common/BitArray;

    invoke-direct {v0}, Lcom/google/zxing/common/BitArray;-><init>()V

    .line 321
    .local v0, "out":Lcom/google/zxing/common/BitArray;
    invoke-virtual {p0}, Lcom/google/zxing/common/BitArray;->getSize()I

    move-result v1

    .line 322
    .local v1, "n":I
    const/4 v2, 0x1

    shl-int v3, v2, p1

    add-int/lit8 v3, v3, -0x2

    .line 323
    .local v3, "mask":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_5

    .line 324
    const/4 v5, 0x0

    .line 325
    .local v5, "word":I
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    if-ge v6, p1, :cond_2

    .line 326
    add-int v7, v4, v6

    if-ge v7, v1, :cond_0

    add-int v7, v4, v6

    invoke-virtual {p0, v7}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 327
    :cond_0
    add-int/lit8 v7, p1, -0x1

    sub-int/2addr v7, v6

    shl-int v7, v2, v7

    or-int/2addr v5, v7

    .line 325
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 330
    .end local v6    # "j":I
    :cond_2
    and-int v6, v5, v3

    if-ne v6, v3, :cond_3

    .line 331
    and-int v6, v5, v3

    invoke-virtual {v0, v6, p1}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 332
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 333
    :cond_3
    and-int v6, v5, v3

    if-nez v6, :cond_4

    .line 334
    or-int/lit8 v6, v5, 0x1

    invoke-virtual {v0, v6, p1}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 335
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 337
    :cond_4
    invoke-virtual {v0, v5, p1}, Lcom/google/zxing/common/BitArray;->appendBits(II)V

    .line 323
    .end local v5    # "word":I
    :goto_2
    add-int/2addr v4, p1

    goto :goto_0

    .line 340
    .end local v4    # "i":I
    :cond_5
    return-object v0
.end method

.method private static totalBitsInLayer(IZ)I
    .locals 2
    .param p0, "layers"    # I
    .param p1, "compact"    # Z

    .line 344
    if-eqz p1, :cond_0

    const/16 v0, 0x58

    goto :goto_0

    :cond_0
    const/16 v0, 0x70

    :goto_0
    mul-int/lit8 v1, p0, 0x10

    add-int/2addr v0, v1

    mul-int/2addr v0, p0

    return v0
.end method
