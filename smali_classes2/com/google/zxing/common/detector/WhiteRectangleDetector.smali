.class public final Lcom/google/zxing/common/detector/WhiteRectangleDetector;
.super Ljava/lang/Object;
.source "WhiteRectangleDetector.java"


# static fields
.field private static final CORR:I = 0x1

.field private static final INIT_SIZE:I = 0x1e


# instance fields
.field private final downInit:I

.field private final height:I

.field private final image:Lcom/google/zxing/common/BitMatrix;

.field private final leftInit:I

.field private final rightInit:I

.field private final upInit:I

.field private final width:I


# direct methods
.method public constructor <init>(Lcom/google/zxing/common/BitMatrix;)V
    .locals 3
    .param p1, "image"    # Lcom/google/zxing/common/BitMatrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->image:Lcom/google/zxing/common/BitMatrix;

    .line 51
    invoke-virtual {p1}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->height:I

    .line 52
    invoke-virtual {p1}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->width:I

    .line 53
    iget v0, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->width:I

    add-int/lit8 v1, v0, -0x1e

    shr-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->leftInit:I

    .line 54
    add-int/lit8 v1, v0, 0x1e

    shr-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->rightInit:I

    .line 55
    iget v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->height:I

    add-int/lit8 v2, v1, -0x1e

    shr-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->upInit:I

    .line 56
    add-int/lit8 v2, v1, 0x1e

    shr-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->downInit:I

    .line 57
    iget v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->upInit:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->leftInit:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->downInit:I

    if-ge v2, v1, :cond_0

    iget v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->rightInit:I

    if-ge v1, v0, :cond_0

    .line 60
    return-void

    .line 58
    :cond_0
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method

.method public constructor <init>(Lcom/google/zxing/common/BitMatrix;III)V
    .locals 3
    .param p1, "image"    # Lcom/google/zxing/common/BitMatrix;
    .param p2, "initSize"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->image:Lcom/google/zxing/common/BitMatrix;

    .line 67
    invoke-virtual {p1}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->height:I

    .line 68
    invoke-virtual {p1}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->width:I

    .line 69
    shr-int/lit8 v0, p2, 0x1

    .line 70
    .local v0, "halfsize":I
    sub-int v1, p3, v0

    iput v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->leftInit:I

    .line 71
    add-int v1, p3, v0

    iput v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->rightInit:I

    .line 72
    sub-int v1, p4, v0

    iput v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->upInit:I

    .line 73
    add-int v1, p4, v0

    iput v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->downInit:I

    .line 74
    iget v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->upInit:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->leftInit:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->downInit:I

    iget v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->height:I

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->rightInit:I

    iget v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->width:I

    if-ge v1, v2, :cond_0

    .line 77
    return-void

    .line 75
    :cond_0
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1
.end method

.method private centerEdges(Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;)[Lcom/google/zxing/ResultPoint;
    .locals 16
    .param p1, "y"    # Lcom/google/zxing/ResultPoint;
    .param p2, "z"    # Lcom/google/zxing/ResultPoint;
    .param p3, "x"    # Lcom/google/zxing/ResultPoint;
    .param p4, "t"    # Lcom/google/zxing/ResultPoint;

    .line 281
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v0

    .line 282
    .local v0, "yi":F
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v1

    .line 283
    .local v1, "yj":F
    invoke-virtual/range {p2 .. p2}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v2

    .line 284
    .local v2, "zi":F
    invoke-virtual/range {p2 .. p2}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v3

    .line 285
    .local v3, "zj":F
    invoke-virtual/range {p3 .. p3}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v4

    .line 286
    .local v4, "xi":F
    invoke-virtual/range {p3 .. p3}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v5

    .line 287
    .local v5, "xj":F
    invoke-virtual/range {p4 .. p4}, Lcom/google/zxing/ResultPoint;->getX()F

    move-result v6

    .line 288
    .local v6, "ti":F
    invoke-virtual/range {p4 .. p4}, Lcom/google/zxing/ResultPoint;->getY()F

    move-result v7

    .line 290
    .local v7, "tj":F
    move-object/from16 v8, p0

    iget v9, v8, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->width:I

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    cmpg-float v9, v0, v9

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x4

    const/high16 v15, 0x3f800000    # 1.0f

    if-gez v9, :cond_0

    .line 291
    new-array v9, v14, [Lcom/google/zxing/ResultPoint;

    new-instance v14, Lcom/google/zxing/ResultPoint;

    sub-float v10, v6, v15

    add-float v11, v7, v15

    invoke-direct {v14, v10, v11}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v14, v9, v13

    new-instance v10, Lcom/google/zxing/ResultPoint;

    add-float v11, v2, v15

    add-float v13, v3, v15

    invoke-direct {v10, v11, v13}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v10, v9, v12

    new-instance v10, Lcom/google/zxing/ResultPoint;

    sub-float v11, v4, v15

    sub-float v12, v5, v15

    invoke-direct {v10, v11, v12}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    const/4 v11, 0x2

    aput-object v10, v9, v11

    new-instance v10, Lcom/google/zxing/ResultPoint;

    add-float v11, v0, v15

    sub-float v12, v1, v15

    invoke-direct {v10, v11, v12}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    const/4 v11, 0x3

    aput-object v10, v9, v11

    return-object v9

    .line 297
    :cond_0
    new-array v9, v14, [Lcom/google/zxing/ResultPoint;

    new-instance v10, Lcom/google/zxing/ResultPoint;

    add-float v11, v6, v15

    add-float v14, v7, v15

    invoke-direct {v10, v11, v14}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v10, v9, v13

    new-instance v10, Lcom/google/zxing/ResultPoint;

    add-float v11, v2, v15

    sub-float v13, v3, v15

    invoke-direct {v10, v11, v13}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    aput-object v10, v9, v12

    new-instance v10, Lcom/google/zxing/ResultPoint;

    sub-float v11, v4, v15

    add-float v12, v5, v15

    invoke-direct {v10, v11, v12}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    const/4 v11, 0x2

    aput-object v10, v9, v11

    new-instance v10, Lcom/google/zxing/ResultPoint;

    sub-float v11, v0, v15

    sub-float v12, v1, v15

    invoke-direct {v10, v11, v12}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    const/4 v11, 0x3

    aput-object v10, v9, v11

    return-object v9
.end method

.method private containsBlackPoint(IIIZ)Z
    .locals 3
    .param p1, "a"    # I
    .param p2, "b"    # I
    .param p3, "fixed"    # I
    .param p4, "horizontal"    # Z

    .line 316
    const/4 v0, 0x1

    if-eqz p4, :cond_2

    .line 317
    move v1, p1

    .local v1, "x":I
    :goto_0
    if-gt v1, p2, :cond_1

    .line 318
    iget-object v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->image:Lcom/google/zxing/common/BitMatrix;

    invoke-virtual {v2, v1, p3}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 319
    return v0

    .line 317
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v1    # "x":I
    :cond_1
    goto :goto_2

    .line 323
    :cond_2
    move v1, p1

    .local v1, "y":I
    :goto_1
    if-gt v1, p2, :cond_4

    .line 324
    iget-object v2, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->image:Lcom/google/zxing/common/BitMatrix;

    invoke-virtual {v2, p3, v1}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 325
    return v0

    .line 323
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 330
    .end local v1    # "y":I
    :cond_4
    :goto_2
    const/4 v0, 0x0

    return v0
.end method

.method private getBlackPointOnSegment(FFFF)Lcom/google/zxing/ResultPoint;
    .locals 9
    .param p1, "aX"    # F
    .param p2, "aY"    # F
    .param p3, "bX"    # F
    .param p4, "bY"    # F

    .line 244
    invoke-static {p1, p2, p3, p4}, Lcom/google/zxing/common/detector/MathUtils;->distance(FFFF)F

    move-result v0

    invoke-static {v0}, Lcom/google/zxing/common/detector/MathUtils;->round(F)I

    move-result v0

    .line 245
    .local v0, "dist":I
    sub-float v1, p3, p1

    int-to-float v2, v0

    div-float/2addr v1, v2

    .line 246
    .local v1, "xStep":F
    sub-float v2, p4, p2

    int-to-float v3, v0

    div-float/2addr v2, v3

    .line 248
    .local v2, "yStep":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_1

    .line 249
    int-to-float v4, v3

    mul-float/2addr v4, v1

    add-float/2addr v4, p1

    invoke-static {v4}, Lcom/google/zxing/common/detector/MathUtils;->round(F)I

    move-result v4

    .line 250
    .local v4, "x":I
    int-to-float v5, v3

    mul-float/2addr v5, v2

    add-float/2addr v5, p2

    invoke-static {v5}, Lcom/google/zxing/common/detector/MathUtils;->round(F)I

    move-result v5

    .line 251
    .local v5, "y":I
    iget-object v6, p0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->image:Lcom/google/zxing/common/BitMatrix;

    invoke-virtual {v6, v4, v5}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 252
    new-instance v6, Lcom/google/zxing/ResultPoint;

    int-to-float v7, v4

    int-to-float v8, v5

    invoke-direct {v6, v7, v8}, Lcom/google/zxing/ResultPoint;-><init>(FF)V

    return-object v6

    .line 248
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 255
    .end local v3    # "i":I
    :cond_1
    const/4 v3, 0x0

    return-object v3
.end method


# virtual methods
.method public detect()[Lcom/google/zxing/ResultPoint;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 95
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->leftInit:I

    .line 96
    .local v1, "left":I
    iget v2, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->rightInit:I

    .line 97
    .local v2, "right":I
    iget v3, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->upInit:I

    .line 98
    .local v3, "up":I
    iget v4, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->downInit:I

    .line 99
    .local v4, "down":I
    const/4 v5, 0x0

    .line 100
    .local v5, "sizeExceeded":Z
    const/4 v6, 0x1

    .line 101
    .local v6, "aBlackPointFoundOnBorder":Z
    const/4 v7, 0x0

    .line 103
    .local v7, "atLeastOneBlackPointFoundOnBorder":Z
    :goto_0
    if-eqz v6, :cond_d

    .line 105
    const/4 v6, 0x0

    .line 110
    const/4 v8, 0x1

    .line 111
    .local v8, "rightBorderNotWhite":Z
    :cond_0
    :goto_1
    const/4 v9, 0x0

    if-eqz v8, :cond_1

    iget v10, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->width:I

    if-ge v2, v10, :cond_1

    .line 112
    invoke-direct {v0, v3, v4, v2, v9}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->containsBlackPoint(IIIZ)Z

    move-result v8

    .line 113
    if-eqz v8, :cond_0

    .line 114
    add-int/lit8 v2, v2, 0x1

    .line 115
    const/4 v6, 0x1

    goto :goto_1

    .line 119
    :cond_1
    iget v10, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->width:I

    if-lt v2, v10, :cond_2

    .line 120
    const/4 v5, 0x1

    .line 121
    goto :goto_5

    .line 127
    :cond_2
    const/4 v10, 0x1

    .line 128
    .local v10, "bottomBorderNotWhite":Z
    :cond_3
    :goto_2
    const/4 v11, 0x1

    if-eqz v10, :cond_4

    iget v12, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->height:I

    if-ge v4, v12, :cond_4

    .line 129
    invoke-direct {v0, v1, v2, v4, v11}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->containsBlackPoint(IIIZ)Z

    move-result v10

    .line 130
    if-eqz v10, :cond_3

    .line 131
    add-int/lit8 v4, v4, 0x1

    .line 132
    const/4 v6, 0x1

    goto :goto_2

    .line 136
    :cond_4
    iget v12, v0, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->height:I

    if-lt v4, v12, :cond_5

    .line 137
    const/4 v5, 0x1

    .line 138
    goto :goto_5

    .line 144
    :cond_5
    const/4 v12, 0x1

    .line 145
    .local v12, "leftBorderNotWhite":Z
    :cond_6
    :goto_3
    if-eqz v12, :cond_7

    if-ltz v1, :cond_7

    .line 146
    invoke-direct {v0, v3, v4, v1, v9}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->containsBlackPoint(IIIZ)Z

    move-result v12

    .line 147
    if-eqz v12, :cond_6

    .line 148
    add-int/lit8 v1, v1, -0x1

    .line 149
    const/4 v6, 0x1

    goto :goto_3

    .line 153
    :cond_7
    if-gez v1, :cond_8

    .line 154
    const/4 v5, 0x1

    .line 155
    goto :goto_5

    .line 161
    :cond_8
    const/4 v9, 0x1

    .line 162
    .local v9, "topBorderNotWhite":Z
    :cond_9
    :goto_4
    if-eqz v9, :cond_a

    if-ltz v3, :cond_a

    .line 163
    invoke-direct {v0, v1, v2, v3, v11}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->containsBlackPoint(IIIZ)Z

    move-result v9

    .line 164
    if-eqz v9, :cond_9

    .line 165
    add-int/lit8 v3, v3, -0x1

    .line 166
    const/4 v6, 0x1

    goto :goto_4

    .line 170
    :cond_a
    if-gez v3, :cond_b

    .line 171
    const/4 v5, 0x1

    .line 172
    goto :goto_5

    .line 175
    :cond_b
    if-eqz v6, :cond_c

    .line 176
    const/4 v7, 0x1

    .line 179
    .end local v8    # "rightBorderNotWhite":Z
    .end local v9    # "topBorderNotWhite":Z
    .end local v10    # "bottomBorderNotWhite":Z
    .end local v12    # "leftBorderNotWhite":Z
    :cond_c
    goto :goto_0

    .line 181
    :cond_d
    :goto_5
    if-nez v5, :cond_1a

    if-eqz v7, :cond_1a

    .line 183
    sub-int v8, v2, v1

    .line 185
    .local v8, "maxSize":I
    const/4 v9, 0x0

    .line 186
    .local v9, "z":Lcom/google/zxing/ResultPoint;
    const/4 v10, 0x1

    .local v10, "i":I
    :goto_6
    if-ge v10, v8, :cond_f

    .line 187
    int-to-float v11, v1

    sub-int v12, v4, v10

    int-to-float v12, v12

    add-int v13, v1, v10

    int-to-float v13, v13

    int-to-float v14, v4

    invoke-direct {v0, v11, v12, v13, v14}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->getBlackPointOnSegment(FFFF)Lcom/google/zxing/ResultPoint;

    move-result-object v9

    .line 188
    if-eqz v9, :cond_e

    .line 189
    goto :goto_7

    .line 186
    :cond_e
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 193
    .end local v10    # "i":I
    :cond_f
    :goto_7
    if-eqz v9, :cond_19

    .line 197
    const/4 v10, 0x0

    .line 199
    .local v10, "t":Lcom/google/zxing/ResultPoint;
    const/4 v11, 0x1

    .local v11, "i":I
    :goto_8
    if-ge v11, v8, :cond_11

    .line 200
    int-to-float v12, v1

    add-int v13, v3, v11

    int-to-float v13, v13

    add-int v14, v1, v11

    int-to-float v14, v14

    int-to-float v15, v3

    invoke-direct {v0, v12, v13, v14, v15}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->getBlackPointOnSegment(FFFF)Lcom/google/zxing/ResultPoint;

    move-result-object v10

    .line 201
    if-eqz v10, :cond_10

    .line 202
    goto :goto_9

    .line 199
    :cond_10
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 206
    .end local v11    # "i":I
    :cond_11
    :goto_9
    if-eqz v10, :cond_18

    .line 210
    const/4 v11, 0x0

    .line 212
    .local v11, "x":Lcom/google/zxing/ResultPoint;
    const/4 v12, 0x1

    .local v12, "i":I
    :goto_a
    if-ge v12, v8, :cond_13

    .line 213
    int-to-float v13, v2

    add-int v14, v3, v12

    int-to-float v14, v14

    sub-int v15, v2, v12

    int-to-float v15, v15

    move/from16 v16, v1

    .end local v1    # "left":I
    .local v16, "left":I
    int-to-float v1, v3

    invoke-direct {v0, v13, v14, v15, v1}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->getBlackPointOnSegment(FFFF)Lcom/google/zxing/ResultPoint;

    move-result-object v11

    .line 214
    if-eqz v11, :cond_12

    .line 215
    goto :goto_b

    .line 212
    :cond_12
    add-int/lit8 v12, v12, 0x1

    move/from16 v1, v16

    goto :goto_a

    .end local v16    # "left":I
    .restart local v1    # "left":I
    :cond_13
    move/from16 v16, v1

    .line 219
    .end local v1    # "left":I
    .end local v12    # "i":I
    .restart local v16    # "left":I
    :goto_b
    if-eqz v11, :cond_17

    .line 223
    const/4 v1, 0x0

    .line 225
    .local v1, "y":Lcom/google/zxing/ResultPoint;
    const/4 v12, 0x1

    .restart local v12    # "i":I
    :goto_c
    if-ge v12, v8, :cond_15

    .line 226
    int-to-float v13, v2

    sub-int v14, v4, v12

    int-to-float v14, v14

    sub-int v15, v2, v12

    int-to-float v15, v15

    move-object/from16 v17, v1

    .end local v1    # "y":Lcom/google/zxing/ResultPoint;
    .local v17, "y":Lcom/google/zxing/ResultPoint;
    int-to-float v1, v4

    invoke-direct {v0, v13, v14, v15, v1}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->getBlackPointOnSegment(FFFF)Lcom/google/zxing/ResultPoint;

    move-result-object v1

    .line 227
    .end local v17    # "y":Lcom/google/zxing/ResultPoint;
    .restart local v1    # "y":Lcom/google/zxing/ResultPoint;
    if-eqz v1, :cond_14

    .line 228
    goto :goto_d

    .line 225
    :cond_14
    add-int/lit8 v12, v12, 0x1

    goto :goto_c

    :cond_15
    move-object/from16 v17, v1

    .line 232
    .end local v12    # "i":I
    :goto_d
    if-eqz v1, :cond_16

    .line 236
    invoke-direct {v0, v1, v9, v11, v10}, Lcom/google/zxing/common/detector/WhiteRectangleDetector;->centerEdges(Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;Lcom/google/zxing/ResultPoint;)[Lcom/google/zxing/ResultPoint;

    move-result-object v12

    return-object v12

    .line 233
    :cond_16
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v12

    throw v12

    .line 220
    .end local v1    # "y":Lcom/google/zxing/ResultPoint;
    :cond_17
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 207
    .end local v11    # "x":Lcom/google/zxing/ResultPoint;
    .end local v16    # "left":I
    .local v1, "left":I
    :cond_18
    move/from16 v16, v1

    .end local v1    # "left":I
    .restart local v16    # "left":I
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 194
    .end local v10    # "t":Lcom/google/zxing/ResultPoint;
    .end local v16    # "left":I
    .restart local v1    # "left":I
    :cond_19
    move/from16 v16, v1

    .end local v1    # "left":I
    .restart local v16    # "left":I
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 181
    .end local v8    # "maxSize":I
    .end local v9    # "z":Lcom/google/zxing/ResultPoint;
    .end local v16    # "left":I
    .restart local v1    # "left":I
    :cond_1a
    move/from16 v16, v1

    .line 239
    .end local v1    # "left":I
    .restart local v16    # "left":I
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1
.end method
