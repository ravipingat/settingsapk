.class public final Lcom/google/zxing/common/StringUtils;
.super Ljava/lang/Object;
.source "StringUtils.java"


# static fields
.field private static final ASSUME_SHIFT_JIS:Z

.field private static final EUC_JP:Ljava/lang/String; = "EUC_JP"

.field public static final GB2312:Ljava/lang/String; = "GB2312"

.field private static final ISO88591:Ljava/lang/String; = "ISO8859_1"

.field private static final PLATFORM_DEFAULT_ENCODING:Ljava/lang/String;

.field public static final SHIFT_JIS:Ljava/lang/String; = "SJIS"

.field private static final UTF8:Ljava/lang/String; = "UTF8"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 31
    const-string v0, "file.encoding"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/common/StringUtils;->PLATFORM_DEFAULT_ENCODING:Ljava/lang/String;

    .line 38
    sget-object v0, Lcom/google/zxing/common/StringUtils;->PLATFORM_DEFAULT_ENCODING:Ljava/lang/String;

    const-string v1, "SJIS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/zxing/common/StringUtils;->PLATFORM_DEFAULT_ENCODING:Ljava/lang/String;

    const-string v1, "EUC_JP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    sput-boolean v0, Lcom/google/zxing/common/StringUtils;->ASSUME_SHIFT_JIS:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static guessEncoding([BLjava/util/Map;)Ljava/lang/String;
    .locals 21
    .param p0, "bytes"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/Map<",
            "Lcom/google/zxing/DecodeHintType;",
            "*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 52
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/zxing/DecodeHintType;*>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eqz v1, :cond_0

    .line 53
    sget-object v2, Lcom/google/zxing/DecodeHintType;->CHARACTER_SET:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 54
    .local v2, "characterSet":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 55
    return-object v2

    .line 60
    .end local v2    # "characterSet":Ljava/lang/String;
    :cond_0
    array-length v2, v0

    .line 61
    .local v2, "length":I
    const/4 v3, 0x1

    .line 62
    .local v3, "canBeISO88591":Z
    const/4 v4, 0x1

    .line 63
    .local v4, "canBeShiftJIS":Z
    const/4 v5, 0x1

    .line 64
    .local v5, "canBeUTF8":Z
    const/4 v6, 0x0

    .line 66
    .local v6, "utf8BytesLeft":I
    const/4 v7, 0x0

    .line 67
    .local v7, "utf2BytesChars":I
    const/4 v8, 0x0

    .line 68
    .local v8, "utf3BytesChars":I
    const/4 v9, 0x0

    .line 69
    .local v9, "utf4BytesChars":I
    const/4 v10, 0x0

    .line 71
    .local v10, "sjisBytesLeft":I
    const/4 v11, 0x0

    .line 73
    .local v11, "sjisKatakanaChars":I
    const/4 v12, 0x0

    .line 74
    .local v12, "sjisCurKatakanaWordLength":I
    const/4 v13, 0x0

    .line 75
    .local v13, "sjisCurDoubleBytesWordLength":I
    const/4 v14, 0x0

    .line 76
    .local v14, "sjisMaxKatakanaWordLength":I
    const/4 v15, 0x0

    .line 79
    .local v15, "sjisMaxDoubleBytesWordLength":I
    const/16 v16, 0x0

    .line 81
    .local v16, "isoHighOther":I
    array-length v1, v0

    const/16 v17, 0x0

    move/from16 v18, v3

    .end local v3    # "canBeISO88591":Z
    .local v18, "canBeISO88591":Z
    const/4 v3, 0x3

    const/16 v20, 0x1

    if-le v1, v3, :cond_1

    aget-byte v1, v0, v17

    const/16 v3, -0x11

    if-ne v1, v3, :cond_1

    aget-byte v1, v0, v20

    const/16 v3, -0x45

    if-ne v1, v3, :cond_1

    const/4 v1, 0x2

    aget-byte v3, v0, v1

    const/16 v1, -0x41

    if-ne v3, v1, :cond_1

    move/from16 v17, v20

    :cond_1
    move/from16 v1, v17

    .line 86
    .local v1, "utf8bom":Z
    const/4 v3, 0x0

    .line 87
    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_18

    if-nez v18, :cond_3

    if-nez v4, :cond_3

    if-eqz v5, :cond_2

    goto :goto_1

    :cond_2
    move/from16 v17, v2

    goto/16 :goto_7

    .line 90
    :cond_3
    :goto_1
    move/from16 v17, v2

    .end local v2    # "length":I
    .local v17, "length":I
    aget-byte v2, v0, v3

    and-int/lit16 v2, v2, 0xff

    .line 93
    .local v2, "value":I
    if-eqz v5, :cond_a

    .line 94
    if-lez v6, :cond_5

    .line 95
    and-int/lit16 v0, v2, 0x80

    if-nez v0, :cond_4

    .line 96
    const/4 v0, 0x0

    move v5, v0

    .end local v5    # "canBeUTF8":Z
    .local v0, "canBeUTF8":Z
    goto :goto_2

    .line 98
    .end local v0    # "canBeUTF8":Z
    .restart local v5    # "canBeUTF8":Z
    :cond_4
    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    .line 100
    :cond_5
    and-int/lit16 v0, v2, 0x80

    if-eqz v0, :cond_a

    .line 101
    and-int/lit8 v0, v2, 0x40

    if-nez v0, :cond_6

    .line 102
    const/4 v0, 0x0

    move v5, v0

    .end local v5    # "canBeUTF8":Z
    .restart local v0    # "canBeUTF8":Z
    goto :goto_2

    .line 104
    .end local v0    # "canBeUTF8":Z
    .restart local v5    # "canBeUTF8":Z
    :cond_6
    add-int/lit8 v6, v6, 0x1

    .line 105
    and-int/lit8 v0, v2, 0x20

    if-nez v0, :cond_7

    .line 106
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 108
    :cond_7
    add-int/lit8 v6, v6, 0x1

    .line 109
    and-int/lit8 v0, v2, 0x10

    if-nez v0, :cond_8

    .line 110
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 112
    :cond_8
    add-int/lit8 v6, v6, 0x1

    .line 113
    and-int/lit8 v0, v2, 0x8

    if-nez v0, :cond_9

    .line 114
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 116
    :cond_9
    const/4 v0, 0x0

    move v5, v0

    .line 127
    :cond_a
    :goto_2
    const/16 v0, 0x7f

    if-eqz v18, :cond_d

    .line 128
    if-le v2, v0, :cond_b

    const/16 v0, 0xa0

    if-ge v2, v0, :cond_b

    .line 129
    const/4 v0, 0x0

    move/from16 v18, v0

    .end local v18    # "canBeISO88591":Z
    .local v0, "canBeISO88591":Z
    goto :goto_3

    .line 130
    .end local v0    # "canBeISO88591":Z
    .restart local v18    # "canBeISO88591":Z
    :cond_b
    const/16 v0, 0x9f

    if-le v2, v0, :cond_d

    .line 131
    const/16 v0, 0xc0

    if-lt v2, v0, :cond_c

    const/16 v0, 0xd7

    if-eq v2, v0, :cond_c

    const/16 v0, 0xf7

    if-ne v2, v0, :cond_d

    .line 132
    :cond_c
    add-int/lit8 v16, v16, 0x1

    .line 142
    :cond_d
    :goto_3
    if-eqz v4, :cond_17

    .line 143
    if-lez v10, :cond_10

    .line 144
    const/16 v0, 0x40

    if-lt v2, v0, :cond_f

    const/16 v0, 0x7f

    if-eq v2, v0, :cond_f

    const/16 v0, 0xfc

    if-le v2, v0, :cond_e

    goto :goto_4

    .line 147
    :cond_e
    add-int/lit8 v10, v10, -0x1

    goto :goto_6

    .line 145
    :cond_f
    :goto_4
    const/4 v0, 0x0

    move v4, v0

    .end local v4    # "canBeShiftJIS":Z
    .local v0, "canBeShiftJIS":Z
    goto :goto_6

    .line 149
    .end local v0    # "canBeShiftJIS":Z
    .restart local v4    # "canBeShiftJIS":Z
    :cond_10
    const/16 v0, 0x80

    if-eq v2, v0, :cond_16

    const/16 v0, 0xa0

    if-eq v2, v0, :cond_16

    const/16 v0, 0xef

    if-le v2, v0, :cond_11

    goto :goto_5

    .line 151
    :cond_11
    const/16 v0, 0xa0

    if-le v2, v0, :cond_13

    const/16 v0, 0xe0

    if-ge v2, v0, :cond_13

    .line 152
    add-int/lit8 v11, v11, 0x1

    .line 153
    const/4 v0, 0x0

    .line 154
    .end local v13    # "sjisCurDoubleBytesWordLength":I
    .local v0, "sjisCurDoubleBytesWordLength":I
    add-int/lit8 v12, v12, 0x1

    .line 155
    if-le v12, v14, :cond_12

    .line 156
    move v13, v12

    move v14, v13

    move v13, v0

    .end local v14    # "sjisMaxKatakanaWordLength":I
    .local v13, "sjisMaxKatakanaWordLength":I
    goto :goto_6

    .line 155
    .end local v13    # "sjisMaxKatakanaWordLength":I
    .restart local v14    # "sjisMaxKatakanaWordLength":I
    :cond_12
    move v13, v0

    goto :goto_6

    .line 158
    .end local v0    # "sjisCurDoubleBytesWordLength":I
    .local v13, "sjisCurDoubleBytesWordLength":I
    :cond_13
    const/16 v0, 0x7f

    if-le v2, v0, :cond_15

    .line 159
    add-int/lit8 v10, v10, 0x1

    .line 161
    const/4 v0, 0x0

    .line 162
    .end local v12    # "sjisCurKatakanaWordLength":I
    .local v0, "sjisCurKatakanaWordLength":I
    add-int/lit8 v13, v13, 0x1

    .line 163
    if-le v13, v15, :cond_14

    .line 164
    move v12, v13

    move v15, v12

    move v12, v0

    .end local v15    # "sjisMaxDoubleBytesWordLength":I
    .local v12, "sjisMaxDoubleBytesWordLength":I
    goto :goto_6

    .line 163
    .end local v12    # "sjisMaxDoubleBytesWordLength":I
    .restart local v15    # "sjisMaxDoubleBytesWordLength":I
    :cond_14
    move v12, v0

    goto :goto_6

    .line 168
    .end local v0    # "sjisCurKatakanaWordLength":I
    .local v12, "sjisCurKatakanaWordLength":I
    :cond_15
    const/4 v0, 0x0

    .line 169
    .end local v12    # "sjisCurKatakanaWordLength":I
    .restart local v0    # "sjisCurKatakanaWordLength":I
    const/4 v12, 0x0

    move v13, v12

    move v12, v0

    .end local v13    # "sjisCurDoubleBytesWordLength":I
    .local v12, "sjisCurDoubleBytesWordLength":I
    goto :goto_6

    .line 150
    .end local v0    # "sjisCurKatakanaWordLength":I
    .local v12, "sjisCurKatakanaWordLength":I
    .restart local v13    # "sjisCurDoubleBytesWordLength":I
    :cond_16
    :goto_5
    const/4 v0, 0x0

    move v4, v0

    .line 88
    .end local v2    # "value":I
    :cond_17
    :goto_6
    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    move/from16 v2, v17

    goto/16 :goto_0

    .line 87
    .end local v17    # "length":I
    .local v2, "length":I
    :cond_18
    move/from16 v17, v2

    .line 174
    .end local v2    # "length":I
    .end local v3    # "i":I
    .restart local v17    # "length":I
    :goto_7
    if-eqz v5, :cond_19

    if-lez v6, :cond_19

    .line 175
    const/4 v5, 0x0

    .line 177
    :cond_19
    if-eqz v4, :cond_1a

    if-lez v10, :cond_1a

    .line 178
    const/4 v4, 0x0

    .line 182
    :cond_1a
    const-string v0, "UTF8"

    if-eqz v5, :cond_1c

    if-nez v1, :cond_1b

    add-int v2, v7, v8

    add-int/2addr v2, v9

    if-lez v2, :cond_1c

    .line 183
    :cond_1b
    return-object v0

    .line 186
    :cond_1c
    const-string v2, "SJIS"

    if-eqz v4, :cond_1e

    sget-boolean v3, Lcom/google/zxing/common/StringUtils;->ASSUME_SHIFT_JIS:Z

    if-nez v3, :cond_1d

    const/4 v3, 0x3

    if-ge v14, v3, :cond_1d

    if-lt v15, v3, :cond_1e

    .line 187
    :cond_1d
    return-object v2

    .line 194
    :cond_1e
    const-string v3, "ISO8859_1"

    if-eqz v18, :cond_22

    if-eqz v4, :cond_22

    .line 195
    const/4 v0, 0x2

    if-ne v14, v0, :cond_20

    if-eq v11, v0, :cond_1f

    goto :goto_8

    :cond_1f
    move/from16 v19, v1

    move/from16 v1, v17

    goto :goto_9

    :cond_20
    :goto_8
    mul-int/lit8 v0, v16, 0xa

    move/from16 v19, v1

    move/from16 v1, v17

    .end local v17    # "length":I
    .local v1, "length":I
    .local v19, "utf8bom":Z
    if-lt v0, v1, :cond_21

    :goto_9
    goto :goto_a

    :cond_21
    move-object v2, v3

    :goto_a
    return-object v2

    .line 194
    .end local v19    # "utf8bom":Z
    .local v1, "utf8bom":Z
    .restart local v17    # "length":I
    :cond_22
    move/from16 v19, v1

    move/from16 v1, v17

    .line 200
    .end local v17    # "length":I
    .local v1, "length":I
    .restart local v19    # "utf8bom":Z
    if-eqz v18, :cond_23

    .line 201
    return-object v3

    .line 203
    :cond_23
    if-eqz v4, :cond_24

    .line 204
    return-object v2

    .line 206
    :cond_24
    if-eqz v5, :cond_25

    .line 207
    return-object v0

    .line 210
    :cond_25
    sget-object v0, Lcom/google/zxing/common/StringUtils;->PLATFORM_DEFAULT_ENCODING:Ljava/lang/String;

    return-object v0
.end method
