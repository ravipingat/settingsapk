.class public final Lcom/google/zxing/common/BitArray;
.super Ljava/lang/Object;
.source "BitArray.java"


# instance fields
.field private bits:[I

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    .line 32
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lcom/google/zxing/common/BitArray;->size:I

    .line 36
    invoke-static {p1}, Lcom/google/zxing/common/BitArray;->makeArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    .line 37
    return-void
.end method

.method private ensureCapacity(I)V
    .locals 4
    .param p1, "size"    # I

    .line 48
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x5

    if-le p1, v0, :cond_0

    .line 49
    invoke-static {p1}, Lcom/google/zxing/common/BitArray;->makeArray(I)[I

    move-result-object v0

    .line 50
    .local v0, "newBits":[I
    iget-object v1, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v2, v1

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 51
    iput-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    .line 53
    .end local v0    # "newBits":[I
    :cond_0
    return-void
.end method

.method private static makeArray(I)[I
    .locals 1
    .param p0, "size"    # I

    .line 306
    add-int/lit8 v0, p0, 0x1f

    shr-int/lit8 v0, v0, 0x5

    new-array v0, v0, [I

    return-object v0
.end method


# virtual methods
.method public appendBit(Z)V
    .locals 5
    .param p1, "bit"    # Z

    .line 221
    iget v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/zxing/common/BitArray;->ensureCapacity(I)V

    .line 222
    if-eqz p1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    iget v2, p0, Lcom/google/zxing/common/BitArray;->size:I

    shr-int/lit8 v3, v2, 0x5

    aget v4, v0, v3

    and-int/lit8 v2, v2, 0x1f

    shl-int v2, v1, v2

    or-int/2addr v2, v4

    aput v2, v0, v3

    .line 225
    :cond_0
    iget v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    .line 226
    return-void
.end method

.method public appendBitArray(Lcom/google/zxing/common/BitArray;)V
    .locals 3
    .param p1, "other"    # Lcom/google/zxing/common/BitArray;

    .line 244
    iget v0, p1, Lcom/google/zxing/common/BitArray;->size:I

    .line 245
    .local v0, "otherSize":I
    iget v1, p0, Lcom/google/zxing/common/BitArray;->size:I

    add-int/2addr v1, v0

    invoke-direct {p0, v1}, Lcom/google/zxing/common/BitArray;->ensureCapacity(I)V

    .line 246
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 247
    invoke-virtual {p1, v1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/zxing/common/BitArray;->appendBit(Z)V

    .line 246
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 249
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public appendBits(II)V
    .locals 3
    .param p1, "value"    # I
    .param p2, "numBits"    # I

    .line 234
    if-ltz p2, :cond_2

    const/16 v0, 0x20

    if-gt p2, v0, :cond_2

    .line 237
    iget v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    add-int/2addr v0, p2

    invoke-direct {p0, v0}, Lcom/google/zxing/common/BitArray;->ensureCapacity(I)V

    .line 238
    move v0, p2

    .local v0, "numBitsLeft":I
    :goto_0
    if-lez v0, :cond_1

    .line 239
    add-int/lit8 v1, v0, -0x1

    shr-int v1, p1, v1

    const/4 v2, 0x1

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_0

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/zxing/common/BitArray;->appendBit(Z)V

    .line 238
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 241
    .end local v0    # "numBitsLeft":I
    :cond_1
    return-void

    .line 235
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Num bits must be between 0 and 32"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clear()V
    .locals 4

    .line 173
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v0, v0

    .line 174
    .local v0, "max":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 175
    iget-object v2, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    const/4 v3, 0x0

    aput v3, v2, v1

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public flip(I)V
    .locals 5
    .param p1, "i"    # I

    .line 78
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    shr-int/lit8 v1, p1, 0x5

    aget v2, v0, v1

    and-int/lit8 v3, p1, 0x1f

    const/4 v4, 0x1

    shl-int v3, v4, v3

    xor-int/2addr v2, v3

    aput v2, v0, v1

    .line 79
    return-void
.end method

.method public get(I)Z
    .locals 3
    .param p1, "i"    # I

    .line 60
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    shr-int/lit8 v1, p1, 0x5

    aget v0, v0, v1

    and-int/lit8 v1, p1, 0x1f

    const/4 v2, 0x1

    shl-int v1, v2, v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public getBitArray()[I
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    return-object v0
.end method

.method public getNextSet(I)I
    .locals 4
    .param p1, "from"    # I

    .line 88
    iget v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    if-lt p1, v0, :cond_0

    .line 89
    return v0

    .line 91
    :cond_0
    shr-int/lit8 v0, p1, 0x5

    .line 92
    .local v0, "bitsOffset":I
    iget-object v1, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    aget v1, v1, v0

    .line 94
    .local v1, "currentBits":I
    and-int/lit8 v2, p1, 0x1f

    const/4 v3, 0x1

    shl-int v2, v3, v2

    sub-int/2addr v2, v3

    not-int v2, v2

    and-int/2addr v1, v2

    .line 95
    :goto_0
    if-nez v1, :cond_2

    .line 96
    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v3, v2

    if-ne v0, v3, :cond_1

    .line 97
    iget v2, p0, Lcom/google/zxing/common/BitArray;->size:I

    return v2

    .line 99
    :cond_1
    aget v1, v2, v0

    goto :goto_0

    .line 101
    :cond_2
    shl-int/lit8 v2, v0, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 102
    .local v2, "result":I
    iget v3, p0, Lcom/google/zxing/common/BitArray;->size:I

    if-le v2, v3, :cond_3

    goto :goto_1

    :cond_3
    move v3, v2

    :goto_1
    return v3
.end method

.method public getNextUnset(I)I
    .locals 4
    .param p1, "from"    # I

    .line 109
    iget v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    if-lt p1, v0, :cond_0

    .line 110
    return v0

    .line 112
    :cond_0
    shr-int/lit8 v0, p1, 0x5

    .line 113
    .local v0, "bitsOffset":I
    iget-object v1, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    aget v1, v1, v0

    not-int v1, v1

    .line 115
    .local v1, "currentBits":I
    and-int/lit8 v2, p1, 0x1f

    const/4 v3, 0x1

    shl-int v2, v3, v2

    sub-int/2addr v2, v3

    not-int v2, v2

    and-int/2addr v1, v2

    .line 116
    :goto_0
    if-nez v1, :cond_2

    .line 117
    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v3, v2

    if-ne v0, v3, :cond_1

    .line 118
    iget v2, p0, Lcom/google/zxing/common/BitArray;->size:I

    return v2

    .line 120
    :cond_1
    aget v2, v2, v0

    not-int v1, v2

    goto :goto_0

    .line 122
    :cond_2
    shl-int/lit8 v2, v0, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 123
    .local v2, "result":I
    iget v3, p0, Lcom/google/zxing/common/BitArray;->size:I

    if-le v2, v3, :cond_3

    goto :goto_1

    :cond_3
    move v3, v2

    :goto_1
    return v3
.end method

.method public getSize()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    return v0
.end method

.method public getSizeInBytes()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/google/zxing/common/BitArray;->size:I

    add-int/lit8 v0, v0, 0x7

    shr-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public isRange(IIZ)Z
    .locals 10
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "value"    # Z

    .line 189
    if-lt p2, p1, :cond_8

    .line 192
    const/4 v0, 0x1

    if-ne p2, p1, :cond_0

    .line 193
    return v0

    .line 195
    :cond_0
    add-int/lit8 p2, p2, -0x1

    .line 196
    shr-int/lit8 v1, p1, 0x5

    .line 197
    .local v1, "firstInt":I
    shr-int/lit8 v2, p2, 0x5

    .line 198
    .local v2, "lastInt":I
    move v3, v1

    .local v3, "i":I
    :goto_0
    if-gt v3, v2, :cond_7

    .line 199
    const/4 v4, 0x0

    if-le v3, v1, :cond_1

    move v5, v4

    goto :goto_1

    :cond_1
    and-int/lit8 v5, p1, 0x1f

    .line 200
    .local v5, "firstBit":I
    :goto_1
    const/16 v6, 0x1f

    if-ge v3, v2, :cond_2

    move v7, v6

    goto :goto_2

    :cond_2
    and-int/lit8 v7, p2, 0x1f

    .line 202
    .local v7, "lastBit":I
    :goto_2
    if-nez v5, :cond_3

    if-ne v7, v6, :cond_3

    .line 203
    const/4 v6, -0x1

    .local v6, "mask":I
    goto :goto_4

    .line 205
    .end local v6    # "mask":I
    :cond_3
    const/4 v6, 0x0

    .line 206
    .restart local v6    # "mask":I
    move v8, v5

    .local v8, "j":I
    :goto_3
    if-gt v8, v7, :cond_4

    .line 207
    shl-int v9, v0, v8

    or-int/2addr v6, v9

    .line 206
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 213
    .end local v8    # "j":I
    :cond_4
    :goto_4
    iget-object v8, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    aget v8, v8, v3

    and-int/2addr v8, v6

    if-eqz p3, :cond_5

    move v9, v6

    goto :goto_5

    :cond_5
    move v9, v4

    :goto_5
    if-eq v8, v9, :cond_6

    .line 214
    return v4

    .line 198
    .end local v5    # "firstBit":I
    .end local v6    # "mask":I
    .end local v7    # "lastBit":I
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 217
    .end local v3    # "i":I
    :cond_7
    return v0

    .line 190
    .end local v1    # "firstInt":I
    .end local v2    # "lastInt":I
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public reverse()V
    .locals 7

    .line 295
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v0, v0

    new-array v0, v0, [I

    .line 296
    .local v0, "newBits":[I
    iget v1, p0, Lcom/google/zxing/common/BitArray;->size:I

    .line 297
    .local v1, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 298
    sub-int v3, v1, v2

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    invoke-virtual {p0, v3}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 299
    shr-int/lit8 v3, v2, 0x5

    aget v5, v0, v3

    and-int/lit8 v6, v2, 0x1f

    shl-int/2addr v4, v6

    or-int/2addr v4, v5

    aput v4, v0, v3

    .line 297
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 302
    .end local v2    # "i":I
    :cond_1
    iput-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    .line 303
    return-void
.end method

.method public set(I)V
    .locals 5
    .param p1, "i"    # I

    .line 69
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    shr-int/lit8 v1, p1, 0x5

    aget v2, v0, v1

    and-int/lit8 v3, p1, 0x1f

    const/4 v4, 0x1

    shl-int v3, v4, v3

    or-int/2addr v2, v3

    aput v2, v0, v1

    .line 70
    return-void
.end method

.method public setBulk(II)V
    .locals 2
    .param p1, "i"    # I
    .param p2, "newBits"    # I

    .line 134
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    shr-int/lit8 v1, p1, 0x5

    aput p2, v0, v1

    .line 135
    return-void
.end method

.method public setRange(II)V
    .locals 8
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 144
    if-lt p2, p1, :cond_6

    .line 147
    if-ne p2, p1, :cond_0

    .line 148
    return-void

    .line 150
    :cond_0
    add-int/lit8 p2, p2, -0x1

    .line 151
    shr-int/lit8 v0, p1, 0x5

    .line 152
    .local v0, "firstInt":I
    shr-int/lit8 v1, p2, 0x5

    .line 153
    .local v1, "lastInt":I
    move v2, v0

    .local v2, "i":I
    :goto_0
    if-gt v2, v1, :cond_5

    .line 154
    if-le v2, v0, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    and-int/lit8 v3, p1, 0x1f

    .line 155
    .local v3, "firstBit":I
    :goto_1
    const/16 v4, 0x1f

    if-ge v2, v1, :cond_2

    move v5, v4

    goto :goto_2

    :cond_2
    and-int/lit8 v5, p2, 0x1f

    .line 157
    .local v5, "lastBit":I
    :goto_2
    if-nez v3, :cond_3

    if-ne v5, v4, :cond_3

    .line 158
    const/4 v4, -0x1

    .local v4, "mask":I
    goto :goto_4

    .line 160
    .end local v4    # "mask":I
    :cond_3
    const/4 v4, 0x0

    .line 161
    .restart local v4    # "mask":I
    move v6, v3

    .local v6, "j":I
    :goto_3
    if-gt v6, v5, :cond_4

    .line 162
    const/4 v7, 0x1

    shl-int/2addr v7, v6

    or-int/2addr v4, v7

    .line 161
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 165
    .end local v6    # "j":I
    :cond_4
    :goto_4
    iget-object v6, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    aget v7, v6, v2

    or-int/2addr v7, v4

    aput v7, v6, v2

    .line 153
    .end local v3    # "firstBit":I
    .end local v4    # "mask":I
    .end local v5    # "lastBit":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 167
    .end local v2    # "i":I
    :cond_5
    return-void

    .line 145
    .end local v0    # "firstInt":I
    .end local v1    # "lastInt":I
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public toBytes(I[BII)V
    .locals 5
    .param p1, "bitOffset"    # I
    .param p2, "array"    # [B
    .param p3, "offset"    # I
    .param p4, "numBytes"    # I

    .line 271
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_2

    .line 272
    const/4 v1, 0x0

    .line 273
    .local v1, "theByte":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    const/16 v3, 0x8

    if-ge v2, v3, :cond_1

    .line 274
    invoke-virtual {p0, p1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 275
    rsub-int/lit8 v3, v2, 0x7

    const/4 v4, 0x1

    shl-int v3, v4, v3

    or-int/2addr v1, v3

    .line 277
    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 273
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 279
    .end local v2    # "j":I
    :cond_1
    add-int v2, p3, v0

    int-to-byte v3, v1

    aput-byte v3, p2, v2

    .line 271
    .end local v1    # "theByte":I
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/zxing/common/BitArray;->size:I

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 312
    .local v0, "result":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/google/zxing/common/BitArray;->size:I

    if-ge v1, v2, :cond_2

    .line 313
    and-int/lit8 v2, v1, 0x7

    if-nez v2, :cond_0

    .line 314
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 316
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/zxing/common/BitArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x58

    goto :goto_1

    :cond_1
    const/16 v2, 0x2e

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 312
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 318
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public xor(Lcom/google/zxing/common/BitArray;)V
    .locals 4
    .param p1, "other"    # Lcom/google/zxing/common/BitArray;

    .line 252
    iget-object v0, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v0, v0

    iget-object v1, p1, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 255
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/zxing/common/BitArray;->bits:[I

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 258
    aget v2, v1, v0

    iget-object v3, p1, Lcom/google/zxing/common/BitArray;->bits:[I

    aget v3, v3, v0

    xor-int/2addr v2, v3

    aput v2, v1, v0

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 253
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Sizes don\'t match"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
