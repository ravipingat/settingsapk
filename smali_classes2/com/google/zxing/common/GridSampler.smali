.class public abstract Lcom/google/zxing/common/GridSampler;
.super Ljava/lang/Object;
.source "GridSampler.java"


# static fields
.field private static gridSampler:Lcom/google/zxing/common/GridSampler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lcom/google/zxing/common/DefaultGridSampler;

    invoke-direct {v0}, Lcom/google/zxing/common/DefaultGridSampler;-><init>()V

    sput-object v0, Lcom/google/zxing/common/GridSampler;->gridSampler:Lcom/google/zxing/common/GridSampler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static checkAndNudgePoints(Lcom/google/zxing/common/BitMatrix;[F)V
    .locals 10
    .param p0, "image"    # Lcom/google/zxing/common/BitMatrix;
    .param p1, "points"    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation

    .line 102
    invoke-virtual {p0}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v0

    .line 103
    .local v0, "width":I
    invoke-virtual {p0}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v1

    .line 105
    .local v1, "height":I
    const/4 v2, 0x1

    .line 106
    .local v2, "nudged":Z
    const/4 v3, 0x0

    .local v3, "offset":I
    :goto_0
    array-length v4, p1

    const/4 v5, 0x0

    const/4 v6, -0x1

    if-ge v3, v4, :cond_5

    if-eqz v2, :cond_5

    .line 107
    aget v4, p1, v3

    float-to-int v4, v4

    .line 108
    .local v4, "x":I
    add-int/lit8 v7, v3, 0x1

    aget v7, p1, v7

    float-to-int v7, v7

    .line 109
    .local v7, "y":I
    if-lt v4, v6, :cond_4

    if-gt v4, v0, :cond_4

    if-lt v7, v6, :cond_4

    if-gt v7, v1, :cond_4

    .line 112
    const/4 v2, 0x0

    .line 113
    if-ne v4, v6, :cond_0

    .line 114
    aput v5, p1, v3

    .line 115
    const/4 v2, 0x1

    goto :goto_1

    .line 116
    :cond_0
    if-ne v4, v0, :cond_1

    .line 117
    add-int/lit8 v8, v0, -0x1

    int-to-float v8, v8

    aput v8, p1, v3

    .line 118
    const/4 v2, 0x1

    .line 120
    :cond_1
    :goto_1
    if-ne v7, v6, :cond_2

    .line 121
    add-int/lit8 v6, v3, 0x1

    aput v5, p1, v6

    .line 122
    const/4 v2, 0x1

    goto :goto_2

    .line 123
    :cond_2
    if-ne v7, v1, :cond_3

    .line 124
    add-int/lit8 v5, v3, 0x1

    add-int/lit8 v6, v1, -0x1

    int-to-float v6, v6

    aput v6, p1, v5

    .line 125
    const/4 v2, 0x1

    .line 106
    .end local v4    # "x":I
    .end local v7    # "y":I
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 110
    .restart local v4    # "x":I
    .restart local v7    # "y":I
    :cond_4
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v5

    throw v5

    .line 129
    .end local v3    # "offset":I
    .end local v4    # "x":I
    .end local v7    # "y":I
    :cond_5
    const/4 v2, 0x1

    .line 130
    array-length v3, p1

    add-int/lit8 v3, v3, -0x2

    .restart local v3    # "offset":I
    :goto_3
    if-ltz v3, :cond_b

    if-eqz v2, :cond_b

    .line 131
    aget v4, p1, v3

    float-to-int v4, v4

    .line 132
    .restart local v4    # "x":I
    add-int/lit8 v7, v3, 0x1

    aget v7, p1, v7

    float-to-int v7, v7

    .line 133
    .restart local v7    # "y":I
    if-lt v4, v6, :cond_a

    if-gt v4, v0, :cond_a

    if-lt v7, v6, :cond_a

    if-gt v7, v1, :cond_a

    .line 136
    const/4 v2, 0x0

    .line 137
    if-ne v4, v6, :cond_6

    .line 138
    aput v5, p1, v3

    .line 139
    const/4 v2, 0x1

    goto :goto_4

    .line 140
    :cond_6
    if-ne v4, v0, :cond_7

    .line 141
    add-int/lit8 v8, v0, -0x1

    int-to-float v8, v8

    aput v8, p1, v3

    .line 142
    const/4 v2, 0x1

    .line 144
    :cond_7
    :goto_4
    if-ne v7, v6, :cond_8

    .line 145
    add-int/lit8 v8, v3, 0x1

    aput v5, p1, v8

    .line 146
    const/4 v2, 0x1

    goto :goto_5

    .line 147
    :cond_8
    if-ne v7, v1, :cond_9

    .line 148
    add-int/lit8 v8, v3, 0x1

    add-int/lit8 v9, v1, -0x1

    int-to-float v9, v9

    aput v9, p1, v8

    .line 149
    const/4 v2, 0x1

    .line 130
    .end local v4    # "x":I
    .end local v7    # "y":I
    :cond_9
    :goto_5
    add-int/lit8 v3, v3, -0x2

    goto :goto_3

    .line 134
    .restart local v4    # "x":I
    .restart local v7    # "y":I
    :cond_a
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v5

    throw v5

    .line 152
    .end local v3    # "offset":I
    .end local v4    # "x":I
    .end local v7    # "y":I
    :cond_b
    return-void
.end method

.method public static getInstance()Lcom/google/zxing/common/GridSampler;
    .locals 1

    .line 55
    sget-object v0, Lcom/google/zxing/common/GridSampler;->gridSampler:Lcom/google/zxing/common/GridSampler;

    return-object v0
.end method

.method public static setGridSampler(Lcom/google/zxing/common/GridSampler;)V
    .locals 0
    .param p0, "newGridSampler"    # Lcom/google/zxing/common/GridSampler;

    .line 48
    sput-object p0, Lcom/google/zxing/common/GridSampler;->gridSampler:Lcom/google/zxing/common/GridSampler;

    .line 49
    return-void
.end method


# virtual methods
.method public abstract sampleGrid(Lcom/google/zxing/common/BitMatrix;IIFFFFFFFFFFFFFFFF)Lcom/google/zxing/common/BitMatrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation
.end method

.method public abstract sampleGrid(Lcom/google/zxing/common/BitMatrix;IILcom/google/zxing/common/PerspectiveTransform;)Lcom/google/zxing/common/BitMatrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/zxing/NotFoundException;
        }
    .end annotation
.end method
